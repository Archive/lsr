'''
Defines a class for storing point-of-regard information.

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
class POR(object):
  '''
  Stores point-of-regard data for accessibles, the user, a L{Tier}'s focus, etc.
  
  @ivar accessible: The accessible of the control which has the point of regard.
  @type accessible: L{pyLinAcc.Accessible}
  @ivar item_offset: Offset of the Item within the accessible which has the
    point of regard. Set to None to indicate the accessible has only 1 item.
    When restricting to visible, this offset will still be relative to total. 
    For example, when the first item in a table is not visible, but the first 
    item is requested, this index will not be 0 (the first in the table), but 
    instead will be the index of the first visible cell. For text, this will 
    be the character index of the first character in the line requested (i.e. 
    the first visible line in the example above).
  @type item_offset: integer or None
  @ivar char_offset: Offset of the character at the caret or virtual cursor
    within the accessible and the item which has the point of regard.
  @type char_offset: integer  
  @ivar incomplete: Indicates that a L{POR} might need additional processing
    before it is used when True. Some L{AEInterfaces.IEventHandler} may create
    L{POR}s that have an item in the L{accessible} slot instead of its parent as
    the L{accessible} and its index as the L{item_offset} in order to save time
    for events that may never be handled. This flag indicates this situation and
    suggests that the L{POR} be corrected properly before it reaches a L{Perk}
    and its L{Task}s for processing. 
  @type incomplete: boolean
  @ivar adapters: Cached adapters for L{AEInterfaces} for this L{POR}
  @type adapters: dictionary

  @note: This class is a structure and based on the LSR coding conventions its
    members (except for the adapters dictionary) can be accessed directly. 
  '''
  def __init__(self, accessible=None, item_offset=None, char_offset=0,
               incomplete=False):
    '''
    Stores the provided accessible, offset to an item within the accessible, and
    offset to a character within the accessible.
    
    @param accessible: Accessible of the control which has the point of regard.
    @type accessible: L{pyLinAcc.Accessible}
    @param item_offset: Offset of the Item within the accessible which has the
      point of regard. Set to None to indicate the accessible has only 1 item.
      When restricting to visible, this offset will still be relative to total. 
      For example, when the first item in a table is not visble, but the first 
      item is requested, this index will not be 0 (the first in the table), but 
      instead will be the index of the first visible cell. For text, this will 
      be the character index of the first character in the line requested (i.e. 
      the first visible line in the example above).
    @type item_offset: integer or None
    @param char_offset: Offset of the character at the caret or virtual cursor
      within the accessible which has the point of regard.
    @type char_offset: integer
    @param incomplete: Indicates that a L{POR} might need additional processing
      before it is used when True. Some L{AEInterfaces.IEventHandler} may 
      create L{POR}s that have an item in the L{accessible} slot instead of
      its parent as the L{accessible} and its index as the L{item_offset} in
      order to save time for events that may never be handled. This flag 
      indicates this situation and suggests that the L{POR} be corrected
      properly before it reaches a L{Perk} and its L{Task}s for processing.
    @type incomplete: boolean
    '''
    self.accessible = accessible
    self.item_offset = item_offset
    self.char_offset = char_offset
    self.incomplete = incomplete
    self.adapters = {}
    
  def getAdapter(self, interface):
    '''
    Gets a cached instead of an adapter for this L{POR} to one of the 
    L{AEInterfaces}.
    
    @param interface: Interface serving as the key for the cache
    @type interface: L{AccAdapt.Interface} class
    '''
    return self.adapters[interface]
  
  def cacheAdapter(self, interface, adapter):
    '''
    Caches an adapter instance that has been previously used for this L{POR}
    to provide the given interface.
    
    @param interface: Interface serving as the key for the cache
    @type interface: L{AccAdapt.Interface} class
    @param adapter: Adapter instance to cache
    @type adapter: L{AccAdapt.PORAdapter}
    '''
    self.adapters[interface] = adapter

  def __eq__(self, other):
    '''
    Compares this L{POR} to the one provided based on all their properties.
    
    @param other: Point of regard to compare
    @type other: L{POR}
    '''
    try:
      return (self.item_offset == other.item_offset) and \
             (self.char_offset == other.char_offset) and \
             (self.accessible == other.accessible)
    except:
      return False
    
  def __hash__(self):
    '''
    Hashes based on the accessible, item offset, and character offset rather 
    than the ID of the L{POR} itself.
    
    @return: Immutable hash code based on the attributes of the L{POR}
    @rtype: integer
    '''
    return hash(self.accessible)^(self.item_offset or 0)^(self.char_offset)
  
  def __ne__(self, other):
    '''
    Compares this L{POR} to the one provided based on all their properties.
    
    @param other: Point of regard to compare
    @type other: L{POR}
    '''
    return not self.__eq__(other)
  
  def __str__(self):
    '''
    Gets a string describing this POR in terms of its L{accessible}, 
    L{item_offset}, and L{char_offset}
    
    @return: String representation
    @rtype: string
    '''
    s = 'POR(%s, %s, %d)' % (self.accessible, self.item_offset, 
                             self.char_offset)
    return s
  
  def isSameChar(self, other):
    '''
    Checks if the character offset in this L{POR} is the same as the one in the
    given L{POR}.
    
    @param other: Point of regard with the character offset to compare
    @type other: L{POR}
    @return: Same character offset?
    @rtype: boolean
    '''
    return self.char_offset == other.char_offset
  
  def isSameItem(self, other):
    '''
    Checks if the item offset in this L{POR} is the same as the one in the
    given L{POR}.
    
    @param other: Point of regard with the item offset to compare
    @type other: L{POR}
    @return: Same item offset?
    @rtype: boolean
    '''
    return self.item_offset == other.item_offset
  
  def isSameAcc(self, other):
    '''
    Checks if the accessible object in this L{POR} is the same as the one in the
    given L{POR}.
    
    @param other: Point of regard with the accessible to compare
    @type other: L{POR}
    @return: Same accessible?
    @rtype: boolean
    '''
    return self.accessible == other.accessible
  
  def isCharAfter(self, other):
    '''
    Checks if the character offset in this L{POR} is after the one in the given
    L{POR}.
    
    @param other: Point of regard with the character offset to compare
    @type other: L{POR}
    @return: Character after the other?
    @rtype: boolean
    '''
    return self.char_offset > other.char_offset
  
  def isCharBefore(self, other):
    '''
    Checks if the character offset in this L{POR} is before the one in the given
    L{POR}.
    
    @param other: Point of regard with the character offset to compare
    @type other: L{POR}
    @return: Character before the other?
    @rtype: boolean
    '''
    return self.char_offset < other.char_offset
 
  def isItemBefore(self, other):
    '''
    Checks if the item offset in this L{POR} is before the one in the given
    L{POR}.
    
    @param other: Point of regard with the item offset to compare
    @type other: L{POR}
    @return: Item before the other?
    @rtype: boolean
    '''
    return self.item_offset < other.item_offset
  
  def isItemAfter(self, other):
    '''
    Checks if the item offset in this L{POR} is after the one in the given
    L{POR}.
    
    @param other: Point of regard with the item offset to compare
    @type other: L{POR}
    @return: Item after the other?
    @rtype: boolean
    '''
    return self.item_offset > other.item_offset
