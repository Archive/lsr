import ORBit, bonobo

IID = 'OAFIID:GNOME_Speech_SynthesisDriver_Festival:proto0.3'
ORBit.CORBA.ORB_init()
driver = bonobo.activation.activate_from_id(IID, 0, False)
driver.driverInit()
voices = driver.getAllVoices()
