#!/usr/bin/python
'''
Emits code for sayZZZ() methods for Task.Tools.Output, where ZZZ is the kind of
information to be output.  The code is saved in a file (named by the constant 
OBJECT_FILE) in the same directory.

@author: Brett Clippingdale
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''

import sys, os

###############################################################################
#
# Beginning of section for manipulating method data.  Each method must have a 
# semantic name, doc string, and core code section.
#
###############################################################################

METHODS = {'name': 'TextAttrs',
           'sem' : 'TEXT_ATTR',
           'doc': 'Calls L{Task.Tools.View.getAccAllTextAttrs} and outputs the result.',
           'code':
'''
    attrs = self.getAccAllTextAttrs(por)
    if not attrs:
      return False
    text = ', '.join(attrs.items())
'''
           },{
          'name':'Word',
          'doc': 'Calls L{Task.Tools.View.getWordText} and outputs the result.',
          'code':
'''
    text = self.getWordText(por)
'''   
          },{
          'name':'Char',
          'doc': 'Calls L{Task.Tools.View.getCharText} and outputs the result.',
          'code':
'''
    text = self.getCharText(por)
'''
          },{
          'name':'Name',
          'doc':'Calls L{Task.Tools.View.getAccName} and outputs the result.',
          'code': 
'''
    text = self.getAccName(por)
'''
          },{
          'name':'Label',
          'doc': 'Calls L{Task.Tools.View.getAccLabel} and outputs the result.',
          'code': 
'''
    text = self.getAccLabel(por)
'''            
          },{
          'name':'Item',
          'doc': 'Calls L{Task.Tools.View.getItemText} and outputs the result.',
          'code': 
'''
    text = self.getItemText(por)
'''            
          },{  
          'name':'Window',
          'doc': 'Calls L{Task.Tools.View.getWindowTitle} and outputs the result.',
          'code':
'''
    text = self.getWindowTitle(por)
'''
          },{  
          'name':'Section',
          'doc': 'Calls L{Task.Tools.View.getAccLabel} and outputs the result.',
          'code':
'''
    text = self.getAccLabel(por)
'''
          },{
          'name': 'App',
          'doc': 'Calls L{Task.Tools.View.getAppName} and outputs the result.',
          'code': 
'''
    text = self.getAppName(por)
'''
          },{ 
          'name': 'Font',
          'doc': '''Calls L{Task.Tools.View.getAccTextAttr} and outputs the value
associated with the "family-name" property.''',
          'code':
'''
    # @todo: DBC: this will not work for all applications, improve this
    text = self.getAccTextAttr('family-name', por)
'''
          },{
          'name':'Count',
          'doc': 'Calls L{Task.Tools.View.getAccCount} and outputs the result.',
          'code':
'''
    text = self.getAccCount(por)
'''
          },{
          'name':'Index',
          'doc': 'Calls L{Task.Tools.View.getAccIndex} and outputs the result.',
          'code':
'''
    text = self.getAccIndex(por)
'''
          },{
          'name':'Color',
          'doc': 
          '''Calls L{Task.Tools.View.getAccTextAttr} and outputs the value
associated with the "fg-color" property.''',
          'code': 
'''
    # @todo: PP: will not work in all cases, improve
    text = self.getAccTextAttr('fg-color', por)
'''
          },{
          'name': 'Role',
          'doc': 'Calls L{Task.Tools.View.getAccRoleName} and outputs the result.',
          'code': 
'''
    text = self.getAccRoleName(por)
'''
          },{
          'name':'Level',
          'doc': 'Calls L{Task.Tools.View.getAccLevel and outputs the result.',
          'code':
'''
    text = self.getAccLevel(por)
'''
          },{
          'name' : 'Status',
          'doc': 'Calls L{Task.Tools.View.getItemText} and outputs the result.',
          'code' :
'''
    text = self.getItemText(por)
'''
          },{
          'name':'State',
          'doc': 'Calls L{Task.Tools.View.getStateText} and outputs the result.',
          'code':
'''
    text = self.getStateText(por)
'''
          },{
          'name':'Desc',
          'doc': 'Calls L{Task.Tools.View.getAccDesc} and outputs the result.',
          'code':
'''
    text = self.getAccDesc(por)
'''            
          },{
          'name':'Hotkey',
          'doc':'''Calls L{Task.Tools.View.getAccClickKey} and outputs the 
result after replacing <> characters.''',
          'code':
'''
    text = self.getAccClickKey(por)
    # remove extra characters from the text; this will probably break hotkeys
    # that use < and >; can fix with a regex
    try:
      text = text.replace('<', '')
      text = text.replace('>', ', ')
    except AttributeError:
      pass
'''
          },{
          'name': 'Error',
          'doc': 'Outputs an LSR system error message. L{POR} ignored.',
          'code': ''
          },{
          'name': 'Info',
          'doc' : 'Outputs an informational LSR system message. L{POR} ignored.',
          'code': ''
          },{
          'name': 'Confirm',
          'doc' : 'Outputs a confirmation message for a user action. L{POR} ignored.',
          'code': ''
          },{
          'name': 'Value',
          'doc' : 'Calls L{Task.Tools.View.getAccFloatValue} and outputs the result.',
          'code': 
'''
    text = self.getAccFloatValue(por)
'''
          },{
          'name': 'ValueExtents',
          'sem' : 'EXTENTS',
          'doc' : 'Calls L{Task.Tools.View.getAccFloatValueExtents} and outputs the result.',
          'code': 
'''
    text = self.getAccFloatValueExtents(por)
'''
          }


##############################################################################
#
# End of section for manipulating method data.  
#
###############################################################################

template = """\
def say%(name)s(self, por=None, text=None, template=u'%%s', 
                dev=None, talk=True):
  '''
  %(doc)s

  @param por: L{POR} pointing at an accessible whose value will be read or
    None when the L{task_por} should be used. The value is read only if the
    text parameter is None. 
  @type por: L{POR}
  @param text: Manually provided text fetched from the L{POR}
  @type text: string
  @param template: Template for text to be spoken. Defaults to %%s indicating 
    the text or value retrieved from the L{POR} is spoken by itself.
  @type template: string
  @param dev: The device to use for output. Defaults to the default output 
    device established by L{setPerkIdealOutput}.
  @type dev: L{AEOutput.Base}
  @param talk: Send a L{AEConstants.Output.CMD_TALK} immediately after this 
    string is sent?
  @type talk: boolean
  @return: Was anything sent for output (True) or was text None (False)?
  @rtype: boolean
  '''
  if text is None:
    por = por or self.task_por
%(code)s
  if text is None:
    return False
  self.say(template %% text, por, AEConstants.%(const)s, self.layer, dev,
           talk)
  return True
"""

def emitSay(name='', doc='', code='', sem=None):
  code = code[1:-1]
  sem = sem or name
  const = 'SEM_' + sem.upper()
  return template % locals()

def main(): 
  for method in METHODS:
    say = emitSay(**method)
    print say

if __name__ == '__main__':
  main()    
    