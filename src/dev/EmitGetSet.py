#!/usr/bin/python
'''
Emits code for get/set methods on attributes for Task.Tools.Output, saving the
code in a file (named by the constant OBJECT_FILE) in same directory.

@author: Brett Clippingdale
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''

import sys, os
OBJECT_FILE = 'emitted_getset_code.py'

GS = ['Mute', 'Voice', 'Volume', 'Rate', 'Pitch']

SET_TEMPLATE = """\
def set%(attr)s(self, val, dev=None, sem=None, layer=None):
  '''
  Sets the value of the %(attr)s setting for the style paired with the given
  device, semantic, and layer. If all of these are None, the default style
  for the ideal output device will be used.

  @param val: The new value for the %(attr)s.
  @type val: object
  @param dev: The device on which this setting should be changed. Defaults to 
    the default output device established by L{setPerkIdealOutput}.
  @type dev: L{AEOutput.Base}
  @param sem: The semantic on which to change the value. Defaults to None 
    meaning the default style
  @type sem: integer
  @param layer: Layer on which to set the value: focus, tier, or background.
    Defaults to the event layer that triggered this L{Task}.
  @type layer: integer
  @raise InvalidStyleError: When the style or the setting name is not valid
  '''
  dev = dev or self.def_out
  layer = layer or self.layer
  s = self.device_man.getStyle(dev, sem, layer)
  try:
    s.setSettingVal('%(attr)s', val)
  except (TypeError, KeyError):
    raise InvalidStyleError
"""

GET_TEMPLATE = """\
def get%(attr)s(self, dev=None, sem=None, layer=None):
  '''  
  Gets the value of %(attr)s for the style paired with the given device,
  semantic, and layer. If all of these are None, the default style for the
  ideal output device will be used.

  @param dev: The device from which this setting should be fetched. Defaults 
    to the default output device established by L{setPerkIdealOutput}.
  @type dev: L{AEOutput.Base}
  @param sem: The semantic on which to change the value. Defaults to None 
    meaning the default style
  @type sem: integer
  @param layer: Layer on which to set the value: focus, tier, or background.
    Defaults to the event layer that triggered this L{Task}.
  @type layer: integer
  @return: The current value of the setting
  @rtype: object
  @raise InvalidStyleError: When the style or the setting name is not valid
  '''
  dev = dev or self.def_out
  layer = layer or self.layer
  s = self.device_man.getStyle(dev, sem, layer)
  try:
    return s.getSettingVal('%(attr)s')
  except (TypeError, KeyError):
    raise InvalidStyleError
"""

def main(): 
  GS.sort()
  for attr in GS:
    print(SET_TEMPLATE % locals())
    print(GET_TEMPLATE % locals())

if __name__ == '__main__':
  main()    
    
