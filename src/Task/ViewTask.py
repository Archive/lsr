'''
Defines a L{Task} to execute in response to a L{AEEvent.ViewChange}.

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''

import Base, EventTask, AEConstants
import AEEvent

class ViewTask(EventTask.EventTask):
  '''
  Executed when a view change occurs.
  
  This class registers its name and whether it should be monitored by default
  in an L{AEMonitor} using the L{Base.registerTaskType} function when this
  module is first imported. An L{AEMonitor} can use this information to build
  its menus. 
  '''
  Base.registerTaskType('ViewTask', True)
  
  def getEventType(self):
    '''
    @return: Type of L{AEEvent} this L{Task} wants to handle
    @rtype: class
    '''
    return AEEvent.ViewChange
  
  def update(self, por, title, gained, layer, **kwargs):
    '''
    Updates this L{Task} in response to a consumed view change event. Called by 
    L{Tier.Tier._executeTask}.
    
    @param por: Point of regard to the root of the new view
    @type por: L{POR}
    @param title: Title of the view
    @type title: string
    @param gained: The kind of view change event
    @type gained: integer
    @param layer: Layer on which the event occurred
    @type layer: integer
    '''
    pass

  def execute(self, por, title, gained, layer, **kwargs):
    '''
    Executes this L{Task} in response to a view change event. Called by 
    L{Tier.Tier._executeTask}.
    
    @param por: Point of regard to the root of the new view
    @type por: L{POR}
    @param title: Title of the view
    @type title: string
    @param gained: The kind of view change event
    @type gained: integer
    @param layer: Layer on which the event occurred
    @type layer: integer
    @return: True to allow other L{Task}s to process this event
    @rtype: boolean
    '''
    if gained == AEConstants.EVENT_VIEW_GAINED:
      return self.executeGained(por=por, title=title, layer=layer, **kwargs)
    elif gained == AEConstants.EVENT_VIEW_LOST:
      return self.executeLost(por=por, title=title, layer=layer, **kwargs)
    elif gained == AEConstants.EVENT_VIEW_STARTUP:
      return self.executeStartup(por=por, title=title, layer=layer, **kwargs)
    elif gained == AEConstants.EVENT_VIEW_FIRST_GAINED:
      return self.executeFirstGained(por=por, title=title, layer=layer, 
                                     **kwargs)
    
  def executeGained(self, por, title, layer, **kwargs):
    '''
    Executes this L{Task} in response to a view gained event. Called by 
    L{execute}.
    
    @param por: Point of regard to the root of the new view
    @type por: L{POR}
    @param title: Title of the view
    @type title: string
    @param layer: Layer on which the event occurred
    @type layer: integer
    @return: True to allow other L{Task}s to process this event
    @rtype: boolean
    '''
    return True
  
  def executeLost(self, por, title, layer, **kwargs):
    '''    
    Executes this L{Task} in response to a view lost event. Called by
    L{execute}.
    
    @param por: Point of regard to the root of the lost view
    @type por: L{POR}
    @param title: Title of the view
    @type title: string
    @param layer: Layer on which the event occurred
    @type layer: integer
    @return: True to allow other L{Task}s to process this event
    @rtype: boolean
    '''
    return True
  
  def executeStartup(self, por, title, layer, **kwargs):
    '''
    Executes this L{Task} in response to the view being identified during LSR
    startup. Called by L{execute}.
    
    @param por: Point of regard to the root of the mutated view
    @type por: L{POR}
    @param title: Title of the view
    @type title: string
    @param layer: Layer on which the event occurred
    @type layer: integer
    @return: True to allow other L{Task}s to process this event
    @rtype: boolean
    '''
    return True

  def executeFirstGained(self, por, title, layer, **kwargs):
    '''
    Executes this L{Task} in response to the view being identified as the 
    active view during LSR startup. Called by L{execute}.
    
    @param por: Point of regard to the root of the mutated view
    @type por: L{POR}
    @param title: Title of the view
    @type title: string
    @param layer: Layer on which the event occurred
    @type layer: integer
    @return: True to allow other L{Task}s to process this event
    @rtype: boolean
    '''
    return True
