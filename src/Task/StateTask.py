'''
Defines a L{Task} to execute when the state of an accesible or item changes.

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''

import Base, EventTask
import AEEvent

class StateTask(EventTask.EventTask):
  '''
  Executed when the state of an accesible or item changes.

  This class registers its name and whether it should be monitored by default
  in an L{AEMonitor} using the L{Base.registerTaskType} function when this
  module is first imported. The L{AEMonitor} can use this information to build
  its menus. 
  '''
  Base.registerTaskType('StateTask', False)

  def getEventType(self):
    '''
    @return: Type of L{AEEvent} this L{Task} wants to handle
    @rtype: class
    '''
    return AEEvent.StateChange
  
  def update(self, por, name, value, layer, **kwargs):
    '''
    Updates this L{Task} in response to a consumed state change event. Called by 
    L{Tier.Tier._executeTask}.
    
    @param por: Point of regard for the related accessible/item
    @type por: L{POR}
    @param name: Name of the state that changed
    @type name: string
    @param value: True if the state is now set, False if not
    @type value: boolean
    @param layer: Layer on which the event occurred
    @type layer: integer
    '''
    pass
  
  def execute(self, por, name, value, layer, **kwargs):
    '''
    Executes this L{Task} in response to a state change event. Called by 
    L{Tier.Tier._executeTask}.
    
    @param por: Point of regard for the related accessible/item
    @type por: L{POR}
    @param name: Name of the state that changed
    @type name: string
    @param value: True if the state is now set, False if not
    @type value: boolean
    @param layer: Layer on which the event occurred
    @type layer: integer
    @return: True to allow other L{Task}s to process this event
    @rtype: boolean
    '''
    return True