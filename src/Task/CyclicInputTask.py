'''
Defines a L{Task} to execute a sequence of  a L{AEEvent.InputGesture} occurs.

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
import InputTask, Base

class CyclicInputTask(InputTask.InputTask):
  '''
  Executes when an input gesture occurs. In turn, this task will execute the 
  next named L{Task.InputTask}s stored in this object.
  
  Unlike other L{Task} classes, this one is unlikely to be subclassed. The
  common use is to instantiate it directly with names of other input tasks
  and bind it to an input command.
  
  This class registers its name and whether it should be monitored by default
  in an L{AEMonitor} using the L{Base.registerTaskType} function when this
  module is first imported. The L{AEMonitor} can use this information to build
  its menus.
  '''
  Base.registerTaskType('CyclicInputTask', False)
  
  def __init__(self, ident, *names):
    '''
    Stores a sequence of named L{InputTask}s to execute.
    
    @param ident: Programmatic identifier
    @type ident: string
    @param names: Names of L{Task}s that should be executed
    @type names: list of string
    '''
    InputTask.InputTask.__init__(self, ident)
    self.names = names
    
  def execute(self, cycle_count, layer, **kwargs):
    '''
    Executes the next named L{Task} in the cycle in response to an input 
    gesture. Called by L{Tier.Tier._executeTask}.
    
    @param cycle_count: Task index in sequence to execute
    @type cycle_count: integer
    @param layer: Layer on which the event occurred, ignored here but pulled 
      out of kwargs so kwargs can be passed directly to L{doTask}
    @type layer: integer
    @param kwargs: Dictionary containing parameters passed to a task.
    @type kwargs: dictionary 
    @return: Should processing continue? Defaults to the return value from the
      invoked L{Task}.
    @rtype: boolean
    '''
    if not len(self.names):
      # do nothing if no tasks to cycle
      return
    i = cycle_count % len(self.names)
    return self.doTask(self.names[i], **kwargs)
  
  def update(self, cycle_count, layer, **kwargs):
    '''
    Executes the next named L{Task} in the cycle in response to an input 
    gesture. Called by L{Tier.Tier._executeTask}.
    
    @param cycle_count: Task index in sequence to execute
    @type cycle_count: integer
    @param layer: Layer on which the event occurred, ignored here but pulled 
      out of kwargs so kwargs can be passed directly to L{doTask}
    @type layer: integer
    @param kwargs: Dictionary containing parameters passed to a task.
    @type kwargs: dictionary 
    @return: Should processing continue? Defaults to the return value from the
      invoked L{Task}.
    @rtype: boolean
    '''
    if not len(self.names):
      # do nothing if no tasks to cycle
      return
    i = cycle_count % len(self.names)
    return self.doTask(self.names[i], propagate=False, **kwargs)
