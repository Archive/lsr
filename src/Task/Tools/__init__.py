'''
Defines the L{Tools} available to L{Task}s and L{Perk}s at runtime. Tools are
grouped in classes according to common lines of functionality to aid in their
maintenance and their documentation.

@author: Peter Parente
@author: Pete Brunet
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''

from Output import Output
from Input import Input
from View import View
from Utils import Utils
from System import System
import Error

class All(Output, Input, View, Utils, System):
  '''
  Provides all standard L{Tools} functionality under one class for easy 
  subclassing by L{Task}s and L{Perk}s.
  '''
  pass
