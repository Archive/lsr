'''
Defines L{Tools} for navigating within an application and inspecting and 
manipulating its accessible objects.

@author: Peter Parente
@author: Pete Brunet
@author: Larry Weiss
@author: Brett Clippingdale
@author: Eirikur Hallgrimsson
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''

from AEInterfaces import *
from Walker import *
from Error import *
from POR import POR
from i18n import _
from pyLinAcc import Interfaces
import Base
import AEConstants
import Word

class View(Base.TaskTools):
  '''
  Provides methods for reading information from accessible objects and for 
  navigating relative to a L{POR}.
  
  Methods prefixed with I{get} do not modify the L{task_por} variable. They 
  only return the L{POR} of interest. Methods prefixed with I{move} do modify 
  these variables. Methods prefixed with I{set} actually change properties of 
  the accessible itself. 

  @note: Selected and editable states not being announced because they happen a
    unexpected times or on undesireable accessibles. Selection can probably be
    done properly with events. Editable we will just avoid for the time being.
  
  @cvar _state_descriptions: Mapping from state name/value pairs to localized 
    string
  @type _state_descriptions: dictionary of 2-tuple, string pairs
  '''
  _state_descriptions = {('checked', False) : _('unchecked'),
                         ('enabled', False) : _('disabled'),
                         #('selected', False) : _('not selected'),
                         ('collapsed', False) : _('expanded'),
                         ('expanded', False) : _('collapsed'),
                         ('animated', False) : _('not animated'),
                         #('editable', False) : _('not editable'),
                         ('checked', True) : _('checked'),
                         ('unchecked', True) : _('unchecked'),
                         #('selected', True) : _('selected'),
                         ('collapsed', True) : _('collapsed'),
                         ('expanded', True) : _('expanded'),
                         ('animated', True) : _('animated'),
                         ('disabled', True) : _('disabled'),
                         #('editable', True) : _('editable')
                       }

  def getAccFloatValue(self, por=None):
    '''
    Gets the floating point value at the given L{POR}.
    When L{por} is None, the L{task_por} is used.

    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @return: Floating point value
    @rtype: float
    @raise PORError: When the L{POR} is invalid
    '''
    por = por or self.task_por
    try:
      ai = IAccessibleInfo(por)
      return ai.getAccFloatValue()
    except LookupError:
      raise PORError
    except NotImplementedError:
      return None  

  def getAccFloatValueExtents(self, por=None):
    ''' 
    Get the extents and step size of the floating point value at the L{POR}.
    When L{por} is None, the L{task_por} is used.

    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @return: The minimum possible value, the maximum possible value, and the
      step size for the floating point value
    @rtype: 3-tuple of float
    @raise PORError: When the L{POR} is invalid
    '''
    por = por or self.task_por
    try:
      ai = IAccessibleInfo(por)
      return ai.getAccFloatValueExtents()
    except LookupError:
      raise PORError
    except NotImplementedError:
      return None

  def getAccDefTextAttrs(self, por=None):
    '''
    Gets a dictionary of name:value default text attribute pairs at the provided
    L{POR}. When L{por} is None, the L{task_por} is used.

    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @return: Name/value pairs for all available attributes
    @rtype: dictionary
    @raise PORError: When the L{POR} is invalid
    '''
    por = por or self.task_por
    try:
      # get the info interface
      ai = IAccessibleInfo(por)
      return ai.getAccDefTextAttrs()
    except LookupError:
      # raise POR error if some accessible was dead, bad or missing
      raise PORError
    except NotImplementedError:
      return None

  def getAccTextSelection(self, n=None, por=None):
    '''    
    Gets a list of all selected text if n is None, or the nth selection if it
    is specified, at the L{POR}. When L{por} is None, the L{task_por} is used.

    @param n: Index of text selection to get or None
    @type n: integer
    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @return: List of selected text strings
    @rtype: list of string
    @raise PORError: When the L{POR} is invalid
    '''
    por = por or self.task_por
    try:
      ai = IAccessibleInfo(por)
      return ai.getAccTextSelection(n)
    except LookupError:
      raise PORError
    except NotImplementedError:
      return None

  def getAccText(self, start=None, end=None):
    '''
    Gets all of the text from the starting offset to the ending offset at the
    given L{POR}s. If start is None, defaults to the beginning of the text. If
    end is None, defaults to the end of the text. When L{start} or L{end} is 
    None, the L{task_por} is used possibly for both cases.

    @param start: Point of regard to the start of the text run; use the current
      task's L{POR} if none supplied
    @type start: L{POR}
    @param end: Point of regard to the end of the text run; use the current
      task's L{POR} if none supplied
    @type end: L{POR}
    @return: Text between start and end offsets
    @rtype: string
    @raise PORError: When the L{POR} is invalid
    '''
    start = start or self.task_por
    end = end or self.task_por
    try:
      return IAccessibleInfo(start).getAccText(end)
    except LookupError:
      raise PORError
    except NotImplementedError:
      return None
  
  def getWindowTitle(self, por=None):
    '''
    Gets the accessible name of the window containing the L{POR}.
    When L{por} is None, the L{task_por} is used.
    
    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @return: Name of the window
    @rtype: string
    @raise PORError: When the L{POR} is invalid
    '''
    por = por or self.task_por
    frame_por = self.getRootAcc(por)    
    try:
      return IAccessibleInfo(frame_por).getAccName()
    except LookupError:
      raise PORError
    except NotImplementedError:
      return None  
  
  def getAppName(self, por=None):
    '''
    Gets the accessible name of the application containing the L{POR}.
    When L{por} is None, the L{task_por} is used.
    
    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @return: Name of the application
    @rtype: string
    @raise PORError: When the L{POR} is invalid
    '''
    por = por or self.task_por
    try:
      return IAccessibleInfo(por).getAccAppName()
    except LookupError:
      raise PORError
    except NotImplementedError:
      return None

  def getAccAppLocale(self, por=None):
    '''
    Gets the POSIX locale of the application containing the L{POR} as a string.
    When L{por} is None, the L{task_por} is used.
    
    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @return: POSIX locale of the application
    @rtype: string
    @raise PORError: When the L{POR} is invalid
    '''
    por = por or self.task_por
    try:
      return IAccessibleInfo(por).getAccAppLocale()
    except LookupError:
      raise PORError
    except NotImplementedError:
      return None

  def getAccAttrs(self, por=None):
    '''
    Gets a dictionary of name:value attribute pairs at the provided L{POR}. 
    When L{por} is None, the L{task_por} is used.

    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @return: Name/value pairs for all available attributes
    @rtype: dictionary of string
    @raise PORError: When the L{POR} is invalid
    '''
    por = por or self.task_por
    try:
      # get the info interface
      ai = IAccessibleInfo(por)
      return ai.getAccAttrs()
    except LookupError:
      # raise POR error if some accessible was dead, bad or missing
      raise PORError
    except NotImplementedError:
      return None

  def getAccName(self, por=None):
    '''
    Gets the accessible name of the component at the provided L{POR}. 
    When L{por} is None, the L{task_por} is used.
    
    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @return: The accessible name of the control of the at the point of regard
    @rtype: string
    @raise PORError: When the L{POR} is invalid
    '''
    por = por or self.task_por
    try:
      return IAccessibleInfo(por).getAccName()
    except LookupError:
      raise PORError
    except NotImplementedError:
      return None
  
  def getAccDesc(self, por=None):
    '''
    Gets the accessible description of the component at the provided L{POR}. 
    When L{por} is None, the L{task_por} is used.

    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @return: The accessible description of the control of the at the point of 
      regard
    @rtype: string
    @raise PORError: When the L{POR} is invalidd
    '''
    por = por or self.task_por
    try:
      return IAccessibleInfo(por).getAccDescription()
    except LookupError:
      raise PORError
    except NotImplementedError:
      return None
      
  def getAccCount(self, por=None):
    '''
    Gets the number of child accessibles of the accessible indicated by
    the given L{POR}. When L{por} is None, the L{task_por} is used.
    
    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @return: Number of accessible children
    @rtype: integer
    @raise PORError: When the L{POR} is invalid
    '''
    por = por or self.task_por
    try:
      ai = IAccessibleInfo(por)
      return ai.getAccChildCount()
    except LookupError:
      raise PORError
    except NotImplementedError:
      return None
  
  def getAccRoleName(self, por=None):
    '''
    Gets the localized role name for the accessible at the provided L{POR}. 
    When L{por} is None, the L{task_por} is used.
    
    The return value of this method is suitable for output. It is not suitable
    for comparison to known role values.
    
    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @return: The role name of the control of the at the point of regard
    @rtype: string
    @raise PORError: When the L{POR} is invalid
    '''
    por = por or self.task_por
    try:
      return IAccessibleInfo(por).getAccRoleName()
    except LookupError:
      raise PORError
    except NotImplementedError:
      return None
    
  def getAccRole(self, por=None):
    '''
    Gets the unlocalized role for the accessible at the provided L{POR}. 
    When L{por} is None, the L{task_por} is used.
    
    The return value of this method is not suitable for output. It is suitable
    for comparison to known role values.
    
    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @return: The role name of the control of the at the point of regard
    @rtype: string
    @raise PORError: When the L{POR} is invalid
    '''
    por = por or self.task_por
    try:
      return IAccessibleInfo(por).getAccRole()
    except LookupError:
      raise PORError
    except NotImplementedError:
      return None
    
  def getAccActionNames(self, por=None):
    '''
    Gets the names of the actions that can be performed at the provided L{POR}.
    When L{por} is None, the L{task_por} is used.
  
    @note: The names appear to be unlocalized.
    
    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @return: Names of the actions at the point of regard
    @rtype: list of string
    @raise PORError: When the L{POR} is invalid
    '''
    por = por or self.task_por
    try:
      return IAccessibleInfo(por).getAccActionNames()
    except LookupError:
      raise PORError
    except NotImplementedError:
      return None
    
  def getAccActionDescs(self, por=None):
    '''
    Gets the descriptions of the actions that can be performed at the 
    provided L{POR}. When L{por} is None, the L{task_por} is used.
    
    @note: Unsure if the descriptions are localized or not.
    
    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @return: Descriptions of the actions at the point of regard
    @rtype: list of string
    @raise PORError: When the L{POR} is invalid
    '''
    por = por or self.task_por
    try:
      return IAccessibleInfo(por).getAccActionDescs()
    except LookupError:
      raise PORError
    except NotImplementedError:
      return None
    
  def getAccActionKeys(self, por=None):
    '''
    Gets the key bindings associated with the actions at the given L{POR}. 
    When L{por} is None, the L{task_por} is used.
    
    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @return: Names of key sequences that all trigger the same action
    @rtype: list of tuple of string
    @raise PORError: When the L{POR} is invalid
    '''
    por = por or self.task_por
    try:
      return IAccessibleInfo(por).getAccActionKeys()
    except LookupError:
      raise PORError
    except NotImplementedError:
      return None
    
  def getAccClickKey(self, por=None):
    '''    
    Gets the key binding associated with the common "click" action which
    appears to be the action most commonly accessed by a hotkey. Only returns
    the last of all defined hotkey sequences as it is usually the globally
    available hotkey rather than the local menu mnemonic or a sequence for 
    activating the menu followed by the mnemonic. When L{por} is None, 
    the L{task_por} is used.
    
    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @return: Hotkey sequence
    @rtype: string
    @raise PORError: When the L{POR} is invalid
    '''
    por = por or self.task_por
    names = self.getAccActionNames(por)
    try:
      i = names.index('click')
    except (ValueError, NotImplementedError, AttributeError):
      return None
    keys = self.getAccActionKeys(por)
    try:
      # return None if the key is blank
      return keys[i][-1] or None
    except IndexError:
      return None
    
  def getAccLevel(self, por=None):
    '''
    Gets the tree level of the item indicated by the given L{POR}. When L{por} 
    is None, the L{task_por} is used.
     
    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @return: Zero indexed level of the item
    @rtype: integer
    @raise PORError: When the L{POR} is invalid
    '''
    por = por or self.task_por
    try:
      ai = IAccessibleInfo(por)
      return ai.getAccLevel()
    except LookupError:
      raise PORError
    except NotImplementedError:
      return None

  def getAccTableExtents(self, por=None):
    '''
    Returns the number of rows and columns in a table indicated by the given
    L{POR}. When L{por} is None, the L{task_por} is used.
    
    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @return: Count of rows and columns
    @rtype: 2-tuple of integer
    @raise PORError: When the L{POR} is invalid
    '''
    por = por or self.task_por
    try:
      ai = IAccessibleInfo(por)
      return ai.getAccTableExtents()
    except LookupError:
      raise PORError
    except NotImplementedError:
      return None

  def getAccVisualExtents(self, por=None):
    '''
    Returns the extents of a visual component at the L{POR}. When L{por} is
    None, the L{task_por} is used.
    
    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @return: Width and height of the point of regard
    @rtype: 2-tuple of integer
    @raise PORError: When the L{POR} is invalid
    '''
    por = por or self.task_por
    try:
      ai = IAccessibleInfo(por)
      return ai.getAccVisualExtents()
    except LookupError:
      raise PORError
    except NotImplementedError:
      return None

  def getAccVisualPoint(self, por=None):
    '''
    Returns the focal point of a visual component at the L{POR}. When L{por} is
    None, the L{task_por} is used.
    
    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @return: x,y coordinates of the focal point of the point of regard
    @rtype: 2-tuple of integer
    @raise PORError: When the L{POR} is invalid
    '''
    por = por or self.task_por
    try:
      ai = IAccessibleInfo(por)
      return ai.getAccVisualPoint()
    except LookupError:
      raise PORError
    except NotImplementedError:
      return None
   
  def getAccPosition(self, por=None):
    '''
    Gets the position of the accessible object, usually upper-left corner
    
    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @return: x,y coordinates of the accessible's position
    @rtype: 2-tuple of integer
    @raise PORError: When the L{POR} is invalid
    '''
    por = por or self.task_por
    try:
      ai = IAccessibleInfo(por)
      return ai.getAccPosition()
    except LookupError:
      raise PORError
    except NotImplementedError:
      return None

  def getAccRow(self, por=None):
    '''
    Gets the row index of the L{POR} within some 2D ordered collection. When
    L{por} is None, the L{task_por} is used.
    
    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @return: Zero indexed row of the item
    @rtype: integer
    @raise PORError: When the L{POR} is invalid
    '''
    por = por or self.task_por
    try:
      ai = IAccessibleInfo(por)
      return ai.getAccRow()
    except LookupError:
      raise PORError
    except NotImplementedError:
      return None
    
  def getAccColumn(self, por=None):
    '''
    Gets the column index of the L{POR} within some 2D ordered collection. When
    L{por} is None, the L{task_por} is used.
    
    @param por: A point of regard; use the current task's POR if none supplied
    @type por: L{POR}
    @return: Zero indexed column of the item
    @rtype: integer
    @raise PORError: When the L{POR} is invalid
    '''
    por = por or self.task_por
    try:
      ai = IAccessibleInfo(por)
      return ai.getAccColumn()
    except LookupError:
      raise PORError
    except NotImplementedError:
      return None
    
  def getAccAtRowColumn(self, row, col, por=None):
    '''
    Gets the L{POR} to the cell in the given L{POR} at the given row and 
    column. When L{por} is None, the L{task_por} is used.
    
    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @param row: Row offset
    @type row: integer
    @param col: Column offset
    @type col: integer
    @return: Point of regard to the given row and column
    @rtype: L{POR}
    @raise PORError: When the L{POR} is invalid
    '''
    por = por or self.task_por
    try:
      ai = IAccessibleInfo(por)
      index = ai.getAccRowColIndex(row, col)
      return POR(por.accessible, index)
    except LookupError:
      raise PORError
    except NotImplementedError:
      return None
    
  def getAccRowHeader(self, por=None):
    '''    
    Gets the text description of a row in a table. When L{por} is None, the
    L{task_por} is used.
            
    @param por: A point of regard; use the current task's POR if none supplied
    @type por: L{POR}
    @return: text description of the row, if there is one.
    @rtype: string
    @raise PORError: When the L{POR} is invalid
    '''
    por = por or self.task_por
    try:
      ai = IAccessibleInfo(por)
      return ai.getAccRowHeader()
    except LookupError:
      raise PORError
    except NotImplementedError:
      return None

  def getAccColumnHeader(self, por=None):
    '''    
    Gets the text description of a column in a table. When L{por} is None, the
    L{task_por} is used.
            
    @param por: A point of regard; use the current task's POR if none supplied
    @type por: L{POR}
    @return: text description of the column, if there is one.
    @rtype: string
    @raise PORError: When the L{POR} is invalid
    '''
    por = por or self.task_por
    try:
      ai = IAccessibleInfo(por)
      return ai.getAccColumnHeader()
    except LookupError:
      raise PORError
    except NotImplementedError:
      return None

  def getAccIndex(self, por=None):
    '''
    Gets the index the L{POR} within some 1D ordered collection. When L{por} is
    None, the L{task_por} is used.
    
    @param por: A point of regard; use the current task's POR if none supplied
    @type por: L{POR}
    @return: Zero index of the item
    @rtype: integer
    @raise PORError: When the L{POR} is invalid
    '''
    por = por or self.task_por
    try:
      ai = IAccessibleInfo(por)
      return ai.getAccIndex()
    except LookupError:
      raise PORError
    except NotImplementedError:
      return None
    
  def getAccLabel(self, por=None):
    '''
    Gets the label for the accessible at the provided L{POR}. When L{por} is 
    None, the L{task_por} is used.
    
    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @return: The AccessibleName of the component that labels the component at 
      the given location.
    @rtype: string
    @raise PORError: When the L{POR} is invalid
    '''
    por = por or self.task_por
    try:
      # get the accessible info interface
      ai = IAccessibleInfo(por)
      # get all labelled by relations
      pors = ai.getAccRelations('labelled by')
    except LookupError:
      raise PORError
    except NotImplementedError:
      return None
    # get names of all label accessibles
    label = []
    for por in pors:
      try:
        label.append(self.getItemText(por))
      except PORError:
        # ignore POR errors
        pass
    # join names into a string
    if not label:
      return None
    return ' '.join(label)
  
  def getAccRelations(self, relation, por=None):
    '''    
    Gets the L{POR}s to accessibles related to the given L{POR} in the manner
    stated by the given relation. When L{por} is None, the L{task_por} is used.
    
    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @param relation: Type of relation
    @type relation: string
    @return: Point of regard to related accessibles
    @rtype: list of L{POR}s
    @raise PORError: When the L{POR} is invalid
    '''
    por = por or self.task_por
    try:
      # get the accessible info interface
      ai = IAccessibleInfo(por)
      # get all labelled by relations
      return ai.getAccRelations(relation)
    except LookupError:
      raise PORError
    except NotImplementedError:
      return None
  
  def hasAccState(self, state, por=None):
    '''        
    Gets if the accessible at the given L{POR} has the given state. When L{por}
    is None, the L{task_por} is used. The state is assumed to be a string that
    can be mapped to an appropriate state constant on the platform or indicates
    an extension state that is represented by a string.
    
    @param state: Name of a state (e.g. 'focused', 'selected', 'selectable')
    @type state: string
    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @return: Does the L{POR} have the given state?
    @rtype: boolean
    @raise PORError: When the L{POR} is invalid
    '''
    por = por or self.task_por
    try:
      ai = IAccessibleInfo(por)
      return ai.hasAccState(state)
    except LookupError:
      raise PORError
    except NotImplementedError:
      return False
    
  def hasOneAccState(self, por=None, *states):
    '''
    Gets if the accessible at the given L{POR} has one of the given states.
    When L{por} is None, the L{task_por} is used. The state is assumed to be a
    string that can be mapped to an appropriate state constant on the platform
    or indicates an extension state that is represented by a string.
    
    This method is more efficient than calling L{hasAccState} multiple times.
    
    @param states: Names of states (e.g. 'focused', 'selected', 'selectable')
    @type states: string
    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @return: Does the L{POR} have the given state?
    @rtype: boolean
    @raise PORError: When the L{POR} is invalid
    '''
    por = por or self.task_por
    try:
      ai = IAccessibleInfo(por)
      return ai.hasAccOneState(*states)
    except LookupError:
      raise PORError
    except NotImplementedError:
      return False
      
  def hasAccRole(self, role, por=None):
    '''    
    Gets if the accessible at the given L{POR} has the given role. When
    L{por} is None, the L{task_por} is used. The role is assumed to be a string
    that can be mapped to an appropriate role constant on the platform or 
    indicates an extension role that is represented by a string.
    
    @param role: Name of a role (e.g. 'terminal', 'glass pane', 'button')
    @type role: string
    @param por: A point of regard; use the current task's POR if none supplied
    @type por: L{POR}
    @return: Does the L{POR} have the given role?
    @rtype: boolean
    @raise PORError: When the L{POR} is invalid
    '''
    por = por or self.task_por
    try:
      ai = IAccessibleInfo(por)
      return ai.hasAccRole(role)
    except LookupError:
      raise PORError
    except NotImplementedError:
      return False
    
  def hasOneAccRole(self, por=None, *roles):
    '''    
    Gets if the accessible at the given L{POR} has any one of the given roles.
    When L{por} is None, the L{task_por} is used. The role is assumed to be a
    string that can be mapped to an appropriate role constant on the platform
    or indicates an extension role that is represented by a string.
    
    This method is more efficient than calling L{hasAccRole} multiple times.
    
    @param roles: Name of a role (e.g. 'terminal', 'glass pane', 'button')
    @type roles: string
    @param por: A point of regard; use the current task's POR if none supplied
    @type por: L{POR}
    @return: Does the L{POR} have the given role?
    @rtype: boolean
    @raise PORError: When the L{POR} is invalid
    '''
    por = por or self.task_por
    try:
      ai = IAccessibleInfo(por)
      return ai.hasAccOneRole(*roles)
    except LookupError:
      raise PORError
    except NotImplementedError:
      return False
      
  def getStateText(self, por=None, name=None, value=None):
    '''    
    Gets text describing the states of the given L{POR} that might be of
    interest to the user. When L{por} is None, the L{task_por} is used. If
    name is specified, only gets the text describing the state with that
    non-translated name. If name is not specified, gets text describing all
    states of interest.
    
    @param por: A point of regard; use the current task's POR if none supplied
    @type por: L{POR}
    @param name: Name of the state to describe
    @type name: string
    @param value: Value of the state to describe
    @type value: string
    @return: String describing the state(s), None if named state not found
    @rtype: string
    @raise PORError: When the L{POR} is invalid
    '''
    # look up just the give name and value
    if name is not None and value is not None:
      return self._state_descriptions.get((name, value))
    
    por = por or self.task_por
    try:
      # get all states
      states = IAccessibleInfo(por).getAccStates()
    except LookupError:
      raise PORError
    except NotImplementedError:
      return None
      
    text = []
    for name in states:
      try:
        # localize all state strings
        desc = self._state_descriptions[(name, True)]
      except KeyError:
        continue
      text.append(desc)
    if not text:
      return None
    return ' '.join(text)
    
  def getItemText(self, por=None):
    '''
    Gets the text of the complete item that includes the specified L{POR}. When 
    L{por} is None, the L{task_por} is used.
    
    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @return: Item text at the given point of regard
    @rtype: string
    @raise PORError: When the L{POR} is invalid
    '''
    por = por or self.task_por
    try:
      text = IAccessibleInfo(por).getAccItemText()
      return text
    except LookupError:
      raise PORError
    except NotImplementedError:
      return None
    
  def getCharText(self, por=None):
    '''
    Gets the text of the current character at the specified L{POR}. When L{por}
    is None, the L{task_por} is used.
    
    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @return: Character at the given point of regard
    @rtype: string
    @raise PORError: When the L{POR} is invalid
    '''
    por = por or self.task_por
    try:
      text = IAccessibleInfo(por).getAccItemText()
      if len(text) <= por.char_offset:
        return u''
      else:
        return text[por.char_offset]
    except LookupError:
      raise PORError
    except NotImplementedError:
      return None
  
  def getWordText(self, por=None):
    '''
    Gets the text of the current word at the specified L{POR}. When L{por} is 
    None, the L{task_por} is used.
    
    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @return: Word text at the given point of regard
    @rtype: string
    @raise PORError: When the L{POR} is invalid
    '''
    por = por or self.task_por
    try:
      text = IAccessibleInfo(por).getAccItemText()
    except LookupError:
      raise PORError
    except NotImplementedError:
      return None
    prev, curr, next = Word.getContextFromString(text, por)
    return str(curr)

  def getAllSelected(self, por=None):
    '''
    Gets all of the selected items or accessible children in the accessible 
    indicated by the given L{POR}. When L{por} is None, the L{task_por} is used. 
    
    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @return: Points of regard to selected items or accessibles
    @rtype: list of L{POR}
    @raise PORError: When the L{POR} is invalid
    '''
    por = por or self.task_por
    try:
      return IAccessibleInfo(por).getAccSelection()
    except LookupError:
      raise PORError
    except NotImplementedError:
      return None
    
  def setAllSelected(self, por=None):
    '''
    Selects all of the items or accessible children in the accessible indicated
    by the given L{POR}. When L{por} is None, the L{task_por} is used. 
    
    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @return: Is setting the focus supported?
    @rtype: boolean
    @raise PORError: When the L{POR} is invalid
    @raise SelectError: When selecting is supported but rejected by the system 
      for some reason
    '''
    por = por or self.task_por
    try:
      rv = IAccessibleAction(por).selectAcc(all=True)
    except LookupError:
      raise PORError
    except NotImplementedError:
      # selection not supported
      return False
    else:
      if not rv:
        # raise an error if supported but failed
        raise SelectError
    return True
  
  def setAllDeselected(self, por=None):
    '''    
    Deselects all of the items or accessible children in the accessible
    indicated by the given L{POR}. When L{por} is None, the L{task_por} is
    used.
    
    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @return: Is setting the focus supported?
    @rtype: boolean
    @raise PORError: When the L{POR} is invalid
    @raise SelectError: When deselecting is supported but rejected by the 
      system for some reason
    '''
    por = por or self.task_por
    try:
      rv = IAccessibleAction(por).deselectAcc(True)
    except LookupError:
      raise PORError
    except NotImplementedError:
      # selection not supported
      return False
    else:
      if not rv:
        # raise an error if supported but failed
        raise SelectError
    return True
  
  def getFirstSelected(self, por=None):
    '''
    Gets the first selected items or accessible children in the accessible 
    indicated by the given L{POR}. When L{por} is None, the L{task_por} is 
    used.
    
    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @return: Point of regard to the first selected item or accessible
    @rtype: L{POR}
    @raise PORError: When the L{POR} is invalid
    '''
    all = self.getAllSelected(por)
    try:
      return all[0]
    except TypeError:
      return None

  def doAction(self, index, por=None):
    '''
    Executes the action at the given index in the list of all actions named
    in the return value from L{getAccActionNames}. When L{por} is None, the 
    L{task_por} is used.
    
    @param index: Index of the action to execute.
    @type index: integer
    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @raise PORError: When the L{POR} is invalid
    @raise ActionError: When executing the action fails for some reason
    '''
    por = por or self.task_por
    try:
      # try to set the focus
      ia = IAccessibleAction(por)
      rv = ia.doAccAction(index)
    except LookupError:
      # the provided por was invalid
      raise PORError
    except NotImplementedError:
      # just return if actions aren't supported
      return
    else:
      if not rv:
        # raise an error if the action is supported by was not executed
        raise ActionError

  def getAccFocus(self, search=False):
    '''
    Gets the L{POR} representing the focus of the foreground application. If
    no focus event has been received yet by this L{Tier} and search is True,
    performs a search for an accessible with state focus.
    
    @note: Setting search to True can result in a very long search operation.
    Be careful.
    @return: Point of regard to the focused accessible
    @rtype: L{POR}
    '''
    por = self.tier.getFocus()
    if por is None and search:
      # find the focus by looking at states
      pred = lambda por: IAccessibleInfo(por).hasAccState('focused')
      return self.findAccByPredicate(pred, self.getViewRootAcc(), False)
    return por
  
  def setAccFocus(self, por=None):
    '''
    Sets the system input focus to the accessible in the provided L{POR}. When 
    L{por} is None, the L{task_por} is used.
    
    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @return: Is setting the focus supported?
    @rtype: boolean
    @raise PORError: When the L{POR} is invalid
    @raise FocusError: When focusing on the L{POR} is supported but rejected by
      the system for some reason
    '''
    por = por or self.task_por
    try:
      # try to set the focus
      ia = IAccessibleAction(por)
      rv = ia.setAccFocus()
    except LookupError:
      # the provided por was invalid
      raise PORError
    except NotImplementedError:
      # return False if not supported
      return False
    else:
      if not rv:
        # raise an error if supported but failed
        raise FocusError
    return True

  def setAccSelected(self, por=None):
    '''    
    Selects the item or accessible indicated by the given L{POR}. When L{por}
    is None, the L{task_por} is used.
    
    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @return: Is setting the selection supported?
    @rtype: boolean
    @raise PORError: When the L{POR} is invalid
    @raise SelectError: When selecting the L{POR} is supported but rejected by
      the system for some reason
    '''
    por = por or self.task_por
    try:
      rv = IAccessibleAction(por).selectAcc(False)
    except LookupError:
      raise PORError
    except NotImplementedError:
      # return False if not supported
      return False
    else:
      if not rv:
        # raise an error if supported but failed
        raise SelectError
    return True
  
  def setAccDeselected(self, por=None):
    '''    
    Deselects the item or accessible indicated by the given L{POR}. When L{por}
    is None, the L{task_por} is used.
    
    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @return: Is setting the selection supported?
    @rtype: boolean
    @raise PORError: When the L{POR} is invalid
    @raise SelectError: When deselecting the L{POR} is supported but rejected
      by the system for some reason
    '''
    por = por or self.task_por
    try:
      rv = IAccessibleAction(por).deselectAcc(False)
    except LookupError:
      raise PORError
    except NotImplementedError:
      # return False if not supported
      return False
    else:
      if not rv:
        # raise an error if supported but failed
        raise SelectError
    return True
  
  def getAccCaret(self, por=None):
    '''
    Gets the text input caret L{POR} within the given accessible L{POR}.
    When L{por} is None, the L{task_por} is used.
    
    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @return: Point of regard to the text insertion caret
    @rtype: L{POR}
    @raise PORError: When the L{POR} is invalid
    '''
    por = por or self.task_por
    try:
      return IAccessibleInfo(por).getAccCaret()
    except LookupError:
      raise PORError
    except NotImplementedError:
      # return None if not supported
      return None

  def setAccCaret(self, por=None):
    '''
    Moves the text input caret to offset indicated by the given L{POR}. 
    When L{por} is None, the L{task_por} is used.
    
    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @return: Is setting the caret supported?
    @rtype: boolean
    @raise PORError: When the L{POR} is invalid
    @raise CaretError: When moving the caret is supported but rejected by the
      system for some reason
    '''
    por = por or self.task_por
    try:
      rv = IAccessibleAction(por).setAccCaret()
    except LookupError:
      raise PORError
    except NotImplementedError:
      # return False if not supported
      return False
    else:
      if not rv:
        # raise an error if supported but failed
        raise CaretError
    return True

  def setAccPOR(self, por=None):
    '''
    Sets the system input focus, selection, and caret offset to the provided 
    L{POR}. Ignores errors trying each as focus, selection, and caret offset
    are not all supported by every control. When L{por} is None, the 
    L{task_por} is used.

    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @return: False if none of focus, selection, or caret can be set; True if 
      at least one was set
    @rtype: boolean
    @raise PORError: When the L{POR} is invalid
    '''
    por = por or self.task_por
    count = 0
    try:
      self.setAccFocus(por)
    except (NotImplementedError, ActionError):
      count += 1
    try:
      self.setAccSelected(por)
    except (NotImplementedError, ActionError):
      count += 1
    try:
      self.setAccCaret(por)
    except (NotImplementedError, ActionError):
      count += 1
    return count != 3

  def getFirstPeerAcc(self, por=None):
    '''
    Gets the L{POR} of the first accessible peer of a given L{POR}. When L{por} 
    is None, the L{task_por} is used.

    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @return: Point of regard to the first peer accessible
    @rtype: L{POR}
    '''
    por = por or self.task_por
    try:
       p = IAccessibleNav(por).getParentAcc()
       return IAccessibleNav(p).getFirstAccChild()
    except (NotImplementedError, IndexError, LookupError):
      return None
    
  def getLastPeerAcc(self, por=None):
    '''
    Gets the L{POR} of the last accessible peer of a given L{POR}. When L{por} 
    is None, the L{task_por} is used.

    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @return: Point of regard to the first peer accessible
    @rtype: L{POR}
    '''
    por = por or self.task_por
    try:
       p = IAccessibleNav(por).getParentAcc()
       return IAccessibleNav(p).getLastAccChild()
    except (NotImplementedError, IndexError, LookupError):
      return None
  
  def getNextPeerAcc(self, por=None):
    '''
    Gets the L{POR} of the next accessible peer of a given L{POR}. When L{por} 
    is None, the L{task_por} is used.

    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @return: Point of regard to the next peer accessible
    @rtype: L{POR}
    '''
    por = por or self.task_por
    try:
       return IAccessibleNav(por).getNextAcc()
    except (IndexError, NotImplementedError, LookupError):
      return None
    
  def getPrevPeerAcc(self, por=None):
    '''    
    Gets the L{POR} of the previous accessible peer of a given L{POR}. When
    L{por} is None, the L{task_por} is used.

    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @return: Point of regard to the next peer accessible
    @rtype: L{POR}
    '''
    por = por or self.task_por
    try:
       return IAccessibleNav(por).getPrevAcc()
    except (IndexError, NotImplementedError, LookupError):
      return None
    
  def getNextAcc(self, por=None):
    '''
    Gets the L{POR} of the next accessible of a given L{POR} in the 
    L{AccessibleWalker} order. When L{por} is None, the L{task_por} is used.

    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @return: Point of regard to the next accessible
    @rtype: L{POR}
    '''
    por = por or self.task_por
    w = AccessibleWalker(por)
    try:
      return w.getNextPOR()
    except NotImplementedError:
      return None

  def getPrevAcc(self, por=None):
    '''
    Gets the L{POR} of the previous accessible of a given L{POR}. When L{por} 
    is None, the L{task_por} is used.

    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @return: Point of regard to the previous accessible or None if not found
    @rtype: L{POR}
    '''
    por = por or self.task_por
    w = AccessibleWalker(por)
    try:
      return w.getPrevPOR()
    except NotImplementedError:
      return None

  def getNextItem(self, por=None, wrap=False, only_visible=False):
    '''
    Gets the L{POR} of the next item in the given L{POR}. When L{por} is None, 
    the L{task_por} is used.

    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @param wrap: Consider a next item outside this accessible if there is no
      next item in this accessible? Defaults to False.
    @type wrap: boolean
    @param only_visible: Only consider visible items? Defaults to False.
    @type only_visible: boolean
    @return: Point of regard to the previous item or None if not found
    @rtype: L{POR}
    '''
    por = por or self.task_por
    w = AccessibleItemWalker(por, only_visible=only_visible)
    try:
      next_por = w.getNextPOR()
    except NotImplementedError:
      return None
    if wrap:
      # return whatever the walker returned
      return next_por
    elif next_por is not None and not next_por.isSameAcc(por):
      # there is no next POR when wrapping is off
      return None
    return next_por
  
  def getCurrItem(self, por=None):
    '''
    Gets the L{POR} of the start of the item. When L{por} is None, the 
    L{task_por} is used.

    @param por: A point of regard; use the current task's POR if none supplied
    @type por: L{POR}
    @return: Point of regard to the first character in the item or None if not 
      found
    @rtype: L{POR}
    '''
    por = por or self.task_por
    return POR(por.accessible, por.item_offset, 0)
  
  def getPrevItem(self, por=None, wrap=False, only_visible=False):
    '''
    Gets the L{POR} of the previous item in the given L{POR}. When L{por} is 
    None, the L{task_por} is used.

    @param por: A point of regard; use the current task's POR if none supplied
    @type por: L{POR}
    @param wrap: Consider a previous item outside this accessible if there is 
      no previous item in this accessible? Defaults to False.
    @type wrap: boolean
    @param only_visible: Only consider visible items? Defaults to False.
    @type only_visible: boolean
    @return: Point of regard to the previous item or None if not found
    @rtype: L{POR}
    '''
    por = por or self.task_por
    w = AccessibleItemWalker(por, only_visible=only_visible)
    try:
      prev_por = w.getPrevPOR()  
    except NotImplementedError:
      return None
    if wrap:
      # return whatever the walker returned
      return prev_por
    elif not prev_por.isSameAcc(por):
      # there is no previous POR when wrapping is off
      return None
    return prev_por

  def getNextWord(self, por=None):
    '''
    Get the L{POR} of the next word in the item indicated by the given L{POR}. 
    When L{por} is None, the L{task_por} is used.
    
    @param por: A point of regard; use the current task's POR if none supplied
    @type por: L{POR}
    @return: Point of regard to the next word or None if not found
    @rtype: L{POR}
    '''
    por = por or self.task_por
    # get the current item text
    try:
      text = IAccessibleInfo(por).getAccItemText()
    except (LookupError, NotImplementedError):
      return None
    # parse the text for the next word
    prev, curr, next = Word.getContextFromString(text, por)
    if next is not None:
      # if we found a next word, return its POR
      return next.getPOR()
    else:
      return None
    
  def getCurrWord(self, por=None):
    '''    
    Get the L{POR} of the current word in the item indicated by the given
    L{POR}. When L{por} is None, the L{task_por} is used.
    
    @param por: A point of regard; use the current task's POR if none supplied
    @type por: L{POR}
    @return: Point of regard to the start of the current word or None if not 
      found
    @rtype: L{POR}
    '''
    por = por or self.task_por
    # get the current item text
    try:
      text = IAccessibleInfo(por).getAccItemText()
    except (LookupError, NotImplementedError):
      return None
    # parse the text for the current word
    prev, curr, next = Word.getContextFromString(text, por)
    return curr.getPOR()

  def getPrevWord(self, por=None):
    '''
    Get the L{POR} of the previous word in the item indicated by the given 
    L{POR}. When L{por} is None, the L{task_por} is used.
    
    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @return: Point of regard to the previous word or None if not found
    @rtype: L{POR}
    '''
    por = por or self.task_por
    # get the current item text; cannot use self.getItemText because it traps
    # errors that we need to know here in order to return on an invalid POR
    try:
      text = IAccessibleInfo(por).getAccItemText()
    except (LookupError, NotImplementedError):
      return None
    # parse the text for the previous word
    prev, curr, next = Word.getContextFromString(text, por)
    if prev is not None:
      # if we found a prev word, return its POR
      return prev.getPOR()
    else:
      return None
    
  def getLastWord(self, por=None):
    '''    
    Get the L{POR} of the current word in the item indicated by the given
    L{POR}. When L{por} is None, the L{task_por} is used.
    
    @param por: A point of regard; use the current task's POR if none supplied
    @type por: L{POR}
    @return: Point of regard to the start of the last word or None if not found
    @rtype: L{POR}
    '''
    por = por or self.task_por
    # get the current item text
    try:
      text = IAccessibleInfo(por).getAccItemText()
    except (LookupError, NotImplementedError):
      return None
    # parse the text
    words = Word.buildWordsFromString(text, por)
    try:
      # get the POR of the last word
      return words[-1].getPOR()
    except IndexError:
      # no words, so just return the given POR
      return por

  def getNextChar(self, por=None):
    '''
    Get the L{POR} of the next character in the item indicated by the given 
    L{POR}. When L{por} is None, the L{task_por} is used.

    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @return: Point of regard to the next character or None if not found
    @rtype: L{POR}
    '''
    por = por or self.task_por
    try:
      # get the text of the current item
      text = IAccessibleInfo(por).getAccItemText()
    except (LookupError, NotImplementedError):
      return None
    if por.char_offset < len(text)-1:
      return POR(por.accessible, por.item_offset, por.char_offset+1)
    else:
      return None
  
  def getPrevChar(self, por=None):
    '''
    Get the L{POR} of the previous character in the item indicated by the given 
    L{POR}. When L{por} is None, the L{task_por} is used.
    
    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @return: Point of regard to the previous character or None if not found
    @rtype: L{POR}
    '''
    por = por or self.task_por
    if por.char_offset > 0:
      return POR(por.accessible, por.item_offset, por.char_offset-1)
    else:
      return None
    
  def getLastChar(self, por=None):
    '''
    Get the L{POR} of the last character in the item indicated by the given
    L{POR}. When L{por} is None, the L{task_por} is used.

    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @return: Point of regard to the last character or None if not found
    @rtype: L{POR}
    '''
    por = por or self.task_por
    try:
      # get the text of the current item
      text = IAccessibleInfo(por).getAccItemText()
    except (LookupError, NotImplementedError):
      return None
    if len(text) == 0:
      return POR(por.accessible, por.item_offset, 0)
    else:
      return POR(por.accessible, por.item_offset, len(text)-1)
  
  def getNextRow(self, por=None):
    '''    
    Get the L{POR} of the first column of the next row relative to the row
    containing the item indicated by the given L{POR}. When L{por} is None, the
    L{task_por} is used.
    
    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @return: Point of regard to the start of the next row or None if not found
    @rtype: L{POR}
    '''
    por = por or self.task_por
    try:
      ai = IAccessibleInfo(por)
      # get the current row and add one, first column
      index = ai.getAccRowColIndex(ai.getAccRow()+1, 0)
      return POR(por.accessible, index)
    except (LookupError, NotImplementedError):
      return None
    
  def getCurrRow(self, por=None):
    '''
    Gets the L{POR} to the cell in the first column of the row containing the 
    item indicated by the given L{POR}. When L{por} is None, the L{task_por} is 
    used.
    
    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @return: Point of regard to the first column of the given row
    @rtype: L{POR}
    '''
    por = por or self.task_por
    try:
      ai = IAccessibleInfo(por)
      # this row, first column
      index = ai.getAccRowColIndex(ai.getAccRow(), 0)
      return POR(por.accessible, index)
    except (LookupError, NotImplementedError):
      return None
  
  def getPrevRow(self, por=None):
    '''
    Get the L{POR} of the first column of the previous row relative to the row
    containing the item indicated by the given L{POR}. When L{por} is None, the
    L{task_por} is used.
    
    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @return: Point of regard to the start of the previous row or None if not 
      found
    @rtype: L{POR}
    '''
    por = por or self.task_por
    try:
      ai = IAccessibleInfo(por)
      # get the current row and add one, first column
      index = ai.getAccRowColIndex(ai.getAccRow()+1, 0)
      return POR(por.accessible, index)
    except (LookupError, NotImplementedError):
      return None
    
  def iterNextAccs(self, por=None):
    '''    
    Builds an iterator over the accessibles next to the current L{POR}. When
    L{por} is None, the L{task_por} is used.
    
    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @return: Point of regard to the previous accessible
    @rtype: L{POR}    
    '''
    por = por or self.getNextAcc(por)
    while por is not None:
      yield por
      por = self.getNextAcc(por)
      
  def iterPrevAccs(self, por=None):
    '''    
    Builds an iterator over the accessibles previous to the current L{POR}.
    When L{por} is None, the L{task_por} is used.
    
    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @return: Point of regard to the previous accessible
    @rtype: L{POR}
    '''
    por = por or self.getPrevAcc(por)
    while por is not None:
      yield por
      por = self.getPrevAcc(por)

  def iterNextItems(self, por=None, wrap=False, only_visible=False):
    '''    
    Builds an iterator over the items next to the current L{POR}. When L{por}
    is None, the L{task_por} is used.

    @param por: A point of regard; use the current task's POR if none supplied
    @type por: L{POR}
    @param wrap: Consider a next item outside this accessible if there is
      no next item in this accessible? Defaults to False.
    @type wrap: boolean
    @param only_visible: Only consider visible items? Defaults to False.
    @type only_visible: boolean
    @return: Point of regard to the next item
    @rtype: L{POR}
    '''
    por = por or self.getNextItem(por, wrap, only_visible)
    while por is not None:
      yield por
      por = self.getNextItem(por, wrap, only_visible)

  def iterPrevItems(self, por=None, wrap=False, only_visible=False):
    '''
    Builds an iterator over the items previous to the current L{POR}. When 
    L{por} is None, the L{task_por} is used.

    @param por: A point of regard; use the current task's POR if none supplied
    @type por: L{POR}
    @param wrap: Consider a previous item outside this accessible if there is
      no previous item in this accessible? Defaults to False.
    @type wrap: boolean
    @param only_visible: Only consider visible items? Defaults to False.
    @type only_visible: boolean
    @return: Point of regard to the previous item
    @rtype: L{POR}
    '''
    por = por or self.getPrevItem(por, wrap, only_visible)
    while por is not None:
      yield por
      por = self.getPrevItem(por, wrap, only_visible)

  def getChildAcc(self, index, por=None):
    '''
    Gets the child accessible at the given index from the L{POR}. When no POR 
    is provided, the L{task_por} is used.
    
    @param index: Child index to retrieve
    @type index: integer
    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @return: Point of regard to the child at the index or None if it does not
      exist
    @rtype: L{POR}
    '''
    por = por or self.task_por
    try:
      return IAccessibleNav(por).getChildAcc(index)
    except (IndexError, LookupError):
      return None

  def getParentAcc(self, por=None):
    '''
    Gets the L{POR} of the first character and first item of the parent of the 
    provided L{POR}. When no POR is provided, the L{task_por} is used.

    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @return: Point of regard for the parent
    @rtype: L{POR}
    '''
    por = por or self.task_por
    try:
      return IAccessibleNav(por).getParentAcc()
    except (NotImplementedError, LookupError):
      return None

  def iterAncestorAccs(self, por=None, only_visible=False,allow_trivial=False):
    '''    
    Builds an iterator over the ancestors of the current L{POR}. When L{por} is
    None, the L{task_por} is used.

    @param por: A point of regard; use the current task's POR if none supplied
    @type por: L{POR}
    @param only_visible: Yield on visible L{POR}s only?
    @type only_visible: boolean
    @param allow_trivial: Yield on trivial L{POR}s too?
    @type allow_trivial: boolean
    @return: Point of regard to an ancestor
    @rtype: L{POR}
    '''
    por = por or self.task_por
    w = AccessibleItemWalker(por, only_visible, allow_trivial)
    try:
      por = w.getParentPOR()
    except NotImplementedError:
      return
    while por is not None:
      yield por
      try:
        por = w.getParentPOR()
      except NotImplementedError:
        return
  
  def getViewRootAcc(self):
    '''
    Gets a L{POR} indicating the root of the active view. This is more 
    efficient than L{getRootAcc} when the root of the I{active} view is needed.

    @return: A point of regard to the root accessible
    @rtype: L{POR}
    '''
    return self.view_man.getAEView()
  
  def getRootAcc(self, por=None):
    '''
    Gets a L{POR} to the top level container of the given L{POR}. When L{por} 
    is None, the L{task_por} is used.
    
    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @return: A point of regard to the root accessible
    @rtype: L{POR}
    @raise PORError: When the provided L{POR} is invalid
    '''
    por = por or self.task_por
    w = AccessibleWalker(por)
    try:
      return w.getFirstPOR()
    except NotImplementedError:
      return None
    
  def getEndAcc(self, por=None):
    '''    
    Gets a L{POR} to the last item of the last accessible under the root of the
    view. When L{por} is None, the L{task_por} is used.
    
    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @return: A point of regard to the last accessible under the root POR
    @rtype: L{POR}
    @raise PORError: When the L{POR} is invalid
    '''
    por = por or self.task_por
    w = AccessibleItemWalker(por)
    try:
      por = w.getLastPOR()
    except NotImplementedError:
      # the por we tried to walk was bad
      raise PORError
    if por is None:
      # no last item
      raise PORError
    else:
      return por
    
  def getLastAccUnder(self, por=None):
    '''
    Gets a L{POR} to the last item of the last accessible under the given root.
    When L{por} is None, the L{task_por} is used.

    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @return: A point of regard to the last accessible under the given POR
    @rtype: L{POR}
    @raise PORError: When the L{POR} is invalid
    '''
    por = por or self.task_por
    while 1:
      try:
        por = IAccessibleNav(por).getLastAccChild()
      except NotImplementedError:
        # we can't walk this POR
        raise PORError
      except LookupError:
        old = por
        try:
          # get the last visible or invisible item
          por = IItemNav(por).getLastItem(False)
        except LookupError:
          pass
        if por == old:
          # stop when we haven't moved, else continue
          return por

  def getAccFromPath(self, por=None, *path):
    '''
    Gets a L{POR} representing the accessible found by treating the path 
    integers as descendant indices stating at the given L{POR}. When no POR is 
    provided, the L{task_por} is used as the starting point.
    
    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @param path: Integers indicate the path to the desired object
    @type path: integer
    @return: Point of regard for the descendant at the specified path
    @rtype: L{POR}
    '''
    por = por or self.task_por
    try:
      # try to get the child indicated by the path
      child_por = IAccessibleNav(por).getChildAcc(path[0])
    except (IndexError, LookupError):
      return None
    # check if we need to recurse anymore
    path = path[1:]
    if not len(path):
      return child_por
    # recurse with the remainder of the path
    por_from_path = self.getAccFromPath(child_por, *path) 
    return por_from_path
  
  def compareAncestorRoles(self, por=None, *roles):
    '''
    Iterates through the given roles comparing them with the roles of the first
    len(roles) ancestors of the given L{POR} starting with the immediate 
    parent. When no POR is provided, the L{task_por} is used as the starting 
    point.
    
    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @param roles: Role names to use as a comparison
    @type roles: string
    @return: Did the given roles match the roles of the corresponding 
      ancestors?
    @rtype: boolean
    '''
    por = por or self.task_por
    # iterate all ancestors
    iter = self.iterAncestorAccs(allow_trivial=True)
    for name in roles:
      try:
        anc_name = self.getAccRoleName(iter.next())
      except StopIteration:
        # not a match if we run out of ancestors
        return False
      if anc_name != name:
        return False
    return True
  
  def findAccByName(self, name, por=None, ancestor=False, depth_first=True):
    '''
    Gets a L{POR} to the first accessible descendant or ancestor of the given
    L{POR} with the given name.

    @param name: Name of the accessible to locate
    @type name: string
    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @param ancestor: Search through ancestors (True) or descendants (False)?
    @type ancestor: boolean
    @param depth_first: Is the search depth-first (True) or breadth-first 
      (False)? Only meaningful when ancestor is False.
    @type depth_first: boolean
    @return: Point of regard to the target if found or None if not
    @rtype: L{POR}
    '''
    pred = lambda por: IAccessibleInfo(por).getAccName() == name
    return self.findAccByPredicate(pred, por, ancestor)

  def findAccByRole(self, role, por=None, ancestor=False, depth_first=True):
    '''
    Gets a L{POR} to the first accessible descendant or ancestor of the given
    L{POR} with the given role.

    @param role: Role of the accessible to locate
    @type role: string
    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @param ancestor: Search through ancestors (True) or descendants (False)?
    @type ancestor: boolean
    @param depth_first: Is the search depth-first (True) or breadth-first 
      (False)? Only meaningful when ancestor is False.
    @type depth_first: boolean
    @return: Point of regard to the target if found or None if not
    @rtype: L{POR}
    '''
    pred = lambda por: IAccessibleInfo(por).hasAccRole(role)
    return self.findAccByPredicate(pred, por, ancestor)
  
  def findAccByObjId(self, objname, por=None, ancestor=False, depth_first=True):
    '''
    Gets a L{POR} to the first accessible descendant or ancestor of the given
    L{POR} with the given 'id' attribute named objname.

    @param objname: Object id name to search
    @type objname: string
    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @param ancestor: Search through ancestors (True) or descendants (False)?
    @type ancestor: boolean
    @param depth_first: Is the search depth-first (True) or breadth-first 
      (False)? Only meaningful when ancestor is False.
    @type depth_first: boolean
    @return: Point of regard to the target if found or None if not
    @rtype: L{POR}
    '''
    pred = lambda por: self.getAccAttrs(por)['id'] == objname
    return self.findAccByPredicate(pred, por, ancestor)
  
  def findAccByPredicate(self, predicate, por=None, ancestor=False, 
                         depth_first=True):
    '''
    Gets a L{POR} to the first accessible descendant or ancestor of the given
    L{POR} matching the given callable predicate.

    @warning: This method should be considered unstable and likely to change in
    future versions of LSR

    @param predicate: Search predicate that evaluates to True or False
    @type predicate: callable
    @param por: A point of regard; use the current task's POR if none supplied
    @type por: L{POR}
    @param ancestor: Search through ancestors (True) or descendants (False)?
    @type ancestor: boolean
    @param depth_first: Is the search depth-first (True) or breadth-first 
      (False)? Only meaningful when ancestor is False.
    @type depth_first: boolean
    @return: Point of regard to the target if found or None if not
    @rtype: L{POR}
    '''
    por = por or self.task_por
    try:
      ai = IAccessibleNav(por)
      if ancestor:
        return ai.findAncestorAcc(predicate)
      else:
        return ai.findDescendantAcc(predicate, depth_first)
    except NotImplementedError:
      return None
    except LookupError:
      raise PORError
    
  def moveToPointer(self):
    '''
    Moves the L{task_por} to the pointer L{POR}.
    '''
    self.task_por = self.tier.getPointer()

  def moveToItemStart(self):
    '''
    Moves the L{task_por} to the first character in the current item of the
    L{POR}.
    '''
    self.task_por.char_offset = 0

  def moveToPOR(self, por):
    '''
    Moves the L{task_por} to the given L{POR}. Just a convenience method. A 
    L{Perk} writer can set self.task_por = x if that is preferred. One 
    advantage of using this method is that it will check if por is None and
    raise an error if so.
    
    @raise PORError: When the L{POR} is invalid
    '''
    if por is None:
      raise PORError
    self.task_por = por

  def moveToRoot(self):
    '''
    Modifies the L{task_por} to the root of the view.
    '''
    self.task_por = self.getRootAcc()

  def moveToEnd(self):
    '''
    Modifies the L{task_por} to the beginning of the last item in the current
    view.

    @raise PORError: When the L{POR} is invalid
    '''
    self.task_por = self.getEndAcc()
      
  def getAccTextAttr(self, name, por=None):
    '''
    Gets the value of the given attribute of an accessible of a given 
    L{POR}. When L{por} is None, the L{task_por} is used.
    
    Valid attribute names/value pairs include: (subject to change, GNOME ATK
    specification):
    
    left-margin: The pixel width of the left margin
    right-margin: The pixel width of the right margin
    indent: The number of pixels that the text is indented
    invisible: Either "true" or "false" indicates whether text is visible or not
    editable: Either "true" or "false" indicates whether text is editable or not
    pixels-above-lines:	Pixels of blank space to leave above each 
    newline-terminated line.
    pixels-below-lines:	Pixels of blank space to leave below each 
    newline-terminated line.
    pixels-inside-wrap:	Pixels of blank space to leave between wrapped lines 
    inside the same newline-terminated line (paragraph).
    bg-full-height: "true" or "false" whether to make the background color for 
    each character the height of the highest font used on the current line, or 
    the height of the font used for the current character.
    rise: Number of pixels that the characters are risen above the baseline
    underline: "none", "single", "double" or "low"
    strikethrough: "true" or "false" whether the text is strikethrough
    size: The size of the characters.
    scale: The scale of the characters: a string representation of a double.
    weight: The weight of the characters.
    language: The language used
    family-name: The font family name
    bg-color: The background color: RGB value of the format "u,u,u"
    fg-color: The foreground color: RGB value of the format "u,u,u"
    bg-stipple: "true" if a GdkBitmap is set for stippling the background color.
    fg-stipple: "true" if a GdkBitmap is set for stippling the foreground color.
    wrap-mode: The wrap mode of the text, if any: "none", "char" or "word"
    direction: The direction of the text, if set: "none", "ltr" or "rtl"
    justification: The justification of the text, if set: "left", "right", 
    "center" or "fill"
    stretch: The stretch of the text, if set: "ultra_condensed", 
    "extra_condensed", "condensed", "semi_condensed", "normal", "semi_expanded",
    "expanded", "extra_expanded" or "ultra_expanded"
    variant: The capitalization of the text, if set: "normal" or "small_caps"
    style: The slant style of the text, if set: "normal", "oblique" or "italic"
    last-defined: not a valid text attribute, for finding end of enumeration
    
    @param name: text attribute name, eg. "fg-color", "justification"
    @type name: string
    @param por: A point of regard; uses the current task's POR if none supplied
    @type por: L{POR}
    @return: value of attribute
    @rtype: string
    @raise PORError: When the L{POR} is invalid
    '''
    por = por or self.task_por
    try:
      # get the info interface
      ai = IAccessibleInfo(por)
      value = ai.getAccTextAttr(name)
      return value
    except LookupError:
      # raise POR error if some accessible was dead, bad or missing
      raise PORError
    except NotImplementedError:
      return None

  def getAccAllTextAttrs(self, por=None):
    '''        
    Like L{getAccTextAttr}, but gets the values of all of the current
    attributes of an accessible of a given L{POR}. When L{por} is None, the
    L{task_por} is used.

    @param por: A point of regard; use the current task's POR if none supplied
    @type por: L{POR}
    @return: Name/value pairs for all available attributes
    @rtype: dictionary
    @raise PORError: When the L{POR} is invalid
    @see: L{getAccTextAttr}
    '''
    por = por or self.task_por
    try:
      # get the info interface
      ai = IAccessibleInfo(por)
      return ai.getAccAllTextAttrs()
    except LookupError:
      # raise POR error if some accessible was dead, bad or missing
      raise PORError
    except NotImplementedError:
      return None

  def getStartOfHardLine(self, por=None):
    '''
    From a given L{POR}, get the L{POR} of the start of the first line that
    follows the previous hard line break, or the start of the first line if it
    is the first line in the accessible. When L{por} is None, the L{task_por} is
    used.
    
    @param por: Point of regard; uses the current task's L{POR} if none supplied
    @type por: L{POR}
    @return: Point of regard to the previous character or None if not found
    @rtype: L{POR}
    @raise PORError: When some L{POR} is invalid
    '''
    por = por or self.task_por
    try:
      prev = self.getPrevItem(por)
      while prev is not None and prev.item_offset is not None:
        prevText = self.getItemText(prev)
        if prevText.endswith('\n'): # stop at a line break
          break
        por = prev
        prev = self.getPrevItem(por)
      return POR(por.accessible, por.item_offset, 0)
    except (LookupError, PORError):
      # whatever we started with was not a valid POR
      raise PORError

  def getEndOfHardLine(self, por=None):
    '''
    From a given L{POR}, get the L{POR} of the end of the first following line
    that ends with a hard line break, or the end of the last line if it is the
    last line in the accessible. When L{por} is None, the L{task_por} is used.
    
    @param por: Point of regard; use the current task's L{POR} if none supplied
    @type por: L{POR}
    @return: Point of regard to the previous character or None if not found
    @rtype: L{POR}
    @raise PORError: When some L{POR} is invalid
    '''
    por = por or self.task_por
    try:    
      next = self.getNextItem(por)
      text = self.getItemText(por)
      while next is not None:
        if text.endswith('\n'): # stop at a line break
          break
        text = self.getItemText(next)
        por = next
        next = self.getNextItem(por)
      if next is None:
        # bottom line in chat window doesn't terminate in '\n'
        return POR(por.accessible, por.item_offset, len(text))
      else:
        return POR(por.accessible, por.item_offset, len(text)-1)
    except (LookupError, PORError):
      # whatever we started with was not a valid POR
      raise PORError

  def getAccTextBetween(self, start_por=None, end_por=None, only_visible=False):
    '''
    Gets all text between two point of regards. If L{start_por} is not 
    specified, defaults to using the current L{task_por}. If L{end_por} is not
    given, defaults to getting all text to the end of the current accessible.
    
    
    @param start_por: A point of regard indicating the start of the text range; 
      uses the current task's POR if none supplied
    @type start_por: L{POR}
    @param end_por: A point of regard indicating the end of the text range; 
      use the POR to the end of the accessible if none supplied
    @type end_por: L{POR}
    @param only_visible: Only consider visible items? Defaults to False.
    @type only_visible: boolean
    @return: Text between the two L{POR}s
    @rtype: string
    @raise PORError: When the provided L{POR} is invalid
    '''
    start_por = start_por or self.task_por
    w = AccessibleItemWalker(start_por, only_visible=only_visible)
    
    text = []
    curr = start_por
    prev = start_por
    # walk until we pass the end_por or hit the end of the current accessible
    while 1:
      if curr is None or (end_por is None and not curr.isSameAcc(start_por)):
        # stop if the end is not specified and we've gotten to the last item of
        # this accessible
        break
      elif end_por.isSameItem(curr):
        # slice up to the requested char offset in this item
        text.append(self.getItemText(curr)[:end_por.char_offset])
        break
      elif curr.isItemAfter(end_por):
        # slice up to the requested char offset in the previous item
        text.append(self.getItemText(prev)[:end_por.char_offset])
        break
      else:
        # otherwise, store all the item text
        text.append(self.getItemText(curr))
      prev = curr
      try:
        curr = w.getNextPOR()
      except NotImplementedError:
        # no POR to walk, so stop early
        break
    return ''.join(text)
  
  def setAccText(self, text, por=None):
    '''
    Sets the given string as the text at the given L{POR}. All existing text
    is replaced. When L{por} is None, the L{task_por} is used.

    @param text: Text to set
    @type text: string
    @param por: Point of regard; use the current task's L{POR} if none supplied
    @type por: L{POR}
    @return: Is setting the text supported?
    @rtype: boolean
    @raise PORError: When the L{POR} is invalid
    @raise TextError: When the text operation fails
    '''
    por = por or self.task_por
    try:
      rv = IAccessibleAction(por).setAccText(text)
    except LookupError:
      raise PORError
    except NotImplementedError:
      # return False if not supported
      return False
    else:
      if not rv:
        # raise an error if supported but failed
        raise TextError
    return True
  
  def insertAccText(self, text, por=None, attrs=None):
    '''
    Inserts the given text at the given L{POR}. When L{por} is None, the 
    L{task_por} is used. If attributes are specified, they will be applied to
    the inserted text.

    @param text: Text to set
    @type text: string
    @param por: Point of regard; use the current task's L{POR} if none supplied
    @type por: L{POR}
    @param attrs: Attributes to set over the text range
    @type attrs: dictionary
    @return: Is inserting text supported?
    @rtype: boolean
    @raise PORError: When the L{POR} is invalid
    @raise TextError: When the text operation fails
    '''
    por = por or self.task_por
    try:
      rv = IAccessibleAction(por).setAccText(text, attrs)
    except LookupError:
      raise PORError
    except NotImplementedError:
      # return False if not supported
      return False
    else:
      if not rv:
        # raise an error if supported but failed
        raise TextError
    return True
  
  def setAccTextAttrs(self, start=None, end=None, **attrs):
    '''    
    Sets the attributes of the text from the given starting L{POR} to the 
    ending L{POR}. When L{start} or L{end} is None, the L{task_por} is used for
    either one or both missing value(s).

    @param attrs: Attributes to set over the text range
    @type attrs: dictionary
    @param start: Point of regard to the start of the text run; use the current
      task's L{POR} if none supplied
    @type start: L{POR}
    @param end: Point of regard to the end of the text run; use the current
      task's L{POR} if none supplied
    @type end: L{POR}
    @return: Is setting attributes supported?
    @rtype: boolean
    @raise PORError: When the L{POR} is invalid
    @raise TextError: When the text operation fails
    @see: L{getAccTextAttr}
    '''
    start = start or self.task_por
    end = end or self.task_por
    try:
      rv = IAccessibleAction(start).setAccTextAttrs(end, attrs)
    except LookupError:
      raise PORError
    except NotImplementedError:
      # return False if not supported
      return False
    else:
      if not rv:
        # raise an error if supported but failed
        raise TextError
    return True
  
  def copyAccText(self, start=None, end=None):
    '''        
    Copies the text from the given starting L{POR} to the ending L{POR}. When
    L{start} or L{end} is None, the L{task_por} is used for either one or both
    missing value(s).

    @param start: Point of regard to the start of the text run; use the current
      task's L{POR} if none supplied
    @type start: L{POR}
    @param end: Point of regard to the end of the text run; use the current
      task's L{POR} if none supplied
    @type end: L{POR}
    @return: Is copy supported?
    @rtype: boolean
    @raise PORError: When the L{POR} is invalid
    @raise TextError: When the text operation fails
    '''
    start = start or self.task_por
    end = end or self.task_por
    try:
      rv = IAccessibleAction(start).copyAccText(end)
    except LookupError:
      raise PORError
    except NotImplementedError:
      # return False if not supported
      return False
    else:
      if not rv:
        # raise an error if supported but failed
        raise TextError
    return True
  
  def cutAccText(self, start=None, end=None):
    '''        
    Cuts the text from the given starting L{POR} to the ending L{POR}. When
    L{start} or L{end} is None, the L{task_por} is used for either one or both
    missing value(s).

    @param start: Point of regard to the start of the text run; use the current
      task's L{POR} if none supplied
    @type start: L{POR}
    @param end: Point of regard to the end of the text run; use the current
      task's L{POR} if none supplied
    @type end: L{POR}
    @return: Is cut supported?
    @rtype: boolean
    @raise PORError: When the L{POR} is invalid
    @raise TextError: When the text operation fails
    '''
    start = start or self.task_por
    end = end or self.task_por
    try:
      rv = IAccessibleAction(start).cutAccText(end)
    except LookupError:
      raise PORError
    except NotImplementedError:
      # return False if not supported
      return False
    else:
      if not rv:
        # raise an error if supported but failed
        raise TextError
    return True

  def deleteAccText(self, start=None, end=None):
    '''        
    Deletes the text from the given starting L{POR} to the ending L{POR}. When
    L{start} or L{end} is None, the L{task_por} is used for either one or both
    missing value(s).

    @param start: Point of regard to the start of the text run; use the current
      task's L{POR} if none supplied
    @type start: L{POR}
    @param end: Point of regard to the end of the text run; use the current
      task's L{POR} if none supplied
    @type end: L{POR}
    @return: Is delete supported?
    @rtype: boolean
    @raise PORError: When the L{POR} is invalid
    @raise TextError: When the text operation fails
    '''
    start = start or self.task_por
    end = end or self.task_por
    try:
      rv = IAccessibleAction(start).cutAccText(end)
    except LookupError:
      raise PORError
    except NotImplementedError:
      # return False if not supported
      return False
    else:
      if not rv:
        # raise an error if supported but failed
        raise TextError
    return True

  def pasteAccText(self, por=None):
    '''        
    Pastes the clipboard text at the given L{POR}. When L{por} is None, the 
    L{task_por} is used.

    @param por: Point of regard to the insertion point; use the current task's 
      L{POR} if none supplied
    @type por: L{POR}
    @return: Is paste supported?
    @rtype: boolean
    @raise PORError: When the L{POR} is invalid
    @raise TextError: When the text operation fails
    '''
    por = por or self.task_por
    try:
      rv = IAccessibleAction(por).pasteAccText()
    except LookupError:
      raise PORError
    except NotImplementedError:
      # return False if not supported
      return False
    else:
      if not rv:
        # raise an error if supported but failed
        raise TextError
    return True
    
  def getPointerPOR(self):
    '''
    Gets the pointer L{POR} for this L{Tier}.
    
    @return: Point of regard stored programmatically by this L{Tier}
    @rtype: L{POR}
    '''
    return self.tier.getPointer()
  
  def setPointerPOR(self, por=None):
    '''
    Sets the pointer L{POR} for this L{Tier}. When L{por} is None, the 
    L{task_por} is used.
    
    @param por: Point of regard to set as the pointer
    @type por: L{POR}
    '''
    por = por or self.task_por
    self.tier.setPointer(por)

  def getAccTextLength(self, por=None):
    '''
    Gets the length of the text in the L{POR}. When L{por} is None, the 
    L{task_por} is used.
    
    @param por: Point of regard to an object containing text
    @type por: L{POR}
    @return: Number of characters in the text or None when character count not
      supported
    @rtype: integer
    @raise PORError: When the L{POR} is invalid
    '''
    por = por or self.task_por
    try:
      return IAccessibleInfo(por).getAccTextCharCount()
    except LookupError:
      raise PORError
    except NotImplementedError:
      return None
  
  def getAccTextSelectionCount(self, por=None):
    '''
    Gets the number of discontiguous text selections in the L{POR}. When L{por}
    is None, the L{task_por} is used.
    
    @param por: Point of regard to an object containing text
    @type por: L{POR}
    @return: Number of selections or None when selections are not supported
    @rtype: integer
    @raise PORError: When the L{POR} is invalid
    '''
    por = por or self.task_por
    try:
      return IAccessibleInfo(por).getAccTextSelectionCount()
    except LookupError:
      raise PORError
    except NotImplementedError:
      return None
  
  def selectAccText(self, n=None, start=None, end=None):
    '''    
    Selects the text from the given starting L{POR} to the ending L{POR}. If n
    is None, a new selection range is added, else the nth selection is modified
    to the given range. When L{start} or L{end} is None, the L{task_por} is
    used for either one or both missing value(s).

    @param start: Point of regard to the start of the text run; use the current
      task's L{POR} if none supplied
    @type start: L{POR}
    @param end: Point of regard to the end of the text run; use the current
      task's L{POR} if none supplied
    @type end: L{POR}
    @return: Is text selection supported?
    @rtype: boolean
    @raise PORError: When the L{POR} is invalid
    @raise SelectError: When the selection operation fails
    '''
    start = start or self.task_por
    end = end or self.task_por
    try:
      rv = IAccessibleAction(start).selectAccText(end, n)
    except LookupError:
      raise PORError
    except NotImplementedError:
      # return False if not supported
      return False
    else:
      if not rv:
        # raise an error if supported but failed
        raise SelectError
    return True

  def deselectAccText(self, n=None, por=None):
    ''' 
    Deselects the text from the given starting L{POR} to the ending L{POR}. If
    n is None, all selected ranges are removed, else the nth selection is
    removed. When L{por} is None, the L{task_por} is used.

    @param por: Point of regard to the text area; use the current task's 
      L{POR} if none supplied
    @type por: L{POR}
    @return: Is text deselection supported?
    @rtype: boolean
    @raise PORError: When the L{POR} is invalid
    @raise SelectError: When the deselection operation fails
    '''
    por = por or self.task_por
    try:
      rv = IAccessibleAction(por).deselectAccText(n)
    except LookupError:
      raise PORError
    except NotImplementedError:
      # return False if not supported
      return False
    else:
      if not rv:
        # raise an error if supported but failed
        raise SelectError
    return True

  def getLastKey(self):
    '''    
    Gets the key code, key sym, and modifiers of the last key pressed when this
    L{Tier} was active. This information should B{not} be used to trigger
    events, but rather as additional information for correcting accessibility
    problems in certain applications where the standard accessibility events do
    not provide enough information.
    
    @note: The values returned by this event are platform specific. This may
      change in the future. Are you sure you don't want to use a 
      L{Task.InputTask}?
    @return: Key code, key sym, and key modifier bit mask
    @rtype: 3-tuple of integer, string, integer
    '''
    return self.tier.getLastKey()
  
  def mouseEventPOR(self, event, por=None):
    '''
    Triggers a mouse event at the center of the given point of regard.
    
    @param event: Mouse event, one of EVENT_SYNTHMOUSE_* in L{AEConstants}
    @type event: string
    @return: Is the action supported?
    @rtype: boolean
    @raise PORError: When the L{POR} is invalid
    @raise MouseError: When the mouse operation fails
    '''
    por = por or self.task_por 
    try:
      rv = IAccessibleAction(por).doMouseAction(event)
    except LookupError:
      raise PORError
    except NotImplementedError:
      # return False if not supported
      return False
    else:
      if not rv:
        # raise an error if supported but failed
        raise MouseError
    return True
  
  def leftClickPOR(self, por=None):
    ''' 
    Performs a mouse left click on the center of a given point of regard.
    
    @param por: Point of regard; use the current task's L{POR} if none supplied      
    @type por: L{POR}
    @return: Did the click succeed?
    @rtype: boolean
    @raise PORError: When the L{POR} is invalid
    @raise MouseError: When the mouse click fails
    '''
    # invoke our more general mouse event method
    return self.mouseEventPOR(AEConstants.EVENT_SYNTHMOUSE_B1C)
    
  def inhibitAutoPointer(self):
    '''
    Stops the L{task_por} from being stored as the L{Tier} pointer L{POR} for
    this event only. Useful when a focus layer event carries a L{POR} that 
    would become the pointer, but shouldn't for some reason.
    '''
    self.tools_auto_pointer = False
    
  def getAccSetSize(self, por=None):
    '''
    Get the number of members within an accessible group.  Often used to 
    determine the size of a radio or menu group.
    
    @param por: Point of regard; use the current task's 
      L{POR} if none supplied
    @type por: L{POR}
    @return: Set/group size.
    @rtype: integer
    '''
    por = por or self.task_por
    attrs = self.getAccAttrs(por=por)
    try:
      return int(attrs['setsize'])
    except (KeyError, TypeError):
      relations = self.getAccRelations('member of', por=por)
      if relations is not None and len(relations) > 0:
        return len(relations)
      else:
        return None
      
  def getAccPosInSet(self, por=None):
    '''
    Get the position of an accessible within a group.
    
    @param por: Point of regard; use the current task's 
      L{POR} if none supplied
    @type por: L{POR}
    @return: Position within a group.
    @rtype: integer
    '''
    por = por or self.task_por
    attrs = self.getAccAttrs(por=por)
    try:
      return int(attrs['posinset'])
    except (KeyError, TypeError):
      return None
  
  def getAccURI(self, index, por=None):
    ''' 
    Obtain a resource locator ('URI') string which can be used to access the 
    content to which this link "points" or is connected.

    @param index: anchor index
    @type index: integer
    @param por: Point of regard; use the current task's L{POR} if none supplied
    @type por: L{POR}
    @return: URI string
    @rtype: string
    @raise PORError: When the L{POR} is invalid
    '''
    por = por or self.task_por 
    try:
      rv = IAccessibleInfo(por).getAccURI(index)
      return rv
    except LookupError:
      raise PORError
    except NotImplementedError:
      return None
    else:
      return None
    
  def getAccAnchorCount(self, por=None):
    ''' 
    Obtain he number of separate anchors associated with this accessible.
    
    @param por: Point of regard; use the current task's L{POR} if none supplied
    @type por: L{POR}
    @return: Number of anchors
    @rtype: integer
    @raise PORError: When the L{POR} is invalid
    '''
    por = por or self.task_por 
    try:
      rv = IAccessibleInfo(por).getAccAnchorCount()
      return rv
    except LookupError:
      raise PORError
    except NotImplementedError:
      return None
    else:
      return None
