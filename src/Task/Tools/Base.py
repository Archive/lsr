'''
Defines the most base L{Tools} class from which all other L{Task.Tools} should
derive.

@author: Peter Parente
@author: Pete Brunet
@author: Larry Weiss
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
import weakref
import AEConstants, AEInterfaces
from i18n import _

class TaskTools(object):
  '''
  Provides initialize and finalize methods for preparing and unpreparing the
  data required by L{Tools} subclasses while a L{Task} or L{Perk} is executing.
  Temporarily stores information such as the event layer, event L{POR}, and
  timestamp.
  
  @ivar acc_eng: Reference to L{AccessEngine} for task context
  @type acc_eng: L{AccessEngine}
  @ivar event_man: Reference to the L{EventManager}
  @type event_man: L{EventManager}
  @ivar view_man: Reference to the L{ViewManager}
  @type view_man: L{ViewManager}
  @ivar tier_man: Reference to the L{TierManager}
  @type tier_man: L{TierManager}
  @ivar device_man: Reference to the L{DeviceManager}
  @type device_man: L{DeviceManager}
  @ivar sett_man: Reference to the L{SettingsManager}
  @type sett_man: L{SettingsManager}
  @ivar tier: The L{Tier} owning the L{Task} or L{Perk} subclass
  @type tier: L{Tier}
  @ivar perk: The L{Perk} owning the L{Task} subclass
  @type perk: L{Perk}
  @ivar layer: Layer on which the event occurred
  @type layer: integer
  @ivar lsr_state: The state object shared across all L{Tier}s
  @type lsr_state: L{AEState.AEState}
  @ivar perk_state: The state object shared across all instances of this 
    L{Perk}
  @type perk_state: L{AEState.AEState}
  @ivar task_por:  The L{POR} for the Task or Perk subclass, set by 
    L{preExecute}
  @type task_por: L{POR}
  @ivar tools_initialized: Is the instance ready to be executed?
  @type tools_initialized: boolean
  @ivar tools_auto_pointer: Should the task_por become the pointer after 
    execution?
  @type tools_auto_pointer: boolean
  @ivar def_out: Default output device to use for all
  L{Task.Tools.Output.Output.say}
    calls in this L{Perk} module.
  @type def_out: weakref.proxy to L{AEOutput}
  '''
  def __init__(self):
    '''
    Create the ivars and initialize them to None or reasonable defaults.
    '''
    self.perk = None
    self.event_man = None
    self.device_man = None
    self.tier_man = None
    self.view_man = None
    self.sett_man = None
    self.lsr_state = None
    self.perk_state = None
    self.task_por = None
    self.tier = None
    self.layer = None
    self.acc_eng = None
    self.def_out = None
    self.tools_initialized = False
    self.tools_auto_pointer = True
    
  def preInit(self, ae, tier, perk=None):
    ''' 
    Initializes the references to the L{Perk}, L{Tier}, L{AccessEngine}
    owning this instance of the L{TaskTools}. Contacts the L{AccessEngine} to
    get refs to all managers. These references are fixed throughout the life of
    the tools object.
    
    Called before state information is loaded from disk.
    
    @param ae: Reference to AccessEngine for event context
    @type ae: L{AccessEngine}
    @param tier: The L{Tier} executing this L{TaskTools} object
    @type tier: L{Tier}
    @param perk: The L{Perk} containing this L{TaskTools} object
    @type perk: L{Perk}
    @return: Did the L{TaskTools} initialize (True) or not (False)?
    @rtype: boolean
    '''
    if self.tools_initialized:
      # don't reinitialize if we already are initialized
      return False
    
    # store a reference to the AccessEngine and owning Tier
    self.acc_eng = ae
    self.tier = tier
    
    # store the perk, which may be this object itself
    if perk is None:
      self.perk = self
    else:
      self.perk = perk
      
    # get all manager references from AccessEngine very quickly
    self.acc_eng.loanManagers(self)
    
    # get all state objects
    self.lsr_state = self.tier_man.getState()
    self.perk_state = self.perk.getState()
    
    # indicate we're initialized now
    self.tools_initialized = True
    
    return True

  def init(self):
    '''
    Does nothing. Reserved for subclasses to initialize themselves.
    '''
    pass

  def postClose(self):
    '''
    Frees the references to the L{Tier}, L{Perk}, and L{AccessEngine} as well 
    as all borrowed references from the L{AccessEngine}. Releases references to
    state information. Intended to be called after L{close}.
    '''
    if not self.tools_initialized:
      # don't re-uninitialize
      return
    # toss references to owning Perk and Tier
    self.tier = None
    self.perk = None
    # throw away manager and access engine refs
    self.acc_eng.unloanManagers(self)
    self.acc_eng = None
    # IMPORTANT: throw away weak ref to device else the callback keeps it alive
    self.def_out = None
    # throw away references to state
    self.lsr_state = None
    self.perk_state = None
    # inidicate we're not initialized anymore
    self.tools_initialized = False

  def close(self):
    '''
    Does nothing. Reserved for subclasses to finalize themselves.
    '''
    pass
  
  def preExecute(self, layer, event_por=None):
    '''
    Configures this L{TaskTools} object before execution.

    @param layer: Layer on which the event occurred
    @type layer: integer
    @param event_por: Point of regard received with the event that triggered
      this execution
    @type event_por: L{POR}
    @return: Is the instance ready for execution?
    @rtype: boolean
    '''
    if not self.tools_initialized:
      # don't do anything if the tools aren't initialized
      return False
    
    # reset the auto pointer flag to True
    self.tools_auto_pointer = True

    # store the layer
    self.layer = layer

    # use the POR from the event or the last saved Task POR if there is no POR 
    # associated with the event
    self.task_por = event_por or self.tier.getPointer()
    
    try:
      # call a method on the default output device to ensure the ref is alive
      self.def_out.getName()
    except (AttributeError, ReferenceError):
      # get an output device to serve as the default output
      self._changeDefaultOutput()
    return True

  def postExecute(self):
    '''
    Unconfigures this L{TaskTools} object after execution. Automatically
    updates the L{Tier} pointer L{POR} if , the L{tools_initialized} flag is
    True, the L{tools_auto_pointer} is True, and the event handled is in the
    focus layer.
    '''
    if (self.tools_initialized and # only store if initialized
        self.tools_auto_pointer and # only store if auto pointer
        AEConstants.LAYER_FOCUS == self.layer): # only store if focus layer
      self.tier.setPointer(self.task_por)

  def _changeDefaultOutput(self, ref=None):
    '''
    Gets an output device based on the ideal capabilities requested by the
    current L{Perk}. Creates a weak proxy for the device object. When the
    weak ref is no longer valid, this method is called again to find another
    suitable device.
    
    @param ref: Weak proxy that is no longer valid
    @type ref: weakref.proxy
    '''
    if not self.tools_initialized:
      return
    # get an output device to serve as the default output
    caps = self.perk.getIdealOutput()
    dev = self.device_man.getOutputByCaps(caps)
    try:
      self.def_out = weakref.proxy(dev, self._changeDefaultOutput)
    except TypeError:
      self.def_out = None
