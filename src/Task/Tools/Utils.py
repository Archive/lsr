'''
Defines L{Tools} that function as a set of utilities.

@author: Brett Clippingdale
@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''

import Base, AEConstants

class Utils(Base.TaskTools):
  '''
  Provides utility methods that assist L{Perk} developers in processing 
  accessibility information, but do not touch accessible objects, do output,
  handle input, manage the LSR system, etc.
  '''
  def getColorString(self, val):
    '''
    Tries to map a given RGB string (if in form "u,u,u") to a nearby color 
    name.
    
    @param val: Representation of an RGB color in form "u,u,u"
    @type val: string
    @return: Localized color name if val param is valid format, otherwise None
    @rtype: string
    '''
    # if color value an RGB 3-tuple, map it to a string value
    try:
      r, g, b = val.split(',')
      r, g, b = int(r), int(g), int(b)
    except (ValueError, TypeError, AttributeError):
      return val
    WORD = 256 # 8-bit word size
    
    VISIBLE = 64
    LOW_COLORATION = 4 
    LOW_SATURATION = 192
    HIGH_SATURATION = 240
    
    # normalize values into three 8-bit values to reprent RGB color
    if r >= WORD or g >= WORD or b >= WORD:
      r = int(r / WORD) # can't guarantee apps will have modulo 256 color
      g = int(g / WORD)
      b = int(b / WORD)
    
    # if all values of RGB 3-tuple have low saturation, amplify them
    while r < VISIBLE and g < VISIBLE and b < VISIBLE and \
          (r > LOW_COLORATION or g > LOW_COLORATION or b > LOW_COLORATION):
      r = r << 1
      g = g << 1
      b = b << 1
      
    # attenuation algo #1  
    ## if all values of RGB 3-tuple have high saturation, attenuate them
    #while r >= LOW_SATURATION and g >= LOW_SATURATION and b >= LOW_SATURATION and \
          #(r < HIGH_SATURATION or g < HIGH_SATURATION or b < HIGH_SATURATION):
      #print "high saturation: shifting rgb from:", r, g, b
      #r = r >> 1
      #g = g >> 1
      #b = b >> 1
      #print "shifted rgb to:" , r, g, b
    
    # attenuation algo #2
    # if all values of RGB 3-tuple have high saturation, attenuate them
    if r >= LOW_SATURATION and g >= LOW_SATURATION and b >= LOW_SATURATION and \
       (r < HIGH_SATURATION or g < HIGH_SATURATION or b < HIGH_SATURATION):
      if r < g and r < b:
        r = r - 64
      elif g < r and g < b:
        g = g - 64
      elif b < r and b < g:
        b = b - 64
      elif r == g == b:
        # assign levels of grey based on saturation
        if r > 250:
          return AEConstants.COLOR_MAP[63] #'white'
        elif r > 200:
          return AEConstants.COLOR_MAP[41] # light grey
        elif r > 96:
          return AEConstants.COLOR_MAP[21] # medium grey
        else:
          return AEConstants.COLOR_MAP[42] # dark grey
        pass
      elif r == g:
        r = r - 64
        g = g - 64
      elif r == b:
        r = r - 64
        b = b - 64
      elif g == b:
        g = g - 64
        b = b - 64
      
    # place high 2-bits of each color into bitfield: rrggbb for value 0..63
    r = r >> 6 << 4
    g = g >> 6 << 2
    b = b >> 6
    rgb = r+g+b # add into 6-bit bitfield (rrggbb) for integer value 0..63
    name = AEConstants.COLOR_MAP[rgb] # get the color value from dictionary
    return name
  
  def convertPOSIXToIANA(self, lang):
    '''
    Converts a POSIX language string to a IANA language tag following RFC 4646
    U{http://www.ietf.org/rfc/rfc4646.txt}, the format recommended for 
    L{AEOutput} device language settings.
    
    The POSIX format is language[_territory][.codeset]
    
    Some examples include "en", "fr_CA", "en_US.UTF-8". Ambiguous conversions 
    such as "zh_CN" are allowed, but do not pinpoint specific spoken dialects 
    (i.e. Mandarin, Cantonese, Pinyin, Taiwanese Mandarin, etc.) The code set
    is currently ignored, though may serve as a hint for the dialect in the
    future.

    @param lang: POSIX locale string
    @type lang: string
    @return: IANA language tag
    @rtype: string
    '''
    tag = []
    lang = lang.split('_')
    try:
      # if first element is there, it's the language code
      tag.append(lang[0])
    except IndexError:
      pass
    try:
      # if the second element is there, it's the territory code
      t = lang[1].split('.')[0]
    except IndexError:
      pass
    else:
      # if there was no language code, backfill using the territory
      if len(tag) == 0:
        tag.append(t)
      tag.append(t) 
    # make lowercase for comparison
    return '-'.join(tag).lower()
