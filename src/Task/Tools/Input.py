'''
Defines L{Tools} for getting input.

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''

import Base
from Error import *

class Input(Base.TaskTools):
  '''
  Provides methods for getting input on any L{AEInput} device.
  '''
  def getInputDevice(self, name=None, *capabilities):
    '''
    Gets an L{AEOutput} device from the L{DeviceManager} given its name or
    its capabilities. The capabilities list may include strings naming 
    L{AEInput} interfaces ("braille" and/or "system input" at present).
    
    If neither is specified, the first available output device in the
    L{DeviceManager} is returned.
    
    @param name: Class (UIE) name of the L{AEInput} device to get
    @type name: string
    @param capabilities: Names of capabilities required on the device
    @type capabilities: list of string
    @return: The named output device
    @rtype: L{AEInput}
    @raise InvalidDeviceError: When the specified L{AEInput} device is not 
      found
    '''
    if name is not None:
      device = self.device_man.getInputByName(name)
    elif len(capabilities):
      device = self.device_man.getInputByCaps(capabilities)
    else: 
      device = self.device_man.getFirstInput()
    if device is None:
      # let the system report the device isn't available
      raise InvalidDeviceError
    return device
  
  def addInputModifiers(self, dev, *codes):
    '''
    Adds the given device code as a modifier on the named L{AEInput} device.
    If the device doesn't support modifiers, ignores the error since it's a 
    non-critical operation.
    
    @param dev: Reference to an input device
    @type dev: L{AEInput}
    @param codes: Action codes to add as modifiers
    @type codes: integer
    '''
    # notify device
    try:
      func = dev.addModifier 
    except AttributeError:
      return
    map(func, codes)
    # register modifier with perk
    self.perk.registerModifiers(dev, codes)
     
  def removeInputModifiers(self, dev, *codes):
    '''
    Removes the given device code as a modifier on the named L{AEInput} device.
    If the device doesn't support modifiers, ignores the error since it's a 
    non-critical operation.
    
    @param dev: Reference to an input device
    @type dev: L{AEInput}
    @param codes: Action codes to add as modifiers
    @type codes: integer
    '''
    # notify device
    try:
      func = dev.removeModifier 
    except AttributeError:
      return
    map(func, codes)
    
    try:
      # unregister modifier from perk
      self.perk.unregisterModifiers(dev, codes)  
    except AttributeError:
      pass
  
  def registerCommand(self, dev, task_ident, propagate, *codes):
    '''
    Registers a L{Task} in this L{Perk} to be executed in response to an 
    L{AEEvent} indicating that the given action codes were input on the given 
    L{AEInput} device. 
    
    @param dev: Device to monitor for input
    @type dev: L{AEInput}
    @param task_ident: Name of the L{Task} registered via 
      L{Task.Tools.System.System.registerTask} to execute when the input 
      gesture is detected on the device
    @type task_ident: string
    @param propagate: Should the input gesture be allowed to propagate to the 
      OS after we receive it?
    @type propagate: boolean
    @param codes: List of lists of action codes forming the L{AEInput.Gesture} 
      that will trigger the execution of the named L{Task}. For example, 
      codes=[[Keyboard.AEK_CTRL, Keyboard.AEK_TILDE]] indicates the single
      gesture of simultaneously pressing Ctrl and ~ on the keyboard device.
    @type codes: list of list of integer
    @raise ValueError: When a L{Task} with the given name is not registered
    @raise InvalidDeviceError: When a L{AEInput} device with the given name is
      not registered
    '''
    self.perk.registerCommandTask(dev, codes, task_ident, propagate)
    
  def unregisterCommand(self, dev, *codes):
    '''
    Unregisters a L{Task} set to execute in response to the given action codes 
    on the given device B{from this L{Perk} only}.
    
    @param dev: Device to monitor for input
    @type dev: L{AEInput}
    @param codes: List of lists of action codes forming the L{AEInput.Gesture} 
      that will trigger the execution of the named L{Task}. For example, 
      codes=[[Keyboard.AEK_CTRL, Keyboard.AEK_TILDE]] indicates the single
      gesture of simultaneously pressing Ctrl and ~ on the keyboard device.
    @type codes: list of list of integer
    @raise KeyError: When a L{AEInput.GestureList} is not registered
    @raise InvalidDeviceError: When a L{AEInput} device with the given name is
      not registered
    '''
    self.perk.unregisterCommandTask(dev, codes)
