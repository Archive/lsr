'''
Defines L{Tools} for registering, unregistering, introspecting, and otherwise
manging L{Tier}s, L{Perk}s, and L{Task}s.

@author: Peter Parente
@author: Scott Haeger
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
import weakref, sys
from Error import *
import AEEvent
import Base
import AEConstants, UIRegistrar

class System(Base.TaskTools):
  '''
  Provides methods for managing L{Tier}s, L{Perk}s, and L{Task}s.
  '''
  def getProfileName(self):
    '''
    Gets the name of the profile LSR is running under.
    
    @return: Profile name
    @rtype: string
    '''
    return self.acc_eng.getProfile()
  
  def pushPerk(self, *names):
    '''
    Adds one or more L{Perk}s to the top of the stack. If more than one L{Perk}
    is specified, the last specified L{Perk} will be at the top of the stack 
    when this method completes. That is, the behavior of pushing more than one
    L{Perk} at a time is the same as if each L{Perk} were pushed individually.
    
    @param names: Names of L{Perk}s to add
    @type names: string
    '''
    reg = UIRegistrar
    perks = []
    for name in names:
      perk = reg.loadOne(name)
      if perk is not None:
        perks.append(perk)
    self.tier.pushPerk(self.acc_eng, *perks)
    
  def insertPerk(self, index, perk):
    '''
    Adds one L{Perk} to the stack at the insertion index. Negative indices are
    valid per Python list convention.
    
    @param index: Index at which the L{Perk} should be inserted into the stack
      where zero is the bottom
    @type index: integer
    @param perk: L{Perk} to add
    @type perk: L{Perk}
    '''
    reg = UIRegistrar
    perk = reg.loadOne(perk)
    if perk is not None:
      self.tier.insertPerk(self.acc_eng, index, perk)
  
  def popPerk(self, *indices):
    '''
    Removes one or more L{Perk}s from the stack given their indices.
    
    @param indices: Indices of L{Perk}s to remove
    @type indices: list of integer
    @raise IndexError: When the stack is empty
    @raise ValueError: When specified L{Perk} is not found
    '''
    self.tier.popPerk(*indices)
    
  def popThisPerk(self):
    '''
    Removes the current L{Perk} from the L{Tier}. No L{Task.Tools} methods 
    should be invoked after this call.
    '''
    perks = self.tier.getPerks()
    i = perks.index(self.perk)
    self.tier.popPerk(i)
    
  def reloadPerks(self):
    '''
    Reloads all L{Perk}s in the current L{Tier}. Any changes made to the 
    L{Perk} logic will take effect immediately after the reload. This method
    does not refer to the L{Perk}s associated with the current profile. It 
    simply reloads all currently loaded L{Perk}s.
    '''
    reg = UIRegistrar
    # get official registered Perk names for all existing Perks in this Tier
    names = self.getPerkClassNames()
    names.reverse()
    # store Tier and AccessEngine references before deletion
    tier = self.tier
    acc_eng = self.acc_eng
    # clear out existing Perks
    self.tier.clearPerks()
    # restore the Tier reference
    self.tier = tier
    self.acc_eng = acc_eng
    # push all new Perks
    self.pushPerk(*names)
    # throw the refs away permanently
    self.tier = None
    self.acc_eng = None
    
  def _getUIEInstances(self, kind, which):
    '''
    Internal method for getting instances of all installed or associated
    L{UIElement}s for the given kind.
    
    @note: Installed UIEs that are not importable because of missing 
      dependencies or other errors are not included in the list.
    @note: Getting loaded chooser names is not currently supported.
    @param kind: Kind of L{UIElement} to fetch metadata for, one of 
      L{UIRegistrar.ALL_KINDS}
    @type kind: string
    @param which: Which names to get, one of the UIE_ constants in 
      L{AEConstants}
    @type which: integer
    @return: All descriptions
    @rtype: list of string
    '''
    reg = UIRegistrar
    if which == AEConstants.UIE_LOADED:
      # handle each type serparately, as loaded instances are stored in diverse
      # locations in LSR
      if kind == reg.PERK:
        # get Perks in this Tier
        uies = self.tier.getPerks()
      elif kind == reg.DEVICE:
        # get devices
        uies = self.device_man.getDevices()
      elif kind == reg.CHOOSER:
        # not supported at present
        uies = []
      elif kind == reg.MONITOR:
        imons, omons = self.device_man.getMonitors()
        uies = list(set(imons) + set(omons) + set(self.tier_man.getMonitors()) 
                    + set(self.event_man.getMonitors()))
      # we already have live instances here, so just return them
      return uies
    
    # use the UIRegistrar to look up class names
    if which == AEConstants.UIE_INSTALLED:
      names = reg.listInstalled(kind)
    elif which == AEConstants.UIE_ALL_ASSOCIATED:
      names = reg.listAssociated(kind, self.acc_eng.getProfile())
    elif which == AEConstants.UIE_TIER_ASSOCIATED and kind == reg.PERK:
      names = reg.listAssociated(kind, self.acc_eng.getProfile(),
                             self.tier.getName())
    # here we need to contruct live instances because all we have is a class 
    # name from the UIRegistrar
    l = []
    for name in names:
      uie = reg.loadOne(name)
      if uie is not None:
        l.append(uie)
    return l

  def _getClassNames(self, kind, which):
    '''
    Internal method for getting class names of all installed or associated
    L{UIElement}s for the given kind.
    
    @note: Installed UIEs that are not importable because of missing 
      dependencies are not listed as installed to maintain parity with 
      L{_getNames} and L{_getDescriptions}.
    @param kind: Kind of L{UIElement} to fetch metadata for, one of 
      L{UIRegistrar.ALL_KINDS}
    @type kind: string
    @param which: Which names to get, one of the UIE_ constants in 
      L{AEConstants}
    @type which: integer
    @return: All descriptions
    @rtype: list of string
    '''
    reg = UIRegistrar
    if which == AEConstants.UIE_INSTALLED:
      names = reg.listInstalled(kind)
    elif which == AEConstants.UIE_ALL_ASSOCIATED:
      names = reg.listAssociated(kind, self.acc_eng.getProfile())
    elif which == AEConstants.UIE_TIER_ASSOCIATED and kind == reg.PERK:
      names = reg.listAssociated(kind, self.acc_eng.getProfile(),
                             self.tier.getName())
    # make sure the UIE is loadable before listing it
    return filter(lambda x: reg.loadOne(x) is not None, names)
    
  def _getNames(self, kind, which):
    '''
    Internal method for getting names of all installed or associated
    L{UIElement}s for the given kind.
    
    @param kind: Kind of L{UIElement} to fetch metadata for, one of 
      L{UIRegistrar.ALL_KINDS}
    @type kind: string
    @param which: Which names to get, one of the UIE_ constants in 
      L{AEConstants}
    @type which: integer
    @return: All descriptions
    @rtype: list of string
    '''
    reg = UIRegistrar
    if which == AEConstants.UIE_INSTALLED:
      return [uie.getName() for uie in (reg.loadOne(name) for name in 
                                        reg.listInstalled(kind))
                                        if uie is not None]
    elif which in (AEConstants.UIE_ALL_ASSOCIATED, 
                   AEConstants.UIE_TIER_ASSOCIATED):
      names = self._getClassNames(kind, which)
      if names is None:
        return None
      return [uie.getName() for uie in (reg.loadOne(name) for name in names)
              if uie is not None]
  
  def _getDescriptions(self, kind, which):
    '''
    Internal method for getting descriptions of all installed or associated
    L{UIElement}s for the given kind.
    
    @param kind: Kind of L{UIElement} to fetch metadata for, one of 
      L{UIRegistrar.ALL_KINDS}
    @type kind: string
    @param which: Which names to get, one of the UIE_ constants in 
      L{AEConstants}
    @type which: integer
    @return: All descriptions
    @rtype: list of string
    '''
    reg = UIRegistrar
    if which == AEConstants.UIE_INSTALLED:
      return [uie.getDescription() for uie in (reg.loadOne(name) for name in 
                                               reg.listInstalled(kind))
                                               if uie is not None]
    elif which in (AEConstants.UIE_ALL_ASSOCIATED, 
                   AEConstants.UIE_TIER_ASSOCIATED):
      names = self._getClassNames(kind, which)
      if names is None:
        return None
      return [uie.getDescription() for uie in (reg.loadOne(name) 
                                               for name in names)
                                               if uie is not None]

  def getPerkMetadata(self, which=AEConstants.UIE_LOADED):
    '''
    Gets metadata for all L{Perk}s in this L{Tier}, all installed L{Perk}s,
    L{Perk}s associated with this L{Tier}, or all L{Perk}s associated with this
    profile depending on the which flag. Includes L{Perk} class names, human
    readable names, and descriptions.
    
    The default value is used if which is invalid.
    
    @param which: Which names to get, one of the UIE_ constants in 
      L{AEConstants}
    @type which: integer
    @return: Class names, names, descriptions
    @rtype: list of 3-tuple of string
    '''
    uies = self._getUIEInstances(UIRegistrar.PERK, which)
    return zip((uie.getClassName() for uie in uies),
               (uie.getName() for uie in uies),
               (uie.getDescription() for uie in uies))  

  def getDeviceMetadata(self, which=AEConstants.UIE_LOADED):
    '''    
    Gets metadata for all L{AEOutput} and L{AEInput} devices currently loaded
    in, installed in, or associated with the active profile depending on the
    which flag.

    The default value is used if which is invalid.
    
    @param which: Which names to get, one of the UIE_ constants in 
      L{AEConstants}
    @type which: integer
    @return: Class names, names, descriptions
    @rtype: list of 3-tuple of string
    '''
    uies = self._getUIEInstances(UIRegistrar.DEVICE, which)
    return zip((uie.getClassName() for uie in uies),
               (uie.getName() for uie in uies),
               (uie.getDescription() for uie in uies))  

  def getMonitorMetadata(self, which=AEConstants.UIE_LOADED):
    '''    
    Gets metadata for all L{AEMonitor}s currently loaded in, installed in, or
    associated with the active profile depending on the which flag.

    The default value is used if which is invalid.
    
    @param which: Which names to get, one of the UIE_ constants in 
      L{AEConstants}
    @type which: integer
    @return: Class names, names, descriptions
    @rtype: list of 3-tuple of string
    '''
    uies = self._getUIEInstances(UIRegistrar.MONITOR, which)
    return zip((uie.getClassName() for uie in uies),
               (uie.getName() for uie in uies),
               (uie.getDescription() for uie in uies))  

  def getChooserMetadata(self, which=AEConstants.UIE_ALL_ASSOCIATED):
    '''        
    Gets metadata for all L{AEChooser}s installed in or associated with the
    active profile depending on the which flag.
    
    The default value is used if which is invalid.
    
    @note: Getting loaded chooser names is not currently supported.   
    @param which: Which names to get, one of the UIE_ constants in 
      L{AEConstants}
    @type which: integer
    @return: Class names, names, descriptions
    @rtype: list of 3-tuple of string
    '''
    uies = self._getUIEInstances(UIRegistrar.CHOOSER, which)
    return zip((uie.getClassName() for uie in uies),
               (uie.getName() for uie in uies),
               (uie.getDescription() for uie in uies))  

  def getDeviceClassNames(self, which=AEConstants.UIE_LOADED):
    '''
    Gets the programmatic class names of all L{AEOutput} and L{AEInput} devices
    currently loaded in, installed in, or associated with the active profile
    depending on the which flag.
    
    The default value is used if which is invalid.
    
    @param which: Which names to get, one of the UIE_ constants in 
      L{AEConstants}
    @type which: integer
    @return: Names of all L{AEOutput} and L{AEInput} classes
    @rtype: list of string
    '''
    if which != AEConstants.UIE_LOADED:
      return self._getClassNames(UIRegistrar.DEVICE, which)
    else:
      return [dev.getClassName() for dev in self.device_man.getDevices()]
    
  def getDeviceNames(self, which=AEConstants.UIE_LOADED):
    '''    
    Gets the human readable names of all L{AEOutput} and L{AEInput} devices
    currently loaded in, installed in, or associated with the active profile
    depending on the which flag.
    
    The default value is used if which is invalid.
    
    @param which: Which names to get, one of the UIE_ constants in 
      L{AEConstants}
    @type which: integer
    @return: Names of all L{AEOutput} and L{AEInput} devices
    @rtype: list of string
    '''
    if which != AEConstants.UIE_LOADED:
      return self._getNames(UIRegistrar.DEVICE, which)
    else:
      return [dev.getName() for dev in self.device_man.getDevices()]
    
  def getDeviceDescriptions(self, which=AEConstants.UIE_LOADED):
    '''    
    Gets the descriptions of all L{AEOutput} and L{AEInput} devices
    currently loaded in, installed in, or associated with the active profile
    depending on the which flag.
    
    The default value is used if which is invalid.
    
    @param which: Which names to get, one of the UIE_ constants in 
      L{AEConstants}
    @type which: integer
    @return: Names of all L{AEOutput} and L{AEInput} devices
    @rtype: list of string
    '''
    if which != AEConstants.UIE_LOADED:
      return self._getDescriptions(UIRegistrar.DEVICE, which)
    else:
      return [dev.getDescription() for dev in self.device_man.getDevices()]
  
  def getMonitorClassNames(self, which=AEConstants.UIE_LOADED):
    '''    
    Gets the programmatic class names of all L{AEMonitor}s currently loaded in,
    installed in, or associated with the active profile depending on the which 
    flag.
    
    The default value is used if which is invalid.
    
    @param which: Which names to get, one of the UIE_ constants in 
      L{AEConstants}
    @type which: integer
    @return: Names of all L{AEMonitor} classes
    @rtype: list of string
    '''
    if which != AEConstants.UIE_LOADED:
      return self._getClassNames(UIRegistrar.MONITOR, which)
    else:
      return [mon.getClassName() for mon in 
              self._getUIEInstances(UIRegistrar.MONITOR, which)]

  def getMonitorNames(self, which=AEConstants.UIE_LOADED):
    '''    
    Gets the human readable names of all L{AEMonitor}s currently loaded in,
    installed in, or associated with the active profile depending on the which 
    flag.
    
    The default value is used if which is invalid.
    
    @param which: Which names to get, one of the UIE_ constants in 
      L{AEConstants}
    @type which: integer
    @return: Names of all L{AEMonitor}s
    @rtype: list of string
    '''
    if which != AEConstants.UIE_LOADED:
      return self._getNames(UIRegistrar.MONITOR, which)
    else:
      return [mon.getName() for mon in 
              self._getUIEInstances(UIRegistrar.MONITOR, which)]
    
  def getMonitorDescriptions(self, which=AEConstants.UIE_LOADED):
    '''    
    Gets the human readable descriptions of all L{AEMonitor}s currently loaded
    in, installed in, or associated with the active profile depending on the 
    which flag.
    
    The default value is used if which is invalid.
    
    @param which: Which names to get, one of the UIE_ constants in 
      L{AEConstants}
    @type which: integer
    @return: Descriptions of all L{AEMonitor}s
    @rtype: list of string
    '''
    if which != AEConstants.UIE_LOADED:
      return self._getDescriptions(UIRegistrar.MONITOR, which)
    else:
      return [mon.getDescription() for mon in 
              self._getUIEInstances(UIRegistrar.MONITOR, which)]
  
  def getChooserClassNames(self, which=AEConstants.UIE_ALL_ASSOCIATED):
    '''    
    Gets the programmatic class names of all L{AEChooser}s installed in or
    associated with the active profile depending on the which flag.
    
    The default value is used if which is invalid.
    
    @note: Getting loaded chooser names is not currently supported.
    @param which: Which names to get, one of the UIE_ constants in 
      L{AEConstants}
    @type which: integer
    @return: Names of all L{AEChooser} classes
    @rtype: list of string
    '''
    if which != AEConstants.UIE_LOADED:
      return self._getClassNames(UIRegistrar.CHOOSER, which)
    
  def getChooserNames(self, which=AEConstants.UIE_ALL_ASSOCIATED):
    '''    
    Gets the human readable names of all L{AEChooser}s installed in or
    associated with the active profile depending on the which flag.
    
    The default value is used if which is invalid.
    
    @note: Getting loaded chooser names is not currently supported.
    @param which: Which names to get, one of the UIE_ constants in 
      L{AEConstants}
    @type which: integer
    @return: Names of all L{AEChooser}s
    @rtype: list of string
    '''
    if which != AEConstants.UIE_LOADED:
      return self._getNames(UIRegistrar.CHOOSER, which)
    
  def getChooserDescriptions(self, which=AEConstants.UIE_ALL_ASSOCIATED):
    '''
    Gets the descriptions of all L{AEChooser}s installed in or associated with
    the active profile depending on the which flag.
    
    The default value is used if which is invalid.
    
    @note: Getting loaded chooser names is not currently supported.
    @param which: Which names to get, one of the UIE_ constants in 
      L{AEConstants}
    @type which: integer
    @return: Descriptions of all L{AEChooser}s
    @rtype: list of string
    '''
    if which != AEConstants.UIE_LOADED:
      return self._getDescriptions(UIRegistrar.CHOOSER, which)

  def getPerkClassNames(self, which=AEConstants.UIE_LOADED):
    '''
    Gets the programmatic class names of all L{Perk}s in this L{Tier}, all
    installed L{Perk}s, L{Perk}s associated with this L{Tier}, or all L{Perk}s 
    associated with this profile depending on the which flag.
    
    The default value is used if which is invalid.
    
    @param which: Which names to get, one of the UIE_ constants in 
      L{AEConstants}
    @type which: integer
    @return: Names of all L{Perk} classes
    @rtype: list of string
    '''
    if which != AEConstants.UIE_LOADED:
      return self._getClassNames(UIRegistrar.PERK, which)
    else:
      return [perk.getClassName() for perk in self.tier.getPerks()]

  def getPerkNames(self, which=AEConstants.UIE_LOADED):
    '''    
    Gets the human readable names of all L{Perk}s in this L{Tier}, all
    installed L{Perk}s, L{Perk}s associated with this L{Tier}, or all L{Perk}s 
    associated with this profile depending on the which flag. 
    
    The default value is used if which is invalid.
    
    @param which: Which names to get, one of the UIE_ constants in 
      L{AEConstants}
    @type which: integer
    @return: Names of all L{Perk}s
    @rtype: list of string
    '''
    if which != AEConstants.UIE_LOADED:
      return self._getNames(UIRegistrar.PERK, which)
    else:
      return [perk.getName() for perk in self.tier.getPerks()]
  
  def getPerkDescriptions(self, which=AEConstants.UIE_LOADED):
    '''    
    Gets the human readable descriptions of all L{Perk}s in this L{Tier}, all
    installed L{Perk}s, L{Perk}s associated with this L{Tier}, or all L{Perk}s 
    associated with this profile depending on the which flag.
    
    The default value is used if which is invalid.
    
    @param which: Which names to get, one of the UIE_ constants in 
      L{AEConstants}
    @type which: integer
    @return: Descriptions of all L{Perk}s
    @rtype: list of string
    '''
    if which != AEConstants.UIE_LOADED:
      return self._getDescriptions(UIRegistrar.PERK, which)
    else:
      return [perk.getDescription() for perk in self.tier.getPerks()]
    
  def registerTask(self, task):
    '''
    Universal task registration method. Uses known interfaces to try to 
    register the given L{Task} as an event task, an input task, a timer task,
    etc. All information for configuring the L{Task} is contained in the 
    task itself.
    
    @param task: Task to register
    @type task: L{Task.Base.Task}
    @raise NotImplementedError: When the L{Task} does not properly implement
      the interfaces it should
    '''
    count = 0
    try:
      # first try as an event task
      self.perk.registerEventTask(task, *task.getEventLayers())
      count += 1
    except (AttributeError, NotImplementedError):
      pass
    try:
      # then try as a timer task
      self.perk.registerTimerTask(task, task.getTimerInterval())
      count += 1
    except AttributeError:
      pass
    try:
      # now try as a named task
      self.perk.registerNamedTask(task, task.getIdentity())
      count += 1
    except (AttributeError, ValueError):
      pass
    if count == 0:
      # not any kind of known task
      raise NotImplementedError
    
  def unregisterTask(self, task):
    '''
    Universal task unregistration method. Uses known interfaces to try to 
    unregister the given L{Task} as an event task, an input task, a timer task,
    etc. All information for configuring the L{Task} is contained in the 
    task itself.
    
    @param task: Task to unregister
    @type task: L{Task.Base.Task}
    @raise NotImplementedError: When the L{Task} does not properly implement
      the interfaces it should
    '''
    count = 0
    perk = self.perk
    try:
      # first try as an event task
      perk.unregisterEventTask(task, *task.getEventLayers())
      count += 1
    except AttributeError:
      pass
    try:
      # then try as a timer task
      # make sure it's a timer task first
      task.getTimerInterval
      perk.unregisterTimerTask(task)
      count += 1  
    except AttributeError:
      pass
    try:
      # now try as a named task
      perk.unregisterNamedTask(task.getIdentity())
      count += 1
    except (AttributeError, ValueError, KeyError):
      pass
    if not count:
      # not any kind of known task
      raise NotImplementedError
    
  def chainTask(self, name, link, target):
    '''
    Chains the L{Task} with the given name to the target L{Task}. If link is
    CHAIN_BEFORE, the linked L{Task} will execute before the target. If link is
    CHAIN_AFTER, the linked L{Task} will execute after. If link is
    CHAIN_AROUND, the linked L{Task} will execute in lieu of the target, but
    can execute the target manually using L{doTask}.
    
    @param target: Name of the L{Task} to link to
    @type target: string
    @param link: One of the CHAIN constants in L{AEConstants.Tools} 
    @type link: integer
    @param name: Name of the L{Task} to link
    @type name: string
    '''
    self.perk.linkTaskToChain(name, link, target)

  def unchainTask(self, name, link, target):
    '''
    Unchains the L{Task} with the given name to the target L{Task}. The link
    parameter has the same meaning as in L{chainTask}. If link is set to None,
    one reference to the named L{Task} is removed from the before, after, and
    around chains.
    
    @note: The target L{Task} must be registered at the time of invocation.
    @param target: Name of the L{Task} to unlink from
    @type target: string
    @param link One of the CHAIN constants in L{AEConstants.Tools} or None
    @type link: integer
    @param name: Name of the L{Task} to unlink
    @type name: string
    @raise ValueError: When the named L{Task} is not unlinked at least once 
    @raise KeyError: When the target L{Task} does not have a chain segement in
      this L{Perk} for the given link type
    '''
    self.perk.unlinkTaskFromChain(name, link, target)
       
  def getTierNamedTask(self, name):
    '''
    Gets the L{Task} with the given name if it is registered anywhere in the
    current L{Tier}. If no L{Task} is registered under the given name, returns 
    None.
    
    Calls L{Tier.Tier.getNamedTask} to search across all L{Perk}s in the owning
    L{Tier}.
    
    @param name: Name of the L{Task} to locate
    @type name: string
    @return: L{Task} with the given name or None
    @rtype: L{Task.Base.Task}
    '''
    return self.tier.getNamedTask(name)
    
  def getTierEventTasks(self, event_type, task_layer):
    '''
    Gets all registered L{Task}s registered to handle the given L{AEEvent} type
    on the given layer from the current L{Tier}. 
    
    Calls L{Tier.Tier.getEventTasks} to search across all L{Perk}s in the 
    owning L{Tier}.  
    
    @param event_type: Desired type of L{AEEvent}
    @type event_type: L{AEEvent} class
    @param task_layer: Layer on which the desired L{Task}s are registered
    @type task_layer: integer
    @return: List of all L{Task}s that handle the given event type and on 
      the given layer in the L{Tier} which owns this L{Perk}
    @rtype: list of L{Task.Base.Task}
    '''
    return self.tier.getEventTasks(event_type, task_layer)
  
  def getNamedTask(self, name):
    '''
    Gets the L{Task} with the given name if it is registered B{in this L{Perk}
    only}. If no L{Task} is registered under the given name in this L{Perk}, 
    returns None.
    
    @param name: Name of the L{Task} to locate
    @type name: string
    @return: L{Task} with the given name or None
    @rtype: L{Task.Base.Task}
    @see: L{getTierNamedTask}
    '''
    return self.perk.getNamedTask(name)
    
  def getEventTasks(self, event_type, task_layer):
    '''
    Get all registered L{Task}s registered to handle the given L{AEEvent} type 
    B{in this L{Perk} only}.
    
    @param event_type: Desired type of L{AEEvent}
    @type event_type: L{AEEvent} class
    @param task_layer: Layer on which the desired L{Task}s are registered
    @type task_layer: integer
    @return: List of all L{Task}s that handle the given event type on the given
      layer in this L{Perk}
    @rtype: list of L{Task.Base.Task}
    @see: L{getTierEventTasks}
    '''
    return self.perk.getEventTasks(event_type, task_layer)
  
  def getDeviceState(self, device_name):
    '''
    Gets the default L{AEState} associated with an L{AEOutput} or L{AEInput} 
    device currently loaded in the L{DeviceManager}.
    
    @param device_name: Name of the device
    @type device_name: string
    @return: Loaded state for the device
    @rtype: L{AEState}
    '''
    try:
      dev = self.getOutputDevice(device_name)
    except InvalidDeviceError:
      return None
    return dev.getDefaultStyle()
  
  def getPerkState(self, perk_name, reload=True):
    '''
    Gets the L{AEState} associated with a L{Perk}. If the state was previously
    persisted to disk, it is loaded from disk. If not, a new state object
    is created for the named Perk and immediately persisted in the user's 
    profile.
    
    @param perk_name: Name of the L{Perk}
    @type perk_name: string
    @return: Loaded state for the L{Perk}
    @rtype: L{AEState}
    '''
    if not reload:
      perk = self.getPerkByName(perk_name)
      return perk.getState()
    
    # load an instance of the perk
    perk = UIRegistrar.loadOne(perk_name)
    if perk is None:
      # quit if we couldn't load the perk
      return None
    # create a new empty state object
    state = perk.getState()
    # try to load previously persisted state
    try:
      return self.sett_man.loadState(perk_name, state)
    except KeyError:
      pass
    # persist a new state instance if it does not exist
    self.sett_man.saveState(perk_name, state)
    return state
  
  def loadAllMonitors(self):
    '''
    Loads and shows all L{AEMonitor}s associated with this profile.
    
    @return: Was at least one monitor loaded?
    @rtype: boolean
    '''
    loaded = False
    reg = UIRegistrar
    mons = reg.loadAssociated(UIRegistrar.MONITOR, self.acc_eng.getProfile())
    emons = self.event_man.getMonitors()
    imons, omons = self.device_man.getMonitors()
    tmons = self.tier_man.getMonitors()
    if not len(emons):
      self.event_man.addMonitors(*mons)
      loaded = True
    if not len(tmons):
      self.tier_man.addMonitors(*mons)
      loaded = True
    if not (len(imons) + len(omons)):
      self.device_man.addMonitors(*mons)
      loaded = True
    return loaded
  
  def unloadAllMonitors(self):
    '''
    Hides and unloads all L{AEMonitor}s associated with this profile.
    
    @return: Was at least one monitor unloaded?
    @rtype: boolean
    '''
    unloaded = False
    emons = self.event_man.getMonitors()
    imons, omons = self.device_man.getMonitors()
    tmons = self.tier_man.getMonitors()
    for mon in (emons, imons, omons, tmons):
      if len(mon):
        mon.clear()
        unloaded = True
    return unloaded
  
  def loadMonitor(self, name):
    '''
    Loads and shows one L{AEMonitor}.
    
    @param name: Class name of the L{AEMonitor}
    @type name: string
    '''
    mon = UIRegistrar.loadOne(name)
    self.event_man.addMonitors(mon)
    self.tier_man.addMonitors(mon)
    self.device_man.addMonitors(mon)
    
  def unloadMonitor(self, name):
    '''
    Hides and unloads one L{AEMonitor}.
    
    @param name: Class name of the L{AEMonitor}
    @type name: string
    '''
    emons = self.event_man.getMonitors()
    tmons = self.tier_man.getMonitors()
    imons, omons = self.device_man.getMonitors()
    for mons in (emons, tmons, imons, omons):
      mons.removeByClassName(name)

  def loadChooser(self, name, **kwargs):
    '''    
    Loads an L{AEChooser} dialog with the given name from disk. Passes the
    keyword arguments to the init method of the chooser. This method should
    typically only be invoked by a L{Task.ChooserTask} subclass.
    
    @param name: UIE name of the chooser
    @type name: string
    @param kwargs: Keyword arguments to initialize the chooser
    @type kwargs: dictionary
    @return: Reference to the chooser if it was created. If it already existed,
      None is returned instead.
    @rtype: L{AEChooser}
    '''
    reg = UIRegistrar
    chooser = reg.loadOne(name)
    if chooser is None:
      return
    try:
      # call the chooser to initialize it with a reference to the event manager
      chooser(self.event_man, self.tier.getIdentity())
    except ValueError, ex:
      # singleton already exists, reactivate
      ex.args[0].activate(**kwargs)
      return None
    else:
      # call init to let the chooser initialize itself
      chooser.init(**kwargs)
      # register to handle events from this chooser
      self.perk.registerChooserTask(chooser, self)
      return chooser
    
  def unloadChooser(self, chooser):
    '''    
    Unloads the given L{AEChooser} dialog. This method should typically only be
    invoked by a L{Task.ChooserTask} subclass.
    
    @param chooser: The chooser to unload
    @type chooser: L{AEChooser}
    '''
    # unregister the task observing this chooser
    self.perk.unregisterChooserTask(chooser)

  def doTask(self, task_name, chain=True, propagate=True, layer=None,**kwargs):
    '''
    Immediately executes a L{Task} registered under the given name in this
    L{Tier}. Keyword arguments for L{Task} execution may be provided. The 
    L{task_por} of this L{Task} is always provided to the L{Task} being 
    executed and this L{Task}'s L{task_por} is updated with the value of the
    pointer after the other L{Task} has finished execution.
    
    The chain parameter dictates whether only the named L{Task} is executed
    (False), or if all other L{Task}s chained to it are as well (True). The
    propagate parameter determines whether the execute or update method is
    called on the L{Task}. This param is not typically specified by a L{Task}
    or L{Perk}. It is used internally by L{Task.CyclicInputTask} to handle the
    propagation flag in Task chains.
    
    @param task_name: Name of the L{Task} to execute
    @type task_name: string
    @param chain: Execute all L{Task}s chained to the one named or just the
      one named?
    @type chain: boolean
    @param propagate: Should chained L{Task}s be executed (True) or updated 
      (False)? 
    @type propagate: boolean
    @param layer: Event layer on which the L{Task} will execute. Defaults to 
      the same layer as the caller if None.
    @type layer: integer
    @param kwargs: Arbitrary keyword arguments to pass to the L{Task}, some of
      which may be required, others which may be optional, and others which
      may be completely ignored
    @type kwargs: dictionary
    '''
    # use the current layer if one is not specified
    if layer is None: layer = self.layer
    # make sure a Task exists with the given name
    task = self.tier.getNamedTask(task_name)
    if task is not None:
      perk = task.getPerk()
      self.tier.manageChain(self.task_por, task_name, task, perk, layer, 
                            kwargs, propagate, chain)
    # use the pointer as the new task por
    self.task_por = self.tier.getPointer()

  def blockNTasks(self, n, task_type, condition=lambda **x: True):
    '''
    Blocks L{Task}s of the given type from executing in response to the next
    n events. This is a convenience shortcut to avoid registering one-shot
    L{Task}s in L{Perk}s to do exactly the same thing.
    
    @param n: Number of events to block
    @type n: integer
    @param task_type: Kind of L{Task} to block
    @type task_type: L{Task} class
    @param condition: Condition statement to check if the event is actually
      one to be consumed. The callable is called with the keyword arguments
      passed to the L{Task} execute method.
    @type condition: callable
    '''
    # define an anonymous class of the given type
    class consumer(task_type):
      def init(self):
        self.count = n
      def execute(self, **kwargs):
        if not condition(**kwargs):
          return
        self.count -= 1
        if self.count == 0:
          self.unregisterTask(self)
        return False
    # register an instance as a blocker
    self.registerTask(consumer(None))
    
  def associatePerk(self, name, tier=None, all_tiers=False, index=None):
    '''
    Associates a L{Perk} with the active profile.
    
    @param name: Name of the UIE to associate
    @type name: string
    @param tier: Name of the L{Tier} with which the L{Perk} will be associated.
      Defaults to the name of the L{Tier} specified by the L{Perk} itself.
    @type tier: string
    @param all_tiers: Should the L{Perk} be loaded on every L{Tier}? Defaults
      to the value specified by the L{Perk} itself.
    @type all_tiers: boolean
    @param index: Load order index of the L{Perk}. Lower means sooner to load
      but also later to handle events (i.e. bottom of the L{Perk} stack).
    @type index: integer
    '''
    UIRegistrar.associate(name, [self.getProfileName()], tier, all_tiers,index)
  
  def disassociatePerk(self, name, tier=None, all_tiers=False):
    '''
    Disassociates a L{Perk} from the active profile.
    
    @param name: Name of the UIE to disassociate
    @type name: string
    @param tier: Name of the L{Tier} from which the L{Perk} will be 
      disassociated. Defaults to the name of the L{Tier} specified by the 
      L{Perk} itself.
    @type tier: string
    @param all_tiers: Should the L{Perk} be removed from the list of Perks to
      load for every L{Tier}? Defaults to the value specified by the L{Perk} 
      itself.
    '''
    UIRegistrar.disassociate(name, [self.getProfileName()], tier, all_tiers)
    
  def associateMonitor(self, name):
    '''
    Associates a L{AEMonitor} with the active profile. Loads it immediately.
    
    @param name: Name of the UIE to associate
    @type name: string
    '''
    UIRegistrar.associate(name, [self.getProfileName()])
    self.loadMonitor(name)
  
  def disassociateMonitor(self, name):
    '''
    Associates a L{AEMonitor} with the active profile. Unloads it immediately.
    
    @param name: Name of the UIE to associate
    @type name: string
    '''
    self.unloadMonitor(name)
    UIRegistrar.disassociate(name, [self.getProfileName()])
  
  def associateDevice(self, name, index=None):
    '''
    Associates a L{AEOutput} or L{AEInput} device with the active profile.
    Does not load it immediately. See L{refreshDevices} for that functionality.
    
    @param name: Name of the UIE to associate
    @type name: string
    @param index: Load order index of the device where lower means sooner, None
      means last.
    @type index: integer
    '''
    UIRegistrar.associate(name, [self.getProfileName()], index=index)
  
  def disassociateDevice(self, name):
    '''
    Disassociates a L{AEOutput} or L{AEInput} device from the active profile. 
    Does not unload it immediately. See L{refreshDevices} for that 
    functionality.
    
    @param name: Name of the UIE to associate
    @type name: string
    '''
    UIRegistrar.disassociate(name, [self.getProfileName()])

  def refreshDevices(self):
    '''
    Unloads all existing devices then immediately loads all devices associated
    with the active profile.
    '''
    self.device_man.unloadDevices()
    self.device_man.loadDevices()
    
  def setTempVal(self, name, value):
    '''
    Stores the given value under the given name. The data will be available via
    L{getTempVal} to all L{Task}s executing or updating in response to the 
    current L{AEEvent}. This includes cascading L{Task.EventTask}s, chained 
    L{Task.InputTask}s with names, etc. Once the event has been handled by all
    L{Task}s, all stored information is discarded automatically.
    
    The name must be an immutable object. The value can be an arbitrary object.
    Only the value from the most recent call with a given name is stored.
    
    @param name: Name to associate with the value
    @type name: immutable
    @param value: Any value to store
    @type value: object
    '''
    self.tier.setTempVal(name, value)

  def getTempVal(self, name):
    '''
    Gets the value previously stored under the given name using L{setTempVal}.
    See that method for details.
    
    If no value has been stored under the given name, None is returned.

    @param name: Name of the stored data
    @type name: immutable
    @return: Value stored under the given name or None
    @rtype: object
    '''
    try:
      return self.tier.getTempVal(name)
    except KeyError:
      return None

  def registerConstants(self, scope, *names):
    '''
    Adds new constants to L{AEConstants} such that they are visible to other
    L{Perk}s. If any of the named constants do not exist, an exception will
    be raised. If any of the named constants are already registered, they will
    be silently ignored by L{AEConstants.register}.
    
    @param scope: Scope in which the constants are defined (e.g. globals())
    @type scope: dictionary
    @param names: Names of constants to register
    @type names: list of string
    @raise KeyError: When one of the named constants does not exist in the 
      scope
    '''
    AEConstants.register(dict(((name, scope[name]) for name in names)))
     
  def getPerkByName(self, name):
    '''
    Gets the L{Perk} with the given name.
    
    If no perk is registered under the given name, KeyError is raised.

    @param name: Name of the perk
    @type name: string
    @return: Perk with the given name
    @rtype: object
    @raise KeyError:  When the perk with the given name is not found
    '''
    for perk in self.tier.getPerks():
      if perk.getClassName() == name:
        return perk
    raise KeyError
    
  def getPerkVar(self, perk_name, var_name):
    '''
    Gets the L{Perk} instance variable with the given name.
    
    If no perk is registered under the given name a KeyError is raised
    if the perk does not contain var_name an AttributeError is raised.

    @param perk_name: Name of the perk
    @type perk_name: immutable
    @param var_name: Name of the instance variable
    @type var_name: immutable
    @return: perk variable with the given name
    @rtype: object
    @raise KeyError:  When the perk or variable with it's given name 
      is not found
    @raise AttributeError:  When attribute is not found
    is not found
    '''
    perk = self.getPerkByName(perk_name)
    return getattr(perk, var_name)
  
  def setPerkVar(self, perk_name, var_name, value):
    '''
    Sets the L{Perk} instance variable with the given name to value.
    
    If no perk is registered under the given name a KeyError is raised
    if the perk does not contain var_name an AttributeError is raised.

    @param perk_name: Name of the perk
    @type perk_name: immutable
    @param var_name: Name of the instance variable
    @type var_name: immutable
    @param value: value to set instance variable
    @type value: immutable
    @return: perk variable with the given name
    @rtype: object
    @raise KeyError:  When the perk or variable with it's given name 
    @raise AttributeError:  When attribute is not found
    '''
    perk = self.getPerkByName(perk_name)
    setattr(perk, var_name, value)
  
  def getPerkSetting(self, setting_name, perk_name=None):
    '''
    Gets the L{Perk} L{Setting} state variable with the given name.
    
    If no perk is registered under the given name or L{setting_name} is not
    found, KeyError is raised.

    @param perk_name: Name of the perk
    @type perk_name: immutable
    @param setting_name: Name of the perk
    @type setting_name: immutable
    @return: L{Setting} object for given perk.
    @rtype: object
    @raise KeyError:  When the perk is not found
    '''
    if perk_name is None:
      perk_name = self.perk.getClassName()
    state = self.getPerkState(perk_name, reload=False)
    return state.getSettingObj(setting_name)
  
  def getPerkSettingVal(self, setting_name, perk_name=None):
    '''
    Gets the value of the L{Perk} L{Setting} state variable with the given 
    name.
    
    If no perk is registered under the given name or if L{setting_name} is not 
    found, KeyError is raised.

    @param perk_name: Name of the perk
    @type perk_name: immutable
    @param setting_name: Name of the perk
    @type setting_name: immutable
    @return: value contained in L{AEState} attribute with given name
    @rtype: object
    @raise KeyError:  When the perk is not found
    '''
    if perk_name is None:
      perk_name = self.perk.getClassName()
    state = self.getPerkState(perk_name, reload=False)
    return state.getSettingVal(setting_name)
  
  def setPerkSettingVal(self, setting_name, value, perk_name=None):
    '''
    Sets the value of the L{Perk} L{Setting} state variable with the given 
    name.
    
    If no perk is registered under the given name or if L{setting_name} is not 
    found, KeyError is raised.

    @param perk_name: Name of the perk
    @type perk_name: immutable
    @param setting_name: Name of the perk
    @type setting_name: immutable
    @param value: value contained in L{AEState} attribute with given name
    @param value: object
    @raise KeyError:  When the L{Perk} is not found
    '''
    if perk_name is None:
      perk_name = self.perk.getClassName()
    state = self.getPerkState(perk_name, reload=False)
    state.setSettingVal(setting_name, value)
    
  def getHistory(self):
    '''
    Gets the zipped contents of L{getTaskHistory} and L{getPerkHistory}. This
    method does not involve extra processing because the history is already
    zipped in the L{Tier}. In fact, the other methods require the extra work
    of unzipping.
    
    The history is ordered from the first L{Task} to respond to an event to
    the most recent L{Task} to respond, including the current L{Task}.
    
    @return: List of L{Task} identities and L{Perk} class names
    @rtype: list of 2-tuple of string
    '''
    return self.tier.getHistory()
    
  def getTaskHistory(self):
    '''
    Gets the identities of all L{Task}s that have executed in response to the
    event currently being processed. This includes all chained L{Task}s and all
    L{Task}s executed using L{doTask} flattened into a linear history.
    
    If any L{Task} that executed did not have an identity, None is substituted
    in its place.
    
    The history is ordered from the first L{Task} to respond to an event to
    the most recent L{Task} to respond, including the current L{Task}.
    
    @return: Identities of the L{Task}s
    @rtype: list of string
    '''
    h = self.tier.getHistory()
    return [names[0] for names in h]
    
  def getPerkHistory(self):
    '''
    Gets the class names of all L{Perk}s that have executed in response to the
    event currently being processed. The contents of the list returned by this
    method have a one-to-one pairing with the list contents from 
    L{getTaskHistory}.
    
    The history is ordered from the first L{Perk} to respond to an event to
    the most recent L{Perk} to respond, including the current L{Perk}.
      
    @return: Class names of the L{Perk}s
    @rtype: list of string
    '''
    h = self.tier.getHistory()
    return [names[1] for names in h]
  
  def getAnchorTaskId(self):
    '''
    Gets the identity of the L{Task} which this one is executing before, after, 
    or around according to a previous chaining. If this L{Task} is not 
    executing as part of a chain, returns None.
    
    @return: Identity of the L{Task} or None
    @rtype: string
    '''
    try:
      return self.tier.getAnchor()[0]
    except TypeError:
      return None
  
  def getAnchorPerkClassName(self):
    '''
    Gets the class name of the L{Perk} owning the L{Task} that would be 
    returned by L{getAnchorTaskId}.
    
    @see: L{getAnchorTaskId}
    @return: Class name of the L{Perk} or None
    @rtype: string
    '''
    try:
      return self.tier.getAnchor()[1]
    except TypeError:
      return None

  def getPerkTaskNames(self, perk_name=None):
    '''
    Gets the names of all L{Task}s currently registered in the named L{Perk}.
    If no name is given, this L{Perk} is used. If the named L{Perk} is not 
    found, an exception is raised.
    
    @param perk_name: Name of the L{Perk} to query or None to mean this one
    @type perk_name: string
    @return: List of all registered L{Task} names
    @rtype: list of string
    @raise KeyError: When the named L{Perk} is not loaded
    '''
    if perk_name is None:
      return self.perk.getTaskNames()
    else:
      for perk in self.tier.getPerks():
        if perk.getClassName() == perk_name:
          return perk.getTaskNames()
      raise KeyError(perk_name)
  
  def getTierTaskNames(self):
    '''
    Gets the names of all L{Task}s currently registered in this L{Tier}.
    
    @return: List of all registered L{Task} names
    @rtype: list of string
    '''
    names = []
    for perk in self.tier.getPerks():
      names.extend(perk.getTaskNames())
    return names
