'''
Defines a hierarchy of L{Task.Tools} exceptions that are raised in tools
methods. A hierarchy of exceptions is used rather than a single exception with
various error codes to support the catching of a particular subtree of the
hierarchy, a single kind of exception, or all exceptions.

@author: Peter Parente
@author: Scott Haeger
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
from i18n import _

class StopTasks(Exception):
  '''
  Special exception that stops execution of all L{Task}s registered to handle
  the current L{AEEvent}. Once this exception is raised, L{Task}s left to 
  handle the event are updated rather than executed.
  '''
  pass

class ToolsError(Exception):
  '''
  Base class for all L{Task.Tools} exceptions. This base class should not
  be instantiated directly as it does not define its own error string. It only
  exists as a catch-all for its subclass exceptions.
  
  @cvar description: Localized error description to be reported to the user 
    whenthe Trap flag in L{TierManager.LSRState} is True.
  @type description: string
  '''
  description = None
  def __init__(self):
    '''
    Stores a blank description and None error code using the parent Python
    Exception constructor. 
    '''
    Exception.__init__(self, self.description)
    
class UnknownError(ToolsError):
  '''
  Exception for a unknown reasons.
  '''
  description = _('unknown task error')
  
class DeviceError(ToolsError):
  '''
  Exception subtree for all device errors.
  '''
  description = _('device error')
  
class InvalidDeviceError(DeviceError):
  '''
  Exception for a request for an invalid device.
  '''
  description = _('invalid device')
  
class InvalidStyleError(DeviceError):
  '''
  Exception for an invalid style.
  '''
  description = _('invalid style property')

class UndefinedKeyError(DeviceError):
  '''
  Exception for an undefined key constant failure.
  '''
  description = _('undefined key')

class CancelledKeyError(DeviceError):
  '''
  Exception for an incomplete key command registration failure.
  '''
  description = _('cancelled key')

class ActionError(ToolsError):
  '''
  Exception subtree for accessible action errors.
  '''
  description = _('action failed')
  
class FocusError(ActionError):
  '''
  Exception for failures setting the focus.
  '''
  description = _('set focus failed')
  
class SelectError(ActionError):
  '''
  Exception for failures setting the selection.
  '''
  description =  _('set selection failed')
  
class MouseError(ActionError):
  '''
  Exception for mouse event failures.
  '''
  description =  _('mouse event failed')
  
class CaretError(ActionError):
  '''
  Exception for failures setting the caret position.
  '''
  description =  _('set caret failed')
  
class TextError(ActionError):
  '''
  Exception for failures manipulating text.
  '''
  description = _('text action failed')
  
class PORError(ToolsError):
  '''
  Exception for L{POR} and related accessible failures.
  '''
  description = _('invalid point of regard')
