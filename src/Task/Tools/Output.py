'''
Defines L{Task.Tools} for doing output.

@author: Peter Parente
@author: Pete Brunet
@author: Brett Clippingdale
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
import os
import AEConstants
import Base, View
import AEOutput
from i18n import _
from Error import *

class Output(Base.TaskTools):
  '''
  Provides methods for doing output on any L{AEOutput} device.

  @cvar may_stop: Count of the number of calls to L{inhibitMayStop}. When 
    greater than zero, the next call to L{mayStop} will decrement the count.
    When zero, L{mayStop} will send a stop to the output device.
  @type may_stop: integer
  ''' 
  may_stop = 0

  def setPerkIdealOutput(self, *capabilities):
    '''
    Sets the ideal device capabilities to be used to locate a device for all 
    output methods invoked from the caller's L{Perk} module, including the 
    Perk class itself and all of its registered L{Task}s. The capabilities list
    should include strings naming L{AEOutput} interfaces ("audio" and/or 
    "braille") at present.
    
    @param capabilities: Names of capabilities required on the device
    @type capabilities: list of string    
    '''
    self.perk.setIdealOutput(*capabilities)
    self._changeDefaultOutput()
    
  def getPerkIdealOutput(self):
    '''        
    Gets the ideal device capabilities to locate a device to be used by all
    output methods invoked from the caller's L{Perk} module, including the Perk
    class itself and all of its registered L{Task}s.
    
    @return: Names of capabilities set as ideal for a default output device
    @rtype: list of string
    '''
    return self.perk.getIdealOutput()
  
  def getOutputDevice(self, name=None, *capabilities):
    '''
    Gets an L{AEOutput} device from the L{DeviceManager} given its name or
    its capabilities. The capabilities list may include strings naming 
    L{AEOutput} interfaces ("audio" and/or "braille" at present).
    
    If neither is specified, the first available output device in the
    L{DeviceManager} is returned.
    
    @param name: Class (UIE) name of the L{AEOutput} device to get
    @type name: string
    @param capabilities: Names of capabilities required on the device
    @type capabilities: list of string
    @return: The named output device
    @rtype: L{AEOutput}
    @raise InvalidDeviceError: When the specified L{AEInput} device is not 
      found
    '''
    if name is not None:
      device = self.device_man.getOutputByName(name)
    elif len(capabilities):
      device = self.device_man.getOutputByCaps(capabilities)
    else: 
      device = self.device_man.getFirstOutput()
    if device is None:
      # let the system report the device isn't available
      raise InvalidDeviceError
    return device
    
  def stopNow(self, dev=None, sem=None, layer=None, reset=True):
    '''
    Tells the referenced output device to interrupt current output and clear 
    buffered data.
    
    @param dev: The device to send the stop to; defaults to None for the 
      default output device
    @type dev: L{AEOutput.Base}
    @param sem: The semantic information to stop; defaults to None for the
      default semantic
    @type sem: integer
    @param layer: Layer on which the event occurred
    @type layer: integer
    @param reset: Reset the L{may_stop} count (True) or leave it as-is when
    sending the stop?
    @type reset: boolean
    '''
    dev = dev or self.def_out
    if reset:
      Output.may_stop = 0
    if not self.tier_man.getState().Stopping:
      return
    if layer is None:
      layer = self.layer
    self.device_man.sendStop(dev, sem, layer)
    
  def mayStop(self, dev=None, sem=None, layer=None):
    '''
    Stops the device if L{may_stop} is zero. When non-zero, decrements the
    count.

    @param dev: The device to send the stop to; defaults to None for the
      default output device
    @type dev: L{AEOutput.Base}
    @param sem: The semantic information to stop; defaults to None for the
      default semantic
    @type sem: integer
    @param layer: Layer on which the event occurred
    @type layer: integer
    @return: Was the stop processed (True) or inhibited (False)?
    @rtype: boolean
    '''
    dev = dev or self.def_out
    if layer is None:
      layer = self.layer
    if Output.may_stop == 0:
      self.stopNow(dev, sem, layer)
      return True
    else:
      Output.may_stop -= 1
      if Output.may_stop < 0:
        Output.may_stop = 0
      return False
      
  def inhibitMayStop(self):
    '''
    Prevents the next call to L{mayStop} from stopping the device provided to
    that call. Adds one to the L{may_stop} flag.
    '''
    Output.may_stop += 1
    
  def stopAll(self, dev=None):
    '''
    Tells the referenced output device to interrupt current output and clear 
    buffered data. Unlike L{stopNow}, this ignores the layer on which the 
    method was called and any semantic information. Everything on the device
    is stopped, even when L{TierManager.LSRState} stopping setting is False.
    
    @param dev: The device to send the stop to; defaults to None for the 
      default output device
    @type dev: L{AEOutput.Base}
    '''
    dev = dev or self.def_out
    self.device_man.sendStop(dev, None, None)
    
  def play(self, filename, sem=None, layer=None, dev=None, talk=True):
    '''
    Outputs the contents of a file. The device used to do the output gets to
    decide what to do with the file based on its contents and the semantic tag
    and event layer information given.

    @param filename: Name of the file to be output
    @type filename: string
    @param sem: The semantic stream on which to send output; defaults to None 
      for the default semantic.
    @type sem: integer
    @param layer: Layer on which the event occurred
    @type layer: integer
    @param dev: The device to send the filename to; defaults to None for the 
      default output device
    @type dev: L{AEOutput.Base}
    @param talk: Send a talk command immediately following the filename?
    @type talk: boolean
    '''
    dev = dev or self.def_out
    if layer is None:
      layer = self.layer
    if not os.path.exists(filename):
      filename = os.path.join(self.perk.getPath(), filename)
    self.device_man.sendFilename(dev, filename, sem, layer)
    if talk:
      self.device_man.sendTalk(dev, sem, layer)
  
  def say(self, text, por=None, sem=None, layer=None, dev=None, talk=True):
    '''
    Sends a string of text to a device that supports string display. If talk is
    False, the device should buffer the text until another method call occurs
    which sends a Talk to the device.

    @param text: Text to output
    @type text: string
    @param por: The point of regard of the start of text, or None when the text 
      is not from the screen
    @type por: L{POR}
    @param sem: The semantic stream on which to send output; defaults to None 
      for the default semantic.
    @type sem: integer
    @param layer: Layer on which the event occurred
    @type layer: integer
    @param dev: The device to send the text to; defaults to None for the 
      default output device
    @type dev: L{AEOutput.Base}
    @param talk: Send a talk command immediately following the string?
    @type talk: boolean
    '''
    dev = dev or self.def_out
    if layer is None:
      layer = self.layer
    self.device_man.sendString(dev, text, sem, layer, por)
    if talk:
      self.device_man.sendTalk(dev, sem, layer)
  
  def send(self, name, value, sem=None, layer=None, dev=None):
    '''
    Sends arbitrary data to a device. The device must recognize the name in
    order to decide what to do with the value data.
    
    This is a generic method which receives all content and commands to be
    rendered and executed on a device. Standard name identifiers should be used
    whenever possible. For instance, L{AEConstants.Output.CMD_STOP} and
    L{AEConstants.Output.CMD_TALK}. However, device specific names and values
    are certainly possible.
    
    Only use this method if no other output method is suitable for your 
    purposes.

    @param name: Descriptor of the data value sent
    @type name: object
    @param value: Content value
    @type value: object
    @param sem: The semantic stream on which to send output; defaults to None 
      for the default semantic.
    @type sem: integer
    @param layer: Layer on which the event occurred
    @type layer: integer
    @param dev: Device that should receive the command
    @type dev: L{AEOutput}
    @return: Return value specific to the given command
    @rtype: object
    '''
    dev = dev or self.def_out
    if layer is None:
      layer = self.layer
    rv = self.device_man.send(dev, name, value, sem, layer)
    return rv
  
  def getStyle(self, dev=None, sem=None, layer=None):
    '''    
    Gets the L{AEOutput.Style} object for the given device, semantic tag, and
    event layer. This is a low level method that allows direct access to
    L{AEOutput.Style} style objects.
  
    @see: L{getStyleSetting}
    @param dev: The device on which to get the value; defaults to None for the 
      default output device.
    @type dev: L{AEOutput.Base}
    @param sem: The semantic on which to get the value.
    @type sem: integer 
    @param layer: Layer on which to get the value, focus, tier, or background
    @type layer: integer
    @return: The style object or None if one does not exist
    @rtype: L{AEOutput.Style}
    '''
    dev = dev or self.def_out
    layer = layer or self.layer
    return self.device_man.getStyle(dev, sem, layer)
  
  def getStyleSetting(self, name, dev=None, sem=None, layer=None):
    '''
    Gets the L{AEOutput.Style} object for the given device, semantic tag, and
    event layer, then immediately fetches the L{AEState.Setting} object with 
    the given name. This is a low level method that allows direct access to
    L{AEState.Setting}s on the style. It is permissible to read/write the value
    property of settings fields and register for change notifications.
    
    @attention: If a method attached to a L{Perk} or L{Task} is used as a 
      callback for a style value change notification, be aware that the L{Perk}
      or L{Task} will NOT be pre-executed when the callback runs with all
      references needed by the L{Task.Tools}.
    
    @param name: Name of the setting object to fetch
    @type name: string
    @param dev: The device on which this setting should be changed. Defaults to 
      the default output device established by L{setPerkIdealOutput}.
    @type dev: L{AEOutput.Base}
    @param sem: The semantic on which to change the value. Defaults to None 
      meaning the default style
    @type sem: integer
    @param layer: Layer on which to set the value: focus, tier, or background.
      Defaults to the event layer that triggered this L{Task}.
    @type layer: integer
    @return: The setting object
    @rtype: L{AEState.Setting}
    @raise InvalidStyleError: When the style or the setting name is not valid
    '''
    s = self.getStyle(dev, sem, layer)
    try:
      return s.getSettingObj(name)
    except (TypeError, KeyError):
      raise InvalidStyleError

  def getStyleVal(self, name, dev=None, sem=None, layer=None):
    '''
    Gets the L{AEOutput.Style} object for the given device, semantic tag, and
    event layer, then immediately fetches the value of the L{AEState.Setting} 
    object with the given name. 
    
    @param name: Name of the setting value to fetch
    @type name: string
    @param dev: The device on which this setting should be changed. Defaults to 
      the default output device established by L{setPerkIdealOutput}.
    @type dev: L{AEOutput.Base}
    @param sem: The semantic on which to change the value. Defaults to None 
      meaning the default style
    @type sem: integer
    @param layer: Layer on which to set the value: focus, tier, or background.
      Defaults to the event layer that triggered this L{Task}.
    @type layer: integer
    @return: The setting value
    @rtype: object
    @raise InvalidStyleError: When the style or the setting name is not valid
    '''
    s = self.getStyle(dev, sem, layer)
    try:
      return s.getSettingVal(name)
    except (TypeError, KeyError):
      raise InvalidStyleError
    
  def setStyleVal(self, name, val, dev=None, sem=None, layer=None):
    '''
    Gets the L{AEOutput.Style} object for the given device, semantic tag, and
    event layer, then immediately sets the value of the L{AEState.Setting} 
    object with the given name. 
    
    @param name: Name of the setting value to fetch
    @type name: string
    @param val: The setting value
    @type val: object
    @param dev: The device on which this setting should be changed. Defaults to 
      the default output device established by L{setPerkIdealOutput}.
    @type dev: L{AEOutput.Base}
    @param sem: The semantic on which to change the value. Defaults to None 
      meaning the default style
    @type sem: integer
    @param layer: Layer on which to set the value: focus, tier, or background.
      Defaults to the event layer that triggered this L{Task}.
    @type layer: integer
    @raise InvalidStyleError: When the style or the setting name is not valid
    '''
    s = self.getStyle(dev, sem, layer)
    try:
      return s.setSettingVal(name,val )
    except (TypeError, KeyError):
      raise InvalidStyleError    

  ##############################################################################
  # The following say___() methods are autogenerated by EmitSay.py                           
  # These are autogenerated in full (rather than with dynamic code generation or
  # with other Python approaches, such as decorators) to allow epydoc to 
  # generate complete documentation for Perk writers
  ##############################################################################
  def sayTextAttrs(self, por=None, text=None, template=u'%s', 
                  dev=None, talk=True):
    '''
    Calls L{Task.Tools.View.View.getAccAllTextAttrs} and outputs the result.
  
    @param por: L{POR} pointing at an accessible whose value will be read or
      None when the L{task_por} should be used. The value is read only if the
      text parameter is None. 
    @type por: L{POR}
    @param text: Manually provided text fetched from the L{POR}
    @type text: string
    @param template: Template for text to be spoken. Defaults to %s indicating 
      the text or value retrieved from the L{POR} is spoken by itself.
    @type template: string
    @param dev: The device to use for output. Defaults to the default output 
      device established by L{setPerkIdealOutput}.
    @type dev: L{AEOutput.Base}
    @param talk: Send a L{AEConstants.Output.CMD_TALK} immediately after this 
      string is sent?
    @type talk: boolean
    @return: Was anything sent for output (True) or was text None (False)?
    @rtype: boolean
    '''
    if text is None:
      por = por or self.task_por
      attrs = self.getAccAllTextAttrs(por)
      if not attrs:
        return False
      text = ', '.join(attrs.items())
    if text is None:
      return False
    self.say(template % text, por, AEConstants.SEM_TEXT_ATTR, self.layer, dev,
             talk)
    return True
  
  def sayWord(self, por=None, text=None, template=u'%s', 
                  dev=None, talk=True):
    '''
    Calls L{Task.Tools.View.View.getWordText} and outputs the result.
  
    @param por: L{POR} pointing at an accessible whose value will be read or
      None when the L{task_por} should be used. The value is read only if the
      text parameter is None. 
    @type por: L{POR}
    @param text: Manually provided text fetched from the L{POR}
    @type text: string
    @param template: Template for text to be spoken. Defaults to %s indicating 
      the text or value retrieved from the L{POR} is spoken by itself.
    @type template: string
    @param dev: The device to use for output. Defaults to the default output 
      device established by L{setPerkIdealOutput}.
    @type dev: L{AEOutput.Base}
    @param talk: Send a L{AEConstants.Output.CMD_TALK} immediately after this 
      string is sent?
    @type talk: boolean
    @return: Was anything sent for output (True) or was text None (False)?
    @rtype: boolean
    '''
    if text is None:
      por = por or self.task_por
      text = self.getWordText(por)
    if text is None:
      return False
    self.say(template % text, por, AEConstants.SEM_WORD, self.layer, dev,
             talk)
    return True
  
  def sayChar(self, por=None, text=None, template=u'%s', 
                  dev=None, talk=True):
    '''
    Calls L{Task.Tools.View.View.getCharText} and outputs the result.
  
    @param por: L{POR} pointing at an accessible whose value will be read or
      None when the L{task_por} should be used. The value is read only if the
      text parameter is None. 
    @type por: L{POR}
    @param text: Manually provided text fetched from the L{POR}
    @type text: string
    @param template: Template for text to be spoken. Defaults to %s indicating 
      the text or value retrieved from the L{POR} is spoken by itself.
    @type template: string
    @param dev: The device to use for output. Defaults to the default output 
      device established by L{setPerkIdealOutput}.
    @type dev: L{AEOutput.Base}
    @param talk: Send a L{AEConstants.Output.CMD_TALK} immediately after this 
      string is sent?
    @type talk: boolean
    @return: Was anything sent for output (True) or was text None (False)?
    @rtype: boolean
    '''
    if text is None:
      por = por or self.task_por
      text = self.getCharText(por)
    if text is None:
      return False
    self.say(template % text, por, AEConstants.SEM_CHAR, self.layer, dev,
             talk)
    return True
  
  def sayName(self, por=None, text=None, template=u'%s', 
                  dev=None, talk=True):
    '''
    Calls L{Task.Tools.View.View.getAccName} and outputs the result.
  
    @param por: L{POR} pointing at an accessible whose value will be read or
      None when the L{task_por} should be used. The value is read only if the
      text parameter is None. 
    @type por: L{POR}
    @param text: Manually provided text fetched from the L{POR}
    @type text: string
    @param template: Template for text to be spoken. Defaults to %s indicating 
      the text or value retrieved from the L{POR} is spoken by itself.
    @type template: string
    @param dev: The device to use for output. Defaults to the default output 
      device established by L{setPerkIdealOutput}.
    @type dev: L{AEOutput.Base}
    @param talk: Send a L{AEConstants.Output.CMD_TALK} immediately after this 
      string is sent?
    @type talk: boolean
    @return: Was anything sent for output (True) or was text None (False)?
    @rtype: boolean
    '''
    if text is None:
      por = por or self.task_por
      text = self.getAccName(por)
    if text is None:
      return False
    self.say(template % text, por, AEConstants.SEM_NAME, self.layer, dev,
             talk)
    return True
  
  def sayLabel(self, por=None, text=None, template=u'%s', 
                  dev=None, talk=True):
    '''
    Calls L{Task.Tools.View.View.getAccLabel} and outputs the result.
  
    @param por: L{POR} pointing at an accessible whose value will be read or
      None when the L{task_por} should be used. The value is read only if the
      text parameter is None. 
    @type por: L{POR}
    @param text: Manually provided text fetched from the L{POR}
    @type text: string
    @param template: Template for text to be spoken. Defaults to %s indicating 
      the text or value retrieved from the L{POR} is spoken by itself.
    @type template: string
    @param dev: The device to use for output. Defaults to the default output 
      device established by L{setPerkIdealOutput}.
    @type dev: L{AEOutput.Base}
    @param talk: Send a L{AEConstants.Output.CMD_TALK} immediately after this 
      string is sent?
    @type talk: boolean
    @return: Was anything sent for output (True) or was text None (False)?
    @rtype: boolean
    '''
    if text is None:
      por = por or self.task_por
      text = self.getAccLabel(por)
    if text is None:
      return False
    self.say(template % text, por, AEConstants.SEM_LABEL, self.layer, dev,
             talk)
    return True
  
  def sayItem(self, por=None, text=None, template=u'%s', 
                  dev=None, talk=True):
    '''
    Calls L{Task.Tools.View.View.getItemText} and outputs the result.
  
    @param por: L{POR} pointing at an accessible whose value will be read or
      None when the L{task_por} should be used. The value is read only if the
      text parameter is None. 
    @type por: L{POR}
    @param text: Manually provided text fetched from the L{POR}
    @type text: string
    @param template: Template for text to be spoken. Defaults to %s indicating 
      the text or value retrieved from the L{POR} is spoken by itself.
    @type template: string
    @param dev: The device to use for output. Defaults to the default output 
      device established by L{setPerkIdealOutput}.
    @type dev: L{AEOutput.Base}
    @param talk: Send a L{AEConstants.Output.CMD_TALK} immediately after this 
      string is sent?
    @type talk: boolean
    @return: Was anything sent for output (True) or was text None (False)?
    @rtype: boolean
    '''
    if text is None:
      por = por or self.task_por
      text = self.getItemText(por)
    if text is None:
      return False
    self.say(template % text, por, AEConstants.SEM_ITEM, self.layer, dev,
             talk)
    return True
  
  def sayWindow(self, por=None, text=None, template=u'%s', 
                  dev=None, talk=True):
    '''
    Calls L{Task.Tools.View.View.getWindowTitle} and outputs the result.
  
    @param por: L{POR} pointing at an accessible whose value will be read or
      None when the L{task_por} should be used. The value is read only if the
      text parameter is None. 
    @type por: L{POR}
    @param text: Manually provided text fetched from the L{POR}
    @type text: string
    @param template: Template for text to be spoken. Defaults to %s indicating 
      the text or value retrieved from the L{POR} is spoken by itself.
    @type template: string
    @param dev: The device to use for output. Defaults to the default output 
      device established by L{setPerkIdealOutput}.
    @type dev: L{AEOutput.Base}
    @param talk: Send a L{AEConstants.Output.CMD_TALK} immediately after this 
      string is sent?
    @type talk: boolean
    @return: Was anything sent for output (True) or was text None (False)?
    @rtype: boolean
    '''
    if text is None:
      por = por or self.task_por
      text = self.getWindowTitle(por)
    if text is None:
      return False
    self.say(template % text, por, AEConstants.SEM_WINDOW, self.layer, dev,
             talk)
    return True
  
  def saySection(self, por=None, text=None, template=u'%s', 
                  dev=None, talk=True):
    '''
    Calls L{Task.Tools.View.View.getAccLabel} and outputs the result.
  
    @param por: L{POR} pointing at an accessible whose value will be read or
      None when the L{task_por} should be used. The value is read only if the
      text parameter is None. 
    @type por: L{POR}
    @param text: Manually provided text fetched from the L{POR}
    @type text: string
    @param template: Template for text to be spoken. Defaults to %s indicating 
      the text or value retrieved from the L{POR} is spoken by itself.
    @type template: string
    @param dev: The device to use for output. Defaults to the default output 
      device established by L{setPerkIdealOutput}.
    @type dev: L{AEOutput.Base}
    @param talk: Send a L{AEConstants.Output.CMD_TALK} immediately after this 
      string is sent?
    @type talk: boolean
    @return: Was anything sent for output (True) or was text None (False)?
    @rtype: boolean
    '''
    if text is None:
      por = por or self.task_por
      text = self.getAccLabel(por)
    if text is None:
      return False
    self.say(template % text, por, AEConstants.SEM_SECTION, self.layer, dev,
             talk)
    return True
  
  def sayApp(self, por=None, text=None, template=u'%s', 
                  dev=None, talk=True):
    '''
    Calls L{Task.Tools.View.View.getAppName} and outputs the result.
  
    @param por: L{POR} pointing at an accessible whose value will be read or
      None when the L{task_por} should be used. The value is read only if the
      text parameter is None. 
    @type por: L{POR}
    @param text: Manually provided text fetched from the L{POR}
    @type text: string
    @param template: Template for text to be spoken. Defaults to %s indicating 
      the text or value retrieved from the L{POR} is spoken by itself.
    @type template: string
    @param dev: The device to use for output. Defaults to the default output 
      device established by L{setPerkIdealOutput}.
    @type dev: L{AEOutput.Base}
    @param talk: Send a L{AEConstants.Output.CMD_TALK} immediately after this 
      string is sent?
    @type talk: boolean
    @return: Was anything sent for output (True) or was text None (False)?
    @rtype: boolean
    '''
    if text is None:
      por = por or self.task_por
      text = self.getAppName(por)
    if text is None:
      return False
    self.say(template % text, por, AEConstants.SEM_APP, self.layer, dev,
             talk)
    return True
  
  def sayFont(self, por=None, text=None, template=u'%s', 
                  dev=None, talk=True):
    '''
    Calls L{Task.Tools.View.View.getAccTextAttr} and outputs the value
    associated with the "family-name" property.
  
    @param por: L{POR} pointing at an accessible whose value will be read or
      None when the L{task_por} should be used. The value is read only if the
      text parameter is None. 
    @type por: L{POR}
    @param text: Manually provided text fetched from the L{POR}
    @type text: string
    @param template: Template for text to be spoken. Defaults to %s indicating 
      the text or value retrieved from the L{POR} is spoken by itself.
    @type template: string
    @param dev: The device to use for output. Defaults to the default output 
      device established by L{setPerkIdealOutput}.
    @type dev: L{AEOutput.Base}
    @param talk: Send a L{AEConstants.Output.CMD_TALK} immediately after this 
      string is sent?
    @type talk: boolean
    @return: Was anything sent for output (True) or was text None (False)?
    @rtype: boolean
    '''
    if text is None:
      por = por or self.task_por
      
      text = self.getAccTextAttr('family-name', por)
    if text is None:
      return False
    self.say(template % text, por, AEConstants.SEM_FONT, self.layer, dev,
             talk)
    return True
  
  def sayCount(self, por=None, text=None, template=u'%s', 
                  dev=None, talk=True):
    '''
    Calls L{Task.Tools.View.View.getAccCount} and outputs the result.
  
    @param por: L{POR} pointing at an accessible whose value will be read or
      None when the L{task_por} should be used. The value is read only if the
      text parameter is None. 
    @type por: L{POR}
    @param text: Manually provided text fetched from the L{POR}
    @type text: string
    @param template: Template for text to be spoken. Defaults to %s indicating 
      the text or value retrieved from the L{POR} is spoken by itself.
    @type template: string
    @param dev: The device to use for output. Defaults to the default output 
      device established by L{setPerkIdealOutput}.
    @type dev: L{AEOutput.Base}
    @param talk: Send a L{AEConstants.Output.CMD_TALK} immediately after this 
      string is sent?
    @type talk: boolean
    @return: Was anything sent for output (True) or was text None (False)?
    @rtype: boolean
    '''
    if text is None:
      por = por or self.task_por
      text = self.getAccCount(por)
    if text is None:
      return False
    self.say(template % text, por, AEConstants.SEM_COUNT, self.layer, dev,
             talk)
    return True
  
  def sayIndex(self, por=None, text=None, template=u'%s', 
                  dev=None, talk=True):
    '''
    Calls L{Task.Tools.View.View.getAccIndex} and outputs the result.
  
    @param por: L{POR} pointing at an accessible whose value will be read or
      None when the L{task_por} should be used. The value is read only if the
      text parameter is None. 
    @type por: L{POR}
    @param text: Manually provided text fetched from the L{POR}
    @type text: string
    @param template: Template for text to be spoken. Defaults to %s indicating 
      the text or value retrieved from the L{POR} is spoken by itself.
    @type template: string
    @param dev: The device to use for output. Defaults to the default output 
      device established by L{setPerkIdealOutput}.
    @type dev: L{AEOutput.Base}
    @param talk: Send a L{AEConstants.Output.CMD_TALK} immediately after this 
      string is sent?
    @type talk: boolean
    @return: Was anything sent for output (True) or was text None (False)?
    @rtype: boolean
    '''
    if text is None:
      por = por or self.task_por
      text = self.getAccIndex(por)
    if text is None:
      return False
    self.say(template % text, por, AEConstants.SEM_INDEX, self.layer, dev,
             talk)
    return True
  
  def sayColor(self, por=None, text=None, template=u'%s', 
                  dev=None, talk=True):
    '''
    Calls L{Task.Tools.View.View.getAccTextAttr} and outputs the value
    associated with the "fg-color" property.
  
    @param por: L{POR} pointing at an accessible whose value will be read or
      None when the L{task_por} should be used. The value is read only if the
      text parameter is None. 
    @type por: L{POR}
    @param text: Manually provided text fetched from the L{POR}
    @type text: string
    @param template: Template for text to be spoken. Defaults to %s indicating 
      the text or value retrieved from the L{POR} is spoken by itself.
    @type template: string
    @param dev: The device to use for output. Defaults to the default output 
      device established by L{setPerkIdealOutput}.
    @type dev: L{AEOutput.Base}
    @param talk: Send a L{AEConstants.Output.CMD_TALK} immediately after this 
      string is sent?
    @type talk: boolean
    @return: Was anything sent for output (True) or was text None (False)?
    @rtype: boolean
    '''
    if text is None:
      por = por or self.task_por
      
      text = self.getAccTextAttr('fg-color', por)
    if text is None:
      return False
    self.say(template % text, por, AEConstants.SEM_COLOR, self.layer, dev,
             talk)
    return True
  
  def sayRole(self, por=None, text=None, template=u'%s', 
                  dev=None, talk=True):
    '''
    Calls L{Task.Tools.View.View.getAccRoleName} and outputs the result.
  
    @param por: L{POR} pointing at an accessible whose value will be read or
      None when the L{task_por} should be used. The value is read only if the
      text parameter is None. 
    @type por: L{POR}
    @param text: Manually provided text fetched from the L{POR}
    @type text: string
    @param template: Template for text to be spoken. Defaults to %s indicating 
      the text or value retrieved from the L{POR} is spoken by itself.
    @type template: string
    @param dev: The device to use for output. Defaults to the default output 
      device established by L{setPerkIdealOutput}.
    @type dev: L{AEOutput.Base}
    @param talk: Send a L{AEConstants.Output.CMD_TALK} immediately after this 
      string is sent?
    @type talk: boolean
    @return: Was anything sent for output (True) or was text None (False)?
    @rtype: boolean
    '''
    if text is None:
      por = por or self.task_por
      text = self.getAccRoleName(por)
    if text is None:
      return False
    self.say(template % text, por, AEConstants.SEM_ROLE, self.layer, dev,
             talk)
    return True
  
  def sayLevel(self, por=None, text=None, template=u'%s', 
                  dev=None, talk=True):
    '''
    Calls L{Task.Tools.View.View.getAccLevel} and outputs the result.
  
    @param por: L{POR} pointing at an accessible whose value will be read or
      None when the L{task_por} should be used. The value is read only if the
      text parameter is None. 
    @type por: L{POR}
    @param text: Manually provided text fetched from the L{POR}
    @type text: string
    @param template: Template for text to be spoken. Defaults to %s indicating 
      the text or value retrieved from the L{POR} is spoken by itself.
    @type template: string
    @param dev: The device to use for output. Defaults to the default output 
      device established by L{setPerkIdealOutput}.
    @type dev: L{AEOutput.Base}
    @param talk: Send a L{AEConstants.Output.CMD_TALK} immediately after this 
      string is sent?
    @type talk: boolean
    @return: Was anything sent for output (True) or was text None (False)?
    @rtype: boolean
    '''
    if text is None:
      por = por or self.task_por
      text = self.getAccLevel(por)
    if text is None:
      return False
    self.say(template % text, por, AEConstants.SEM_LEVEL, self.layer, dev,
             talk)
    return True
  
  def sayStatus(self, por=None, text=None, template=u'%s', 
                  dev=None, talk=True):
    '''
    Calls L{Task.Tools.View.View.getItemText} and outputs the result.
  
    @param por: L{POR} pointing at an accessible whose value will be read or
      None when the L{task_por} should be used. The value is read only if the
      text parameter is None. 
    @type por: L{POR}
    @param text: Manually provided text fetched from the L{POR}
    @type text: string
    @param template: Template for text to be spoken. Defaults to %s indicating 
      the text or value retrieved from the L{POR} is spoken by itself.
    @type template: string
    @param dev: The device to use for output. Defaults to the default output 
      device established by L{setPerkIdealOutput}.
    @type dev: L{AEOutput.Base}
    @param talk: Send a L{AEConstants.Output.CMD_TALK} immediately after this 
      string is sent?
    @type talk: boolean
    @return: Was anything sent for output (True) or was text None (False)?
    @rtype: boolean
    '''
    if text is None:
      por = por or self.task_por
      text = self.getItemText(por)
    if text is None:
      return False
    self.say(template % text, por, AEConstants.SEM_STATUS, self.layer, dev,
             talk)
    return True
  
  def sayState(self, por=None, text=None, template=u'%s', 
                  dev=None, talk=True):
    '''
    Calls L{Task.Tools.View.View.getStateText} and outputs the result.
  
    @param por: L{POR} pointing at an accessible whose value will be read or
      None when the L{task_por} should be used. The value is read only if the
      text parameter is None. 
    @type por: L{POR}
    @param text: Manually provided text fetched from the L{POR}
    @type text: string
    @param template: Template for text to be spoken. Defaults to %s indicating 
      the text or value retrieved from the L{POR} is spoken by itself.
    @type template: string
    @param dev: The device to use for output. Defaults to the default output 
      device established by L{setPerkIdealOutput}.
    @type dev: L{AEOutput.Base}
    @param talk: Send a L{AEConstants.Output.CMD_TALK} immediately after this 
      string is sent?
    @type talk: boolean
    @return: Was anything sent for output (True) or was text None (False)?
    @rtype: boolean
    '''
    if text is None:
      por = por or self.task_por
      text = self.getStateText(por)
    if text is None:
      return False
    self.say(template % text, por, AEConstants.SEM_STATE, self.layer, dev,
             talk)
    return True
  
  def sayDesc(self, por=None, text=None, template=u'%s', 
                  dev=None, talk=True):
    '''
    Calls L{Task.Tools.View.View.getAccDesc} and outputs the result.
  
    @param por: L{POR} pointing at an accessible whose value will be read or
      None when the L{task_por} should be used. The value is read only if the
      text parameter is None. 
    @type por: L{POR}
    @param text: Manually provided text fetched from the L{POR}
    @type text: string
    @param template: Template for text to be spoken. Defaults to %s indicating 
      the text or value retrieved from the L{POR} is spoken by itself.
    @type template: string
    @param dev: The device to use for output. Defaults to the default output 
      device established by L{setPerkIdealOutput}.
    @type dev: L{AEOutput.Base}
    @param talk: Send a L{AEConstants.Output.CMD_TALK} immediately after this 
      string is sent?
    @type talk: boolean
    @return: Was anything sent for output (True) or was text None (False)?
    @rtype: boolean
    '''
    if text is None:
      por = por or self.task_por
      text = self.getAccDesc(por)
    if text is None:
      return False
    self.say(template % text, por, AEConstants.SEM_DESC, self.layer, dev,
             talk)
    return True
  
  def sayHotkey(self, por=None, text=None, template=u'%s', 
                  dev=None, talk=True):
    '''
    Calls L{Task.Tools.View.View.getAccClickKey} and outputs the 
    result after replacing <> characters.
  
    @param por: L{POR} pointing at an accessible whose value will be read or
      None when the L{task_por} should be used. The value is read only if the
      text parameter is None. 
    @type por: L{POR}
    @param text: Manually provided text fetched from the L{POR}
    @type text: string
    @param template: Template for text to be spoken. Defaults to %s indicating 
      the text or value retrieved from the L{POR} is spoken by itself.
    @type template: string
    @param dev: The device to use for output. Defaults to the default output 
      device established by L{setPerkIdealOutput}.
    @type dev: L{AEOutput.Base}
    @param talk: Send a L{AEConstants.Output.CMD_TALK} immediately after this 
      string is sent?
    @type talk: boolean
    @return: Was anything sent for output (True) or was text None (False)?
    @rtype: boolean
    '''
    if text is None:
      por = por or self.task_por
      text = self.getAccClickKey(por)
      # remove extra characters from the text; this will probably break hotkeys
      # that use < and >; can fix with a regex
      try:
        text = text.replace('<', '')
        text = text.replace('>', ', ')
      except AttributeError:
        pass
    if text is None:
      return False
    self.say(template % text, por, AEConstants.SEM_HOTKEY, self.layer, dev,
             talk)
    return True
  
  def sayError(self, por=None, text=None, template=u'%s', 
                  dev=None, talk=True):
    '''
    Outputs an LSR system error message. L{POR} ignored.
  
    @param por: L{POR} pointing at an accessible whose value will be read or
      None when the L{task_por} should be used. The value is read only if the
      text parameter is None. 
    @type por: L{POR}
    @param text: Manually provided text fetched from the L{POR}
    @type text: string
    @param template: Template for text to be spoken. Defaults to %s indicating 
      the text or value retrieved from the L{POR} is spoken by itself.
    @type template: string
    @param dev: The device to use for output. Defaults to the default output 
      device established by L{setPerkIdealOutput}.
    @type dev: L{AEOutput.Base}
    @param talk: Send a L{AEConstants.Output.CMD_TALK} immediately after this 
      string is sent?
    @type talk: boolean
    @return: Was anything sent for output (True) or was text None (False)?
    @rtype: boolean
    '''
    if text is None:
      por = por or self.task_por
  
    if text is None:
      return False
    self.say(template % text, por, AEConstants.SEM_ERROR, self.layer, dev,
             talk)
    return True
  
  def sayInfo(self, por=None, text=None, template=u'%s', 
                  dev=None, talk=True):
    '''
    Outputs an informational LSR system message. L{POR} ignored.
  
    @param por: L{POR} pointing at an accessible whose value will be read or
      None when the L{task_por} should be used. The value is read only if the
      text parameter is None. 
    @type por: L{POR}
    @param text: Manually provided text fetched from the L{POR}
    @type text: string
    @param template: Template for text to be spoken. Defaults to %s indicating 
      the text or value retrieved from the L{POR} is spoken by itself.
    @type template: string
    @param dev: The device to use for output. Defaults to the default output 
      device established by L{setPerkIdealOutput}.
    @type dev: L{AEOutput.Base}
    @param talk: Send a L{AEConstants.Output.CMD_TALK} immediately after this 
      string is sent?
    @type talk: boolean
    @return: Was anything sent for output (True) or was text None (False)?
    @rtype: boolean
    '''
    if text is None:
      por = por or self.task_por
  
    if text is None:
      return False
    self.say(template % text, por, AEConstants.SEM_INFO, self.layer, dev,
             talk)
    return True
  
  def sayConfirm(self, por=None, text=None, template=u'%s', 
                  dev=None, talk=True):
    '''
    Outputs a confirmation message for a user action. L{POR} ignored.
  
    @param por: L{POR} pointing at an accessible whose value will be read or
      None when the L{task_por} should be used. The value is read only if the
      text parameter is None. 
    @type por: L{POR}
    @param text: Manually provided text fetched from the L{POR}
    @type text: string
    @param template: Template for text to be spoken. Defaults to %s indicating 
      the text or value retrieved from the L{POR} is spoken by itself.
    @type template: string
    @param dev: The device to use for output. Defaults to the default output 
      device established by L{setPerkIdealOutput}.
    @type dev: L{AEOutput.Base}
    @param talk: Send a L{AEConstants.Output.CMD_TALK} immediately after this 
      string is sent?
    @type talk: boolean
    @return: Was anything sent for output (True) or was text None (False)?
    @rtype: boolean
    '''
    if text is None:
      por = por or self.task_por
  
    if text is None:
      return False
    self.say(template % text, por, AEConstants.SEM_CONFIRM, self.layer, dev,
             talk)
    return True
  
  def sayValue(self, por=None, text=None, template=u'%s', 
                  dev=None, talk=True):
    '''
    Calls L{Task.Tools.View.View.getAccFloatValue} and outputs the result.
  
    @param por: L{POR} pointing at an accessible whose value will be read or
      None when the L{task_por} should be used. The value is read only if the
      text parameter is None. 
    @type por: L{POR}
    @param text: Manually provided text fetched from the L{POR}
    @type text: string
    @param template: Template for text to be spoken. Defaults to %s indicating 
      the text or value retrieved from the L{POR} is spoken by itself.
    @type template: string
    @param dev: The device to use for output. Defaults to the default output 
      device established by L{setPerkIdealOutput}.
    @type dev: L{AEOutput.Base}
    @param talk: Send a L{AEConstants.Output.CMD_TALK} immediately after this 
      string is sent?
    @type talk: boolean
    @return: Was anything sent for output (True) or was text None (False)?
    @rtype: boolean
    '''
    if text is None:
      por = por or self.task_por
      text = self.getAccFloatValue(por)
    if text is None:
      return False
    self.say(template % text, por, AEConstants.SEM_VALUE, self.layer, dev,
             talk)
    return True
  
  def sayValueExtents(self, por=None, text=None, template=u'%s', 
                  dev=None, talk=True):
    '''
    Calls L{Task.Tools.View.View.getAccFloatValueExtents} and outputs the
    result.
  
    @param por: L{POR} pointing at an accessible whose value will be read or
      None when the L{task_por} should be used. The value is read only if the
      text parameter is None. 
    @type por: L{POR}
    @param text: Manually provided text fetched from the L{POR}
    @type text: string
    @param template: Template for text to be spoken. Defaults to %s indicating 
      the text or value retrieved from the L{POR} is spoken by itself.
    @type template: string
    @param dev: The device to use for output. Defaults to the default output 
      device established by L{setPerkIdealOutput}.
    @type dev: L{AEOutput.Base}
    @param talk: Send a L{AEConstants.Output.CMD_TALK} immediately after this 
      string is sent?
    @type talk: boolean
    @return: Was anything sent for output (True) or was text None (False)?
    @rtype: boolean
    '''
    if text is None:
      por = por or self.task_por
      text = self.getAccFloatValueExtents(por)
    if text is None:
      return False
    self.say(template % text, por, AEConstants.SEM_EXTENTS, self.layer, dev,
             talk)
    return True
    
  def setBrlCaretPosition(self, value, sem=None, layer=None, dev=None):
    '''
    Sets caret location on the device. 
  
    @param value: Caret position
    @type value: integer
    @param sem: The semantic stream on which to send output; defaults to None 
      for the default semantic.
    @type sem: integer
    @param layer: Layer on which the event occurred
    @type layer: integer
    @param dev: Device that should receive the command
    @type dev: L{AEOutput}
    '''
    self.send(AEConstants.CMD_CARET, value, sem=sem, \
              layer=layer, dev=dev)
    
  def setBrlTruncate(self, value, sem=None, layer=None, dev=None):
    '''
    Sets a two-item tuple (left, right) to the device indicating if a line of 
    text has been truncated either on the left or right side. 

    @param value: Two-item tuple of booleans (left, right) indicating trucation
    @type value: tuple
    @param sem: The semantic stream on which to send output; defaults to None 
      for the default semantic.
    @type sem: integer
    @param layer: Layer on which the event occurred
    @type layer: integer
    @param dev: Device that should receive the command
    @type dev: L{AEOutput}
    @return: Return value specific to the given command
    @rtype: object
    '''
    self.send(AEConstants.CMD_TRUNCATE, value, sem=sem, \
              layer=layer, dev=dev)
    
  def getBrlEllipsisSizes(self, sem=None, layer=None, dev=None):
    '''
    Gets a two-item tuple (left and right) containing the ellipsis length.
    
    @param sem: The semantic stream on which to send output; defaults to None 
      for the default semantic.
    @type sem: integer
    @param layer: Layer on which the event occurred
    @type layer: integer
    @param dev: Device that should receive the command
    @type dev: L{AEOutput}
    @return: (left, right) ellipsis length
    @rtype: tuple
    '''
    return self.send(AEConstants.CMD_GET_ELLIPSIS_SIZE, None, sem=sem, \
                     layer=layer, dev=dev)
    
  def getBrlMissingCellCount(self, sem=None, layer=None, dev=None):
    '''
    Gets the number of missing/broken cells defined by the 
    "missing cell string" user setting.

    @param sem: The semantic stream on which to send output; defaults to None 
      for the default semantic.
    @type sem: integer
    @param layer: Layer on which the event occurred
    @type layer: integer
    @param dev: Device that should receive the command
    @type dev: L{AEOutput}
    @return: Number of missing/broken cells
    @rtype: integer
    '''
    return self.send(AEConstants.CMD_GET_MISSINGCELL_COUNT, None, sem=sem, \
              layer=layer, dev=dev)
     
  def setMagGoto(self, value, sem=None, layer=None, dev=None):
    '''
    Sets the viewport of the magnifier

    @param value: viewport settings in the form (x1, y1, x2, y2)
    @type value: tuple
    @param sem: The semantic stream on which to send output; defaults to None 
      for the default semantic.
    @type sem: integer
    @param layer: Layer on which the event occurred
    @type layer: integer
    @param dev: Device that should receive the command
    @type dev: L{AEOutput}
    '''
    self.send(AEConstants.CMD_GOTO, value, sem=sem, layer=layer, dev=dev)
              
  def getMagROI(self, sem=None, layer=None, dev=None):
    '''
    Get the region of interest from magnifier device

    @param sem: The semantic stream on which to send output; defaults to None 
      for the default semantic.
    @type sem: integer
    @param layer: Layer on which the event occurred
    @type layer: integer
    @param dev: Device that should receive the command
    @type dev: L{AEOutput}
    @return: ROI as a four-item tuple in the form (x1, y1, x2, y2)
    @rtype: tuple
    '''
    return self.send(AEConstants.CMD_GET_ROI, None, sem=sem, \
              layer=layer, dev=dev)
  
  def getClosestLang(self, tag, sem=None, layer=None, dev=None):
    '''
    Get the closest language supported by the device to the one given by the
    IANA tag. See the L{AEOutput.Style.AudioStyle} comment about languages
    for help on formatting a proper IANA tag.

    @param tag: IANA tag name
    @type tag: string
    @param sem: Semantic tag indicating the kind of output; defaults to None 
      for the default semantic.
    @type sem: integer
    @param layer: Layer on which the event occurred
    @type layer: integer
    @param dev: Device that should receive the command
    @type dev: L{AEOutput}
    @return: IANA tag name closest to the one given or None meaning no 
      supported language makes sense
    @rtype: string
    '''
    return self.send(AEConstants.CMD_GET_CLOSEST_LANG, tag, sem=sem, \
                     layer=layer, dev=dev)
