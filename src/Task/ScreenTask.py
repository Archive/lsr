'''
Defines a L{Task} to execute when some aspective of an accessible's or item's
appearance on the screen changes.

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''

import Base, EventTask, AEConstants
import AEEvent

class ScreenTask(EventTask.EventTask):
  '''
  Executed when bounds or visible data changes.

  This class registers its name and whether it should be monitored by default 
  in an L{AEMonitor} using the L{Base.registerTaskType} function
  when this module is first imported. The L{AEMonitor} can use this
  information to build its menus.
  '''
  Base.registerTaskType('ScreenTask', False)

  def getEventType(self):
    '''
    @return: Type of L{AEEvent} this L{Task} wants to handle
    @rtype: class
    '''
    return AEEvent.ScreenChange
  
  def update(self, por, kind, layer, **kwargs):
    '''
    Updates this L{Task} in response to a consumed screen change event. Called 
    by L{Tier.Tier._executeTask}.
    
    @param por: Point of regard for the related accessible/item
    @type por: L{POR}
    @param kind: Indicates the kind of selection event
    @type kind: integer
    @param layer: Layer on which the event occurred
    @type layer: integer
    '''
    pass
  
  def execute(self, por, kind, layer, **kwargs):
    '''
    Executes this L{Task} in response to a screen change event. Called by 
    L{Tier.Tier._executeTask}.
    
    @param por: Point of regard for the related accessible/item
    @type por: L{POR}
    @param kind: Indicates the kind of selection event
    @type kind: integer
    @param layer: Layer on which the event occurred
    @type layer: integer
    @return: True to allow other L{Task}s to process this event
    @rtype: boolean
    '''
    if kind == AEConstants.SCREEN_OBJECT_BOUNDS:
      return self.executeResized(por=por, layer=layer, **kwargs)
    elif kind == AEConstants.SCREEN_TEXT_BOUNDS:
      return self.executeReflowed(por=por, layer=layer, **kwargs)
    else:
      return self.executeRefreshed(por=por, layer=layer, **kwargs)
      
  def executeResized(self, por, layer, **kwargs):
    '''
    Executes this L{Task} in response to a change in the bounds of an object.
    
    @param por: Point of regard for the related accessible/item
    @type por: L{POR}
    @param layer: Layer on which the event occurred
    @type layer: integer
    @return: True to allow other L{Task}s to process this event
    @rtype: boolean
    '''
    return True
  
  def executeReflowed(self, por, layer, **kwargs):
    '''
    Executes this L{Task} in response to a change in the bounds of a body of
    text.
    
    @param por: Point of regard for the related accessible/item
    @type por: L{POR}
    @param layer: Layer on which the event occurred
    @type layer: integer
    @return: True to allow other L{Task}s to process this event
    @rtype: boolean
    '''
    return True
  
  def executeRefreshed(self, por, layer, **kwargs):
    '''
    Executes this L{Task} in response to a change in the visible data on an
    object.
    
    @param por: Point of regard for the related accessible/item
    @type por: L{POR}
    @param layer: Layer on which the event occurred
    @type layer: integer
    @return: True to allow other L{Task}s to process this event
    @rtype: boolean
    '''
    return True