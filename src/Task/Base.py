'''
Defines the base class for all L{Task}s.

@author: Peter Parente
@author: Larry Weiss
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
import Tools, UIElement, UIRegistrar

default_types = []
all_types = []

def registerTaskType(name, default):
  '''
  Called once by every L{Task} class to register itself for buffering and 
  filtering by an L{AEMonitor}.
  
  @param name: Name of the L{Task}
  @type name: string
  @param default: Should this L{Task} be buffered by default?
  @type default: boolean
  '''
  global default_types
  global all_types
  all_types.append(name)
  if default:
    default_types.append(name)
    
def getDefaults():
  '''
  Suggests the default L{Task}s to be monitored.
  
  @return: Names of defaults to monitor
  @rtype: list of string
  '''
  return default_types  
  
def getNames():
  '''
  Gets the names of all the L{Task} types.
  
  @return: List of all L{Task} names
  @rtype: list of string
  '''
  return all_types

class Task(Tools.All, UIElement.UIE):
  '''
  Most base class for all L{Task}s that execute in a L{Perk}. Stores a 
  string identifying a particular class instance. Implements L{UIElement}
  to provide information to the user about its purpose even though it is not
  managed by the L{UIRegistrar}.
  
  @ivar ident: Programmatic identifier for this L{Task}
  @type ident: string
  ''' 
  def __init__(self, ident):
    '''
    Stores the L{Task} identifier.
    
    @param ident: Programmatic identifier
    @type ident: string
    '''
    Tools.All.__init__(self)
    self.ident = ident
    
  def execute(self, **kwargs):
    '''
    Executes this L{Task} in response to an event. Called by 
    L{Tier.Tier._executeTask}.
    
    @param kwargs: Dictionary containing parameters passed to a task.
    @type kwargs: dictionary 
    @return: Should processing continue? Always returns True by default.
    @rtype: boolean
    '''
    return True
  
  def update(self, **kwargs):
    '''
    Allows this L{Task} to update itself in response to an event that was 
    consumed by another L{Task} that executed before it. Called by 
    L{Tier.Tier._executeTask}.
    
    @param kwargs: Dictionary containing parameters passed to a task.
    @type kwargs: dictionary 
    '''
    pass
  
  def getIdentity(self):
    '''
    Gets the unique string identity assigned to this L{Task}.
    
    @return: Identity of this L{Task}
    @rtype: string
    @raise ValueError: When L{ident} is None, meaning this L{Task} has no 
      unique identity
    '''
    if self.ident is None:
      raise ValueError
    return self.ident
  
  def getPerk(self):
    '''
    Gets the L{Perk} in which this L{Task} was registered.
    
    @return: Reference to the L{Perk}
    @rtype: L{Perk}
    '''
    return self.perk
  
  def getPath(self):
    '''
    @return: Absolute path location of the module containing the implementor
      of this interface
    @rtype: string
    '''
    return UIRegistrar.getPath(self.perk.getClassName())
