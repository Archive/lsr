'''
Defines a L{Task} to execute when the selector changes.

@author: Pete Brunet
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''

import Base, EventTask, AEConstants
import AEEvent

class SelectorTask(EventTask.EventTask):
  '''
  Executed when a selector change occurs. 

  This class registers its name and whether it should be monitored by default
  in an L{AEMonitor} using the L{Base.registerTaskType} function when this
  module is first imported. The L{AEMonitor} can use this information to build
  its menus.  
  ''' 
  Base.registerTaskType('SelectorTask', True)

  def getEventType(self):
    '''
    @return: Type of L{AEEvent} this L{Task} wants to handle
    @rtype: class
    '''
    return AEEvent.SelectorChange
  
  def update(self, por, text, layer, kind, **kwargs):
    '''
    Update this L{Task} in response to a consumed selector change event. Called 
    by L{Tier.Tier._executeTask}.
    
    @param text: The accessible text or name of the item at the POR
    @type text: string
    @param por: Point of regard for the related accessible
    @type por: L{POR}
    @param kind: Indicates the kind of selection event
    @type kind: integer
    @param layer: Layer on which the event occurred
    @type layer: integer
    '''
    pass
  
  def execute(self, por, text, layer, kind, **kwargs):
    '''
    Executes this L{Task} in response to a selector change event. Called by 
    L{Tier.Tier._executeTask}.
    
    @param text: The accessible text or name of the item at the POR
    @type text: string
    @param por: Point of regard for the related accessible
    @type por: L{POR}
    @param layer: Layer on which the event occurred
    @type layer: integer
    @param kind: Indicates the kind of selection event
    @type kind: integer
    @return: Should processing continue? Always returns True by default.
    @rtype: boolean
    '''
    if kind == AEConstants.EVENT_ACTIVE_ITEM_SELECT:
      return self.executeActive(por=por, text=text, layer=layer, **kwargs)
    elif kind == AEConstants.EVENT_ADD_ITEM_SELECT:
      return self.executeAdded(por=por, text=text, layer=layer, **kwargs)
    elif kind == AEConstants.EVENT_REMOVE_ITEM_SELECT:
      return self.executeRemoved(por=por, text=text, layer=layer, **kwargs)
    elif kind == AEConstants.EVENT_CHANGE_TEXT_SELECT:
      return self.executeText(por=por, text=text, layer=layer, **kwargs)
  
  def executeActive(self, por, text, layer, **kwargs):
    '''
    Executes this L{Task} in response to an active selection change event.
    Called by L{Tier.Tier._executeTask}.
    
    @param text: The accessible text or name of the item at the POR
    @type text: string
    @param por: Point of regard for the related accessible
    @type por: L{POR}
    @param layer: Layer on which the event occurred
    @type layer: integer
    @return: Should processing continue? Always returns True by default.
    @rtype: boolean
    '''
    pass
  
  def executeText(self, por, text, layer, **kwargs):
    '''
    Executes this L{Task} in response to an addition to the current selection.
    Called by L{Tier.Tier._executeTask}.
    
    @param text: The accessible text or name of the item at the POR
    @type text: string
    @param por: Point of regard for the related accessible
    @type por: L{POR}
    @param layer: Layer on which the event occurred
    @type layer: integer
    @return: Should processing continue? Always returns True by default.
    @rtype: boolean
    '''
    pass
  
  def executeAdded(self, por, text, layer, **kwargs):
    '''
    Executes this L{Task} in response to a removal from the current selection.
    Called by L{Tier.Tier._executeTask}.
    
    @param text: The accessible text or name of the item at the POR
    @type text: string
    @param por: Point of regard for the related accessible
    @type por: L{POR}
    @param layer: Layer on which the event occurred
    @type layer: integer
    @return: Should processing continue? Always returns True by default.
    @rtype: boolean
    '''
    pass
  
  def executeRemoved(self, por, text, layer, **kwargs):
    '''
    Executes this L{Task} in response to a removal from the current selection.
    Called by L{Tier.Tier._executeTask}.
    
    @param text: The accessible text or name of the item at the POR
    @type text: string
    @param por: Point of regard for the related accessible
    @type por: L{POR}
    @param layer: Layer on which the event occurred
    @type layer: integer
    @return: Should processing continue? Always returns True by default.
    @rtype: boolean
    '''
    pass
 