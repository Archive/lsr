'''
Defines a L{Task} to execute when a timed interval passes.

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
import Base

class TimerTask(Base.Task):
  '''
  Executes when a timed interval passes. The interval should not be changed
  once this L{Task} has been registered. Only the interval set at registration
  time will be honored.

  This class registers its name and whether it should be monitored by default
  in an L{AEMonitor} using the L{Base.registerTaskType}
  function when this module is first imported. The L{AEMonitor}
  can use this information to build its menus.  
  
  @ivar interval: Interval for the timer in seconds
  @type interval: integer
  '''
  Base.registerTaskType('TimerTask', False)
  
  def __init__(self, ident, interval):
    '''
    Stores the desired interval.
    
    @note: Changed from second to millisecond resolution
    
    @param interval: Interval for the timer in milliseconds
    @type interval: integer
    @param ident: Programmatic identifier
    @type ident: string
    '''
    Base.Task.__init__(self, ident)
    self.interval = interval
  
  def getTimerInterval(self):
    '''
    @return: Interval for this timer
    @rtype: integer
    '''
    return self.interval
  
  def execute(self, interval, layer, **kwargs):
    '''
    Executes this L{Task} in response to a timer event. Called by
    L{Tier.Tier._executeTask}.
    
    @param interval: Interval in seconds on which the timer fires
    @type interval: integer
    @param layer: Layer on which the event occurred
    @type layer: integer
    '''
    pass
