'''
Defines a Task to execute when the focus changes.

@author: Peter Parente
@author: Pete Brunet
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''

import Base, EventTask
import AEEvent

class FocusTask(EventTask.EventTask):
  '''
  Executed when a focus change occurs. 
  
  This class registers its name and whether it should be monitored by default
  in an L{AEMonitor} using the L{Base.registerTaskType} function when this
  module is first imported. The L{AEMonitor} can use this information to build
  its menus.
  '''
  Base.registerTaskType('FocusTask', True)
  
  def getEventType(self):
    '''
    @return: Type of L{AEEvent} this L{Task} wants to handle
    @rtype: class
    '''
    return AEEvent.FocusChange
  
  def update(self, por, gained, layer, **kwargs):
    '''
    Updates this L{Task} in response to a consumed focus change event. Called 
    by L{Tier.Tier._executeTask}.
    
    @param por: Point of regard related at which the event occurred
    @type por: L{POR}
    @param gained: Was focus gained (True) or lost (False)
    @type gained: boolean
    @param layer: Layer on which the event occurred
    @type layer: integer
    '''
    pass
  
  def execute(self, por, gained, layer, **kwargs):
    '''
    Executes this L{Task} in response to a focus change event. Called by 
    L{Tier.Tier._executeTask}.
    
    @param por: Point of regard related at which the event occurred
    @type por: L{POR}
    @param gained: Was focus gained (True) or lost (False)
    @type gained: boolean
    @param layer: Layer on which the event occurred
    @type layer: integer
    @return: True to allow other L{Task}s to process this event
    @rtype: boolean
    '''
    if gained:
      return self.executeGained(por=por, layer=layer, **kwargs)
    else:
      return self.executeLost(por=por, layer=layer, **kwargs)
  
  def executeGained(self, por, layer, **kwargs):
    '''
    Executes this L{Task} in response to a focus gained event. Called by 
    L{execute}.
    
    @param por: Focused point of regard
    @type por: L{POR}
    @param layer: Layer on which the event occurred
    @type layer: integer
    @return: True to allow other L{Task}s to process this event
    @rtype: boolean
    '''
    return True
  
  def executeLost(self, por, layer, **kwargs):
    '''
    Executes this L{Task} in response to a focus lost event. Called by 
    L{execute}.
    
    @param por: Unfocused point of regard
    @type por: L{POR}
    @param layer: Layer on which the event occurred
    @type layer: integer
    @return: True to allow other L{Task}s to process this event
    @rtype: boolean
    '''
    return True
