'''
Defines a L{Task} to execute when the mouse pointer moves or a mouse button
changes state.

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''

import Base, EventTask, AEConstants
import AEEvent

class MouseTask(EventTask.EventTask):
  '''
  Executed when a mouse moves or mouse buttons are pressed.

  This class registers its name and whether it should be monitored by default
  in an L{AEMonitor} using the L{Base.registerTaskType} function when this
  module is first imported. The L{AEMonitor} can use this information to build
  its menus.
  '''
  Base.registerTaskType('MouseTask', False)

  def getEventType(self):
    '''
    @return: Type of L{AEEvent} this L{Task} wants to handle
    @rtype: class
    '''
    return AEEvent.MouseChange
  
  def update(self, kind, button, pos, **kwargs):
    '''    
    Updates this L{Task} in response to a consumed mouse event. Called by
    L{Tier.Tier._executeTask}.
    
    @param kind: Kind of event
    @type kind: integer
    @param pos: Absolute position of the mouse pointer
    @type pos: 2-tuple of integer
    @param button: Number of the button pressed
    @type button: integer
    '''
    pass
  
  def execute(self, kind, pos, button, **kwargs):
    '''
    Executes this L{Task} in response to a mouse event. Called by 
    L{Tier.Tier._executeTask}.
    
    @param kind: Kind of event
    @type kind: integer
    @param pos: Absolute position of the mouse pointer
    @type pos: 2-tuple of integer
    @param button: Number of the button pressed or released
    @type button: integer
    @return: True to allow other L{Task}s to process this event
    @rtype: boolean
    '''
    if kind == AEConstants.EVENT_MOUSE_MOVE:
      return self.executeMoved(pos=pos, **kwargs)
    elif kind == AEConstants.EVENT_MOUSE_PRESS:
      return self.executePressed(button=button, **kwargs)
    else:
      return self.executeReleased(button=button, **kwargs)
      
  def executeMoved(self, pos, **kwargs):
    '''
    Executes this L{Task} in response to a mouse movement.
    
    @param pos: Absolute position of the mouse pointer
    @type pos: 2-tuple of integer
    @return: True to allow other L{Task}s to process this event
    @rtype: boolean
    '''
    return True
  
  def executePressed(self, button, **kwargs):
    '''
    Executes this L{Task} in response to a mouse button press.
    
    @param button: Number of the button pressed
    @type button: integer
    @return: True to allow other L{Task}s to process this event
    @rtype: boolean
    '''
    return True
  
  def executeReleased(self, button, **kwargs):
    '''
    Executes this L{Task} in response to a mouse button release.
    
    @param button: Number of the button released
    @type button: integer
    @return: True to allow other L{Task}s to process this event
    @rtype: boolean
    '''
    return True
