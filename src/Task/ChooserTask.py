'''
Defines a L{Task} to execute when a L{AEEvent.InputGesture} occurs in order to
start an L{AEChooser} dialog with the user.

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
import Base, InputTask

class ChooserTask(InputTask.InputTask):
  '''
  Executes when an input gesture occurs or a chooser changes state.
  
  This class registers its name and whether it should be monitored by default
  in an L{AEMonitor} using the L{Base.registerTaskType} function when this
  module is first imported. The L{AEMonitor} can use this information to build
  its menus.
  '''
  Base.registerTaskType('ChooserTask', False)
  
  def execute(self, chooser=None, kind=None, **kwargs):
    '''
    Executes this L{Task} in response to some change in a L{AEChooser}, 
    starting it, ending it, or applying its current options. Called by 
    L{Tier.Tier._executeTask}.
    
    @param chooser: Chooser that fired the event
    @type chooser: L{AEChooser}
    @param kind: Kind of signal, APPLY, OK, or CANCEL or an arbitrary
    constant provided by the L{AEChooser}
    @type kind: integer
    @return: Should processing continue? Always returns True by default.
    @rtype: boolean
    '''
    if chooser is None:
      return self.executeStart(**kwargs)
    elif kind in (chooser.OK, chooser.CANCEL):
      # call signal then end
      # return the AND of the return values
      update = self.executeSignal(chooser=chooser, kind=kind, **kwargs)
      end = self.executeEnd(chooser=chooser, kind=kind, **kwargs)
      return end and update
    else:
      # call signal
      return self.executeSignal(chooser=chooser, kind=kind, **kwargs)     

  def executeStart(self, **kwargs):
    '''
    Executes this L{Task} to start a new L{AEChooser}, typically in response
    to input on an L{AEInput} device. Called by L{Tier.Tier._executeTask}.
    
    @return: Should processing continue? Always returns True by default.
    @rtype: boolean
    '''
    return True
  
  def executeSignal(self, chooser, kind, **kwargs):
    '''  
    Executes this L{Task} in response to a chooser request to have its current
    options immediately applied without its completion. Called by
    L{Tier.Tier._executeTask}.
    
    @param chooser: Chooser that fired the event
    @type chooser: L{AEChooser}
    @param kind: Kind of signal, APPLY, OK, or CANCEL or an arbitrary
    constant provided by the L{AEChooser}
    @type kind: integer
    @param kwargs: Arbitrary keyword arguments returned by the chooser
    @type kwargs: dictionary
    '''
    return True
  
  def executeEnd(self, chooser, **kwargs):
    '''
    Executes this L{Task} in response to the ending of a chooser dialog. 
    Called by L{Tier.Tier._executeTask}.

    The default implementation calls L{Task.Tools.System.System.unloadChooser}
    which is the desired behavior for most L{ChooserTask}s.

    @param chooser: Chooser that fired the event
    @type chooser: L{AEChooser}
    @param kwargs: Arbitrary keyword arguments returned by the chooser
    @type kwargs: dictionary
    '''
    self.unloadChooser(chooser)
    return True
