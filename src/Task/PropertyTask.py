'''
Defines a L{Task} to execute when a text or numeric property changes.

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''

import Base, EventTask, AEConstants
import AEEvent

class PropertyTask(EventTask.EventTask):
  '''
  Executed when a text or numeric property change occurs. 

  This class registers its name and whether it should be monitored by default
  in an L{AEMonitor} using the L{Base.registerTaskType} function when this
  module is first imported. The L{AEMonitor} can use this information to build
  its menus.
  '''
  Base.registerTaskType('PropertyTask', False)

  def getEventType(self):
    '''
    @return: Type of L{AEEvent} this L{Task} wants to handle
    @rtype: class
    '''
    return AEEvent.PropertyChange
  
  def update(self, por, name, value, layer, **kwargs):
    '''
    Updates this L{Task} in response to a consumed property change event. Called
    by L{Tier.Tier._executeTask}.
    
    @param por: Point of regard for the related accessible
    @type por: L{POR}
    @param name: Name of the property that changed
    @type name: string
    @param value: New value of the property
    @type value: string or number
    @param layer: Layer on which the event occurred
    @type layer: integer
    '''
    pass
  
  def execute(self, por, name, value, layer, **kwargs):
    '''
    Executes this L{Task} in response to a property change event. Called by 
    L{Tier.Tier._executeTask}.
    
    @param por: Point of regard for the related accessible
    @type por: L{POR}
    @param name: Name of the property that changed
    @type name: string
    @param value: New value of the property
    @type value: string or number
    @param layer: Layer on which the event occurred
    @type layer: integer
    @return: True to allow other L{Task}s to process this event
    @rtype: boolean
    '''
    return True