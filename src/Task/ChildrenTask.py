'''
Defines a L{Task} to execute when a child has been added/removed to an 
accessible.

@author: Brett Clippingdale
@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''

import Base, EventTask
import AEEvent

class ChildrenTask(EventTask.EventTask):
  '''
  Executed on add/remove of an accessible's children. 
  
  This class registers its name and whether it should be monitored by default
  in an L{AEMonitor} using the L{Base.registerTaskType} function when this
  module is first imported. The L{AEMonitor} can use this information to build
  its menus.
  '''
  Base.registerTaskType('ChildrenTask', True)
  
  def getEventType(self):
    '''
    @return: Type of L{AEEvent} this L{Task} wants to handle
    @rtype: class
    '''
    return AEEvent.ChildrenChange
  
  def update(self, por, added, child_por, layer, **kwargs):
    '''
    Updates this L{Task} in response to a consumed children change event. 
    Called by L{Tier.Tier._executeTask}.
    
    @param por: The L{POR} for the related accessible
    @type por: L{POR}
    @param added: True when a child is added, False when removed
    @type added: boolean
    @param child_por: The L{POR} of added/removed child
    @type child_por: L{POR}
    @param layer: Layer on which the event occurred
    @type layer: integer
    '''
    pass
  
  def execute(self, por, added, child_por, layer,**kwargs):
    '''
    Executes this L{Task} in response to a children change event. Called by 
    L{Tier.Tier._executeTask}.
    
    @param por: The L{POR} for the related accessible
    @type por: L{POR}
    @param added: True when a child is added, False when removed
    @type added: boolean
    @param child_por: The L{POR} of added/removed child
    @type child_por: L{POR}
    @param layer: Layer on which the event occurred
    @type layer: integer
    @return: True to allow other L{Task}s to process this event
    @rtype: boolean
    '''

    if added  == True:
      return self.executeChildAdded(por=por, child_por=child_por, layer=layer)
    else:
      return self.executeChildRemoved(por=por, child_por=child_por, layer=layer)
  
  def executeChildAdded(self, por, child_por, layer, **kwargs):
    '''
    Executes this L{Task} in response to a ChildrenChange-added event. Called by 
    L{execute}.
    
    @param por: The L{POR} for the related accessible
    @type por: L{POR}
    @param child_por: The L{POR} of added/removed child
    @type child_por: L{POR}
    @param layer: Layer on which the event occurred
    @type layer: integer
    '''
    return True

  def executeChildRemoved(self, por, child_por, layer, **kwargs):
    '''
    Executes this L{Task} in response to a ChildrenChange-removed event. Called 
    by L{execute}.
    
    @param por: The L{POR} for the related accessible
    @type por: L{POR}
    @param child_por: The L{POR} of added/removed child
    @type child_por: L{POR}
    @param layer: Layer on which the event occurred
    @type layer: integer
    '''
    return True
  