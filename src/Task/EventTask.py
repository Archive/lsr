'''
Defines the base class for all L{Task}s.

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
import Base

class EventTask(Base.Task):
  '''
  Base class for all L{Task}s that execute in response to L{AEEvent}s.
  
  The focus, tier, and background parameters specify on which layer
  the L{Task} will handle events. If focus is True, the L{Task} will be
  executed in response to an event from a focused control within this L{Tier}.
  If tier is True, the L{Task} will be executed in response to an event from an
  unfocused control within this L{Tier}. If background is True, the L{Task}
  will be executed in response to an event from any control within the tier
  when the L{Tier} is not active.
    
  The three layers are mutually exclusive. You may set any combination of
  focus, tier, and background to True to register the given L{Task} on each
  selected layer in one call. If all three parameters are False, the
  registration defaults to the focus layer.
  
  @ivar layers: Execute on focused events, unfocused events when the L{Tier} 
  is active, and/or events when the L{Tier} is inactive?
  @type layers: 3-tuple of boolean
  ''' 
  def __init__(self, ident, focus=False, tier=False, background=False, 
               all=False):
    '''
    Stores layer information.
    
    @param ident: Programmatic identifier
    @type ident: string
    @param focus: Execute for events in focus?
    @type focus: boolean
    @param tier: Execute for unfocused events when the L{Tier} is active?
    @type tier: boolean
    @param background: Execute for events when the L{Tier} is inactive?
    @type background: boolean
    '''
    Base.Task.__init__(self, ident)
    if all:
      self.layers = (True, True, True)
    else:
      self.layers = (focus, tier, background)
    
  def getEventType(self):
    '''
    @return: Type of event this L{Task} wants to handle
    @rtype: L{AEEvent} class
    @raise NotImplementedError: When a subclass does not define the type of 
      L{AEEvent} it wants to handle
    '''
    raise NotImplementedError
  
  def getEventLayers(self):
    '''
    @return: Event layer information
    @rtype: 3-tuple of boolean
    '''
    return self.layers
  