'''
Defines a L{Task} to execute when the accessible hierarchy changes.

@author: Brett Clippingdale
@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''

import Base, EventTask
import AEEvent

class TableTask(EventTask.EventTask):
  '''
  Executed when a table change occurs (insert/delete/reorder of a row/column). 

  This class registers its name and whether it should be monitored by default
  in an L{AEMonitor} using the L{Base.registerTaskType} function when this
  module is first imported. The L{AEMonitor} can use this information to build
  its menus. 
  '''
  Base.registerTaskType('TableTask', False)

  def getEventType(self):
    '''
    @return: Type of L{AEEvent} this L{Task} wants to handle
    @rtype: class
    '''
    return AEEvent.TableChange
  
  def update(self, por, is_row, added, first_child_por, last_child_por, layer, 
             **kwargs):
    '''
    Updates this L{Task} in response to a consumed table change event. 
    Called by L{Tier.Tier._executeTask}.
    
    @param por: The L{POR} for the related accessible
    @type por: L{POR}
    @param is_row: True if a table row, False if a table column
    @type is_row: boolean
    @param added: True when a row/column is added, False when removed, None when
                  reordered
    @type added: boolean
    @param first_child_por: The L{POR} of first inserted/deleted row/column
    @type first_child_por: L{POR}
    @param last_child_por: The L{POR} of last inserted/deleted row/column
    @type last_child_por: L{POR}
    @param layer: Layer on which the event occurred
    @type layer: integer
    '''
    pass
  
  def execute(self, por, is_row, added, first_child_por, last_child_por, layer,
              **kwargs):
    '''
    Executes this L{Task} in response to a hierarchy change event. Called by 
    L{Tier.Tier._executeTask}.
    
    @param por: The L{POR} for the related accessible
    @type por: L{POR}
    @param is_row: True if a table row, False if a table column
    @type is_row: boolean
    @param added: True when a row/column is added, False when removed, None when
                  reordered
    @type added: boolean
    @param first_child_por: The L{POR} of first inserted/deleted row/column
    @type first_child_por: L{POR}
    @param last_child_por: The L{POR} of last inserted/deleted row/column
    @type last_child_por: L{POR}
    @param layer: Layer on which the event occurred
    @type layer: integer
    @return: True to allow other L{Task}s to process this event
    @rtype: boolean
    '''

    if is_row == True: #row
      if added == True: #row-inserted
        return self.executeTableRowInserted(por=por, 
                              first_child_por=first_child_por,
                              last_child_por=last_child_por,
                              layer=layer)
      elif added == False: #row-deleted
        return self.executeTableRowDeleted(por=por, 
                              first_child_por=first_child_por,
                              last_child_por=last_child_por,
                              layer=layer)
      else: #row-reordered
        return self.executeTableRowReordered(por=por, layer=layer)
      
    else: # column
      if added == True: #column-inserted
        return self.executeTableColumnInserted(por=por, 
                              first_child_por=first_child_por,
                              last_child_por=last_child_por,
                              layer=layer)
      elif added == False: #column-deleted
        return self.executeTableColumnDeleted(por=por, 
                              first_child_por=first_child_por,
                              last_child_por=last_child_por,
                              layer=layer)
      else: #column-reordered
        return self.executeTableColumnReordered(por=por, layer=layer)

  
  def executeTableRowInserted(self, por, first_child_por, last_child_por, \
                         layer, **kwargs):
    '''
    Executes this L{Task} in response to a row-inserted table change event. 
    Called by L{execute}.
        
    @param por: The L{POR} for the related accessible
    @type por: L{POR}
    @param first_child_por: The L{POR} of first inserted row
    @type first_child_por: L{POR}
    @param last_child_por: The L{POR} of last inserted row
    @type last_child_por: L{POR}
    @param layer: Layer on which the event occurred
    @type layer: integer
    '''
    return True
  
  def executeTableRowDeleted(self, por, first_child_por, last_child_por, \
                         layer, **kwargs):
    '''
    Executes this L{Task} in response to a row-deleted table change event. 
    Called by L{execute}.
        
    @param por: The L{POR} for the related accessible
    @type por: L{POR}
    @param first_child_por: The L{POR} of first deleted row
    @type first_child_por: L{POR}
    @param last_child_por: The L{POR} of last deleted row
    @type last_child_por: L{POR}
    @param layer: Layer on which the event occurred
    @type layer: integer
    '''
    return True
  
  def executeTableRowReordered(self, por, layer, **kwargs):
    '''
    Executes this L{Task} in response to a row-reordered table change event. 
    Called by L{execute}.
        
    @param por: The L{POR} for the related accessible
    @type por: L{POR}
    @param layer: Layer on which the event occurred
    @type layer: integer
    '''
    return True
  
  def executeTableColumnInserted(self, por, first_child_por, last_child_por, \
                         layer, **kwargs):
    '''
    Executes this L{Task} in response to a column-inserted table change event. 
    Called by L{execute}.
        
    @param por: The L{POR} for the related accessible
    @type por: L{POR}
    @param first_child_por: The L{POR} of first inserted column
    @type first_child_por: L{POR}
    @param last_child_por: The L{POR} of last inserted column
    @type last_child_por: L{POR}
    @param layer: Layer on which the event occurred
    @type layer: integer
    '''
    return True
  
  def executeTableColumnDeleted(self, por, first_child_por, last_child_por, \
                         layer, **kwargs):
    '''
    Executes this L{Task} in response to a column-deleted table change event. 
    Called by L{execute}.
        
    @param por: The L{POR} for the related accessible
    @type por: L{POR}
    @param first_child_por: The L{POR} of first deleted column
    @type first_child_por: L{POR}
    @param last_child_por: The L{POR} of last deleted column
    @type last_child_por: L{POR}
    @param layer: Layer on which the event occurred
    @type layer: integer
    '''
    return True 
  
  def executeTableColumnReordered(self, por, layer, **kwargs):
    '''
    Executes this L{Task} in response to a column-reordered table change event. 
    Called by L{execute}.
        
    @param por: The L{POR} for the related accessible
    @type por: L{POR}
    @param layer: Layer on which the event occurred
    @type layer: integer
    '''
    return True