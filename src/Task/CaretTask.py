'''
Defines a Task to execute when the caret changes.

@author: Pete Brunet
@author: Larry Weiss
@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
import Base, EventTask
import AEEvent

class CaretTask(EventTask.EventTask):
  '''
  Executed when a caret change occurs. 
  
  This class registers its name and whether it should be monitored by default
  in an L{AEMonitor} using the L{Base.registerTaskType} function when this
  module is first imported. The L{AEMonitor} can use this information to build
  its menus.
  '''
  Base.registerTaskType('CaretTask', True)
  
  def getEventType(self):
    '''
    @return: Type of L{AEEvent} this L{Task} wants to handle
    @rtype: class
    '''
    return AEEvent.CaretChange
  
  def update(self, por, text, text_offset, added, layer, **kwargs):
    '''
    Updates this L{Task} in response to a consumed caret change event. Called by 
    L{Tier.Tier._executeTask}.
    
    @param por: Point of regard for the related accessible
    @type por: L{POR}
    @param text: The text inserted, deleted or the line of the caret
    @type text: string
    @param text_offset: The offset of the inserted/deleted text or the absolute 
      offset when movement only
    @type text_offset: integer
    @param added: True when text added, False when text deleted, and None 
      (the default) when event is for caret movement only
    @type added: boolean
    @param layer: Layer on which the event occurred
    @type layer: integer
    '''
    pass
  
  def execute(self, por, text, text_offset, added, layer, **kwargs):
    '''
    Executes this L{Task} in response to a caret change event. Called by 
    L{Tier.Tier._executeTask}.
    
    @param por: Point of regard for the related accessible
    @type por: L{POR}
    @param text: The text inserted, deleted or the line of the caret
    @type text: string
    @param text_offset: The offset of the inserted/deleted text or the absolute 
      offset when movement only
    @type text_offset: integer
    @param added: True when text added, False when text deleted, and None 
      (the default) when event is for caret movement only
    @type added: boolean
    @param layer: Layer on which the event occurred
    @type layer: integer
    @return: True to allow other L{Task}s to process this event
    @rtype: boolean
    '''
    if added is None:
      return self.executeMoved(por=por, text=text, text_offset=text_offset, 
                               layer=layer, **kwargs)
    elif added:
      return self.executeInserted(por=por, text=text, text_offset=text_offset, 
                                  layer=layer, **kwargs)
    else:
      return self.executeDeleted(por=por, text=text, text_offset=text_offset, 
                                 layer=layer, **kwargs)
  
  def executeInserted(self, por, text, text_offset, layer, **kwargs):
    '''
    Executes this L{Task} in response to a caret insert event. Called by 
    L{execute}.
    
    @param por: Point of regard for the related accessible
    @type por: L{POR}
    @param text: The text inserted, deleted or the line of the caret
    @type text: string
    @param text_offset: The offset of the inserted/deleted text or the absolute 
      offset when movement only
    @type text_offset: integer
    @param layer: Layer on which the event occurred
    @type layer: integer
    @return: True to allow other L{Task}s to process this event
    @rtype: boolean
    '''
    return True
    
  def executeMoved(self, por, text, text_offset, layer, **kwargs):
    '''    
    Executes this L{Task} in response to a caret movement event. Called by
    L{execute}.
    
    @param por: Point of regard for the related accessible
    @type por: L{POR}
    @param text: The text inserted, deleted or the line of the caret
    @type text: string
    @param text_offset: The offset of the inserted/deleted text or the absolute 
      offset when movement only
    @type text_offset: integer
    @param layer: Layer on which the event occurred
    @type layer: integer
    @return: True to allow other L{Task}s to process this event
    @rtype: boolean
    '''
    return True
    
  def executeDeleted(self, por, text, text_offset, layer, **kwargs):
    '''
    Executes this L{Task} in response to a caret delete event. Called by 
    L{execute}.
    
    @param por: Point of regard for the related accessible
    @type por: L{POR}
    @param text: The text inserted, deleted or the line of the caret
    @type text: string
    @param text_offset: The offset of the inserted/deleted text or the absolute 
      offset when movement only
    @type text_offset: integer
    @param layer: Layer on which the event occurred
    @type layer: integer
    @return: True to allow other L{Task}s to process this event
    @rtype: boolean
    '''
    return True