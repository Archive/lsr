'''
Defines the L{AccessEngine} class which runs the main program loop.

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
import gobject, bonobo
import EventManager, ViewManager, TierManager, DeviceManager, SettingsManager
import time, signal, logging
from i18n import _

log = logging.getLogger('AccessEngine')

class AccessEngine(object):
  '''
  Runs the main bonobo loop so that AT-SPI events are received and GUI monitors
  function properly. Allows clients to register for callbacks from the main loop
  on a set interval. Maintains a dictionary of managers that can be loaned out
  to L{Task.Tools} objects
  
  @ivar callbacks: Callback IDs for functions to invoke on timers or idle using
    gobject
  @type callbacks: dictionary
  @ivar vm: Reference to the L{ViewManager}
  @type vm: L{ViewManager}
  @ivar tm: Reference to the L{TierManager}
  @type tm: L{TierManager}
  @ivar em: Reference to the L{EventManager}
  @type em: L{EventManager}
  @ivar dm: Reference to the L{DeviceManager}
  @type dm: L{DeviceManager}
  @ivar sm: Reference to the L{SettingsManager}
  @type sm: L{SettingsManager}
  @ivar manager_dict: Dictionary of managers keyed by manager names (device_man,
    event_man, tier_man, and view_man)
  @type manager_dict: dictionary
  @ivar none_dict: Dictionary of None values with the same keys as 
    L{manager_dict}
  @type none_dict: dictionary
  @ivar profile: Name of the profile this instance of L{AccessEngine} was
      started under
  @type profile: string
  @ivar welcome: Output an introductory message when the engine starts?
  @type welcome: boolean
  '''
  def __init__(self, profile, welcome):
    '''
    Creates the L{ViewManager}, L{TierManager}, and L{EventManager} objects.
    Calls the init method here to intialize the managers with references to
    other managers.

    @param profile: Name of the profile LSR was executed under
    @type profile: string
    @param welcome: Output an introductory message?
    @type welcome: boolean
    '''
    self.profile = profile
    self.callbacks = {}
    self.alive = False
    self.manager_dict = None
    self.none_dict = None
    self.welcome = welcome
    
    # build all the managers (include reference to AccessEngine
    self.sm = SettingsManager.SettingsManager(self.profile)
    self.em = EventManager.EventManager(self)
    self.vm = ViewManager.ViewManager(self)
    self.tm = TierManager.TierManager(self)
    self.dm = DeviceManager.DeviceManager(self)

  def _init(self, **managers):
    '''
    Calls init on each manager providing it with references to the other
    managers in case it wants to hold one or more references. Called implicitly
    by L{run}.
    
    @keyword event_man: Reference to the L{EventManager}
    @type event_man: L{EventManager}
    @keyword view_man: Reference to the L{ViewManager}
    @type view_man: L{ViewManager}
    @keyword tier_man: Reference to the L{TierManager}
    @type tier_man: L{TierManager}
    @keyword device_man: Reference to the L{DeviceManager}
    @type device_man: L{DeviceManager}
    @keyword sett_man: Reference to the L{SettingsManager}
    @type sett_man: L{SettingsManager}
    '''
    # store the dictionary of all managers so we can quickly loan them out in 
    # the future to other objects that need them
    self.manager_dict = managers
    # create a dictionary with the same keys, but None values so we can quickly
    # unloan managers in the future
    self.none_dict = dict([(name, None) for name in managers.keys()])
    # init the managers, in correct dependency order    
    self.sm.init(**managers)
    self.em.init(**managers)
    self.vm.init(**managers)
    self.tm.init(**managers)
    self.dm.init(**managers)
    
    # have the device manager speak a welcome message
    if self.welcome:
      self.dm.sendWelcome()
    # get rid of any events that leak through while we are starting up
    self.em.flushEvents()
    # tell the view manager to locate the active view and inform the tier 
    # manager about running applications
    self.vm.initViews()
    
  def _close(self):
    '''
    Shutsdown all managers. Called implicitly by L{run} when the main loop 
    ends. Sends an exiting message to the default device.
    '''
    if self.welcome:
      self.dm.sendGoodbye()
    self.tm.close()
    self.vm.close()
    self.em.close()
    self.sm.close()
    self.dm.close()
    
  def quit(self, *args):
    '''    
    Quits the gtk main loop. Extra parameters are ignored but captured by args
    so that this method may be reigstered as a signal handler callback.
    '''
    try:
      bonobo.main_quit()
    except RuntimeError:
      # probably already quitting
      return

  def loanManagers(self, obj):
    '''
    Updates the namespace dictionary of the given object with the names and 
    values of the managers stored in L{manager_dict}. These references can be
    wiped out by a later call to L{unloanManagers}.
    
    @param obj: An object with a __dict__ attribute
    @type obj: object
    '''
    obj.__dict__.update(self.manager_dict)
  
  def unloanManagers(self, obj):
    '''
    Updates the namespace dictionary of the given object with the names and 
    None values of stored in L{none_dict}. This effectively wipes out references
    to managers previously loaned out by L{loanManagers}.
    
    @param obj: An object with a __dict__ attribute
    @type obj: object
    '''
    obj.__dict__.update(self.none_dict)

  def addTimer(self, callback, ms=None, *args):
    '''
    Creates a timer that calls the given callback on the interval specified by
    the given milliseconds. If ms is None, registers for callbacks on idle.

    @param callback: Function to call
    @type callback: callable
    @param args: Additional arguments to be given to the callback
    @type args: list
    '''
    if ms is not None:
      cid = gobject.timeout_add(ms, callback, *args)
    else:
      cid = gobject.idle_add(ms, callback, *args)
    self.callbacks[callback] = cid

  def removeTimer(self, callback):
    '''
    Ceases calls to the given registered callback.

    @param callback: Function to stop calling
    @type callback: callable
    @raise KeyError: When the callback is not already registered
    '''
    cid = self.callbacks[callback]
    gobject.source_remove(cid)

  def run(self):
    '''
    Runs an event loop by creating a gobject.MainLoop object and calling the
    iteration method of its gobject.MainContext. Makes the timer callbacks
    registered via L{AccessEngine.addTimer} on each iteration of the loop.
    '''
    # set the alive flag to true
    self.alive = True
    
    # register a signal handler for gracefully killing the loop
    signal.signal(signal.SIGINT, self.quit)
    signal.signal(signal.SIGTERM, self.quit)
    
    # initialize all managers
    self._init(event_man=self.em, view_man=self.vm, tier_man=self.tm,
               device_man=self.dm, sett_man=self.sm)

    # run the bonobo main loop, but make sure to release the GIL to allow other
    # threads
    def releaseGIL():
      time.sleep(1e-5)
      return True
    
    i = gobject.idle_add(releaseGIL)
    bonobo.main()
    gobject.source_remove(i)
    
    # cleanup callbacks
    for c in self.callbacks:
      self.removeTimer(c)
    
    # uninitialize all managers
    self._close()

    # state that we are trying to quit
    log.info('exiting...')
    # make sure any other threads get a chance to run to completion
    time.sleep(1)
    
  def getProfile(self):
    '''
    Gets the name of the profile this L{AccessEngine} instance is running 
    under.
    
    @return: Name of the profile
    @rtype: string
    '''
    return self.profile
