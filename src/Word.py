'''
Defines classes and functions for parsing bodies of text to find words and 
prepare them for output to the user.

The top-level functions in this module are optimized to build L{Word}s from
bodies of text containing more than a single L{Word}. A chunking scheme based
on the average length of words in the English language reduces the number of
calls to L{Word.append} and generally outperforms single character at a time
processing (at least for English text).

@var VOWELS: Vowels in the used to determine if a word can be spoken
@type VOWELS: string

@author: Peter Parente
@author: Larry Weiss
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
import unicodedata as ud
import AEState
from POR import POR
from AEConstants import WORD_NON_BLANK, WORD_ALPHABETIC, WORD_ALPHA_NUMERIC, \
     WORD_ALPHA_PUNCT, WORD_ALPHA_NUMERIC_PUNCT, WORD_LAST
from i18n import _

# define vowels if there are any in a language
VOWELS = _('AEIOUYaeiouy')

class WordState(AEState.AEState):
  '''
  Settings for L{Word} parsing. This class contains the set of all settings 
  that will be respected by the parser. Subclasses may override these settings

  The following variables are not truly instance variables, but are proxied
  by L{AEState.Setting} objects.

  Caps (bool): When set to True, capitalization is preserved when presenting
  text. Defaults to True.

  MaxRepeat (integer): Specifies the minimum number of times a character must 
  be found in sequence before it is considered a repeat. Defaults to 4.

  WordDef (enum): Set to NON_BLANK to define the main part of a word to only
  include non-blank characters. Set to ALPHABETIC to define the main part 
  of a word to only include alphabetic characters. Set to ALPHA_NUMERIC to
  define the main part of a word to only include alphabetic and numeric
  characters. Set to ALPHA_NUMERIC_PUNCT to define the main part of a word
  to only include characters that are alphabetic, numeric, or punctuation.
  Defaults to NON_BLANK.

  Ignore (string): Set to a string of characters that should be treated as if 
  they were blank. Defaults to the NBSP character.
  '''
  def __init__(self):
    '''
    Initializes all settings objects.
    '''
    super(WordState, self).__init__()
    self._initWordSettings()
      
  def _initWordSettings(self):
    '''
    Creates all settings for L{Word} parsing.
    '''
    self.newBool('Caps', True, _('Preserve caps?'), 
                 _('When set, capitalization is preserved in the output. '
                   'Otherwise, all strings are lowercased.'))
    self.newNumeric('MaxRepeat', 4, _('Maximum repeat'), 1, 100, 0, 
                    _('Defines the maximum number of times a '
                      'character should be allowed to repeat without the word '
                      'being spelled.'))
    self.newEnum('WordDef', WORD_NON_BLANK, _('Word definition'), 
                 {_('Non-blank') : WORD_NON_BLANK,
                  _('Alphanumeric and punct.') : WORD_ALPHA_NUMERIC_PUNCT,
                  _('Alphanumeric') : WORD_ALPHA_NUMERIC,
                  _('Alpha and punct.') : WORD_ALPHA_PUNCT,
                  _('Alphabetic') : WORD_ALPHABETIC},
                 _('Defines the characters that comprise a word.'))
    self.newString('Ignore', u'\u00a0', _('Ignored characters'), 
                   _('Defines the characters to be treated as blanks.'))

class DefaultWordState(object):
  '''
  Dummy L{WordState} look-alike used by L{getContextFromString} as a default 
  set of settings when no state object is specified. Defines all the same
  attributes as L{WordState}, but without instantiating unecessary settings.
  '''
  Caps = True
  MaxRepeat = 4
  WordDef = WORD_NON_BLANK
  Ignore = u'\u00a0'

def getContextFromString(string, por, state=DefaultWordState):
  '''  
  Gets the previous, current, and next L{Word}s relative to the given L{POR}. 
  If any word is missing, a None value is returned in its place. The string is 
  considered to be at the zero offset of the Item indicated by the L{POR}. Uses
  a default L{WordState} object if none is provided.
  
  @param string: Text to parse for words
  @type string: string
  @param state: Settings used to define a word
  @type state: L{WordState}  
  @param por: Point of regard indicating the source accessible and Item for the
    string
  @return: Previous, current, and next words surrounding the L{POR}
  @rtype: 3-tuple of L{Word}
  '''
  words = buildWordsFromString(string, por, state)
  c_off = por.char_offset
  # handle degenerate cases of no words or one word
  if len(words) == 0:
    return None, por, None
  elif len(words) == 1:
    return None, words[0], None
  # handle 0 to len(words)-2 cases by searching
  for i, w in enumerate(words):
    w_off = w.getPOR().char_offset
    if w_off > c_off:
      if i == 1:
        # the por is in the first word
        return None, words[0], words[1]
      else:
        return words[i-2], words[i-1], words[i]
  # handle the case when the por is in the last word
  return words[i-1], words[i], None

def buildWordsFromString(string, por=None, state=DefaultWordState, 
                         main_ob=None, trail_ob = None):
  '''
  Parses the given string to build a list of L{Word}s using the given state and
  the given L{POR}. When no L{POR} is given a dummy POR is constructed. Each
  L{Word} constructed will use the provided or constructed L{POR} to indicate 
  it's position as if the string was from the same component and Item. The 
  character offset from the given or constructed L{POR} is not used. The string
  is always considered to be at the zero offset of the Item indicated by the 
  L{POR}. Uses a default L{WordState} object if none is provided.
  
  @param string: Text to parse for words
  @type string: string
  @param state: System settings used to define a word
  @type state: L{WordState}  
  @param por: Point of regard indicating the source accessible and Item for the
    string
  @type por: L{POR}
  @param main_ob: Function to invoke for each character in the main part of a 
    word
  @type main_ob: callable
  @param trail_ob: Function to invoke for each character in the trailing part
    of a word
  @type trail_ob: callable
  @return: L{Word}s parsed from the string
  @rtype: list of L{Word}
  '''
  # keep track of the number of characters parsed so far
  count = 0
  # initialize an empty list
  words = []
  if por is None:
    # build a default POR when none provided
    por = POR(None, None, 0)
  else:
    # else use the accessible and item info from the given POR
    por = POR(por.accessible, por.item_offset, 0)
  # build a first, empty word
  w = Word(state, por, main_ob, trail_ob)
  # split the source text into chunks the size of the average word length in 
  # the English language
  chunks = (string[i:i+6] for i in xrange(0, len(string), 6))
  for chunk in chunks:
    # loop over all chunks in the source text
    while chunk is not None:
      # try to append all of this chunk to the current word
      chunk = w.append(chunk)
      if chunk is not None:
        # store this word
        words.append(w)
        # update the char count
        count += w.getSourceLength()
        # create a new word and set its POR
        w = Word(state, POR(por.accessible, por.item_offset, count),
                 main_ob, trail_ob)
  words.append(w)
  return words

class Word(object):
  '''  
  Represents a word in a body of text. Each L{Word} has a main and a trailing
  part where the main part is processed according to other flags in the current
  L{WordState} to improve its presentation to the user via a speech or other
  output device while the trailing part remains unprocessed. The value of
  WordDef determines what characters lie in the main and trailing parts of each
  word. The following constants are available in L{AEConstants}.
  
    - WORD_NON_BLANK: All non-blank characters are added to the main part
    - WORD_ALPHABETIC: All characters considered letters in the current locale
      are added to the main part  
    - WORD_ALPHA_NUMERIC: All characters considered letters and digits in the
      current locale are added to the main part
    - WORD_ALPHA_PUNCT: All characters considered letters and punctuation in 
      the current locale are added to the main part
    - WORD_ALPHA_NUMERIC_PUNCT: All characters considered letters, digits, and
      punctuation in the current locale are added to the main part
  
  Characters in the ignore list are considered blank. A L{POR} 
  can be associated with a L{Word} to indicate its context in a larger body of 
  text.
  
  Callables may be specified as observers for characters processed by the main
  and trail parts of each L{Word}. An observer must take four parameters, this
  L{Word} instance, the L{WordState} in use, the current character, and the 
  list of all characters in the main or trail part of the word. The observer
  should return the character to be added. The list may be modified in place
  to affect the final contents of the word.

  @ivar state: Settings that determine the definition of a L{Word} and how
    it is prepared for output
  @type state: L{WordState}
  @ivar por: Point of regard indicating where this L{Word} originated
  @type por: L{POR}
  @ivar source_word: Original text of this L{Word} without any preparation for
    output applied
  @type source_word: list
  @ivar has_main: Has at least one main character been parsed?
  @type has_main: boolean
  @ivar main_part: Part of this L{Word} that will receive extra preparation for
    output
  @type main_part: list
  @ivar trail_part: Part of the word that will receive little preparation for 
    output
  @type trail_part: list
  @ivar main_done: Is the L{main_part} complete?
  @type main_done: boolean
  @ivar trail_done: Is the L{trail_part} complete?
  @type trail_done: boolean
  @ivar more: Are there likely more L{Word}s after this one in the text source
    where this L{Word} originated?
  @type more: boolean
  @ivar curr_repeat: Indicates a character should be considered a repeat iff
    this value > MaxRepeat. It is not the exact number of repetitions of a 
    character as it is optimized for speed, not accuracy    
  @type curr_repeat: integer
  @ivar last_char: Last character appended to this L{Word}
  @type last_char: string
  @ivar main_ob: Function to invoke for each character in the main part of a 
    word
  @type main_ob: callable
  @ivar trail_ob: Function to invoke for each character in the trailing part
    of a word
  @type trail_ob: callable
  '''
  def __init__(self, state, por, main_ob=None, trail_ob=None):
    '''
    Stores the L{WordState} and initializes all instance variables.
    
    @param state: State that defines L{Word}s and how they are processed
    @type state: L{WordState}
    @param por: Point of regard indicating where this L{Word} originated
    @type por: L{POR}
    @param main_ob: Function to invoke for each character in the main part of a
      word
    @type main_ob: callable
    @param trail_ob: Function to invoke for each character in the trailing part
      of a word
    @type trail_ob: callable
    '''
    self.state = state
    self.por = por
    self.source_word = []
    self.main_part = []
    self.trail_part = []
    self.has_main = False
    self.main_done = False
    self.trail_done = False
    self.more = False
    self.curr_repeat = 0
    self.last_char = None
    self.main_ob = main_ob
    self.trail_ob = trail_ob

  def __eq__(self, other):
    '''
    Compares this L{Word} to the one provided based on their L{POR}s and 
    content. If their L{source_word}s and L{POR}s are the same, they are 
    considered equal.
    
    @param other: Word to compare
    @type other: L{Word}
    '''
    return (self.por == other.por) and (self.source_word == other.source_word)
  
  def __unicode__(self):
    '''
    Gets this L{Word} as a unicode string.
    
    @return: Main part of the string joined with the trail
    @rtype: string
    '''
    return u''.join(self.main_part+self.trail_part)
  
  def __str__(self):
    '''
    Gets this L{Word} as a non-unicode string.
    
    @return: Main part of the string joined with the trail
    @rtype: string
    '''
    return ''.join(self.main_part+self.trail_part)
  
  def _isMainChar(self, ch):
    '''
    Determines if the given character should be considered a part of the main
    part of this word or not based on the definition of the word given by
    L{WordState}.
    
    @param ch: Character to test
    @type ch: string
    '''
    if self.state.WordDef == WORD_NON_BLANK:
      return not self.isBlank(ch)
    elif self.state.WordDef == WORD_ALPHABETIC:
      return self.isAlpha(ch)
    elif self.state.WordDef == WORD_ALPHA_NUMERIC:
      return self.isAlpha(ch) or self.isNumeric(ch)
    elif self.state.WordDef == WORD_ALPHA_PUNCT:
      return self.isAlpha(ch) or self.isPunctuation(ch)
    elif self.state.WordDef == WORD_ALPHA_NUMERIC_PUNCT:
      return self.isAlpha(ch) or self.isNumeric(ch) or self.isPunctuation(ch)
    else:
      return False
    
  def replaceMain(self, text):
    '''
    Replaces the main part of the word with the given string.
    
    @param text: Text to use as the main part of the word
    @type text: string
    '''
    self.main_part = text
  
  def replaceTrail(self, text):
    '''
    Replaces the main part of the word with the given string.
    
    @param text: Text to use as the main part of the word
    @type text: string
    '''
    self.main_part = text
    
    
  def getPOR(self):
    '''
    Gets the L{POR} associated with the start of this L{Word}.
    
    @return: Point of regard pointing to the start of this word
    @rtype: L{POR}
    '''
    return self.por
    
  def isBlank(self, ch):
    '''
    Determines if the given character is blank or ignored.
    
    @param ch: Character to test
    @type ch: string
    @return: Is the character a blank?
    @rtype: boolean
    '''
    return ch.isspace() or ch in self.state.Ignore
  
  def isAlpha(self, ch):
    '''
    Determines if the given character is a letter in the current locale.
    
    @param ch: Character to test
    @type ch: string
    @return: Is the character a letter?
    @rtype: boolean
    '''
    return ch.isalpha()
    
  def isNumeric(self, ch):
    '''
    Determines if the given character is a number in the current locale.
    
    @param ch: Character to test
    @type ch: string
    @return: Is the character a number?
    @rtype: boolean
    '''
    return ch.isdigit()
  
  def isPunctuation(self, ch):
    '''
    Determines if the given character is a punctuation mark.
    
    @param ch: Character to test
    @type ch: string
    @return: Is the character a punctuation mark?
    @rtype: boolean
    '''
    cat = ud.category(unicode(ch))
    return (cat == 'Lm' or cat[0] in ['M', 'P', 'S'])
  
  def isSymbol(self, ch):
    '''
    Determines if the given character is a symbol.
    
    @param ch: Character to test
    @type ch: string
    @return: Is the character a symbol?
    @rtype: boolean
    '''
    return ud.category(unicode(ch)).startswith('C')

  def isVowel(self, ch):
    '''
    Determines if the given character is a vowel. Relies on a translator to
    list all vowels in the current locale.

    @param ch: Character to test
    @type ch: string
    @return: Is the character a Latin vowel?
    @rtype: boolean
    '''  
    return ch in VOWELS
  
  def isCap(self, ch):
    '''
    Determines if the given character is an upper case letter.

    @param ch: Character to test
    @type ch: string
    @return: Is the character capitalized?
    @rtype: boolean
    '''      
    return ch.isupper()
  
  def getCharValue(self, ch):
    '''
    Gets the unicode hex value for a character sans the 0x prefix.
    
    @param ch: Single character
    @type ch: string
    @return: Hex value of the character
    @rtype: string
    '''
    return hex(ord(ch))[2:]
  
  def getCharName(self, ch):
    '''
    Gets the unicode name of the character, one of the strings listed in the
    U{http://unicode.org/charts/charindex.html}. If the character could not be
    determined from the given string, returns an empty string. Note that these 
    names are not localized.
    
    @param ch: Single character
    @type ch: string
    @return: Name of the character
    @rtype: string
    '''
    try:
      return ud.name(unicode(ch)).lower()
    except Exception:
      return ''
  
  def getCharDescription(self, ch):
    '''
    Gets a localized description of the given character. The most detailed
    description for a character is returned so that, for instance, 'e' is
    described as a vowel and not just a letter.

    @param ch: Character to test
    @type ch: string
    @return: Localized description of the character according to the processing
      done by this L{Word} class and based on the current state
    @rtype: boolean
    '''  
    if ch in self.state.Ignore:
      return _('ignored')
    elif self.isBlank(ch):
      return _('blank')
    elif self.isAlpha(ch):
      if self.isCap(ch):
        return _('capital')
      elif self.isVowel(ch):
        return _('vowel')
      else:
        return _('letter')
    elif self.isNumeric(ch):
      return _('number')
    elif self.isPunctuation(ch):
      return _('punctuation')
    elif self.isSymbol(ch):
      return _('symbol')    
  
  def getSource(self):
    '''
    Gets the unprocessed text of this word as it was seen in the original text
    source.
    
    @return: Parsed word without any processing applied
    @rtype: string
    '''
    return self.source_word
  
  def getSourceLength(self):
    '''
    Gets the length of the unprocessed source text of this L{Word}.
    
    @return: Length of the L{source_word}
    @rtype: integer
    '''
    return len(self.source_word)
  
  def getMainLength(self):
    '''
    Gets the length of the processed main part of this L{Word}.
    
    @return: Length of the L{main_part}
    @rtype: integer
    '''
    return len(self.main_part)
  
  def moreAvailable(self):
    '''
    Makes a guess as to whether or not there are more L{Word}s in the body of
    text from which this word originated. This guess is based on whether or not
    the last chunk passed to L{append} was processed in full.
    
    @return: Are there likely more L{Word}s in the original body of text
    @rtype: boolean
    '''
    return self.more
  
  def hasRepeat(self):
    '''    
    Gets if this L{Word} has a character repeated more than the maximum number
    of repetitions allowed or not.
    
    @return: Does this L{Word} containg a repeated character?
    @rtype: boolean
    '''
    if self.curr_repeat > self.state.MaxRepeat and not self.isAllNumeric():
      return True
    return False
  
  def hasCap(self):
    '''
    Gets if this L{Word} contains an uppercase letter or not.
    
    @return: Does this L{Word} contain a capital letter?
    @rtype: boolean
    '''
    for ch in self.source_word:
      if self.isCap(ch):
        return True
    return False
  
  def hasVowel(self):
    '''
    Gets if this L{Word} contains a vowel or not.
    
    @return: Does this L{Word} contain a vowel?
    @rtype: boolean
    '''
    for ch in self.source_word:
      if self.isVowel(ch):
        return True
    return False
  
  def isAllCaps(self):
    '''
    Gets if this L{Word} is all capitals or not.
    
    @return: Is this L{Word} all capital letters?
    @rtype: boolean
    '''
    return self.isCap(self.source_word)
  
  def isAllNumeric(self):
    '''
    Gets if this L{Word} is all numbers or not.
    
    @return: Is this L{Word} all numbers?
    @rtype: boolean
    '''
    return self.isNumeric(self.source_word)
  
  def isAllBlank(self):
    '''
    Gets if this L{Word} is all blanks or not.
    
    @return: Is this L{Word} all blanks?
    @rtype: boolean
    '''
    for ch in self.source_word:
      if not self.isBlank(ch):
        return False
    return True
  
  #def shouldBeSpelled(self):
    #'''
    #Gets if this L{Word} should be spelled based on if it is a single
    #character, if ForceSpell is on and the word is not all numbers and does not
    #have a vowel, if SpellCaps is on and the word is all caps, and if the word
    #has a repeating letter and is all numbers.
    
    #@return: Should this L{Word} be spelled rather than sounded out as written?
    #@rtype: boolean
    #'''
    #if (self.state.Format == FORMAT_SPELL or 
        #self.state.Format == FORMAT_PHONETIC):
      #return True
    #elif not self.isAllBlank():
      #if (self.getMainLength() == 1 and 
          #(self.state.ForceSpell and not self.state.Format == FORMAT_TEXT)):
        ## single character
        #return True
      #elif (self.state.ForceSpell and 
            #(not self.isAllNumeric() and not self.hasVowel())):
        ## force spelling of words without vowels
        #return True
      #elif self.state.SpellCaps and self.isAllCaps():
        ## force spelling of acronyms
        #return True
      #elif self.curr_repeat > self.state.MaxRepeat and not self.isAllNumeric():
        ## for spelling of words with repeats in them
        #return True
    #return False
  
  def append(self, chunk):
    '''    
    Parses the given chunk of text for characters that should be added to the
    L{main_part} or L{trail_part} of this L{Word}. If this word has neither
    L{main_done} or L{trail_done} set, then all main characters determined by
    L{_isMainChar} up to the first non-main character are added to the main
    part of this word. When the first non-main word is encountered,
    L{main_done} is set. If this word has L{main_done} set and L{trail_done}
    unset, all non-main characters are added to the trail part of this word.
    When another main character is encountered after L{main_done} is set,
    L{trail_done} is set and the remainder of the given chunk is returned
    unprocessed to be added to another L{Word}. Once L{trail_done} is set, no
    further text can be appended to this L{Word}.
    
    @param chunk: Chunk of text to parse for words
    @type chunk: string
    @return: Unprocessed portion of the chunk or None if fully processed
    @rtype: string or None
    @see: L{_processMain}
    @see: L{_processTrail}
    '''
    if self.trail_done:
      # don't add anything new after the trail is complete
      return chunk
    for i, ch in enumerate(chunk):
      mc = self._isMainChar(ch)
      if mc:
        if self.main_done:
          # not accepting more main characters
          self.trail_done = True
          self.more = True
          return chunk[i:]
        else:
          self.has_main = True
          # process a new main character
          ch = self._processMain(ch)
      else:
        # process a new trail character
        ch = self._processTrail(ch)
        self.main_done = True
      # detect character repeitions for everything except the ellipsis
      # this method does not keep accurate track of the number of repetitions,
      # only that some character was repeated more than REPEAT number of times
      # in this word
      if ch == self.last_char and ch != '.':
        self.curr_repeat += 1
      elif self.curr_repeat < self.state.MaxRepeat:
        self.last_char = ch
        self.curr_repeat = 1
    return None
  
  def _processMain(self, ch):
    '''    
    Adds the given character to the L{source_word}. If Caps is unset, makes the
    character lowercase. If CapExpand and the character is a capital letter or
    NumExpand and the character is a number, inserts a space in L{main_part}.
    Finally inserts the possibly lowercased character in L{main_part}.
    
    @param ch: Character to process
    @type ch: string
    @return: Character inserted in L{trail_part}
    @rtype: string
    '''
    self.source_word.append(ch)
    if ch in self.state.Ignore:
      ch = u' '
    if not self.state.Caps:
      ch = ch.lower()
    if self.main_ob:
      ch = self.main_ob(self, self.state, ch, self.main_part)
    self.main_part.append(ch)
    return ch
            
  def _processTrail(self, ch):
    '''
    Adds the given character to the L{source_word}. If the character is a 
    blank, inserts a space in L{trail_part}, else inserts the character.
    
    @param ch: Character to process
    @type ch: string
    @return: Character inserted in L{trail_part}
    @rtype: string
    '''
    if ch in self.state.Ignore:
      ch = u' '
    self.source_word.append(ch)
    if self.trail_ob:
      ch = self.trail_ob(self, self.state, ch, self.trail_part)
    self.trail_part.append(ch)
    return ch
  
#if __name__ == '__main__':
  #import sys
  #def printWords(words):
    #for w in words:
      #sys.stdout.write('"%s"' % str(w))
    #for w in words:
      #print w.getPOR()
  
  #class TestState(object):
    #def __init__(self, **kwargs):
      #self.__dict__.update(kwargs)

  #test = "JAWS is a screen reader. I'd like to say, 'I'd like $10.00 for free.' Hey--look at that!"
  #state = TestState(WordDef=WORD_NON_BLANK, Caps=True, CapExpand=True, 
                    #NumExpand=True, Ignore='', MaxRepeat=3)
  #printWords(buildWordsFromString(test, None, state))
  #for i in range(len(test)):
    #p, c, n = getContextFromString(test, state, POR(None, None, i))
    #try: print p.getPOR(), p,
    #except: print 'no prev',
    #try: print c.getPOR(), c,
    #except: print 'no current',
    #try: print n.getPOR(), n,
    #except: print 'no next',
    #print
