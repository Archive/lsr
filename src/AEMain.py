#!/usr/bin/python
'''
Defines the main function which parses command line arguments and acts on them.
Creates an instance of the L{UIRegistrar} to add a new user interface element
(UIE) to the repository on disk when commands for the L{UIRegistrar} are
present. If the kill switch is found, kills the running instance of LSR started
by this user. Otherwise, starts an instance of the L{AccessEngine} and runs the
screen reader.

The L{main} function in this module parses the command line parameters and
dispatches command line options to the other top level functions defined here.
The other top level functions split up the work of installing/uninstalling new
UIEs (L{install}, L{uninstall}), associating/unassociating (L{associate}, 
L{disassociate}), creating/removing/duplicating profiles (L{createProfile}, 
L{removeProfile}, L{duplicateProfile}), initializing the global list of 
installed extensions L{initGlobal}, and running the screen reader (L{run}).
Other top level functions are provided for convenience such as L{configA11y} 
and L{generate}.

See the LSR man pages for help with using command line parameters.

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at 
U{http://www.opensource.org/licenses/bsd-license.php}
'''
try:
  # little bit of fun to see how psyco improves things
  import psyco
  psyco.profile()
except ImportError:
  pass

import optparse, os, signal, sys, subprocess, glob, re
import UIRegistrar, AEConstants, AEOutput, AELog
from i18n import _
from SettingsManager import SettingsManager
from AEConstants import HOME_USER, HOME_DIR, PROG_VERSION, PROG_DATE, \
     TEMPLATE_DIR, DEFAULT_ASSOCIATIONS, DEFAULT_PROFILE

# define a regular expression for doing word wrap on the command line
wrap_rx=re.compile(u"([\u2e80-\uffff])", re.UNICODE)

def wrap(text, width=79, encoding='utf-8', indent=4):
  '''    
  A word-wrap function that preserves existing line breaks and most spaces in
  the text. Expects that existing line breaks are posix newlines. Recipe by
  Junyong Pan from the Python Cookbook:
  
  U{http://aspn.activestate.com/ASPN/Cookbook/Python/Recipe/358117}
  
  @param text: Text to wrap
  @type text: string
  @param width: Maximum width of a line
  @type width: integer
  @return: Wrapped text as a string
  @rtype: string
  '''
  return reduce(lambda line, word, width=width: '%s%s%s' %              
                (line,
                 [' ','\n'+' '*indent, ''][(len(line)-line.rfind('\n')-1
                       + len(word.split('\n',1)[0] ) >= width) or
                      line[-1:] == '\0' and 2],
                 word),
                wrap_rx.sub(r'\1\0 ', unicode(text,encoding)).split(' ')
            ).replace('\0', '').encode(encoding)

def welcome():
  '''
  Prints copyright, license, and version info.
  '''
  print '%s ver. %s rev. %s' % \
        (AEConstants.NAME, AEConstants.PROG_VERSION, AEConstants.PROG_DATE)
  print AEConstants.COPYRIGHT
  print
  
def confirmRemove(name):
  '''
  Confirms deletion actions from the command prompt. Looks for a press of 'y'
  to indicate confirmation.
  
  @param name: Name of the "thing" being removed
  @type name: string
  @return: Confirm (True) or cancel (False)?
  @rtype: boolean
  '''
  ch = raw_input(AEConstants.MSG_CONFIRM_REMOVE % name)
  return ch == 'y'

def install(options):
  '''
  Installs a new UIE in the repository via the L{UIRegistrar}.
  
  @param options: Options seen on the command line
  @type options: optparse.Values
  @return: Description of the action
  @rtype: string
  @raise Exception: When the L{UIRegistrar} reports an error completing the 
    install
  '''
  reg = UIRegistrar
  reg.install(options.action_value, local=not options.uie_global)
  return _('%s installed' % options.action_value)

def uninstall(options):
  '''
  Uninstalls a UIE from the repository via the L{UIRegistrar}.

  @param options: Options seen on the command line
  @type options: optparse.Values
  @return: Description of the action
  @rtype: string
  @raise Exception: When the L{UIRegistrar} reports an error completing the 
    uninstall
  '''
  # confirm deletion
  if not confirmRemove(options.action_value):
    return _('Cancelled uninstall of %s') % options.action_value
  reg = UIRegistrar
  reg.uninstall(options.action_value, local=not options.uie_global)
  return _('%s uninstalled' % options.action_value)
  
def associate(options):
  '''  
  Associates a UIE with a profile so that it is loaded at a particular time
  (e.g. on startup, when any tier is created, or when a particular tier is
  created).
  
  @param options: Options seen on the command line
  @type options: optparse.Values
  @return: Description of the action
  @rtype: string
  @raise Exception: When the L{UIRegistrar} reports an error completing the 
    association
  '''
  reg = UIRegistrar
  profiles = reg.associate(options.action_value, options.profile, 
                           options.uie_tier, options.uie_all_tiers,
                           options.uie_index)
  if not profiles:
    return _('%s added to no profiles') % options.action_value
  else:
    return _('%s added to %s') % (options.action_value, ', '.join(profiles))

def disassociate(options):
  ''' 
  Disassociates a UIE with a profile so that it is no longer loaded at a
  particular time.
  
  @param options: Options seen on the command line
  @type options: optparse.Values
  @return: Description of the action
  @rtype: string
  @raise Exception: When the L{UIRegistrar} reports an error completing the 
    disassociation
  '''
  # confirm the deletion action
  if not confirmRemove(options.action_value):
    return _('Cancelled removal of %s') % options.action_value
  reg = UIRegistrar
  profiles = reg.disassociate(options.action_value, options.profile, 
                              options.uie_tier, options.uie_all_tiers)
  return _('%s removed from %s' % (options.action_value, ', '.join(profiles)))

def generate(options):
  '''
  Generates a template file for the given kind of UIE.
  
  @param options: Options seen on the command line
  @type options: optparse.Values
  @return: Description of the action
  @rtype: string
  @raise Exception: When there is a problem generating the new template because
    not all required parameters were specified, the template does not exist,
    the destination path is not writable, etc.
  '''
  # get the path to the UIE to generate and the kind
  path, kind = options.action_value
  # figure out the name from the path
  name, ext = os.path.basename(path).split(os.extsep)
  if options.uie_tier is not None:
    tier = "'%s'" % options.uie_tier
  else:
    tier = None
  # fill in the template and copy it to the appropriate location
  try:
    temp = file(os.path.join(TEMPLATE_DIR, kind), 'r').read()
  except IOError:
    raise ValueError(_('No template for %s') % kind)
  if options.uie_all_tiers:
    desc = 'all applications'
  elif tier:
    desc = options.uie_tier
  else:
    desc = 'nothing'
  # fill in the metadata for the new extension
  uie = temp % dict(name=name, tier=tier, all_tiers=options.uie_all_tiers,
                    desc=desc)
  try:
    file(path, 'w').write(uie)
  except IOError:
    raise ValueError(_('Could not create %s file') % kind)
  # install the UIE
  options.action_value = path
  rv = install(options)
  # associate the UIE with the given profiles
  options.action_value = name
  options.profile = options.profile or AEConstants.GENERATE_PROFILES
  return associate(options)
  
def createProfile(options):
  '''
  Creates a new, empty profile.
  
  @param options: Options seen on the command line
  @type options: optparse.Values
  @return: Description of the action
  @rtype: string
  @raise Exception: When the L{SettingsManager} reports an error creating the
    profile
  '''
  sm = SettingsManager(options.action_value)
  sm.createProfile()
  return _('Created profile %s') % options.action_value

def duplicateProfile(options):
  '''
  Duplicates an existing profile.
  
  @param options: Options seen on the command line
  @type options: optparse.Values
  @return: Description of the action
  @rtype: string
  @raise Exception: When the L{SettingsManager} reports an error copying the
    profile
  '''
  sm = SettingsManager(options.action_value[0])
  sm.copyProfile(options.action_value[1])
  return _('Duplicated profile %s as %s') % options.action_value

def removeProfile(options):
  '''
  Removes an existing profile.
  
  @param options: Options seen on the command line
  @type options: optparse.Values
  @return: Description of the action
  @rtype: string
  @raise Exception: When the L{SettingsManager} reports an error deleting the
    profile
  '''
  sm = SettingsManager(options.action_value)
  sm.ensureProfile()
  # confirm deletion
  if not confirmRemove(options.action_value):
    return _('Cancelled removal of profile %s') % options.action_value
  sm.deleteProfile()
  return _('Removed profile %s') % options.action_value

def exportPath(options):
  '''
  Gets the path to the package containing this file. Essentially, this is
  the path to the LSR package on disk.
  
  @param options: Options seen on the command line
  @type options: optparse.Values
  @return: Description of the action
  @rtype: string
  '''
  return os.path.abspath(os.path.normpath(os.path.dirname(__file__)))
  
def say(options):
  '''
  Uses the first available audio L{AEOutput} device to output a string.
  
  @param options: Options seen on the command line
  @type options: optparse.Values
  @return: Description of the action
  @rtype: string
  '''
  import DeviceManager, SettingsManager

  if options.profile is None:
    profile = DEFAULT_PROFILE
  else:
    profile = options.profile[0]
    
  class DummyEngine(object):
    # dummy engine object which has the one method needed by the device manager
    def getProfile(self):
      return profile
    
  # build a partially initialized device manager
  dm = DeviceManager.DeviceManager(DummyEngine())
  dm.settings_manager = SettingsManager.SettingsManager(profile)
  dm.loadDevices()
  
  # get an audio output device
  try:
    dev = dm.getOutputByCaps(['audio'])
  except Exception:
    return _('No audio output device')
  # output the utf-8 encoded string seen on the command line
  s = unicode(options.action_value, 'utf-8')
  dm.send(dev, AEConstants.CMD_STRING_SYNC, s, None, None)
  dm.close()
  return _('Said: %s') % s
    
def show(options):
  '''
  Shows all installed scripts and profiles, indicating all associated UIE's
  via the L{UIRegistrar}.
  
  @param options: Options seen on the command line
  @type options: optparse.Values
  @return: Description of the action
  @rtype: string
  '''
  reg = UIRegistrar
  if options.profile is None:
    # list of profiles
    print _('Profiles')
    print SettingsManager.listProfiles()
    print
    
    # list installed UIEs if no profile is given
    print _('Installed UIEs')
    i_count = 0
    for kind in UIRegistrar.ALL_KINDS:
      names = reg.listInstalled(kind)
      i_count += len(names)
      print kind.title()+'s'
      print wrap('    '+',  '.join(names))
    return _('%d installed UIEs') % i_count   
  else:
    # list UIEs associated with each profile otherwise
    for name in options.profile:
      print _('Associated UIEs (%s)') % name
      a_count = 0
      for kind in UIRegistrar.ALL_KINDS:
        if kind in UIRegistrar.STARTUP_KINDS:
          names = reg.listAssociated(kind, name)
          if not names:
            # don't print a blank line if we have zero of a kind of UIE
            continue
          a_count += len(names)
          print kind.title()+'s'
          print wrap('    '+', '.join(names))
        elif kind == UIRegistrar.PERK:
          # handle any tier perks
          print kind.title()
          names = reg.listAssociatedAny(kind, name)
          a_count += len(names)
          print wrap('    '+', '.join(names))
          for app, names in reg.listAssociatedMap(kind, name).items():
            # then tier specific perks
            a_count += len(names)
            print '  (%s)' % app
            print wrap('    '+', '.join(names))
      print _('%d associated UIEs') % a_count
    return ''

def run(options):
  '''
  Runs the screen reader.
  
  @param options: Options seen on the command line
  @type options: optparse.Values
  @return: Description of the action
  @rtype: string
  @raise Exception: When anything goes wrong during initialization
  '''
  import Adapters
  # make sure the profile exists
  if options.profile is None:
    profile = DEFAULT_PROFILE
  else:
    profile = options.profile[0]
  if profile not in SettingsManager.listProfiles():
    raise ValueError(_('Profile %s does not exist') % options.profile)
  # import AE here so we can do other ops outside a Gnome session
  import AccessEngine
  # use the profile specified on the command line or the default
  ae = AccessEngine.AccessEngine(profile, not options.no_intro)
  rv = ae.run()
  return rv

def initProfiles(options):
  '''
  Initializes the default user profiles using the L{UIRegistrar}. All UIEs 
  shipped with LSR are associated with their appropriate profiles by this 
  method in a well-defined order specified here.
  
  @param options: Options seen on the command line
  @type options: optparse.Values
  @return: Description of the action
  @rtype: string
  @raise Exception: When anything goes wrong during the initialization of the
    default profiles
  '''
  for profile, uies in DEFAULT_ASSOCIATIONS.items():
    for uie in uies:
      try:
        UIRegistrar.associate(uie, profiles=[profile])
      except (AttributeError, KeyError), e:
        # not a UIE or not installed; skip
        pass
  return _('Initialized default profiles')

def initGlobal(options):
  '''  
  Initializes the global cache of installed UIEs in the L{HOME_DIR} folder
  using the L{UIRegistrar}. All UIEs in the Perks, Devices, Choosers, and
  Monitors are installed by this method. Any UIEs with names that do not
  conflict with the defaults are left untouched by this process.
  
  @param options: Options seen on the command line
  @type options: optparse.Values
  @return: Description of the action
  @rtype: string
  @raise ValueError: When anything goes wrong during initialization of the
    global repository
  '''
  uies = []
  for kind in UIRegistrar.ALL_KINDS:
    if kind == UIRegistrar.PERK:
      # account for perk subdirs
      path = os.path.join(HOME_DIR, kind.title()+'s')
      for subdir in os.listdir(path):
        subdir = os.path.join(HOME_DIR, kind.title()+'s', subdir)
        if os.path.isdir(subdir):
          path = os.path.join(subdir, '*.py')
          uies.extend(glob.glob(path))
    else:
      # other kinds of UIEs don't have subdirs
      path = os.path.join(HOME_DIR, kind.title()+'s', '*.py')
      uies.extend(glob.glob(path))
  for uie in uies:
    try:
      UIRegistrar.install(uie, local=False, overwrite=True)
    except AttributeError:
      # not a UIE; skip
      pass
  return _('Initialized global extensions')

def getAction(option, opt_str, value, parser):
  '''
  Maps a command line param to the name of a function in this module that will
  handle it. For instance, the command line parameter for installing a UIE is
  mapped to the L{install} function which will carry out the command.
  '''
  parser.values.action = option.default
  parser.values.action_value = value
  
def configA11y(options):
  '''
  Runs gconf to check if desktop accessibility support is enabled or not. If 
  not, enables it and asks the user to logout. 
  
  @note: Current implementation is GNOME, AT-SPI dependent
  @param options: Options seen on the command line
  @type options: optparse.Values
  @raise ValueError: When accessibility support is not enabled
  '''
  if options.no_a11y:
    return
  try:
    import gconf
  except ImportError:
    return
  cl = gconf.client_get_default()
  if not cl.get_bool('/desktop/gnome/interface/accessibility'):
    # use the UIRegistrar to load the A11yChooser
    chooser = UIRegistrar.loadOne('A11yChooser')
    chooser.init()
  
def configGUI(options):
  '''  
  Configures the GUI toolkit with a default icon list for all LSR top level 
  windows. Also enables accessibility for this application.

  @note: Current implementation is gtk/gail dependent
  '''
  if options.no_bridge:
    # remove the bridge on purpose
    os.environ['GTK_MODULES'] = ''
  else:
    # add GTK_MODULES to the environment so all GUIs are accessible
    os.environ['GTK_MODULES'] = 'gail:atk-bridge'

  import pygtk
  pygtk.require('2.0')
  import gtk
  # load all sizes of icons
  it = gtk.IconTheme()
  try:
    icons = [it.load_icon(AEConstants.PROG_NAME, size, gtk.ICON_LOOKUP_NO_SVG) 
             for size in AEConstants.ICON_SIZES]
  except Exception:
    # ignore errors, and just don't use the icon
    pass
  else:
    gtk.window_set_default_icon_list(*icons)
    
def parseOptions():
  '''
  Parses the command line arguments based on the definition of the expected
  arguments provided here.
  
  @return: Parsed options
  @rtype: optparse.Values
  '''
  # create a command line option parser
  op = optparse.OptionParser()
  # add all the command line options to the parser
  op.add_option('-g', '--generate', action='callback', callback=getAction,
                help=AEConstants.HELP_GENERATE, default=generate, type='str',
                nargs=2)
  op.add_option('-i', '--install', action='callback', callback=getAction,
                help=AEConstants.HELP_INSTALL, default=install, type='str',
                nargs=1)
  op.add_option('-u', '--uninstall', action='callback', callback=getAction,
                help=AEConstants.HELP_UNINSTALL, default=uninstall,type='str',
                nargs=1)
  op.add_option('-a', '--associate', action='callback', callback=getAction,
                help=AEConstants.HELP_ASSOCIATE, default=associate,type='str',
                nargs=1)
  op.add_option('-d', '--disassociate', action='callback', callback=getAction,
                help=AEConstants.HELP_DISASSOCIATE, default=disassociate,
                type='str', nargs=1)
  op.add_option('-c', '--create-profile', action='callback',callback=getAction,
                help=AEConstants.HELP_CREATE_PROFILE, default=createProfile,
                type='str', nargs=1)
  op.add_option('-r', '--remove-profile', action='callback',callback=getAction,
                help=AEConstants.HELP_REMOVE_PROFILE, default=removeProfile,
                type='str', nargs=1)
  op.add_option('--dupe', '--duplicate-profile', action='callback', 
                callback=getAction, default=duplicateProfile,
                help=AEConstants.HELP_DUPLICATE_PROFILE, type='str', nargs=2)
  op.add_option('-s', '--show', action='callback', callback=getAction,
                default=show, help=AEConstants.HELP_SHOW)
  op.add_option('-p', '--profile', action='append', dest='profile',
                help=AEConstants.HELP_PROFILE)
  op.add_option('-y', '--say', action='callback', callback=getAction,
                help=AEConstants.HELP_SAY, default=say, type='str')
  op.add_option('--index', dest='uie_index', action='store', type='int',
                help=AEConstants.HELP_INDEX)
  op.add_option('--global', action='store_true', default=False,
                dest='uie_global', help=AEConstants.HELP_GLOBAL)
  op.add_option('--all-tiers', '--all-apps', action='store_true', 
                default=False,
                dest='uie_all_tiers', help=AEConstants.HELP_ALL_TIERS)
  op.add_option('--tier', '--app', dest='uie_tier', action='store', type='str',
                help=AEConstants.HELP_TIER)
  op.add_option('-l', '--log-level', dest='log_level', action='store',
                choices=['none', 'debug', 'print', 'info', 'warning', 'error'],
                help=AEConstants.HELP_LOG_LEVEL % '[debug, print, info, ' \
                'warning, error, critical]')
  op.add_option('--log-channel', dest='log_channels', action='append',
                help=AEConstants.HELP_LOG_CHANNEL % '[Perk, Tier, ...]')
  op.add_option('--log-file', dest='log_file', action='store', type='str',
                help=AEConstants.HELP_LOG_FILENAME)
  op.add_option('--log-conf', dest='log_conf', action='store', type='str',
                help=AEConstants.HELP_LOG_CONFIG)
  op.add_option('--init-global', action='callback', callback=getAction, 
                default=initGlobal, help=AEConstants.HELP_INIT_GLOBAL)
  op.add_option('--init-profiles', action='callback', callback=getAction, 
                default=initProfiles, help=AEConstants.HELP_INIT_PROFILES)
  op.add_option('--no-intro', action='store_true', default=False, 
                dest='no_intro', help=AEConstants.HELP_NO_INTRO)
  op.add_option('--no-a11y', action='store_true', default=False, 
                dest='no_a11y', help=AEConstants.HELP_NO_A11Y)
  op.add_option('--no-bridge', action='store_true', default=False,
                dest='no_bridge', help=AEConstants.HELP_NO_BRIDGE)
  op.add_option('--export-path', action='callback', callback=getAction,
                default=exportPath, help=AEConstants.HELP_EXPORT_PATH)
  (options, args) = op.parse_args()
  return options

def main():
  '''
  Print the welcome message, parses command line arguments, and configures the
  logging system. Dispatches control to one of the top-level functions in this
  module depending on the command line parameters specified. 

  If the LSR L{AccessEngine} should be run (i.e. not configured or tested), 
  checks that a11y support is enabled in gconf. If not, enables it but exits
  with an error.
  '''
  # parse the command line options
  options = parseOptions()

  try:
    # see if an action has been specified
    mtd = options.action
  except AttributeError:
    # if not, run LSR
    mtd = run

  # check if the default profiles need to be created
  # but don't do it if we're initializing the global repo since we might be
  # the root user
  if mtd is not initGlobal and not SettingsManager.hasDefaultProfiles():
    initProfiles(None)

  if mtd is not exportPath:
    # show the welcome message
    welcome()

  if mtd is run:
    # configure GUI toolkit
    configGUI(options)
    # make sure accessibility is enabled on the desktop and for LSR
    configA11y(options)
    # set up the logging system
    AELog.configure(options.log_conf, options.log_level, options.log_channels,
                    options.log_file)
    # run the AccessEngine
    mtd(options)
  else:
    import traceback as tb
    try:
      # execute the function for the action
      print mtd(options)
    except Exception, e:
      info = tb.extract_tb(sys.exc_info()[2])
      print '%s (%d): %s' % (os.path.basename(info[-1][0]), info[-1][1], e)

if __name__ == '__main__':
  main()
