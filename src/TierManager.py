'''
Defines a class responsible for managing all L{Tier}s.

@author: Peter Parente
@author: Pete Brunet
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
import logging
import AEEvent, AEState, AEMonitor, AEConstants, UIRegistrar
from Tier import Tier
from UIRegistrar import MONITOR, PERK
from AEInterfaces import implements
from i18n import _

log = logging.getLogger('Tier')

class LSRState(AEState.AEState):
  '''
  Stateful information for all L{Tier}s. These should be considered system wide
  global settings.
  
  Trap (bool): When set to False, L{Task.Tools.Error} will be raised but not
  presented to the user. If True, errors will be raised and presented. Defaults
  to True.
  
  Stopping (bool): When set to False, only forceful stop commands will be sent
  the the output device. Useful for debugging. Defaults to True.
  '''
  def init(self):
    self.newBool('Trap', True, _('Announce script exceptions?'),
                 _('When set, important Perk exceptions will be announced on '
                   'output devices.'))
    self.newBool('Stopping', True, _('Allow automatic stops?'), 
                 _('When unset, only forceful stops will be sent to output '
                   'devices.'))
    
  def getGroups(self):
    '''
    Gets the configurable settings with metadata describing them.
    
    @return: Root group of all configurable settings
    @rtype: L{AEState.Group}
    '''
    g = self.newGroup()
    g.extend(['Trap', 'Stopping'])
    return g

class TierManager(object):
  '''
  Creates L{Tier}s for all processes detectable by L{pyLinAcc}. Routes
  L{AEEvent}s to the active L{Tier} for handling by L{Task}s registered by
  L{Perk}s. Changes the active L{Tier} as the active top-level application 
  changes.
  
  @ivar tiers: All L{Tier}s created for running applications
  @type tiers: dictionary
  @ivar active_tier: L{Tier} for the application currently in focus
  @type active_tier: L{Tier}
  @ivar acc_eng: The Access Engine that started this manager
  @type acc_eng: L{AccessEngine}
  @ivar view_manager: Reference to the L{ViewManager} to be used to dynamically 
    change which events should be monitored based on the registered L{Task}s
  @type view_manager: L{ViewManager}
  @ivar sett_manager: Reference to the L{SettingsManager} to be used to persist
    and load L{LSRState}
  @type sett_manager: L{SettingsManager}
  @ivar state: Global state informaion. Has properties representing system-wide
    settings.
  @type state: L{LSRState}
  '''
  def __init__(self, ae):
    '''
    Creates the empty dictionary for storing L{Tier}s and initializes the active
    tier to None. Stores a reference to L{AccessEngine}.
    
    @param ae: References to the L{AccessEngine} that created this manager
    @type ae: L{AccessEngine}
    '''
    self.state = None
    self.tiers = {}
    self.active_tier = None
    self.acc_eng = ae
    self.view_manager = None
    self.sett_manager = None
    self.monitors = AEMonitor.MonitorCollection()
    self.gesture = [None, 0]
    
  def init(self, view_man, sett_man, **kwargs):
    '''
    Stores a references to the L{DeviceManager}. Called by L{AccessEngine} 
    at startup.
    
    @param sett_man: Reference to the L{SettingsManager} to be used to load a
      persisted L{LSRState} object
    @type sett_man: L{SettingsManager}
    @param view_man: Reference to the L{ViewManager} to be used to dynamically 
      change which events should be monitored based on the registered L{Task}s
    @type view_man: L{ViewManager}
    @param kwargs: References to managers 
    @type kwargs: dictionary
    '''
    self.view_manager = view_man
    self.sett_manager = sett_man
    # load all startup monitors
    reg = UIRegistrar
    mons = reg.loadAssociated(MONITOR, self.acc_eng.getProfile())
    self.addMonitors(*mons)
    self.state = LSRState()
    self.state.init()
    try:
      # load the LSRState if available
      self.state = sett_man.loadState(__name__, self.state)
    except KeyError:
      # or use the new one
      pass
    # indicate interest in private events
    self.setEventInterest(AEEvent.PrivateChange, True)
      
  def close(self):
    '''
    Removes all monitors and persists the current L{LSRState}. Informs all 
    existing L{Tier}s that their state should be persisted also.
    '''
    # persist global state if it's dirty
    self.sett_manager.saveState(__name__, self.state)
    # let all Tiers persist their state too
    map(Tier.clearPerks, self.tiers.values())
    # shut down all monitors
    self.monitors.clear()
    
  def getState(self):
    '''
    @return: State shared across all L{Tier}s
    @rtype: L{LSRState}
    '''
    return self.state
  
  def getMonitors(self):
    '''
    @return: Collection of all loaded L{AEMonitor}s
    @rtype: L{AEMonitor.MonitorCollection}
    '''
    return self.monitors
    
  def addMonitors(self, *monitors):
    '''
    Adds one or more L{AEMonitor}s to the list of monitors to be 
    notified about events. 
    
    @param monitors: L{AEMonitor}s to notify
    @type monitors: tuple of L{AEMonitor}s
    '''
    self.monitors.add(AEEvent.Base.AccessEngineEvent, monitors)
        
  def showTask(self, task, perk):
    '''    
    Informs L{AEMonitor}s added via L{addMonitors} of a L{Task} executing or
    updating.
    
    @param perk: L{Perk} handling the event
    @type perk: L{Perk}
    @param task: L{Task} handling the event
    @type task: L{Task}
    '''
    self.monitors.show(task=task, perk=perk)
        
  def showEvent(self, event, tier_name):
    '''    
    Informs L{AEMonitor}s added via L{addMonitors} of an event.
    
    @param event: An L{AEEvent} object
    @type event: L{AEEvent}
    @param tier_name: Name of the L{Tier} executing the event
    @type tier_name: string
    '''
    self.monitors.show(event=event, tier_name=tier_name)
    
  def showChain(self, chain_type, anchor_ident):
    '''    
    Informs L{AEMonitor}s added via L{addMonitors} of a chained L{Task} 
    executing.
    
    @param chain_type: One of the L{AEConstants} CHAIN_* constants
    @type chain_type: integer
    @param anchor_ident: Identifier of the anchor L{Task}
    @type anchor_ident: string
    '''
    self.monitors.show(chain_type=chain_type, anchor_ident=anchor_ident)
    
  def showPropagate(self, propagate):
    '''    
    Informs L{AEMonitor}s added via L{addMonitors} of the propagation return
    value from a L{Task}.
    
    @param propagate: Was the event consumed (False) or allowed to propagate 
      (True)?
    @type propagate: boolean
    '''
    self.monitors.show(propagate=propagate)
    
  def freeDeadTiers(self, aids):
    '''
    Compares the list of given application names and IDs with those currently
    associated with L{Tier}s. Frees L{Tier}s in the L{tiers} dictionary that
    no longer have associated applications.
    
    @param aids: List of identifiers for applications currently in existence
    @type aids: list
    '''
    map(self.removeTier, set(self.tiers) - set(aids)) 
  
  def createTier(self, name, aid, por, init):
    '''
    Creates a new L{Tier} using the application name and ID as a hash code. 
    If init is True, only creates the L{Tier} if a specific L{Perk} is 
    associated with this profile for the named application.

    @param name: Name of the now active application
    @type name: string
    @param aid: Unique ID for the application associated with the L{Tier}
    @type aid: hashable
    @param init: Initialize the L{Tier} only if a L{Perk} is associated for
      this particular application?
    @type init: boolean
    @param por: Point of regard to the top object represented by the L{Tier}
    @type por: L{POR}
    @return: The new L{Tier} that was created
    @rtype: L{Tier}
    '''
    # create a UIRegistrar instance
    reg = UIRegistrar
    prof = self.acc_eng.getProfile()
    # don't create the Tier if we're only initializing Tiers for applications 
    # that have app specific Perks
    if init and not reg.hasTierAssociated(PERK, prof, name):
      return None
    # create the Tier object and store it
    tier = Tier(self, name, aid, por)
    self.tiers[aid] = tier
    # load all default and application specific Perks, push them onto the Tier
    perks = reg.loadAssociated(PERK, prof, name)
    tier.pushPerk(self.acc_eng, *perks)
    log.debug('created Tier %s', name)
    return tier

  def removeTier(self, aid):
    '''
    Removes an existing L{Tier}.
    
    @param aid: Unique ID for the application associated with the L{Tier}
    @type aid: hashable
    '''
    self.tiers[aid].clearPerks()
    del self.tiers[aid]
    log.debug('removed Tier %s', aid)
    
  def switchTier(self, name, aid, por):
    '''    
    Switches the active L{Tier} based on the current view's root accessible.
    The view's root accessible is queried for information that can uniquely
    identify the L{Tier} to use.
    
    If a L{Tier} already exists for the given view, it is activated. If one
    does not exist, a L{Tier} is created using L{createTier} and made active.
    
    @param name: Name of the now active application
    @type name: string
    @param aid: Unique ID for the application associated with the L{Tier}
    @type aid: hashable
    @param por: Point of regard to the top object represented by the L{Tier}
    @type por: L{POR}
    '''
    try:
      tier = self.tiers[aid]
    except KeyError:
      # create a new tier for this application
      tier = self.createTier(name, aid, por, False)
    # make the tier active
    self.active_tier = tier
    
  def setEventInterest(self, kind, wants):
    '''
    Informs the L{ViewManager} of a change in interest in L{AEEvent}s by some
    L{Tier}. Used to optimize which system events are monitored in order to
    improve performance. Just acts as a proxy.
    
    @param kind: Kind of L{AEEvent} of interest to a L{Task}
    @type kind: L{AEEvent} class
    @param wants: Does a L{Perk} want an event (i.e. a L{Task} is registering 
      for it or no longer want an event (i.e. a L{Task} is unregistering for 
      it)?
    @type wants: boolean
    '''
    self.view_manager.setEventInterest(kind, wants)

  def _resetGesture(self):
    '''
    Resets the gesture sequence counter.
    '''
    self.gesture = [None, 0]
    
  def manageEvent(self, event):
    '''
    Dispatches an L{AEEvent} to the L{Tier} to be handled by its L{Perk}s.
    Makes the final determination of on what layer the event occurred. Events
    from a focused source are already marked as
    L{AEConstants.Event.LAYER_FOCUS}.  Events from a unfocused source in the
    active L{Tier} are marked as L{AEConstants.Event.LAYER_TIER}. All other
    events for which a L{Tier} exists to handle them are marked as
    L{AEConstants.Event.LAYER_BACKGROUND}.
    
    @param event: Event to dispatch to the L{Tier}
    @type event: L{AEEvent.Base.AccessEngineEvent}
    '''
    # opt: check if the active tier wants this kind of event so we don't waste
    # time completing PORs when we request the event layer
    if self.active_tier and self.active_tier.wantsEvent(event):
      # get the current task layer and complete the event POR
      task_layer = event.getLayer()
    else:
      # otherwise, assume background
      task_layer = AEConstants.LAYER_BACKGROUND
      
    aid = event.getAppID()
    if aid is None:
      # no app ID means a global event, use the active tier
      tier = self.active_tier
    else:
      try:
        # get the tier that should handle the event
        tier = self.tiers[aid]
      except KeyError:
        # no tier means no handler, filter the event
        return
    
    if tier is not self.active_tier:
      # if the tier is not the active tier, the event layer is always 
      # background
      event.setLayer(AEConstants.LAYER_BACKGROUND)
    elif task_layer != AEConstants.LAYER_FOCUS:
      # if the tier is the active tier, but the layer is not already marked as
      # focus, then the layer is always the tier layer
      event.setLayer(AEConstants.LAYER_TIER)
    # let the tier manage the event
    tier.setShow(len(self.monitors))
    tier.manageEvent(event)
    tier.clearState()
   
  def manageGesture(self, event):
    '''
    Dispatches an L{AEEvent} to the active L{Tier} so that it can determine 
    which registered L{Task}, if any, should be executed in response to some
    L{AEInput.Gesture}.

    @param event: Event to dispatch to the L{Tier}
    @type event: L{AEEvent.Base.AccessEngineEvent}
    '''
    tier = self.active_tier
    if tier is not None:
      # keep track of sequences of input gestures
      g = event.getTaskKey()
      if g == self.gesture[0]:
        self.gesture[1] += 1
      else:
        self.gesture[0] = g
        self.gesture[1] = 0
      # let the active Tier manage the event
      tier.setShow(len(self.monitors))
      tier.manageGesture(event, self.gesture[1])
      tier.clearState()
      
  def manageChooser(self, event):
    '''
    Dispatches an L{AEEvent.ChooserChange} to the L{Tier} associated with the
    event so that it can determine which registered L{Task}, if any, should be 
    executed in response to a change in the chooser such as its completion or 
    its cancellation.

    @param event: Event to dispatch to the L{Tier}
    @type event: L{AEEvent.Base.AccessEngineEvent}
    '''
    aid = event.getAppID()
    try:
      # choose the Tier given its application ID
      tier = self.tiers[aid]
    except KeyError:
      return
    # let that Tier manage the event
    tier.setShow(len(self.monitors))
    tier.manageKeyedTask(event)
    tier.clearState()
    
  def manageTimer(self, event):
    '''
    Dispatches an L{AEEvent.TimerAlert} to the L{Tier} associated with the 
    event so that it can determine which registered L{Task}, if any, should be
    executed in response to the timer firing.
    
    @param event: Event to dispatch to the L{Tier}
    @type event: L{AEEvent.Base.AccessEngineEvent}
    '''
    aid = event.getAppID()
    try:
      # choose the Tier given its application ID
      tier = self.tiers[aid]
    except KeyError:
      return
    if tier is self.active_tier:
      # the event came from an unfocused control in the active tier
      event.setLayer(AEConstants.LAYER_TIER)
    # let that Tier manage the event
    tier.setShow(len(self.monitors))
    tier.manageKeyedTask(event) 
    tier.clearState()

  def managePrivate(self, event):
    '''
    Dispatches an L{AEEvent.PrivateChange} to the active L{Tier} so it can 
    store important information without passing it along to L{Perk}s and 
    L{Task}s.
    
    @param event: Event to dispatch to the L{Tier}
    @type event: L{AEEvent.Base.AccessEngineEvent}
    '''
    if self.active_tier is not None:
      self.active_tier.managePrivate(event)
      self.active_tier.clearState()
