'''
Defines a class responsible for managing the saving and loading of settings
from disk.

@var PROFILES_PATH: Path to all persisted setting for user profiles
@type PROFILES_PATH: string

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
from AEConstants import HOME_USER, BUILTIN_PROFILES, initUserPath
from i18n import _
import shelve, os, shutil, whichdb

# paths to parts of the settings repository
PROFILES_PATH = os.path.join(HOME_USER, 'profiles')
# make sure the profile folder is available
if initUserPath(PROFILES_PATH):
  print 'created ~/.lsr/profiles'

class SettingsManager(object):
  '''
  Manages the persistence of Python objects. Has methods for loading and
  saving state under a given name in the profile under which LSR is running or
  the profile named when the manager is instantiated. Maintains an in-memory
  cache of some objects loaded from disk to ensure multiple calls to L{load}
  return the same reference.
  
  @ivar name: Name of the profile
  @type name: string
  @ivar profile: Path to the profile database
  @type profile: string
  @ivar cache: Cache of previously loaded state objects. Used to ensure all
    references returned by L{loadState} point to the same state object. All
    objects in this cache are persisted to disk when the manager is closed.
  @type cache: dictionary of string : L{AEState}
  '''
  def __init__(self, profile):
    '''
    Stores the reference to the L{AccessEngine}. Opens the shelved data on disk
    in the profile currently used by the L{AccessEngine}.
    
    @param profile: Name of the profile to access using this manager. Can be
      used to access a profile other than the one returned by 
      L{AccessEngine.AccessEngine.getProfile}.
    @type profile: string
    '''
    self.cache = {}
    # at least one of profile or acc_eng must be specified
    self.name = profile
    self.profile = os.path.join(PROFILES_PATH, self.name+'.profile')
    
  def init(self, **kwargs):
    '''Does nothing.'''
    pass
    
  def close(self):
    '''
    Persists all state in the L{cache}.
    '''
    for name, state in self.cache.items():
      self.saveState(name, state)
      
  def loadStyles(self, device, default_style, style_cls):
    '''
    Loads a collection of L{AEOutput.Style} objects from disk. The loaded 
    styles are not stored in the L{cache} as the singleton requirement does
    not hold for output device styles. As a result, the styles loaded here will
    not be automatically persisted when this object is destroyed. L{saveStyles}
    should be invoked directly.
    
    @param device: Output device whose style objects we're loading
    @type device: L{AEOutput}
    @param default_style: Instance of a default style object to be populated
      with data
    @type default_style: L{AEOutput.Style.Style}
    @param style_cls: Class for constructing new styles to populate
    @type style_cls: L{AEOutput.Style.Style} class
    @raise KeyError: When the name is not a valid key
    @raise OSError: When the profile file cannot be opened or read
    '''
    default_data, styles_data = self.load(device.getClassName())
    # get the default style first
    default_style.unserialize(default_data)
    flyweight_styles = {}
    for key, data in styles_data:
      # build flyweights ontop of the default style
      st = style_cls(default_style)
      st.init(device)
      st.unserialize(data)
      flyweight_styles[key] = st
    return flyweight_styles
  
  def saveStyles(self, device, default_style, other_styles):
    '''
    Saves the internal data of a collection L{AEOutput.Style} objects to disk.
    
    @param device: Output device whose style objects we're persisting
    @type device: L{AEOutput}
    @param default_style: Instance of a default style object to have its data
      persisted
    @type default_style: L{AEOutput.Style.Style}
    @param other_styles: Dictionary of other styles to have their data 
      persisted under the keys used in the dictionary
    @type other_styles: dictionary of immutable : 
      L{AEOutput.Style.Style}
    @raise OSError: When the profile file cannot be opened or saved
    '''
    flyweight_data = [(key, style.serialize()) for key, style in 
                      other_styles.items()]
    self.save(device.getClassName(), 
              (default_style.serialize(), flyweight_data))
  
  def loadState(self, name, state):
    '''
    Loads an L{AEState} object from disk. If cached is True, stores a copy of
    the state to be returned in memory such that future calls to load return
    the same instance with the same state.

    @param name: Name under which the object was previously stored
    @type name: string
    @param state: LSR state object to initialize with the loaded data
    @type state: L{AEState}
    @return: Singleton instance of the L{AEState} object for the given name
    @rtype: L{AEState}
    @raise KeyError: When the name is not a valid key
    @raise OSError: When the profile file cannot be opened or read
    '''
    # check the cache first
    try:
      return self.cache[name]
    except KeyError:
      pass
    # cache the instance to ensure shared references to state
    self.cache[name] = state
    # try to load data for the state object
    data = self.load(name)
    # wrap the data in the state object
    state.unserialize(data)
    return state
  
  def saveState(self, name, state):
    '''
    Saves the internal data of an L{AEState} object to disk. Does not Pickle
    the state object directly, but rather calls its serialize method to get a
    simple dictionary. This is done to avoid the problems caused by trying to
    persist state objects in modules that have been reloaded at runtime. Also
    does not Pickle states that are not dirty according to 
    L{AEState.Base.AEState.isDirty}.
    
    @param name: Name under which to save the object
    @type name: string
    @param state: LSR state object whose data should be stored
    @type state: L{AEState}
    @raise OSError: When the profile file cannot be opened or saved
    '''
    if state.isDirty():
      self.save(name, state.serialize())
    
  def save(self, name, data):
    '''    
    Saves the given object under the given name in the profile used to 
    initialize this manager. Pickles the given object without any further
    processing on the part of this manager.
    
    @param name: Name under which to save the object
    @type name: string
    @param data: Object to store
    @type data: object
    @raise OSError: When the profile file cannot be opened or saved
    '''
    db = shelve.open(self.profile, protocol=-1)
    db[name] = data
    db.close()
  
  def load(self, name):
    '''        
    Loads the object from the profile used to initialize the manager. Unpickles
    the object under the given name without any additional processing on the
    part of this mananger.
    
    @param name: Name under which the object was previously stored
    @type name: string
    @return: Object loaded
    @rtype: object
    @raise KeyError: When the name is not a valid key
    @raise OSError: When the profile file cannot be opened or read
    '''
    db = shelve.open(self.profile, protocol=-1)
    try:
      data = db[name]
    finally:
      db.close()
    return data
  
  def createProfile(self):
    '''
    Initializes a new profile on disk.
    
    @raise ValueError: When the profile already exists
    @raise OSError: When the profile file cannot be created
    '''
    if self.existsProfile():
      raise ValueError(_('Profile %s already exists' % self.name))
    db = shelve.open(self.profile, protocol=-1)
    db.close()
    
  def ensureProfile(self):
    '''
    @raise ValueError: When the profile does not exist
    '''
    if not self.existsProfile():
      raise ValueError(_('Profile %s does not exist') % self.name)
  
  def existsProfile(self):
    '''
    @return: Does the managed profile exist on disk?
    @rtype: boolean
    '''
    return whichdb.whichdb(self.profile) is not None
  
  def deleteProfile(self):
    '''
    Deletes the managed profile from disk.
    
    @raise ValueError: When the profile is built-in and cannot be deleted or 
      the profile does not exist
    @raise OSError: When the profile database cannot be deleted
    '''
    if self.name in BUILTIN_PROFILES:
      raise ValueError(_('Cannot remove built-in %s profile') % self.name)
    if not self.existsProfile():
      raise ValueError(_('Profile %s does not exist' % self.name))
    os.unlink(self.profile)
    
  def copyProfile(self, name):
    '''
    Copies this profile to another name on disk. This method overwrites the 
    destination if it already exists.
    
    @param name: Destination profile
    @type name: string
    @raise ValueError: When the profile does not exist
    @raise OSError: When the profile file cannot be copied to the destination
    '''
    if not self.existsProfile():
      raise ValueError(_('Profile %s does not exist' % profile_name))
    shutil.copy(self.profile, os.path.join(PROFILES_PATH, name+'.profile'))
    
  @classmethod
  def listProfiles(cls):
    '''
    Gets a list of existing profile names.
    
    @return: List of all profiles stored on disk
    @rtype: list of string
    '''
    return [name.split(os.extsep)[0] for name in os.listdir(PROFILES_PATH)]
  
  @classmethod
  def hasDefaultProfiles(cls):
    '''
    Gets if the L{BUILTIN_PROFILES} exist.
    
    @return: Do the default profiles exist?
    @rtype: boolean
    '''
    profiles = cls.listProfiles()
    for name in BUILTIN_PROFILES:
      if name not in profiles:
        return False
    return True
