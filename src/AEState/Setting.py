'''
Defines a class representing a configurable setting in a L{AEState} object.

@author: Peter Parente
@author: Eitan Isaacson
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''

import weakref, new, copy
import os.path

class Proxy(object):
  '''
  Our own proxy object which enables weak references to bound and unbound
  methods and arbitrary callables. Pulls information about the function, 
  class, and instance out of a bound method. Stores a weak reference to the
  instance to support garbage collection.
  '''
  def __init__(self, cb):
    try:
      try:
        self.inst = weakref.ref(cb.im_self)
      except TypeError:
        self.inst = None
      self.func = cb.im_func
      self.klass = cb.im_class
    except AttributeError:
      self.inst = None
      self.func = cb.im_func
      self.klass = None
     
  def __call__(self, *args, **kwargs):
    '''
    Proxy for a call to the weak referenced object. Take arbitrary params to
    pass to the callable.
    
    @raise ReferenceError: When the weak reference refers to a dead object
    '''
    if self.inst is not None and self.inst() is None:
      raise ReferenceError
    elif self.inst is not None:
      # build a new instance method with a strong reference to the instance
      mtd = new.instancemethod(self.func, self.inst(), self.klass)
    else:
      # not a bound method, just return the func
      mtd = self.func
    # invoke the callable and return the result
    return mtd(*args, **kwargs)
  
  def __eq__(self, other):
    '''
    Compare the held function and instance with that held by another proxy.
    
    @param other: Another proxy object
    @type other: L{Proxy}
    @return: Whether this func/inst pair is equal to the one in the other proxy
      object or not
    @rtype: boolean
    '''
    try:
      return self.func == other.func and self.inst() == other.inst()
    except Exception:
      return False

  def __ne__(self, other):
    '''
    Inverse of __eq__.
    '''
    return not self.__eq__(other)
  
class RawRangeView(object):
  '''
  Exposes the raw value, min, and max properties of a L{NumericSetting}.
  '''
  def __init__(self, setting):
    self.setting = setting

  def _getValue(self):
    return self.setting.value
  
  def _setValue(self, val):
    self.setting.value = val
  
  min = property(lambda self: self.setting.min)
  max = property(lambda self: self.setting.max)
  precision = property(lambda self: self.setting.precision)
  value = property(_getValue, _setValue)

class PercentRangeView(RawRangeView):
  '''
  Exposes the percent value, min, and max of a L{PercentRangeSetting}
  '''  
  def _setPercent(self, val):
    '''
    Converts the given percentage to a raw value and stores it.
    
    @param val: New percent to store
    @type val: integer
    @raise TypeError: When the value cannot be converted to a integer
    '''
    # convert percent to value
    val = (val/100.0 * (self.setting.max - self.setting.min))+self.setting.min
    self.setting.value = val
    
  def _getPercent(self):
    '''
    Gets the current value as a percentage.
    
    @return: Current value as %
    @rtype: integer
    '''
    # converts value to percent
    val = self.setting.value
    return int(round(val/float(self.setting.max - self.setting.min)*100, 0))

  precision = property(lambda self: 0)
  min = property(lambda self: 0)
  max = property(lambda self: 100)
  value = property(_getPercent, _setPercent)
  
class RelRawRangeView(RawRangeView):
  '''
  Exposes the raw value, min, and max properties of a L{RelNumericSetting}.
  '''
  def _getMin(self):
    if self.setting.parent is None:
      return self.setting.min
    else:
      return self.setting.rel_min
  
  def _getMax(self):
    if self.setting.parent is None:
      return self.setting.max
    else:
      return self.setting.rel_max

  min = property(_getMin)
  max = property(_getMax)

class RelPercentRangeView(PercentRangeView):
  '''
  Exposes the percent value, min, and max of a L{RelPercentRangeSetting}.
  '''  
  def _getMin(self):
    if self.setting.parent is None:
      return 0
    else:
      return self.setting.rel_min
  
  def _getMax(self):
    if self.setting.parent is None:
      return 100
    else:
      return self.setting.rel_max

  min = property(_getMin)
  max = property(_getMax)

class Group(list):
  '''  
  Named collection of related L{Setting}s.
  
  Both the name and the state of the group should be considered public
  readable.
  
  @ivar state: Object whose properties are contained within this group
  @type state: L{AEState.AEState}
  @ivar name: Name of the group
  @type name: string
  '''
  def __init__(self, state, name=None):
    '''
    Stores the L{Group} state and its name.
    
    @param state: Object whose properties are contained within this group
    @type state: L{AEState.AEState}
    @param name: Name of the group
    @type name: string
    '''
    self.state = state 
    self.name = name
    
  def __str__(self):
    return 'Group %s: %s' % (self.name, list.__str__(self))
    
  def newGroup(self, name):
    '''
    Adds a subgroup to this group with the given name.
    
    @param name: Name of the new group
    @type name: string
    @return: The newly created group
    @rtype: L{Group}
    '''
    grp = Group(self.state, name)
    self.append(grp)
    return grp
  
  def iterSettings(self, reverse=False):
    '''
    Iterates over the L{Setting}s and L{Group}s stored in this L{Group}. Yield
    both the index of the item and the item itself.
    
    @param reverse: Iterate in reverse direction?
    @type reverse: boolean
    @return: Next index and setting object on each iteration
    @rtype: 2-tuple of (integer, L{Setting} or L{Group})
    '''
    if reverse: self.reverse()
    for i, obj in enumerate(self):
      if isinstance(obj, Group):
        yield (i, obj)
      else:
        yield (i, self.state.getSettingObj(obj))
    if reverse: self.reverse()

class Setting(object):
  '''
  A wrapper around a value with a strict type in a L{AEState.AEState} object.
  Provides a label and longer description of the value. Has a flag indicating
  whether the value should be persisted or not. Defines methods for caching the
  current value and restoring it later. Supports the observer pattern to notify
  listeners of changes to the value.
  
  All instance variables should be considered public readable except for those
  prefixed with an underscore. The L{value} property should also be considered
  public writable. L{value} is a property which will correctly notify observers
  when changed.
  
  Subclasses may define new value properties that perform conversions or do
  other tasks. The data retrieved via the value property must always be the 
  data that should be persisted to disk for a setting.
  
  @ivar state: Object in which this setting resides
  @type state: weakref.proxy to L{AEState.AEState}
  @ivar name: Name of the setting
  @type name: string
  @ivar default: Default value of the setting
  @type default: object
  @ivar label: Label of the setting
  @type label: string
  @ivar description: Extended description of the setting
  @type description: string
  @ivar persist: Should this setting value be persisted to disk?
  @type persist: boolean
  @ivar _value: Arbitrary protected value of this setting.
  @type _value: object
  @ivar _cached: Cached value from the last L{save} invocation
  @type _cached: object
  @ivar _observers: Callables to notify on a value change mapped to their
    arguments
  @type _observers: dictionary
  '''
  def __init__(self, state, name, default, label, description, persist):
    '''
    Initializes all instance variables.
    '''
    self.state = weakref.proxy(state)
    self.name = name
    self.default = default
    self.label = label
    self.description = description
    self.persist = persist
    self._value = default
    self._cached = None
    self._observers = {}
    
    # register the state object as a listener for value changes so it can 
    # update its dirty set
    self.addObserver(self.state._makeDirty)
    
  def update(self, setting):
    '''
    Updates this setting with the default value and current value of another
    setting of the same kind.
    
    @param setting: Setting of the same kind as this one
    @type setting: L{Setting}
    '''
    self.default = setting.default
    self.value = setting.value
    
  def copy(self):
    '''
    Makes a shallow copy of this object.
    
    @return: Shallow copy of this setting
    @rtype: L{Setting}
    '''
    return copy.copy(self)

  def _getValue(self):
    '''
    Gets the value.
    
    @return: Current value of the setting
    @rtype: object
    '''
    return self._value
  
  def _setValue(self, val):
    '''
    Sets the value and notifies all observers only if the new value is 
    different from the old.
    
    @param val: New value to store in the setting
    @type val: object
    '''
    if val != self._value:      
      self._value = val
      self._notify()
    
  value = property(_getValue, _setValue)

  def _notify(self):
    '''
    Notifies all observers of a change in value. If an observer is dead, as
    indicated by a ReferenceError, removes it from the list of observers. 
    '''
    dead = []
    for ob, args in self._observers.items():
      try:
        ob(self.state, self, *args)
      except ReferenceError:
        dead.append(ob)
    # remove dead observer proxies
    map(self._observers.pop, dead)
         
  def addObserver(self, ob, *args):
    '''
    Stores a weak reference to the observer to be notified when the value
    of this setting changes. Bound methods B{will} work as observers. 
    
    Additional arguments will be passed to the observer on notification. Weak
    reference proxies to arguments will be used whenever possible to avoid 
    inhibiting garbage collection.
    
    @note: Because weak references are used, a reference may be dead when it
      is later delivered to an observer. The observer may safely ignore this 
      error as it will be caught by the L{_notify} method. Notification will
      cease immediately as soon as the first ReferenceError is encountered.
    @param ob: The observer
    @type ob: callable
    '''
    weak_args = []
    for a in args:
      try:
        weak_args.append(weakref.proxy(a))
      except TypeError:
        weak_args.append(a)
    self._observers[Proxy(ob)] = weak_args
  
  def removeObserver(self, ob):
    '''
    Removes a weak reference to the observer. Pass a strong reference to the
    observer to remove, not a weak reference.
    
    @param ob: The observer
    @type ob: callable
    @raise KeyError: When the given callable is not an observer of this
      setting
    '''
    del self._observers[Proxy(ob)]
    
  def clearObservers(self):
    '''
    Remove all observers.
    '''
    self._observers = []

  def save(self):
    '''
    Caches the current value for later restoration. Only one value may be 
    cached at a time. It's a box, not a stack.
    '''
    self._cached = self._value
  
  def restore(self):
    '''
    Restores the cached value. Sends notification only if the value in the 
    cache is different from the current value.
    '''
    if self._cached is not None:
      if self._value != self._cached:
        # this will send notification only if we're not restoring the same
        # value as the current value
        self.value = self._cached
      self.cached = None
      
  def serialize(self):
    '''
    Gets the current L{_value} as the data to serialize. This default 
    implementation bypasses the property fget method.
    
    @return: Data value to serialize
    @rtype: object
    '''
    return self._value
  
  def unserialize(self, val):
    '''
    Sets the current L{_value} from data that was unserialized. This default 
    implementation bypasses the property fset method.
    
    @param val: Data value to store
    @type val: object
    '''
    self._value = val

class BoolSetting(Setting):
  '''Represents a boolean setting.'''
  def _setValue(self, val):
    '''
    Stores the given value as a boolean.
    
    @param val: New value
    @type val: boolean
    @raise TypeError: When the value cannot be converted to a boolean
    '''
    Setting._setValue(self, bool(val))
    
  value = property(Setting._getValue, _setValue)

class StringSetting(Setting):
  '''Represents a string setting.'''
  def _setValue(self, val):
    '''
    Stores the given value as a string.
    
    @param val: New value
    @type val: string
    @raise TypeError: When the value cannot be converted to a string
    '''
    Setting._setValue(self, str(val))
    
  value = property(Setting._getValue, _setValue)
  
class FilenameSetting(StringSetting):
  '''
  Represents a string filename setting.
  
  @ivar relative_path: Absolute path to which '.' or a filename with no path 
    refers (i.e. the root of the starting filename path)
  @type relative_path: string
  '''
  def __init__(self, state, name, default, label, description, persist, 
               relative_path):
    Setting.__init__(self, state, name, default, label, description, persist)
    self.relative_path = relative_path
    
  def update(self, setting):
    '''
    Updates the relative path in addition to everything done in the parent
    class method.
    
    @param setting: Setting of the same kind as this one
    @type setting: L{FilenameSetting}
    '''
    super(FilenameSetting, self).update(setting)
    self.relative_path = setting.relative_path
    
  def _getValue(self):
    '''
    Gets the filename as a string.
    
    @return: Current filename value
    @rtype: string
    '''
    val = Setting._getValue(self)
    if not os.path.isfile(val):
      val = os.path.join(self.relative_path, os.path.basename(val))
    return val
  
  def _setValue(self, val):
    '''
    Stores the given value as a string.
    
    @param val: New value
    @type val: string
    @raise TypeError: When the value cannot be converted to a string
    '''
    val = str(val)
    if os.path.dirname(val) == self.relative_path:
      new = os.path.basename(val)
    StringSetting._setValue(self, val)

  value = property(_getValue, _setValue)

class NumericSetting(Setting):
  '''
  Represents a numeric setting where the bounds are arbitrarily chosen.
  
  @ivar min: Minimum value in the range
  @type min: number
  @ivar max: Maximum value in the range
  @type max: number
  @ivar precision: Number of decimal places
  @type precision: integer
  '''
  def __init__(self, state, name, default, label, description, persist, min,
               max, precision):
    Setting.__init__(self, state, name, default, label, description, persist)
    self.min = min
    self.max = max
    self.precision = precision
    
  def update(self, setting):
    '''
    Updates the min, max, and precision in addition to everything done in the 
    parent class method.
    
    @param setting: Setting of the same kind as this one
    @type setting: L{NumericSetting}
    '''
    super(NumericSetting, self).update(setting)
    self.min = setting.min
    self.max = setting.max
    self.precision = setting.precision
    
  def _fixBounds(self, val, min, max, prec):
    '''
    Snaps values to the specific range and precision.
    
    @param val: Value to bound
    @type val: numeric
    @param min: Minimum of range
    @type min: numeric
    @param max: Maximum of range
    @type max: numeric
    @param prec: Number of decimal places of precision
    @type prec: integer
    @return: Number snapped to the appropriate range and rounded or truncated
      to the proper precision
    @rtype: numeric
    @raise TypeError: When the value is no a numeric
    '''
    # snap values to range
    if val > max:
      val = max
    elif val < min:
      val = min
    # take precision into account
    if prec == 0:
      val = int(val)
    else:
      val = round(val, prec)
    return val
  
  def _setValue(self, val):
    '''
    Stores the given value as an integer or float with the given precision. 
    Uses round to store floats and int to store integers.
    
    @param val: New value to store
    @type val: numeric
    @raise TypeError: When the value is not a numeric
    '''
    Setting._setValue(self,self._fixBounds(val, self.min, self.max, 
                                           self.precision))
    
  value = property(Setting._getValue, _setValue)
  
  def getView(self):
    '''
    Returns an object that proxies as a view for the value, min, max, and 
    precision for this object. Having this separate view permits other ranges
    to override those values with their own conversions (e.g. percent, relative
    ranges).
    
    @return: A raw view of this range
    @rtype: L{RawRangeView}
    '''
    return RawRangeView(self)

class RangeSetting(NumericSetting):
  '''
  Represents a numeric setting where the bounds are critical.
  '''
  pass
  
class PercentRangeSetting(RangeSetting):
  '''
  Represents a numeric setting that should be scaled for presentation to a 
  user as a percentage between 0% and 100%. The value property of this setting
  returns the raw value while the percent property returns the raw value 
  converted to a percentage.
  '''
  def getView(self):
    '''
    Returns a view that converts raw values to percentages.
    
    @return: A percent range view of this range
    @rtype: L{PercentRangeView}
    '''
    return PercentRangeView(self)

class ChoiceSetting(Setting):
  '''
  Represents a one of many choice setting where the labels to be displayed to
  a user and the values are the same.
  
  @ivar values: Collection of choice values
  @type values: list
  '''
  def __init__(self, state, name, default, label, description, persist,
               choices):
    Setting.__init__(self, state, name, default, label, description, persist)
    self.values = choices
    
  def addChoice(self, choice):
    '''
    Add a new choice possibility.
    
    @param choice: New option
    @type choice: string
    '''
    self.values.append(choice)
    
  def update(self, setting):
    '''    
    Updates the choices in addition to everything done in the parent class
    method.
    
    @param setting: Setting of the same kind as this one
    @type setting: L{ChoiceSetting}
    '''
    super(ChoiceSetting, self).update(setting)
    self.values = setting.values
    
  def _setValue(self, val):
    '''
    Stores the given choice value as the active selection. If the value is not
    one of the available choices, defaults to the first choice.
    
    @param val: New value
    @type val: object
    '''
    if val not in self.values:
      val = self.default
    Setting._setValue(self, val)
    
  value = property(Setting._getValue, _setValue)
  
  def unserialize(self, val):
    '''
    Does not bypass the fset property method so that when data is restored, all
    invalid values are set back to the default.

    @param val: Data value to store
    @type val: object
    '''
    self.value = val
  
class EnumSetting(ChoiceSetting):
  '''
  Represents a one of many choice setting where text labels describe an 
  arbitrary value. The labels collection is intended to be presented to the
  user while the values collection is intended to stored values to be used
  internally only.
  
  @ivar labels: Collection of choice labels
  @type labels: list
  '''
  def __init__(self, state, name, default, label, description, persist, 
               choices):
    # sort alphabetically
    self.labels = choices.keys()
    self.labels.sort()
    ChoiceSetting.__init__(self, state, name, default, label, description, 
                           persist, [choices[key] for key in self.labels])
    
  def getLabel(self, choice):
    '''
    Gets the label for a given choice.
    
    @param choice: Value for which to retrieve the label
    @type choice: object
    @raise ValueError: When L{choice} is not a valid value
    '''
    return self.labels[self.values.index(choice)]
    
  def addChoice(self, label, choice):
    '''
    Add a new choice possibility with the given label.
    
    @param label: Human readable name
    @type label: string
    @param choice: New option
    @type choice: object
    '''
    super(EnumSetting, self).addChoice(choice)
    self.labels.append(label)
    
  def update(self, setting):
    '''    
    Updates the labels in addition to everything done in the parent class
    method.
    
    @param setting: Setting of the same kind as this one
    @type setting: L{EnumSetting}
    '''
    super(EnumSetting, self).update(setting)
    self.labels = setting.labels

class ColorSetting(Setting):
  '''
  Represents a color choice setting, with our without an alpha channel. The
  value must be a collection with 3 or 4 values in it representing RGBA.
  '''
  def _setValue(self, val):
    '''
    Stores the given color and (optionally) alpha information.
    
    @param val: New value
    @type val: collection
    @raise TypeError: When the value is not a 3 or 4 element collection
    '''
    if len(val) != 3 and len(val) != 4:
      raise TypeError
    Setting._setValue(self, val)
    
  value = property(Setting._getValue, _setValue)

class RelNumericSetting(NumericSetting):
  '''  
  Defines a numeric setting that specifies a value relative to another one. If
  the parent style is None, acts just like a regular
  L{AEState.Setting.NumericSetting}.
  
  The relative value is persisted to disk for settings that have an equivalent
  in the parent style. The absolute value is persisted otherwise.
  
  @ivar parent: Parent style of the stored style, None if no parent
  @type parent: L{AEOutput.Style}
  @ivar rel_min: Relative minimum based off of the true -(max - min)
  @type rel_min: numeric
  @ivar rel_max: Relative maximum bassed off of the true max - min
  @type rel_max: numeric
  '''
  def __init__(self, style, name, default, label, description, 
               persist, min, max, precision):
    if style.getDefault() is not None:
      # we have a parent style, set the default to zero, or no different from
      # the parent
      default = 0
    NumericSetting.__init__(self, style, name, default, label, 
                            description, persist, min, max, precision)
    self.parent = style.getDefault()
    self.rel_min = -(max-min)
    self.rel_max = max-min
    
  def update(self, setting):
    '''    
    Updates the relative min and max in addition to everything done in the 
    parent class method.
    
    @param setting: Setting of the same kind as this one
    @type setting: L{RelNumericSetting}
    '''
    super(RelNumericSetting, self).update(setting)
    self.rel_min = setting.rel_min
    self.rel_max = setting.rel_max
  
  def _getValue(self):
    '''
    Gets the current absolute value.
    
    @return: Relative value
    @rtype: numeric
    '''
    # get relative value in this setting object
    val =  NumericSetting._getValue(self)
    if self.parent is None:
      # return value as-is, we have no relative parent
      return val
    else:
      # return relative value + parent value bound to true value range
      val += self.parent.getSettingVal(self.name)
      return self._fixBounds(val, self.min, self.max, self.precision)
  
  def _setValue(self, val):
    '''
    Stores the given absolute value as an integer or float with the given 
    precision. Uses round to store floats and int to store integers.
    
    @param val: New absolute value to store
    @type val: numeric
    @raise TypeError: When the value is not a numeric
    '''
    if self.parent is None:
      # store using Numeric set value method, no relative parent
      val = NumericSetting._setValue(self, val)
    else:
      # store absolute value - parent value bound to the relative value range
      val -= self.parent.getSettingVal(self.name)
      self._value = self._fixBounds(val, self.rel_min, self.rel_max, 
                                    self.precision)

  value = property(_getValue, _setValue)
  
  def getView(self):
    '''
    Gets a relative range view that returns the proper min and max values
    depending on if the value is relative or not.
    
    @return: A relative view of this range
    @rtype: L{RelRawRangeView}
    '''
    return RelRawRangeView(self)
  
class RelRangeSetting(RelNumericSetting):
  '''
  Defines a range setting that specifies a value relative to another one. If 
  the parent style is None, acts just like a regular 
  L{RangeSetting}.
  '''
  pass

class RelPercentRangeSetting(RelRangeSetting):
  '''
  Defines a percent setting that specifies a value relative to another one. If 
  the parent style is None, acts just like a regular
  L{PercentRangeSetting}.
  
  The relative percentage of this setting is additive,
  '''
  def getView(self):
    '''
    Gets a relative range view that returns the proper min and max values
    depending on if the value is relative or not.
    
    @return: A relative percent view of this range
    @rtype: L{RelPercentRangeView}
    '''
    return RelPercentRangeView(self)
