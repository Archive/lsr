'''
Defines an abstract base class for all persistent and/or configurable setting
classes.

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''

from Setting import *

def _uuid():
  '''
  Generate a new unique identifier to help in comparison of L{AEState} copies.
  
  @return: Unique ID
  @rtype: integer
  '''
  i = 0
  while 1:
    yield i
    i += 1
uuid = _uuid()

class AEState(object):
  '''
  Abstract base class for objects containing data that will have their data 
  serialized to disk or configured by the user. All new* methods construct
  L{Setting} objects which contain metadata describing setting values for the
  purpose of checking bounds, generating configuration dialogs, etc.
  
  All subclasses of L{AEState} should override L{init} and L{getGroups}. The
  init method should be used to create new settings and initialize non-setting
  instance variables. The overriden init does not need to invoke any base class
  init methods to create inherited settings. All baser class settings are 
  created in the __init__ constructor instead. The getGroups method should
  call L{newGroup} to create group objects containing the names of related 
  settings to aid the generation of configuration dialogs. Group objects 
  themselves also have newGroup methods to support nested groupings.
  
  Two attribute names are reserved by L{AEState} objects. L{settings} is the
  dictionary containing all L{Setting} instances. L{dirty} is a set that
  stores the names of settings that have been modified in the current instance.
  The list is automatically reset when the instance is persisted. The
  L{isDirty}, L{makeClean}, L{iterDirty} methods may be used to check if any
  part of the state is dirty, clear the list of dirty attributes, and iterate
  over the names and values of the dirty attributes to enable optimization at
  higher levels.
  
  Note that the L{AEState} object itself is not designed to be persisted to 
  disk, but rather to act as a delegate for settings to be persisted. The 
  reason for this is that not all persistence mechanisms can support the
  serialization of Python objects.
  
  @ivar dirty: Set of dirty attribute names in this state object
  @type dirty: set
  @ivar settings: Dictionary of named L{Setting} objects
  @type settings: dictionary
  @ivar ident: Unique ID for this state object which is given to all copies
  @type ident: integer
  '''
  def __init__(self):
    '''
    Initialize the dirty set and settings dict.
    '''
    # special init to avoid call to __setattr__
    self.__dict__['settings'] = {}
    self.__dict__['dirty'] = set()
    self.__dict__['ident'] = uuid.next()
  
  def init(self):
    '''
    Does nothing by default. Use to create settings after instantiation.
    '''
    pass
        
  def getGroups(self):
    '''
    Gets the root L{Setting.Group} object and all of its child L{Setting} names
    and nested groups.
    
    @raise NotImplementedError: When not overridden in a subclass, indicating
      configuration is not supported
    '''
    raise NotImplementedError
  
  def __eq__(self, other):
    '''
    Compares two state objects for equality based on their attributes.
    
    @param other: Another state object
    @type other: L{AEState}
    '''
    try:
      return self.ident == other.ident
    except Exception:
      return False
  
  def __ne__(self, other):
    '''
    Compares two state objects for inequality based on their attributes.
    
    @param other: Another state object
    @type other: L{AEState}
    '''
    return not self.__eq__(other)
  
  def copy(self):
    '''
    Makes a copy of this state object and all of its L{Setting}s.
    '''
    c = self.__class__()
    c.dirty = self.dirty
    c.ident = self.ident
    for name, val in self.settings.items():
      c.settings[name] = val.copy()
    return c
  
  def addSetting(self, setting):
    '''
    Adds a copy of an existing setting to this state object under the same 
    name.
    
    @param setting: Existing setting to copy and store
    @type setting: L{Setting}
    '''
    self.settings[setting.name] = setting.copy()
  
  def newGroup(self, label=None):
    '''
    Builds a new L{Group} object relating settings. The group is not stored
    internally, just returned for temporary use by another object.
    
    @param label: Label for the new L{Group}
    @type label: string
    '''
    return Group(self, label)
  
  def newString(self, name, default, label, description='', persist=True):
    '''
    Adds a new L{StringSetting} to this group.
    
    @param name: New name of the setting
    @type name: string
    @param default: Default value of the setting
    @type default: string
    @param label: Label for the new L{Setting}
    @type label: string
    @param description: Extended description of the L{Setting}
    @type description: string
    @param persist: Persist the setting value to disk?
    @type persist: boolean
    @return: New setting object
    @rtype: L{StringSetting}
    '''
    s = StringSetting(self, name, default, label, description, persist)
    self.settings[name] = s
    return s
    
  def newFilename(self, name, default, label, relative_path, description='', 
                  persist=True):
    '''
    Adds a new L{FilenameSetting} to this group.
    
    @param name: New name of the setting
    @type name: string
    @param default: Default value of the setting
    @type default: string
    @param label: Label for the new L{Setting}
    @type label: string
    @param relative_path: Absolute path to which '.' or a filename with no 
      path refers
    @type relative_path: string
    @param description: Extended description of the L{Setting}
    @type description: string
    @param persist: Persist the setting value to disk?
    @type persist: boolean
    @return: New setting object
    @rtype: L{FilenameSetting}
    '''
    relative_path = os.path.realpath(relative_path)
    if os.path.isfile(relative_path):
      relative_path = os.path.dirname(relative_path)
    s = FilenameSetting(self, name, default, label, description, persist, 
                        relative_path)
    self.settings[name] = s
    return s
  
  def newBool(self, name, default, label, description='', persist=True):
    '''
    Adds a new L{BoolSetting} to this group.
    
    @param name: New name of the setting
    @type name: string
    @param default: Default value of the setting
    @type default: boolean
    @param label: Label for the new L{Setting}
    @type label: string
    @param description: Extended description of the L{Setting}
    @type description: string
    @param persist: Persist the setting value to disk?
    @type persist: boolean
    @return: New setting object
    @rtype: L{BoolSetting}
    '''
    s = BoolSetting(self, name, default, label, description, persist)
    self.settings[name] = s
    return s
    
  def newNumeric(self, name, default, label, min, max, precision, 
                 description='', persist=True):
    '''
    Adds a new L{NumericSetting} to this group.
    
    @param name: New name of the setting
    @type name: string
    @param default: Default value of the setting
    @type default: float
    @param label: Label for the new L{Setting}
    @type label: string
    @param min: Minimum value in the range
    @type min: number
    @param max: Maximum value in the range
    @type max: number
    @param precision: Number of decimal places
    @type precision: integer
    @param description: Extended description of the L{Setting}
    @type description: string
    @param persist: Persist the setting value to disk?
    @type persist: boolean
    @return: New setting object
    @rtype: L{NumericSetting}
    '''
    s = NumericSetting(self, name, default, label, description, persist, min, 
                       max, precision)
    self.settings[name] = s
    return s

  def newRange(self, name, default, label, min, max, precision, description='',
               persist=True):
    '''
    Adds a new L{RangeSetting} to this group.
    
    @param name: New name of the setting
    @type name: string
    @param default: Default value of the setting
    @type default: float
    @param label: Label for the new L{Setting}
    @type label: string
    @param min: Minimum value in the range
    @type min: number
    @param max: Maximum value in the range
    @type max: number
    @param precision: Number of decimal places
    @type precision: integer
    @param description: Extended description of the L{Setting}
    @type description: string
    @param persist: Persist the setting value to disk?
    @type persist: boolean
    @return: New setting object
    @rtype: L{RangeSetting}
    '''
    s = RangeSetting(self, name, default, label, description, persist, min, 
                     max, precision)
    self.settings[name] = s
    return s
    
  def newPercent(self, name, default, label, min, max, precision, 
                 description='', persist=True):
    '''
    Adds a new L{PercentRangeSetting} to this group.
    
    @param name: New name of the setting
    @type name: string
    @param default: Default value of the setting
    @type default: float
    @param label: Label for the new L{Setting}
    @type label: string
    @param min: Minimum value in the range to be shown as 0%
    @type min: number
    @param max: Maximum value in the range to be shown as 100%
    @type max: number
    @param precision: Number of decimal places
    @type precision: integer
    @param description: Extended description of the L{Setting}
    @type description: string
    @param persist: Persist the setting value to disk?
    @type persist: boolean
    @return: New setting object
    @rtype: L{PercentRangeSetting}
    '''
    s = PercentRangeSetting(self, name, default, label, description, persist, 
                            min, max, precision)
    self.settings[name] = s
    return s
  
  def newChoice(self, name, default, label, choices, description='', 
                persist=True):
    '''
    Adds a new L{ChoiceSetting} to this group.
    
    @param name: New name of the setting
    @type name: string
    @param default: Default value of the setting
    @type default: string
    @param label: Label for the new L{Setting}
    @type label: string
    @param choices: Collection of choices
    @type choices: list
    @param description: Extended description of the L{Setting}
    @type description: string
    @param persist: Persist the setting value to disk?
    @type persist: boolean
    @return: New setting object
    @rtype: L{ChoiceSetting}
    '''
    s = ChoiceSetting(self, name, default, label, description, persist, 
                      choices)
    self.settings[name] = s
    return s

  def newColor(self, name, default, label, description='', persist=True):
    '''
    Adds a new L{ColorSetting} to this group.
    
    @param name: New name of the setting
    @type name: string
    @param default: Default value of the setting
    @type default: 3 or 4-tuple of integer
    @param label: Label for the new L{Setting}
    @type label: string
    @param description: Extended description of the L{Setting}
    @type description: string
    @param persist: Persist the setting value to disk?
    @type persist: boolean
    @return: New setting object
    @rtype: L{ColorSetting}
    '''
    s = ColorSetting(self, name, default, label, description, persist)
    self.settings[name] = s
    return s
    
  def newEnum(self, name, default, label, choices, description='', 
              persist=True):
    '''
    Adds a new L{EnumSetting} to this group.
    
    @param name: New name of the setting
    @type name: string
    @param default: Default value of the setting
    @type default: integer
    @param label: Label for the new L{Setting}
    @type label: string
    @param choices: Collection of choices mapped to the values they represent
    @type choices: dictionary
    @param description: Extended description of the L{Setting}
    @type description: string
    @param persist: Persist the setting value to disk?
    @type persist: boolean
    @return: New setting object
    @rtype: L{EnumSetting}
    '''
    s = EnumSetting(self, name, default, label, description, persist, choices)
    self.settings[name] = s
    return s
      
  def __setattr__(self, name, val):
    '''
    Stores a setting value if the setting is already defined by the give name
    or an instance attribute if the name doesn't not define a setting.
    Provided for convenience over using L{setSettingVal}.
    
    @param name: Name of the setting or instance attribute
    @type name: string
    @param val: Arbitrary value to store
    @type val: object
    '''
    try:
      self.__dict__['settings'][name].value = val
    except KeyError:
      self.__dict__[name] = val
  
  def __getattr__(self, name):
    '''
    Gets a setting value if the setting is already defined by the give name
    or an instance attribute if the name doesn't not define a setting.
    Provided for convenience over using L{getSettingVal}.
    
    @param name: Name of the setting or instance attribute
    @type name: string
    @return: Arbitrary value to get
    @rtype: object
    @raise AttributeError: When the name does not define a setting or instance
      attribute
    '''
    try:
      # try to fetch a setting first
      return self.__dict__['settings'][name].value
    except KeyError:
      pass
    try:
      # then try to fetch an instance attribute
      return self.__dict__[name]
    except KeyError:
      raise AttributeError("'%s' object has no attribute '%s'" % 
                           (self.__class__.__name__, name))

  def save(self):
    '''Invokes L{Setting.Setting.save} on all settings in the state.'''
    for sett in self.settings.values():
      sett.save()
      
  def restore(self):
    '''Invokes L{Setting.Setting.restore} on all settings in the state.'''
    for sett in self.settings.values():
      sett.restore()
      
  def hasSetting(self, name):
    '''
    Gets if this object has a setting with the given name.
    
    @param name: Name of the setting
    @type name: string
    @return: Has the setting or not?
    @rtype: boolean
    '''
    return self.settings.has_key(name)
  
  def getSettingObj(self, name):
    '''
    Retrieves the L{Setting} object with the given name.
    
    @param name: Name of the setting
    @type name: string
    @return: The named setting object
    @rtype: L{Setting}
    @raise KeyError: When a L{Setting} with the given name is not defined
    '''
    return self.settings[name]
  
  def getSettingVal(self, name):
    '''
    Convenience method for getting the value of a L{Setting} object.

    @param name: Name of the setting
    @type name: string
    @return: Current value of a setting
    @rtype: object
    @raise KeyError: When a L{Setting} with the given name is not defined
    '''
    return self.settings[name].value
  
  def setSettingVal(self, name, val):
    '''
    Convenience method for setting the value of a L{Setting} object.
    
    @param name: Name of the setting
    @type name: string
    @param val: Value to store
    @type val: object
    @raise KeyError: When a L{Setting} with the given name is not defined
    '''
    self.settings[name].value = val
   
  def serialize(self):
    '''
    Grabs all L{Setting} values and stores them in a dictionary keyed by 
    setting name. Invokes L{restore} first to ensure only user accepted values
    are persisted.
    
    @return: Mapping from setting names to values
    @rtype: dictionary
    '''
    self.restore()
    return dict([(name, sett.serialize()) for name, sett in 
                 self.settings.items() if sett.persist])
  
  def unserialize(self, data):
    '''
    Stores all L{Setting} values given a dictionary mapping setting names to
    values. Ignores names in the dictionary that do not exist in L{settings}.
    
    @param data: Mapping from setting names to values
    @type data: dictionary
    '''
    for name, value in data.items():
      try:
        self.settings[name].unserialize(value)
      except KeyError:
        # nice! filters out old settings that are no longer defined
        pass
      
  def isDirty(self):
    '''
    Gets if values in L{settings} have changed since the last call to 
    L{makeClean}.
    
    @return: Dirty or not?
    @rtype: boolean
    '''
    return len(self.dirty) > 0
  
  def makeClean(self):
    '''
    Resets the dirty set to empty.
    '''
    self.dirty = set()
    
  def _makeDirty(self, state, setting):
    '''
    Adds the given setting name to the dirty set.
    
    @param state: State object to which the setting is attached
    @type state: weakref.proxy to L{AEState}
    @param setting: Setting that changed value
    @type setting: L{Setting}
    '''
    self.dirty.add(setting.name)
    
  def iterDirty(self):
    '''
    Iterates over all of the names in L{dirty} in no particular order.
    
    @return: Name of a dirty setting
    @rtype: string
    '''
    for name in self.dirty:
      yield name
  
  def __iter__(self):
    '''
    Iterates over all of the setting objects in L{settings} 
    in no particular order.
    
    @return: A setting
    @rtype: L{Setting}
    '''
    for setting in self.settings.values():
      yield setting
