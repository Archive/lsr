'''
Defines a class that extends L{pyLinAcc.Event.Manager} to support L{AEEvent}s.

@var MAX_EVENT: Maximum number of raw or L{AEEvent}s to process in a single
  callback from the main loop
@type MAX_EVENTS: integer
@var EVENT_INTERVAL: Number of ms between calls to pump the event queues
@type EVENT_INTERVAL: integer

@author: Peter Parente
@author: Pete Brunet
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
import pyLinAcc
import Queue, time, logging
import AEEvent, AEMonitor, UIRegistrar, AEConstants
from AEInterfaces import implements
from UIRegistrar import MONITOR
from i18n import _

log = logging.getLogger('Event')
EVENT_INTERVAL = 10
MAX_EVENTS = 10

class EventManager(pyLinAcc.Event.Manager):
  '''
  Watches for AT-SPI events and reports them to observers. Maintains a queue
  of L{AEEvent} that are executed on a set interval.

  @ivar acc_eng: Reference to the L{AccessEngine} object
  @type acc_eng: L{AccessEngine}
  @ivar view_manager: Reference to the L{ViewManager} object
  @type view_manager: L{ViewManager}
  @ivar tier_manager: Reference to the L{TierManager} object
  @type tier_manager: L{TierManager}
  @ivar raw_queue: FIFO queue of L{pyLinAcc.Event.Event}s to be dispatched
  @type raw_queue: Queue.Queue
  @ivar ae_queue: FIFO queue of L{AEEvent} to be executed
  @type ae_queue: Queue.Queue
  @ivar timer_id: ID of the timer pumping the event queue
  @type timer_id: integer
  @ivar last_event:
  @type last_event: L{AEEvent}
  '''
  def __init__(self, acc_eng):
    '''
    Initializes the parent class. Creates an event queue. Asks the
    L{AccessEngine} to schedule interval callbacks to the
    L{EventManager.onPumpRawEvents} and L{EventManager.onPumpAEEvents} methods.

    @param acc_eng: Reference to the L{AccessEngine} that created this manager
    @type acc_eng: L{AccessEngine}
    '''
    pyLinAcc.Event.Manager.__init__(self)
    self.acc_eng = acc_eng
    self.ae_queue = Queue.Queue()
    self.raw_queue = Queue.Queue()
    self.monitors = AEMonitor.MonitorCollection()
    self.last_event = None

    # ask the access engine to notify us on a set interval to pump both the
    # raw and AE event queues
    self.acc_eng.addTimer(self.onPumpRawEvents, EVENT_INTERVAL)
    self.acc_eng.addTimer(self.onPumpAEEvents, EVENT_INTERVAL)

  def init(self, view_man, tier_man, **kwargs):
    '''
    Stores references to the L{ViewManager} and L{TierManager}. Instantiates all
    L{AEMonitor} UIEs registered with L{UIRegistrar} to be loaded at startup. 
    Called by L{AccessEngine} at startup.

    @param tier_man: Reference to the L{TierManager}
    @type tier_man: L{TierManager}
    @param view_man: Reference to the L{ViewManager}
    @type view_man: L{ViewManager}
    @param kwargs: References to managers not of interest here
    @type kwargs: dictionary
    '''
    # store other manager references
    self.view_manager = view_man
    self.tier_manager = tier_man
    # load all startup monitors
    reg = UIRegistrar
    mons = reg.loadAssociated(MONITOR, self.acc_eng.getProfile())
    self.addMonitors(*mons)
    
  def close(self):
    '''
    Throws away all events in the L{raw_queue} and L{ae_queue}. Calls the parent
    class close method.
    '''
    super(EventManager, self).close()
    self.raw_queue = Queue.Queue()
    self.ae_queue = Queue.Queue()
    self.monitors.clear()

  def addMonitors(self, *monitors):
    '''
    Adds one or more L{AEMonitor}s to the list of monitors to be 
    notified about events.
    
    @param monitors: L{AEMonitor}s to notify
    @type monitors: tuple of L{AEMonitor}s
    '''
    self.monitors.add(pyLinAcc.Event.Event, monitors)
    
  def getMonitors(self):
    '''
    @return: Collection of all loaded L{AEMonitor}s
    @rtype: L{AEMonitor.MonitorCollection}
    '''
    return self.monitors

  def showEvent(self, event):
    '''    
    Informs L{AEMonitor}s added via L{addMonitors} of a raw event.
    
    @param event: Raw accessibility event
    @type event: L{pyLinAcc.Event.Event}
    '''
    self.monitors.show(event=event)

  def _executeEvent(self, event):
    '''
    Executes the provided L{AEEvent}. Provides references to all managers as 
    named parameters to the event's execute method. Catches and logs all 
    exceptions to prevent bad events from getting re-queued for later execution.

    @param event: Event to execute
    @type event: L{AEEvent}
    @return: Did the event execute properly or does it need to be re-queued for
        later execution?
    @rtype: True
    '''
    # don't let our pump die if something fails
    try:
      return event.execute(view_manager=self.view_manager,
                           tier_manager=self.tier_manager,
                           event_manager=self)
    except LookupError:
      # just ignore dead objects
      return True
    except Exception, e:
      # don't get bad events stuck in a loop
      log.exception(str(e))
      return True

  def flushEvents(self):
    '''
    Flushes the event queue by destroying it and recreating it.
    '''
    self.ae_queue = Queue.Queue()
    self.raw_queue = Queue.Queue()

  def postEvents(self, *events):
    '''    
    Add L{AEEvent}s to the queue. Events can be added by any part of the system
    Events marked with immediate priority are executed now instead of being
    added to the queue. Immediate execution is necessary for some kinds of
    events.
    
    @param events: Events to queue for dispatch
    @type events: tuple of L{AEEvent}s
    '''
    for e in events:
      if e is not None:
        if e.priority == AEConstants.EXEC_IMMEDIATE:
          # some events need to execute immediately (e.g. view update)
          if self._executeEvent(e) == False:
            # if it doesn't work now, try again later
            log.debug('re-queuing immediate AEEvent')
            self.ae_queue.put_no_wait(e)
        else:
          self.ae_queue.put_nowait(e)
        
  def handleEvent(self, event):
    '''
    Overrides the base implementation of L{handleEvent} so that all 
    L{pyLinAcc.Event.Event}s of interest are queued rather than immediately
    dispatched. This breaks the synchronous connection between LSR and the rest
    of the desktop. See L{pyLinAcc.Event.Manager.handleEvent} for more details.
    
    @param event: Wrapped AT-SPI event
    @type event: L{pyLinAcc.Event.Event}
    '''
    self.raw_queue.put_nowait(event)
        
  def onPumpRawEvents(self):
    '''
    Pumps all L{pyLinAcc.Event.Event}s in the L{raw_queue}. Calls the inherited
    L{pyLinAcc.Event.Manager._dispatchEvent} method to forward to the event to
    all registered clients. Catches and logs all exceptions to prevent them
    from propogating to the main loop that called this method.
    
    This method is called on a set interval by the main event loop in
    L{AccessEngine.AccessEngine.run}. It is registered for callbacks in the
    constructor of this class. 
    
    @return: True to continue receiving notifications
    @rtype: boolean
    '''
    for i in xrange(MAX_EVENTS):
      try:
        # get next waiting events
        event = self.raw_queue.get_nowait()
      except Queue.Empty:
        break
      # notify monitors that the event was received
      self.showEvent(event)
      try:
        self._dispatchEvent(event)
      except LookupError:
        break
      except Exception:
        # catch all exceptions to prevent our main loop from dying
        log.exception('Raw event exception')
    return True

  def onPumpAEEvents(self):
    '''
    Pumps all L{AEEvent}s in the L{ae_queue}. Executes each event pumped. 
    Re-queues events for later execution that return False from their execute 
    methods.
    
    This method is called on a set interval by the main event loop in
    L{AccessEngine.AccessEngine.run}. It is registered for callbacks in the
    constructor of this class.
    
    @return: True to continue receiving notifications
    @rtype: boolean
    '''
    later = []
    for i in xrange(MAX_EVENTS):
      try:
        event = self.ae_queue.get_nowait()
      except Queue.Empty:
        break
      rv = self._executeEvent(event)
      if rv == False:
        # try to execute the event again later
        later.append(event)
        log.debug('re-queuing AEEvent')
      self.last_event = event
    # put all events that failed to execute back in the queue for later
    map(self.postEvents, later)
    return True

