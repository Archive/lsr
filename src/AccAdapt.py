'''
Defines machinery for stating interfaces and adapting objects to those 
interfaces on demand.

@var _adapters: Lists of adapters and conditions under which they should be 
  applied keyed by the interface provided
@type _adapters: dictionary
@var _default_adapters: Adapters to be used when no better adapter is available 
  for a given subject or interface keyed by the interface provided
@type _default_adapters: dictionary

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
import sys, logging, weakref

log = logging.getLogger('Adapt')

# adapter registries
_adapters = {}
_default_adapters = {}

class AdaptationError(RuntimeError):
  '''Error raised when a suitable adapter could not be found.'''
  pass
        
def registerAdapters(adapters=[]):
  '''
  Registers all L{Adapter}s in the caller's local namespace. An adapter must
  by a class deriving from L{Adapter} and should have a class variable named
  I{provides} containing a list of L{Interface}s that the adapter implements. An
  adapter can also expose a I{when} attribute containing a condition under which
  the adapter can be used. If I{when} is not given, the adapter is used as the
  default for the given interface. Only one default may be registered per
  interface.
  
  @note: This function uses a small hack to get the caller's locals. This hack
    can be removed in favor of the caller passing in its locals dictionary
    explicitly, but that places more responsibility on the caller who may
    already forget to call this function to begin with.
  '''
  if not len(adapters):
    # get the caller's locals
    frame = sys._getframe(1)
    adapters = frame.f_locals.values()
  # iterate over all objects in the caller's namespace
  for item in adapters:
    try:
      # get the item's base classes
      is_adapter = issubclass(item, Adapter)
    except:
      # the item is not a class
      continue
    # if one of the base classes is Adapter
    if is_adapter:
      adapter = item
      if adapter.when is None:
        # no condition implies default
        condition = None
      elif callable(adapter.when):
        # just store it for later invocation
        condition = adapter.when
      else:
        # ignore unknown when statements
        continue
      if adapter.singleton:
        # if marked as singleton, store one instance of adapter instead of class
        adapter = adapter()
      for interface in adapter.provides:
        # register the caller under all of the interfaces it provides
        _add(adapter, interface, condition)

def _add(adapter, interface, condition=None):
  '''
  Adds an adapter to the registry. If condition is specified, the adapter is
  added to the L{_adapters} dictionary. If no condition is given, the adapter
  is set as the default for the given interface in the L{_default_adapters}
  dictionary.
  
  @param adapter: Instance to be registered as an L{Adapter}
  @type adapter: L{Adapter}
  @param interface: L{Interface} provided by the L{Adapter}
  @type interface: L{Interface} class
  @param condition: Function expressing a condition under which the 
    adapter can be applied to a subject. The subject is available in the
    variable I{subject} in the condition. A value of None implies the L{Adapter}
    is a default to be used when no other L{Adapter} is available for this 
    interface.
  @type condition: string
  '''
  if condition is None:
    _default_adapters[interface] = adapter
  else:
    possible = _adapters.setdefault(interface, [])
    possible.append((condition, adapter))

def _get(interface, subject):
  '''
  Gets an appropriate adapter to the desired interface for the given subject.
  First tries to find an L{Adapter} to the L{Interface} by satisfying one of the 
  conditions of the registered L{Adapter}s. If that fails, trys to use a default
  L{Adapter} to the L{Interface}. If no default is registered, raises an
  exception.
  
  @param interface: L{Interface} to which the subject should be adapted
  @type interface: L{Interface} class
  @param subject: Object to adapt to the interface
  @type subject: object
  @raise AdaptationError: When no suitable adapter is available for the subject
  '''
  try:
    # get all adapters to this interface
    possible = _adapters[interface]
  except KeyError:
    pass
  else:
    # check if this subject satisfies any adapter condition
    for condition, adapter in possible:
      try:
        if condition(subject):
          #log.debug('using adapter %s for %s', adapter, subject)
          return adapter(subject)
      except Exception, e:
        #log.debug('could not evaluate %s: %s', adapter.when, str(e))
        continue
  try:
    # try to use a default adapter
    adapter = _default_adapters[interface]
    #log.debug('using default adapter %s for %s', adapter, subject)
    return adapter(subject)
  except KeyError:
    raise AdaptationError('Could not adapt %s to %s' % (subject, interface))

class Interface(object):
  '''
  Base class for all interfaces. Acts as a factory for instantiating the proper
  registered adapter for the given L{Interface} subclass and subject. For 
  example, assume INavigable is a subclass of this class. The code:  

  adapted_object = INavigable(some_object)

  will return an object adapting some_object to the INavigable interface if such
  and adapter exists. If some_object has an attribute called providesInterfaces
  which contains INavigable, the original object will be returned. If no 
  suitable adapter exists, an exception is raised.
  
  The L{_get} function does most of the work in retrieving an appropriate 
  adapter.
  '''
  def __new__(interface, subject=None):
    '''
    Creates an adapter instance providing the given interface for the given
    subject.
    
    @param interface: Desired interface for the subject
    @type interface: L{Interface} class
    @param subject: Object to adapt to the interface
    @type subject: object
    @raise AdaptationError: When no suitable adapter exists to provide the
      desired interface for the given subject
    '''
    #try:
      #if interface in subject.provides: 
        #return subject
    #except AttributeError:
      #pass
    try:
      return subject.getAdapter(interface)(subject)
    except (AttributeError, KeyError):
      pass
    adapter = _get(interface, subject)
    try:
      subject.cacheAdapter(interface, adapter)
    except AttributeError:
      pass
    return adapter
  
class Adapter(object):
  '''
  Base class for all adapter classes. Has default class attributes indicating
  the adapter provides no interfaces by default. Has a default constructor that
  takes and stores the subject being adapted.
  
  @cvar provides: L{Interface}s provided by this adapter
  @type provides: list of L{Interface}
  @cvar when: Condition under which this adapter is applicable to the given
    subject. The condition can either be a staticmethod callable that results
    in a boolean value or a string that will be evaluated as a boolean Python 
    expression in the namespace in which it is defined. This implies any 
    variables in the global namespace of the L{Adapter} subclass can be used
    in the when clause. String conditions are only evaluated once at startup
    rather than on each call. If the condition is None, the L{Adapter} is 
    considered a default.
  @type when: string or callable
  @cvar singleton: If True, only one instance of this adapter will be registered
    for use such that it is reused for all subjects. If False, this class will
    be registered for use such that it will act as a factory for producing 
    adapter instances for all subjects.
  @type singleton: boolean
  '''
  provides = []
  when = None
  singleton = False

  def __init__(self, subject=None):
    '''
    Stores the subject of the adapter.
    
    @param subject: Object being adapted
    @type subject: object
    '''
    self.subject = subject
  
  def __call__(self, subject):
    '''
    Stores the subject of the adapter.
    
    @param subject: Object being adapted
    @type subject: object
    @return: Reference to this instance
    @rtype: object
    '''
    self.subject = subject
    return self

class PORAdapter(Adapter):
  '''
  Convenience base class for L{Adapter}s for L{POR}s. Provides direct access
  to L{POR} data through instance variables.
  
  @ivar accessible: Reference to the accessible in the L{POR}
  @type accessible: L{pyLinAcc.Accessible}
  @ivar item_offset: Reference to the item offset in the L{POR}
  @type item_offset: integer
  @ivar char_offset: Reference to the character offset in the L{POR}
  @type char_offset: integer
  '''
  def __init__(self, subject=None):
    '''
    Override storing of subject L{POR} in adapter to also store its accessible
    and item offset in instance variables for convenince.
    
    @param subject: Point of regard adapted by this object
    @type subject: L{POR}
    '''
    try:
      self.subject = weakref.proxy(subject)
    except TypeError:
      self.subject = subject
    if self.subject is not None:
      self.accessible = subject.accessible
      self.item_offset = subject.item_offset
      self.char_offset = subject.char_offset
  
  def __call__(self, subject):
    '''
    Override storing of subject L{POR} in adapter to also store its accessible
    and item offset in instance variables for convenince.
    
    @param subject: Point of regard adapted by this object
    @type subject: L{POR}
    '''
    try:
      self.subject = weakref.proxy(subject)
    except TypeError:
      self.subject = subject
    self.accessible = subject.accessible
    self.item_offset = subject.item_offset
    self.char_offset = subject.char_offset
    return self