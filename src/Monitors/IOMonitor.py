'''
Defines a GTK dialog for buffering L{AEEvent}s and how L{Perk}s and L{Task}s 
handle them.

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
from AEConstants import LAYER_NAMES, SEMANTIC_NAMES
from DeviceManager import InputEvent, OutputEvent, DeviceEvent
from GTKEventDialog import GTKEventDialog
import AEInput, AEOutput, AEConstants
from i18n import _
import pango

__uie__ = dict(kind='monitor', profiles=['developer'])

class IOMonitor(GTKEventDialog):
  '''
  Logs information about input and output to a GUI window.
  
  @ivar last_dev: Name of the last device to which output was sent
  @type last_dev: string
  '''
  def init(self):
    '''
    Initialize the monitor instance variables.
    '''
    super(IOMonitor, self).init()
    self.last_dev = None
    # insert style tags used in this monitor into textbuffer
    self._insert_one_tag_into_buffer('gray_fg', {'foreground':'#A5A5A5'})
    self._insert_one_tag_into_buffer('red_fg', {'foreground':'red'})
    '''
    Gets the localized name of this monitor.
    
    @return: Monitor name
    @rtype: string
    '''
    return _('I/O monitor')
  
  def getEventNames(self):
    '''
    Gets the event categories to be displayed in the View menu for filtering.
    
    @return: Event categories
    @rtype: list of string
    '''
    return AEInput.getNames() + AEOutput.getNames()
  
  def getEventDefaults(self):
    '''
    Gets the default event categories to check in the View menu.
    
    @return: Event categories
    @rtype: list of string
    '''
    return AEInput.getDefaults() + AEOutput.getDefaults()
  
  def getEventType(self):
    '''
    Gets the L{DeviceManager.DeviceEvent} base class to indicate the type 
    of events this monitor wants to buffer.
    
    @return: Base type of the event this monitor should buffer
    @rtype: L{DeviceManager.DeviceEvent} class
    '''
    return DeviceEvent
      
  def show(self, event, value=None, dev=None, sem=None, layer=None):
    '''
    Buffers additional details about how the L{AEEvent} was handled by various
    L{Perk}s and L{Task}s. The event is only provided as a means of filtering 
    information.
    
    @param event: Wrapper giving the direction of a command (input/output) and 
      the command name
    @type event: L{DeviceManager.DeviceEvent}
    @param value: Value of the command sent to/from a device
    @type value: object
    @param sem: Semantic constant indicating the kind of output
    @type sem: integer
    @param layer: Layer constant indicating from where the event came
    @type layer: integer
    @param dev: Device on which the event occurred
    @type dev: L{AEInput} or L{AEOutput}
    @raise IOError: When the monitor is no longer accepting data to buffer 
    '''
    if not self.isInitialized():
      raise IOError
    elif not self._isShown(event.getName()):
      return
    
    if dev and dev.getName() != self.last_dev:
      # show the name of a device
      name = dev.getName()
      self.last_dev = name
      self._queueText('\n\n{device: %s}\n' % name, tags=['red_fg'])

    if event.cmd == AEConstants.CMD_TALK:
      self._queueText(' {%s: %s} ' %(event.getName(), SEMANTIC_NAMES.get(sem)), 
                      tags=['gray_fg'])
    elif event.cmd == AEConstants.CMD_STOP:
      self._queueText('{%s}\n' % event.getName(), tags=['gray_fg'])
    elif event.cmd == AEConstants.CMD_STRING:
        self._queueText(str(value))
    elif event == AEConstants.CMD_FILENAME:
        self._queueText('{%s: %s}' % (event.getName(), str(value)), 
                        tags=['gray_fg'])
    else:
      self._queueText('{%s: %s}\n' % (event.getName(), str(value)), 
                      tags=['gray_fg'])
