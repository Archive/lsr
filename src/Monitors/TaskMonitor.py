'''
Defines a GTK dialog for buffering L{AEEvent}s and how L{Perk}s and L{Task}s 
handle them.

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
import pygtk
pygtk.require('2.0')
import gtk
import AEEvent, AEConstants
from GTKEventDialog import GTKEventDialog
from i18n import _
import time, datetime
import pango

__uie__ = dict(kind='monitor', profiles=['developer'])

class TaskMonitor(GTKEventDialog):
  '''
  Logs information about Tasks running in LSR Perks to a GUI window.
  
  @ivar filter_tasks: Should logging information about L{Task}s be inhibited?
  @type filter_tasks: boolean
  @ivar tabs: Stack of tab characters indenting logged events and tasks to 
    appropriate levels
  @type tabs: string
  '''
  def init(self):
    super(TaskMonitor, self).init()
    self.filter_tasks = False
    self.tabs = '\t'
    self.timestamp = False
    
    # insert style tags used in this monitor into textbuffer
    self._insert_one_tag_into_buffer('bold', {'weight':pango.WEIGHT_BOLD})
    self._insert_one_tag_into_buffer('small', {'scale':pango.SCALE_SMALL})
    self._insert_one_tag_into_buffer('blue_foreground', {'foreground':'blue'})  
    self._insert_one_tag_into_buffer('red_foreground', {'foreground':'red'})
    self._insert_one_tag_into_buffer('green_foreground',{'foreground':'green'})
  
  def getName(self):
    '''
    Gets the localized name of this monitor.
    
    @return: Monitor name
    @rtype: string
    '''
    return _('Task monitor')
  
  def getEventNames(self):
    '''
    Gets the event categories to be displayed in the View menu for filtering.
    
    @return: Event categories
    @rtype: list of string
    '''
    return AEEvent.getNames()
  
  def getEventDefaults(self):
    '''
    Gets the default event categories to check in the View menu.
    
    @return: Event categories
    @rtype: list of string
    '''
    return AEEvent.getDefaults()
  
  def getEventType(self):
    '''
    Gets the L{AEEvent.Base.AccessEngineEvent} base class to indicate the type 
    of events this monitor wants to buffer.
    
    @return: Base type of the event this monitor should buffer
    @rtype: L{AEEvent.Base.AccessEngineEvent} class
    '''
    return AEEvent.Base.AccessEngineEvent    
      
  def show(self, event=None, task=None, chain_type=None, propagate=None, 
           **kwargs):
    '''
    Buffers additional details about how the L{AEEvent} was handled by various
    L{Perk}s and L{Task}s.

    @param event: Event to filter on
    @type event: object
    @param task: L{Task} executing
    @type task: L{Task}
    @param chain_type: One of the L{AEConstants} CHAIN_* constants
    @type chain_type: integer
    @param propagate: Is the event allowed to propagate to other L{Task}s?
    @type propagate: boolean
    @raise IOError: When the monitor is no longer accepting data to buffer 
    '''
    if not self.isInitialized():
      raise IOError

    # assume the class name is the one we used to register the event type
    if ((event is None and self.filter_tasks) or 
        (event is not None and not self._isShown(event.__class__.__name__))):
      # don't allow any task info to be buffered until another unfiltered
      # event is received and don't do anything if the parent event is not 
      # monitored
      self.filter_tasks = True
      return
    
    self.filter_tasks = False
    if propagate is not None:
      # show return value information
      self._showReturn(propagate)
    elif chain_type is not None:
      # show chain information
      self._showChain(chain_type, **kwargs)
    elif task is not None:
      # show task information
      self._showTask(task, **kwargs)
    else:
      # show event information
      self._showEvent(event, **kwargs)
      
  def _addViewMenuItems(self, menu):
    menu.append(gtk.SeparatorMenuItem())
    item = gtk.CheckMenuItem(_('Timestamp'))
    menu.append(item)
    item.connect('activate', self._onTimeStamp, _('Timestamp'))

  def _showEvent(self, event, tier_name):
    '''
    Buffer information about the current event.
    
    @param event: Event to filter on
    @type event: object
    @param tier_name: Name of the L{Tier} that handled the event
    @type tier_name: string
    '''
    eventstr = str(event)
    split = eventstr.find('\n')
    self._queueText(eventstr[:split], tags=['bold', 'small'])
    self._queueText(eventstr[split:]+'\n', tags=['small'])
    self._queueText('%stier: %s\n' % (self.tabs, tier_name), 
                    tags=['small'])
    if self.timestamp:
      self._queueText('%s%s\n' % (self.tabs, 
                              datetime.datetime.fromtimestamp(time.time())),
                              tags=['small'])
    
  def _showChain(self, chain_type, anchor_ident):
    '''
    Buffer information about a L{Task} chained to an anchor.
    
    @param chain_type: One of the L{AEConstants} CHAIN_* constants
    @type chain_type: integer
    @param anchor_ident: Identifier of the anchor L{Task}
    @type anchor_ident: string
    '''
    # log info about the chain
    if chain_type is AEConstants.CHAIN_BEFORE:
      template = '%s(before: %s)\n'
      tags = ['blue_foreground']
    elif chain_type is AEConstants.CHAIN_AFTER:
      template = '%s(after: %s)\n'
      tags = ['red_foreground']
    elif chain_type is AEConstants.CHAIN_AROUND:
      template = '%s(around: %s)\n'
      tags = ['green_foreground']
    tags.append('small')
    self._queueText(template % (self.tabs, anchor_ident), tags=tags)
  
  def _showTask(self, task, perk):
    '''
    Buffer information about a L{Task} executing.
    
    @param task: Executing Task
    @type task: L{Task}
    @param perk: Perk in which the L{Task} resides
    @type perk: L{Perk}
    '''
    # log the task info
    tags=['small']
    # make anchor task bold.  All others will be normal weight
    if len(self.tabs) == 1:
      tags.append('bold')
    template = '%s%s in %s\n'
    self._queueText(template % (self.tabs,
                                task.getClassName(), 
                                perk.getClassName()), tags=tags)
    # add to the indent level
    self.tabs += '\t'

  def _showReturn(self, propagate):
    '''
    Buffer information about the return value of a L{Task}.
    
    @param propagate: Are L{Task}s later in a chain or deeper in the L{Perk}
      stack allowed to execute (True) or update (False)?
    @type propagate: boolean
    '''
    # show propagate info
    self._queueText('%spropagate: %s\n' % (self.tabs, str(propagate)), 
                     tags=['small'])
    # subtract from the indent level
    self.tabs = self.tabs[:-1]
      
  def _onTimeStamp(self, widget, event_name):
    if widget.get_active():
      self.timestamp = True
    else:
      self.timestamp = False
    
