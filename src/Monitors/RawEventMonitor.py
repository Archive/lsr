'''
Defines a GTK dialog for buffering L{pyLinAcc} events.

@author: Peter Parente
@author: Brett Clippingdale
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''

import pyLinAcc
from GTKEventDialog import GTKEventDialog
from i18n import _

__uie__ = dict(kind='monitor', profiles=['developer'])

class RawEventMonitor(GTKEventDialog):
  '''
  Logs information about platform accessibility events to a GUI window.
  '''
  def getName(self):
    '''
    Gets the localized name of this monitor.
    
    @return: Monitor name
    @rtype: string
    '''
    return _('Event monitor')
  
  def getEventNames(self):
    '''
    Gets the event categories to be displayed in the View menu for filtering.
    
    @return: Event categories
    @rtype: list of string
    '''
    return pyLinAcc.getNames()
  
  def getEventDefaults(self):
    '''
    Gets the default event categories to check in the View menu.
    
    @return: Event categories
    @rtype: list of string
    '''
    return pyLinAcc.getDefaults()
  
  def getEventType(self):
    '''
    Gets the L{pyLinAcc.Event.Event} base class to indicate the type of events
    this monitor wants to buffer.
    
    @return: Base type of the event this monitor should buffer
    @rtype: L{pyLinAcc.Event.Event} class
    '''
    return pyLinAcc.Event.Event
  
  def _isShown(self, event_name):
    '''
    Gets if the given event name is in the L{shown} list and if L{logging} is
    enabled.
    
    @param event_name: Name of the event
    @type event_name: string
    @return: Is the event to be shown?
    @rtype: boolean
    '''
    if not self.logging:
      return False
    for n in self.shown:
      if event_name.startswith(n):
        return True
    return False
    
  def show(self, event, **kwargs):
    '''
    Renders an event as text into the gtk.TextBuffer. The context has no effect
    on rendering in this monitor and so it defaults to None.

    @param event: Event to buffer
    @type event: L{pyLinAcc.Event.Event}
    '''
    if not self.isInitialized():
      raise IOError
    try:
      app = event.source.getApplication()
    except Exception:
      return
    if (self._isShown(event.type.name) and not 
        app.name.lower().startswith('lsr')):
      self._queueText(str(event)+'\n')