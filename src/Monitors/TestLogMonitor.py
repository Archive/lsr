'''
Defines the abstract base class for all L{AEMonitor} subclasses.

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
import logging
from AEMonitor import AEMonitor
from AEConstants import LAYER_NAMES, SEMANTIC_NAMES
from DeviceManager import InputEvent, OutputEvent, DeviceEvent
from i18n import _
import AEOutput

__uie__ = dict(kind='monitor')

# get a reference to the logging system
log = logging.getLogger('TestLogMonitor')

class TestLogMonitor(AEMonitor):
  '''  
  Logs information sent to output devices to using the LSR logging system. The
  format of the output is intended to be used in automated regression testing.
  
  @ivar last_dev: Name of the last device to which output was sent
  @type last_dev: string
  '''
  def init(self):
    '''
    Initialize the monitor instance variables.
    '''
    super(TestLogMonitor, self).init()
    self.last_dev = None
  
  def getName(self):
    '''
    Gets the localized name of this monitor.
    
    @return: Monitor name
    @rtype: string
    '''
    return _('Test logging monitor')
  
  def getEventNames(self):
    '''
    Gets the event categories to be logged.
    
    @return: Event categories
    @rtype: list of string
    '''
    return AEOutput.getNames()
  
  def getEventDefaults(self):
    '''
    Gets the default event categories to log.
    
    @return: Event categories
    @rtype: list of string
    '''
    return AEOutput.getDefaults()
  
  def getEventType(self):
    '''
    Gets the L{DeviceManager.DeviceEvent} base class to indicate the type 
    of events this monitor wants to buffer.
    
    @return: Base type of the event this monitor should buffer
    @rtype: L{DeviceManager.DeviceEvent} class
    '''
    return OutputEvent
      
  def show(self, event, value=None, dev=None, sem=None, layer=None):
    '''
    Buffers additional details about how the L{AEEvent} was handled by various
    L{Perk}s and L{Task}s. The event is only provided as a means of filtering 
    information.
    
    @param event: Wrapper giving the direction of a command (input/output) and 
      the command name
    @type event: L{DeviceManager.DeviceEvent}
    @param value: Value of the command sent to/from a device
    @type value: object
    @param sem: Semantic constant indicating the kind of output
    @type sem: integer
    @param layer: Layer constant indicating from where the event came
    @type layer: integer
    @param dev: Device on which the event occurred
    @type dev: L{AEInput} or L{AEOutput}
    @raise IOError: When the monitor is no longer accepting data to buffer 
    '''
    if not self.isInitialized():
      raise IOError
    
    if dev and dev.getName() != self.last_dev:
      # show the name of a device
      name = dev.getName()
      self.last_dev = name
      log.info('\n{device: %s}' % name)
      
    # convert to human readable names
    sem = SEMANTIC_NAMES.get(sem)
    try:
      layer = LAYER_NAMES[layer]
    except TypeError:
      layer = 'None'
    log.info('{%s: %s %s} %s', event.getName(), sem, layer, str(value))
