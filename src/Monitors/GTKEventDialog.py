'''
Defines a gtk dialog for buffering L{AEMonitor} data.

@var PUMP_MAX: Maximum number of items to buffer during one timer interval.
@type PUMP_MAX: integer
@var PUMP_INTERVAL: Milliseconds between timer fire.
@type PUMP_INTERVAL: integer

See the pygtk class reference at
U{http://www.pygtk.org/pygtk2reference/gtk-class-reference.html} and the pygtk
tutorial at U{http://www.pygtk.org/pygtk2tutorial/index.html} for details about
the construction of gtk GUIs.

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''

import pygtk
pygtk.require('2.0')
import gtk, gobject
import warnings, Queue
import AEMonitor
from i18n import _

# pump constants
PUMP_MAX = 100
PUMP_INTERVAL = 250
WIN_WIDTH = 400
WIN_HEIGHT = 450

class GTKEventDialog(AEMonitor.AEMonitor):
  '''  
  Buffers text describing raw L{pyLinAcc} events. Lets the user choose which 
  events are displayed and to enable or disable logging altogether. Supports the
  saving of logs to text files on disk. Items are only buffered when the 
  L{window} created by this object does not have the focus to prevent the text 
  box from scrolling while the user is trying to inspect it or select its
  contents.

  @ivar shown: Names of events to be shown
  @type shown: list of string
  @ivar text_buffer: Buffer holding string representations of raw events
  @type text_buffer: gtk.TextBuffer
  @ivar text_view: View of past raw events
  @type text_view: gtk.TextView
  @ivar window: Top level window
  @type window: gtk.Window
  @ivar logging: Is logging to the text view enabled?
  @type logging: boolean
  @ivar timer: ID for the timer callback to L{_onPumpQueue}
  @type timer: integer
  @ivar queue: Queue of text to buffer
  @type queue: Queue.Queue
  @ivar scroll: Scroll the text buffer on the next timer interval?
  @type scroll: boolean
  '''
  num_windows = 0
  def __init__(self):
    '''
    Initializes instance variables.
    '''
    AEMonitor.AEMonitor.__init__(self)
    self.logging = True
    self.shown = []
    self.window = None
    self.text_buffer = None
    self.text_view = None
    self.timer = None
    self.queue = Queue.Queue()
    self.scroll = False

  def init(self):
    '''
    Creates and shows the monitor window and its components. Calls the parent 
    implementation to set the L{is_init} flag to True.
    '''
    # create and configure the window object
    self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)
    self.window.set_title(self.getName())
    self.window.set_size_request(WIN_WIDTH, WIN_HEIGHT)
    self.window.move((WIN_WIDTH+10)*GTKEventDialog.num_windows, 0)
    GTKEventDialog.num_windows += 1

    # create the layout box
    box = gtk.VBox(False, 0)
    self.window.add(box)

    # create the menu bar using the XML description
    menu_bar = self._createMenuBar()
    box.pack_start(menu_bar, False)

    # logging option
    display_check = gtk.CheckButton(_('_Enable logging'))
    display_check.set_active(self.logging)
    display_check.connect('toggled', self._onCheckLogging)
    box.pack_start(display_check, False)

    # create one text buffer, two views
    self.text_buffer = gtk.TextBuffer()
    self.text_view = gtk.TextView(self.text_buffer)
    self.text_view.set_pixels_below_lines(1)
    self.text_view.set_wrap_mode(gtk.WRAP_WORD)
    self.text_view.set_editable(False)
        
    # make the text views scrollable
    sw1 = gtk.ScrolledWindow()
    sw1.add(self.text_view)    
    box.pack_start(sw1, True, True)
    
    # create timer for pumping text to buffer
    self.timer = gobject.timeout_add(PUMP_INTERVAL, self._onPumpQueue)

    # show all elements
    self.window.show_all()
    
    # set the initialized flag to True
    super(GTKEventDialog, self).init()
    
  def close(self, widget=None):
    '''
    Destroys the window, sets L{logging} to False to prevent further adds to the
    queue, and removes the timer. Calls the parent implementation to set the 
    L{is_init} flag to False.
    
    @param widget: Source of the gtk quit event or None if called externally
    @type widget: gtk.Widget
    '''
    if not self.isInitialized():
      return
    GTKEventDialog.num_windows -= 1
    self.logging = False
    gobject.source_remove(self.timer)
    self.window.destroy()
    self.window = None
    del self.queue
    # set the initialized flag to False
    super(GTKEventDialog, self).close()

  def _createMenuBar(self):
    '''
    Creates the main menu bar consisting of a File menu for saving the active
    buffer, clearing the active buffer, and quiting the program; a Raw menu for
    selecting which raw events should be buffered; and a View menu for 
    selecting which types of events should be buffered.

    @return: Fully constructed menu bar and menus
    @rtype: gtk.MenuBar
    '''
    # create an accelerator group
    acc = gtk.AccelGroup()
    self.window.add_accel_group(acc)

    # build the file menu
    file_menu = gtk.Menu()
    i = gtk.ImageMenuItem(gtk.STOCK_SAVE_AS)
    i.add_accelerator('activate', acc, ord('S'), gtk.gdk.CONTROL_MASK,
                      gtk.ACCEL_VISIBLE)
    i.connect('activate', self._onSaveLog)
    file_menu.append(i)

    i = gtk.ImageMenuItem(gtk.STOCK_CLEAR)
    i.add_accelerator('activate', acc, ord('L'), gtk.gdk.CONTROL_MASK,
                      gtk.ACCEL_VISIBLE)
    i.connect('activate', self._onClearLog)
    file_menu.append(i)

    # assign the top level menus labels
    file_item = gtk.MenuItem(_('_File'))
    file_item.set_submenu(file_menu)
    
    # create the Raw and Task menus dynamically based on what events and tasks
    # are exposed by the pyLinAcc and Task packages
    raw_item = gtk.MenuItem(_('_View'))
    raw_item.set_submenu(self._createViewMenu())

    # put the menus on a menubar
    menu_bar = gtk.MenuBar()
    menu_bar.append(file_item)
    menu_bar.append(raw_item)
    return menu_bar
    
  def _createViewMenu(self):
    '''
    Builds a menu with checkable items indicating whether a certain type of 
    event should be buffered. The names of the menu items and whether they are 
    checked or not are gotten from the L{getEventNames} and L{getEventDefaults}
    methods.
    
    @return: Fully constructed menu
    @rtype: gtk.Menu
    '''
    display_menu = gtk.Menu()
    # iterate over all the names
    for name in self.getEventNames():
      # create a chackable menu item
      i = gtk.CheckMenuItem(name)
      # create a callback to onCheckFilters
      i.connect('activate', self._onCheckFilters, name)
      # set default listeners
      if name in self.getEventDefaults():
        i.set_active(True)
      display_menu.append(i)
    # add additional menu items if needed 
    try:
      self._addViewMenuItems(display_menu)
    except NotImplementedError:
      pass
    return display_menu

  def _onSaveLog(self, widget):
    '''
    Saves the text in the L{text_buffer} to a plain text file on disk. Grabs all
    the text in the active textbox using L{_getActiveLogText}. Uses the standard
    gtk.FileChooserDialog to request a filename and location for the save.

    @param widget: Source of the gtk activate event
    @type widget: gtk.Widget
    '''
    # create a file dialog
    dlg = gtk.FileChooserDialog(_('_Save log'), self.window,
                                gtk.FILE_CHOOSER_ACTION_SAVE)
    # add OK and cancel buttons
    dlg.add_button(gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL)
    dlg.add_button(gtk.STOCK_OK, gtk.RESPONSE_ACCEPT)
    # show the dialog modally (i.e. wait for a response after calling run())
    if dlg.run() == gtk.RESPONSE_ACCEPT:
      # save the log to a file if the user presses OK
      filename = dlg.get_filename()
      try:
        f = file(filename, 'w')
        f.write(self._getActiveLogText())
        f.close()
      finally:
        # allow any exceptions to propagate and get printed
        dlg.destroy()
    else:
      dlg.destroy()
      
  def _getActiveLogText(self):
    '''
    Grabs all of the text from the text buffer. Used by L{_onSaveLog} to save 
    the buffer to disk.
    
    @return: All text in the buffer used by the active textbox
    @rtype: string
    '''
    start = self.text_buffer.get_start_iter()
    end = self.text_buffer.get_end_iter()
    return self.text_buffer.get_text(start, end)

  def _onClearLog(self, widget):
    '''
    Clears the text buffer.

    @param widget: Source of the gtk event
    @type widget: gtk.Widget
    '''
    self.text_buffer.set_text('')
    
  def _onCheckLogging(self, widget):
    '''
    Sets if logging to the text buffer is enabled or not.

    @param widget: Source of the gtk toggled event
    @type widget: gtk.Widget
    '''
    self.logging = widget.get_active()

  def _onCheckFilters(self, widget, event_name):
    '''
    Adds or removes an event from the shown list when an item is checked or
    unchecked respectively in the View menu.
    
    @param widget: Source of the gtk activate event
    @type widget: gtk.Widget
    @param event_name: Name of the event
    @type event_name: string
    '''
    if widget.get_active():
      self.shown.append(event_name)
    else:
      self.shown.remove(event_name)
      
  def _isShown(self, event_name):
    '''
    Gets if the given event name is in the L{shown} list and if L{logging} is
    enabled.
    
    @param event_name: Name of the event
    @type event_name: string
    @return: Is the event to be shown?
    @rtype: boolean
    '''
    return self.logging and event_name in self.shown
      
  def _queueText(self, text, tags=None):
    '''
    Queues text for later display in the L{text_view} when the L{window} does
    not have the focus and when the L{timer} fires.
    
    @param text: Text to buffer and display
    @type text: string
    '''
    self.queue.put_nowait((text,tags))
    
  def _onPumpQueue(self):
    '''
    Buffers all waiting events as long as the top level window is not the 
    foreground window. Buffering stops immediately when the window is active
    to allow the user to read and select text without having it scroll. 
    Buffering stops when L{PUMP_MAX} items have been displayed during this call.
    
    @return: True to ensure this method is called on the next interval
    @rtype: boolean
    '''
    # count the number of events buffered so we know when to scroll and can
    # bail out if we're getting flooded
    count = 0
    # loop while the top level window is not the foreground window and we have
    # not buffered more than MAX_PUMP items
    while count < PUMP_MAX and not self.window.is_active():
      try:
        # get the text to buffer
        text,tags = self.queue.get_nowait()
        count += 1
      except Queue.Empty:
        break
      # buffer the text
      end = self.text_buffer.get_end_iter()
      if tags is not None:
        self.text_buffer.insert_with_tags_by_name(end, text, *tags)
      else:
        self.text_buffer.insert(end, text)
    if self.scroll and not self.window.is_active():
      # scroll to the bottom
      self.text_view.scroll_to_iter(self.text_buffer.get_end_iter(), 0.0)
      self.scroll = False
    if count:
      self.scroll = True
    return True
    
  def _addViewMenuItems(self, menu):
    raise NotImplementedError
  
  def _insert_one_tag_into_buffer(self, name, params):
    '''
    Adds one named style tag into the L{GtkTextBuffer} tag table. These 
    tags can then be used to add style to text that is queued in L{_queueText}.
    
    @param name: Tag name of this style
    @type name: string
    @param params: dictionary of key, value pairs that represent a style.
    @type params: dictionary
    '''
    tag = gtk.TextTag(name)
    for key, value in params.iteritems():
      tag.set_property(key, value)
    table = self.text_buffer.get_tag_table()
    table.add(tag)