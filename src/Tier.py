'''
Defines a class for managing the L{Perk}s loaded for a single instance of an
application.

@author: Peter Parente
@author: Pete Brunet
@author: Larry Weiss
@author: Scott Haeger
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
import logging, weakref, traceback, sys, os
import AEState, AEInput, Perk, Task, AEConstants
from POR import POR

log = logging.getLogger('Tier')

class Tier(object):
  '''
  Manages the L{Perk}s for a single instance of a running program (a process). 
  Supports the addition and removal of L{Perk}s from its stack.  Executes 
  L{Task}s registered in each L{Perk} in response to L{AEEvent}s. Provides
  methods to assist L{Perk}s and L{Task}s in finding registered L{Task}s 
  throughout the L{Tier}.

  @ivar perks: List of L{Perk}s treated as a stack (last in, first executed)
  @type perks: list
  @ivar wanted_events: Lookup table for what L{AEEvent}s are desired by any
    L{Task} in any L{Perk} in this L{Tier}. Used to optimize event dispatch and
    processing. Unwanted events are ignored.
  @type wanted_events: dictionary
  @ivar tier_manager: L{TierManager} that created this L{Tier}
  @type tier_manager: L{TierManager}
  @ivar pointer_por: Pointer of regard for the user focus
  @type pointer_por: L{POR}
  @ivar focus_por: Point of regard for the application focus
  @type focus_por: L{POR}
  @ivar name: Name of this L{Tier}
  @type name: string
  @ivar aid: ID uniquely identifying this L{Tier} from all other L{Tier}s
  @type aid: opaque
  @ivar last_key: Stores the key code, key sym, and modifiers for the last key 
    pressed
  @type last_key: 3-tuple of integer, string, integer
  @ivar perk_refs: References to L{Perk}s keyed by task names, commands, 
    identifiers, etc. Many to one mapping. See L{addTaskRef} and 
    L{removeTaskRef}. 
  @type perk_refs: weakref.WeakValueDictionary
  @ivar chain_refs: References to L{Perk}s keyed by task names. Many to many
    mapping. See L{addChainRef} and L{removeChainRef}.
  @type chain_refs: dictionary
  @ivar chain_stack: Stack of L{Task} and L{Perk} names currently executing in 
    one or more chains before, after, or around one or more anchors
  @type chain_stack: list
  @ivar temp_data: Arbitrary name/value pairs stored by L{Task}s executing in
    this L{Tier} in response to a single event. The data is persistent until 
    the event has been handled by all L{Task}s at which time it is cleared.
  @type temp_data: dictionary
  @ivar task_history: Stack of L{Task} and L{Perk} names having already 
    executed in response to an L{AEEvent}, including all chained L{Task}s and
    all L{Task}s executed from within other L{Task}s
  @type task_history: list
  @ivar show: Call show* methods on L{TierManager} to log events? Set by the
    L{TierManager} based on whether monitors are loaded or not. Optimization.
  @type show: boolean
  '''
  def __init__(self, tier_manager, name, aid, por):
    '''
    Stores a reference to the L{TierManager} that created this L{Tier}. Creates
    an empty list of L{Perk}s. Initializes the stored focus L{POR} to None and
    the pointer L{POR} to the application. Creates the weak dictionary for 
    L{Perk} L{Task}s and stores other information about the L{Tier}.
    
    @param tier_manager: Manager that created this L{Tier}
    @type tier_manager: L{TierManager}
    @param name: Name of this L{Tier}
    @type name: string
    @param aid: ID uniquely identifying this L{Tier} from all other L{Tier}s
    @type aid: opaque
    @param por: First point of regard to the application represented by 
      this L{Tier}. Typically, the top most accessible.
    @type por: L{POR}
    '''
    self.tier_manager = tier_manager
    self.perks = []
    self.wanted_events = {}
    self.name = name
    self.aid = aid
    self.perk_refs = weakref.WeakValueDictionary()
    self.chain_refs = {}
    self.pointer_por = por
    self.focus_por = None
    self.last_key = (None, None, None)
    self.auto_pointer = True
    # fields to be cleared after each event
    self.clearState()

  def close(self):
    '''
    Clears all L{Perk}s and closes them.
    '''
    self.clearPerks()
    
  def setShow(self, show):
    '''
    Sets whether show methods should be called on the L{TierManager} when
    handling events or not based on the presence of L{AEMonitor}s in the
    manager.
    
    @param show: Call show methods to notify monitors?
    @type show: boolean
    '''
    self.show = show
  
  def getPointer(self):
    '''
    Gets the user L{POR} for this L{Tier}.
    
    @return: Point of regard of user attention
    @rtype: L{POR}
    '''
    return self.pointer_por
  
  def setPointer(self, por):
    '''
    Sets the user L{POR} for this L{Tier}.
    
    @param por: Point of regard of user attention
    @type por: L{POR}
    '''
    self.pointer_por = por

  def getFocus(self):
    '''
    Gets the application focus L{POR} for this L{Tier}.
    
    @return: Point of regard of application focus
    @rtype: L{POR}
    '''
    return self.focus_por
  
  def getHistory(self):
    '''
    Gets the entire linear history of L{Task}s that have executed and the 
    L{Perk}s they are in.
    
    @return: List of L{Task} identities and L{Perk} class names
    @rtype: list of 2-tuple of string
    '''
    return self.task_history
  
  def getAnchor(self):
    '''
    Gets the identity of the L{Task} to which the current L{Task} is chained 
    and its corresponding L{Perk}. If there is no anchor for the executing 
    L{Task}, this method returns None.
    
    @return: L{Task} identity and L{Perk} class name or None
    @rtype: 2-tuple of string
    '''
    try:
      return self.chain_stack[-1]
    except IndexError:
      return None
  
  def getLastKey(self):
    '''    
    Gets the last key code, key sym, and key modifiers. Useful for dealing with
    applications that do a poor job of synthesizing caret and text events.
    
    @return: The key code, key sym, and modifiers for the last key pressed
    @rtype: 3-tuple of integer, string, integer
    '''
    return self.last_key
  
  def getTempVal(self, name):
    '''
    Gets data from L{temp_data}.
    
    @param name: Name of the stored data
    @type name: immutable
    @return: Value stored under the given name
    @rtype: object
    @raise KeyError: When no data is stored under the given name
    '''
    return self.temp_data[name]
  
  def setTempVal(self, name, value):
    '''
    Stores data for the duration of the execution of an L{AEEvent}.
    
    @param name: Name to associate with the value
    @type name: immutable
    @param value: Any value to store
    @type value: object
    '''
    self.temp_data[name] = value
    
  def clearState(self):
    '''
    Clears all stored L{temp_data}, the L{chain_stack}, L{task_history}, and
    the L{show} flag.
    '''
    self.temp_data = {}
    self.task_history = []
    self.chain_stack = []
    self.show = False
  
  def addChainRef(self, target, perk):
    '''
    Adds a weak reference to a L{Perk} as one to check for L{Task}s which are 
    part of a the chain for the target. Optimization for the L{manageChain}
    method.

    @param target: Name of the L{Task} to link to
    @type target: string
    @param perk: Perk have at least one link in the chain for the target
    @type perk: L{Perk} 
    '''
    chain = self.chain_refs.setdefault(target, weakref.WeakKeyDictionary())
    chain[perk] = None
     
  def removeChainRef(self, target, perk):
    '''
    Removes a weak reference to a L{Perk} as one to no longer check for 
    L{Task}s which are part of the chain for the target. Optimization for the 
    L{manageChain} method.
    
    @param target: Name of the L{Task} to unlink from
    @type target: string
    @param perk: Perk have at least one link in the chain for the target
    @type perk: L{Perk}
    '''
    try:
      chain = self.chain_refs[target]
      del chain[perk]
    except (KeyError, ValueError):
      pass

  def addTaskRef(self, key, perk):
    '''
    Adds a key that can be used later to quickly look up a reference to a
    L{Perk} in this L{Tier}. Optimization for the L{getKeyedTask},
    L{getCommandTask}, and L{getNamedTask} methods.
    
    @param key: Key under which to hash the L{Perk}
    @type key: immutable
    @param perk: Perk to store in the hash
    @type perk: L{Perk}
    '''
    self.perk_refs[key] = perk
    
  def removeTaskRef(self, key):
    '''
    Removes a key used to quickly look up a reference to a L{Perk} in this 
    L{Tier}. Keys are cleaned up automatically when L{Perk}s are destroyed.
    However, this method may be used to manually remove L{Perk}s at any time.
    
    @param key: Key under which to hash the L{Perk}
    @type key: immutable
    '''
    try:
      del self.perk_refs[key]
    except KeyError:
      pass
    
  def getName(self):
    '''
    @return: Name of this L{Tier} (i.e. name of the app with which it is 
      associated)
    @rtype: string
    '''
    return self.name
  
  def getIdentity(self):
    '''
    @return: Unique identifier for this L{Tier}
    @rtype: opaque
    '''
    return self.aid

  def pushPerk(self, ae, *perks):
    '''
    Adds one or more L{Perk}s to the top of the stack. If more than one L{Perk}
    is specified, the last specified L{Perk} will be at the top of the stack 
    when this method completes. That is, the behavior of pushing more than one
    L{Perk} at a time is the same as if each L{Perk} were pushed individually.
    
    @param ae: The AccessEngine context to use for initializing the Perk
    @type ae: L{AccessEngine}
    @param perks: L{Perk}s to add
    @type perks: list of L{Perk}
    '''
    self.insertPerk(ae, 0, *perks)
      
  def insertPerk(self, ae, index, *perks):
    '''
    Adds one L{Perk} to the stack at the insertion index. Negative indices are
    valid per Python list convention.
    
    @param ae: The AccessEngine context to use for initializing the Perk
    @type ae: L{AccessEngine}
    @param index: Index at which the L{Perk} should be inserted in the stack
    @type index: integer
    @param perks: L{Perk}s to add
    @type perks: list of L{Perk}
    '''
    for perk in perks:
      # put the Perk somewhere in the stack
      self.perks.insert(index, perk)
      # initialize the Perk base classes with important, persistent references
      perk.preInit(ae, self)
      # initialize this particular instance with temporary references
      perk.preExecute(AEConstants.LAYER_FOCUS)
      try:
        # now let Perk specific code run
        perk.init()
      except Exception, e:
        # pop the perk from the stack if it fails to initialize
        self.perks.pop(index)
        # let it finalize
        try:
          perk.close()
        except Exception:
          # ignore errors while finalizing
          pass
        perk.postClose()
        # warn about the error
        info = traceback.extract_tb(sys.exc_info()[2])
        log.debug('cannot initialize %s (%s, %d): %s', perk.getClassName(), 
                  os.path.basename(info[-1][0]),
                  info[-1][1], str(e))
    
  def popPerk(self, *indices):
    '''
    Removes one or more L{Perk}s from the stack given their indices. Negative
    indices are valid per Python list convention. Calls L{Perk.Perk.close} on 
    each to ensure they have a chance to persist state.
    
    @param indices: Indices of L{Perk}s to remove
    @type indices: list of integer
    @raise IndexError: When the stack is empty
    @raise ValueError: When a specific index is out of the bounds of the stack
    '''
    indices = list(indices)
    # have to pop in reverse order to avoid corrupting original indices
    indices.sort(reverse=True)
    perks = map(self.perks.pop, indices)
    for perk in perks:
      try:
        perk.close()
      except Exception:
        # ignore errors during close
        pass
      perk.postClose()

  def clearPerks(self):
    '''
    Removes all L{Perk}s from the stack. Calls L{Perk.Perk.close} on each to 
    ensure they have a chance to persist state.
    '''
    for perk in self.perks:
      try:
        perk.close()
      except Exception:
        # ignore errors during close
        pass
      perk.postClose()
    self.perks = []
      
  def getPerks(self):
    '''
    Gets all L{Perk}s pushed onto this L{Tier} in execution order.
    
    @note: This method does not return a copy of the stack. Modifications will
      affect this L{Tier}!
    @return: All L{Perk}s pushed onto this L{Tier}
    @rtype: list of L{Perk}
    '''
    return self.perks
  
  def setEventInterest(self, kind, wants):
    '''    
    Updates the L{wanted_events} dictionary to indicate that a L{Task} in a
    L{Perk} in this L{Tier} has been registered for a particular kind of
    L{AEEvent}. This information is used for optimization purposes such that no
    processing will occur on the event unless at least one L{Task} is
    registered that will use it.
    
    @param kind: Kind of L{AEEvent} of interest to a L{Task}
    @type kind: L{AEEvent} class
    @param wants: Does a L{Perk} want an event (i.e. a L{Task} is registering 
      for it or no longer want an event (i.e. a L{Task} is unregistering for 
      it)?
    @type wants: boolean
    '''
    count = self.wanted_events.setdefault(kind, 0)
    if wants:
      count += 1
    else:
      count -= 1
    if count <= 0:
      del self.wanted_events[kind]
    else:
      self.wanted_events[kind] = count
    # inform the TierManager of the new event interest
    self.tier_manager.setEventInterest(kind, wants)
      
  def wantsEvent(self, event):
    '''
    Gets if this L{Tier} wants a particular kind of L{AEEvent} given that one of
    its L{Perk}s has a L{Task} that wants to handle it.
    
    @param event: Event to be tested
    @type event: L{AEEvent}
    '''
    return self.wanted_events.has_key(type(event))
  
  def getKeyedTask(self, key):
    '''
    Gets a L{Task} registered under a particular key set to execute in response
    to events. None is returned if not found.
    
    @param key: Unique ID for locating a registered L{Task}
    @type key: integer
    @return: L{Task} set to execute in response to the event or None
    @rtype: L{Task}
    '''
    try:
      perk = self.perk_refs[key]
    except KeyError:
      return None
    return perk.getKeyedTask(key)
  
  def getCommandTask(self, gesture_list):
    '''
    Gets a L{Task} registered to execute in response to the L{AEInput.Gesture}.
    None is returned if not found.
    
    @param gesture_list: Gestures and device expressed as a list of virtual key 
      codes
    @type gesture_list: L{AEInput.Gesture}
    @return: Name of the L{Task} to execute in response to the input gesture 
      or None if no L{Task} registered to execute
    @rtype: string
    '''
    try:
      perk = self.perk_refs[gesture_list]
    except KeyError:
      return None
    return perk.getCommandTask(gesture_list)

  def getNamedTask(self, name):
    '''
    Gets a L{Task} with the given name by iterating through the registered 
    L{Perk}s until one is found containing a L{Task} registered under the given
    name. None is returned if not found.
    
    @param name: Name of the L{Task} to find 
    @type name: string
    @return: L{Task} with the given name or None
    @rtype: L{Task.Base.Task}
    '''
    try:
      perk = self.perk_refs[name]
    except KeyError:
      return None
    return perk.getNamedTask(name)
    
  def getEventTasks(self, event_type, task_layer):
    '''    
    Gets all L{Task}s registered to handle the given type of event on the
    given layer by iterating through the registered {Perk}s. The L{Task}s are
    returned in the order they will be executed both within and across
    L{Perk}s in this L{Tier}.

    @param event_type: Desired type of L{AEEvent}
    @type event_type: L{AEEvent} class
    @param task_layer: Layer on which the desired L{Task}s are registered
    @type task_layer: integer
    @return: List of all L{Task}s registered to handle the given type of event
      on the given layer
    @rtype: list of L{Task.Base.Task}
    '''
    tasks = []
    map(tasks.extend, [p.getEventTasks(event_type, task_layer) for p in 
                       self.perks])
    return tasks
  
  def getChainedTasks(self, target):
    '''    
    Gets all L{Task}s linked to the target by iterating through L{Perk}
    references established by L{addChainRef}. The L{Task}s are returned in the
    order in which they were registered within a L{Perk}. Ordering is undefined
    across L{Perk}s within a chain segment.

    @param target: Name of the L{Task} to link to
    @type target: string
    @return: L{Task} names for before, around, and after segments
    @rtype: 3-tuple of list, string, list
    @raise KeyError: When the target has no chained L{Task}s
    '''
    befores = []
    afters = []
    perks = self.chain_refs[target].keys()
    arounds = [p.mergeChain(target, befores, afters) for p in perks]
    return befores, arounds[0], afters
  
  def _executeTask(self, task, perk, por, layer, params, propagate):
    '''
    Executes the given L{Task} in response to the given L{AEEvent}.
    
    @param task: A L{Task} registered to handle the given event
    @type task: L{Task}
    @param perk: The L{Perk} containing the L{Task}
    @type perk: L{Perk}
    @param por: Point of regard where the event occurred
    @type por: L{POR}
    @param propagate: Should this event be propagated to the 
      L{Task.Base.Task.execute} method or should we call 
      L{Task.Base.Task.update} instead for housekeeping?
    @type propagate: boolean
    @param layer: Layer on which the L{AEEvent} occurred, one of 
      L{AEConstants.Event.LAYER_FOCUS}, L{AEConstants.Event.LAYER_TIER}, 
      L{AEConstants.Event.LAYER_BACKGROUND}
    @type layer: integer
    @param params: Keyword arguments to be provided to the L{Task} as 
      parameters
    @type params: dictionary
    @return: Should the next registered L{Task} be executed or only updated?
    @rtype: boolean
    @raise AssertionError: When the L{Perk} or L{Task} does not preExecute
      successfully
    '''
    # pass important references to the Task
    if not (perk.preExecute(layer, por) and task.preExecute(layer, por)):
      raise AssertionError
    try:
      if propagate:
        # unpack the dictionary and execute the task
        rv = task.execute(layer=layer, **params)
      else:
        # unpack the dictionary and update the task
        task.update(layer=layer, **params)
        rv = False
    except Task.ToolsError, ex:
      # output the Task.ToolsError if state.Trap is True
      if self.tier_manager.getState().Trap:
        task.sayError(text=str(ex))
      else:
        log.exception(ex)
      rv = True
    except Exception:
      # log any other Perk exception
      log.exception('%s: %s %s', task.getClassName(), layer, params)
      # continue processing
      rv = True
    if rv is None:
      # continue processing if no return value is specified
      rv = True
    elif not rv:
      # inhibit automatic updating of the pointer if propogation is blocked
      task.inhibitAutoPointer()
    # uninitialize the Task, but not the Perk since it has nothing to do
    task.postExecute()
    return rv
  
  def manageChain(self, por, name, task, perk, layer, task_params, 
                   propagate, chain):
    '''
    Executes all L{Task}s chained the the given L{Task}, recursively if 
    chain=True.
    
    @note: This method is purposely long because much code that could be
    refactored into other methods is inlined for performance reasons.
    
    @param name: Identity of the L{Task} to execute
    @type name: string
    @param task: Task to execute
    @type task: L{Task}
    @param perk: Perk in which the Task resides
    @type perk: L{Perk}
    @param layer: Layer in which to execute the L{Task}, one of 
      L{AEConstants.Event.LAYER_FOCUS}, L{AEConstants.Event.LAYER_TIER}, 
      L{AEConstants.Event.LAYER_BACKGROUND}
    @type layer: integer
    @param task_params: Keyword data to pass to the L{Task} as parameters on 
      execution or update
    @type task_params: dictionary
    @param propagate: Should chained L{Task}s be executed or updated?
    @type propagate: boolean
    @param chain: Execute all L{Task}s chained to the one named or not?
    @type chain: boolean
    '''
    # pull show into local vars
    show = self.show
    # pull perk name into local vars once
    p_name = perk.getClassName()
    
    if not chain:
      # no chaining, just execute, duplicate execute code for performance
      # add to the linear history before executing so that it's available
      # to changed and doTask tasks
      self.task_history.append((name, p_name))
      if show:
        # execute this Task and log information
        self.tier_manager.showTask(task, perk)
        try:
          propagate = self._executeTask(task, perk, por, layer, task_params, 
                                        propagate)
        except AssertionError:
          pass
        self.tier_manager.showPropagate(propagate)
      else:
        # execute only, duplicated for performance
        try:
          propagate = self._executeTask(task, perk, por, layer, task_params, 
                                        propagate)
        except AssertionError:
          pass
      # do not push the task back on the stack because we're done anyways
    else:
      # fetch all chain segments at once
      try:
        befores, around_name, afters = self.getChainedTasks(name)
      except KeyError:
        # no chaining, just execute, duplicate execute code for performance
        # add to the linear history before executing so that it's available
        # to changed and doTask tasks
        self.task_history.append((name, p_name))
        if show:
          # execute this Task and log information
          self.tier_manager.showTask(task, perk)
          try:
            propagate = self._executeTask(task, perk, por, layer, task_params, 
                                          propagate)
          except AssertionError:
            pass
          self.tier_manager.showPropagate(propagate)
        else:
          # execute only, duplicated for performance
          try:
            propagate = self._executeTask(task, perk, por, layer, task_params, 
                                          propagate)
          except AssertionError:
            pass
        # do not push the task back on the stack because we're done anyways
      else:
        # put the name of this task and its perk onto the stack
        self.chain_stack.append((name, p_name))
        
        # get the chain of tasks to run before this one
        for before_name in befores:
          if show:
            self.tier_manager.showChain(AEConstants.CHAIN_BEFORE, name)
          # look for a Perk with a Task with the given name
          task = self.getNamedTask(before_name)
          # bail immediately if it doesn't exist
          if task is not None: 
            perk = task.getPerk()
            # recurse with before tasks
            propagate = self.manageChain(por, before_name, task, perk, layer, 
                                          task_params, propagate, chain)
          
        # check for task to run instead of this one
        if around_name is None:
          # pop this task
          cache = self.chain_stack.pop()
          # add to the linear history before executing so that it's available
          # to changed and doTask tasks
          self.task_history.append((name, p_name))
          if show:
            # execute this Task and log information
            self.tier_manager.showTask(task, perk)
            try:
              propagate = self._executeTask(task, perk, por, layer,
                                            task_params, propagate)
            except AssertionError:
              pass
            self.tier_manager.showPropagate(propagate)
          else:
            # execute only, duplicated for performance
            try:
              propagate = self._executeTask(task, perk, por, layer,
                                            task_params, propagate)
            except AssertionError:
              pass
          # get the pointer as the starting POR for the next task execution
          por = self.getPointer()
          # push this task back onto the stack
          self.chain_stack.append(cache)
        else:
          if show:
            self.tier_manager.showChain(AEConstants.CHAIN_AROUND, name)
          # look for a Perk with a Task with the given name
          task = self.getNamedTask(around_name)
          # bail immediately if it doesn't exist
          if task is not None: 
            perk = task.getPerk()
            # recurse with around tasks
            propagate = self.manageChain(por, around_name, task, perk, layer,
                                         task_params, propagate, chain)
          # get the pointer after the recursive step
          por = self.getPointer()
        
        # get the chain of tasks to run after this one
        for after_name in afters:
          if show:
            self.tier_manager.showChain(AEConstants.CHAIN_AFTER, name)
          # look for a Perk with a Task with the given name
          task = self.getNamedTask(after_name)
          # bail immediately if it doesn't exist
          if task is not None: 
            perk = task.getPerk()
            # recurse with fter tasks
            propagate = self.manageChain(por, after_name, task, perk, layer, 
                                         task_params, propagate, chain)
        # pop the name of this task off the stack permanently
        self.chain_stack.pop()
    # return the current value of propagate
    return propagate

  def manageEvent(self, event):
    '''
    Manages an event by iterating through the L{Perk} stack (top to bottom) and
    checking for registered L{Task}s of the given type. Executes the registered
    L{Task}s (last registered, first executed) in each L{Perk} until one of the 
    following conditions is met:
      - All L{Task}s have executed
      - A L{Task} returns False
      - A L{Task} raises an exception
    
    In the latter two cases, no additional L{Task}s in the current L{Perk} or
    additional L{Perk}s in this L{Tier} are executed. Instead the
    L{Task.Base.Task.update} methods are called to allow housekeeping
    operations (e.g updating state) to be performed.
    
    If a L{Task} returns neither True or False (e.g. it returns None) a 
    warning is logged and the return value is treated as if it were True. This
    likely means the L{Task} forgot to specify a return value.

    @param event: Event to process
    @type event: L{AEEvent.Base.AccessEngineEvent}
    '''
    try:
      # grab POR from any focus type event
      self.focus_por = event.getFocusPOR()
    except AttributeError:
      pass
    
    if not self.wantsEvent(event):
      # quit immediately if no Task wants this event
      return
    
    # pull show into local vars
    show = self.show
    
    # show the event in registered monitors
    if show:
      self.tier_manager.showEvent(event, self.name)
    
    # get these values once to initialize, most are constant for all Tasks
    event_por = event.getPOR()
    task_layer = event.getLayer()
    task_params = event.getDataForTask()
    event_type = type(event)

    # run through all the registered Perks
    propagate = True
    for perk in self.perks:
      # run through all Tasks of the given type in this Perk
      for task in perk.getEventTasks(event_type, task_layer):
        try:
          i = task.getIdentity()
        except ValueError:
          i = None
        # add to linear history before executing so it's available to chained
        # and doTask tasks
        self.task_history.append((i, perk.getClassName()))
        
        if show:
          # execute task and log information
          self.tier_manager.showTask(task, perk)
          propagate = self.manageChain(event_por, i, task, perk, task_layer,
                                        task_params, propagate, True)
          self.tier_manager.showPropagate(propagate)
        else:
          # execute only, duplicated for performance
          propagate = self.manageChain(event_por, i, task, perk, task_layer,
                                        task_params, propagate, True)

  def manageGesture(self, event, count):
    '''
    Manages an event by getting the L{AEInput.Gesture} that triggered it and
    locating a L{Task} registered to execute in response to it. If a L{Task} 
    could not be found for the given event, the L{Task} registered for invalid
    gestures is executed instead.

    @param event: Event to process
    @type event: L{AEEvent.InputGesture}
    @param count: Number of times this gesture has been issued without 
      interruption
    @type count: integer
    '''
    # look for a Perk with a Task registered to respond to this input command
    name = self.getCommandTask(event.getTaskKey())
    if name is None: return
    
    # show the event in registered monitors
    if self.show:
      self.tier_manager.showEvent(event, self.name)
    # extract layer and data for task
    layer = event.getLayer()
    task_params = event.getDataForTask()
    # insert the count into the data for the task
    task_params['cycle_count'] = count
    # look for a Perk with a Task with the given name
    task = self.getNamedTask(name)
    if task is not None: 
      # account for chains on the task
      perk = task.getPerk()
      self.manageChain(None, name, task, perk, layer, task_params, True, True)

  def manageKeyedTask(self, event):
    '''    
    Manages an event by locating a L{Task} under a particular key (not keyboard
    key, but some immutable identifier) registered to execute in response to
    it.

    @param event: Event to process
    @type event: L{AEEvent}
    '''
    # look for a Perk with a Task registered to respond to this event
    task = self.getKeyedTask(event.getTaskKey())
    # pull show into local namespace
    show = self.show
    # show the event in registered monitors
    if show:
      self.tier_manager.showEvent(event, self.name)
    if task is not None:
      layer = event.getLayer()
      perk = task.getPerk()
      task_params = event.getDataForTask()
      if show:
        # execute that Task and log its information
        self.tier_manager.showTask(task, perk)
        try:
          rv = self._executeTask(task, perk, None, layer, task_params, True)
        except AssertionError:
          pass
        self.tier_manager.showPropagate(rv)
      else:
        # execute only, duplicated for performance
        try:
          self._executeTask(task, perk, None, layer, task_params, True)
        except AssertionError:
          pass

  def managePrivate(self, event):
    '''
    Pulls private data from an event and stores it in appropriate instance
    variables. Does not forward the event to L{Perk}s and L{Task}s. Rather,
    the stored information is made public through L{Task.Tools} methods.
    
    @param event: Event to process
    @type event: L{AEEvent}
    '''
    data = event.getDataForTask()
    kind = data['kind']
    if kind == event.KEY_PRESS:
      # store last scan code and key sym string
      self.last_key = data['code'], data['sym'], data['mod']
