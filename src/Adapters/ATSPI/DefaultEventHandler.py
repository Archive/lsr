'''
Defines default L{AccAdapt.Adapter}s for the L{AEInterfaces.IEventHandler} 
interface on L{pyLinAcc.Accessible} objects.

@var EVENT_HANDLERS: Maps event names to method names to be called to 
  handle them
@type EVENT_HANDLERS: dictionary
@var AE_MAP: Mapping from L{AEEvent}s to raw events that must be registered
  to generate them
@type AE_MAP: dictionary

@author: Pete Brunet
@author: Peter Parente
@author: Brett Clippingdale
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
from POR import POR
from AEEvent import *
from AccAdapt import Adapter
from AEInterfaces import *
from pyLinAcc import Interfaces, Constants
import pyLinAcc, AEConstants

# opt: AE to raw event mapping, only registers for raw events when desired
AE_MAP = {ViewChange : ['window:activate', 'window:deactivate', 
                        'window:create', 'window:destroy'],
          FocusChange : ['focus', 'object:state-changed:focused'],
          CaretChange : ['object:text-caret-moved',
                         'object:text-changed'],
          ChildrenChange : ['object:children-changed'],
          PropertyChange : ['object:property-change'],
          SelectorChange : ['object:text-selection-changed',
                            'object:selection-changed',
                            'object:active-descendant-changed'],
          # opt: only register for state events that might be worth reporting 
          # to a script for the time being, lots of state change processing
          # can really slow down the screen reader
          StateChange : ['object:state-changed:expanded', 
                         'object:state-changed:checked',
                         'object:state-changed:enabled',
                         'object:state-changed:sensitive',
                         'object:state-changed:animated',
                         'object:state-changed:busy',
                         'object:state-changed:visible',
                         'object:state-changed:showing'],
          TableChange : ['object:row-inserted',
                         'object:row-deleted',
                         'object:row-reordered',
                         'object:column-inserted',
                         'object:column-deleted',
                         'object:column-reordered'],
          ScreenChange : ['object:bounds-changed',
                          'object:text-bounds-changed',
                          'object:visible-data-changed'],
          MouseChange : ['mouse:abs', 'mouse:button'],
          PrivateChange : ['keyboard:press']
          }

EVENT_HANDLERS = {
  'keyboard:press' : '_handleKeyPressEvent',
  'window:activate': '_handleViewChange',
  'focus' : '_handleFocusEvent',
  'object:state-changed:focused' : '_handleFocusEvent',
  'object:text-changed:insert' : '_handleTextEvent',
  'object:text-changed:delete' : '_handleTextEvent',
  'object:text-caret-moved' : '_handleCaretEvent',
  'object:text-selection-changed' : '_handleTextSelectionEvent',
  'object:selection-changed' : '_handleSelectionChangedEvent',
  'object:active-descendant-changed': '_handleDescendantEvent',
  'object:property-change:accessible-name': '_handlePropertyEvent',
  'object:property-change:accessible-role': '_handlePropertyEvent',
  'object:property-change:accessible-description': '_handlePropertyEvent',
  'object:property-change:accessible-value': '_handlePropertyEvent',
  'object:property-change:accessible-table-caption': '_handlePropertyEvent',
  'object:property-change:accessible-table-summary' : '_handlePropertyEvent',
  'object:property-change:accessible-table-column-description' : 
  '_handlePropertyEvent',
  'object:property-change:accessible-table-row-description' : 
  '_handlePropertyEvent',
  'object:children-changed:add' : '_handleChildrenEvent',
  'object:children-changed:remove' : '_handleChildrenEvent',
  'object:row-inserted' : '_handleTableEvent',
  'object:row-deleted' : '_handleTableEvent',
  'object:row-reordered' : '_handleTableEvent',
  'object:column-inserted' : '_handleTableEvent',
  'object:column-deleted' : '_handleTableEvent',
  'object:column-reordered' : '_handleTableEvent',
  'object:visible-data-changed' : '_handleScreenEvent',
  'object:text-bounds-changed' : '_handleScreenEvent',
  'object:object-bounds-changed' : '_handleScreenEvent'
  # PP: not handling accessible parent as it seems to cause problems
  #'object:property-change:accessible-parent' : '_handleHierarchyEvent',
}

class DefaultEventHandlerAdapter(Adapter):
  '''
  Adapts all events from AT-SPI accessibles to the interfaces defined in
  L{provides}. No condition for adaption is given implying that this adapter is
  used as a default by L{AccAdapt} when no better adapter is available.
  
  This class is meant to be subclassed by more specific event handlers. Only the
  protected handler methods (those starting with _handle) need to be overridden 
  as the public method L{getAEEvents} will call the most child implementation of 
  the appropriate event handling method.
  
  @cvar last_focus: Last accessible to receive focus
  @type last_focus: pyLinAcc.Accessible
  '''
  provides = [IEventHandler]
  singleton = True
  last_focus = None
  
  def getRawEvents(self, kind):
    '''
    Gets a list of raw AT-SPI event names that map to the given kind of 
    L{AEEvent}.
    
    @param kind: Indicates the type of L{AEEvent} some part of the system wants
      to be able to process
    @type kind: L{AEEvent} class
    @return: List of AT-SPI event names
    @rtype: list of string
    @raise KeyError: When no mapping exists for the given event
    '''
    return AE_MAP[kind]
  
  @pyLinAcc.errorToLookupError
  def getAEViewEvents(self, event, collector, vm):
    '''
    Determines if the active view has changed and what events need to be
    fired in response. The possible cases include a normal view change 
    (app1 window to app2 window), a floating widget change (app1 window to
    app1 floater), or a overlay change (app1 window to app2 floater). Resets 
    the last focus when the third case occurs so that the first application
    can announce its restored focus when the floater goes away. For instance,
    if the metacity task switcher appears and the user immediately closes it,
    he lands right back in the window where he started. In this case, the
    new focus is the last focus, but we still want to announce it.
    
    @param event: Raw event
    @type event: L{pyLinAcc.Event.Event}
    @param collector: Callable object that collects L{AEEvent}s to process. 
      Accepts N parameters of type L{AEEvent}.
    @type collector: callable
    @param vm: Reference to the view manager that has information about the 
      current view and needs to store a reference to a new view
    @type vm: L{ViewManager}
    '''
    try:
      # make sure we can get the event type and role
      et = event.type.major
      role = event.source.getRole()
    except Exception:
      # bad event
      return

    # get if this is a window
    is_win = role == Constants.ROLE_WINDOW
    
    if et == 'activate' and (vm.setRawView(event.source) or 
                             not vm.getRawActive()):
        # only a view change if the event source wasn't already the active view
        vm.setRawActive(True)
        collector(ViewChange(vm.getRawView(), AEConstants.EVENT_VIEW_GAINED))
    elif vm.getRawView() is not None and et == 'deactivate':
      por = POR(event.source)
      if por == vm.getRawView():
        # only unset the flag if another activate hasn't been received
        vm.setRawActive(False)
      collector(ViewChange(por, AEConstants.EVENT_VIEW_LOST))
    elif not vm.getRawActive() and is_win:
      # some other application created a floating window
      if et == 'create' and vm.setRawView(event.source):
        collector(ViewChange(vm.getRawView(), AEConstants.EVENT_VIEW_GAINED))
        # forget about the last focus so that a focus event can be fired when
        # the previous application is activated
        DefaultEventHandlerAdapter.last_focus = None
      elif et == 'destroy':
        collector(ViewChange(POR(event.source), AEConstants.EVENT_VIEW_LOST))

  @pyLinAcc.errorToLookupError
  def getAEEvents(self, event, collector):
    '''
    Determines what L{AEEvent}s should be posted to L{EventManager} for later
    execution by a L{Tier} based on the provided raw L{pyLinAcc.Event.Event}.
    Makes an initial decision about whether the event occurred in a focused
    control or an unfocused control.
    
    @param event: Raw event
    @type event: L{pyLinAcc.Event.Event}
    @param collector: Callable object that collects L{AEEvent}s to process. 
      Accepts N parameters of type L{AEEvent}.
    @type collector: callable
    '''
    acc = self.subject
    if acc is None:
      try:
        # handle the case where there is no subject, e.g. a device event
        name = EVENT_HANDLERS[event.type.name]
        method = getattr(self, name)
      except (KeyError, AttributeError):
        return
      # always consider key events focused
      focused = True
    else:
      try:
        ss = acc.getState()
        role = acc.getRole()
        if (ss.contains(Constants.STATE_DEFUNCT) or 
            role == Constants.ROLE_REDUNDANT_OBJECT):
          # abort immediately if the accessible has state defunct or if it has 
          # a redundant object role
          return

        # make a first guess as to whether the object is focused or not
        # NOTE: fixed focus layer bug where events from unfocused controls 
        # came through as being from the focus; testing against last known 
        # focused control now, not just states except in case where no focus
        # event has been received yet
        lf = DefaultEventHandlerAdapter.last_focus
        focused = (lf == event.source or 
                   ss.contains(Constants.STATE_ACTIVE) or
                   (ss.contains(Constants.STATE_FOCUSED) and lf is None))
      except AttributeError:
        # abort immediately if the CORBA object is dead
        return
    
    try:
      # try to hash against an exact method
      name = EVENT_HANDLERS[event.type.name]
      method = getattr(self, name)
    except (KeyError, AttributeError):
      # if that fails, try to check the major event type
      if event.type.klass == 'mouse':
        method = self._handleMouseEvent
      elif event.type.major == 'state-changed':
        method = self._handleStateEvent
      else:
        return
      
    if method == self._handleFocusEvent:
      if self._filterFocusEvent(event):
        # filter unwanted focus events
        return

    # call the method that handles this event
    # keyword arguments will be passed as-is to AEEvent constructors
    events = method(event, focused=focused)

    if events: 
      if method == self._handleFocusEvent:
        # synthesize focus lost events
        if lf is not None:
          lost_events = self._handleUnfocusEvent(lf)
          if lost_events: collector(*lost_events)
        DefaultEventHandlerAdapter.last_focus = event.source
      # send the generated events
      collector(*events)

  def _handleUnfocusEvent(self, source, **kwargs):
    '''
    Creates an L{AEEvent.FocusChange} indicating that the accessible being
    adapted has lost the focus.
    
    @param source: Source of the last raw focus change event
    @type source: L{pyLinAcc.Accessible}
    @param kwargs: Parameters to be passed to any created L{AEEvent}
    @type kwargs: dictionary
    @return: L{AEEvent.FocusChange}
    @rtype: tuple of L{AEEvent}
    '''
    por = POR(source, None, 0)
    return (FocusChange(por, False, **kwargs),)
  
  def _filterFocusEvent(self, event):
    '''
    Determines if a focus event is a repeat on an already focused object or
    not.
    
    @param event: Source of the last raw focus change event
    @type event: L{pyLinAcc.Accessible}
    @return: True if this method believes the event should be ignored, False if
      not
    @rtype: boolean
    '''
    return ((event.type.klass != 'focus' and not event.detail1) or 
            DefaultEventHandlerAdapter.last_focus == event.source or
            event.source.getRole() == Constants.ROLE_PAGE_TAB_LIST)

  def _handleFocusEvent(self, event, **kwargs):
    '''
    Creates an L{AEEvent.FocusChange} indicating that the accessible being
    adapted has gained the focus. Also creates a L{AEEvent.SelectorChange}.
    These two L{AEEvent}s will be posted by the caller.
    
    @param event: Raw focus change event
    @type event: L{pyLinAcc.Event.Event}
    @param kwargs: Parameters to be passed to any created L{AEEvent}
    @type kwargs: dictionary
    @return: L{AEEvent.FocusChange} and L{AEEvent.SelectorChange}
    @rtype: tuple of L{AEEvent}
    '''
    por = POR(self.subject, None, 0)
    # adapt the accessible to an adapter which provides an IAccesssibleInfo
    # interface and get the accessible's item text
    item = IAccessibleInfo(por).getAccItemText()
    # focus events are always in the focus layer
    kwargs['focused'] = True
    return (FocusChange(por, True, **kwargs),  
            SelectorChange(por, item, **kwargs))
  
  def _handlePropertyEvent(self, event, **kwargs):
    '''
    Creates an L{AEEvent.PropertyChange} indicating that some simple property 
    of an accessible changed. These two L{AEEvent}s will be posted by the 
    caller.
    
    The L{POR} for the event soruce returned by this method will be marked as 
    incomplete as the accessible may actually be an item. It will be resolved
    at a later time by the L{AEEvent} if the event will actually be processed.
    
    @param event: Raw property change event
    @type event: L{pyLinAcc.Event.Event}
    @param kwargs: Parameters to be passed to any created L{AEEvent}
    @type kwargs: dictionary
    @return: Property change event
    @rtype: tuple of L{AEEvent}
    '''
    por = POR(self.subject, None, 0, incomplete=True)
    # strip off the initial word
    name = event.type.minor[len('accessible-'):]
    if name == 'value':
      # try to get the property value from the accessible
      iv = Interfaces.IValue(self.subject)
      value = iv.currentValue
    elif name == 'role':
      value = unicode(self.subject.getLocalizedRoleName(), 'utf-8')
    else:
      # otherwise use the string that ships with the event
      value = unicode(event.any_data, 'utf-8')
    return (PropertyChange(por, name, value, **kwargs),)
  
  def _handleStateEvent(self, event, **kwargs):
    '''
    Creates an L{AEEvent.StateChange} indicating that some simple property of
    an accessible changed. These two L{AEEvent}s will be posted by the caller.
    
    The L{POR} for the event soruce returned by this method will be marked as 
    incomplete as the accessible may actually be an item. It will be resolved
    at a later time by the L{AEEvent} if the event will actually be processed.
    
    @param event: Raw state change event
    @type event: L{pyLinAcc.Event.Event}
    @param kwargs: Parameters to be passed to any created L{AEEvent}
    @type kwargs: dictionary
    @return: L{AEEvent.StateChange} event
    @rtype: tuple of L{AEEvent}
    '''
    por = POR(self.subject, None, 0, incomplete=True)
    return (StateChange(por, event.type.minor, event.detail1, **kwargs),)
  
  def _handleChildrenEvent(self, event, **kwargs):
    '''
    Creates an L{AEEvent.ChildrenChange} indicating that a child has been added/
    removed to an accessible. This L{AEEvent} will be posted by the caller.
    
    The L{POR} for the event soruce returned by this method will be marked as 
    incomplete as the accessible may actually be an item. It will be resolved
    at a later time by the L{AEEvent} if the event will actually be processed.
    
    @param event: Raw children change event
    @type event: L{pyLinAcc.Event.Event}
    @param kwargs: Parameters to be passed to any created L{AEEvent}
    @type kwargs: dictionary
    @return: L{AEEvent.ChildrenChange} event
    @rtype: tuple of L{AEEvent}
    '''    
    por = POR(self.subject, None, 0)
    child_por = POR(event.any_data, None, 0, incomplete=True)
    return (ChildrenChange(por, event.type.minor == 'add', child_por, 
                           **kwargs),)
    
  def _handleTableEvent(self, event, **kwargs):
    '''
    Creates an L{AEEvent.TableChange} indicating a insert/delete/reorder of a 
    row/column in a table-based accessible. This L{AEEvent} will be posted by 
    the caller.
    
    @param event: Raw table change event
    @type event: L{pyLinAcc.Event.Event}
    @param kwargs: Parameters to be passed to any created L{AEEvent}
    @type kwargs: dictionary
    @return: L{AEEvent.TableChange} event
    @rtype: tuple of L{AEEvent}
    '''
    por = POR(self.subject, None, 0)
    # per AT-SPI 1.7 specification
    it = Interfaces.ITable(por.accessible) 
    # index of first child of ins/del/reorder
    index1 = it.getIndexAt(event.detail1, 0)
    # index of last child of ins/del/reorder
    index2 = it.getIndexAt(event.detail1 + event.detail2 - 1, 0)   
    first_child_por = POR(self.subject, index1, 0)
    last_child_por = POR(self.subject, index2, 0)
    name = event.type.major
    # determine whether a row or column event?
    is_row = name.startswith('row')
    # event: inserted, deleted or reordered?
    if name.endswith('inserted'):
      added = True
    elif name.endswith('deleted'):
      added = False
    else: # re-ordered
      added = None
    return (TableChange(por, is_row, added, first_child_por,
                        last_child_por, **kwargs),)

  def _handleScreenEvent(self, event, **kwargs):
    '''
    Creates an L{AEEvent.ScreenChange} event in response to a visible data or
    bounds change on an object or text.
    
    @param event: Raw visibility or bounds change event
    @type event: L{pyLinAcc.Event.Event}
    @param kwargs: Parameters to be passed to any created L{AEEvent}
    @type kwargs: dictionary
    @return: L{AEEvent.ScreenChange} event
    @rtype: tuple of L{AEEvent}
    '''
    major = event.type.major
    if major.startswith('text'):
      kind = AEConstants.EVENT_TEXT_BOUNDS
    elif major.startswith('bounds'):
      kind = AEConstants.EVENT_OBJECT_BOUNDS
    else:
      kind = AEConstants.EVENT_VISIBLE_DATA
    por = POR(self.subject, None, 0, incomplete=True)
    return (ScreenChange(por, kind, **kwargs),)

  def _handleMouseEvent(self, event, **kwargs):
    '''
    Creates an L{AEEvent.ScreenChange} event in response to a visible data or
    bounds change on an object or text.
    
    @param event: Raw visibility or bounds change event
    @type event: L{pyLinAcc.Event.Event}
    @param kwargs: Parameters to be passed to any created L{AEEvent}
    @type kwargs: dictionary
    @return: L{AEEvent.ScreenChange} event
    @rtype: tuple of L{AEEvent}
    '''
    major = event.type.major
    if major == 'abs':
      return (MouseChange(AEConstants.EVENT_MOUSE_MOVE, pos=(event.detail1, 
                                                             event.detail2)),)
    elif event.type.minor.endswith('p'):
      kind = AEConstants.EVENT_MOUSE_PRESS
    else:
      kind = AEConstants.EVENT_MOUSE_RELEASE
    return (MouseChange(kind, button=int(event.type.minor[0])),)
  
  def _handleKeyPressEvent(self, event, **kwargs):
    '''
    Creates an L{AEEvent.PrivateChange} event in response to a key press on
    the keyboard. This event is kept private because L{Perk}s and L{Task}s 
    should not be relying on key codes and key syms to trigger actions, but
    rather using abstracted L{AEEvent.InputGesture} events and 
    L{Task.InputTask}s.
    
    @param event: Raw keyboard event
    @type event: L{pyLinAcc.Event.Event}
    @param kwargs: Parameters to be passed to any created L{AEEvent}
    @type kwargs: dictionary
    @return: L{AEEvent.PrivateChange} event
    @rtype: tuple of L{AEEvent}
    '''
    return (PrivateChange(PrivateChange.KEY_PRESS, sym=event.any_data[0], 
                          code=event.detail2, mod=event.any_data[1]),)
