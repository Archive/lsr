'''
Defines L{AccAdapt.Adapter}s for AT-SPI accessibles that manage their 
descendants.

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
import pyLinAcc
from DefaultInfo import *
from DefaultNav import *
from DefaultAction import *
from AEInterfaces import *
from pyLinAcc import Interfaces, Constants

class ContainerAccInfoAdapter(DefaultAccInfoAdapter):
  '''
  Overrides L{DefaultAccInfoAdapter} to provide information specific to 
  containers that have STATE_MANAGES_DESCENDANTS. Expects the subject to be
  a L{POR}.
  '''
  provides = [IAccessibleInfo]
  
  @staticmethod
  def when(subject):
    '''
    Tests if the given subject can be adapted by this class.
    
    @param subject: L{POR} containing an accessible to test
    @type subject: L{POR}
    @return: True when the subject meets the condition named in the docstring
    for this class, False otherwise
    @rtype: boolean
    '''
    ss = subject.accessible.getState()
    return ss.contains(Constants.STATE_MANAGES_DESCENDANTS)
  
  def _getSubItems(self, acc):
    '''
    Gets subitems of an item in a container that manages its descendants.
    Subitems appear to be used to indicate items that cannot be selected 
    independently of each other in a list or tree control so they are made
    children of a dummy accessible. This is most often observed when a list or
    tree has an icon + plus text in one of its cells.
    
    @param acc: Accessible that may or may not have subitems
    @type acc: L{pyLinAcc.Accessible}
    @return: List of accessibles, including the given one
    @rtype: list of L{pyLinAcc.Accessible}
    '''
    si = [acc] + [acc.getChildAtIndex(i) for i in xrange(acc.childCount)]
    return si
  
  @pyLinAcc.errorToLookupError
  def getAccDefTextAttrs(self):
    '''
    Gets all of the default text attributes assigned to the subject if item
    offset is None, else the default attributes of the item offset child.
    
    @return: Name:value pairs of the default text attributes
    @rtype: dictionary
    @raise LookupError: When the accessible object is dead
    @raise IndexError: When the item offset is outside the bounds of the 
      number of children of the subject accessible
    @raise NotImplementedError: When this accessible does not support the text
      interface
    '''
    if self.item_offset is None:
      return super(ContainerAccInfoAdapter, self).getAccDefTextAttrs()
    c = self.accessible.getChildAtIndex(self.item_offset)
    if c is None:
      raise IndexError
    return IAccessibleInfo(POR(c)).getAccDefTextAttrs()
    
  @pyLinAcc.errorToLookupError
  def getAccAllTextAttrs(self):
    '''
    Gets all the text attributes of a given accessible if item offset is None
    or the attributes of the item offset child if it is not.
    
    @return: Name:value pairs of the text attributes at the character offset
    @rtype: dictionary
    @raise LookupError: When the accessible object is dead
    @raise IndexError: When the item offset is outside the bounds of the 
      number of children of the subject accessible
    @raise NotImplementedError: When this accessible does not support the text
      interface
    '''
    if self.item_offset is None:
      return super(ContainerAccInfoAdapter, self).getAccAllTextAttrs()
    c = self.accessible.getChildAtIndex(self.item_offset)
    if c is None:
      raise IndexError
    return IAccessibleInfo(POR(c)).getAccAllTextAttrs()
  
  @pyLinAcc.errorToLookupError
  def getAccSelection(self):
    '''  
    Gets a list of L{POR}s referring to the selected items within the subject 
    accessible.
    
    @return: Points of regard to selected items
    @rtype: list of L{POR}s
    @raise LookupError: When the subject accessible is dead
    '''
    try:
      sel = Interfaces.ISelection(self.accessible)
    except NotImplementedError:
      return []
    # return PORs that have the item index set properly
    return [POR(self.accessible, sel.getSelectedChild(i).getIndexInParent(), 0)
            for i in range(sel.nSelectedChildren)]
    
  @pyLinAcc.errorToLookupError
  def getAccRoleName(self):
    '''
    Gets the accessible role name of the subject if item offset is None else 
    the role name of the given item. The name is localized.
    
    @return: Accessible role name of requested object
    @rtype: string
    @raise LookupError: When the subject accessible or the child is dead
    @raise IndexError: When the item offset is outside the bounds of the 
      number of children of the subject accessible
    '''
    if self.item_offset is None:
      return super(ContainerAccInfoAdapter, self).getAccRoleName()
    try:
      c = self.accessible.getChildAtIndex(self.item_offset)
      #l = [i.getLocalizedRoleName() for i in self._getSubItems(c)]
      return unicode(c.getLocalizedRoleName(), 'utf-8')
    except AttributeError:
      raise IndexError
    
  @pyLinAcc.errorToLookupError
  def getAccRole(self):
    '''
    Gets the accessible role of the subject if item offset is None else 
    the role name of the given item.
    
    @return: Accessible role of requested object
    @rtype: string
    @raise LookupError: When the subject accessible or the child is dead
    @raise IndexError: When the item offset is outside the bounds of the 
      number of children of the subject accessible
    '''
    if self.item_offset is None:
      return super(ContainerAccInfoAdapter, self).getAccRole()
    try:
      c = self.accessible.getChildAtIndex(self.item_offset)
      return c.getRoleName()
    except AttributeError:
      raise IndexError
  
  @pyLinAcc.errorToLookupError
  def getAccName(self):
    '''
    Gets the accessible name of the subject if item offset is None else the 
    name of the given item.
    
    @return: Accessible name of requested object
    @rtype: string
    @raise LookupError: When the subject accessible or the child is dead
    @raise IndexError: When the item offset is outside the bounds of the 
      number of children of the subject accessible
    '''
    if self.item_offset is None:
      return super(ContainerAccInfoAdapter, self).getAccName()
    try:
      c = self.accessible.getChildAtIndex(self.item_offset)
      l = [i.name for i in self._getSubItems(c)]
      return unicode(' '.join(l), 'utf-8')
    except AttributeError:
      raise IndexError
      
  @pyLinAcc.errorToLookupError  
  def getAccDescription(self):
    '''
    Gets the accessible description of the subject if item offset is None else 
    the name of the given item.
    
    @return: Accessible description of requested object
    @rtype: string
    @raise LookupError: When the subject accessible or the child is dead
    @raise IndexError: When the item offset is outside the bounds of the 
      number of children of the subject accessible
    '''
    if self.item_offset is None:
      return super(ContainerAccInfoAdapter, self).getAccDescription()
    try:
      c = self.accessible.getChildAtIndex(self.item_offset)
      l = [i.description for i in self._getSubItems(c)]
      return unicode(' '.join(l), 'utf-8')
    except AttributeError:
      raise IndexError
      
  @pyLinAcc.errorToLookupError
  def getAccItemText(self):
    '''
    Gets the accessible name of the subject if item offset is None else the
    name of the given child accessible.

    @return: Accessible description of requested object
    @rtype: string
    @raise LookupError: When the subject accessible or the child is dead
    @raise IndexError: When the item offset is outside the bounds of the 
      number of children of the subject accessible
    '''
    if self.item_offset is None:
      return super(ContainerAccInfoAdapter, self).getAccItemText()
    try:
      c = self.accessible.getChildAtIndex(self.item_offset)
      # use the IAccessibleInfo interface to get the text from the child to 
      # account for the possibility that the child implements the AT-SPI Text 
      # interface
      l = [IAccessibleInfo(POR(i)).getAccItemText() 
           for i in self._getSubItems(c)]
      return u' '.join(l)
    except AttributeError:
      raise IndexError
  
  @pyLinAcc.errorToLookupError
  def getAccStates(self):
    '''
    Gets a list of names of states indicating the current state of the given 
    accessible.

    @return: Names of all states
    @rtype: list of string
    @raise LookupError: When the subject accessible or the child is dead
    @raise IndexError: When the item offset is outside the bounds of the 
      number of children of the subject accessible
    '''
    if self.item_offset is None:
      return super(ContainerAccInfoAdapter, self).getAccStates()
    try:
      c = self.accessible.getChildAtIndex(self.item_offset)
    except AttributeError:
      raise IndexError
    return IAccessibleInfo(POR(c)).getAccStates()
  
  @pyLinAcc.errorToLookupError 
  def getAccIndex(self):
    '''
    Gets the index of an item as the item offset.

    @return: Zero indexed item offset
    @rtype: integer
    @raise LookupError: When the table or item is no longer valid
    '''
    if self.item_offset is None:
      return super(ContainerAccInfoAdapter, self).getAccIndex()
    else:
      return self.item_offset
  
  @pyLinAcc.errorToLookupError
  def hasAccState(self, state):
    '''
    Gets if the subject has the given state. 
    
    @param state: Name of the state (e.g. 'focused', 'selected', 'selectable')
    @type state: string
    @return: Does the accessible have the given state?
    @rtype: boolean
    @see: L{getAccStates}
    '''
    if self.item_offset is None:
      return super(ContainerAccInfoAdapter, self).hasAccState(state)
    try:
      c = self.accessible.getChildAtIndex(self.item_offset)
    except AttributeError:
      raise IndexError
    return IAccessibleInfo(POR(c)).hasAccState(state)
  
  @pyLinAcc.errorToLookupError
  def hasAccOneState(self, *states):
    '''
    Gets if the subject has at least one of the given states.
    
    @param states: State names(e.g. 'focused', 'selected', 'selectable')
    @type states: string
    @return: Does the accessible have at least one of the given states?
    @rtype: boolean
    '''
    if self.item_offset is None:
      return super(ContainerAccInfoAdapter, self).hasAccOneState(*states)
    try:
      c = self.accessible.getChildAtIndex(self.item_offset)
    except AttributeError:
      raise IndexError
    return IAccessibleInfo(POR(c)).hasAccOneState(*states)
  
  @pyLinAcc.errorToLookupError
  def hasAccAllStates(self, *states):
    '''
    Gets if the subject has all of the given states.
    
    @param states: State names(e.g. 'focused', 'selected', 'selectable')
    @type states: string
    @return: Does the accessible have all of the given states?
    @rtype: boolean
    '''
    if self.item_offset is None:
      return super(ContainerAccInfoAdapter, self).hasAccAllStates(*states)
    try:
      c = self.accessible.getChildAtIndex(self.item_offset)
    except AttributeError:
      raise IndexError
    return IAccessibleInfo(POR(c)).hasAccAllStates(*state)
  
  @pyLinAcc.errorToLookupError
  def getAccActionNames(self):
    '''
    Gets the list of all action names defined by the subject.
    
    @return: List of all action names
    @rtype: list of string
    @raise NotImplementedError: When the subject does not support actions
    '''
    if self.item_offset is None:
      super(ContainerAccInfoAdapter, self).getAccActionNames()
    try:
      c = self.accessible.getChildAtIndex(self.item_offset)
    except AttributeError:
      raise IndexError
    return IAccessibleInfo(POR(c)).getAccActionNames()
  
  @pyLinAcc.errorToLookupError
  def getAccActionDescs(self):
    '''
    Gets the list of all action descriptions defined by the subject.
    
    @return: List of all action descriptions
    @rtype: list of string
    @raise NotImplementedError: When the subject does not support actions
    '''
    if self.item_offset is None:
      super(ContainerAccInfoAdapter, self).getAccActionDescs()
    try:
      c = self.accessible.getChildAtIndex(self.item_offset)
    except AttributeError:
      raise IndexError
    return IAccessibleInfo(POR(c)).getAccActionDescs()

  @pyLinAcc.errorToLookupError
  def getAccActionCount(self):
    '''
    Gets the number of available actions on the subject.
    
    @return: Number of actions available
    @rtype: integer
    @raise NotImplementedError: When the subject does not support actions
    '''
    if self.item_offset is None:
      super(ContainerAccInfoAdapter, self).getAccActionCount()
    try:
      c = self.accessible.getChildAtIndex(self.item_offset)
    except AttributeError:
      raise IndexError
    return IAccessibleInfo(POR(c)).getAccActionCount()

  @pyLinAcc.errorToLookupError
  def getAccActionKeys(self):
    '''
    Gets the key bindings associated with all available actions. Parses the
    key bindings into tuples since some actions can have multiple ways of 
    activating them dependent on the context.
    
    @return: List of tuples, each containing one or more string keysym names
      indicating the keys to press in sequence to perform the action
    @rtype: list of tuple of string
    @raise NotImplementedError: When the subject does not support actions
    '''
    if self.item_offset is None:
      super(ContainerAccInfoAdapter, self).getAccActionKeys()
    try:
      c = self.accessible.getChildAtIndex(self.item_offset)
    except AttributeError:
      raise IndexError
    return IAccessibleInfo(POR(c)).getAccActionKeys()

  @pyLinAcc.errorToLookupError
  def getAccVisualExtents(self):
    '''
    Gets the visual width and height of the subject.

    @return: Width and height extents
    @rtype: 2-tuple of integer
    @raise LookupError: When the accessible object is dead
    '''
    if self.item_offset is None:
      return super(ContainerAccInfoAdapter, self).getAccVisualExtents()
    try:
      c = self.accessible.getChildAtIndex(self.item_offset)
    except AttributeError:
      # not a valid child
      raise IndexError
    # keep the character offset into the child
    return IAccessibleInfo(POR(c,None,self.char_offset)).getAccVisualExtents()

  @pyLinAcc.errorToLookupError
  def getAccVisualPoint(self):
    '''
    Gets the focal point within the subject, where the focal point is defined
    as the center of the active item.

    @return: x,y coordinates
    @rtype: 2-tuple of integer
    @raise LookupError: When the accessible object is dead
    '''
    if self.item_offset is None:
      return super(ContainerAccInfoAdapter, self).getAccVisualPoint()
    try:
      c = self.accessible.getChildAtIndex(self.item_offset)
    except AttributeError:
      # not a valid child
      raise IndexError
    # keep the character offset into the child
    return IAccessibleInfo(POR(c,None,self.char_offset)).getAccVisualPoint() 
  
class ContainerAccActionAdapter(DefaultAccActionAdapter):
  '''
  Overrides L{DefaultAccActionAdapter} to provide information specific to 
  containers that have STATE_MANAGES_DESCENDANTS. Expects the subject to be a
  L{POR}.
  '''
  provides = [IAccessibleAction]
  
  @staticmethod
  def when(subject):
    '''
    Tests if the given subject can be adapted by this class.

    @param subject: L{POR} containing an accessible to test
    @type subject: L{POR}
    @return: True when the subject meets the condition named in the docstring
    for this class, False otherwise
    @rtype: boolean
    '''
    acc = subject.accessible
    c = Constants
    ss = acc.getState()
    return ss.contains(c.STATE_MANAGES_DESCENDANTS)
  
  @pyLinAcc.errorToLookupError
  def setAccFocus(self):
    '''
    Gives the subject accessible the focus when the item offset is None or 
    one of its items when the offset is an integer.
    
    @return: Did accessible accept (True) or refuse (False) the focus change?
    @rtype: boolean
    @raise LookupError: When the accessible object is dead   
    @raise NotImplementedError: When the accessible does not support an
      interface for setting focus
    '''
    if self.item_offset is None:
      super(ContainerAccActionAdapter, self).setAccFocus()
    else:
      child = self.accessible.getChildAtIndex(self.item_offset)
      c = Interfaces.IComponent(child)
      return c.grabFocus()      
  
  @pyLinAcc.errorToLookupError
  def selectAcc(self, all=False):
    '''
    Selects the accessible object implementing this interface when item offset 
    is None and all is False. Selects all items when item offset is None and
    all is True. Otherwise, selects the one item indicated by the item offset 
    in the subject L{POR}.
    
    @param all: Select all items?
    @type all: boolean
    @return: Did accessible accept (True) or refuse (False) the selection?
    @rtype: boolean
    @raise LookupError: When the accessible object is dead
    @raise NotImplementedError: When the accessible does not support an 
      interface for setting the selection
    '''
    if self.item_offset is None:
      return super(ContainerAccActionAdapter,self).selectAcc(all)
    else:
      c = Interfaces.ISelection(self.accessible)
      return c.selectChild(self.item_offset)
    
  @pyLinAcc.errorToLookupError
  def doAccAction(self, index):
    '''
    Executes the accessible action given by the index on the item_offset.
    
    @param index: Index of the action to execute
    @type index: integer
    @return: Did the action execute (True) or not (False)?
    @rtype: boolean
    @raise LookupError: When the accessible object is dead
    @raise NotImplementedError: When the accessible does not support the action
      interface
    '''
    if self.item_offset is None:
      return super(ContainerAccActionAdapter,self).doAccAction(index)
    else:
      child = self.accessible.getChildAtIndex(self.item_offset)
      act = Interfaces.IAction(child)
      return act.doAction(index)
