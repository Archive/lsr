'''
Defines L{AccAdapt.Adapter}s for AT-SPI accessibles implementing the List 
interface for the Gecko toolkit only.  

@author: Scott Haeger
@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
from POR import POR
from AEEvent import *
from AEInterfaces import *
from DefaultEventHandler import DefaultEventHandlerAdapter

from pyLinAcc import Constants, Interfaces
import pyLinAcc, AEConstants
from i18n import _



class ListEventHandlerAdapter(DefaultEventHandlerAdapter):
  '''  
  Overrides L{DefaultEventHandlerAdapter} to fire L{AEEvent.FocusChange}
  events on focus changes for list controls. Also, fires a 
  L{AEEvent.SelectorChange} event for the first selected list item one 
  is present.  Expects the subject to be a L{pyLinAcc.Accessible}.  
  '''
  provides = [IEventHandler]
  
  @staticmethod
  def when(subject):
    '''
    Tests if the given subject can be adapted by this class.
    
    @param subject: Accessible to test
    @type subject: L{pyLinAcc.Accessible}
    @return: True when the subject meets the condition named in the docstring
      for this class, False otherwise
    @rtype: boolean
    '''
    attrs = subject.getAttributes()
    return ('tag:SELECT' in attrs and \
            subject.getApplication().toolkitName == 'Gecko')
      
  def _handleFocusEvent(self, event, **kwargs):
    '''
    Creates an L{AEEvent.FocusChange} indicating that the accessible being
    adapted has gained the focus.  Also, creates a L{AEEvent.SelectorChange}
    for the first selected item.
     
    @param event: Raw focus change event
    @type event: L{pyLinAcc.Event.Event}
    @param kwargs: Parameters to be passed to any created L{AEEvent}
    @type kwargs: dictionary
    @return: L{AEEvent.FocusChange} and L{AEEvent.CaretChange}
    @rtype: tuple of L{AEEvent}
    '''
    events = [] 
    # create focus event on list 
    por = POR(self.subject, None, 0)
    kwargs['focused'] = True
    events.append(FocusChange(por, True, **kwargs))
    # create a selector event for the first selected item
    s = Interfaces.ISelection(self.subject)
    if s.nSelectedChildren > 0:
      listitem = s.getSelectedChild(0)
      # do we want to fake item offset here?
      por = POR(listitem, None, 0)
      listitemtext = IAccessibleInfo(por).getAccItemText()
      events.append(SelectorChange(por, listitemtext, **kwargs))
    return events
    