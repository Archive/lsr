'''
Defines L{AccAdapt.Adapter}s for combo boxes that receive the focus but have
children that fire events.


@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''

from POR import POR
from AEEvent import *
from AEInterfaces import *
from TextAdapter import TextEventHandlerAdapter
from pyLinAcc import Constants, Interfaces
import pyLinAcc
from DefaultNav import *

class ComboboxEventHandlerAdapter(TextEventHandlerAdapter):
  '''  
  Overrides L{TextEventHandlerAdapter} to enable processing of events from
  text areas within combo boxes where the combo box gets focus, the text area
  doesn't, and the text area is the source of all text events. Fires a
  L{AEEvent.FocusChange} and either a L{AEEvent.CaretChange} or
  L{AEEvent.SelectorChange} on focus. Expects the subject to be a
  L{pyLinAcc.Accessible}.
  
  Adapts subject accessibles that have a role of combo box or whose parent have
  have a role of combo box.
  '''
  provides = [IEventHandler]
  
  @staticmethod
  def when(subject):
    '''
    Tests if the given subject can be adapted by this class.
    
    @param subject: Accessible to test
    @type subject: L{pyLinAcc.Accessible}
    @return: True when the subject meets the condition named in the docstring
    for this class, False otherwise
    @rtype: boolean
    '''
    r = subject.getRole()
    pr = subject.parent.getRole()
    c = Constants
    roles = (c.ROLE_COMBO_BOX, c.ROLE_EDITBAR, c.ROLE_AUTOCOMPLETE)
    return (r in roles or pr in roles)
  
  def _getTextArea(self):
    '''
    Looks for a widgets supporting the text interface within the combo box.
    
    @return: The accessible that provides the text interface and the accessible
      already queried to that interface
    @rtype: 2-tuple of L{pyLinAcc.Accessible}
    '''
    text = None
    # see if the combobox is acting as a proxy for the text box
    try:
      text = Interfaces.IText(self.subject)
      return self.subject, text
    except NotImplementedError:
      pass
    # if not, find the child text box
    for i in xrange(self.subject.childCount):
      try:
        acc = self.subject.getChildAtIndex(i)
        text = Interfaces.IText(acc)
        return acc, text
      except NotImplementedError:
        pass
    raise NotImplementedError
  
  def _isFocused(self):
    '''
    Gets if the subject or its parent is focused.
    
    @return: Does the subject or its parent have focus?
    @rtype: boolean
    '''
    f = Constants.STATE_FOCUSED
    if (self.subject.getState().contains(f) or 
        self.subject.parent.getState().contains(f)):
      return True
    return False
      
  def _handleFocusEvent(self, event, **kwargs):
    '''
    Creates an L{AEEvent.FocusChange} indicating that the accessible being
    adapted has gained the focus. Also a L{AEEvent.CaretChange}. This sequence 
    of L{AEEvent}s will be posted by the caller.
     
    @param event: Raw focus change event
    @type event: L{pyLinAcc.Event.Event}
    @param kwargs: Parameters to be passed to any created L{AEEvent}
    @type kwargs: dictionary
    @return: L{AEEvent.FocusChange} and L{AEEvent.CaretChange}
    @rtype: tuple of L{AEEvent}
    '''
    # build a focus event on the subject
    kwargs['focused'] = True
    por = POR(self.subject, None, 0)
    focus_evt = FocusChange(por, True, **kwargs)

    # try to locate the text area in the combo box
    try:
      acc, text = self._getTextArea()
    except NotImplementedError:
      # if we don't support text, then we should have a selection at least
      item = IAccessibleInfo(por).getAccItemText()
      return (focus_evt, SelectorChange(por, item, **kwargs))

    char_offset = text.caretOffset
    # get the text of the caret line, it's starting and ending offsets
    line, sOff, eOff = \
      text.getTextAtOffset(char_offset, Constants.TEXT_BOUNDARY_LINE_START)
    # create a POR with start offset of the line and relative caret offset
    por = POR(acc, sOff, char_offset - sOff)
    
    # create a focus event for this POR
    # and add a caret event to indicate the current position
    return (focus_evt, CaretChange(por, unicode(line, 'utf-8'), sOff, **kwargs))
  
  def _handleTextEvent(self, event, focused, **kwargs):
    '''
    Called when text is inserted or deleted (object:text-changed:insert & 
    object:text-changed:delete). Creates and returns an L{AEEvent.CaretChange}
    to indicate a change in the caret context.
    
    L{pyLinAcc.Event.Event}.type.minor is "insert" or "delete".
    L{pyLinAcc.Event.Event}.detail1 has start offset of text change.
    L{pyLinAcc.Event.Event}.detail2 has length of text change.
    L{pyLinAcc.Event.Event}.any_data has text inserted/deleted.
    
    @param event: Raw text-changed event
    @type event: L{pyLinAcc.Event.Event}
    @param kwargs: Parameters to be passed to any created L{AEEvent}
    @type kwargs: dictionary
    @return: L{AEEvent.CaretChange}
    @rtype: tuple of L{AEEvent}
    '''
    # try to locate the text area in the combo box
    try:
      acc, text = self._getTextArea()
    except NotImplementedError:
      return None
    
    # recompute focus
    focused = focused or self._isFocused()
    
    # get the text of the caret line, it's starting and ending offsets
    line, sOff, eOff = \
      text.getTextAtOffset(text.caretOffset, Constants.TEXT_BOUNDARY_LINE_START)
    # create a POR with start offset of the line and relative caret offset
    por = POR(self.subject, sOff, text.caretOffset - sOff)

    # create Caret event with current POR, text added/removed, where it changed,
    # and whether it was inserted.
    return (CaretChange(por, unicode(event.any_data, 'utf-8'), 
                        event.detail1, (event.type.minor == 'insert'),
                        focused=focused, **kwargs),)
  
  def _handleCaretEvent(self, event, focused, **kwargs):
    '''
    Creates and returns an L{AEEvent.CaretChange} indicating the caret moved in
    the subject accessible.
    
    L{pyLinAcc.Event.Event}.detail1 has caret offset.
    
    @param event: Raw caret movement event
    @type event: L{pyLinAcc.Event.Event}
    @param kwargs: Parameters to be passed to any created L{AEEvent}
    @type kwargs: dictionary
    @return: L{AEEvent.CaretChange}
    @rtype: tuple of L{AEEvent}
    '''
    # try to locate the text area in the combo box
    try:
      acc, text = self._getTextArea()
    except NotImplementedError:
      return None
    
    # recompute focus
    focused = focused or self._isFocused()
    
    # detail1 has new caret position
    caret_offset = event.detail1

    # get the text of the new caret line, it's starting and ending offsets
    line, sOff, eOff = \
      text.getTextAtOffset(caret_offset, Constants.TEXT_BOUNDARY_LINE_START)

    # create a POR with start offset of the line and relative caret offset
    por = POR(self.subject, sOff, caret_offset - sOff)

    # create Caret event with new POR, line at caret, and offset of caret
    return (CaretChange(por, unicode(line, 'utf-8'), caret_offset,
                        focused=focused, **kwargs),)
                                         
                    
class ComboBoxNavAdapter(DefaultNavAdapter):
  '''
  Overrides L{DefaultNavAdapter} to provide navigation over text lines as 
  items and to avoid traversing text children as separate accessible children 
  in the L{IAccessibleNav} interface. 
  
  Adapts accessibles that have a role of terminal or have a state of multiline, 
  single line, or editable and provide the Text interface. 
  Does not adapt L{POR} accessibles with role of page tab.
  '''
  provides = [IItemNav, IAccessibleNav]

  @staticmethod
  def when(subject):
    '''
    Tests if the given subject can be adapted by this class.
    
    @param subject: Accessible to test
    @type subject: L{pyLinAcc.Accessible}
    @return: True when the subject meets the condition named in the docstring
    for this class, False otherwise
    @rtype: boolean
    '''   
    acc = subject.accessible
    r = acc.getRole()
    pr = acc.parent.getRole()
    c = Constants
    roles = (c.ROLE_COMBO_BOX, c.ROLE_EDITBAR, c.ROLE_AUTOCOMPLETE)
    return (r in roles or pr in roles)
  
  def getNextAcc(self):
    '''
    Gets the next accessible relative to the one providing this interface.
    
    @return: Point of regard to the next accessible
    @rtype: L{POR}
    @raise IndexError: When there is no next accessible
    @raise LookupError: When lookup for the next accessible fails even though
      it may exist
    '''
    if self.accessible.getRole() == Constants.ROLE_COMBO_BOX:
      cb = self.accessible
    else: 
      cb = self.accessible.parent
      
    i = cb.getIndexInParent()
    has_parent = cb.parent is not None
    if i < 0 or not has_parent:
      # indicate lookup of the next peer failed
      raise LookupError
    # get the accessible at the next index (the peer)
    child = cb.parent.getChildAtIndex(i+1)
    if child is None:
      # indicate there is no next peer
      raise IndexError
    return POR(child, None, 0)
    
  def getPrevAcc(self):
    '''
    Gets the previous accessible relative to the one providing this interface.
    
    @return: Point of regard to the previous accessible
    @rtype: L{POR}
    @raise IndexError: When there is no previous accessible
    @raise LookupError: When lookup for the previous accessible fails even 
      though it may exist
    '''
    if self.accessible.getRole() == Constants.ROLE_COMBO_BOX:
      cb = self.accessible
    else: 
      cb = self.accessible.parent
    
    i = cb.getIndexInParent()
    has_parent = cb.parent is not None
    if i <= 0 or not has_parent:
      # indicate lookup of the previous peer failed
      raise LookupError
    # get the accessible at the previous index (the peer)
    child = cb.parent.getChildAtIndex(i-1)
    if child is None:
      # indicate there is no previous peer
      raise IndexError
    return POR(child, None, 0)
    
  def getParentAcc(self):
    '''
    Gets the parent accessible relative to the one providing this interface.
    
    @return: Point of regard to the parent accessible
    @rtype: L{POR}
    @raise LookupError: When lookup for the parent accessible fails because it
      does not exist
    '''
    if self.accessible.getRole() == Constants.ROLE_COMBO_BOX:
      cb = self.accessible
    else: # probably textbox
      cb = self.accessible.parent
      
    parent = cb.parent
    if parent is None:
      raise LookupError
    return POR(parent, None, 0)
    
  def getFirstAccChild(self):
    '''
    Gets the first accessible child in the combobox list.
    
    @return: Point of regard to the first list item
    @rtype: L{POR}
    @raise LookupError: When lookup for child fails because it does not exist
    '''
    if self.accessible.getRole() == Constants.ROLE_COMBO_BOX:
      cb = self.accessible
    else: # probably textbox
      cb = self.accessible.parent
      
    # find head of list based on known parent roles
    listhead = self._findListHead(cb)
    if listhead is None:
      raise LookupError
    
    return POR(listhead, None, 0)
    
  def getLastAccChild(self):
    '''
    Gets the last accessible child in combobox list.
    
    @return: Point of regard to the last child accessible
    @rtype: L{POR}
    @raise LookupError: When lookup for child fails because it does not exist
    '''
    if self.accessible.getRole() == Constants.ROLE_COMBO_BOX:
      cb = self.accessible
    else: # probably textbox
      cb = self.accessible.parent
      
    listhead = self._findListHead(cb)
    if listhead is None:
      raise LookupError
    
    child = listhead.parent.getChildAtIndex(listhead.parent.childCount-1)
    if child is None:
      raise LookupError
    return POR(child, None, 0)
    
  def _findListHead(self, cb):
    '''
    Performs a depth only search to find the first list item in a
    combobox list.  Could be of type menu item or list item.  
    
    @return: Point of regard to the first child accessible
    @rtype: L{POR}
    @raise LookupError: When lookup for child fails because it does not exist
    '''
    listhead = None
    c = cb.getChildAtIndex(0)
    while c is not None:
      c_role = c.getRoleName()
      if c_role == 'menu' or c_role == 'list':
        if c.childCount > 0:
          listhead = c.getChildAtIndex(0)
        else:
          listhead = cb
        break
      c = c.getChildAtIndex(0)
    return listhead
  
  
    
