'''
Defines L{AccAdapt.Adapter}s for AT-SPI tree accessibles. Trees implement both
the Table and the Selection interfaces.

@author: Peter Parente
@author: Eirikur Hallgrimsson
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''

import pyLinAcc
from ContainerAdapter import *
from TableAdapter import *
from DefaultNav import *
from AEInterfaces import *
from pyLinAcc import Interfaces, Constants

class TreeAccInfoAdapter(TableAccInfoAdapter):
  '''
  Overrides L{TableAccInfoAdapter} to generate selector events on focus
  and on selection. Expects the subject to be a L{pyLinAcc.Accessible}.
  
  Adapts subject accessibles that provide the L{pyLinAcc.Interfaces.ITable} 
  interface and have ROLE_TREE_TABLE.
  '''  
  provides = [IAccessibleInfo]

  @staticmethod
  def when(por):
    '''
    Tests if the given subject can be adapted by this class.
    
    @param por: Point of regard to test
    @type por: L{POR}
    @return: True when the subject meets the condition named in the docstring
      for this class, False otherwise
    @rtype: boolean
    '''
    acc = por.accessible
    r = acc.getRole()
    # make sure the role indicates it is a tree table
    if r != Constants.ROLE_TREE_TABLE:
      return False
    # make sure the table interface exists
    tab = Interfaces.ITable(acc)
    return True
 
  @pyLinAcc.errorToLookupError 
  def getAccLevel(self):
    '''
    Gets the level of a node in a tree where zero is the root.
    
    @return: Level of the node
    @rtype: integer
    @raise NotImplementedError: When a tree doesn't support the NODE_OF 
      accessible relation
    '''
    # don't count any level when we're on the tree itself, not one of its nodes
    if self.item_offset is None:
      raise NotImplementedError
    
    count = 0
    node = self.accessible.getChildAtIndex(self.item_offset)
    relations = node.getRelationSet()
    if not relations:
      # some tree nodes don't have relations (e.g. multicolumn tree tables)
      # but the node in the first column in this row will have the relation
      table = Interfaces.ITable(self.accessible)
      row = table.getRowAtIndex(self.item_offset)
      node = table.getAccessibleAt(row, 0)
      relations = node.getRelationSet()
    
    if not relations:
      # can't get level if there are no relations at this point
      raise NotImplementedError
      
    def parent(relations):
      for rel in relations:
        if rel.getRelationType() == Constants.RELATION_NODE_CHILD_OF:
          return rel.getTarget(0).getRelationSet() # return parent's relation set
      return None # end of Parent(relations)
     
    while True:
      temp = parent(relations)
      if temp:
        count +=1
        relations = temp # Set up for looking up.
      else:
        return count

class TreeNavAdapter(DefaultNavAdapter):
  '''
  Overrides L{DefaultNavAdapter} to provide navigation over tree cells as items.
  Expects the subject to be a L{POR}.
  
  Adapts subject accessibles that provide the L{pyLinAcc.Interfaces.ITable} 
  interface and have ROLE_TREE_TABLE.
  '''
  provides = [IAccessibleNav, IItemNav]

  @staticmethod
  def when(subject):
    '''
    Tests if the given subject can be adapted by this class.
    
    @param subject: L{POR} containing an accessible to test
    @type subject: L{POR}
    @return: True when the subject meets the condition named in the docstring
    for this class, False otherwise
    @rtype: boolean
    '''
    acc = subject.accessible
    r = acc.getRole()
    return (r == Constants.ROLE_TREE_TABLE and Interfaces.ITable(acc))
  
  @pyLinAcc.errorToLookupError
  def _getVisibleItemExtents(self, only_visible):
    '''
    Gets the item offsets of the first and last items in a tree of cells.
    
    @param only_visible: Only consider the first and last cells visible in
      the tree (True) or the absolute first and last cells (False)?
    @return: First and last item offsets
    @rtype: 2-tuple of integer
    @raise LookupError: When the first or last item or parent accessible is
      not available
    '''
    acc = self.accessible
    if only_visible:
      comp = Interfaces.IComponent(acc)
      e = comp.getExtents(Constants.DESKTOP_COORDS)
      # get the first item
      x, y = e.x+FUDGE_PX, e.y+FUDGE_PX
      first = comp.getAccessibleAtPoint(x, y, Constants.DESKTOP_COORDS)
      # get the last item
      x, y = e.x+e.width-FUDGE_PX, e.y+e.height-FUDGE_PX
      last = comp.getAccessibleAtPoint(x, y, Constants.DESKTOP_COORDS)
    else:
      first = None
      last = None
    # compute indices
    if first:
      i = first.getIndexInParent()
    else:
      i = 0
    if last:
      j = last.getIndexInParent()
    else:
      t = Interfaces.ITable(acc)
      j = t.getIndexAt(t.nRows-1, t.nColumns-1)
    return i, j
  
  @pyLinAcc.errorToLookupError
  def getNextItem(self, only_visible=True): 
    '''
    Gets the next item relative to the one indicated by the L{POR}
    providing this interface.
    
    @param only_visible: True when Item in the returned L{POR} must be visible
    @type only_visible: boolean
    @return: Point of regard to the next item in the same accessible
    @rtype: L{POR}
    @raise IndexError: When there is no next item
    @raise LookupError: When lookup for the next item fails even though it may 
      exist
    '''
    acc = self.accessible
    off = self.item_offset
    # get the first and last indicies
    i, j = self._getVisibleItemExtents(only_visible)
    # and check if the first item is visible since it might be a header
    #v = IAccessibleInfo(POR(acc.getChildAtIndex(0))).isAccVisible()
    #if off is None and v:
    #  # return the possible header
    #  return POR(acc, 0, 0)
    if off is None or off < i:
      # return the first visible, non-header item
      return POR(acc, i, 0)
    elif off+1 >= i and off+1 <= j:
      # compute the row and column offsets
      t = Interfaces.ITable(acc)
      r, c = t.getRowAtIndex(off), t.getColumnAtIndex(off)
      # now find the next item offsets
      if c < t.nColumns-1:
        c += 1
      else:
        r += 1
        c = 0
      # return the previous visible item
      n_off = t.getIndexAt(r, c)
      return POR(acc, n_off, 0)
    else:
      # no more visible items
      raise IndexError
    
  @pyLinAcc.errorToLookupError
  def getPrevItem(self, only_visible=True):
    '''
    Gets the previous item relative to the one indicated by the L{POR} 
    providing this interface. 
    
    @param only_visible: True when Item in the returned L{POR} must be visible
    @type only_visible: boolean
    @return: Point of regard to the previous item in the same accessible
    @rtype: L{POR}
    @raise IndexError: When there is no previous item
    @raise LookupError: When lookup for the previous item fails even though it
      may exist
    '''
    acc = self.accessible
    off = self.item_offset
    comp = Interfaces.IComponent(acc)
    # get the first and last indicies
    i, j = self._getVisibleItemExtents(only_visible)
    # and check if the first item is visible since it might be a header
    #v = IAccessibleInfo(POR(acc.getChildAtIndex(0))).isAccVisible()
    if off is None:
      # no more visible items
      raise IndexError
    elif off > j:
      # return the last visible item
      return POR(acc, j, 0)
    elif off-1 >= i and off-1 <= j:
      # compute the row and column offsets
      t = Interfaces.ITable(acc)
      r, c = t.getRowAtIndex(off), t.getColumnAtIndex(off)
      # now find the previous item offsets
      if c:
        c -= 1
      else:
        r -= 1
        c = t.nColumns-1
      # return the previous visible item
      p_off = t.getIndexAt(r, c)
      return POR(acc, p_off, 0)
    #elif off > 0 and v:
    #  # return the visible header item
    #  return POR(acc, 0, 0)
    else:
      # return the tree iteself
      return POR(acc, None, 0)
    
  @pyLinAcc.errorToLookupError
  def getLastItem(self, only_visible=True):
    '''
    Gets the last item relative to the one indicated by the L{POR} 
    providing this interface.
    
    @param only_visible: True when Item in the returned L{POR} must be visible
    @type only_visible: boolean
    @return: Point of regard to the last item in the same accessible
    @rtype: L{POR}
    @raise LookupError: When lookup for the last item fails even though it may 
      exist
    '''
    acc = self.accessible
    comp = Interfaces.IComponent(acc)
    # try getting the last item by index first
    child = acc.getChildAtIndex(acc.childCount-1)
    if IAccessibleInfo(POR(child)).isAccVisible() or not only_visible:
      return POR(acc, acc.childCount-1, 0)
    # use coords to get the last visible item
    i, j = self._getVisibleItemExtents(only_visible)
    return POR(acc, j, 0)
    
  @pyLinAcc.errorToLookupError
  def getFirstItem(self, only_visible=True):
    '''
    Gets the first item relative to the one indicated by the L{POR} 
    providing this interface.
    
    @param only_visible: True when Item in the returned L{POR} must be visible
    @type only_visible: boolean
    @return: Point of regard to the last item in the same accessible
    @rtype: L{POR}
    @raise LookupError: When lookup for the last item fails even though it may 
      exist
    '''
    acc = self.accessible
    comp = Interfaces.IComponent(acc)
    # try getting the first item by index first
    child = acc.getChildAtIndex(0)
    if IAccessibleInfo(POR(child)).isAccVisible() or not only_visible:
      return POR(acc, 0, 0)
    # use coords to get the last visible item
    i, j = self._getVisibleItemExtents(only_visible)
    return POR(acc, i, 0)
  
  @pyLinAcc.errorToLookupError 
  def getFirstAccChild(self):
    '''
    Always raises LookupError. Tables have items but no children.
    
    @raise LookupError: Always
    '''
    raise LookupError

  @pyLinAcc.errorToLookupError 
  def getLastAccChild(self):
    '''
    Always raises LookupError. Tables have items but no children.
    
    @raise LookupError: Always
    '''
    raise LookupError

  @pyLinAcc.errorToLookupError
  def getChildAcc(self, index):
    '''
    Always raises LookupError. Tables have items but no children.
    
    @raise LookupError: Always
    '''
    raise LookupError