'''
Defines L{AccAdapt.Adapter}s for producing complete L{POR}s for AT-SPI 
accessibles.

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''

from POR import POR
from AccAdapt import PORAdapter, Adapter
from AEInterfaces import *
from pyLinAcc import Constants
import pyLinAcc

class PORFromPORAdapter(PORAdapter):
  '''
  Adapts a L{POR} to the L{IPORFactory} interface. Expects the subject to be a
  L{POR}.
  '''
  provides = [IPORFactory]
  
  @staticmethod
  def when(subject):
    '''
    Tests if the given subject can be adapted by this class.
    
    @param subject: L{POR} to test
    @type subject: L{POR}
    @return: True when the subject meets the condition named in the docstring
    for this class, False otherwise
    @rtype: boolean
    '''
    try:
      subject.accessible
      return True
    except AttributeError:
      return False
  
  @pyLinAcc.errorToLookupError
  def create(self):
    '''
    Returns a complete L{POR} built based on the subject L{POR}. The complete
    L{POR} is guaranteed to match the definition of an LSR point of regard
    with a proper accessible and item index.
    
    This method resolves any problems introduced by the 
    L{DefaultEventHandlerAdapter} when it creates L{POR}s for state, property,
    and children events. These events can originate from children that are 
    items according to LSR, not full accessibles. When this method is called,
    it checks if the accessible in the L{POR} is an immediate descendant of an
    accessible that has STATE_MANAGES_DESCENDANT. If so, it create a L{POR} with
    that parent as the accessible and the item of the descendant as the item
    index. Otherwise, the original L{POR} is returned.
    
    @return: Point of regard
    @rtype: L{POR}
    '''
    acc = self.accessible
    try:
      parent = acc.parent
      ss = parent.getState()
    except AttributeError:
      pass
    else:
      if ss.contains(Constants.STATE_MANAGES_DESCENDANTS):
        return POR(parent, acc.getIndexInParent(), 0)
    return POR(acc, self.item_offset, self.char_offset)
  
class PORFromAccessibleAdapter(Adapter):
  '''  
  Adapts all AT-SPI accessibles to the L{IPORFactory} interface. No condition
  for adaption is given implying that this adapter is used as a default by
  L{AccAdapt} when no better adapter is available.
  '''
  provides = [IPORFactory]
  
  @pyLinAcc.errorToLookupError
  def create(self):
    '''
    Returns a complete L{POR} built based on the subject L{POR}. The complete
    L{POR} is guaranteed to match the definition of an LSR point of regard
    with a proper accessible and item index.
    
    This method resolves any problems introduced by the adapters that create
    L{POR}s that are actually items according to LSR.
    
    @return: Point of regard
    @rtype: L{POR}
    '''
    acc = self.subject
    try:
      parent = acc.parent
      ss = parent.getState()
    except AttributeError:
      pass
    else:
      if ss.contains(Constants.STATE_MANAGES_DESCENDANTS):
        return POR(parent, acc.getIndexInParent(), 0)
    return POR(acc, None, 0)
