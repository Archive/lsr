'''
Defines L{AccAdapt.Adapter}s for objects accessible objects in the AT-SPI
toolkit.

@author: Peter Parente
@author: Pete Brunet
@author: Brett Clippingdale
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
from DefaultInfo import *
from DefaultAction import *
from DefaultNav import *
from DefaultEventHandler import *

from FactoryAdapter import *

from ContainerAdapter import *
from TextAdapter import *
from TableAdapter import *
from TreeAdapter import *
from ComboboxAdapter import *
from PopupAdapter import *
from ImageAdapter import *
from HypertextAdapter import *
from ListAdapter import *

# register all Adapter subclasses in order from most specific to least
import AccAdapt
AccAdapt.registerAdapters([HypertextEventHandlerAdapter,
                           ComboboxEventHandlerAdapter,
                           PopupMenuEventHandlerAdapter,
                           TextEventHandlerAdapter,
                           SimpleTextEventHandlerAdapter,
                           ListEventHandlerAdapter,
                           TableEventHandlerAdapter,
                           DegenerateTableEventHandlerAdapter,
                           DefaultEventHandlerAdapter])
AccAdapt.registerAdapters([HypertextAccInfoAdapter,
                           ImageAccInfoAdapter,
                           MenuPopupAccInfoAdapter,
                           TextAccInfoAdapter,
                           TreeAccInfoAdapter,
                           TableAccInfoAdapter,
                           ContainerAccInfoAdapter,
                           DefaultAccInfoAdapter])
AccAdapt.registerAdapters([ContainerAccActionAdapter,
                           DefaultAccActionAdapter])
AccAdapt.registerAdapters([HypertextNavAdapter,
                           ComboBoxNavAdapter,
                           TreeNavAdapter, 
                           TableNavAdapter, 
                           TextNavAdapter,
                           DefaultNavAdapter])
AccAdapt.registerAdapters([PORFromPORAdapter, PORFromAccessibleAdapter])