'''
Defines default L{AccAdapt.Adapter}s for the L{AEInterfaces.IAccessibleInfo} 
interface on L{pyLinAcc.Accessibility.Accessible} objects.

@author: Peter Parente
@author: Pete Brunet
@author: Brett Clippingdale
@author: Eirikur Hallgrimsson
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
import pyLinAcc
from POR import POR
from AccAdapt import PORAdapter
from AEInterfaces import *
from pyLinAcc import Constants, Interfaces

class DefaultAccInfoAdapter(PORAdapter):
  '''
  Adapts all AT-SPI accessibles to the L{IAccessibleNav} interface. No
  condition for adaption is given imp`lying that this adapter is used as a 
  default by L{AccAdapt} when no better adapter is available. Expects the 
  subject to be a L{POR}.
  
  @cvar VISIBLE_STATES: States that an accessible must have to be considered 
    visible
  @type VISIBLE_STATES: tuple
  @cvar TRIVIAL_STATES: States that an accessible must not have to be
    considered non-trivial
  @type TRIVIAL_STATES: tuple
  @cvar TRIVIAL_ROLES: Roles that an accessible must not have to be considered
    non-trivial
  @type TRIVIAL_ROLES: tuple
  '''
  provides = [IAccessibleInfo]
  
  VISIBLE_STATES = (Constants.STATE_VISIBLE,
                    Constants.STATE_SHOWING)
  TRIVIAL_STATES = (Constants.STATE_DEFUNCT,
                    Constants.STATE_INVALID,
                    Constants.STATE_INDETERMINATE)
  TRIVIAL_ROLES = (Constants.ROLE_INVALID,
                   Constants.ROLE_GLASS_PANE,
                   Constants.ROLE_FILLER,
                   Constants.ROLE_VIEWPORT,
                   Constants.ROLE_MENU_BAR,
                   Constants.ROLE_TOOL_BAR,
                   Constants.ROLE_SCROLL_BAR,
                   Constants.ROLE_PANEL,
                   Constants.ROLE_SPLIT_PANE,
                   Constants.ROLE_SCROLL_PANE,
                   Constants.ROLE_PAGE_TAB_LIST,
                   Constants.ROLE_SEPARATOR)

  @pyLinAcc.errorToLookupError
  def getAccAttrs(self):
    '''
    Gets a list of name:value attribute pairs for the subject.
    
    @return: Name:value pairs of the object attributes
    @rtype: dictionary
    @raise LookupError: When the accessible object is dead
    @raise NotImplementedError: When this accessible does not support object
      attributes
    ''' 
    try:
      attrs = self.accessible.getAttributes()
    except (AttributeError, Constants.NotImplemented,Constants.CORBAException):
      try:
        doc = Interfaces.IDocument(self.accessible)
        attrs = doc.getAttributes()
      except (AttributeError, Constants.NotImplemented,Constants.CORBAException):
        raise NotImplementedError
    return dict([attr.split(':', 1) for attr in attrs])

  @pyLinAcc.errorToLookupError
  def getAccFloatValueExtents(self):
    '''
    Gets the minimum, maximum, and step for the floating point value exposed
    by the subject.

    @return: Minimum, maximum, and step values
    @rtype: 3-tuple of float
    @raise LookupError: When the accessible object is dead
    @raise NotImplementedError: When this accessible does not support the Value
      interface
    '''
    val = Interfaces.IValue(self.accessible)  
    min = val.minimumValue
    max = val.maximumValue
    # minimum increment is sometimes missing, default to 1.0
    try:
      inc = val.minimumIncrement
    except Constants.NotImplemented:
      inc = 1.0
    return (min,max,inc)
  
  @pyLinAcc.errorToLookupError
  def getAccFloatValue(self):
    '''
    Gets the current floating point value of the subject
    
    @return: Current value
    @rtype: float
    @raise LookupError: When the accessible object is dead
    @raise NotImplementedError: When this accessible does not support the Value
      interface    
    '''
    val = Interfaces.IValue(self.accessible)
    return val.currentValue

  @pyLinAcc.errorToLookupError
  def getAccDefTextAttrs(self):
    '''
    Gets all of the default text attributes assigned to the subject.
    
    @return: Name:value pairs of the default text attributes
    @rtype: dictionary
    @raise LookupError: When the accessible object is dead
    @raise NotImplementedError: When this accessible does not support the text
      interface
    '''
    text = Interfaces.IText(self.accessible)
    try:
      attrs = text.getDefaultAttributeSet()
      if attrs is None:
        return {}
    except (AttributeError, Constants.NotImplemented):
      val = text.getDefaultAttributes()
      if val is '':
        return {}
      attrs = val.split('; ', 1)
    return dict([attr.split(':', 1) for attr in attrs])
  
  @pyLinAcc.errorToLookupError
  def getAccTextSelectionCount(self):
    '''
    Gets the number of discontiguous selected text ranges.
    
    @return: Number of selections
    @rtype: integer
    @raise LookupError: When the accessible object is dead
    @raise NotImplementedError: When the accessible does not support the Text
      interface
    '''
    return Interfaces.IText(self.accessible).getNSelections()
  
  @pyLinAcc.errorToLookupError
  def getAccCaret(self):
    '''
    Gets the current offset of the caret in this text object as a L{POR}.
    
    @return: Point of regard of the text caret in this object
    @rtype: L{POR}
    @raise LookupError: When the accessible object is dead
    @raise NotImplementedError: When the accessible does not support the Text
      interface
    '''
    text = Interfaces.IText(self.accessible)
    caret = text.caretOffset
    # get the text of the caret line, it's starting and ending offsets
    line, start, end = \
      text.getTextAtOffset(caret,Constants.TEXT_BOUNDARY_LINE_START)
    # create a POR with start offset of the line and relative caret offset
    return POR(self.accessible, start, caret - start)
  
  @pyLinAcc.errorToLookupError
  def getAccTextCharCount(self):
    '''
    Gets the number of characters in a text body.
    
    @return: Total number of characters
    @rtype: integer
    @raise LookupError: When the accessible object is dead
    @raise NotImplementedError: When the accessible does not support the Text
      interface
    '''
    return Interfaces.IText(self.accessible).characterCount
  
  @pyLinAcc.errorToLookupError
  def getAccTextSelection(self, n=None):
    '''
    Gets a list of all text selections in the subject if n is None, or the nth
    selection if it is specified.

    @param n: Index of selection to be returned, or None to indicate all
    @type n: integer
    @return: List of selected text runs
    @rtype: list of string
    @raise LookupError: When the accessible object is dead
    @raise NotImplementedError: When this accessible does not support the Text
      interface
    '''
    tex = Interfaces.IText(self.accessible)  
    if n is not None:
      ranges = [tex.getSelection(n)]
    else:
      ranges = (tex.getSelection(sel) for sel in xrange(tex.getNSelections()))
    return [tex.getText(*range) for range in ranges]

  @pyLinAcc.errorToLookupError
  def getAccText(self, por):
    '''    
    Gets all text starting at item offset + char offset up to the offset given
    by the L{POR}. Gets from the beginning of the text if subject item offset
    is None. Gets to the end of the text if the L{POR} item offset is None.

    @param por: Point of regard to the end of the text range to get
    @type por: L{POR}
    @return: Text between the offsets
    @rtype: string
    @raise LookupError: When the accessible object is dead
    @raise NotImplementedError: When this accessible does not support the Text
      interface
    '''
    if self.item_offset is None:
      start = 0
    else:
      start = self.item_offset+self.char_offset
    if por.item_offset is None:
      # get all of the text if end is -1, see at-spi doc
      end = -1
    else:
      end = por.item_offset + por.char_offset
    tex = Interfaces.IText(self.accessible)
    return tex.getText(start, end)

  @pyLinAcc.errorToLookupError
  def getAccTableExtents(self):
    '''
    Returns the number of rows and columns in a table as a 2-tuple of integers
    Not implemented for non-Table objects.

    @return: We never return.
    @raise NotImplementedError: Always
    '''
    raise NotImplementedError

  @pyLinAcc.errorToLookupError
  def getAccVisualExtents(self):
    '''
    Gets the size of the bounding box of an accessible.

    @return: Width and height
    @rtype: 2-tuple of integer
    @raise NotImplementedError: When this accessible does not support the
      Component interface
    '''
    c = Interfaces.IComponent(self.accessible)
    bb = c.getExtents(Constants.DESKTOP_COORDS)
    return bb.width, bb.height

  @pyLinAcc.errorToLookupError
  def getAccVisualPoint(self):
    '''
    Gets the visual position of the focal point wihin an accessible object,
    the center in a simple box-like control.

    @return: x,y coordinate
    @rtype: 2-tuple integer
    @raise NotImplementedError: When this accessible does not support the
      Component interface
    '''
    try:
      text = Interfaces.IText(self.accessible)
    except NotImplementedError:
      text = None
      
    if text:
      # use the character extents to track to the current character
      off = self.char_offset
      x, y, w, h = text.getCharacterExtents(off, Constants.DESKTOP_COORDS)
      return x, y
    else:
      # just center on the widget
      c = Interfaces.IComponent(self.accessible)
      bb = c.getExtents(Constants.DESKTOP_COORDS)
      return bb.x+bb.width/2, bb.y+bb.height/2
  
  @pyLinAcc.errorToLookupError
  def getAccPosition(self):
    '''
    Gets the position of the accessible object, usually upper-left corner.
    
    @return: x,y coordinate
    @rtype: 2-tuple integer
    @raise NotImplementedError: When this accessible does not support the
      Component interface
    '''
    c = Interfaces.IComponent(self.accessible)
    return c.getPosition(Constants.DESKTOP_COORDS)

  @pyLinAcc.errorToLookupError
  def getAccTextAttr(self, name):
    '''
    Gets the value of a given attribute name.
    
    @param name: Name of the attribute (eg. fgcolor, bgcolor)
    @type name: string
    @return: Value of the accessible attribute
    @rtype: string
    @raise LookupError: When the accessible object is dead
    @raise NotImplementedError: When this accessible does not support the Text
      interface
    '''
    try:
      text = Interfaces.IText(self.accessible)
      io = self.item_offset or 0
      val, s, e, d = text.getAttributeValue(io+self.char_offset, name)
      return val
    except (AttributeError, Constants.NotImplemented):
      attrs = self.getAccAllTextAttrs()
      return attrs.get(name, '')
  
  @pyLinAcc.errorToLookupError 
  def getAccIndex(self):
    '''
    Gets the index of an item as its child index in its parent.

    @return: Zero indexed child offset under the parent accessible
    @rtype: integer
    @raise LookupError: When the table or item is no longer valid
    '''
    i = self.accessible.getIndexInParent()
    if i < 0:
      raise LookupError
    return i
  
  def getAccLevel(self):
    '''
    Gets the (tree) level for the item object implementing this interface. This
    is NOT IMPLEMENTED for non-tree objects.
     
    @raise NotImplementedError: Always
    '''
    raise NotImplementedError
  
  def getAccRow(self):
    '''    
    Gets the row of the item object implementing this interface. This
    is NOT IMPLEMENTED for non-table objects.
    
    @raise NotImplementedError: Always
    '''
    raise NotImplementedError
  
  def getAccColumn(self):
    '''    
    Gets the column of the item object implementing this interface. This
    is NOT IMPLEMENTED for non-table objects.
    
    @raise NotImplementedError: Always
    '''
    raise NotImplementedError
    
  def getAccRowColIndex(self, row, col):
    '''
    Gets the 1D index of the cell at the given 2D row and column. This is not 
    implemented for non-table objects.
    
    @param row: Row index
    @type row: integer
    @param col: Column index
    @type col: integer
    @raise NotImplementedError: Always
    '''
    raise NotImplementedError

  def getAccColumnHeader(self):
    '''    
    Gets the column header text of the item object implementing this interface. 
    This is NOT IMPLEMENTED for non-table objects.
    
    @raise NotImplementedError: Always
    '''
    raise NotImplementedError

  def getAccRowHeader(self):
    '''    
    Gets the row header text of the item object implementing this interface. 
    This is NOT IMPLEMENTED for non-table objects.
    
    @raise NotImplementedError: Always
    '''
    raise NotImplementedError

  @pyLinAcc.errorToLookupError
  def getAccAllTextAttrs(self):
    '''
    Gets all the text attributes of a given accessible.
    
    @return: Name:value pairs of the text attributes at the character offset
    @rtype: dictionary
    @raise LookupError: When the accessible object is dead
    @raise NotImplementedError: When this accessible does not support the text
      interface
    '''
    text = Interfaces.IText(self.accessible)  
    # use item_offset of zero if None
    if self.item_offset is None:
      io = 0
    else:
      io = self.item_offset
    # get the attributes at the given item_offset (start of a line) plus 
    # char_offset
    index = io+self.char_offset
    try:
      raise AttributeError      
      # do not get default attributes
      attrs, start, end = text.getAttributeRun(index, False)
      if attrs is None:
        return {}
    except (AttributeError, Constants.NotImplemented):
      val, start, end = text.getAttributes(index)
      if val is '':
        return {}
      attrs = val.split('; ', 1)
    return dict([attr.split(':', 1) for attr in attrs])
  
  @pyLinAcc.errorToLookupError
  def getAccSelection(self):
    '''  
    Gets a list L{POR}s referring to the selected child acessibles within the
    subject accessible.
    
    @return: Points of regard to selected accessibles
    @rtype: list of L{POR}s
    @raise LookupError: When the subject accessible is dead
    '''
    try:
      sel = Interfaces.ISelection(self.accessible)
    except NotImplementedError:
      return []
    # return PORs with unique accessibles for each selected child
    return [POR(sel.getSelectedChild(i)) for i in range(sel.nSelectedChildren)]
  
  @pyLinAcc.errorToLookupError
  def getAccChildCount(self):
    '''
    Gets the number of child accessibles for the object implementing this 
    interface.
    
    @return: Number of accessible children
    @rtype: integer
    @raise LookupError: When the accessible object is dead
    '''
    return self.accessible.childCount
  
  @pyLinAcc.errorToLookupError
  def getAccAppName(self):
    '''
    Gets the name of the application application to which the subject accessible 
    belongs.
    
    @return: Name of the subject's application
    @rtype: string
    @raise LookupError: When the accessible or any parent accessible up to the
      application is dead
    '''
    app = self.accessible.getApplication()
    if app is None:
      raise LookupError
    try:
      return unicode(app.name, 'utf-8')
    except UnicodeDecodeError:
      #363431: protect against junk app names
      return ''

  @pyLinAcc.errorToLookupError
  def getAccAppID(self):
    '''
    Gets a unique ID identifying the application to which the subject accessible 
    belongs.
    
    Currently, the name and ID of the application are used. See 
    U{http://www.linuxbase.org/~gk4/a11y/idl/interfaceAccessibility_1_1Application.html#o2} 
    for information about the application ID.
    
    @return: Application's name and runtime ID
    @rtype: 2-tuple of string, integer
    @raise LookupError: When the accessible or any parent accessible up to the
      application is dead
    '''
    app = self.accessible.getApplication()
    try:
      return (app.name, app.id)
    except AttributeError:
      return None

  @pyLinAcc.errorToLookupError
  def getAccAppLocale(self):
    '''
    Gets the POSIX locale of an application as a string.  At the present time 
    this routine returns only the locale set for messages.
    
    @return: POSIX locale as string
    @rtype: string
    @raise LookupError: When the accessible or any parent accessible up to the
      application is dead
    '''
    app = Interfaces.IApplication(self.accessible.getApplication())
    return app.getLocale(Constants.LOCALE_TYPE_MESSAGES)

  @pyLinAcc.errorToLookupError
  def getAccRoleName(self):
    '''
    Gets the accessible role name of the subject. Ignores the given item offset 
    because the default adapter assumes there are no items.
    
    @return: Localized role name
    @rtype: string
    @raise LookupError: When the accessible object is dead
    '''
    return unicode(self.accessible.getLocalizedRoleName(), 'utf-8')
  
  @pyLinAcc.errorToLookupError
  def getAccRole(self):
    '''
    Gets the accessible role of the subject. Ignores the given item offset 
    because the default adapter assumes there are no items.
    
    @return: Unlocalized role
    @rtype: string
    @raise LookupError: When the accessible object is dead
    '''
    return self.accessible.getRoleName()
    
  @pyLinAcc.errorToLookupError
  def getAccName(self):
    '''
    Gets the accessible name of the subject. Ignores the given item offset 
    because the default adapter assumes there are no items.
    
    @return: Accessible name of requested object
    @rtype: string
    @raise LookupError: When the subject accessible is dead
    '''
    return unicode(self.accessible.name, 'utf-8')
        
  @pyLinAcc.errorToLookupError
  def getAccDescription(self):
    ''' 
    Gets the accessible description of the subject. Ignores the given item 
    offset because the default adapter assumes there are no items.

    @return: Accessible description of requested object
    @rtype: string
    @raise LookupError: When the subject accessible is dead
    '''
    return unicode(self.accessible.description, 'utf-8')
  
  @pyLinAcc.errorToLookupError
  def getAccItemText(self):
    '''
    Gets the accessible text of this object or its name if the text is not
    available. Ignores the given item offset because the default adapter 
    assumes there are no items.

    @return: Accessible text of requested item
    @rtype: string
    @raise LookupError: When the accessible object is dead
    '''
    try:
      text = Interfaces.IText(self.accessible).getText(0, -1)
      if text:
        return unicode(text, 'utf-8')
    except NotImplementedError:
      pass
    return self.getAccName()
  
  @pyLinAcc.errorToLookupError
  def getAccRelations(self, kind):
    '''
    Gets a list of accessibles related to the subject accessible in the manner
    given by kind.
    
    @param kind: Name specifying the kind of relations to retrieve
    @type kind: string
    @return: List of L{POR}s related to subject accessible in the manner 
      given by kind
    @rtype: list of L{POR}
    @raise LookupError: When the accessible object is dead
    '''
    # map the kind parameter to a pyLinAcc constant, or stay with the string
    kind = pyLinAcc.stringToConst('relation', kind)
    # get the entire relation set
    rs = self.accessible.getRelationSet()
    pors = []
    for r in rs:
      if r.getRelationType() == kind:
        # found one or more labels
        for i in range(r.getNTargets()):
          # get the target PORs, using IPORFactory to ensure PORs to items are
          # built properly
          por = IPORFactory(r.getTarget(i)).create()
          pors.append(por)
        break
    return pors
  
  @pyLinAcc.errorToLookupError
  def getAccStates(self):
    '''
    Gets a list of names of states indicating the current state of the given 
    accessible. Ignores the given item offset because the default adapter 
    assumes there are no items.

    @return: Names of all states
    @rtype: list
    @raise LookupError: When the accessible object is dead
    '''
    role = self.accessible.getRoleName()
    states = set([pyLinAcc.stateToString(val) for val in 
                  self.accessible.getState().getStates()])
    # collapsed isn't always set properly, so add it if expandable but not
    # expanded
    if 'expandable' in states and 'expanded' not in states:
      states.add('collapsed')
    # give some indicator of not being enabled
    if ('enabled' not in states and role != 'icon' and 
        ('focusable' in states or 'editable' in states or 
         'selectable' in states)):
      states.add('disabled')
    # give an indication of not checked
    if (role.find('check') > -1 and 'checked' not in states):
      states.add('unchecked')
    return states
  
  @pyLinAcc.errorToLookupError
  def isAccTopLevelWindow(self):
    '''
    Gets whether or not the subject accessible is a top-level container in the
    accessible hierarchy.
    
    @return: Is the subject a top-level container or not?
    @rtype: boolean
    @raise LookupError: When the accessible object is dead
    '''
    try:
      app = Interfaces.IApplication(self.accessible.parent)
      return True
    except NotImplementedError:
      return False
   
  @pyLinAcc.errorToLookupError
  def isAccTrivial(self):
    '''    
    Gets if the accessible should be considered trivial. This implementation
    considers an accessible trivial if the state is one of L{TRIVIAL_STATES}
    or the role is one of L{TRIVIAL_ROLES}.
    
    @return: Does the accessible consider itself trivial?
    @rtype: boolean
    @raise LookupError: When the accessible object is dead
    '''
    # if it has a name, it's not trivial
    name = self.accessible.name
    #if not name or not name.strip():
      #return True
    if name and name.strip():
      return False
    # ensure no bad roles
    if self.accessible.getRole() in self.TRIVIAL_ROLES:
      return True
    ss = self.accessible.getState()
    # ensure no bad state
    for state in self.TRIVIAL_STATES:
      if ss.contains(state):
        return True
    return False
  
  @pyLinAcc.errorToLookupError
  def isAccVisible(self):
    '''
    Gets if an accessible is visible. This implementation considers an 
    accessible visible if it has all of the states in L{VISIBLE_STATES}.
    
    @return: Does the accessible consider itself visible?
    @rtype: boolean
    @raise LookupError: When the accessible object is dead
    '''
    ss = self.accessible.getState()
    # ensure all good states
    for state in self.VISIBLE_STATES:
      if not ss.contains(state):
        return False
    return True

  @pyLinAcc.errorToLookupError
  def hasAccState(self, state):
    '''
    Gets if the subject has the given state. 
    
    @param state: State name (e.g. 'focused', 'selected', 'selectable')
    @type state: string
    @return: Does the accessible have the given state?
    @rtype: boolean
    @see: L{getAccStates}
    '''
    return state in self.getAccStates()

  @pyLinAcc.errorToLookupError
  def hasAccOneState(self, *states):
    '''
    Gets if the subject has at least one of the given states.
    
    @param states: States names (e.g. 'focused', 'selected', 'selectable')
    @type states: string
    @return: Does the accessible have at least one of the given states?
    @rtype: boolean
    @see: L{getAccStates}
    '''
    actual = self.getAccStates()
    for desired in states:
      if desired in actual:
        return True
    return False
  
  @pyLinAcc.errorToLookupError
  def hasAccAllStates(self, *states):
    '''
    Gets if the subject has all of the given states.
    
    @param states: State names (e.g. 'focused', 'selected', 'selectable')
    @type states: string
    @return: Does the accessible have all of the given states?
    @rtype: boolean
    @see: L{getAccStates}
    '''
    actual = self.getAccStates()
    for desired in states:
      if desired not in actual:
        return False
    return True

  @pyLinAcc.errorToLookupError
  def hasAccRole(self, role):
    '''
    Gets if the subject has the given role. The given string role is compared
    to the unlocalized string role of the accessible.
    
    @param role: Name of the role (e.g. 'terminal', 'glass pane')
    @type role: string
    @return: Does the accessible have the given role?
    @rtype: boolean
    '''
    return role == self.accessible.getRoleName()

  @pyLinAcc.errorToLookupError
  def hasAccOneRole(self, *roles):
    '''
    Gets if the subject has any one of the given roles. The given role strings
    are compared to the unlocalized string role of the accessible.
    
    @param roles: Names of the roles (e.g. 'terminal', 'glass pane')
    @type roles: list of string
    @return: Does the accessible have any one of the given roles?
    @rtype: boolean
    '''
    return self.accessible.getRoleName() in roles
    

  @pyLinAcc.errorToLookupError
  def getAccActionNames(self):
    '''
    Gets the list of all action names defined by the subject.
    
    @return: List of all action names
    @rtype: list of string
    @raise NotImplementedError: When the subject does not support actions
    '''
    act = Interfaces.IAction(self.accessible)
    return [act.getName(i) for i in xrange(act.nActions)]
  
  @pyLinAcc.errorToLookupError
  def getAccActionDescs(self):
    '''
    Gets the list of all action descriptions defined by the subject.
    
    @return: List of all action descriptions
    @rtype: list of string
    @raise NotImplementedError: When the subject does not support actions
    '''
    act = Interfaces.IAction(self.accessible)
    return [act.getDescription(i) for i in xrange(act.nActions)]

  @pyLinAcc.errorToLookupError
  def getAccActionCount(self):
    '''
    Gets the number of available actions on the subject.
    
    @return: Number of actions available
    @rtype: integer
    @raise NotImplementedError: When the subject does not support actions
    '''
    act = Interfaces.IAction(self.accessible)
    return act.nActions

  @pyLinAcc.errorToLookupError
  def getAccActionKeys(self):
    '''
    Gets the key bindings associated with all available actions. Parses the
    key bindings into tuples since some actions can have multiple ways of 
    activating them dependent on the context.
    
    @return: List of tuples, each containing one or more string keysym names
      indicating the keys to press in sequence to perform the action
    @rtype: list of tuple of string
    @raise NotImplementedError: When the subject does not support actions
    '''
    act = Interfaces.IAction(self.accessible)
    keys = (act.getKeyBinding(i) for i in xrange(act.nActions))
    return [k.split(';') for k in keys]

  def allowsAccEmbeds(self):
    '''
    Always False. Default accessibles don't allow embedding.
    
    @return: False
    @rtype: boolean
    '''
    return False
  
  def isAccEmbedded(self):
    '''
    Gets whether or not the accessible considers itself embedded in its 
    parent's content by calling the parent's L{allowsAccEmbeds} method.
    
    @return: Does the accessible consider itself embedded?
    @rtype: boolean
    @raise LookupError: When the accessible object is dead
    '''
    parent = IAccessibleNav(self.subject).getParentAcc()
    return IAccessibleInfo(parent).allowsAccEmbeds()
  
  def getAccURI(self, index):
    '''
    Obtain a resource locator ('URI') string which can be used to access the 
    content to which this link "points" or is connected.
    
    @return: URI string
    @rtype: string
    @raise LookupError: When the accessible object is invalid
    '''
    # temporary workaround for mozilla bug #379747
    if self.subject.accessible.getApplication().toolkitName == 'Gecko':
      ht = Interfaces.IHypertext(self.subject.accessible.parent)
      c = IAccessibleInfo(self.subject).getAccIndex()
      h = ht.getLink(c)
      return h.getURI(index)
    else:
      return Interfaces.IHyperlink(self.subject).getURI(index)
  
  def getAccAnchorCount(self):
    ''' 
    Obtain he number of separate anchors associated with this accessible.
    
    @return: Number of anchors
    @rtype: integer
    @raise LookupError: When the accessible object is invalid
    '''
    return Interfaces.IHyperlink(self.subject).nAnchors
  