'''
Defines default L{AccAdapt.Adapter}s for the L{AEInterfaces.IAccessibleAction} 
interface on L{pyLinAcc.Accessibility.Accessible} objects.

@author: Brett Clippingdale
@author: Peter Parente
@author: Eirikur Hallgrimsson
@author: Scott Haeger
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
from AccAdapt import PORAdapter
from AEInterfaces import *
from POR import POR
import pyLinAcc
from pyLinAcc import Interfaces, Constants

class DefaultAccActionAdapter(PORAdapter):
  '''
  Adapts all AT-SPI accessibles to the L{IAccessibleAction} interface. No
  condition for adaption is given implying that this adapter is used as a 
  default by L{AccAdapt} when no better adapter is available.
  
  Expects the subject to be a L{pyLinAcc.Accessible}.
  '''
  provides = [IAccessibleAction]
  
  @pyLinAcc.errorToLookupError
  def setAccCaret(self):
    '''
    Moves the caret to the location given by the item offset + the char offset.
    Moves to the beginning of the text if the item offset is None.
    
    @return: Indicator of whether the caret was moved successfully
    @rtype: boolean
    @raise LookupError: When the accessible object is dead
    @raise NotImplementedError: When the Text interface is not supported
    '''
    acc = self.accessible
    if self.item_offset is None: 
      off = 0
    else:
      off = self.item_offset + self.char_offset
    tex = Interfaces.IText(acc)
    return tex.setCaretOffset(off)
  
  @pyLinAcc.errorToLookupError
  def setAccText(self, text):
    '''
    Replace contents of text area with the given text.

    @param text: Text to set as the content of the text area given by the 
      L{POR}
    @type text: string
    @return: Indicator of whether the text was set successfully or not
    @rtype: boolean
    @raise LookupError: When the accessible object is dead   
    @raise NotImplementedError: When the EditableText interface is not 
      supported
    '''
    tex = Interfaces.EditableText(self.accessible)
    return tex.setTextContents(text)

  @pyLinAcc.errorToLookupError
  def insertAccText(self, text, attrs):
    '''
    Inserts text at the location given by the item offset + the char offset.
    Inserts at the beginning of the text if the item offset is None.

    @param text: Text to insert at the given position
    @type text: string
    @param attrs: Dictionary of string name/value pairs of text attributes to
      set on the inserted text
    @type attrs: dictionary
    @return: Indicator of whether the text was inserted successfully or not
    @rtype: boolean
    @raise LookupError: When the accessible object is dead   
    @raise NotImplementedError: When the EditableText interface is not 
      supported
    '''
    acc = self.accessible 
    if self.item_offset is None: 
      off = 0
    else:
      off = self.item_offset + self.char_offset
    tex = Interfaces.EditableText(acc)
    if len(attrs) == 0:
      return tex.insertText(off, text, len(text))
    else:
      attrs = '; '.join(['%s:%s' % (name, val) for name, val in attrs.items()])
      return tex.insertTextWithAttributes(off, text, len(text), attrs)

  @pyLinAcc.errorToLookupError
  def setAccTextAttrs(self, por, attrs):
    '''
    Sets the accessible text attributes starting at item offset + char offset 
    up to the offset given by the L{POR}. Always replaces the current
    attributes at present.

    @param por: Point of regard to the end of the text run
    @type por: L{POR}
    @param attrs: Dictionary of string name/value pairs of text attributes to
      set on the text range
    @type attrs: dictionary
    @return: Indicator of whether the text attributes were set successfully or 
      not
    @rtype: boolean
    @raise LookupError: When the accessible object is dead   
    @raise NotImplementedError: When the EditableText interface is not 
      supported
    '''
    acc = self.accessible
    if self.item_offset is None:
      start_off = 0
    else:
      start_off = self.item_offset + self.char_offset
    text_area = Interfaces.EditableText(acc)
    if por.item_offset is None:
      end_off = text_area.characterCount
    else:
      end_off = por.item_offset + por.char_offset
    # build the attribute string
    attrs = '; '.join(['%s:%s' % (name, val) for name, val in attrs.items()])
    try:
      # always replace text attributes for the time being
      return text_area.setAttributeRun(attrs, start_off, end_off, True)
    except (pyLinAcc.NotImplemented, AttributeError):
      return text_area.setAttributes(attrs, start_off, end_off)

  @pyLinAcc.errorToLookupError
  def copyAccText(self, por):
    '''        
    Copies the text starting at item offset + char offset up to the offset
    given by the L{POR}. Copies from the beginning of the text if subject item
    offset is None. Copies to the end of the text if the L{POR} item offset is
    None.

    @param por: Point of regard to the end terminus of the copy operation
    @type por: L{POR}
    @return: Indicator of whether the text was copied successfully or not
    @rtype: boolean
    @raise LookupError: When the accessible object is dead   
    @raise NotImplementedError: When the EditableText interface is not 
      supported
    '''
    acc = self.accessible
    if self.item_offset is None:
      start_off = 0
    else:
      start_off = self.item_offset + self.char_offset
    text_area = Interfaces.EditableText(acc)
    if por.item_offset is None:
      end_off = text_area.characterCount    
    else:
      end_off = por.item_offset + por.char_offset
    return text_area.copyText(start_off, end_off)
    
  @pyLinAcc.errorToLookupError
  def cutAccText(self, por):
    '''    
    Cuts the text starting at item offset + char offset up to the offset given
    by the L{POR}. Cuts from the beginning of the text if subject item offset
    is None. Cuts to the end of the text if the L{POR} item offset is None.

    @param por: Point of regard to the end terminus of the cut operation
    @type por: L{POR}
    @return: Indicator of whether the text was cut successfully or not
    @rtype: boolean
    @raise LookupError: When the accessible object is dead   
    @raise NotImplementedError: When the EditableText interface is not supported
    '''
    acc = self.accessible
    if self.item_offset is None:
      start_off = 0
    else:
      start_off = self.item_offset + self.char_offset
    text_area = Interfaces.EditableText(acc)
    if por.item_offset is None:
      end_off = text_area.characterCount    
    else:
      end_off = por.item_offset + por.char_offset
    return text_area.cutText(start_off, end_off)

  @pyLinAcc.errorToLookupError
  def deleteAccText(self, por):
    '''    
    Deletes the text starting at item offset + char offset up to the offset
    given by the L{POR}. Deletes from the beginning of the text if subject item
    offset is None. Deletes to the end of the text if the L{POR} item_offset is
    None.

    @param por: Point of regard to the end terminus of the delete operation
    @type por: L{POR}
    @return: Indicator of whether the text was deleted successfully or not
    @rtype: boolean
    @raise LookupError: When the accessible object is dead   
    @raise NotImplementedError: When the EditableText interface is not 
      supported
    '''
    acc = self.accessible
    if self.item_offset is None:
      start_off = 0
    else:
      start_off = self.item_offset + self.char_offset
    text_area = Interfaces.EditableText(acc)
    if por.item_offset is None:
      end_off = text_area.characterCount    
    else:
      end_off = por.item_offset + por.char_offset
    return text_area.deleteText(start_off, end_off)

  @pyLinAcc.errorToLookupError
  def pasteAccText(self):
    '''
    Pastes the text at item offset + char offset. Pastes from the beginning of
    the text is item offset is None.

    @return: Indicator of whether the text was pasted successfully or not
    @rtype: boolean
    @raise LookupError: When the accessible object is dead   
    @raise NotImplementedError: When the EditableText interface is not 
      supported
    '''
    acc = self.accessible
    if self.item_offset is None:
      start_off = 0
    else:
      start_off = self.item_offset + self.char_offset
    text_area = Interfaces.EditableText(acc)
    return text_area.pasteText(start_off)
  
  @pyLinAcc.errorToLookupError
  def selectAccText(self, por, n=None):
    '''
    Adds a new selection if n is None or sets the nth selection otherwise. 
    Selects the text starting at item offset + char offset up to the offset
    given by the L{POR}. Selects to the end of the text if the L{POR} 
    item_offset is None.
    
    @param por: Point of regard to the end terminus of the select operation
    @type por: L{POR}
    @return: Indicator of whether the text was selected successfully or not
    @rtype: boolean
    @raise LookupError: When the accessible object is dead   
    @raise NotImplementedError: When the Text interface is not supported
    '''
    acc = self.accessible
    if self.item_offset is None:
      start_off = 0
    else:
      start_off = self.item_offset + self.char_offset
    text_area = Interfaces.Text(acc)
    if por.item_offset is None:
      end_off = text_area.characterCount    
    else:
      end_off = por.item_offset + por.char_offset
    if n is None:
      # add a new selection range
      return text_area.addSelection(start_off, end_off)
    else:
      # set a given selection to a new range
      return text_area.setSelection(n, start_off, end_off)

  def deselectAccText(self, n=None):
    '''
    Removes a all text selections if n is None or removes the nth text 
    selection otherwise. When n is None, returns True if at least one selection
    was removed.
    
    @return: Indicator of whether the text was deselected successfully or not
    @rtype: boolean
    @raise LookupError: When the accessible object is dead   
    @raise NotImplementedError: When the Text interface is not supported
    '''
    text_area = Interfaces.Text(acc)
    if n is None:
      # deselect all
      values = [text_area.removeSelection(i) for i in 
                xrange(text_area.getNSelections())]
      # return if any one succeeds
      return reduce(lambda x, y: x or y, values)
    else:
      # deselect one
      return text_area.removeSelection(n)

  @pyLinAcc.errorToLookupError
  def doMouseAction(self, event):
    '''
    Performs a mouse event on the center of a given point of regard.
    
    @param event: Mouse event to perform
    @type event: integer (EVENT_SYNTHMOUSE*)
    @return: Indicator of whether the event succeded or not
    @raise LookupError: When the accessible object is dead
    '''
    ai = IAccessibleInfo(self.subject)
    vpt_x, vpt_y = ai.getAccPosition()
    width, height = ai.getAccVisualExtents()
    devcont = pyLinAcc.Registry.getDeviceEventController()
    devcont.generateMouseEvent(vpt_x + width*.5, vpt_y+ height*.5, 
                               event)
    # we don't get notification of whether it succeeded or failed from AT-SPI,
    # always assume success
    return True
  
  @pyLinAcc.errorToLookupError
  def setAccFocus(self):
    '''
    Gives the subject accessible the keyboard focus. 
    
    @return: Did accessible accept (True) or refuse (False) the focus change?
    @rtype: boolean
    @raise LookupError: When the accessible object is dead   
    @raise NotImplementedError: When the accessible does not support an
      interface for setting focus
    '''
    c = Interfaces.IComponent(self.accessible)
    return c.grabFocus()
  
  @pyLinAcc.errorToLookupError
  def selectAcc(self, all=False):
    '''    
    Selects the subject accessible if all is False. Selects all children if all
    is True.
    
    @param all: Select all children?
    @type all: boolean
    @return: Did accessible accept (True) or refuse (False) the selection?
    @rtype: boolean
    @raise LookupError: When the accessible object is dead
    @raise NotImplementedError: When the accessible does not support an
      interface for setting selection
    '''
    acc = self.accessible
    if all:
      # get the selection interface on this accessible
      isel = Interfaces.ISelection(acc)
      # try to select all children
      return isel.selectAll()
    else:
      # get this accessible's index in its parent
      i = acc.getIndexInParent()
      parent = acc.parent
      # try to select using the parent
      isel = Interfaces.ISelection(parent)
      return isel.selectChild(i)
    
  @pyLinAcc.errorToLookupError
  def deselectAcc(self, all=False):
    '''
    Deselects the subject accessible if all is False. Deselects all children if
    all is True.
    
    @param all: Deselect all children?
    @type all: boolean
    @return: Did accessible accept (True) or refuse (False) the deselection?
    @rtype: boolean
    @raise LookupError: When the accessible object is dead
    @raise NotImplementedError: When the accessible does not support an
      interface for setting selection
    '''
    acc = self.accessible
    if all:
      # get the selection interface on this accessible
      isel = Interfaces.ISelection(acc)
      # try to select all children
      return isel.clearSelection()
    else:
      # get this accessible's index in its parent
      i = acc.getIndexInParent()
      parent = acc.parent
      # try to select using the parent
      isel = Interfaces.ISelection(parent)
      return isel.deselectChild(i)
  
  @pyLinAcc.errorToLookupError
  def doAccAction(self, index):
    '''
    Executes the accessible action given by the index.
    
    @param index: Index of the action to execute
    @type index: integer
    @return: Did the action execute (True) or not (False)?
    @rtype: boolean
    @raise LookupError: When the accessible object is dead
    @raise NotImplementedError: When the accessible does not support the action
      interface
    '''
    act = Interfaces.IAction(self.accessible)
    return act.doAction(index)
