'''
Defines default L{AccAdapt.Adapter}s for the L{AEInterfaces.IAccessibleNav} and
L{AEInterfaces.IItemNav} interfaces on L{POR.POR} objects.

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
from POR import POR
from AccAdapt import PORAdapter
from AEInterfaces import *
from pyLinAcc import Interfaces
import pyLinAcc

class DefaultNavAdapter(PORAdapter):
  '''  
  Adapts all AT-SPI accessibles to the L{IAccessibleNav} and L{IItemNav}
  interfaces. No condition for adaptation is given implying that this adapter is
  used as a default by L{AccAdapt} when no better adapter is available. Expects 
  the subject to be a L{POR}.
  '''
  provides = [IAccessibleNav, IItemNav]
  
  def getNextItem(self, only_visible=True): 
    '''
    Always raises IndexError as this default adapter assumes the subject has 
    only one item.
    
    @param only_visible: True when Item in the returned L{POR} must be visible
    @type only_visible: boolean
    @return: Point of regard to the next item in the same accessible
    @rtype: L{POR}
    @raise IndexError: When there is no next item
    '''
    raise IndexError
    
  def getPrevItem(self, only_visible=True):
    '''
    Always raises IndexError as this default adapter assumes the subject has 
    only one item.
    
    @param only_visible: True when Item in the returned L{POR} must be visible
    @type only_visible: boolean
    @return: Point of regard to the previous item in the same accessible
    @rtype: L{POR}
    @raise IndexError: When there is no previous item
    '''
    raise IndexError
  
  def getFirstItem(self, only_visible=True):
    '''    
    Gets a L{POR} pointing to the first item in the subject L{POR}. Ignores the
    only visible flag.
    
    @param only_visible: True when Item in the returned L{POR} must be visible
    @type only_visible: boolean
    @return: Point of regard to the first item in the same accessible
    @rtype: L{POR}
    @raise IndexError: When there is no first item
    '''
    return POR(self.accessible, None, 0)
  
  def getLastItem(self, only_visible=True):
    '''
    Gets a L{POR} pointing to the first item in the subject L{POR} since it is
    assumed to be the only item in this default adapter. Ignores the only
    visible flag.
    
    @param only_visible: True when Item in the returned L{POR} must be visible
    @type only_visible: boolean
    @return: Point of regard to the last item in the same accessible
    @rtype: L{POR}
    @raise IndexError: When there is no last item
    '''
    return POR(self.accessible, None, 0)
  
  def getAccAsItem(self, por):
    '''
    Always raises IndexError as this default adapter assumes the subject has 
    only one item.
    
    @param por: Point of regard to a child of the subject
    @type por: L{POR}
    @return: Point of regard to an item of the subject
    @rtype: L{POR}
    @raise IndexError: When there is no next item
    '''
    raise IndexError
  
  @pyLinAcc.errorToLookupError
  def getNextAcc(self):
    '''
    Gets the next peer accessible object if possible and if it exists.
    
    @return: Point of regard to the next accessible, or None if there is no next
      peer
    @rtype: L{POR}
    @raise IndexError: When there is no next accessible
    @raise LookupError: When lookup for the next accessible fails even though
      it may exist
    '''
    acc = self.accessible
    # get the index of this accessible
    i = acc.getIndexInParent()
    has_parent = acc.parent is not None
    if i < 0 or not has_parent:
      # indicate lookup of the next peer failed
      raise LookupError
    # get the accessible at the next index (the peer)
    child = acc.parent.getChildAtIndex(i+1)
    if child is None:
      # indicate there is no next peer
      raise IndexError
    return POR(child, None, 0)

  @pyLinAcc.errorToLookupError
  def getPrevAcc(self):
    '''    
    Gets the previous peer accessible object if possible and if it exists.
    
    @return: Point of regard to the previous accessible
    @rtype: L{POR}
    @raise IndexError: When there is no previous accessible
    @raise LookupError: When lookup for the previous accessible fails even 
      though it may exist
    '''
    acc = self.accessible
    # get the index of this accessible
    i = acc.getIndexInParent()
    has_parent = acc.parent is not None
    if i <= 0 or not has_parent:
      # indicate lookup of the previous peer failed
      raise LookupError
    # get the accessible at the previous index (the peer)
    child = acc.parent.getChildAtIndex(i-1)
    if child is None:
      # indicate there is no previous peer
      raise IndexError
    return POR(child, None, 0)

  @pyLinAcc.errorToLookupError
  def getParentAcc(self):
    '''    
    Gets the parent accessible object if possible and if it exists.
    
    @return: Point of regard to the parent accessible
    @rtype: L{POR}
    @raise LookupError: When lookup for the parent accessible fails because it
      does not exist
    '''
    # get the parent of this accessible
    parent = self.accessible.parent
    if parent is None:
      raise LookupError
    return POR(parent, None, 0)
  
  @pyLinAcc.errorToLookupError 
  def getFirstAccChild(self):
    '''
    Gets the first accessible child relative to the subject accessible.
    
    @return: Point of regard to the first child accessible
    @rtype: L{POR}
    @raise LookupError: When lookup for child fails because it does not exist
    '''
    child = self.accessible.getChildAtIndex(0)
    if child is None:
      raise LookupError
    return POR(child, None, 0)

  @pyLinAcc.errorToLookupError 
  def getLastAccChild(self):
    '''
    Gets the last accessible child relative to the subject accessible.
    
    @return: Point of regard to the last child accessible
    @rtype: L{POR}
    @raise LookupError: When lookup for child fails because it does not exist
    '''
    child = self.accessible.getChildAtIndex(self.accessible.childCount-1)
    if child is None:
      raise LookupError
    return POR(child, None, 0)

  @pyLinAcc.errorToLookupError  
  def getChildAcc(self, index):
    '''
    Gets the child accessible at the given index relative to the one providing
    this interface.
    
    @param index: Index of the child to retrieve
    @type index: integer
    @return: Point of regard to the child accessible
    @rtype: L{POR}
    @raise IndexError: When there is no child at the given index
    @raise LookupError: When the lookup for the child fails even though it may
      exist
    '''
    child = self.accessible.getChildAtIndex(index)
    if child is None:
      raise IndexError
    return POR(child, None, 0)
  
  @pyLinAcc.errorToLookupError
  def findDescendantAcc(self, predicate, depth_first):
    '''
    Searches all descendants for one matching the given predicate.
    
    @warning: The predicate is currently supplied with accessibles, not PORs.
    This behavior will likely change in the future.
    
    @param predicate: Search predicate
    @type predicate: callable
    @param depth_first: Perform the search in depth-first (True) or breadth 
      first (False) order?
    @type depth_first: boolean
    @return: Point of regard to the target or None if not found
    @rtype: L{POR}
    @raise LookupError: When the lookup fails during the search
    '''
    # define our own predicate which will create PORs
    def por_predicate(acc):
      if acc is not None:
        por = IPORFactory(acc).create()
        return predicate(por)
      return False
    acc = self.accessible.findDescendant(por_predicate, not depth_first)
    if acc is not None:
      acc = IPORFactory(acc).create()
    return acc
  
  @pyLinAcc.errorToLookupError
  def findAncestorAcc(self, predicate):
    '''
    Searches all ancestors for one matching the given predicate.
    
    @warning: The predicate is currently supplied with accessibles, not PORs.
    This behavior will likely change in the future.
    
    @param predicate: Search predicate
    @type predicate: callable
    @return: Point of regard to the target or None if not found
    @rtype: L{POR}
    @raise LookupError: When the lookup fails during the search
    '''
    # define our own predicate which will create PORs
    def por_predicate(acc):
      if acc is not None:
        por = IPORFactory(acc).create()
        return predicate(por)
      return False
    acc = self.accessible.findAncestor(por_predicate)
    if acc is not None:
      acc = IPORFactory(acc).create()
    return acc
