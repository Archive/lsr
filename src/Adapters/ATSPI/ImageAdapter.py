'''
Defines an L{AccAdapt.Adapter}s for the L{AEInterfaces.IAccessibleInfo} 
interface to report text information about images.

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''

from DefaultInfo import *
from AEInterfaces import *
from pyLinAcc import Interfaces, Constants

class ImageAccInfoAdapter(DefaultAccInfoAdapter):
  '''
  Overrides L{DefaultNavAdapter} to provide text information about images. 
  Expects the subject to be a L{POR}.

  Adapts accessibles that provide the Image interface and
  have a role of image or icon.
  '''
  provides = [IAccessibleInfo]
  
  @staticmethod
  def when(subject):
    '''
    Tests if the given subject can be adapted by this class.
    
    @param subject: L{POR} containing an accessible to test
    @type subject: L{POR}
    @return: True when the subject meets the condition named in the docstring
    for this class, False otherwise
    @rtype: boolean
    '''
    acc = subject.accessible
    r = acc.getRole()
    c = Constants
    return ((r == c.ROLE_ICON or r == c.ROLE_IMAGE) and 
            Interfaces.IImage(acc) is not None)
  
  @pyLinAcc.errorToLookupError
  def getAccItemText(self):
    '''
    Gets the accessible description of the image. Ignores the given item offset 
    because images shouldn't have children.

    @return: Accessible name of requested object
    @rtype: string
    @raise LookupError: When the accessible object is dead
    '''
    acc = self.accessible
    s = acc.name or Interfaces.IImage(acc).imageDescription
    return unicode(s, 'utf-8')
