'''
Defines L{AccAdapt.Adapter}s for AT-SPI accessibles implementing the Text 
interface.

@author: Pete Brunet
@author: Peter Parente
@author: Brett Clippingdale
@author: Scott Haeger
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
from POR import POR
from AEEvent import *
from AEInterfaces import *
from DefaultInfo import DefaultAccInfoAdapter
from DefaultNav import *
from DefaultEventHandler import DefaultEventHandlerAdapter
from pyLinAcc import Constants, Interfaces
import pyLinAcc, AEConstants
from i18n import _
import time

FUDGE_PX = 3

def _multiLineCondition(acc):
  '''
  Tests whether an accessible should be treated as a multiline text area or
  a simple one line text control.
  
  @param acc: Accessible to test
  @type acc: L{pyLinAcc.Accessible}
  '''
  Interfaces.IText(acc)
  r = acc.getRole()
  c = Constants
  if r in (c.ROLE_TERMINAL, c.ROLE_PARAGRAPH):
    # terminals and paragraphs are always multiline
    # note: this does not account for paragraphs that implement hypertext
    return True
  elif r in (c.ROLE_PAGE_TAB, c.ROLE_LABEL, c.ROLE_EDITBAR):
    # these widgets sometimes show as multiline, but are succinct enough to
    # treat as single line
    return False
  s = acc.getState()
  if s.contains(c.STATE_SINGLE_LINE):
    # single line widgets are not multiline (duh)
    return False
  # multiline widgets that weren't filtered in previous tests or editable
  # widgets are considered multiline
  return s.contains(c.STATE_MULTI_LINE) #or s.contains(c.STATE_EDITABLE)

class TextAccInfoAdapter(DefaultAccInfoAdapter):
  '''
  Overrides L{DefaultAccInfoAdapter} to provide information about lines of text
  as items. Expects the subject to be a L{POR}.
  
  Adapts accessibles that have a role of terminal or have a state of multiline, 
  single line, or editable and provide the Text interface. 
  Does not adapt L{POR} accessibles with role of page tab.
  '''
  provides = [IAccessibleInfo]
  
  @staticmethod
  def when(subject):
    '''
    Tests if the given subject can be adapted by this class.
    
    @param subject: L{POR} containing an accessible to test
    @type subject: L{POR}
    @return: True when the subject meets the condition named in the docstring
      for this class, False otherwise
    @rtype: boolean
    '''
    return _multiLineCondition(subject.accessible)
  
  @pyLinAcc.errorToLookupError
  def getAccItemText(self):
    '''
    Gets the accessible text of the subject if item_offset is None and if that
    text is not an empty string. If it is empty, gets the accessible name. If
    item_offset is specified, gets that line of text.
    
    @return: Accessible text of requested object
    @rtype: string
    @raise LookupError: When the subject accessible is dead
    @raise IndexError: When the item offset is outside the bounds of the number
      of lines of text in the subject accessible     
    '''
    text = Interfaces.IText(self.accessible)
    if self.item_offset is None:
      # gets the accessible name of the parent if item offset is None
      return super(TextAccInfoAdapter, self).getAccName()
    else:
      # get the line at the provided offset
      line, sOff, eOff = \
          text.getTextAtOffset(self.item_offset, 
                               Constants.TEXT_BOUNDARY_LINE_START)
      return unicode(line, 'utf-8')
    return u''
  
  #@pyLinAcc.errorToLookupError
  #def getAccRoleName(self):
    #'''
    #Gets the accessible role name of the subject. Ignores the given item offset 
    #because the default adapter assumes there are no items.
    
    #@return: Localized role name
    #@rtype: string
    #@raise LookupError: When the accessible object is dead
    #'''
    #if self.item_offset is None:
      ## gets the accessible role name of the parent if item offset is None
      #return super(TextAccInfoAdapter, self).getAccRoleName()
    #else:
      #return _('text line')

  @pyLinAcc.errorToLookupError
  def getAccVisualExtents(self):
    '''
    Gets the visual width and height of a character or the entire text area if
    item offset is None.

    @return: Width and height extents
    @rtype: 2-tuple of integer
    @raise LookupError: When the accessible object is dead
    '''
    if self.item_offset is None:
      return super(TextAccInfoAdapter, self).getAccVisualExtents()
    off = self.item_offset + self.char_offset
    text = Interfaces.IText(self.accessible)
    x, y, w, h = text.getCharacterExtents(off, Constants.DESKTOP_COORDS)
    return w, h

  @pyLinAcc.errorToLookupError
  def getAccVisualPoint(self):
    '''
    Gets the center of a character or the entire text area if item offset is
    None.

    @return: x,y coordinates
    @rtype: 2-tuple of integer
    @raise LookupError: When the accessible object is dead
    '''
    if self.item_offset is None:
      return super(TextAccInfoAdapter, self).getAccVisualPoint()
    # compute the offset in the line
    off = self.item_offset + self.char_offset
    text = Interfaces.IText(self.accessible)
    x, y, w, h = text.getCharacterExtents(off, Constants.DESKTOP_COORDS)
    return x+w/2, y+h/2

class TextNavAdapter(DefaultNavAdapter):
  '''
  Overrides L{DefaultNavAdapter} to provide navigation over text lines as 
  items and to avoid traversing text children as separate accessible children 
  in the L{IAccessibleNav} interface. 
  
  Adapts accessibles that have a role of terminal or have a state of multiline, 
  single line, or editable and provide the Text interface. 
  Does not adapt L{POR} accessibles with role of page tab.
  '''
  provides = [IItemNav, IAccessibleNav]

  @staticmethod
  def when(subject):
    '''
    Tests if the given subject can be adapted by this class.
    
    @param subject: L{POR} containing an accessible to test
    @type subject: L{POR}
    @return: True when the subject meets the condition named in the docstring
    for this class, False otherwise
    @rtype: boolean
    '''
    return _multiLineCondition(subject.accessible)
  
  @pyLinAcc.errorToLookupError
  def _getVisibleItemExtents(self, only_visible):
    '''
    Gets the item offsets of the first and last items in a text control.
    
    @param only_visible: Only consider the first and last characters visible in
      the text box (True) or the absolute first and last characters (False)?
    @type only_visible: boolean
    @return: First and last item offsets
    @rtype: 2-tuple of integer
    @raise LookupError: When the accessible does not implement IComponent
    '''
    acc = self.accessible
    comp = Interfaces.IComponent(acc)
    text = Interfaces.IText(acc)
    if only_visible:
      e = comp.getExtents(Constants.DESKTOP_COORDS)
      # get the first item
      first = text.getOffsetAtPoint(e.x, e.y, Constants.DESKTOP_COORDS)
      # get the last item
      last = text.getOffsetAtPoint(e.x+e.width, e.y+e.height, \
                                   Constants.DESKTOP_COORDS)
    else:
      first = 0
      last = text.characterCount-1
    if first == last or first == -1 or last == -1:
      # nit: protection against bad bounds reporting
      first = 0
      last = text.characterCount-1
    return first, last
    
  @pyLinAcc.errorToLookupError
  def getNextItem(self, only_visible=True): 
    '''
    Gets the next item relative to the one indicated by the L{POR}
    providing this interface.

    @param only_visible: True when Item in the returned L{POR} must be visible
    @type only_visible: boolean
    @return: Point of regard for the beginning of the next item in the same 
      accessible
    @rtype: L{POR}
    @raise IndexError: When there is no next item
    @raise LookupError: When lookup for the next item fails even though it may 
      exist
    '''
    acc = self.accessible
    off = self.item_offset
    text = Interfaces.IText(acc)
    # get the first and last visible character offsets
    first, last = self._getVisibleItemExtents(only_visible)
    
    if off is None:
      # use the first line if we're not on an item yet
      return POR(acc, first, 0)
    
    # get the current line start and end offsets
    line, lstart, lend = \
        text.getTextAtOffset(off, Constants.TEXT_BOUNDARY_LINE_START)
    # get the next line
    nlstart = lend
    
    if nlstart < first:
      # return the starting offset of the first visible line
      por = POR(acc, first, 0)
    if nlstart < last:
      # return the starting offset of the next line
      por = POR(acc, nlstart, 0)
    else:
      # last item
      raise IndexError
    return por
    
  @pyLinAcc.errorToLookupError
  def getPrevItem(self, only_visible=True):
    '''
    Gets the previous item relative to the one indicated by the L{POR} 
    providing this interface. 
    
    @param only_visible: True when Item in the returned L{POR} must be visible
    @type only_visible: boolean
    @return: Point of regard for the beginning of the previous item in the same 
      accessible
    @rtype: L{POR}
    @raise IndexError: When there is no previous item
    @raise LookupError: When lookup for the previous item fails even though it
      may exist
    '''
    acc = self.accessible
    off = self.item_offset
    text = Interfaces.IText(acc)
    # get the first and last visible character offsets
    first, last = self._getVisibleItemExtents(only_visible)
    
    if off is None:
      # on the text accessible itself
      raise IndexError
    elif off == 0:
      # return the text area itself; needed to prevent negative indexing
      return POR(acc, None, 0)
         
    # get the current line start and end offsets
    line, lstart, lend = \
        text.getTextAtOffset(off, Constants.TEXT_BOUNDARY_LINE_START)
    # get the prev line
    line, plstart, plend = \
        text.getTextAtOffset(lstart-1, Constants.TEXT_BOUNDARY_LINE_START)
    
    if plstart > last:
      # return the starting index of the last line
      a, last_start, c = \
        text.getTextAtOffset(last, Constants.TEXT_BOUNDARY_LINE_START)
      por = POR(acc, last_start, 0)
    elif plstart >= first:
      # return the starting offset of the previous line
      por = POR(acc, plstart, 0)
    else:
      # return the text area itself
      por = POR(acc, None, 0)    
    return por
    
  @pyLinAcc.errorToLookupError
  def getFirstItem(self, only_visible=True):
    '''
    Gets the first item relative to the one indicated by the L{POR} 
    providing this interface.
    
    @param only_visible: True when Item in the returned L{POR} must be visible
    @type only_visible: boolean
    @return: Point of regard for the beginning of the first item in the same 
      accessible
    @rtype: L{POR}
    @raise LookupError: When lookup for the last item fails even though it may 
      exist
    '''
    acc = self.accessible
    if only_visible:
      first, last = self._getVisibleItemExtents(only_visible)
      return POR(acc, first, 0)
    else:
      return POR(acc, 0, 0)
        
  @pyLinAcc.errorToLookupError
  def getLastItem(self, only_visible=True):
    '''
    Gets the last item relative to the one indicated by the L{POR} 
    providing this interface.
    
    @param only_visible: True when Item in the returned L{POR} must be visible
    @type only_visible: boolean
    @return: Point of regard for the beginning of the last item in the same 
      accessible
    @rtype: L{POR}
    @raise LookupError: When lookup for the last item fails even though it may 
      exist
    '''
    acc = self.accessible
    text = Interfaces.IText(acc)
    first, last = self._getVisibleItemExtents(only_visible)
    # get the starting offset of the last line
    a, last_start, c = \
        text.getTextAtOffset(last-1, Constants.TEXT_BOUNDARY_LINE_START)
    return POR(acc, last, 0)

  @pyLinAcc.errorToLookupError 
  def getFirstAccChild(self):
    '''
    Always raises LookupError. Children are exposed through their parent.
    
    @raise LookupError: Always
    '''
    raise LookupError

  @pyLinAcc.errorToLookupError 
  def getLastAccChild(self):
    '''
    Always raises LookupError. Children are exposed through their parent.
    
    @raise LookupError: Always
    '''
    raise LookupError

  @pyLinAcc.errorToLookupError
  def getChildAcc(self, index):
    '''
    Always raises LookupError. Children are exposed through their parent.
    
    @raise LookupError: Always
    '''
    raise LookupError
  
class SimpleTextEventHandlerAdapter(DefaultEventHandlerAdapter):
  '''  
  Overrides L{DefaultEventHandlerAdapter} to fire L{AEEvent.CaretChange}
  events for caret and text events. Expects the subject to be a
  L{pyLinAcc.Accessible}.
  
  Adapts subject accessibles that provide the Text interface.
  '''
  provides = [IEventHandler]
  
  @staticmethod
  def when(subject):
    '''
    Tests if the given subject can be adapted by this class.
    
    @param subject: Accessible to test
    @type subject: L{pyLinAcc.Accessible}
    @return: True when the subject meets the condition named in the docstring
    for this class, False otherwise
    @rtype: boolean
    '''
    return Interfaces.IText(subject) is not None
  
  def _handleTextEvent(self, event, **kwargs):
    '''
    Called when text is inserted or deleted (object:text-changed:insert & 
    object:text-changed:delete). Creates and returns an L{AEEvent.CaretChange}
    to indicate a change in the caret context.
    
    L{pyLinAcc.Event.Event}.type.minor is "insert" or "delete".
    L{pyLinAcc.Event.Event}.detail1 has start offset of text change.
    L{pyLinAcc.Event.Event}.detail2 has length of text change.
    L{pyLinAcc.Event.Event}.any_data has text inserted/deleted.
    
    @param event: Raw text-changed event
    @type event: L{pyLinAcc.Event.Event}
    @param kwargs: Parameters to be passed to any created L{AEEvent}
    @type kwargs: dictionary
    @return: L{AEEvent.CaretChange}
    @rtype: tuple of L{AEEvent}
    '''
    # get text interface
    text = Interfaces.IText(self.subject)
    cOff = event.detail1
    # get the text of the caret line, it's starting and ending offsets
    line, sOff, eOff = \
      text.getTextAtOffset(cOff, Constants.TEXT_BOUNDARY_LINE_START)
    # create a POR with start offset of the line and relative caret offset
    por = POR(self.subject, sOff, cOff - sOff)

    # create Caret event with current POR, text added/removed, where it changed,
    # and whether it was inserted.
    return (CaretChange(por, unicode(event.any_data, 'utf-8'), 
                        cOff, (event.type.minor == 'insert'),
                        **kwargs),)
  
  def _handleCaretEvent(self, event, **kwargs):
    '''
    Creates and returns an L{AEEvent.CaretChange} indicating the caret moved in
    the subject accessible.
    
    L{pyLinAcc.Event.Event}.detail1 has caret offset.
    
    @param event: Raw caret movement event
    @type event: L{pyLinAcc.Event.Event}
    @param kwargs: Parameters to be passed to any created L{AEEvent}
    @type kwargs: dictionary
    @return: L{AEEvent.CaretChange}
    @rtype: tuple of L{AEEvent}
    '''
    # get text interface
    text = Interfaces.IText(self.subject)
    # detail1 has new caret position
    caret_offset = event.detail1
    
    # 2007-05-14
    # perform a special check account for end of line differences across
    # implementations; if the offset is greater than the character count
    # and the last character is not a newline, correct by going back one 
    # character offset and asking for the line; done to correct problems in
    # single line entry fields in FF
    qo = caret_offset
    if (caret_offset > 0 and 
        caret_offset >= text.characterCount and
        text.getText(caret_offset-1, -1) != '\n'):
      qo -= 1
      
    # get the text of the new caret line, its starting and ending offsets
    line, sOff, eOff = \
      text.getTextAtOffset(qo, Constants.TEXT_BOUNDARY_LINE_START)

    # create a POR with start offset of the line and relative caret offset
    por = POR(self.subject, sOff, caret_offset - sOff)

    # create Caret event with new POR, line at caret, and offset of caret
    return (CaretChange(por, unicode(line, 'utf-8'), caret_offset,
                        **kwargs),)
  
  def _handleTextSelectionEvent(self, event, **kwargs):
    '''
    Creates and returns a L{AEEvent.SelectorChange} event when text selection
    changes.
    
    @param event: Raw caret movement event
    @type event: L{pyLinAcc.Event.Event}
    @param kwargs: Parameters to be passed to any created L{AEEvent}
    @type kwargs: dictionary
    @return: L{AEEvent.SelectorChange}
    @rtype: tuple of L{AEEvent}
    '''
    # get text interface
    text = Interfaces.IText(self.subject)
    # get selections
    n = text.getNSelections()
    # create a POR with start offset of the line and relative caret offset
    line, start, end = \
      text.getTextAtOffset(text.caretOffset,Constants.TEXT_BOUNDARY_LINE_START)    
    por = POR(self.subject, start, text.caretOffset - start)
    
    if n == 0:
      # fire an event with no text if there is no selection
      return (SelectorChange(por, u'', AEConstants.EVENT_CHANGE_TEXT_SELECT,
                             **kwargs),)
    else:
      # we don't know which selection changed, so we'll just assume the first
      # one for the time being; multi-select editors are rare
      sel = text.getText(*text.getSelection(0))
      return (SelectorChange(por, unicode(sel, 'utf-8'), 
                             AEConstants.EVENT_CHANGE_TEXT_SELECT, **kwargs),)
    
  def _handleFocusEvent(self, event, **kwargs):
    '''
    Creates an L{AEEvent.FocusChange} indicating that the accessible being
    adapted has gained the focus. Also a L{AEEvent.CaretChange}. This sequence 
    of L{AEEvent}s will be posted by the caller.
     
    @param event: Raw focus change event
    @type event: L{pyLinAcc.Event.Event}
    @param kwargs: Parameters to be passed to any created L{AEEvent}
    @type kwargs: dictionary
    @return: L{AEEvent.FocusChange} and L{AEEvent.CaretChange}
    @rtype: tuple of L{AEEvent}
    '''
    try:
      text = Interfaces.IEditableText(self.subject)
    except NotImplementedError:
      # only fire caret move when the text is editable, else fire selector
      return super(SimpleTextEventHandlerAdapter,
                   self)._handleFocusEvent(event, **kwargs)

    kwargs['focused'] = True
    # get the text of the caret line, it's starting and ending offsets
    line = text.getText(0, -1)
    # create a POR with start offset of the line and relative caret offset
    por = POR(self.subject, 0, 0)
    char_offset = text.caretOffset
    
    # GTK and Gecko (Firefox) toolkits handle caret events differently.
    # GTK fails to produce a caret event while Gecko produces one for you.
    # This code branches on the toolkit type and returns the required events
    # for each toolkit type.
    if self.subject.getApplication().toolkitName == 'Gecko':
      return (FocusChange(por, True, **kwargs),)
    else:
      # create a focus event for this POR
      # and add a caret event to indicate the current position
      return (FocusChange(por, True, **kwargs),    
              CaretChange(por, unicode(line, 'utf-8'), char_offset, **kwargs))

class TextEventHandlerAdapter(SimpleTextEventHandlerAdapter):
  '''  
  Overrides L{SimpleTextEventHandlerAdapter} to fire L{AEEvent.CaretChange}
  events on focus changes for text controls. Expects the subject to be a
  L{pyLinAcc.Accessible}.
  
  Adapts subject accessibles that have a role of terminal or have a state of 
  multiline, single line, or editable and provide the Text
  interface. Does not adapt accessibles with role of page tab.
  '''
  provides = [IEventHandler]
  
  @staticmethod
  def when(subject):
    '''
    Tests if the given subject can be adapted by this class.
    
    @param subject: Accessible to test
    @type subject: L{pyLinAcc.Accessible}
    @return: True when the subject meets the condition named in the docstring
      for this class, False otherwise
    @rtype: boolean
    '''
    return _multiLineCondition(subject)
      
  def _handleFocusEvent(self, event, **kwargs):
    '''
    Creates an L{AEEvent.FocusChange} indicating that the accessible being
    adapted has gained the focus. Also a L{AEEvent.CaretChange}. This sequence 
    of L{AEEvent}s will be posted by the caller.
     
    @param event: Raw focus change event
    @type event: L{pyLinAcc.Event.Event}
    @param kwargs: Parameters to be passed to any created L{AEEvent}
    @type kwargs: dictionary
    @return: L{AEEvent.FocusChange} and L{AEEvent.CaretChange}
    @rtype: tuple of L{AEEvent}
    '''
    kwargs['focused'] = True
    text = Interfaces.IText(self.subject)
    char_offset = text.caretOffset
    # get the text of the caret line, it's starting and ending offsets
    line, sOff, eOff = \
      text.getTextAtOffset(char_offset, Constants.TEXT_BOUNDARY_LINE_START)
    # create a POR with start offset of the line and relative caret offset
    por = POR(self.subject, sOff, char_offset - sOff)
    
    # create a focus event for this POR
    # and add a caret event to indicate the current position
    return (FocusChange(por, True, **kwargs),    
            CaretChange(por, unicode(line, 'utf-8'), char_offset-sOff,
                        **kwargs))
