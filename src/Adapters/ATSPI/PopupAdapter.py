'''
Defines an L{AccAdapt.Adapter}s for the L{AEInterfaces.IAccessibleInfo} 
interface to correct for the problem of pop-up items (e.g menus) always having
states visible and showing after having been activated once. 

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''

from DefaultInfo import *
from DefaultEventHandler import *
from AEInterfaces import *
from pyLinAcc import Constants, Interfaces
import pyLinAcc

class MenuPopupAccInfoAdapter(DefaultAccInfoAdapter):
  '''
  Overrides L{DefaultNavAdapter} to provide correct visibility information about
  menus and menu items. Expects the subject to be a L{POR}.
  
  Adapts accessibles that have the string "menu" in their role names 
  (unlocalized) and a parent with ROLE_MENU.
  '''
  provides = [IAccessibleInfo]
  
  @staticmethod
  def when(subject):
    '''
    Tests if the given subject can be adapted by this class.
    
    @param subject: L{POR} containing an accessible to test
    @type subject: L{POR}
    @return: True when the subject meets the condition named in the docstring
    for this class, False otherwise
    @rtype: boolean
    '''
    acc = subject.accessible
    pr = acc.parent.getRole()
    c = Constants
    return (pr == c.ROLE_MENU and acc.getRoleName().find('menu') > -1)
  
  @pyLinAcc.errorToLookupError
  def isAccVisible(self):
    '''
    Gets if a menu item (item, check item, submenu, etc.) is visible by testing
    if its parent menu is selected.
    
    @return: Does the accessible consider itself visible?
    @rtype: boolean
    @raise LookupError: When the accessible object is dead
    '''
    ss = self.accessible.parent.getState()
    return ss.contains(Constants.STATE_SELECTED) or \
           ss.contains(Constants.STATE_EXPANDED)

class PopupMenuEventHandlerAdapter(DefaultEventHandlerAdapter):
  '''
  Overrides L{DefaultEventHandlerAdapter} to avoid generating focus events on
  selection. Expects the subject to be a raw L{pyLinAcc.Accessible}.
  
  Adapts accessibles with ROLE_MENU.
  '''  
  provides = [IEventHandler]

  @staticmethod
  def when(subject):
    '''
    Tests if the given subject can be adapted by this class.
    
    @param subject: L{POR} containing an accessible to test
    @type subject: L{POR}
    @return: True when the subject meets the condition named in the docstring
    for this class, False otherwise
    @rtype: boolean
    '''
    c = Constants
    return subject.getRole() == c.ROLE_MENU

  def _handleFocusEvent(self, event, **kwargs):
    '''
    Creates an L{AEEvent.FocusChange} indicating that the accessible being
    adapted has gained the focus. Also creates a L{AEEvent.SelectorChange}.
    These two L{AEEvent}s will be posted by the caller.
    
    @param event: Raw focus change event
    @type event: L{pyLinAcc.Event.Event}
    @param kwargs: Parameters to be passed to any created L{AEEvent}
    @type kwargs: dictionary
    @return: L{AEEvent.FocusChange} and L{AEEvent.SelectorChange}
    @rtype: tuple of L{AEEvent}
    '''
    kwargs['focused'] = True
    try:
      sel = Interfaces.ISelection(self.subject)
      count = sel.nSelectedChildren
    except NotImplementedError:
      count = 0
    if count == 0:
      por = POR(self.subject, None, 0) 
      # adapt the accessible to an adapter which provides an IAccesssibleInfo
      # interface and get the accessible's item text
      item = IAccessibleInfo(por).getAccItemText()
      return (FocusChange(por, True, **kwargs), 
              SelectorChange(por, item, **kwargs))