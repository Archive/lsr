'''
Defines L{AccAdapt.Adapter}s for AT-SPI table accessibles. Tables implement the
Table interface but not the Selection interface.

@author: Pete Brunet
@author: Peter Parente
@author: Eirikur Hallgrimsson
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
from POR import POR
from AEEvent import *
from AEInterfaces import *
from DefaultEventHandler import *
from DefaultNav import *
from ContainerAdapter import *
from pyLinAcc import Constants, Interfaces
import pyLinAcc

FUDGE_PX = 5

class TableAccInfoAdapter(ContainerAccInfoAdapter):
  '''
  Overrides L{ContainerAccInfoAdapter} to generate selector events on focus
  and on selection. Expects the subject to be a L{pyLinAcc.Accessible}.
  
  Adapts subject accessibles that provide the L{pyLinAcc.Interfaces.ITable} 
  interface and have ROLE_TABLE.
  '''  
  provides = [IAccessibleInfo]

  @staticmethod
  def when(por):
    '''
    Tests if the given POR can be adapted by this class.
    
    @param por: Accessible to test
    @type por: L{POR}
    @return: True when the subject meets the condition named in the docstring
    for this class, False otherwise
    @rtype: boolean
    '''
    acc = por.accessible
    r = acc.getRole()
    # make sure the role is a table
    if r != Constants.ROLE_TABLE:
      return False
    # make sure the table interface exists
    tab = Interfaces.ITable(acc)
    return True

  @pyLinAcc.errorToLookupError 
  def getAccRow(self):
    '''
    Gets the row of an item in a table.
    
    @return: Zero indexed row of the item
    @rtype: integer
    @raise LookupError: When the table or item is no longer valid
    '''
    if self.item_offset is None:
      return None
    tab = Interfaces.ITable(self.accessible)
    return tab.getRowAtIndex(self.item_offset)
      
  @pyLinAcc.errorToLookupError 
  def getAccColumn(self):
    '''
    Gets the column of an item in a table.
    
    @return: Zero indexed column of the item
    @rtype: integer
    @raise LookupError: When the table or item is no longer valid
    '''
    if self.item_offset is None:
      return None
    tab = Interfaces.ITable(self.accessible)
    return tab.getColumnAtIndex(self.item_offset)

  @pyLinAcc.errorToLookupError
  def getAccRowColIndex(self, row, col):
    '''
    Gets the 1D index of the cell at the given 2D row and column.
    
    @param row: Row index
    @type row: integer
    @param col: Column index
    @type col: integer
    @return: 1D index into the table
    @rtype: integer
    @raise IndexError: When the row/column offsets are invalid
    @raise LookupError: When the table is no longer valid
    '''
    tab = Interfaces.ITable(self.accessible)
    i = tab.getIndexAt(row, col)
    if i < 0:
      raise IndexError
    return i

  @pyLinAcc.errorToLookupError 
  def getAccRowHeader(self):
    '''
    Gets the text description of a row in a table.

    @return: The descriptive text.
    @rtype: string
    @raise LookupError: When the table or item is no longer valid
    '''
    tab = Interfaces.ITable(self.accessible)
    if self.item_offset is not None:
      row = tab.getRowAtIndex(self.item_offset)
      return tab.getRowDescription(row)
    return None
    
  @pyLinAcc.errorToLookupError 
  def getAccColumnHeader(self):
    '''
    Gets the text description of a column in a table.

    @return: The descriptive text.
    @rtype: string
    @raise LookupError: When the table or item is no longer valid
    '''
    tab = Interfaces.ITable(self.accessible)
    if self.item_offset is not None:
      col = tab.getColumnAtIndex(self.item_offset)
      return tab.getColumnDescription(col)
    return None

  @pyLinAcc.errorToLookupError
  def getAccTableExtents(self):
    '''
    Returns the number of rows and columns in the table.

    @return: Count of rows and columns
    @rtype: 2-tuple of integer
    @raise LookupError: When the table is no longer valid
    '''
    tab = Interfaces.ITable(self.accessible)
    return (tab.nRows, tab.nColumns)
  
class TableNavAdapter(DefaultNavAdapter):
  '''
  Overrides L{DefaultNavAdapter} to provide navigation over table cells as 
  items. Expects the subject to be a L{POR}. Does not walk headers. 
  Those can be gotten and reported separately as context information.
  
  Note that not all tables properly respond to requests for accessibles at 
  (x,y) coordinates on the screen. Most tables seem to always return their
  first accessible (not first visible accessible) for the top left corner and
  last accessible (not last visible accessible) for the bottom right corner, but
  this depends on the application.
  
  Adapts subject accessibles that provide the L{pyLinAcc.Interfaces.ITable} 
  interface.
  '''
  provides = [IAccessibleNav, IItemNav]

  @staticmethod
  def when(subject):
    '''
    Tests if the given subject can be adapted by this class.
    
    @param subject: L{POR} containing an accessible to test
    @type subject: L{POR}
    @return: True when the subject meets the condition named in the docstring
      for this class, False otherwise
    @rtype: boolean
    '''
    acc = subject.accessible
    ss = acc.getState()
    if not ss.contains(Constants.STATE_MANAGES_DESCENDANTS):
      return False
    return Interfaces.ITable(acc)
  
  @pyLinAcc.errorToLookupError
  def _getVisibleItemExtents(self, only_visible):
    '''
    Gets the item offsets of the first and last items in a table of cells.
    
    @param only_visible: Only consider the first and last cells visible in
      the table (True) or the absolute first and last cells (False)?
    @type only_visible: boolean
    @return: First and last item offsets
    @rtype: 2-tuple of integer
    @raise LookupError: When the first or last item or parent accessible is
      not available
    '''
    acc = self.accessible
    if only_visible:
      comp = Interfaces.IComponent(acc)
      e = comp.getExtents(Constants.WINDOW_COORDS)
      # get the first item
      x, y = e.x+FUDGE_PX, e.y+FUDGE_PX
      try:
        first = comp.getAccessibleAtPoint(x, y, Constants.WINDOW_COORDS)
      except TypeError:
        first = None
      # get the last item
      x, y = e.x+e.width-FUDGE_PX, e.y+e.height-FUDGE_PX
      try:
        last = comp.getAccessibleAtPoint(x, y, Constants.WINDOW_COORDS)
      except TypeError:
        last = None
    else:
      first = None
      last = None
    # compute indices
    if first:
      i = first.getIndexInParent()
    else:
      i = 0
    if last:
      j = last.getIndexInParent()
    else:
      t = Interfaces.ITable(acc)
      j = t.getIndexAt(t.nRows-1, t.nColumns-1)
    return i, j
  
  @pyLinAcc.errorToLookupError
  def getNextItem(self, only_visible=True): 
    '''
    Gets the next item relative to the one indicated by the L{POR}
    providing this interface.

    @param only_visible: True when Item in the returned L{POR} must be visible
    @type only_visible: boolean
    @return: Point of regard to the next item in the same accessible
    @rtype: L{POR}
    @raise IndexError: When there is no next item
    @raise LookupError: When lookup for the next item fails even though it may 
      exist
    '''
    acc = self.accessible
    off = self.item_offset
    # get the first and last indices
    i, j = self._getVisibleItemExtents(only_visible)
    if off is None or off < i:
      # return the first visible, non-header item
      if not IAccessibleInfo(POR(acc.getChildAtIndex(i))).isAccVisible():
        raise IndexError
      return POR(acc, i, 0)
    elif off+1 >= i and off+1 <= j:
      if IAccessibleInfo(POR(acc.getChildAtIndex(off+1))).isAccVisible():
        # use the next higher index if it was found to be visible
        return POR(acc, off+1, 0)
      else:
        # wrap to the first visible item in the next row
        # compute the row and column offsets
        t = Interfaces.ITable(acc)
        r = t.getRowAtIndex(off)
        r += 1
        c = t.getColumnAtIndex(i)
        n_off = t.getIndexAt(r, c)
        if not IAccessibleInfo(POR(acc.getChildAtIndex(n_off))).isAccVisible():
          raise IndexError
        return POR(acc, n_off, 0)
    else:
      # no more visible items
      raise IndexError
    
  @pyLinAcc.errorToLookupError
  def getPrevItem(self, only_visible=True):
    '''
    Gets the previous item relative to the one indicated by the L{POR} providing
    this interface. 
    
    @param only_visible: True when Item in the returned L{POR} must be visible
    @type only_visible: boolean
    @return: Point of regard to the previous item in the same accessible
    @rtype: L{POR}
    @raise IndexError: When there is no previous item
    @raise LookupError: When lookup for the previous item fails even though it
      may exist
    '''
    acc = self.accessible
    off = self.item_offset
    comp = Interfaces.IComponent(acc)
    # get the first and last indices
    i, j = self._getVisibleItemExtents(only_visible)
    # and check if the first item is visible since it might be a header
    if off is None:
      # no more visible items
      raise IndexError
    elif off > j:
      # return the last visible item
      return POR(acc, j, 0)
    elif off-1 >= i and off-1 <= j:
      # compute the row and column offsets
      if IAccessibleInfo(POR(acc.getChildAtIndex(off-1))).isAccVisible():
        # use the next higher index if it is visible
        return POR(acc, off-1, 0)
      else:
        # wrap to the first visible item in the next row
        # compute the row and column offsets
        t = Interfaces.ITable(acc)
        r, c = t.getRowAtIndex(off), t.getColumnAtIndex(j)
        r -= 1
        n_off = t.getIndexAt(r, c)
        if n_off <= i:
          raise IndexError
        return POR(acc, n_off, 0)   
    else:
      # return the table iteself
      return POR(acc, None, 0)
    
  @pyLinAcc.errorToLookupError
  def getLastItem(self, only_visible=True):
    '''
    Gets the last item relative to the one indicated by the L{POR} 
    providing this interface.
    
    @param only_visible: True when Item in the returned L{POR} must be visible
    @type only_visible: boolean
    @return: Point of regard to the last item in the same accessible
    @rtype: L{POR}
    @raise LookupError: When lookup for the last item fails even though it may 
      exist
    '''
    acc = self.accessible
    comp = Interfaces.IComponent(acc)
    # try getting the last item by index first
    child = acc.getChildAtIndex(acc.childCount-1)
    if IAccessibleInfo(POR(child)).isAccVisible() or not only_visible:
      return POR(acc, acc.childCount-1, 0)
    # use coords to get the last visible item
    i, j = self._getVisibleItemExtents(only_visible)
    return POR(acc, j, 0)
    
  @pyLinAcc.errorToLookupError
  def getFirstItem(self, only_visible=True):
    '''
    Gets the first item relative to the one indicated by the L{POR} 
    providing this interface.
    
    @param only_visible: True when Item in the returned L{POR} must be visible
    @type only_visible: boolean
    @return: Point of regard to the last item in the same accessible
    @rtype: L{POR}
    @raise LookupError: When lookup for the last item fails even though it may 
      exist
    '''
    acc = self.accessible
    comp = Interfaces.IComponent(acc)
    # try getting the first item by index first
    child = acc.getChildAtIndex(0)
    if IAccessibleInfo(POR(child)).isAccVisible() or not only_visible:
      return POR(acc, 0, 0)
    # use coords to get the first visible item
    i, j = self._getVisibleItemExtents(only_visible)
    return POR(acc, i, 0)
  
  @pyLinAcc.errorToLookupError 
  def getFirstAccChild(self):
    '''
    Always raises LookupError. Tables have items but no children.
    
    @raise LookupError: Always
    '''
    raise LookupError

  @pyLinAcc.errorToLookupError 
  def getLastAccChild(self):
    '''
    Always raises LookupError. Tables have items but no children.
    
    @raise LookupError: Always
    '''
    raise LookupError

  @pyLinAcc.errorToLookupError
  def getChildAcc(self, index):
    '''
    Always raises LookupError. Tables have items but no children.
    
    @raise LookupError: Always
    '''
    raise LookupError
 
class DegenerateTableEventHandlerAdapter(DefaultEventHandlerAdapter):
  '''  
  Overrides L{DefaultEventHandlerAdapter} to generate selector events on
  selection change. Does not generate the ideal selector events on focus
  because the degenerate subject does not implement the Selection interface. As
  a result, the active descendant cannot be determined. Expects the subject to
  be a raw L{pyLinAcc.Accessible}.
  
  Adapts subject accessibles that provide the L{pyLinAcc.Interfaces.ITable}
  interface.
  ''' 
  @staticmethod
  def when(subject):
    '''
    Tests if the given subject can be adapted by this class.
    
    @param subject: Accessible to test
    @type subject: L{pyLinAcc.Accessible}
    @return: True when the subject meets the condition named in the docstring
    for this class, False otherwise
    @rtype: boolean
    '''
    return Interfaces.ITable(subject)
  
  def _handleDescendantEvent(self, event, **kwargs):
    '''
    Creates an L{AEEvent.SelectorChange} indicating the "selector" moved in this
    accessible.
    
    @param event: Raw decendent changed event
    @type event: L{pyLinAcc.Event.Event}
    @param kwargs: Parameters to be passed to any created L{AEEvent}
    @type kwargs: dictionary
    @return: L{AEEvent.SelectorChange}
    @rtype: tuple of L{AEEvent} 
    '''
    return (self._getSelectorEvent(event.any_data, event.detail1, **kwargs),)
  
  def _getSelectorEvent(self, accessible, item_offset, **kwargs):
    '''
    Creates an L{AEEvent.SelectorChange} indicating the selector moved in this
    accessible. 

    This method corrects for the possibility that the selected item actually
    have children that have the important information which are themselves not
    selected but returned as children of the even source. Right now, the last
    child in such a case appears to carry the information. More robust
    processing may be needed in the future.
    
    @param accessible: Accessible that generated this event
    @type accessible: L{pyLinAcc.Accessible}
    @param item_offset: Offset of item involved in the selection event
    @type item_offset: integer
    @param kwargs: Parameters to be passed to any created L{AEEvent}
    @type kwargs: dictionary
    @return: Selection event 
    @rtype: L{AEEvent.SelectorChange}
    '''
    if accessible.childCount > 0:
      accessible = accessible.getChildAtIndex(accessible.childCount - 1)
    
    # Notes:
    #   When adding support for more than one selection, we'll probably have
    #   to keep track of each state change (SELECTED or not) and report the most
    #   recently changed item or pair of items.  Some examples:
    #   - a single item became selected or unselected
    #   - one item became unselected and an adjacent one became selected
    #   In the second case the perk should probably receive both items
    #   The Selection interface has a list of one or more selected items.
    #   selection.nSelectedChildren indicates how many there are and 
    #   selection.getSelectedChild(index) accesses one of them.


    # create a POR, pass it and the item text at the POR to the Tier
    por = POR(self.subject, item_offset, 0)
    acc_child_text = IAccessibleInfo(por).getAccItemText()
    return SelectorChange(por, acc_child_text, **kwargs)
  
class TableEventHandlerAdapter(DegenerateTableEventHandlerAdapter):
  '''  
  Overrides L{DegenerateTableEventHandlerAdapter} to generate selector events 
  on focus and on selection. Expects the subject to be a raw
  L{pyLinAcc.Accessible}.
  
  Adapts subject accessibles that provide the L{pyLinAcc.Interfaces.ISelection},
  interface and have ROLE_TABLE or ROLE_TREE_TABLE.
  '''
  provides = [IEventHandler]

  @staticmethod
  def when(subject):
    '''
    Tests if the given subject can be adapted by this class.
    
    @param subject: Accessible to test
    @type subject: L{pyLinAcc.Accessible}
    @return: True when the subject meets the condition named in the docstring
      for this class, False otherwise
    @rtype: boolean
    '''
    r = subject.getRole()
    c = Constants
    return (r in (c.ROLE_TABLE, c.ROLE_TREE_TABLE) and 
            Interfaces.ISelection(subject))
  
  #def _handleDescendantEvent(self, event, **kwargs):
    #'''
    #Creates an L{AEEvent.SelectorChange} indicating the "selector" moved in this
    #accessible.
    
    #@param event: Raw decendent changed event
    #@type event: L{pyLinAcc.Event.Event}
    #@param kwargs: Parameters to be passed to any created L{AEEvent}
    #@type kwargs: dictionary
    #@return: L{AEEvent.SelectorChange}
    #@rtype: tuple of L{AEEvent} 
    #'''
    ## clear the seen descendant state for this POR
    #try:
      #del self.selection_state[por]
    #except KeyError:
      #pass
    #parent = super(TableEventHandlerAdapter, self)
    #return parent._handleDescendantEvent(event, **kwargs)    
  
  #def _handleSelectionChangedEvent(self, event, **kwargs):
    #'''
    #Creates an L{AEEvent.SelectorChange} indicating an item was added to or 
    #removed from a selection.
    
    #@param event: Raw selection changed event
    #@type event: L{pyLinAcc.Event.Event}
    #@param kwargs: Parameters to be passed to any created L{AEEvent}
    #@type kwargs: dictionary
    #@return: L{AEEvent.SelectorChange}
    #@rtype: tuple of L{AEEvent} 
    #'''
    #if event.type.minor == 'add':
      #return (self._getSelectorEvent(event.any_data, event.detail1, **kwargs),)
    #else:
      #return (self._getSelectorEvent(event.any_data, event.detail1, **kwargs),)

  def _handleFocusEvent(self, event, **kwargs):
    '''
    Creates an L{AEEvent.FocusChange} indicating that the accessible being
    adapted has gained the focus. Also creates a L{AEEvent.SelectorChange}.
    These two L{AEEvent}s will be posted by the caller.
    
    @param event: Raw focus change event
    @type event: L{pyLinAcc.Event.Event}
    @param kwargs: Parameters to be passed to any created L{AEEvent}
    @type kwargs: dictionary
    @return: L{AEEvent.FocusChange} and L{AEEvent.SelectorChange}
    @rtype: tuple of L{AEEvent}
    '''
    # build a focus event
    kwargs['focused'] = True
    por = POR(self.subject, None, 0)
    focus_event = FocusChange(por, True, **kwargs)

    # get the selection interface
    selection = Interfaces.ISelection(self.subject)
    # ignore selector events when nothing is selected
    if selection.nSelectedChildren == 0:
      return (focus_event,)
    # get the selected object, for now, just the first item in the selection
    acc_child = selection.getSelectedChild(0)
    if acc_child is None:
      return (focus_event,)
    item_offset = acc_child.getIndexInParent()
    # build and return a selector event
    return focus_event, self._getSelectorEvent(acc_child, item_offset,**kwargs)
