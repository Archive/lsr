'''
Defines L{AccAdapt.Adapter}s for AT-SPI hypertext accessibles potentially
containing embedded objects.

@var EMBED_CHAR: Unicode embed character u'\u0xfffc'
@type EMBED_CHAR: unicode
@var EMBED_VAL: Unicode embed character value 0xfffc
@type EMBED_VAL: integer
@var embed_rex: Compiled regular expression for finding embed characters
@type embed_rex: _sre.SRE_Pattern

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
import re
import pyLinAcc
from DefaultNav import *
from DefaultInfo import *
from TextAdapter import *
from AEInterfaces import *
from pyLinAcc import Interfaces, Constants

EMBED_CHAR = u'\ufffc'
EMBED_VAL = 0xfffc
embed_rex = re.compile(EMBED_CHAR)

def _getPrevItem(acc, char):
  '''
  Gets the offset of the start of the previous item. The first of the following
  rules satisfied working backward from the offset in char defines the start 
  of the previous item:

  1) the previous embed
  2) the beginning of a wrapped line
  3) the character one greater than the previous embed on the previous line

  @param acc: Accessible object supporting embed characters
  @type acc: L{pyLinAcc.Accessible}
  @param char: Starting offset relative to the start of all text in the object
  @type char: integer
  @return: Index of the start of the previous item relative to the start of all 
    text in the object
  @rtype: integer
  @raise IndexError: When no previous item is available in this object
  '''
  if char < 0: char = 0
  it = Interfaces.IText(acc)
  # get the current line start and end offsets
  text, lstart, lend = \
      it.getTextAtOffset(char, Constants.TEXT_BOUNDARY_LINE_START)
  # if we're at the beginning of a line, use the previous line instead
  if lstart == char:
    text, lstart, lend = \
      it.getTextAtOffset(char-1, Constants.TEXT_BOUNDARY_LINE_START)
  text = unicode(text, 'utf-8')
  # compute the item offset relative to the start of this line
  rio = char-lstart
  # seek backward from the starting offset
  index = text.rfind(EMBED_CHAR, 0, rio)
  if index < 0:
    if rio <= 0:
      # beginning of text
      raise IndexError
    else:
      # start of line
      return lstart
  elif (rio-index) > 1:
    # if we didn't move back just one character, there's some more text on 
    # the previous line that should count as its own chunk, so move ahead one
    # again
    return lstart+index+1
  else:
    # return the offset of the embed
    return lstart+index

def _getNextEmbedCharOffset(acc, char):
  '''
  Gets the offset of the start of the next item. The first of the following
  rules satisfied working forward from the offset in char defines the start 
  of the next item:

  1) the next embed
  2) the beginning of a wrapped line

  @param acc: Accessible object supporting embed characters
  @type acc: L{pyLinAcc.Accessible}
  @param char: Starting offset relative to the start of all text in the object
  @type char: integer
  @return: Index of the start of the next item relative to the start of all 
    text in the object
  @rtype: integer
  @raise IndexError: When no next item is available in this object
  '''
  if char < 0: char = 0
  it = Interfaces.IText(acc)
  # get the current line start and end offsets
  text, lstart, lend = \
      it.getTextAtOffset(char, Constants.TEXT_BOUNDARY_LINE_START)
  text = unicode(text, 'utf-8')
  # compute the item offset relative to the start of this line
  rio = char-lstart
  # seek forward from the starting offset
  index = text.find(EMBED_CHAR, rio)
  if index < 0:
    if lend >= it.characterCount-1:
      # end of text
      raise IndexError
    else:
      # end of line
      return lend
  return lstart+index

def _getEmbedCharOffset(acc, index):
  '''
  Gets the character offset of the embedded character at the given index.
  
  @param acc: Accessible which should be searched for embed characters
  @type acc: L{pyLinAcc.Accessible}
  @param index: Index of the embedded character in the count of all embedded
    characters
  @type index: integer
  @return: Offset in characters of the embedded character
  @rtype: integer
  @raise IndexError: When the given index is outside the bounds of the number
    of embed characters in the text
  '''
  if index is None:
    # no embedded characters at None index
    raise IndexError
  elif index < 0:
    # no embedded characters, return start index
    return 0
  # IHypertext(acc).getLink(index) returns the Hyperlink object associated with
  # this index
  hlink = Interfaces.IHypertext(acc).getLink(index)
  # returns the starting offset of the Hyperlink within the Hypertext object
  return hlink.startIndex

  #htext.getLinkIndex(self.item_offset)
  #link = htext.getLink(i-1)
  #if link is None:
  #return POR(acc, 0, 0)
  #else:
  #return POR(acc, link.endOffset, 0)

class HypertextNavAdapter(DefaultNavAdapter):
  '''
  Overrides L{DefaultNavAdapter} to provide navigation over hypertext embedded
  objects as items and children. Expects the subject to be a L{POR}.
  
  Adapts subject accessibles that provide the L{pyLinAcc.Interfaces.IHypertext} 
  and L{pyLinAcc.Interfaces.IText} interfaces.
  '''
  provides = [IAccessibleNav, IItemNav]

  @staticmethod
  def when(subject):
    '''
    Tests if the given subject can be adapted by this class.
    
    @param subject: L{POR} containing an accessible to test
    @type subject: L{POR}
    @return: True when the subject meets the condition named in the docstring
      for this class, False otherwise
    @rtype: boolean
    '''
    acc = subject.accessible
    Interfaces.IHypertext(acc)
    Interfaces.IText(acc)
    return True
  
  @pyLinAcc.errorToLookupError
  def getNextItem(self, only_visible=True): 
    '''
    Gets the next item relative to the one indicated by the L{POR} providing
    this interface.
    
    Currently ignores only_visible.
    
    @param only_visible: True when Item in the returned L{POR} must be visible
    @type only_visible: boolean
    @return: Point of regard to the next item in the same accessible
    @rtype: L{POR}
    @raise IndexError: When there is no next item
    @raise LookupError: When lookup for the next item fails even though it may 
      exist
    '''
    # get local references to accessible, link offset, etc.
    acc = self.accessible
    if self.item_offset is None:
      char = 0
    else:
      char = self.item_offset + 1
    text = Interfaces.IText(acc)
    htext = Interfaces.IHypertext(acc)
    count = text.characterCount
    # look one character ahead
    if char >= count:
      # if it does not exist, raise IndexError
      raise IndexError
    elif char != 0 and text.getCharacterAtOffset(char-1) != EMBED_VAL:
      # if we're currently on text, not an item, find the next embed
      char = _getNextEmbedCharOffset(acc, char-1)
      # continue with the remaining if statements using the new char
    if text.getCharacterAtOffset(char) == EMBED_VAL:
      # if it's the embed character, return the POR of the embed
      index = htext.getLinkIndex(char)
      if index < 0:
        # raise an error if there is a next embed, but it's not fetchable
        raise LookupError
      return  POR(acc.getChildAtIndex(index), None, 0)
      #return IAccessibleNav(por).getFirstItem(only_visible)
    else:
      # else, return the POR for the next chunk of text
      return POR(acc, char, 0)
  
  @pyLinAcc.errorToLookupError
  def getPrevItem(self, only_visible=True):
    ''''    
    Gets the previous item relative to the one indicated by the L{POR}
    providing this interface.
    
    Currently ignores only_visible.
    
    @param only_visible: True when Item in the returned L{POR} must be visible
    @type only_visible: boolean
    @return: Point of regard to the previous item in the same accessible
    @rtype: L{POR}
    @raise IndexError: When there is no previous item
    @raise LookupError: When lookup for the previous item fails even though it
      may exist
    '''
    # get local references to accessible, link offset, ...
    acc = self.accessible
    if self.item_offset is None:
      raise IndexError
      #char = 0
    else:
      char = self.item_offset - 1
    text = Interfaces.IText(acc)
    htext = Interfaces.IHypertext(acc)
    # look one character back
    if char < 0:
      # if it does not exist, return the item offset as None
      return POR(acc, None, 0)
    elif text.getCharacterAtOffset(char+1) != EMBED_VAL:
      # if we're currently on text, not an item, find the previous embed
      char = _getPrevItem(acc, char+1)
      # continue with the remaining if statements using the new char
      
    if text.getCharacterAtOffset(char) == EMBED_VAL:
      # if it's the embed character, return the POR of the embed
      index = htext.getLinkIndex(char)
      if index < 0:
        # raise an error if there is a previous embed, but it's not fetchable
        raise LookupError
      por = POR(acc.getChildAtIndex(index), None, 0)
      li = IItemNav(por).getLastItem(only_visible)
      return li
    else:
      # else, return the POR for the previous chunk of text
      por = POR(acc, char, 0)
      return por

  @pyLinAcc.errorToLookupError
  def getLastItem(self, only_visible=True):
    '''
    Gets the last item relative to the one indicated by the L{POR} 
    providing this interface.
    
    Currently ignores only_visible.
    
    @param only_visible: True when Item in the returned L{POR} must be visible
    @type only_visible: boolean
    @return: Point of regard to the last item in the same accessible
    @rtype: L{POR}
    @raise LookupError: When lookup for the last item fails even though it may 
      exist
    '''
    acc = self.accessible
    text = Interfaces.IText(acc)
    htext = Interfaces.IHypertext(acc)
    if text.getCharacterAtOffset(text.characterCount-1) == EMBED_VAL:
      # return the POR of the last embed
      por = POR(acc.getChildAtIndex(acc.childCount-1), None, 0)
      return IItemNav(por).getLastItem(only_visible)
    else:
      # get the previous embed + 1
      char = _getPrevItem(acc, text.characterCount-1)
      if text.getCharacterAtOffset(char) == EMBED_VAL:
        # move ahead one from the located embed
        return POR(acc, char+1, 0)
      else:
        # just use the given POR
        return POR(acc, char, 0)

  @pyLinAcc.errorToLookupError
  def getFirstItem(self, only_visible=True):
    '''
    Gets the first item relative to the one indicated by the L{POR} 
    providing this interface.
    
    Currently ignores only_visible.
    
    @param only_visible: True when Item in the returned L{POR} must be visible
    @type only_visible: boolean
    @return: Point of regard to the first item in the same accessible
    @rtype: L{POR}
    @raise LookupError: When lookup for the first item fails even though it may 
      exist
    '''
    # don't recurse into embeds, let a Perk handle that if it doesn't want to
    # deal with item_offset=None blankness
    return POR(self.accessible, None, 0)
    
  @pyLinAcc.errorToLookupError 
  def getAccAsItem(self, por):
    '''
    Converts the L{POR} to a child accessible to an equivalent L{POR} to an
    item of the subject.
    
    @param por: Point of regard to a child of the subject
    @type por: L{POR}
    @return: Point of regard to an item of the subject
    @rtype: L{POR}
    @raise LookupError: When lookup for the offset fails
    @raise IndexError: When the offset of the child is invalid as an item index
    '''
    index = IAccessibleInfo(por).getAccIndex()
    off = _getEmbedCharOffset(self.accessible, index)
    por = POR(self.accessible, off, 0)
    return por

  @pyLinAcc.errorToLookupError 
  def getFirstAccChild(self):
    '''
    Always raises LookupError. Hypertext has no children per se, only embeds.
    
    @raise LookupError: Always
    '''
    raise LookupError

  @pyLinAcc.errorToLookupError 
  def getLastAccChild(self):
    '''
    Always raises LookupError. Hypertext has no children per se, only embeds.
    
    @raise LookupError: Always
    '''
    raise LookupError

  @pyLinAcc.errorToLookupError
  def getChildAcc(self, index):
    '''
    Always raises LookupError. Hypertext has no children per se, only embeds.
    
    @raise LookupError: Always
    '''
    raise LookupError
  
class HypertextAccInfoAdapter(DefaultAccInfoAdapter):
  '''
  Overrides L{DefaultNavAdapter} to provide information about hypertext 
  objects. Expects the subject to be a L{POR}.

  Adapts subject accessibles that provide the L{pyLinAcc.Interfaces.IHypertext} 
  and L{pyLinAcc.Interfaces.IText} interfaces.
  '''
  provides = [IAccessibleInfo]

  @staticmethod
  def when(subject):
    '''
    Tests if the given subject can be adapted by this class.
    
    @param subject: L{POR} containing an accessible to test
    @type subject: L{POR}
    @return: True when the subject meets the condition named in the docstring
      for this class, False otherwise
    @rtype: boolean
    '''
    acc = subject.accessible
    off = subject.item_offset
    Interfaces.IHypertext(acc)
    text = Interfaces.IText(acc)
    return True

  def allowsAccEmbeds(self):
    '''
    Always True. Hypertext allows embedding.
    
    @return: True
    @rtype: boolean
    '''
    return True

  @pyLinAcc.errorToLookupError
  def getAccItemText(self):
    '''
    Gets a chunk of accessible text past the embed character indicated by the
    item offset to the next embed character or end of line.
    
    @return: Accessible text of requested item
    @rtype: string
    @raise LookupError: When the accessible object is dead
    '''
    if self.item_offset is None:
      return self.accessible.name
    it = Interfaces.IText(self.accessible)
    # get the current line start and end offsets
    text, lstart, lend = \
        it.getTextAtOffset(self.item_offset,Constants.TEXT_BOUNDARY_LINE_START)
    # convert to unicode
    text = unicode(text, 'utf-8')
    # compute the item offset relative to the start of this line
    ro = self.item_offset-lstart
    # locate the next embed character
    i = text.find(EMBED_CHAR, ro)
    if i < 0:
      return text[ro:] # text is substring, not all text which io is relative to
    else:
      # slice up to the embed character
      return text[ro:i]
    
class HypertextEventHandlerAdapter(TextEventHandlerAdapter):
  '''  
  Overrides L{DefaultEventHandlerAdapter} to create proper L{POR}s for 
  hypertext objects having embed characters. Expects the subject to be a raw
  L{pyLinAcc.Accessible}.
  
  Adapts subject accessibles that provide the L{pyLinAcc.Interfaces.IHypertext} 
  and L{pyLinAcc.Interfaces.IText} interfaces.
  '''
  provides = [IEventHandler]

  @staticmethod
  def when(subject):
    '''
    Tests if the given subject can be adapted by this class.
    
    @param subject: Accessible to test
    @type subject: L{pyLinAcc.Accessible}
    @return: True when the subject meets the condition named in the docstring
      for this class, False otherwise
    @rtype: boolean
    '''
    Interfaces.IHypertext(subject)
    Interfaces.IText(subject)
    return True

  def _handleFocusEvent(self, event, **kwargs):
    '''
    Creates an L{AEEvent.FocusChange} indicating that the accessible being
    adapted has gained the focus. Corrects the L{POR} for the focus to account
    for the case where the hypertext object receiving the focus has an embed 
    character at the first position in its text such that the embedded object
    should probably be the target of the first selector event instead.
    
    @param event: Raw focus change event
    @type event: L{pyLinAcc.Event.Event}
    @param kwargs: Parameters to be passed to any created L{AEEvent}
    @type kwargs: dictionary
    @return: L{AEEvent.FocusChange} and L{AEEvent.SelectorChange}
    @rtype: tuple of L{AEEvent}
    '''
    focus_por = POR(self.subject, None, 0)
    # navigate to the first item of the given POR
    item_por = IItemNav(focus_por).getFirstItem(False)
    # adapt the accessible to an adapter which provides an IAccesssibleInfo
    # interface and get the accessible's item text
    item = IAccessibleInfo(item_por).getAccItemText()
    # focus events are always in the focus layer
    kwargs['focused'] = True
    return (FocusChange(focus_por, True, **kwargs),
            SelectorChange(item_por, item, **kwargs))
