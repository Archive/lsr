'''
Defines L{AccAdapt.Adapter}s for objects in accessibility toolkits to the
L{AccAdapt.Interface}s declared in L{AEInterfaces}. Subpackages contain 
adapters on a per toolkit basis. Adapters for toolkits that are not available
are not imported.

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
try:
  from ATSPI import *
except ImportError, e:
  print 'AT-SPI adapters unavailable:', e