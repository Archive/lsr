'''
Defines a class responsible for managing all input and output devices and
monitors.

@author: Larry Weiss
@author: Peter Parente
@author: Brett Clippingdale
@author: Scott Haeger
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
import logging, sys
import AEConstants, AEOutput, AEInput, AEEvent, AEMonitor, UIRegistrar
from AEInterfaces import implements
from UIRegistrar import DEVICE, MONITOR
from i18n import _

log = logging.getLogger('Device')

class DeviceManager(object):
  '''
  Creates and manages the devices and monitors use by LSR. Keeps a list of each
  and also defines a "default" for devices. Provides a common interface to all
  output and input devices and mirrors I/O to the monitors. Provides a mapping
  from output styles to semantic concepts in the user interface to allow
  customization of how information is presented.
  
  @ivar event_manager: L{EventManager} to which this L{DeviceManager} can post 
    L{AEEvent}s 
  @type event_manager: L{EventManager}
  @ivar out_devs: All output devices registered and initialized
  @type out_devs: list
  @ivar in_devs: All input devices registered and initialized
  @type in_devs: list
  @ivar in_mons: Collection of monitors to notify about input
  @type in_mons: L{AEMonitor.MonitorCollection}
  @ivar out_mons: Collection of monitors to notify about output
  @type out_mons: L{AEMonitor.MonitorCollection}
  @ivar acc_eng: The AccessEngine reference that created this class
  @type acc_eng: L{AccessEngine}
  @ivar marks: Maps index markers on devices to 
  @type marks: dictionary
  @ivar temp_devs: List of L{AEOutput} and L{AEInput} devices last removed in 
    L{unloadDevices} and held until the next call of L{loadDevices}. The 
    purpose of this list is to ensure strong references to all devices exist,
    and thus weak references are not destroyed, until the the L{DeviceManager}
    is repopulated with devices.
  @type temp_devs: list
  '''
  def __init__(self, acc_eng):
    '''
    Creates the empty lists for devices and monitors.

    @param acc_eng: The AccessEngine reference that created this class
    @type acc_eng: L{AccessEngine}
    '''
    self.out_devs = []
    self.in_devs = []
    self.in_mons = AEMonitor.MonitorCollection()
    self.out_mons = AEMonitor.MonitorCollection()
    self.acc_eng = acc_eng
    self.event_manager = None
    self.settings_manager = None
    self.marks = {}
    self.temp_devs = []

  def init(self, event_man, sett_man, **kwargs):
    '''
    Called by L{AccessEngine} at startup.

    @param event_man: L{EventManager} to which this L{DeviceManager} can
      post L{AEEvent}s 
    @type event_man: L{EventManager}
    @param sett_man: L{SettingsManager} which this L{DeviceManager} can use to
      load and persist device settings
    @type sett_man: L{EventManager}
    @param kwargs: References to managers not of interest here
    @type kwargs: dictionary
    '''
    self.event_manager = event_man
    self.settings_manager = sett_man
    
    # load all I/O monitor classes to be created at startup
    mons = UIRegistrar.loadAssociated(MONITOR, self.acc_eng.getProfile())
    self.addMonitors(*mons)
        
    # load all device classes to be created at startup
    self.loadDevices()
    
  def loadDevices(self):
    '''
    Iterates over all devices in the active profile and loads ones that provide
    functionality different from those already loaded.
    '''
    devs = UIRegistrar.loadAssociated(DEVICE, self.acc_eng.getProfile())
    # register all devices, making the first input and output the defaults
    for d in devs:
      # call registerDevice which will automatically set defaults
      try:
        self.registerDevice(d)
      except (AEOutput.AEOutputError, AEInput.AEInputError):
        log.debug('could not initialize device: %s', d.getName())
      except NotImplementedError, e:
        log.debug('device %s does not provide a new interface: %s' % 
                 (d.getName(), str(e)))
    
    # warn if we didn't initialize at least one output device
    if not len(self.out_devs):
      log.warn(_('no output available'))
    if not len(self.in_devs):
      log.warn(_('no input available'))
    self.temp_devs = []

  def unloadDevices(self):
    '''
    Unloads all devices from L{out_devs} and L{in_devs}, but keeps strong 
    references to all objects in L{temp_devs} until L{loadDevices} is called 
    again. Keeping strong references ensures weak references in L{Perk}s 
    continue to exist until the L{DeviceManager} has new devices available 
    for I/O.
    
    '''
    for d in self.out_devs[:]:
      self.temp_devs.append(d)
      self.unregisterDevice(d)
    #for d in self.in_devs:
      #self.unregisterDevice(d)
    
  def close(self):
    '''
    Shuts down this manager and all its registered L{AEOutput} and L{AEInput}
    devices and L{AEMonitor}s. Saves all style information for currently 
    loaded devices.
    '''
    for d in self.out_devs[:]:
      self.unregisterDevice(d)
    for d in self.in_devs[:]:
      self.unregisterDevice(d)
    self.in_mons.clear()
    self.out_mons.clear()
    
  def send(self, dev, name, value, sem, layer):
    '''
    Sends arbitrary data to a device. The device must recognize the name in
    order to decide what to do with the value data.
    
    This is a generic method which receives all content and commands to be 
    rendered and executed on a device. Standard name identifiers should be 
    used whenever possible. For instance, L{AEConstants.Output.CMD_STOP} and
    L{AEConstants.Output.CMD_TALK}. However, device specific names and values
    are certainly possible.

    @param name: Descriptor of the data value sent
    @type name: object
    @param value: Content value
    @type value: object
    @param sem: The semantic stream on which to send output; defaults to None 
      for the default semantic.
    @type sem: integer
    @param layer: Layer on which the event occurred
    @type layer: integer
    @param dev: Device that should receive the command
    @type dev: L{AEOutput}
    @return: Return value specific to the given command
    @rtype: object
    '''
    # notify monitors
    self.out_mons.show(OutputEvent(name), value=value, dev=dev, sem=sem, 
                       layer=layer)
    # quit if no device specified
    if dev is None: return
    if sem is None or layer is None:
      try:
        return dev.send(name, value, None)
      except NotImplementedError:
        return None
    else:
      style = dev.getStyle((sem, layer))
      # respect muting
      if style.Mute: return
      # send the data
      try:
        rv = dev.send(name, value, style)
      except NotImplementedError:
        # no return value when not implemented
        rv = None
      # mark the style as clean
      style.makeClean()
      return rv

  def sendStop(self, dev, sem, layer):
    '''
    Sends the stop command to the referenced output device.

    @param dev: Device that should receive the command
    @type dev: L{AEOutput}
    @param sem: Semantic description of the information to stop None to 
      indicate stopping all output
    @type sem: integer
    @param layer: Layer on which the event occurred
    @type layer: integer
    '''
    # notify monitors
    self.out_mons.show(OutputEvent(AEConstants.CMD_STOP), dev=dev, sem=sem, 
                       layer=layer)
    if dev is None: return
    if sem is None and layer is None:
      # send a global stop
      dev.send(AEConstants.CMD_STOP, None, None)
    elif sem is None:
      # find all styles associated with the given layer
      all = set([dev.getStyle((sem, layer)) for sem in
                 AEConstants.SEMANTIC_STYLES.keys()])
      for style in all:
        dev.send(AEConstants.CMD_STOP, None, style)
    else:
      style = dev.getStyle((sem, layer))
      dev.send(AEConstants.CMD_STOP, None, style)

  def sendTalk(self, dev, sem, layer):
    '''
    Tells the specified output device to send buffered data.

    @param dev: Device that should receive the command
    @type dev: L{AEOutput}
    @param sem: Semantic information to start outputting or None to indicate 
      that all buffered information should be output
    @type sem: integer
    @param layer: Layer on which the event occurred
    @type layer: integer
    '''
    # notify monitors
    self.out_mons.show(OutputEvent(AEConstants.CMD_TALK), dev=dev, sem=sem, 
                       layer=layer)
    if dev is None: return
    if sem is None:
      style = None
    else:
      style = dev.getStyle((sem, layer))
    # send the command
    dev.send(AEConstants.CMD_TALK, None, style)

  def sendFilename(self, dev, filename, sem, layer):
    '''
    Sends the filename to the specified output device.

    @param dev: Device that should receive the command
    @type dev: L{AEOutput}
    @param filename: The filename to send to the referenced device
    @type filename: string
    @param sem: Semantic description of the text to send or None to indicate the
      information is void of any known semantic
    @type sem: integer 
    @param layer: Layer on which the event occurred
    @type layer: integer
    '''
    # notify monitors
    self.out_mons.show(OutputEvent(AEOutput.NAME_FILENAME), value=filename, 
                       dev=dev, sem=sem, layer=layer)
    if dev is None: return
    # find the style
    style = dev.getStyle((sem, layer))
    # respect muting
    if style.Mute: return
    try:
      dev.send(AEConstants.CMD_FILENAME, filename, style)
    except NotImplementedError:
      pass
    # make the style clean
    style.makeClean()
    
  def sendString(self, dev, text, sem, layer, por=None):
    '''
    Sends the string to the specified output device.

    @param dev: Device that should receive the command
    @type dev: L{AEOutput}
    @param text: The text to send to the referenced device
    @type text: string
    @param sem: Semantic description of the text to send or None to indicate 
      the information is void of any known semantic
    @type sem: integer 
    @param layer: Layer on which the event occurred
    @type layer: integer
    @param por: Point of regard to the start of the string if one exists or 
      None if the string did not originate from a L{POR}
    @type por: L{POR}
    '''
    # notify monitors
    self.out_mons.show(OutputEvent(AEConstants.CMD_STRING), value=text, 
                       dev=dev, sem=sem, layer=layer)  
    if dev is None: return
    # get the style
    style = dev.getStyle((sem, layer))
    # respect muting
    if style.Mute: return
    # parse the string using the current style
    parsed = dev.parseString(text, style, por, sem)
    # send all words, pors, and styles
    for word, por, new_style in parsed:
      dev.send(AEConstants.CMD_STRING, word, new_style)
      # mark the style as clean
      new_style.makeClean()
    # mark the style clean
    style.makeClean()

  def sendIndex(self, dev, value, sem, layer):
    '''
    Sends the referenced index marker to the referenced device.

    @param dev: Device that should receive the command
    @type dev: L{AEOutput}
    @param sem: Semantic description of the index text to send or None to 
      indicate the information is void of any known semantic
    @type sem: integer
    @param layer: Layer on which the event occurred
    @type layer: integer
    '''
    # notify registered monitors
    self.out_mons.show(OutputEvent(AEConstants.CMD_INDEX), dev=dev, sem=sem,
                       layer=layer)
    if dev is None: return
    # get the style
    style = dev.getStyle((sem, layer))
    # respect muting
    if style.Mute: return
    try:
      # generate a new marker on the device
      mark = dev.send(AEConstants.CMD_INDEX, None, style)
    except NotImplementedError:
      # do nothing when indexing not implemented
      return
    # store the mark and associated data for later retrieval
    #self.marks[(dev, mark)] = (sem, layer, data)

  def sendWelcome(self):
    '''Outputs the LSR welcome message on the default output device.'''
    try:
      dev = self.out_devs[0]
    except IndexError:
      # no output available
      return
    msg = _('welcome to %s version %s, revision %s') % \
        (AEConstants.NAME, AEConstants.PROG_VERSION, AEConstants.PROG_DATE)
    st = dev.getStyle((AEConstants.SEM_ITEM, AEConstants.LAYER_FOCUS))
    dev.send(AEConstants.CMD_STRING, msg, st)
    dev.send(AEConstants.CMD_TALK, None)
    
  def sendGoodbye(self):
    '''Outputs the LSR goodbye message on the default output device.'''
    try:
      dev = self.out_devs[0]
    except IndexError:
      # no output available
      return
    st = dev.getStyle((AEConstants.SEM_ITEM, AEConstants.LAYER_FOCUS))
    dev.send(AEConstants.CMD_STRING_SYNC, _('lsr exiting'), st)

  def _isDuplicateDevice(self, dev, output):
    '''
    Checks if the given device is a duplicate in that it provides no new 
    interfaces beyond those provided by the devices already registered.
    
    @param dev: A device class
    @type dev: L{AEInput} or L{AEOutput} class
    @param output: Is the device an output device (True) or input (False)?
    @type output: boolean
    '''
    if output:
      reg_devs = self.out_devs
    else:
      reg_devs = self.in_devs
    # get interfaces device implements
    dev_int = dev.getCapabilities()
    # check if some registered device already implements all of these 
    # interfaces
    for reg_dev in reg_devs:
      reg_int = reg_dev.getCapabilities()
      if set(reg_int).issuperset(dev_int):
        return True
    return False

  def _registerInputDevice(self, dev):
    '''
    Registers the given device as an input device if the device implements the
    L{AEInput} base class and provides some capabilities not already provided
    by another registered input device. For instance, if a device reports it
    supports the "system keyboard" capability, no other system keyboard device
    will be loaded.
    
    @param dev: Device to attempt to register
    @type dev: L{AEInput.AEInput}
    @return: True if the device was registered, False if it is not an 
      L{AEInput} device, and None if it provides no new interfaces
    @rtype: boolean or None
    '''
    if not implements(dev, AEInput.AEInput):
      # don't register something that isn't an input device
      return False
    if self._isDuplicateDevice(dev, False):
      # don't register a device that provides all the same interfaces as a
      # previous device
      return False
    if dev not in self.out_devs:
      # don't reinitialize a device that was added to out_devs
      dev.init()
    # add DeviceManager as a gesture listener
    dev.addInputListener(self._onGesture)
    # append to the in_devs list after successfully initialized
    self.in_devs.append(dev)
    log.debug('added input device %s', str(dev))
    return True
  
  def _unregisterInputDevice(self, dev):
    '''
    Unregisters the given input device. Removes this manager from
    the list of gesture listeners stored in the device.
        
    @param dev: Device to unregister
    @type dev: L{AEInput.AEInput}
    '''
    try:
      # remove from list of input devices      
      self.in_devs.remove(dev)
      log.debug('removed input device %s', dev.getName())
    except ValueError:
      pass
    try:
      # do not observe gestures
      dev.removeInputListener(self._onGesture)
    except AttributeError:
      pass
  
  def _unregisterOutputDevice(self, dev):
    '''
    Unregisters the given output device. Provides the device with a reference 
    to the L{SettingsManager} so it can save state. Removes this manager from
    the list of index listeners stored in the device.
    
    @param dev: Device to unregister
    @type dev: L{AEOutput.AEOutput}
    '''
    try:
      # let an output device save its styles
      dev.saveStyles(self.settings_manager)
    except AttributeError:
      pass
    try:
      # remove from list of output devices
      self.out_devs.remove(dev)
      log.debug('removed output device %s', dev.getName())
    except ValueError:
      pass
    try:
      # do not observe indices
      dev.removeIndexListener(self._onIndex)
    except AttributeError:
      pass
  
  def _registerOutputDevice(self, dev):
    '''
    Registers the given device as an output device if the device implements the
    L{AEOutput} base class and provides some subinterface not already provided
    by another registered output device. For instance, if a device reports
    having "audio" capability, no other device providing just this capability
    will be loaded.
    
    @param dev: Device to attempt to register
    @type dev: L{AEOutput.AEOutput}
    @return: True if the device was registered, False if it is not an 
      L{AEOutput} device, and None if it provides no new interfaces
    @rtype: boolean or None
    '''
    if not implements(dev, AEOutput.AEOutput):
      # don't register something that isn't an output device
      return False
    dev = dev.getProxy()
    if self._isDuplicateDevice(dev, True):
      # don't register a device that provides all the same interfaces as a
      # previous device
      return None
    if dev not in self.in_devs:
      # don't reinitialize a device that was added to in_devs
      dev.init()
    # add DeviceManager as an index listener when implemented
    try:
      dev.addIndexListener(self._onIndex)
    except (AttributeError, NotImplementedError):
      # do nothing when indexing not implemented or supported
      pass
    # append to the out_devs list after successfully initialized
    self.out_devs.append(dev)
    log.debug('added output device %s', str(dev))
    # first output device registered is default

    try:
      # try to load styles for the device
      dev.loadStyles(self.settings_manager)
    except KeyError:
      # or initialize new output styles
      self._initOutputStyles(dev)
    # finally, always call postInit to let the device do more initialization
    # after the styles are available
    dev.postInit()
    return True
  
  def _initOutputStyles(self, dev):
    '''
    Initializes styles for an L{AEOutput} device. Calls
    L{AEOutput.Base.AEOutput.createDistinctStyles} on the device to get an
    initial batch of styles to use to distinguish some types of information. If
    that method is not implemented, the exception is ignored. Future requests
    to use styles per semantic tag will resort to making flyweights for the
    default style on the device.
    
    @param dev: Device reference
    @type dev: L{AEOutput}
    '''
    # create styles based on the abilities of this device
    num_layers = len(AEConstants.LAYERS_ALL)
    num_groups = len(AEConstants.STYLE_GROUP_ALL)
    try:
      styles = dev.createDistinctStyles(num_groups, num_layers)
    except NotImplementedError:
      return
    # create semantics, layer to styles mapping for device
    for layer, i in enumerate(AEConstants.LAYERS_ALL):
      # provide a default style for a semantic of None
      s = styles[i*num_groups]
      dev.setStyle((None, layer), s)
      for sem, grp in AEConstants.SEMANTIC_STYLES.items():
        # provide a style for (semantic, layer, device)
        s = styles[i*num_groups + (grp % num_groups)]
        dev.setStyle((sem, layer), s)

  def registerDevice(self, dev):
    '''    
    Registers the referenced device based on its one or more interfaces.
    When the interface is determined, the init() method is called on the device.
    Returns true when the reference is of a known type that initializes
    successfully.
    
    @param dev: The device to register
    @type dev: L{AEOutput} or L{AEInput} class
    @raise NotImplementedError: When the device implements none of the required
      interfaces
    @raise AEOutputError: When output device initialization fails
    @raise AEInputError: When input device initialization fails
    '''
    is_out = self._registerOutputDevice(dev)
    is_in = self._registerInputDevice(dev)

    if is_out == False and is_in == False:
      # raise an exception if none of the required interfaces was implemented
      raise NotImplementedError
    
  def unregisterDevice(self, dev):
    '''
    Unregisters a device from both the input and output lists based on its
    capabilities.
    
    @param dev: The device to unregister
    @type dev: L{AEOutput} or L{AEInput} class
    '''
    try:
      dev.close()
    except Exception:
      pass
    self._unregisterOutputDevice(dev)
    self._unregisterInputDevice(dev)

  def addMonitors(self, *monitors):
    '''
    Adds one or more L{AEMonitor}s to the list of monitors to be notified about
    IO events. 
    
    @param monitors: L{AEMonitor}s to notify
    @type monitors: tuple of L{AEMonitor}s
    '''
    self.in_mons.add(InputEvent, monitors)
    self.out_mons.add(OutputEvent, monitors)

  def getMonitors(self):
    '''
    @return: Collections of all loaded input and output L{AEMonitor}s in that
      order
    @rtype: 2-tuple L{AEMonitor.MonitorCollection}
    '''
    return self.in_mons, self.out_mons
  
  def getFirstOutput(self):
    '''
    Gets the first L{AEOutput} device to successfully load.
    
    @return: First loaded output device or None if no output is available
    @rtype: L{AEOutput}
    '''
    try:
      return self.out_devs[0]
    except IndexError:
      return None

  def getOutputByName(self, name):
    '''
    Gets the L{AEOutput} device registered under the given name.
    
    @param name: Name of the output device
    @type name: string
    @return: Output device or None if not registered
    @rtype: L{AEOutput}
    '''
    for dev in self.out_devs:
      if dev.getClassName() == name:
        return dev
    return None
  
  def getOutputByCaps(self, caps):
    '''
    Gets the L{AEOutput} device registered with the given capabilities.
    
    @param caps: Desired capabilities of the output device.
    @type caps: list of string
    @return: Output device or None if caps could not be satisfied
    @rtype: L{AEOutput}
    '''
    for dev in self.out_devs:
      if set(dev.getCapabilities()).issuperset(caps):
        return dev
    return None
  
  def getInputByName(self, name):
    '''
    Gets the L{AEInput} device registered under the given name.
    
    @param name: Name of the input device
    @type name: string
    @return: Input device or None if not registered
    @rtype: L{AEInput}
    '''
    for dev in self.in_devs:
      if dev.getClassName() == name:
        return dev
    return None

  def getInputByCaps(self, caps):
    '''
    Gets the L{AEInput} device registered with the given capabilities.
    
    @param caps: Desired capabilities of the input device.
    @type caps: list of string
    @return: Output device or None if caps could not be satisfied
    @rtype: L{AEOutput}
    '''
    for dev in self.in_devs:
      if set(dev.getCapabilities()).issuperset(caps):
        return dev
    return None
  
  def _onGesture(self, gesture, timestamp, **kwargs):
    '''    
    Creates an L{AEEvent} indicating the given gesture was found on a
    registered input device. When executed, the L{AEEvent} will notify the
    L{TierManager} about the gesture and allow it to activate the appropriate
    L{Task} registered to respond to the gesture in the active L{Tier}.
    
    @param gesture: Gesture seen on an input device
    @type gesture: L{AEInput.Gesture}
    @param timestamp: Time at which the event occurred
    @type timestamp: float
    @param kwargs: Additional keyword arguments to pass to the L{Task}
    @type kwargs: dictionary
    '''
    event = AEEvent.InputGesture(gesture, timestamp, **kwargs)
    self.event_manager.postEvents(event)
    # notify monitors
    self.in_mons.show(InputEvent(AEConstants.CMD_GESTURE), value=gesture, 
                      dev=gesture.getDevice())
    
  def _onIndex(self, dev, index):
    pass
    # notify monitors
    #self.out_mons.show(event=OutputEvent.MARKER)
    
  def getDevices(self):
    '''
    @return: References to all loaded devices
    @rtype: list of L{UIElement}
    '''
    return list(set(self.in_devs).union(self.out_devs))
  
  def getStyle(self, dev, sem, layer):
    '''
    Gets the style for semantic/layer of a given device.
    
    @param dev: The device from which to get the style.
    @type dev: L{AEOutput}
    @param sem: The device semantic from which to get the style, or None to
      get the device's default style.
    @type sem: integer 
    @param layer: Layer on which the event occurred
    @type layer: integer
    @return: the L{AEOutput.Style} for the given semantic of a device
    @rtype: L{AEOutput.Style}
    @raise KeyError: When the semantic or layer is invalid
    '''
    if dev is None: return None
    if sem is None or layer is None:
      return dev.getDefaultStyle()
    return dev.getStyle((sem, layer))

class DeviceEvent(object):
  '''
  Base type for indicating IO events to monitors such that a monitor may 
  select whether to monitor input, output, both, or neither based on this
  type and its subtypes.
  
  The L{cmd} attribute of an instance of this class should be considered 
  public readable.
  
  @ivar cmd: Command constant for this event
  @type cmd: object
  '''
  def __init__(self, cmd):
    '''
    Stores the command constant.
    
    @param cmd: Command constant for this event
    @type cmd: object
    '''
    self.cmd = cmd
    
class InputEvent(DeviceEvent):
  '''Event on an input device, wrapped for display by a L{AEMonitor}.'''
  def getName(self):
    '''
    @return: Human readable name for the command
    @rtype: string
    '''
    return AEConstants.INPUT_COMMAND_NAMES[self.cmd]
  
class OutputEvent(DeviceEvent):
  '''Event on an output device, wrapped for display by a L{AEMonitor}.'''
  def getName(self):
    '''
    @return: Human readable name for the command
    @rtype: string
    '''
    return AEConstants.OUTPUT_COMMAND_NAMES[self.cmd]
