'''
Defines a function for configuring the standard Python logging module, a class
that redirects print statements through the configured logging system, and
a class that defines a logging handler using XML-RPC as the transport.

@var LOG_FORMAT: Default log output format
@type LOG_FORMAT: string

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at 
U{http://www.opensource.org/licenses/bsd-license.php}
'''
import logging, logging.config, sys, socket, xmlrpclib

# define a default format for log entries
# see http://docs.python.org/lib/node352.html
LOG_FORMAT = '[%(name)s %(levelname)s %(asctime)s] %(message)s'

class XMLRPCHandler(logging.Handler):
  '''
  Defines an XML-RPC handler for the standard Python logging system.
  
  @ivar server: Server proxy mirroring methods on the server side
  @type server: xmlrpclib.ServerProxy
  '''
  def __init__(self, host='127.0.0.1', port=9000):
    '''
    Constructs a proxy to the server.
    
    @param host: Hostname on which the server is running
    @type host: string
    @param port: Port on which the server is listening
    @type port: integer
    '''
    logging.Handler.__init__(self)
    self.server = xmlrpclib.ServerProxy('http://%s:%d' % (host, port))
  
  def emit(self, record):
    '''
    Writes interpolated string log messages to the server.
    
    @param record: Record to log
    @type record: logging.LogRecord
    '''
    if record is not None:
      msg = self.format(record)
      if msg is not None:
        try:
          self.server.write(msg)
        except socket.error:
          # ignore connection exceptions
          pass

# hack: the logging.config.fileConfig function only looks in the logging
# module name space for handlers; put our handler there
logging.XMLRPCHandler = XMLRPCHandler

class PrintLogger(object):
  '''
  Provides a dirt-simple interface compatible with stdout and stderr. When
  assigned to sys.stdout or sys.stderr, an instance of this class redirects 
  print statements to the logging system. This means the result of the 
  print statements can be silenced, sent to a file, etc. using the command
  line options to LSR. The log level used is defined by the L{LEVEL} constant
  in this class.
  
  @cvar LEVEL: Logging level for writes directed through this class, above
    DEBUG and below INFO
  @type LEVEL: integer
  @ivar log: Reference to the Print log channel
  @type log: logging.Logger
  '''
  LEVEL = 15
  
  def __init__(self):
    '''
    Create the logger.
    '''
    self.log = logging.getLogger('print')
    self.chunks = []
    
  def write(self, data):
    '''
    Write the given data at the debug level to the logger. Stores chunks of
    text until a new line is encountered, then sends to the logger.
    
    @param data: Any object that can be converted to a string
    @type data: stringable
    '''
    s = data.encode('utf-8')
    if s.endswith('\n'):
      self.chunks.append(s[:-1])
      s = ''.join(self.chunks)
      self.log.log(self.LEVEL, s)
      self.chunks = []
    else:
      self.chunks.append(s)

def configure(log_conf=None, log_level=None, log_channels=None, log_file=None):
  '''
  Configures the logging system based on the setting specified on the command
  line or Python logging module compatible configuration file. Defaults to 
  logging messages at the INFO level or higher to stdout. Options in the
  configuration file always override those given on the command line.
  
  @param log_conf: Filename of a logging module configuration file. See
    U{http://antonym.org/node/76} and 
    U{http://docs.python.org/lib/logging-config-fileformat.html}.
  @type log_conf: string
  @param log_level: Include messages at this level or higher
  @type log_level: integer
  @param log_channels: Include messages sent only on these channels, or None
    meaning all
  @type log_channels: list of string
  @param log_file: Log messages in a basic, fixed format to this file. Provided
    for convenience for users who do not want to write their own logging conf
    file.
  @type log_file: string
  '''
  # if a configuration file is provided, use that
  if log_conf is not None:
    logging.config.fileConfig(log_conf)
  else:
    # otherwise, fall back on a basic config
    if log_level is None:
      # default to logging critical messages only
      log_level = 'info'
    if log_level == 'print':
      # set the log level to print statements if specified
      level = PrintLogger.LEVEL
    else:
      # get the logging constant from the logging module by name; it's an
      # uppercase version of the log_level string provided on the command line
      level = getattr(logging, log_level.upper())
    # get the root logger
    root = logging.getLogger()
    # log to a file or to stderr
    if log_file:
      handler = logging.FileHandler(log_file)
    else:
      handler = logging.StreamHandler(sys.stderr)
    # set the format and handler for messages
    formatter = logging.Formatter(LOG_FORMAT)
    root.addHandler(handler)
    root.setLevel(level)
    handler.setFormatter(formatter)
    # add filters to the root handler
    if log_channels is not None:
      for name in log_channels:
        handler.addFilter(logging.Filter(name))
  # create a print log channel, redirect all print statements to the log
  stdout = sys.stdout
  sys.stdout = PrintLogger()
