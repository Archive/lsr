'''
Defines a class unifying navigation over all accessibles and their items.

@todo: PP: need robust strategy for handling cycles (mark nodes as visited 
  somehow?)

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''

import Base
from AEInterfaces import *

class AccessibleItemWalker(Base.Walker):
  '''  
  Walks the accessible hierarchy by accessibles and their items returning
  L{POR}s. The walk order is an in-order traversal of the subtree of the
  accessible hierarchy rooted at a top-level window accessible. The subtree is
  assumed to have no loops, though logic could be added to detect them. The
  supported flags determine whether invisible or trivial items are skipped or
  not. This class uses the L{AEInterfaces} to leave the decision of what
  constitutes an item, what constitutes a trivial accessible, and what
  constitutes and invisible accessible to the objects that implement the
  interfaces.
  
  @ivar allow_invisible: Stop on invisible L{POR}s as well as visible?
  @type allow_invisible: boolean
  @ivar allow_trivial: Stop on trivial L{POR}s as well as non-trivial?
  @type allow_trivial: boolean
  '''
  def __init__(self, por, only_visible=True, allow_trivial=False):
    '''
    Stores the starting L{POR}.
    
    @param only_visible: Stop only on visible L{POR}s?
    @type only_visible: boolean
    @param allow_trivial: Stop on trivial L{POR}s as well as non-trivial?
    @type allow_trivial: boolean
    @param por: The current L{POR} for the L{Walker}
    @type por: L{POR}
    '''
    super(AccessibleItemWalker, self).__init__(por)
    self.allow_invisible = not only_visible
    self.allow_trivial = allow_trivial
    
  def _getNextItem(self, por):
    '''    
    Gets the next item in the current accessible in the given L{POR}. Returns
    that next item if it exists. If not or if the accessible in the given
    L{POR} is not visible, returns the current L{POR} and the L{_getFirstChild}
    as the method to call to continue the search.
    
    @param por: Initial L{POR}
    @type por: L{POR}
    @return: L{POR} and the next method to call to continue the search, or 
      L{POR} and None to indicate the search is complete
    @rtype: 2-tuple of L{POR}, callable
    '''
    if self.allow_invisible or IAccessibleInfo(por).isAccVisible():
      try:
        # try to get the next item
        item = IItemNav(por).getNextItem(not self.allow_invisible)
        return (item, None)
      except (LookupError, IndexError):
        # there's no next item
        pass
    return (por, self._getFirstChild)
            
  def _getFirstChild(self, por):
    '''    
    Gets the first child of the current accessible in the given L{POR}. Returns
    the child accessible if it exists, is visible, and is not trivial. If it
    does not exist or is invisible, returns the given L{POR} and
    L{_getNextPeer} as the method to call to continue the search. If it is
    trivial, returns the child accessible and L{_getFirstChild} as the method
    to call to continue the search.
    
    @param por: Initial L{POR}
    @type por: L{POR}
    @return: L{POR} and the next method to call to continue the search, or 
      L{POR} and None to indicate the search is complete
    @rtype: 2-tuple of L{POR}, callable
    '''
    try:
      # try to get first child accessible
      child = IAccessibleNav(por).getFirstAccChild()
      ai = IAccessibleInfo(child)
      if not self.allow_invisible and not ai.isAccVisible():
        # don't use this child or its children if it is not visible
        return (child, self._getNextPeer)
      elif not self.allow_trivial and ai.isAccTrivial():
        # skip this child and move a level deeper if it is trivial
        return (child, self._getFirstChild)
      else:
        # use this child
        return (child, None)
    except (LookupError, IndexError):
      # there's no child accessible
      return (por, self._getNextPeer)
  
  def _getNextPeer(self, por):
    '''    
    Gets the next peer of the current accessible in the given L{POR}. Returns
    the peer accessible if it exists, is visible, and is not trivial. If it
    does not exist, returns the given L{POR} and L{_getParentNextItem} as the
    method to call to continue the search. If it is invisible, returns the peer
    L{POR} and L{_getNextPeer} as the method to call to continue the search. If
    it is trivial, returns the peer and L{_getFirstChild} as the method to call
    to continue the search. If it is trivial, returns the child accessible and
    L{_getFirstChild} as the method to call to continue the search.

    @param por: Initial L{POR}
    @type por: L{POR}
    @return: L{POR} and the next method to call to continue the search, or 
      L{POR} and None to indicate the search is complete
    @rtype: 2-tuple of L{POR}, callable
    '''
    try:
      # don't navigate peers if this accessible is embedded
      if IAccessibleInfo(por).isAccEmbedded():
        raise IndexError
      # try getting the next peer accessible
      next = IAccessibleNav(por).getNextAcc()
      ai = IAccessibleInfo(next)
      if not self.allow_invisible and not ai.isAccVisible():
        # skip to the next peer if this one is not visible
        return (next, self._getNextPeer)
      elif not self.allow_trivial and ai.isAccTrivial():
        # skip this peer and move to its children if it is trivial
        return (next, self._getFirstChild)
      else:
        # use this peer
        return (next, None)
    except (LookupError, IndexError):
      # there's no next peer accessible
      return (por, self._getParentNextItem)
    
  def _getParentNextItem(self, por):
    '''
    Gets the next item in the parent accessible of the current accessible in
    the given L{POR}. Returns the next item if it exists. Else, returns the
    parent accessible and L{_getNextPeer} as the method to call to continue the
    search if the parent exists. Returns a sentinel (None, None) if there is no
    parent indicating the given L{POR} is the root of the subtree containing
    the starting L{POR}.
    
    @param por: Initial L{POR}
    @type por: L{POR}
    @return: L{POR} and the next method to call to continue the search, or 
      L{POR} and None to indicate the search is complete
    @rtype: 2-tuple of L{POR}, callable
    '''
    try:
      parent = IAccessibleNav(por).getParentAcc()
      if IAccessibleInfo(parent).isAccTopLevelWindow():
        # stop if the parent is the root of the active window
        raise IndexError
    except (LookupError, IndexError):
      # there's no parent, so bail because we're on the last accessible
      return (None, None)
    # convert a child accessible to an item if possible
    try:
      # try to navigate to the next item
      por = IItemNav(parent).getAccAsItem(por)
      return (por, self._getNextItem)
    except (LookupError, IndexError):
      # get the next peer of the parent accessible
      return (parent, self._getNextPeer)

  def _getPrevItem(self, por):
    '''
    Gets the previous item in the current accessible in the given L{POR}.
    Returns that previous item if it exists. If not or if the accessible in the
    given L{POR} is not visible, returns the current L{POR} and the
    L{_getPrevPeer} as the method to call to continue the search.
    
    @param por: Initial L{POR}
    @type por: L{POR}
    @return: L{POR} and the next method to call to continue the search, or 
      L{POR} and None to indicate the search is complete
    @rtype: 2-tuple of L{POR}, callable
    '''
    if self.allow_invisible or IAccessibleInfo(por).isAccVisible():
      try:
        # try to get the previous item
        item = IItemNav(por).getPrevItem(not self.allow_invisible)
        if item.isSameAcc(por):
          # use the item if it's in the same accessible
          return (item, None)
        else:
          # otherwise try to get the last child
          return (item, self._getLastChild)
      except (LookupError, IndexError):
        # there's no previous item
        pass
    return (por, self._getPrevPeer)
  
  def _getPrevPeer(self, por):
    '''
    Gets the previous peer of the current accessible in the given L{POR}. If it
    does not exist, returns the given L{POR} and L{_getParent} as the method to
    call to continue the search. If it is not visible, returns the peer
    accessible and L{_getPrevPeer} as the method to call to continue the search.
    Otherwise, returns the peer accessible and L{_getLastChild} as the method to
    call to continue the search.
    
    @param por: Initial L{POR}
    @type por: L{POR}
    @return: L{POR} and the next method to call to continue the search, or 
      L{POR} and None to indicate the search is complete
    @rtype: 2-tuple of L{POR}, callable
    '''
    try:
       # don't navigate peers if this accessible is embedded
      if IAccessibleInfo(por).isAccEmbedded():
        raise IndexError
      # try to get the previous peer
      prev = IAccessibleNav(por).getPrevAcc()
      ai = IAccessibleInfo(prev)
      if ai.isAccTopLevelWindow():
        # stop if the parent is the root of the active window
        return (None, None)
      elif self.allow_invisible or ai.isAccVisible():
        # get the deepest last child
        return (prev, self._getLastChild)  
      else:
        # skip this peer and its children
        return (prev, self._getPrevPeer)
    except (LookupError, IndexError):
      # there's no previous peer accessible
      return (por, self._getParentPrevItem)
    
  def _getParentPrevItem(self, por):
    '''
    Gets the previous item in the parent accessible of the current accessible
    in the given L{POR}. Returns the previous item if it exists. Else, returns
    the parent accessible and L{_getParent} as the method to call to continue
    the search if the parent exists. Returns a sentinel (None, None) if there
    is no parent indicating the given L{POR} is the root of the subtree
    containing the starting L{POR}.
    
    @param por: Initial L{POR}
    @type por: L{POR}
    @return: L{POR} and the next method to call to continue the search, or 
      L{POR} and None to indicate the search is complete
    @rtype: 2-tuple of L{POR}, callable
    '''
    try:
      ai = IAccessibleInfo(por)
      if ai.isAccTopLevelWindow():
        raise IndexError
      parent = IAccessibleNav(por).getParentAcc()
    except (LookupError, IndexError):
      # there's no parent, so bail because we're on the last accessible
      return (None, None)
    # convert a child accessible to an item if possible
    try:
      # try to navigate to the previous item
      por = IItemNav(parent).getAccAsItem(por)
      return (por, self._getPrevItem)
    except (LookupError, IndexError):
      # get the previous peer of the parent accessible
      return (por, self._getParent)

  def _getLastChild(self, por):
    '''    
    Gets the last child of the accessible in the given L{POR}. If it does not
    exist, checks if the given L{POR} is invisible or trivial. If so, returns
    the given L{POR} and L{_getPrevPeer} to continue the search. If not,
    returns a L{POR} to the last item in the given L{POR} as the result.

    If the last child does exist, checks if it is visible. If so, returns the
    child and L{_getLastChild} to continue the search. If not, returns the
    child and L{_getPrevPeer} to continue the search.
    
    @param por: Initial L{POR}
    @type por: L{POR}
    @return: L{POR} and the next method to call to continue the search, or 
      L{POR} and None to indicate the search is complete
    @rtype: 2-tuple of L{POR}, callable
    '''
    try:
      # try to get the last child
      child = IAccessibleNav(por).getLastAccChild()
      ai = IAccessibleInfo(child)
      if self.allow_invisible or ai.isAccVisible():
        # try for the next deeper last child
        return (child, self._getLastChild)
      else:
        # try for the previous peer of the invisible child
        return (child, self._getPrevPeer)
    except (LookupError, IndexError), e:
      ai = IAccessibleInfo(por)
      if ((not self.allow_invisible and not ai.isAccVisible()) or 
           (not self.allow_trivial and ai.isAccTrivial())):
        # try the previous peer of the invisible or trivial accessible
        return (por, self._getPrevPeer)
      else:
        # use the last item in this accessible
        return (por, self._getLastItem)
    
  def _getLastItem(self, por):
    '''
    Gets the last visible item of the given L{POR}. Returns the given L{POR} if
    any errors occur.
    
    @param por: Initial L{POR}
    @type por: L{POR}
    @return: L{POR} pointing to the last item of the accessible in the given
      L{POR}
    @rtype: L{POR}
    '''
    try:
      item = IItemNav(por).getLastItem(not self.allow_invisible)
      ai = IAccessibleInfo(item)
      if item.isSameAcc(por):
        # use this por if the item is in the same accessible
        return (item, None)
      elif self.allow_invisible or ai.isAccVisible():
        # try for the next deeper last child
        return (item, self._getLastChild)
      else:
        # try for the previous peer of the invisible item
        return (item, self._getPrevItem)
    except (LookupError, IndexError):
      # just use the current por if there is an error
      return (por, None)

  def _getParent(self, por):
    '''
    Gets the parent accessible of the one in the given L{POR}. Returns the last
    item in the parent if it exists. If it does not exist, Returns a sentinel
    (None, None) indicating the given L{POR} is the root of the subtree
    containing the starting L{POR}. If the parent is invisible or trivial,
    returns the parent and L{_getPrevPeer} as the method to call to continue 
    the search.
    
    @param por: Initial L{POR}
    @type por: L{POR}
    @return: L{POR} and the next method to call to continue the search, or 
      L{POR} and None to indicate the search is complete, or None and None to
      indicate we're at the root
    @rtype: 2-tuple of L{POR}, callable
    '''
    try:
      # try to get the parent
      parent = IAccessibleNav(por).getParentAcc()
      ai = IAccessibleInfo(parent)
      if ((not self.allow_invisible and not ai.isAccVisible()) or 
          (not self.allow_trivial and ai.isAccTrivial())):
        # skip the parent and move to its previous peer
        return (parent, self._getPrevPeer)
      else:
        # use the parent, but get its last item
        return (parent, None)
    except (LookupError, IndexError):
      # there's no parent, so bail because we're at the root
      return (None, None)

  def getNextPOR(self):
    '''
    Gets the next L{POR} in the walk order. Calls L{_getNextItem}, 
    L{_getFirstChild}, L{_getNextPeer}, and L{_getParentNextItem} to attempt to 
    get the next valid L{POR}. Each method determines whether the L{POR} is 
    valid as the next L{POR}, and, if not, which call to make next. Each method
    potentially returns a L{POR} and the next method to call to continue the
    search for the next L{POR}.
    
    @return: Next L{POR} or None if this is the last L{POR}
    @rtype: L{POR} or None
    '''
    state = self._getNextItem
    por = self.por
    while state is not None:
      #print por, state.func_name
      por, state = state(por)
    if por is not None:
      self.por = por
    #print 'STOP', por
    return por

  def getPrevPOR(self):
    '''
    Gets the previous L{POR} in the walk order. Calls L{_getPrevItem},
    L{_getPrevPeer}, L{_getLastChild}, and L{_getParentPrevItem} to attempt to
    get the previous valid L{POR}. Each method determines whether the L{POR} is
    valid as the previous L{POR}, and, if not, which call to make next. Each
    method potentially returns a L{POR} and the next method to call to continue
    the search for the previous L{POR}.
    
    @return: Previous L{POR} or None if this is the first L{POR}
    @rtype: L{POR} or None
    '''
    state = self._getPrevItem
    por = self.por
    while state is not None:
      #print state.func_name, str(por)
      por, state = state(por)
    if por is not None:
      self.por = por
    #print 'STOP prev', por
    return por
  
  def getFirstPOR(self):
    '''
    Gets the first L{POR} in the walk order. Searches up the accessible 
    hierarchy until an accessible with no parent is encountered. The last 
    visited child of that accessible is the first L{POR} (i.e. the top-level
    window containing the starting L{POR}).
    
    @return: First L{POR}
    @rtype: L{POR}
    '''
    por = self.por
    child = self.por
    while 1:
      try:
        parent = IAccessibleNav(por).getParentAcc()
      except LookupError:
        self.por = child
        return child
      else:
        child = por
        por = parent
        
  def getLastPOR(self):
    '''
    Gets the last L{POR} in the walk order. Searches down the last child branch
    of the accessible hierarchy to the deepest accessible. The search then
    proceeds through previous L{POR}s in the walk order until the first visible,
    non-trivial accessible is encountered. The last item of that accessible is
    the last L{POR}.
    
    @return: Last L{POR}
    @rtype: L{POR}
    '''
    # get to the first POR first so we can traverse the very last branch
    self.getFirstPOR()
    while 1:
      try:
        self.por = IAccessibleNav(self.por).getLastAccChild()
      except LookupError:
        ai = IAccessibleInfo(self.por)
        if ((not ai.isAccTrivial() or self.allow_trivial) and 
            (ai.isAccVisible() or self.allow_invisible)):
          old = self.por
          # get the last item
          try:
            self.por = IItemNav(self.por).getLastItem(not self.allow_invisible)
          except LookupError:
            pass
          if self.por == old:
            # only stop if we didn't move, else continue checking
            return self.por
        else:
          # back off until we find a non-trivial accessible
          return self.getPrevPOR()
        
  def getParentPOR(self):
    '''
    Gets the parent L{POR} of the current L{POR}. Searches up the accessible
    hierarchy to find the first non-trivial, visible containing element.
    
    @return: Parent L{POR} or None if at root
    @rtype: L{POR}
    '''
    while 1:
      try:
        # try to get the parent
        self.por = IAccessibleNav(self.por).getParentAcc()
        ai = IAccessibleInfo(self.por)
        if ((self.allow_invisible or ai.isAccVisible()) and 
            (not ai.isAccTrivial() or self.allow_trivial)):
          return self.por
      except (LookupError, IndexError):
        # there's no parent, so bail because we're at the root
        return None