'''
Defines an abstract base class acting as an interface for all walkers. Defines
the methods to be provided.

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''

class Walker(object):
  '''    
  Walks accessible hiearchy in a well-defined order determined by a subclass of
  this base class. The traversal is stateless so that the walker can begin at
  any node in the tree and walk in any direction.
  
  @ivar por: The starting L{POR} for the L{Walker}
  @type por: L{POR}
  '''
  def __init__(self, por):
    '''
    Stores the starting L{POR}.
    
    @param por: The current L{POR} for the L{Walker}
    @type por: L{POR}
    '''
    self.por = por
    
  def getCurrPOR(self):
    '''
    @return: Point of regard to the current object
    @rtype: L{POR}
    @raise NotImplementedError: When not overriden in a subclass
    '''
    return self.por
  
  def getParentPOR(self):
    '''
    @return: Point of regard to the parent of the current object
    @rtype: L{POR}
    @raise NotImplementedError: When not overriden in a subclass
    '''
    raise NotImplementedError
  
  def getNextPOR(self):
    '''
    @return: Point of regard to the next object in the walk order
    @rtype: L{POR}
    @raise NotImplementedError: When not overriden in a subclass
    '''
    raise NotImplementedError
  
  def getPrevPOR(self):
    '''
    @return: Point of regard to the previous object in the walk order
    @rtype: L{POR}
    @raise NotImplementedError: When not overriden in a subclass
    '''
    raise NotImplementedError
  
  def getFirstPOR(self):
    '''
    @return: Point of regard to the first of all objects
    @rtype: L{POR}
    @raise NotImplementedError: When not overriden in a subclass
    '''
    raise NotImplementedError

  def getLastPOR(self):
    '''
    @return: Point of regard to the last of all objects
    @rtype: L{POR}
    @raise NotImplementedError: When not overriden in a subclass
    '''
    raise NotImplementedError
  