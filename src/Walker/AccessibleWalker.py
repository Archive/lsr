'''
Defines a class that provides a "raw" walk of all accessibles in the hiearchy.

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
import Base
from AEInterfaces import *

class AccessibleWalker(Base.Walker):
  '''
  Walks the accessible hierarchy exactly as it is presented. The walk order is 
  an in-order traversal of the subtree of the accessible hierarchy rooted at a 
  top-level window accessible. The subtree is assumed to have no loops, 
  though logic could be added to detect them.
  '''
  def _getFirstChild(self, por):
    '''    
    Gets the first child of the current accessible in the given L{POR}. Returns
    the child accessible if it exists. If it does not exist, returns the given
    L{POR} and L{_getNextPeer} as the method to call to continue the search.
    
    @param por: Initial L{POR}
    @type por: L{POR}
    @return: L{POR} and the next method to call to continue the search, or 
      L{POR} and None to indicate the search is complete
    @rtype: 2-tuple of L{POR}, callable
    '''
    try:
      # try to get first child accessible
      child = IAccessibleNav(por).getFirstAccChild()
      return (child, None)
    except (LookupError, IndexError):
      # there's no child accessible
      return (por, self._getNextPeer)
  
  def _getNextPeer(self, por):
    '''    
    Gets the next peer of the current accessible in the given L{POR}. Returns
    the peer accessible if it exists, is visible, and is not trivial. If it does
    not exist, returns the given L{POR} and L{_getParentNextPeer} as the method
    to call to continue the search. If it is invisible, returns the peer L{POR}
    and L{_getNextPeer} as the method to call to continue the search. If it is
    trivial, returns the peer and L{_getFirstChild} as the method to call to
    continue the search. If it is trivial, returns the child accessible and
    L{_getFirstChild} as the method to call to continue the search.

    @param por: Initial L{POR}
    @type por: L{POR}
    @return: L{POR} and the next method to call to continue the search, or 
      L{POR} and None to indicate the search is complete
    @rtype: 2-tuple of L{POR}, callable
    '''
    try:
      # try getting the next peer accessible
      next = IAccessibleNav(por).getNextAcc()
      return (next, None)
    except (LookupError, IndexError):
      # there's no next peer accessible
      return (por, self._getParentNextPeer)

  def _getParentNextPeer(self, por):
    '''
    Gets the parent accessible of the current accessible in the given L{POR}. 
    Returns the parent and L{_getNextPeer} as the method to call to continue the
    search if the parent exists. Returns a sentinel (None, None) if there is no
    parent indicating the given L{POR} is the root of the subtree containing
    the starting L{POR}.
    
    @param por: Initial L{POR}
    @type por: L{POR}
    @return: L{POR} and the next method to call to continue the search, or 
      L{POR} and None to indicate the search is complete
    @rtype: 2-tuple of L{POR}, callable
    '''
    try:
      parent = IAccessibleNav(por).getParentAcc()
      if IAccessibleInfo(parent).isAccTopLevelWindow():
        # stop if the parent is the root of the active window
        return (None, None)
      else:
        # get the next peer of the parent accessible
        return (parent, self._getNextPeer)
    except (LookupError, IndexError):
      # there's no parent, so bail because we're on the last accessible
      return (None, None)
  
  def _getPrevPeer(self, por):
    '''
    Gets the previous peer of the current accessible in the given L{POR}. If it
    does not exist, returns the given L{POR} and L{_getParent} as the method to
    call to continue the search. If it is not visible, returns the peer
    accessible and L{_getPrevPeer} as the method to call to continue the search.
    Otherwise, returns the peer accessible and L{_getLastChild} as the method to
    call to continue the search.
    
    @param por: Initial L{POR}
    @type por: L{POR}
    @return: L{POR} and the next method to call to continue the search, or 
      L{POR} and None to indicate the search is complete
    @rtype: 2-tuple of L{POR}, callable
    '''
    try:
      # try to get the previous peer
      prev = IAccessibleNav(por).getPrevAcc()
      ai = IAccessibleInfo(prev)
      if ai.isAccTopLevelWindow():
        # stop if the parent is the root of the active window
        return (None, None)
      else:
        # get the deepest last child
        return (prev, self._getLastChild)
    except (LookupError, IndexError):
      # there's no previous peer accessible
      return (por, self._getParent)

  def _getLastChild(self, por):
    '''
    Gets the last child of the accessible in the given L{POR}. If it does not
    exist, checks if the given L{POR} is invisible or trivial. If so, returns 
    the given L{POR} and L{_getPrevPeer} to continue the search. If not, returns
    a L{POR} to the last item in the given L{POR} as the result. 

    If the last child does exist, checks if it is visible. If so, returns the
    child and L{_getLastChild} to continue the search. If not, returns the
    child and L{_getPrevPeer} to continue the search.
    
    @param por: Initial L{POR}
    @type por: L{POR}
    @return: L{POR} and the next method to call to continue the search, or 
      L{POR} and None to indicate the search is complete
    @rtype: 2-tuple of L{POR}, callable
    '''
    try:
      # try to get the last child
      child = IAccessibleNav(por).getLastAccChild()
      # try for the next deeper last child
      return (child, self._getLastChild)
    except (LookupError, IndexError):
      #use the last child
      return (por, None)
    
  def _getParent(self, por):
    '''
    Gets the parent accessible of the one in the given L{POR}. Returns the last
    item in the parent if it exists. If it does not exist, Returns a sentinel
    (None, None) indicating the given L{POR} is the root of the subtree
    containing the starting L{POR}. If the parent is invisible or trivial,
    returns the parent and L{_getPrevPeer} as the method to call to continue the
    search.
    
    @param por: Initial L{POR}
    @type por: L{POR}
    @return: L{POR} and the next method to call to continue the search, or 
      L{POR} and None to indicate the search is complete, or None and None to
      indicate we're at the root
    @rtype: 2-tuple of L{POR}, callable
    '''
    try:
      # try to get the parent
      parent = IAccessibleNav(por).getParentAcc()
      # if there is a parent, use it
      return (parent, None)
    except (LookupError, IndexError):
      # there's no parent, so bail because we're at the root
      return (None, None)
    
  def _getLastAcc(self, por):
    '''
    Gets the last accessible of the given L{POR}. Returns the given L{POR} if
    any errors occur.
    
    @param por: Initial L{POR}
    @type por: L{POR}
    @return: L{POR} pointing to the last accessible in the given L{POR}
    @rtype: L{POR}
    '''
    try:
      return IAccessibleNav(por).getLastAccChild()
    except LookupError:
      # just use the current por if there is an error
      return por

  def getNextPOR(self):
    '''    
    Gets the next L{POR} in the walk order. Calls L{_getFirstChild},
    L{_getNextPeer}, and L{_getParentNextPeer} to attempt to get the next valid
    L{POR}. Each method determines whether the L{POR} is valid as the next
    L{POR}, and, if not, which call to make next. Each method potentially
    returns a L{POR} and the next method to call to continue the search for the
    next L{POR}.
    
    @return: Next L{POR} or None if this is the last L{POR}
    @rtype: L{POR} or None
    '''
    state = self._getFirstChild
    por = self.por
    while state is not None:
      por, state = state(por)
    if por is not None:
      self.por = por
    return por

  def getPrevPOR(self):
    '''    
    Gets the previous L{POR} in the walk order. Calls L{_getPrevPeer},
    L{_getLastChild}, and L{_getParent} to attempt to get the previous valid
    L{POR}. Each method determines whether the L{POR} is valid as the previous
    L{POR}, and, if not, which call to make next. Each method potentially
    returns a L{POR} and the next method to call to continue the search for the
    previous L{POR}.
    
    @return: Previous L{POR} or None if this is the first L{POR}
    @rtype: L{POR} or None
    '''
    state = self._getPrevPeer
    por = self.por
    while state is not None:
      por, state = state(por)
    if por is not None:
      self.por = por
    return por  
  
  def getParentPOR(self):
    '''   
    @return: Point of regard to the parent of the current accessible or None if
      it does not exist
    @rtype: L{POR}
    '''
    try:
      self.por = IAccessibleNav(self.por).getParentAcc()
    except LookupError:
      return None
    return self.por
  
  def getFirstPOR(self):
    '''
    @return: Point of regard to the first accessible
    @rtype: L{POR}
    '''
    por = self.por
    child = self.por
    while 1:
      try:
        parent = IAccessibleNav(por).getParentAcc()
      except LookupError:
        self.por = child
        return child
      else:
        child = por
        por = parent

  def getLastPOR(self):
    '''
    @return: Point of regard to the last accessible
    @rtype: L{POR}
    '''
    # get to the first POR first so we can traverse the very last branch
    self.getFirstPOR()
    while 1:
      try:
        self.por = IAccessibleNav(self.por).getLastAccChild()
      except LookupError:
        # stop when we can find any more children
        return self.por