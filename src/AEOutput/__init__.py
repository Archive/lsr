'''
Defines abstract classes representing the interfaces that must be implemented
by an output device for LSR. All output devices derive from the base class in
L{AEOutput}. Devices that provide audio output should derive from the
L{Audio} subclass. Devices that provide Braille output should derive from
the L{Braille} subclass.

Concrete classes implementing the methods of L{AEOutput} and it's subclasses
that ship with LSR are not located in this package. Rather, they are stored in
the Devices folder and referenced by the L{UIRegistrar} when they need to be
loaded.

@author: Larry Weiss
@author: Peter Parente
@author: Brett Clippingdale
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
from Base import *
from Audio import Audio
from Braille import Braille
from Style import *
from Error import *
