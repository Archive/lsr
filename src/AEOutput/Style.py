'''
Defines classes for representing device capabilities and styles. 

@author: Brett Clippingdale
@author: Peter Parente
@author: Scott Haeger
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
from i18n import _
from AEConstants import *
import Word, AEState

class Style(AEState.AEState):
  '''
  Extends L{AEState} with new kinds of settings for devices.
  
  Mute (bool): Is all output inhibited? Non-persistent by default.

  @ivar parent_style: Another style object to which this one should dispatch 
    attribute lookups if attributes are not defined in this object
  @type parent_style: L{Style}
  '''
  def __init__(self, parent_style=None):
    '''
    Initializes the style object to which to dispatch requests for attributes
    that are not defined in this object.
    
    @param parent_style: Parent style object
    @type parent_style: L{Style}
    '''
    super(Style, self).__init__()
    # store the parent style
    self.parent_style = parent_style
    
    if self.isDefault():
      # initialize defaults
      self._initDefault()
      # always create the mute setting since DeviceManager depends on it
      self.newBool('Mute', False, _('Mute'), 
                   _('When set, all output is inhibited. This setting is not '
                     'saved to disk.'), False)
    else:
      # initialize flyweights
      self._initFlyweight()
      
  def init(self, device):
    '''
    Extends the default method signature with a parameter containing a 
    reference the device to which this style belongs.
    
    @param device: Output device reference
    @type device: L{AEOutput}
    '''
    pass
      
  def _initDefault(self):
    '''
    Does nothing by default. Override to initialze settings that should be
    created on default objects.
    '''
    pass
  
  def _initFlyweight(self):
    '''
    Copies references to all default settings into this object. This means the
    setting are shared between this object and the default.
    '''
    self.settings.update(self.parent_style.settings)

  def isDefault(self):
    '''
    Gets if the current style is the default meaning it does not have a parent.
    
    @return: Is this style the default?
    @rtype: boolean
    '''
    return self.parent_style is None
    
  def getDefault(self):
    '''
    Gets a reference to the default style if it exists or None if it does not.
    
    @return: L{parent_style}
    @rtype: L{Style}
    '''
    return self.parent_style

  def copy(self):
    '''
    Makes a copy of this object and its parent.
    
    @return: New style object
    @rtype: L{Style}
    '''
    c = super(Style, self).copy()
    ps = self.parent_style
    if ps is not None:
      psc = ps.copy()
      c.parent_style = psc
    return c
  
  def isDirty(self):
    '''
    Gets if values in L{settings} have changed since the last call to 
    L{makeClean}.
    
    @return: Dirty or not?
    @rtype: boolean
    '''
    d = super(Style, self).isDirty()
    if self.parent_style is None:
      return d
    else: 
      return d or self.parent_style.isDirty()
  
  def makeClean(self):
    '''
    Resets the dirty set to empty.
    '''
    super(Style, self).makeClean()
    if self.parent_style is not None:
      self.parent_style.makeClean()
    
  def iterDirty(self):
    '''
    Iterates over all of the names in L{dirty} starting with the parent and
    then working through this object.
    
    @return: Name of a dirty setting
    @rtype: string
    '''
    for name in self.parent_style.iterDirty():
      yield name
    for name in self.dirty:
      yield name
      
  def newRelNumeric(self, name, default, label, min, max, precision, 
                    description='', persist=True):
    '''
    Adds a new L{AEState.Setting.RelNumericSetting} to this group.
    
    @param name: New name of the setting
    @type name: string
    @param default: Default value of the setting
    @type default: float
    @param label: Label for the new L{AEState.Setting}
    @type label: string
    @param min: Minimum value in the range
    @type min: number
    @param max: Maximum value in the range
    @type max: number
    @param precision: Number of decimal places
    @type precision: integer
    @param description: Extended description of the L{AEState.Setting}
    @type description: string
    @param persist: Persist the setting value to disk?
    @type persist: boolean
    @return: New setting object
    @rtype: L{AEState.NumericSetting}
    '''
    s = AEState.RelNumericSetting(self, name, default, label, description, 
                                  persist, min, max, precision)
    self.settings[name] = s
    return s

  def newRelRange(self, name, default, label, min, max, precision, 
                  description='', persist=True):
    '''
    Adds a new L{AEState.Setting.RelRangeSetting} to this group.
    
    @param name: New name of the setting
    @type name: string
    @param default: Default value of the setting
    @type default: float
    @param label: Label for the new L{AEState.Setting}
    @type label: string
    @param min: Minimum value in the range
    @type min: number
    @param max: Maximum value in the range
    @type max: number
    @param precision: Number of decimal places
    @type precision: integer
    @param description: Extended description of the L{AEState.Setting}
    @type description: string
    @param persist: Persist the setting value to disk?
    @type persist: boolean
    @return: New setting object
    @rtype: L{AEState.RangeSetting}
    '''
    s = AEState.RelRangeSetting(self, name, default, label, description,
                                persist, min, max, precision)
    self.settings[name] = s
    return s
    
  def newRelPercent(self, name, default, label, min, max, precision, 
                    description='', persist=True):
    '''
    Adds a new L{AEState.Setting.RelPercentRangeSetting} to this group.
    
    @param name: New name of the setting
    @type name: string
    @param default: Default value of the setting
    @type default: float
    @param label: Label for the new L{AEState.Setting}
    @type label: string
    @param min: Minimum value in the range to be shown as 0%
    @type min: number
    @param max: Maximum value in the range to be shown as 100%
    @type max: number
    @param precision: Number of decimal places
    @type precision: integer
    @param description: Extended description of the L{AEState.Setting}
    @type description: string
    @param persist: Persist the setting value to disk?
    @type persist: boolean
    @return: New setting object
    @rtype: L{AEState.Setting.PercentRangeSetting}
    '''
    s = AEState.RelPercentRangeSetting(self, name, default, label, description,
                                       persist, min, max, precision)
    self.settings[name] = s
    return s

class AudioStyle(Style, Word.WordState):
  '''
  Defines the basic style attributes for all L{AEOutput.Audio} devices. Most
  attributes documented below do not exist in this base class. It is the job 
  for a specific output device definition to add new settings to its own style
  class class if it supports any of the missing concepts. Only those settings 
  related to text parsing are pre-defined here for use by the default 
  implementation of parseString in L{AEOutput.Audio}.
  
  CapString (string): Set to the string that should be presented prior to the
  spelling of capitalized letters and presented twice before spelling fully
  capitalized words. Defaults to 'cap'.
 
  Volume (range): Current volume

  Rate (range): Current speech rate

  Pitch (range): Current baseline pitch

  Voice (enum): Current voice number

  PosX (integer): Current spatial position x-coordinate, left negative

  PosY (integer): Current spatial position y-coordinate, down negative

  PosZ (integer): Current spatial position z-coordinate, behind negative

  Language (choice): Current language in use by the speech synthesizer 
  formatted according to the syntax set forth in RFC 4646 and drawn from the
  registry created by RFC 4645. See U{http://www.ietf.org/rfc/rfc4646.txt} and
  U{http://www.iana.org/assignments/language-subtag-registry}. If an engine
  implements some unknown derivation, use the 'x' private marker in the name
  and continue with additional designations. See IBMSpeech for an example
  implementation. The device is responsible for case-insensitive comparisons.

  Instrument (enum): Current MIDI instrument number

  Continuous (bool): Is output continuous (i.e. looping)?

  Channel (integer): Channel number indicating concurrency of output. Styles 
  sharing a channel number are output sequentially. Styles having different 
  channel numbers are played concurrently if possible.

  Stoppable (bool): Can output be stopped?

  Speakable (bool): Can output be spoken?

  Soundable (bool): Can output be played as a non-speech sound?

  Gender (enum): Speech vocal tract gender

  Aspiration (range): Speech breathiness

  Frication (range): Emphasis of fricatives in speech

  Intonation (range): Speech pitch variation

  HeadSize (range): Speech speaker head size for reverberation

  SpellCaps (bool): When set to True, fully capitalized words will be spelled
  for presentation if the device supports spelling.

  Blank (string): String to substitute for blank output. Defaults to 'blank' 
  localized.

  CapExpand (bool):  When set to True, a space will be inserted before
  embedded capital letters in the presentation of a word. Defaults to True.

  NumExpand (bool):  When set to True, a space will be inserted before embedded
  numbers in the presentation of a word. Defaults to False.

  SpellFormat (enum): Set to FORMAT_TEXT to present words without additional
  spelling. Set to FORMAT_PRONOUNCE to spell punctuation characters for
  presentation. Set to FORMAT_SPELL to spell all characters for 
  presentation. Set to FORMAT_PHONETIC to spell all characters phonetically 
  for presentation. Defaults to FORMAT_TEXT. Any values other than 
  FORMAT_TEXT may be ignored by a device.
  '''
  def _initDefault(self):
    '''
    Called automatically by the L{Style} base class when this style is a 
    default. Initializes all default audio parsing options. If you want these
    settings created, be sure to call this base class implementation if you
    override in a subclass. Defaults to creating all L{Word.WordState} 
    settings.
    '''
    self._initWordSettings()
    self.newString('CapString', 'cap', _('Cap string'),
                  _('String to say once before spelling capitalized letters '
                     ' and twice before spelling fully capitalized words.'))
    self.newString('Blank', _('blank'), _('Blank string'), 
                   _('String to speak when a blank or ignored character is '
                     'output on its own.'))
    self.newBool('CapExpand', True, _('Expand caps?'), 
              _('When set, insert a space before each capital letter in the '
                'output.'))
    self.newBool('NumExpand', False,  _('Expand numbers?'), 
                 _('When set, insert a space before each number in the '
                   'output.'))
    self.newEnum('SpellFormat', FORMAT_TEXT, _('Pronunciation rule'), 
                 {_('Pronounce words') : FORMAT_TEXT,
                  _('Pronounce punctuation') : FORMAT_PRONOUNCE,
                  _('Pronounce characters') : FORMAT_SPELL,
                  _('Pronounce phonetic') : FORMAT_PHONETIC},
                 _('Defines what and how characters are pronounced.'), False)

  def _newWordGroup(self, group):
    '''
    Builds a group containing settings that should be available for all audio
    devices given that the L{Word} parser and the
    L{AEOutput.Audio.Audio.parseString} methods support them without any extra
    work on the part of the device.
    
    @param group: Parent group for the word settings group
    @type group: L{AEState.Setting.Group}
    @return: Group containing the word settings, can be extended as needed
    @rtype: L{AEState.Setting.Group}
    '''
    w = group.newGroup(_('Words'))
    w.extend(['WordDef', 
             'SpellFormat',
             'MaxRepeat',
             'Ignore',
             'Blank',
             'CapString',
             'Caps',
             'CapExpand',
             'NumExpand'])
    return w

class BrailleStyle(Style):
  '''
  Defines the basic style attributes for all L{AEOutput.Braille} devices. 
  
  DisplayColumns (integer) Number of cells in each display row. 
  
  DisplayRows (integer) Number of row in display.
  
  TotalCells (integer) Total cells in display.
  
  CaretStyle (enum): Current caret style
  
  EllipsisRight (bool): Does user want right ellipsis shown? 
  
  EllipsisLeft (bool): Does user want left ellipsis shown? 
  
  EllipsisStyle (enum): Current ellipsis style
  
  CellMask (string): Missing cell string.  0=bad cell; 1=good cell.
  
  @cvar missingcellcnt: Count of missing cells in CellMask. Initialized as a 
    class variable for convienced, but used as an instance variable.
  @type missingcellcnt: integer
  '''
  missingcellcnt = 0
  
  def _initDefault(self):
    '''
    Called automatically by the L{Style} base class when this style is a 
    default. Initializes all default braille options. If you want these
    settings created, be sure to call this base class implementation if you
    override in a subclass.
    '''
    # braille display properties
    self.newNumeric('DisplayColumns', 40, _('Columns in display'), 0, 80, 0)
    self.newNumeric('DisplayRows', 1, _('Rows in display'), 0, 10, 0)
    self.newNumeric('TotalCells', 40, _('Total cells in display'), 0, 800, 0)
    self.newEnum('CaretStyle', CARET_TWO_BOTTOM, _('Caret Display Style'), 
                {_('No Caret') : CARET_NONE,
                _('Two Bottom Pins') : CARET_TWO_BOTTOM,
                _('Bottom Right Pins') : CARET_BOTTOM_RIGHT,
                _('All Pins') : CARET_ALL},
                _('Defines the caret representation.'))
                
    self.newBool('EllipsisRight', False, _('Ellipsis right?'), 
                _('When set, ellipsis will appear when additional text is to '
                  'the right of the viewport.'))
    self.newBool('EllipsisLeft', False, _('Ellipsis left?'), 
              _('When set, ellipsis will appear when additional text is to '
                'the left of the viewport.'))
    self.newEnum('EllipsisStyle', ELLIPSIS_ELLIPSIS, 
                 _('Ellipsis Display Style'), 
                 {_('Pin 3 in 3 cells') : ELLIPSIS_ELLIPSIS,
                  _('Continuation symbol') : ELLIPSIS_CONTINUATION},
                 _('Defines the ellipsis representation.'))

    cm = self.newString('CellMask', '', _('Broken Cell String'), 
                  _('String of zeros and ones indicating the functionality of '
                    'each cell'))
    # add observer to count missing cells on cell mask           
    cm.addObserver(self._setMissingCells)
    
  def _setMissingCells(self, style, setting):
    '''
    Updates the missing cell count instance variable whenever the CellMask
    setting changes value.
    
    @param style: This style object
    @type style: L{BrailleStyle}
    @param setting: Setting that change value, namely CellMask
    @type setting: L{AEState.Setting}
    '''
    self.missingcellcnt = self.CellMask.count('0')
  
  def _newBrailleGroup(self, root):
    '''
    Builds groups of standard Braille settings that should be available for all 
    Braille devices. The groups are added directly to the root group provided.
    
    @param root: Parent group for the word settings group
    @type root: L{AEState.Setting.Group}
    @return: The root group again
    @rtype: L{AEState.Setting.Group}
    '''
    c = root.newGroup(_('Caret'))
    c.append('CaretStyle')
    d = root.newGroup(_('Ellipsis'))
    d.append('EllipsisRight')
    d.append('EllipsisLeft')
    d.append('EllipsisStyle')
    e = root.newGroup(_('Broken Cells'))
    e.append('CellMask')
    return root
