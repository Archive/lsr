'''
Defines the base class for all audio output devices.

@author: Brett Clippingdale
@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
import time
import Base, Style
import Word, AEConstants
from i18n import _
from ThreadProxy import *

class Audio(Base.AEOutput):
  ''' 
  Defines the base class for all audio output devices. Provides default 
  implementations of L{sendStringSync} and L{parseString} specific to audio
  devices.
  
  Some methods defined here raise NotImplementedError to ensure that
  derived classes create appropriate implementions. Implementing 
  L{sendIndex} is optional. The L{DeviceManager} catches the not implemented 
  exception in this case.
  
  @ivar listeners: List of callables that should be notified when a marker 
    inserted with L{sendIndex} is encountered
  @type listeners: list
  '''
  STYLE = Style.AudioStyle
  
  def __init__(self):
    '''
    Initializes the empty listeners list.
    '''
    Base.AEOutput.__init__(self)
    self.listeners = []
    
  def getCapabilities(self):
    '''
    @return: 'audio' as the only capability of this device.
    @rtype: list of string
    '''
    return ['audio']
  
  def addIndexListener(self, listener):
    '''
    Adds a listener that should be notified when speech has progressed to the 
    point where a marker was inserted with L{sendIndex}.
    
    @param listener: The method that should be called when markers received.
    @type listener: callable
    '''
    self.listeners.append(listener)

  def removeIndexListener(self, listener):
    '''
    Removes the specified listener.
    
    @param listener: The method that should no longer be called when markers 
      received.
    @type listener: callable
    @raise ValueError: When the given listener is not already registered
    '''
    self.listeners.remove(listener)  
  
  def getProxy(self):
    '''
    Looks at the L{USE_THREAD} flag to see if the device implementing this 
    interface wants a thread proxy or not.
    
    @return: self or L{ThreadProxy.AudioThreadProxy}
    @rtype: L{AEOutput}
    '''
    if self.USE_THREAD == True:
      return AudioThreadProxy(self)
    elif self.USE_THREAD == False:
      return self
    else:
      raise NotImplementedError('USE_THREAD not specified')
      
  def _parseMain(self, word, style, ch, part):
    '''    
    Adds support for the L{AEOutput.Style.AudioStyle} settings of
    CapExpand and NumExpand. This method is notified during parsing of the main
    part of a L{Word}.
    
    @param word: Word currently parsing its source text
    @type word: L{Word}
    @param style: Style used to configure the parsing process
    @type style: L{AEOutput.Style.AudioStyle}
    @param ch: Character in the word
    @type ch: string
    @param part: List of characters already processed in this part of the word
    @type part: list
    @return: Character(s) to be appended to the list
    @rtype: string
    '''
    sf = style.SpellFormat
    if sf and (word.isPunctuation(ch) or word.isSymbol(ch)):
      # spelling is anything but words, always add a space
      if part:
        part.append(', ')
      # get the unicode character name
      return word.getCharName(ch) or ch
    elif sf == Style.FORMAT_SPELL:
      # spelling is set to characters only, always add a space
      if part:
        part.append(', ')
      try:
        # try to find a spelling
        return AEConstants.SPELLED_MAP[ch]
      except KeyError:
        # unicode hex value followed
        return _('uni-code %s') % word.getCharValue(ch)
    elif sf == Style.FORMAT_PHONETIC:
      # spelling is set to NATO phonetic, always add a space
      if part:
        part.append(', ')
      try:
        # try to use a nato code word
        return AEConstants.NATO_MAP[ch]
      except KeyError:
        # unicode hex value followed by the character classification
        return _('uni-code %s %s') % (word.getCharValue(ch), 
                                      word.getCharDescription(ch))

    # only handle cap/num expand if spelling isn't enabled
    if style.CapExpand and word.isCap(ch) and word.getMainLength():
      part.append(' ')
    elif style.NumExpand and word.isNumeric(ch) and word.getMainLength():
      part.append(' ')    
    
    return ch

  def parseString(self, text, style, por, sem):
    '''
    Provides a default implementation of parsing that formats words for audio
    devices supporting speech output. The base L{Word} class is used plus some
    additional processing for:
    
      - blank words
      - expanded caps
      - expanded numbers
      - spelling format
      - individual characters
    
    @param text: Text to be parsed
    @type text: string
    @param style: Style object defining how the text should be parsed
    @type style: L{AEOutput.Style}
    @param por: Point of regard for the first character in the text, or None if
      the text is not associated with a POR
    @type por: L{POR}
    @param sem: Semantic tag for the text to aid parsing
    @type sem: integer
    @return: Parsed words
    @rtype: 3-tuple of lists of string, L{POR}, L{AEOutput.Style}
    '''
    rv = []
    # parse words
    words = Word.buildWordsFromString(text, por, style, self._parseMain)
    for i, w in enumerate(words):
      ns = None
      if len(text) == 1 and style.SpellFormat == AEConstants.FORMAT_TEXT:
        # handle individual characters by spelling them if we're treating them
        # as words
        try:
          # try to find a spelling
          ns = AEConstants.SPELLED_MAP[text]
        except KeyError:
          ns = text
          
      if w.isAllBlank():
        if i == 0 and len(words) > 1:
          # ignore initial blank words when there is more than one word in the
          # group
          continue
        # fill in blank words
        if sem != AEConstants.SEM_CHAR:
          ns = style.Blank
          
      if ns is None:
        # leave non-blanks alone
        ns = unicode(w)
      if por is None:
        # don't bother with PORs if we didn't start with one
        por = None
      else:
        por = w.getPOR()
      # ignore style changes for the time being
      rv.append((ns, por, style))
    return rv
  
  def send(self, name, value, style=None):
    '''    
    Dispatches known commands to their appropriate handlers.
    
    @param name: Descriptor of the data value sent
    @type name: object
    @param value: Content value
    @type value: object
    @param style: Style with which this value should be output
    @type style: L{AEOutput.Style}
    @return: Return value specific to the given command
    @rtype: object
    @raise NotImplementedError: When the handler for a common command is not 
      implemented by a subclass
    '''
    if name == AEConstants.CMD_STOP:
      self.sendStop(style)
    elif name == AEConstants.CMD_TALK:
      self.sendTalk(style)
    elif name == AEConstants.CMD_STRING:
      self.sendString(value, style)
    elif name == AEConstants.CMD_STRING_SYNC:
      self.sendStringSync(value, style)
    elif name == AEConstants.CMD_FILENAME:
      # may not be implemented
      self.sendFilename(value, style)
    elif name == AEConstants.CMD_INDEX:
      # may not be implemented
      return self.sendIndex(style)
    elif name == AEConstants.CMD_GET_CLOSEST_LANG:
      # may not be implemented
      return self.sendGetClosestLang(value)
      
  def sendString(self, text, style):
    '''
    Sends a string of one or more characters to the device. The style object
    is used by the device in deciding how the given text should be presented.

    @param text: Text to send to the device
    @type text: string
    @param style: Style with which this text should be output
    @type style: L{AEOutput.Style}
    @raise NotImplementedError: When not overridden in a subclass
    '''
    raise NotImplementedError
  
  def sendFilename(self, fn, style):
    '''
    Sends a string filename to the device, the contents of which should be 
    output. The style object is used by the device in decided how the given
    text should be presented.
    
    Typically, this method should be implemented by an audio device that 
    supports playback of waveform or sequencer files. It might also be used
    by devices as a way of synthesizing the entire contents of a file without
    having to pass all of the contents through the rest of the system.
    
    @param fn: Absolute filename
    @type fn: string
    @param style: Style with which this text should be output
    @type style: L{AEOutput.Style}
    @raise NotImplementedError: When not overridden in a subclass
    '''
    raise NotImplementedError

  def sendStop(self, style=None):
    '''
    Purges buffered text and styles, and interrupts on-going output.

    @param style: Style indicating which channel on which the stop should be 
      performed; None indicates stop on all channels
    @type style: L{AEOutput.Style}
    @raise NotImplementedError: When not overridden in a subclass
    '''
    raise NotImplementedError

  def sendTalk(self, style=None):
    '''
    Indicates all text buffered by L{sendString} should now be output. 
    For devices that do the buffering in the driver, this action may mean
    simply sending the command. For devices that do not buffer, this action 
    means sending text and styles buffered in the LSR device definition.

    @param style: Style indicating which channel on which the talk should be 
      performed; None indicates talk on all channels
    @type style: L{AEOutput.Style}
    @raise NotImplementedError: When not overridden in a subclass
    '''
    raise NotImplementedError
  
  def sendIndex(self, style):
    '''
    Inserts a marker in the output stream. The device should notify all 
    listeners when the marker is reached. The marker is typically a 
    monotonically increase integer number mod the maximum integer value 
    supported by the device.
    
    @param style: Style indicating which channel on which the marker should be 
      inserted
    @type style: L{AEOutput.Style}
    @return: Unique marker identifying the index inserted
    @rtype: integer
    @raise NotImplementedError: When not overridden in a subclass
    '''
    raise NotImplementedError  
  
  def sendStringSync(self, text, style):
    '''
    Buffers a complete string to send to the device synchronously.

    This should B{not} be used in place of L{sendString} since this will not
    return until the string is finished being output. This is provided B{only}
    for the convenience of utility writers. Uses L{sendStop}, L{sendString}, 
    L{sendTalk}, and then sleeps until L{isActive} returns False.
    
    This method sends the stop, string, and talk commands directly to the 
    device, bypassing any thread proxy. This could be problematic if the audio
    library in question cannot be used in more than one thread.

    @param text: String to send to the device
    @type text: string
    @param style: Style on which this string should be output; None implies 
      some reasonable default should be used
    @type style: L{AEOutput.Style}
    '''
    if style is None:
      style = self.default_style
    self.send(AEConstants.CMD_STOP, None, None)
    self.send(AEConstants.CMD_STRING, text, style)
    self.send(AEConstants.CMD_TALK, None, None)

    # wait until done speaking; this blocks the main thread -- be careful
    while self.isActive():
      time.sleep(1)

  def sendGetClosestLang(self, tag):
    '''
    Maps a language tag to the closest one possible as supported by this 
    device. The result may be an exact match or only a partial match. If 
    absolutely nothing matches the requested tag, not even starting at the
    major language, None should be returned.
    
    @param tag: IANA language tag, lower case
    @type tag: string
    @return: IANA language tag or None
    @rtype: string
    '''
    raise NotImplementedError
