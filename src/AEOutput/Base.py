'''
Defines the base class for all L{AEOutput} devices.

@author: Larry Weiss
@author: Peter Parente
@author: Brett Clippingdale
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
import UIElement, Style, AEConstants

def getDefaults():
  '''
  Suggests the default L{AEOutput}s events to be monitored.
  
  @return: Names of defaults to monitor
  @rtype: list of string
  '''
  return AEConstants.OUTPUT_COMMAND_NAMES.values()
  
def getNames():
  '''
  Gets the names of all the L{AEOutput} command types.
  
  @return: List of all known L{AEOutput} command names
  @rtype: list of string
  '''
  names = AEConstants.OUTPUT_COMMAND_NAMES
  names = [names[i] for i in (AEConstants.CMD_STOP, 
                              AEConstants.CMD_TALK, 
                              AEConstants.CMD_STRING)]
  names.sort()
  return names

class AEOutput(UIElement.UIE):
  '''
  Defines the base class for all output devices. All output devices used by
  LSR should derive from this class and should implement the methods defined
  here. All methods defined here raise NotImplementedError to ensure that
  derived classes create appropriate implementions.
  
  @cvar STYLE: Style class to use when constructing style objects for this 
    device. A subclass with configurable device settings should
    override the default value (L{AEOutput.Style}) with a reference to
    its own default class defining default values for all device properties.
  @type STYLE: L{AEOutput.Style} class
  @cvar USE_THREAD: Should this device use a separate thread to queue output?
    Devices that block while doing output (e.g. serial Braille device) should 
    use a thread to keep the main thread unlocked and responsive. Defaults to 
    None so a subclass must override it with an explicit boolean value.
  @type USE_THREAD: boolean
  @cvar COMMAND_CHARS: String of characters that are treated as commands on the
    device implementing this interface. These characters are replaced with
    blanks in any string sent to the device.
  @type COMMAND_CHARS: string
  @ivar styles: Mapping from arbitrary, immutable keys to style objects
  @type styles: dictionary
  '''
  USE_THREAD = None
  COMMAND_CHARS = ''
  STYLE = Style.Style

  def __init__(self):
    '''
    Initializes the styles dictionary and default style object.
    '''
    self.styles = {}
    # instantiate an instance of the STYLE class; it may be filled with 
    # data when preInit is called
    self.default_style = self.STYLE()
    
  def getCapabilities(self):
    '''
    Gets a list of strings representing the capabilities of this device. 
    Typical output capabilities include "audio" and "braille" though others are
    certainly possible.
    
    The L{DeviceManager} will only load a device if another device doesn't
    already provide all of its capabilities.
    
    @return: Lowercase names of output capabilities
    @rtype: list of string
    '''
    raise NotImplementedError
    
  def loadStyles(self, sett_man):
    '''    
    Called after the L{init} method by the L{DeviceManager} to ensure that the
    device is functioning before time is spent unserializing its style data.
    
    Calls the init method on the default style object and provides it with a
    reference to this initialized device. Then tries to load the persisted 
    setting values from disk. If that fails, the L{DeviceManager} will try to
    call L{createDistinctStyles} instead.
    
    @note: This method should not be overriden by a subclass. See L{postInit} 
      if you need to run code after the styles are created or restored from
      disk.
    @param sett_man: Instance of the settings manager
    @type sett_man: L{SettingsManager}
    @raise KeyError: When styles have not previously been persisted for this
      device
    @raise OSError: When the profile file cannot be opened or read
    '''
    self.default_style.init(self)
    # use the settings manager to try to load persisted styles
    self.styles = sett_man.loadStyles(self, self.default_style, self.STYLE)
    
  def saveStyles(self, sett_man):
    '''
    Persists styles to disk. Called after the L{close} method by the 
    L{DeviceManager} to ensure the device is properly shutdown before 
    serializing its data.
    
    @note: This method should not be overriden by a subclass.
    @param sett_man: Instance of the settings manager
    @type sett_man: L{SettingsManager}
    @raise KeyError: When styles have not previously been persisted for this
      device
    @raise OSError: When the profile file cannot be opened or read
    '''
    # use the settings manager to try to persist styles
    sett_man.saveStyles(self, self.default_style, self.styles)
  
  def setStyle(self, key, style):
    '''
    Stores the style object under the given key. The style object should be
    one previously generated by this device (e.g. using 
    L{createDistinctStyles}) but it is not an enforced requirement. Always
    makes the style clean before storing it.
    
    @param key: Any immutable object
    @type key: immutable
    @param style: L{AEOutput} subclass of L{AEState}
    @type style: L{AEState}
    '''
    style.makeClean()
    self.styles[key] = style
    
  def getStyle(self, key):
    '''
    Gets the style object stored under the given key. If the key is unknown,
    returns an empty flyweight backed by the default style and stores the
    new style in L{styles}.
    
    @param key: Any immutable object
    @type key: immutable
    @return: L{AEOutput} subclass of L{AEState}
    @rtype: L{AEState}
    '''
    try:
      return self.styles[key]
    except KeyError:
      # create a new style based on the default
      st = self.STYLE(self.default_style)
      # initialize the new style
      st.init(self)
      self.styles[key] = st
      return st
  
  def getDefaultStyle(self):
    '''
    Gets the default style of the device.
    
    @return: Default style of the device
    @rtype: L{AEOutput.Style}
    '''
    return self.default_style
  
  def createDistinctStyles(self, num_groups, num_layers):
    '''
    Creates a default set of distinct styles for this device. The device should
    instantiate L{AEOutput.Style} objects having properties that reflect the
    capabilities of this device, leaving any unsupported fields set to the 
    value of None. A total number of num_groups + num_layers style objects
    should be returned.
    
    The properties of the style object should be set so as to distinguish 
    content presented using the style. For instance, audio devices may
    distinguish styles in the following recommended manner:
    
      - Create a total of num_groups styles with different voices and pitches.
      - If the device supports multiple channels,
        - Duplicate the styles num_layers times assigning each set of 
          duplicates to the same channel.
      - If the device does not support multiple channels,
        - Duplicate the styles num_layers times.

    For Braille and magnification devices, don't implement this method unless
    you have some creative way of making the device respond differently to the
    semantic tags defined in L{AEConstants}.
    
    The total number of requested styles (num_groups * num_layers) must be
    returned. If the device cannot honor the request for the number of distinct
    styles it is asked to generate, it may duplicate styles it has already
    created using L{AEState.Base.AEState.copy} to fulfill the quota.
    
    The device is B{not} expected to create distinct styles across invocations 
    of this method. This method should only be called once by L{DeviceManager}
    to create a default set of styles for the device if the manager cannot load
    previously persisted settings from disk. If this method is not implemented,
    calls to L{getStyle} during normal operation of the L{DeviceManager} will
    end up making copies based on the default style.
    
    @param num_groups: Number of sematic groups the requestor would like to
      represent using distinct styles
    @type num_groups: integer
    @param num_layers: Number of content origins (e.g. output originating from
      a background task versus the focus) the requestor would like to represent
      using distinct styles
    @type num_layers: integer
    @return: New styles
    @rtype: list of L{AEOutput.Style}
    @raise NotImplementedError: When not overridden in a subclass
    '''
    raise NotImplementedError
  
  def init(self):
    '''
    Called after the instance is created to initialize the device.
    May be called to re-initialize the device after a call to L{close}.

    @raise NotImplementedError: When not overridden in a subclass
    @raise Error.InitError: When a communication or state problem exists for 
      the specific device
    '''
    raise NotImplementedError
  
  def postInit(self):
    '''
    Called after the L{init} method and after either L{loadStyles} or 
    L{createDistinctStyles}. Override this method to perform additional 
    initilization after the setting values are available.
    
    @raise Error.InitError: When a communication or state problem exists for 
      the specific device
    '''
    pass

  def close(self):
    '''
    Closes an initialized output device.

    @raise NotImplementedError: When not overridden in a subclass
    '''
    raise NotImplementedError
  
  def getProxy(self):
    '''
    Gets the object that the L{DeviceManager} will use to communicate with this
    device. The returned object may either be a proxy (e.g. a thread) or this
    device itself.
    
    @return: An output object that implements this class
    @rtype: L{AEOutput}
    @raise NotImplementedError: When not overridden in a subclass
    '''
    raise NotImplementedError
  
  def send(self, name, value, style=None):
    '''    
    Sends arbitrary data to a device. The device must recognize the name in
    order to decide what to do with the value data. If given, the style object
    is used to decide how the data should be presented if it is content to be
    rendered.
    
    This is a generic method which receives all content and commands to be 
    rendered and executed on a device. Standard name identifiers should be 
    used whenever possible. For instance, L{AEConstants.Output.CMD_STOP} and
    L{AEConstants.Output.CMD_TALK}. However, device specific names and values
    are certainly possible.
    
    @param name: Descriptor of the data value sent
    @type name: object
    @param value: Content value
    @type value: object
    @param style: Style with which this value should be output
    @type style: L{AEOutput.Style}
    @return: Return value specific to the given command
    @rtype: object
    @raise NotImplementedError: When not overridden in a subclass
    '''
    raise NotImplementedError
  
  def isActive(self):
    '''
    Indicates whether the device is active (e.g. giving output, has buffered 
    content to output, is otherwise busy) or not.

    @return: True when active, False when not
    @rtype: boolean
    @raise NotImplementedError: When not overriden in a subclass
    '''
    raise NotImplementedError
