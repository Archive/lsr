'''
Defines the base class for all Braille output devices.

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
import Base, AEConstants

class Braille(Base.AEOutput):
  ''' 
  Defines the base class for all Braille output devices. Provides default 
  implementation of L{parseString} specific to Braille devices.
  '''
  def getCapabilities(self):
    '''
    @return: 'braille' as the only capability of this device.
    @rtype: list of string
    '''
    return ['braille']  
  
  def getProxy(self):
    '''
    Looks at the L{USE_THREAD} flag to see if the device implementing this 
    interface wants a thread proxy or not.
    
    @return: self or L{ThreadProxy.BrailleThreadProxy}
    @rtype: L{AEOutput}
    '''
    if self.USE_THREAD == True:
      raise NotImplementedError
    elif self.USE_THREAD == False:
      return self
    else:
      raise NotImplementedError('USE_THREAD not specified')

  def parseString(self, text, style, por, sem):
    '''
    Dummy implementation returns the text as-is.

    @param text: Text to be parsed
    @type text: string
    @param style: Style object defining how the text should be parsed
    @type style: L{AEOutput.Style}
    @param por: Point of regard for the first character in the text, or None if
      the text is not associated with a POR
    @type por: L{POR}
    @param sem: Semantic tag for the text
    @type sem: integer
    @return: Parsed words
    @rtype: 3-tuple of lists of string, L{POR}, L{AEOutput.Style}
    '''
    return [(text, por, style)]
  
  def send(self, name, value, style=None):
    '''    
    Dispatches known commands to their appropriate handlers.
    
    @param name: Descriptor of the data value sent
    @type name: object
    @param value: Content value
    @type value: object
    @param style: Style with which this value should be output
    @type style: L{AEOutput.Style}
    @return: Return value specific to the given command
    @rtype: object
    @raise NotImplementedError: When the handler for a common command is not 
      implemented by a subclass
    '''
    if name == AEConstants.CMD_STOP:
      self.sendStop(style)
    elif name == AEConstants.CMD_TALK:
      self.sendTalk(style)
    elif name in (AEConstants.CMD_STRING, AEConstants.CMD_STRING_SYNC):
      self.sendString(value, style)
    elif name == AEConstants.CMD_FILENAME:
      # may not be implemented
      self.sendFilename(value, style)
    elif name == AEConstants.CMD_CARET:
      self.sendCaret(value, style)
    elif name == AEConstants.CMD_TRUNCATE:
      self.sendTruncate(value[0], value[1], style)
    elif name == AEConstants.CMD_GET_ELLIPSIS_SIZE:
      return self.sendGetEllipsisSizes(style)
    elif name == AEConstants.CMD_GET_MISSINGCELL_COUNT:
      return self.sendGetMissingCellCount()

      
  def sendCaret(self, pos, style):
    '''
    Sends the current caret position relative to the first cell (zero-offset) 
    on the device. The style object is used by the device in deciding how the
    caret should be presented.

    @param pos: Zero-offset cell position of the caret, up to the device to 
      change to one-offset if need be
    @type pos: string
    @param style: Style with which the caret should be indicated
    @type style: L{AEOutput.Style}
    @raise NotImplementedError: When not overridden in a subclass
    '''
    raise NotImplementedError
  
  def sendTruncate(self, left, right, style):
    '''
    Sends indicators of whether text was truncated on either side of the 
    current line or not. The style object is used by the device in deciding how
    the truncation should be presented.

    @param left: Was text truncated to the left?
    @type left: boolean
    @param right: Was text truncated to the right?
    @type right: boolean
    @param style: Style with which the truncation should be indicated
    @type style: L{AEOutput.Style}
    @raise NotImplementedError: When not overridden in a subclass
    '''
    raise NotImplementedError
  
  def sendGetEllipsisSizes(self, style):
    '''
    @param style: Style with which the ellipsis are defined
    @type style: L{AEOutput.Style}
    @return: tuple containing length of left and right ellipsis
    @rtype: tuple
    @raise NotImplementedError: When not overridden in a subclass
    '''
    raise NotImplementedError
   
  def sendGetEllipsisSizes(self, style):
    '''
    @param style: Style with which the ellipsis are defined
    @type style: L{AEOutput.Style}
    @return: tuple containing length of left and right ellipsis
    @rtype: tuple
    @raise NotImplementedError: When not overridden in a subclass
    '''
    raise NotImplementedError
  
  def sendGetEllipsisSizes(self, style):
    '''
    @param style: Style with which the ellipsis are defined
    @type style: L{AEOutput.Style}
    @return: tuple containing length of left and right ellipsis
    @rtype: tuple
    @raise NotImplementedError: When not overridden in a subclass
    '''
    raise NotImplementedError
  
  def sendGetMissingCellCount(self):
    '''
    @return: integer containing missing cell count
    @rtype: integer
    @raise NotImplementedError: When not overridden in a subclass
    '''
    raise NotImplementedError

  def sendString(self, text, style):
    '''
    Sends a string of one or more characters to the device. The style object
    is used by the device in deciding how the given text should be presented.

    @param text: Text to send to the device
    @type text: string
    @param style: Style with which this text should be output
    @type style: L{AEOutput.Style}
    @raise NotImplementedError: When not overridden in a subclass
    '''
    raise NotImplementedError
  
  def sendFilename(self, fn, style):
    '''
    Sends a string filename to the device, the contents of which should be 
    output. The style object is used by the device in decided how the given
    text should be presented.
    
    Typically, this method should be implemented by an audio device that 
    supports playback of waveform or sequencer files. It might also be used
    by devices as a way of synthesizing the entire contents of a file without
    having to pass all of the contents through the rest of the system.
    
    @param fn: Absolute filename
    @type fn: string
    @param style: Style with which this text should be output
    @type style: L{AEOutput.Style}
    @raise NotImplementedError: When not overridden in a subclass
    '''
    raise NotImplementedError

  def sendStop(self, style=None):
    '''
    Purges buffered text and styles, and interrupts on-going output.

    @param style: Style indicating which channel on which the stop should be 
      performed; None indicates stop on all channels
    @type style: L{AEOutput.Style}
    @raise NotImplementedError: When not overridden in a subclass
    '''
    raise NotImplementedError

  def sendTalk(self, style=None):
    '''
    Indicates all text buffered by L{sendString} should now be output. 
    For devices that do the buffering in the driver, this action may mean
    simply sending the command. For devices that do not buffer, this action 
    means sending text and styles buffered in the LSR device definition.

    @param style: Style indicating which channel on which the talk should be 
      performed; None indicates talk on all channels
    @type style: L{AEOutput.Style}
    @raise NotImplementedError: When not overridden in a subclass
    '''
    raise NotImplementedError
