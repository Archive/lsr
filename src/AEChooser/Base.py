'''
Defines the base class for all choosers in LSR.

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
import weakref, os.path
import AEEvent, UIElement

GLOBAL_SINGLETONS = weakref.WeakValueDictionary()
APP_SINGLETONS = {}

class AEChooser(UIElement.UIE):
  '''  
  Most abstract base class for all L{AEChooser} dialogs.
  
  This class is abstract as most of its methods raise NotImplementedError and 
  need to be overriden in subclasses.
  
  @cvar GLOBAL_SINGLETON: Allow only one instance of a chooser to exist at a
    time globally? This flag should be overriden by a subclass to indicate
    whether the chooser is a global singleton or not.
  @type GLOBAL_SINGLETON: boolean
  @cvar APP_SINGLETON: Allow only one instance of a chooser to exist at a 
    time for a given application? This flag should be overriden by a subclass 
    to indicate whether the chooser is an application singleton or not.
    Meaningless if L{GLOBAL_SINGLETON} is set.
  @type APP_SINGLETON: boolean
  @cvar OK: Indicates the chooser is completing and its options should be
    applied
  @type OK: integer
  @cvar CANCEL: Indicates the chooser is canceling and its options should be
    ignored
  @type CANCEL: integer
  @cvar APPLY: Indicates the chooser options should be applied immediately 
    with no changes to its state
  @type APPLY: integer
  @ivar event_man: Reference to the event manager to which an event will be
    posted when the user indicates the dialog is completed, cancelled, or
    the current optionss applied
  @type event_man: L{EventManager}
  @ivar aid: Unique identifier for the application L{Tier} with which the 
    L{AEChooser} that fired this event is associated
  @type aid: opaque
  @ivar block_signals: Blocks future signals from being sent by the L{_signal}
    method after a L{OK} or L{CANCEL} signal is sent
  @type block_signals: boolean
  '''
  GLOBAL_SINGLETON = False
  APP_SINGLETON = False
  # these must be mutually exclusive from the signal constants used by subclass
  CANCEL = -1000
  APPLY = -1001
  OK = -1002
  
  def __init__(self):
    '''
    Initializes the L{event_man} and L{aid} variables to None and sets the 
    L{block_signals} flag to False.
    '''
    self.aid = None
    self.event_man = None
    self.block_signals = False
    
  def __call__(self, event_man, aid):
    '''    
    Stores a reference the L{EventManager} that will be notified by L{_signal}
    and a reference to the L{Tier} in which the L{Perk} associated with this
    L{AEChooser} is loaded.
    
    @param event_man: Reference to the event manager to which an event will be
      posted when the user indicates the dialog is completed, cancelled, or
      the current options applied
    @type event_man: L{EventManager}
    @param aid: Unique identifier for the application L{Tier} with which the 
      L{AEChooser} that fired this event is associated
    @type aid: L{Tier}
    @raise ValueError: When the L{GLOBAL_SINGLETON} flag is set to True and a 
      singleton already exists or L{APP_SINGLETON} flag is set to True and a
      chooser for the application already exists.
    '''
    cls = self.getClassName()
    
    if self.GLOBAL_SINGLETON and cls in GLOBAL_SINGLETONS:
      raise ValueError(GLOBAL_SINGLETONS[cls])
    elif (self.APP_SINGLETON and 
          APP_SINGLETONS.has_key(cls) and 
          aid in APP_SINGLETONS[cls]):
      raise ValueError(APP_SINGLETONS[cls][aid])
    
    self.aid = aid
    self.event_man = event_man
    # store a weakref for singletons
    if self.GLOBAL_SINGLETON:
      GLOBAL_SINGLETONS[cls] = self
    # store a WeakValueDictionary
    elif self.APP_SINGLETON:
      try:
        APP_SINGLETONS[cls][aid] = self
      except KeyError:
        APP_SINGLETONS[cls] = weakref.WeakValueDictionary({aid:self})
          
  def init(self, **kwargs):
    '''
    Initializes the chooser. Should enable the chooser for interaction with the
    user.
    
    @raise NotImplementedError: When not overridden by a subclass
    '''
    raise NotImplementedError
  
  def activate(self, **kwargs):
    '''
    Activates the chooser. Called when a singleton instance of the chooser
    already exists, but an attempt was just made to create another. Does 
    nothing by default. Could be used to, for instance, raise a window to the
    foreground.
    '''
    pass
    
  def close(self):
    '''
    Closes the chooser. Should prevent further chooser interaction with the 
    user.
    
    @raise NotImplementedError: When not overridden by a subclass
    '''
    raise NotImplementedError
  
  def update(self, **kwargs):
    '''
    Does an update of some aspect of the L{AEChooser}. A subclass can override
    this method to support updates from observers of L{AEEvent.ChooserChange}
    events.
    
    @param kwargs: Arbitrary data given by the observer. The L{AEChooser}
      implementor should strong-name keyword params of interest.
    @type kwargs: dictionary
    '''
    pass
  
  def _getResource(self, name):
    '''
    Gets the absolute path to a file located in the same directory as this
    L{AEChooser}. Useful for locating resources like glade files.
    
    @return: Path to a resource file in the same directory as the chooser
    @rtype: string
    '''
    return os.path.join(self.getPath(), name)

  def _signal(self, kind, **kwargs):
    '''        
    Posts an L{AEEvent.ChooserChange} event to the L{EventManager}. Chooser
    change events should have an arbitrary kind or one of the special L{OK},
    L{APPLY}, L{CANCEL} valus. Any keyword arguments will be delivered to the
    observer. The event will also include a reference to this L{AEChooser} such
    that an observer can call the L{update} method on it. The event is 
    delivered only to the L{Perk} responsible for managing this chooser.
    
    After one cancel or one OK signal is fired, no more signals are sent.
    
    @param kind: Kind of signal, one of L{OK}, L{APPLY}, L{CANCEL} or a chooser
      defined integer constant
    @type kind: integer
    @param kwargs: Arbitrary data to be delivered with the event
    @type kwargs: dictionary
    '''
    if self.block_signals:
      return
    self.event_man.postEvents(AEEvent.ChooserChange(self.aid, self, kind, 
                                                    **kwargs))
    if kind == self.CANCEL or kind == self.OK:
      self.block_signals = True
