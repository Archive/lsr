'''
Defines the abstract base class for all L{AEMonitor} subclasses.

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
import UIElement

class AEMonitor(UIElement.UIE):
  '''  
  Most abstract base class for all L{AEMonitor} displays.
  
  This class is abstract as most of its methods raise NotImplementedError and 
  need to be overriden in subclasses.
  
  @ivar is_init: Is the monitor initialized via the L{init} method?
  @type is_init: boolean
  '''
  def __init__(self):
    '''
    Sets the L{is_init} flag to False.
    '''
    self.is_init = False
  
  def isInitialized(self):
    '''
    Gets if the monitor is initialized or not.
    
    @return: Is the dialog initialized?
    @rtype: boolean
    '''
    return self.is_init
  
  def init(self):
    '''
    Sets the L{is_init} flag to True. Subclasses should override this method
    to create and show their monitor's UI.
    '''
    self.is_init = True
    
  def close(self):
    '''
    Sets the L{is_init} flag to False. Subclasses should override this method 
    to hide and destroy their monitor's UI.
    '''
    self.is_init = False
  
  def getEventType(self):
    '''
    Abstract method. Gets the base class indicating the type of event of 
    interest to this monitor.
    
    @return: Base type of the event this monitor should buffer
    @rtype: class
    @raise NotImplementedError: When this method is not overidden in a subclass
    '''
    raise NotImplementedError
  
  def getEventNames(self):
    '''
    Abstract method. Gets names identifying all possible kinds of events to be
    monitored. The names are used to determine which events are actually 
    buffered once received by the monitor and which are ignored.
    
    @return: All event kinds to potentially buffer
    @rtype: list of string
    @raise NotImplementedError: When this method is not overidden in a subclass
    '''
    raise NotImplementedError
  
  def getEventDefaults(self):
    '''
    Abstract method. Gets the names of events returned by L{getEventNames} that
    will be buffered by default.
    
    @return: Default event kinds to buffer
    @rtype: list of string
    @raise NotImplementedError: When this method is not overidden in a subclass
    '''
    raise NotImplementedError
  
  def show(self, **kwargs):
    '''
    Abstract method. Buffers the given key word arguments. It is up to the
    monitor that implements this method to decide how to interpret and show the
    given data.
    
    @param kwargs: Information to buffer
    @type kwargs: dictionary
    @raise IOError: When the monitor is no longer accepting data to buffer 
    @raise NotImplementedError: When this method is not overidden in a subclass
    '''
    raise NotImplementedError