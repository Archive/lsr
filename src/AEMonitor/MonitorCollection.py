'''
Defines a convenience collection that can be used to notify all L{AEMonitor}s
of a certain event.

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
import logging
import Base
from AEInterfaces import implements

log = logging.getLogger('Monitor')

class MonitorCollection(object):
  '''
  Collection of known monitors. Defines methods for informing all elements about
  about information to be buffered.
  
  @ivar monitors: list of L{AEMonitor}s that will be notified of events
  @type monitors: list of L{AEMonitor}s
  ''' 
  def __init__(self):
    '''Initializes the monitor list.'''
    self.monitors = []

  def __iter__(self):
    '''
    @return: Iterator over the list of L{AEMonitor}s
    @rtype: iterator
    '''
    return iter(self.monitors)
  
  def __len__(self):
    '''
    @return: Number of monitors in the collection
    @rtype: integer
    '''
    return len(self.monitors)
  
  def __contains__(self, val):
    '''
    Checks if the given monitor class name or instance is in the monitor
    collection.
    
    @param val: Instance of a monitor or a string representing its class name
    @type val: string or L{AEMonitor}
    @return: Is the monitor in the collection?
    @rtype: boolean
    '''
    if isinstance(val, str):
      for mon in self.monitors:
        if mon.getClassName() == val:
          return True
      return False
    else:
      return val in self.monitors
    
  def clear(self):
    '''Closes all monitors in L{monitors}.'''
    for m in self.monitors:
      try:
        m.close()
      except Exception, e:
        pass
      
  def removeByClassName(self, name):
    '''
    Removes one L{AEMonitor} instance given its class name from the collection.
    
    @param name: Class name of the monitor to remove
    @type name: string
    '''
    for m in self.monitors:
      if m.getClassName() == name:
        m.close()
        # only one possible based on UIE naming convention, so this is safe
        self.monitors.remove(m)
        return

  def remove(self, *mons):
    '''
    Removes one or more L{AEMonitor} instances from the collection.
    
    @param mons: Monitors to remove
    @type mons: list of L{AEMonitor}
    '''
    for mon in mons:
      self.monitors.remove(mon)
      try:
        mon.close()
      except Exception:
        pass
    
  def add(self, kind, monitors):
    ''' 
    Adds one or more L{AEMonitor}s to the list of monitors to be notified.
    Filters the list of monitors based on the kind of events in which they are 
    interested.
    
    @param kind: Kind of event of interest to the monitor
    @type kind: class
    @param monitors: L{AEMonitor}s to notify
    @type monitors: tuple of L{AEMonitor}s
    '''
    for m in monitors:
      if implements(m, Base.AEMonitor) and (implements(m.getEventType(), kind) 
                                        or implements(kind, m.getEventType())):
        # store only the desired kind of monitors
        self.monitors.append(m)
        # make sure the monitor isn't already initialized
        if not m.isInitialized():
          # initialize each monitor so it is displayed
          m.init()

  def show(self, *args, **kwargs):
    '''
    Informs L{AEMonitor}s added via L{add} of information to be buffered. Calls
    L{Base.AEMonitor.show} on all monitors. Monitors raising IOError in show 
    are removed from the L{monitors} list and are no longer notified of events.
    
    @param args: Information to buffer
    @type args: list
    @param kwargs: Information to buffer
    @type kwargs: dictionary
    '''
    i = 0
    # can't use a for loop here because we're removing elements along the way
    while i < len(self.monitors):
      m = self.monitors[i]
      try:
        # buffer the given information
        m.show(*args, **kwargs)
      except IOError:
        # remove any monitors that have indicated that want no more events
        del self.monitors[i]
        continue
      except Exception:
        # log all exceptions other than IO
        log.exception('Monitor exception')
      i += 1