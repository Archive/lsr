'''
Defines a L{Perk} class that can register L{Task}s which will be executed in
response to L{AEEvent}s and L{AEInput.Gesture}s.

@author: Peter Parente
@author: Pete Brunet
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
import logging, weakref, os, sys
import Task, AEInput, AEEvent, AEInterfaces, UIElement, AEConstants
from i18n import _
from AEState import AEState as PerkState

log = logging.getLogger('Perk')

def _postTimerEvent(interval, tid, aid, wtask, weman):
  '''
  Callback for timer events. Tries to post an L{AEEvent} for the L{Task}
  intended to handle the event if the L{Task} is still alive.
  
  @param interval: Interval in seconds on which the timer fires
  @type interval: interval
  @param tid: ID of the L{Task} to execute in response to the 
    L{AEEvent.TimerAlert}
  @type tid: integer
  @param wtask: Weak reference to the L{Task} to execute. Used to determine if
    the L{Task} is still alive. If not, the timer is unregistered.
  @type wtask: weakref.ref
  @param weman: Weak reference to the L{EventManager} to use to post 
    L{AEEvent.TimerAlert} events.
  @type weman: weakref.proxy
  @return: Continue timer alerts on the interval (True) or stop them (False).
  @rtype: boolean
  '''
  if not wtask():
    return False
  try:
    weman.postEvents(AEEvent.TimerAlert(aid, tid, interval))
  except ReferenceError:
    return False
  return True

class Perk(Task.Tools.All, UIElement.UIE):
  '''    
  Registers and maintains a collection of L{Task}s that will execute in
  response to L{AccessEngine} events and commands (L{AEEvent}s and
  L{AEInput.Gesture}s.). This class should be sublclassed by L{Perk} developers
  who wish to define, modify, or extend the LSR user interface. This class
  derives from L{Task.Tools.All} to allow the use of the convenience methods
  defined in that class during L{Perk} intialization.
  
  @cvar STATE: Class to instantiate and use to store state information across
    all instances of this L{Perk}. Defaults to the L{AEState} base class, but
    can be overridden with a subclass of L{AEState}.
  @type STATE: L{AEState.AEState} class
  @ivar named_tasks: L{Task}s registered by name
  @type named_tasks: dictionary string : Task instance
  @ivar keyed_tasks: L{Task}s registered by unique integer ID
  @type keyed_tasks: dictionary integer : Task instance
  @ivar event_tasks: L{Task}s registered by type
  @type event_tasks: dictionary of (Task subclass : list of Task instance)
  @ivar commands: Name of a L{Task} to execute in response to a 
    L{AEInput.GestureList} sequence on some L{AEInput} device. The name should
    be hashed into the L{named_tasks} dictionary to fully resolve the L{Task}.
  @type commands: weakref.WeakValueDictionary of (GestureList : string)
  @ivar state: Settings for this L{Perk}. Defaults to an empty L{PerkState} 
    object that can be used to store state information that is not 
    configurable or persistable unless L{STATE} is specified.
  @type state: L{PerkState}
  @ivar out_caps: Capabilities set by L{setIdealOutput} as ideal for output
    from this L{Perk}. These are used to "late bind" a device for use during
    execution of this L{Perk} and its L{Task}s. Defaults to audio.
  @type out_caps: list of string
  @ivar before_chains: Mapping from target L{Task} name to a list of L{Task}
    names which should be executed before the target
  @type before_chains: dictionary of {string : list of string}
  @ivar after_chains: Mapping from target L{Task} name to a list of L{Task}
    names which should be executed after the target
  @type after_chains: dictionary of {string : list of string}
  @ivar around_chains: Mapping from target L{Task} name to one L{Task} name 
    which should be executed instead of the target
  @type around_chains: dictionary of {string : list of string}
  @ivar registered_modifiers: Mapping from device to a registered modifier for
  that device
  @type registered_modifiers: weakref.WeakKeyDictionary
  '''
  STATE = PerkState
  
  def __init__(self, *args, **kwargs):
    '''
    Creates empty dictionaries for named, event, command, and chooser L{Task}s.
    Create a state object for storage using the class in L{STATE} in case the
    L{Perk} hasn't previously persisted its settings on disk.

    @note: L{Perk} writers should override L{init} to do initialization, not
    this method.
    '''
    super(Perk, self).__init__(*args, **kwargs)
    self.named_tasks = {}
    self.keyed_tasks = {}
    self.event_tasks = {}
    self.commands = {}
    # default to audio output
    self.out_caps = ['audio']
    # define empty chains for tasks
    self.before_chains = {}
    self.after_chains = {}
    self.around_chains = {}
    self.registered_modifiers = weakref.WeakKeyDictionary()

    # instantiate an instance of the STATE class; it may be filled with data
    # when preInit is called with state already persisted to disk
    self.state = self.STATE()
    # initialize state settings
    self.state.init()
    
  def preInit(self, ae, *args, **kwargs):
    '''    
    Overrides pre-initialization to load persisted state from disk. Fetches the
    managers from the L{AccessEngine} first so that state can be loaded using
    the L{SettingsManager}. Loads the state, then invokes the super class
    version of this method to finish initialization.

    @note: L{Perk} writers should override L{init} to do initialization, not
    this method.    
    @param ae: Reference to AccessEngine for event context
    @type ae: L{AccessEngine}
    '''
    # get managers, this duplicates some work done in the super class, but
    # it's harmless and only done once per Perk
    ae.loanManagers(self)
    try:
      # use the settings manager to try to load persisted state for this Perk
      self.state = self.sett_man.loadState(self.getClassName(), self.state)
    except KeyError:
      # do nothing, a blank state object has already been created
      pass
    # invoke the super class to complete initialization
    super(Perk, self).preInit(ae, *args, **kwargs)
  
  def postClose(self):
    '''
    Frees all L{Task}s managed by this L{Perk}.

    @note: L{Perk} writers should override L{close} to do finialization, not
    this method.
    '''
    # let the tools postClose run second
    super(Perk, self).postClose()
    # finalize all Tasks
    for tasks in self.event_tasks.values():
      map(Task.Task.close, tasks)
      map(Task.Task.postClose, tasks)
    for tasks in (self.named_tasks.values(), self.keyed_tasks.values()):
      map(Task.Task.close, tasks)
      map(Task.Task.postClose, tasks)
    # unregister modifiers
    for dev, codes in self.registered_modifiers.iteritems():
      self.unregisterModifiers(dev, codes)
      
    # throw away all references
    self.named_tasks = {}
    self.keyed_tasks = {}
    self.event_tasks = {}
    self.commands = {}
    self.registered_modifiers = weakref.WeakKeyDictionary()
    
  def setIdealOutput(self, *capabilities):
    '''
    Sets the ideal device capabilities to be used to locate a device for all 
    output methods invoked from the caller's L{Perk} module, including the 
    Perk class itself and all of its registered L{Task}s. The capabilities list
    should include strings naming L{AEOutput} interfaces ("audio" and/or 
    "braille") at present.
    
    @param capabilities: Names of capabilities required on the device
    @type capabilities: list of string    
    '''
    self.out_caps = capabilities
    
  def getIdealOutput(self):
    '''        
    Gets the ideal device capabilities to locate a device to be used by all
    output methods invoked from the caller's L{Perk} module, including the Perk
    class itself and all of its registered L{Task}s.
    
    @return: Names of capabilities set as ideal for a default output device
    @rtype: list of string
    '''
    return self.out_caps
  
  def getState(self):
    '''
    @return: Returns the settings for this L{Perk} or a dummy object that can
      be used to store state information that is neither user configurable or 
      persistable
    @rtype: L{AEState.AEState}
    '''
    return self.state

  def _initTask(self, task):
    '''
    Initializes a L{Task} by calling L{Task.Base.Task.preInit} and 
    L{Task.Base.Task.init} if it has not already been initialized.
    
    @param task: L{Task} to initialize
    @type task: L{Task}
    '''
    if task.preInit(self.acc_eng, self.tier, self):
      task.init()

  def registerTimerTask(self, task, interval):
    '''
    Registers a L{Task} to be called on a set interval.
    
    @param interval: Interval in seconds which the L{Task} will be notified
    @type interval: integer
    '''
    tid = id(task)
    aid = self.tier.getIdentity()
    wtask = weakref.ref(task)
    weman = weakref.proxy(self.event_man)
    self._initTask(task)
    self.keyed_tasks[tid] = task
    # use the access engine timer to notify our callback
    self.acc_eng.addTimer(_postTimerEvent, interval, interval, tid, aid,
                          wtask, weman)
    self.tier.addTaskRef(tid, self)
  
  def registerCommandTask(self, device, codes, name, propagate=False):
    '''
    Registers a L{Task} in this L{Tier} to be executed in response to an 
    L{AEEvent} indicating that the given action codes were input on the given 
    L{AEInput} device.
    
    @param device: Input device to monitor
    @type device: L{AEInput}
    @param codes: List of lists of action codes forming the L{AEInput.Gesture} 
      that will trigger the execution of the named L{Task}. For example, 
      codes=[[Keyboard.AEK_CTRL, Keyboard.AEK_TILDE]] indicates the single
      gesture of simultaneously pressing Ctrl and ~ on the keyboard device.
    @type codes: list of list of integer
    @param name: Name of the L{Task} registered via L{registerNamedTask} to 
      execute when the input gesture is detected on the device
    @type name: string
    @param propagate: Should the input gesture be allowed to propagate to the
      OS after we receive it?
    @type propagate: boolean
    @raise ValueError: When a L{Task} with the given name is not registered
    '''
    # construct the key list and find the Task with the given name
    gl = AEInput.GestureList(device, codes)
    if self.tier.getCommandTask(gl) is not None:
      # raise a value error if there is already a command registered for this
      # sequence of keys in this Tier
      raise ValueError(
        'command already registered with action codes %s on device %s' % 
        (codes, device.getName()))
    # add key list to our dictionary of commands and store the task to execute
    # as its value
    self.commands[gl] = name
    self.tier.addTaskRef(gl, self)
    if not propagate:
      # try to add a filter to the device, ignore if not supported
      try:
        device.addFilter(gl)
      except NotImplementedError:
        pass
    # accept a KEY_CMD from the device, ignore if not supported  
    try:
      device.addKeyCmd(codes)
    except NotImplementedError:
      pass
      
  def registerChooserTask(self, chooser, task):
    '''
    Registers a L{Task} with the given name to be executed in response to a 
    change in the given L{AEChooser}.
    
    @param chooser: Chooser that the L{Task} should observe
    @type chooser: L{AEChooser}
    @param task: Instance of a L{Task} that should observe the chooser
    @type task: L{Task.ChooserTask}
    '''
    cid = id(chooser)
    if self.tier.getKeyedTask(cid) is not None:
      raise ValueError('Chooser Task already registered')
    self._initTask(task)
    self.keyed_tasks[cid] = task
    self.tier.addTaskRef(cid, self)
    
  def registerNamedTask(self, task, name):
    '''
    Registers a new L{Task} under the given name if no L{Task} is already 
    registered with that name in this L{Tier}.
    
    Only one L{Task} can be registered under a name in a L{Tier}. If a L{Task}
    is already registered under the given name, any other registration with
    that name is ignored.
    
    @param task: L{Task} to register
    @type task: L{Task.Base.Task}
    @param name: Name to associate with the L{Task}
    @type name: string
    @raise ValueError: When a L{Task} with the given name is already registered
      in this L{Tier}
    '''
    if self.tier.getNamedTask(name) is None:
      # initialize the Task
      self._initTask(task)
      self.named_tasks[name] = task
      self.tier.addTaskRef(name, self)
    else:
      raise ValueError(name)
    
  def registerEventTask(self, task, focus=False, tier=False, background=False):
    '''
    Registers a new L{Task} in this L{Perk} under the L{Task} type. The type
    determines which kind of L{AEEvent} will trigger the execution of the
    registered L{Task}. If one or more Tasks are already registered for this 
    type, the given L{Task} will be inserted at the top of the registered stack
    of L{Task}s (i.e. it will be executed first for the appropriate event).
    
    The L{focus}, L{tier}, and L{background} parameters specify on which layer
    the L{Task} will handle events. If focus is True, the L{Task} will be
    executed in response to an event from a focused control within this
    L{Tier}. If tier is True, the L{Task} will be executed in response to an
    event from an unfocused control within this L{Tier}. If background is True,
    the L{Task} will be executed in response to an event from any control
    within the tier when the L{Tier} is not active.
    
    The three layers are mutually exclusive. You may set any combination of
    focus, tier, and background to True to register the given L{task} on each
    selected layer in one call. If all three parameters are False, the 
    registration defaults to the focus layer.
    
    The L{Task} passed to this method must implement the interface defined by
    L{Task.EventTask}.

    @param task: L{Task} to register
    @type task: L{Task.Base.Task}
    @param focus: Should this L{Task} handle events from focused accessibles in 
      this L{Tier}?
    @type focus: boolean
    @param tier: Should this L{Task} handle events from unfocused accessibles
      in this L{Tier}?
    @type tier: boolean
    @param background: Should this L{Task} handle events from any accessible in 
      this L{Tier} when the L{Tier} is inactive?
    @type background: boolean
    @raise AttributeError: When the L{Task} does not implement 
      L{Task.EventTask}
    @raise NotImplementedError: When the L{Task} does not properly subclass
      L{Task.EventTask}
    '''
    # default to the focus layer if no layer is specified    
    default = not (focus or tier or background)
    d = {AEConstants.LAYER_FOCUS : focus or default, 
         AEConstants.LAYER_TIER : tier, 
         AEConstants.LAYER_BACKGROUND : background}
    # get the kind of AEEvent the Task 
    kind = task.getEventType()
    # initialize the Task
    self._initTask(task)
    # opt: inform the Tier of the kind of event that is desired by this Perk
    try:
      self.tier.setEventInterest(kind, True)
    except KeyError:
      # ignore unknown event types, could be internal
      pass
    for layer, val in d.items():
      if val:
        # get the list of Task registered for this type and layer or insert an
        # empty list and get it instead
        curr = self.event_tasks.setdefault((kind, layer), [])
        # insert the task at the front of the list
        curr.insert(0, task)
        
  def unregisterTimerTask(self, task):
    '''
    Unregisters a L{Task} from being called on a set interval.
    
    @param task: L{Task} to unregister
    @type task: L{Task}
    @raise KeyError: When the L{Task} is not registered
    '''
    tid = id(task)
    del self.keyed_tasks[tid]
    # let the task finalize itself
    task.close()
    task.postClose()
    self.tier.removeTaskRef(tid)

  def unregisterCommandTask(self, device, codes):
    '''
    Unregisters a L{Task} set to execute in response to the given action codes 
    on the given device B{from this L{Perk} only}.
    
    @param device: Input device to monitor
    @type device: L{AEInput}
    @param codes: List of lists of action codes forming the L{AEInput.Gesture} 
      that will trigger the execution of the named L{Task}. For example, 
      codes=[[Keyboard.AEK_CTRL, Keyboard.AEK_TILDE]] indicates the single
      gesture of simultaneously pressing Ctrl and ~ on the keyboard device.
    @type codes:  list of list of integer
    @raise KeyError: When a L{AEInput.GestureList} is not registered
    '''
    # GestureList has __eq__ and __hash__ overridden so two different instances
    # can hash to the same dictionary location as long as their devices and
    # codes are the same
    gl = AEInput.GestureList(device, codes)
    del self.commands[gl]
    self.tier.removeTaskRef(gl)
    # try to remove a filter from the device, ignore if not supported
    try:
      device.removeFilter(gl)
    except (NotImplementedError, AttributeError):
      pass
    
    # remove an accepted KEY_CMD from the device, ignore if not supported
    try:
      device.removeKeyCmd(codes)
    except NotImplementedError:
      pass
    
  def unregisterChooserTask(self, chooser):
    '''    
    Unregisters a L{Task} so it is no longer executed in response to a change
    in the given L{AEChooser}.
    
    @param chooser: Chooser that the L{Task} should no longer observe
    @type chooser: L{AEChooser}
    @raise KeyError: When the given L{AEChooser} is not registered
    '''
    cid = id(chooser)
    task = self.keyed_tasks[cid]
    del self.keyed_tasks[cid]
    
    # We don't want to close the Task because technically it's not getting
    # unregistered, just decoupled from the chooser it was managing. The Task
    # must stay registered so that the chooser can be invoked again in the 
    # future. Do not call Task.close() here!
    
    self.tier.removeTaskRef(cid)

  def unregisterNamedTask(self, name):
    '''
    Unregisters a named L{Task} B{from this L{Perk} only}. If a Task with the 
    given name is not found in this L{Perk}, an exception is raised.
    
    @param name: Name of the L{Task} to unregister
    @type name: string
    @raise KeyError: When a L{Task} with the given name is not registered
    '''
    task = self.named_tasks[name]
    del self.named_tasks[name]
    # let the task finalize itself
    task.close()
    task.postClose()
    self.tier.removeTaskRef(name)
    
  def unregisterEventTask(self, task, focus=False, tier=False, 
                          background=False):
    '''
    Unregisters the given L{Task} instance B{from this L{Perk} only}. If the 
    given L{Task} instance was not registered for an event in this L{Perk}, an 
    exception is raised. The L{focus}, L{tier}, and L{background} parameters
    state from which layer(s) this L{Task} should be unregistered.
    
    The L{Task} passed to this method must implement the interface defined by
    L{Task.EventTask}.
    
    @param task: L{Task} to unregister
    @type task: L{Task.Base.Task}
    @raise KeyError: When there are no L{Task}s registered with the type of the 
        given L{Task}
    @raise ValueError: When the given L{Task} is not registered on one of the
      specified layers
    @raise AttributeError: When the L{Task} does not implement 
      L{Task.EventTask}
    @raise NotImplementedError: When the L{Task} does not properly subclass
      L{Task.EventTask}
    @see: L{registerEventTask}
    '''
    # default to the focus layer if no layer is specified    
    default = not (focus or tier or background)
    d = {AEConstants.LAYER_FOCUS : focus or default, AEConstants.LAYER_TIER : tier, 
         AEConstants.LAYER_BACKGROUND : background}
    # get the task type
    kind = task.getEventType()
    # let the Task finalize itself
    task.close()
    task.postClose()
    # opt: inform the Tier of the kind of event that is no longer
    # desired by this Perk
    try:
      self.tier.setEventInterest(kind, False)
    except KeyError:
      # ignore unknown event types, could be internal
      pass
    for layer, val in d.items():
      if val:
        # get the list of Task registered for this type and layer or insert an
        # empty list and get it instead
        curr = self.event_tasks[(kind, layer)]
        curr.remove(task)
    
  def getCommandTask(self, gesture_list):
    '''
    Gets the L{Task} registered B{in this L{Perk} only} to execute in response
    to the given L{AEInput.GestureList}.
    
    Called by L{Tier.Tier.getCommandTask} during a search through all L{Perk}s
    in the owning L{Tier} for the given key list. 
    
    @param gesture_list: Gestures and device on which they were performed
    @type gesture_list: L{AEInput.GestureList}
    @return: Name of the L{Task} set to execute in response to the input 
      gesture or None
    @rtype: string
    '''
    return self.commands.get(gesture_list)
  
  def getKeyedTask(self, key):
    '''
    Gets a L{Task} registered under a particular key set to execute in response
    to events. None is returned if not found.
    
    Called by L{Tier.Tier.getKeyedTask} during a search through all L{Perk}s
    in the owning L{Tier} for the given L{AEChooser}.
    
    @param key: Unique key identifying this L{Task}
    @type key: integer
    @return: L{Task} set to execute in response to the chooser or None
    @rtype: L{Task.ChooserTask.ChooserTask}
    '''
    return self.keyed_tasks.get(key)
    
  def getNamedTask(self, name):
    '''
    Gets the L{Task} with the given name if it is registered B{in this L{Perk}
    only}. If no L{Task} is registered under the given name in this L{Perk}, 
    returns None.
    
    Called by L{Tier.Tier.getNamedTask} during a search through all
    L{Perk}s in the owning L{Tier} for the given name.
    
    @param name: Name of the L{Task} to locate
    @type name: string
    @return: L{Task} with the given name or None
    @rtype: L{Task.Base.Task}
    '''
    return self.named_tasks.get(name)
    
  def getEventTasks(self, event_type, task_layer):
    '''
    Get all registered L{Task}s registered to handle the given L{AEEvent} type 
    B{in this L{Perk} only}. Makes a copy of the list of tasks such that 
    registering or unregistering L{Task}s does not affect the list gotten
    by this method.
    
    Called by L{Tier.Tier.getEventTasks} during a search through all
    L{Perk}s in the owning L{Tier} for the given task type.
    
    @param event_type: Desired type of L{AEEvent}
    @type event_type: L{AEEvent} class
    @param task_layer: Layer on which the desired L{Task}s are registered
    @type task_layer: integer
    @return: List of all L{Task}s that handle the given event type on the given
      layer in this L{Perk}
    @rtype: list of L{Task.Base.Task}
    '''
    return list(self.event_tasks.get((event_type, task_layer), []))
  
  def getChainSegment(self, target, link):
    '''
    Gets a copy of the segment of the L{Task} chain for target specified by the
    link type. A copy is returned to avoid iteration errors if the list changes
    size during execution of the L{Task}s in the chain.
    
    @param target: Name of the L{Task} to link to
    @type target: string
    @param link: One of the CHAIN constants in L{AEConstants.Tools} 
    @type link: integer
    @return: L{Task} names chained in the given manner
    @rtype: list of string
    '''
    if link == AEConstants.CHAIN_BEFORE:
      return self.before_chains.get(target, [])[:]
    elif link == AEConstants.CHAIN_AFTER:
      return self.after_chains.get(target, [])[:]
    elif link == AEConstants.CHAIN_AROUND:
      alist = []
      if self.around_chains.has_key(target):
        alist.append(self.around_chains[target])
      return alist
    
  def mergeChain(self, target, before, after):
    '''
    Adds the before and after segments of the chain for the target specified.
    The parameters before and after are lists, possibly containing segments 
    from other L{Perk}s. These lists are modified in place for performance.
    The around segment is returned since it is a single value.

    @param target: Name of the L{Task} to link to
    @type target: string
    @return: L{Task} name for around link type, or None if nothing is linked
      around the target
    @rtype: string
    '''
    try:
      before.extend(self.before_chains[target])
    except KeyError:
      pass
    try:
      after.extend(self.after_chains[target])
    except KeyError:
      pass
    return self.around_chains.get(target, None)
  
  def linkTaskToChain(self, name, link, target):
    '''
    Links a L{Task} to the one named in target. The L{Task} will be added 
    either before, after, or around depending on the value of link. Does not
    allow a L{Task} to link to itself (i.e. name cannot equal target).
    
    Invokes L{Tier.Tier.addChainRef} to add a reference to this L{Perk} to the
    L{Tier} which will later have to traverse the chain across L{Perk}s.

    @param name: Name of the L{Task} to link
    @type name: string
    @param link: One of the CHAIN constants in L{AEConstants.Tools} 
    @type link: integer
    @param target: Name of the L{Task} to link to
    @type target: string
    @raise ValueError: When the link type is unknown
    @raise AssertionError: When the name equals the target
    '''
    assert name != target
    if link == AEConstants.CHAIN_BEFORE:
      self.before_chains.setdefault(target, []).append(name)
    elif link == AEConstants.CHAIN_AFTER:
      self.after_chains.setdefault(target, []).append(name)
    elif link == AEConstants.CHAIN_AROUND:
      self.around_chains[target] = name
    else:
      raise ValueError(link)
    # add a reference to this Perk to the Tier
    self.tier.addChainRef(target, self)
      
  def unlinkTaskFromChain(self, name, link, target):
    '''
    Unlinks a L{Task} from the one named in target. The L{Task} will be
    unlinked either before, after, or around depending on the value of link. 
    If link is None, the L{Task} will be unlinked from all cases if
    possible.
    
    Invokes L{Tier.Tier.removeChainRef} to delete a reference to this L{Perk}
    to the L{Tier} which will later have to traverse the chain across L{Perk}s.

    @param name: Name of the L{Task} to link
    @type name: string
    @param link: One of the CHAIN constants in L{AEConstants.Tools} 
    @type link: integer
    @param target: Name of the L{Task} to unlink from
    @type target: string
    @raise ValueError: When the named L{Task} is not unlinked at least once or
      the link type is unknown
    @raise KeyError: When the target L{Task} does not have a chain segement in
      this L{Perk} for the given link type
    '''
    if link is not None:
      if link == AEConstants.CHAIN_BEFORE:
        self.before_chains[target].remove(name)
        if len(self.before_chains[target]) == 0:
          del self.before_chains[target]
      elif link == AEConstants.CHAIN_AFTER:
        self.after_chains[target].remove(name)
        if len(self.after_chains[target]) == 0:
          del self.after_chains[target]
      elif link == AEConstants.CHAIN_AROUND:
        # don't unlink around unless it matches
        if self.around_chains[target] == name:
          del self.around_chains[target]
        else:
          raise ValueError(name)
      else:
        raise ValueError(link)
    else:
      # track how many removals we make
      count = 0
      # try removing from all chain segments
      try:
        self.before_chains[target].remove(name)
        if len(self.before_chains[target]) == 0:
          del self.before_chains[target]
        count += 1
      except (KeyError, ValueError):
        pass
      try:
        self.after_chains[target].remove(name)
        if len(self.after_chains[target]) == 0:
          del self.after_chains[target]
        count += 1
      except (KeyError, ValueError):
        pass
      try:
        if self.around_chains[target] == name:
          # there is only one so get rid of key and value
          del self.around_chains[target]
          count += 1
      except (KeyError, ValueError):
        pass

      if count == 0:
        # error if we didn't remove anything
        raise ValueError
    # remove reference in tier if they are no longer needed
    if not self.around_chains.has_key(target) \
               and not self.before_chains.has_key(target) \
               and not self.after_chains.has_key(target) :
      self.tier.removeChainRef(target, self)
        
  def getTaskNames(self):
    '''
    Gets the names of all L{Task}s registered in this L{Perk}.
    
    @return: All names
    @rtype: list of string
    '''
    return self.named_tasks.keys()
  
  def unregisterModifiers(self, dev, modifiers):
    '''
    Unregisters a list of input modifiers. Ignores the error cases when the
    device has no registered modifiers or one of the given modifiers is not
    registered.
    
    @param dev: An L{AEInput} device.
    @type dev: L{AEInput}
    @param modifiers: A list of key codes used as modifiers.
    @type modifiers: integer
    '''
    for m in modifiers:
      try:
        self.registered_modifiers[dev].remove(m)
      except (KeyError, ValueError):
        pass
    
  def registerModifiers(self, dev, modifiers):
    '''
    Registers a list of input modifiers.
    
    @param dev: An L{AEInput} device.
    @type dev: L{AEInput}
    @param modifiers: A list of key codes used as modifiers.
    @type modifiers: integer
    '''
    self.perk.registered_modifiers.setdefault(dev, []).extend(modifiers)
