'''
Defines a class responsible for handling events in the currently focused view.

@var CHECK_INTERVAL: Time in ms that the L{ViewManager} will iterate over all
  top level applications and provide them in a list to L{TierManager}
@type CHECK_INTERVAL: integer
@var DEFAULT_EVENTS: Names of events that will be dispatched by the 
  L{ViewManager} by default
@type DEFAULT_EVENTS: list

@author: Peter Parente
@author: Pete Brunet
@author: Brett Clippingdale
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
import logging
import pyLinAcc
from AEEvent import *
from POR import POR
from AEInterfaces import *
from Walker import AccessibleWalker

log = logging.getLogger('View')

# interval on which the manager will search for dead views
CHECK_INTERVAL = 60000

# events that we should always watch, regardless of Task interests
DEFAULT_EVENTS = [ViewChange, FocusChange, SelectorChange, CaretChange, 
                  PropertyChange, PrivateChange]

class ViewManager(object):
  '''
  Stores a root accessible representing the active view when an event occurs
  indicating that the view may have changed. Two view roots must be tracked
  by this object. The first, the raw view, is needed by L{Adapters} which will
  handle raw view change events. The second, the L{AccessEngine} view, is 
  needed by the rest of the system when processing L{AEEvent}s.
  
  @ivar event_manager: Reference to the L{EventManager}
  @type event_manager: L{EventManager}
  @ivar raw_view: Root accessible representing the active view according to the
    raw event stream.
  @type raw_view: L{pyLinAcc.Accessible}
  @ivar ae_view: Root L{POR} represnting the active view according to the 
    L{AEEvent} stream.
  @type ae_view: L{POR}
  @ivar tier_manager: Reference to the L{TierManager} for clearing dead L{Tier}
    objects on a set interval
  @type tier_manager: L{TierManager}
  @ivar wanted_events: Lookup table for what L{AEEvent}s are desired by any
    L{Task} in any L{Perk} in all L{Tier}s. Used to optimize event dispatch and
    processing. Unwanted events are ignored.
  @type wanted_events: dictionary
  @ivar raw_active: Is there an raw active view?
  @type raw_active: boolean
  '''
  def __init__(self, ae):
    '''
    Initializes instance variables that will store a reference to the 
    L{EventManager} and active view to None.
    
    @param ae: Reference to the L{AccessEngine}, currently unused
    @type ae: L{AccessEngine}
    '''
    self.tier_manager = None
    self.event_manager = None
    self.raw_view = None
    self.wanted_events = {}
    self.raw_active = False
    # register for a callback to check living applications
    ae.addTimer(self.onIterateApps, CHECK_INTERVAL)
      
  def init(self, event_man, tier_man, **kwargs):
    '''
    Stores a reference to the L{EventManager}. Registers with the 
    L{EventManager} using the L{EventManager.EventManager.addClient} to receive 
    raw events related to the active view. Also registers 
    for raw events that may indicate that this L{ViewManager} needs to manage a
    new view. Called by L{AccessEngine} at startup.
    
    @param event_man: Reference to the L{EventManager}
    @type event_man: L{EventManager}
    '''
    # store the tier manager so we can note running applications at startup
    self.tier_manager = tier_man
    # store the event manager so we can post events later
    self.event_manager = event_man
    # register for events that almost certainly mean the view has changed
    self.event_manager.addClient(self.onRawViewEvent, 
                                 *IEventHandler().getRawEvents(ViewChange))
    # track how many dependencies we have on a certain kind of event
    for kind in DEFAULT_EVENTS:
      self.setEventInterest(kind, True)
    
  def close(self):
    '''Does nothing.'''
    pass
  
  def initViews(self):
    '''   
    Walks through the top level applications on the first desktop. Calls
    L{TierManager.TierManager.createTier} for application encountered. If the
    L{TierManager} determines that an app has at least one L{Perk} written for
    that application, creates a L{Tier} for the application and loads all of
    its L{Perk}s. If the L{Tier} is created, this method sends
    L{AEEvent.ViewChange} messages to the L{Tier} with a flag of
    L{AEConstants.Event.EVENT_VIEW_STARTUP} such that L{Perk}s in the L{Tier}
    may initialize themselves and begin any desired background processes.
    '''
    try:
      d = pyLinAcc.Registry.getDesktop(0)
    except Exception:
      # just stop if we get an error right away
      return
    ai = IAccessibleInfo(POR(d))
    an = IAccessibleNav(POR(d))
    events = []
    # iterate over all children of the desktop
    for i in xrange(ai.getAccChildCount()):
      try:
        app = an.getChildAcc(i)
      except (LookupError, IndexError):
        continue
      # get application names and IDs
      aai = IAccessibleInfo(app)
      aid = aai.getAccAppID()
      name = aai.getAccAppName()
      # inform the tier manager
      if self.tier_manager.createTier(name, aid, app, True) is not None:
        events.append(ViewChange(app, AEConstants.EVENT_VIEW_STARTUP))
      # if we've found the active app, just continue
      if self.raw_view is not None:
        continue
      aan = IAccessibleNav(app)
      # iterate over all top level windows in the app
      for x in xrange(aai.getAccChildCount()):
        try:
          win = aan.getChildAcc(x)
        except (LookupError, IndexError):
          continue
        # look for one that has state active
        wai = IAccessibleInfo(win)
        if wai.hasAccState('active'):
          self.raw_view = win
          self.raw_active = True
          break
    # send a view change gained event to the active tier
    if self.raw_view is not None:
      events.append(ViewChange(self.raw_view, 
                               AEConstants.EVENT_VIEW_FIRST_GAINED))
    self.event_manager.postEvents(*events)
      
  def setEventInterest(self, kind, wants):
    '''
    Sets or unsets an interest in a particular kind of L{AEEvent}. This
    information is used to register or unregister for raw events on-demand as
    an optimization.
    
    @param kind: Indicates the type of L{AEEvent} some part of the system wants
      to be able to process
    @type kind: L{AEEvent} class
    @param wants: Does the system want to process the given kind of L{AEEvent}?
    @type wants: boolean
    ''' 
    count = self.wanted_events.setdefault(kind, 0)
    if wants:
      count += 1
    else:
      count -= 1
    if count <= 0:
      del self.wanted_events[kind]
      self.event_manager.removeClient(self.onRawEvent, 
                                      *IEventHandler().getRawEvents(kind))
      return
    elif count == 1:
      self.event_manager.addClient(self.onRawEvent,
                                   *IEventHandler().getRawEvents(kind))
    self.wanted_events[kind] = count
    
  def getRawActive(self):
    '''
    Gets the whether some view is active or not.
    
    @return: Value of L{raw_active}
    @rtype: boolean
    '''
    return self.raw_active
  
  def setRawActive(self, val):
    '''
    Sets the whether some view is active or not.
    
    @param val: Value to store in L{raw_active}
    @type val: boolean
    '''
    self.raw_active = val

  def getRawView(self):
    '''
    Gets the root L{POR} of the active view according to the raw event
    stream. This is the view known to L{Adapters}.
    
    @see: L{setRawView}
    @return: Root L{POR} of the raw active view
    @rtype: L{POR}
    '''
    return self.raw_view
  
  def setRawView(self, accessible):
    '''    
    Stores the root L{POR} of the active view according to the raw event 
    stream. This is the view known to L{Adapters}.
    
    @see: L{getRawView}
    @param accessible: Event source that triggered the change of view. Usually
      a top level window or panel.
    @type accessible: L{pyLinAcc.Accessible}
    @return: Was a new view root set or not?
    @rtype: boolean
    '''
    rv = False
    # walk to the first POR
    root = POR(accessible)
    # see if it is different from our current root POR
    if root != self.raw_view:
      # store the new view
      self.raw_view = root
      rv = True
    return rv
  
  def getAEView(self):
    '''
    Gets the root L{POR} of the active view according to the L{AEEvent} stream.
    This is the view known to L{Perk}s.
    
    @see: L{setAEView}
    @return: Root L{POR} of the active L{AccessEngine} view
    @rtype: L{POR}
    '''
    return self.ae_view
  
  def setAEView(self, por):
    '''
    Stores the root L{POR} of the active view according to the L{AEEvent}
    stream. This is the view known to L{Adapters}.
    
    @see: L{getAEView}
    @param por: New root L{POR} of the active L{AccessEngine} view. May be
      set to None if there is no active view.
    @type por: L{POR}
    '''
    self.ae_view = por
  
  def onIterateApps(self):
    '''
    Collects all application names and IDs and gives them to the L{TierManager}
    so it can free any L{Tier}s that are no longer associated with running
    applications.
    
    @return: True to continue receiving notifications
    @rtype: boolean
    '''
    try:
      d = pyLinAcc.Registry.getDesktop(0)
    except Exception:
      # stop if we get an error immediately
      return
    ai = IAccessibleInfo(POR(d))
    an = IAccessibleNav(POR(d))
    aids = []
    # iterate over all children of the desktop
    for i in xrange(ai.getAccChildCount()):
      try:
        app = an.getChildAcc(i)
      except (LookupError, IndexError):
        continue
      # get all application identifiers
      aai = IAccessibleInfo(app)
      aids.append(aai.getAccAppID())
    # inform the TierManager about the living applications
    self.tier_manager.freeDeadTiers(aids)
    return True
   
  def onRawEvent(self, event):
    '''
    Based on a raw L{pyLinAcc.Event.Event} posts L{AEEvent}s to the active
    L{Tier} through the L{EventManager}.
    
    @param event: Event indicating some change
    @type event: L{pyLinAcc.Event.Event}
    '''
    # only forward events if there is a view
    if self.raw_view is not None:
      try:
        eh = IEventHandler(event.source)
        eh.getAEEvents(event, self.event_manager.postEvents)
      except LookupError:
        # ignore dead accessibles
        return
    
  def onRawViewEvent(self, event):
    '''   
    Creates a L{AEEvent.ViewChange} event in response to a window activation or
    deactivation. Also responds to create or destroy events from floating
    windows. Called in response to any of the raw events returned by
    L{AEInterfaces.IEventHandler.getAEViewEvents} and registered with the
    L{EventManager} in the L{ViewManager.init} method when this object was
    created.

    @param event: Event that may indicate the view has changed
    @type event: L{pyLinAcc.Event.Event}
    '''
    try:
      eh = IEventHandler(event.source)
      eh.getAEViewEvents(event, self.event_manager.postEvents, self)
    except LookupError:
      # ignore dead accessibles
      return
