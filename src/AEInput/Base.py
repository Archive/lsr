'''
Defines the abstract base class for all L{AEInput} subclasses.

@author: Peter Parente
@author: Scott Haeger
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
import logging
import UIElement, AEConstants

log = logging.getLogger('Input')

def getDefaults():
  '''
  Suggests the default L{AEOutput}s events to be monitored.
  
  @return: Empty list, don't monitor by default
  @rtype: list of string
  '''
  return []
  
def getNames():
  '''
  Gets the names of all the L{AEInput} command types.
  
  @return: List of all known L{AEInput} command names
  @rtype: list of string
  '''
  names = AEConstants.INPUT_COMMAND_NAMES.values()
  names.sort()
  return names

class AEInput(UIElement.UIE):
  '''  
  Most abstract base class for all L{AEInput} devices. Maintains a collection of
  L{Gesture} listeners that can be notified using L{_notifyInputListeners}.
  Defines simple L{init} and L{close} methods that change the state of the
  L{ready} flag.
  
  This class is abstract as most of its methods raise NotImplementedError and 
  need to be overriden with input device specific code in subclasses.

  @ivar listeners: Collection of listeners that are notified about L{Gesture}s
    on an input device
  @type listeners: list of callable
  @ivar ready: Is the input device initialized?
  @type ready: boolean
  '''
  def __init__(self):
    '''
    Initializes the listeners list to empty and sets the ready flag to False.
    '''
    self.listeners = []

  def init(self):
    '''
    Called after the instance is created to initialize the device.

    If called when already initialized, this will restore the device to it's
    initialized state. May also be called to re-initialize the device after
    a call to L{close}.

    @raise NotImplementedError: When not overridden in a subclass
    @raise Error.InitError: When a communication or state problem exists for 
      the specific device
    '''
    raise NotImplementedError
    
  def getCapabilities(self):
    '''
    Gets a list of strings representing the capabilities of this device. 
    Typical output capabilities include "system input," "braille," "switch,"
    etc. though others are certainly possible.
    
    The L{DeviceManager} will only load a device if another device doesn't
    already provide all of its capabilities.
    
    @return: Lowercase names of output capabilities
    @rtype: list of string
    '''
    raise NotImplementedError
    
  def close(self): 
    '''
    Closes an initialized input device.

    @raise NotImplementedError: When not overridden in a subclass
    '''
    raise NotImplementedError
    
  def addInputListener(self, listener):
    '''
    Adds a listener to be notified whenever a L{Gesture} occurs on an input
    device. Listeners are called in the order they are added and are given the
    L{Gesture} detected.
    
    @param listener: Object to call when a L{Gesture} occurs
    @type listener: callable
    '''
    self.listeners.append(listener)
    
  def removeInputListener(self, listener):
    '''
    Removes an existing listener for L{Gesture}s.
    
    @param listener: Object to remove from the listener list
    @type listener: callable
    @raise ValueError: When removing a listener that is not registered
    '''
    self.listeners.remove(listener)
    
  def inputListenersExist(self):
    '''
    Gets if there are any registered L{Gesture} listeners for this device.
    
    @return: Is there at least one listener registered?
    @rtype: boolean
    '''
    return len(self.listeners) > 0
    
  def _notifyInputListeners(self, gesture, timestamp, **kwargs):
    '''
    Notifies registered listeners about a L{Gesture} seen on the input device. 
    Catches all exceptions from the callback and logs them.
    
    @param gesture: L{Gesture} to send to listeners
    @type gesture: L{Gesture}
    @param timestamp: Time at which at the gesture happened
    @type timestamp: float
    @param kwargs: Additional data to include in the notification
    @type kwargs: dictionary
    '''
    for listener in self.listeners:
      try:
        listener(gesture, timestamp, **kwargs)
      except Exception:
        log.exception('AEInput exception')

  def sortGesture(self, gesture):
    '''
    Sorts the actions in the given L{AEInput.Gesture} and returns them as a list
    of integers. The sort is done on a copy so the action codes held by the
    provided L{AEInput.Gesture} are not touched.

    @param gesture: L{AEInput.Gesture} to sort
    @type gesture: L{AEInput.Gesture}
    @return: Sorted list of action codes that may be wrapped in a new
      L{AEInput.Gesture}
    @rtype: list of integer
    '''
    codes = gesture.getActionCodes()
    codes.sort()
    return codes
  
  def getMaxActions(self):
    '''
    Abstract method. Gets the maximum number of actions that can be in a 
    L{Gesture} on this input device.
    
    @return: Maximum number of actions per L{Gesture} supported by this device
    @rtype: integer
    @raise NotImplementedError: When this method is not overridden by a subclass
    '''
    raise NotImplementedError
    
  def asString(self, gesture): 
    ''' 
    Abstract method. Gets a human readable representation of the given
    L{Gesture}.
    
    @param gesture: L{Gesture} object to render as text
    @type gesture: L{Gesture}
    @return: Text representation of the L{Gesture} 
    @rtype: string
    @raise NotImplementedError: When this method is not overridden by a subclass
    '''
    raise NotImplementedError
  
  def addKeyCmd(self, codes):
    ''' 
    Abstract method. Registers KEY_CMD with device .
    
    @param codes: list of lists of KEY_CMD* codes
    @type codes: list
    @raise NotImplementedError: When this method is not overridden by a subclass
    '''
    pass
    
  def removeKeyCmd(self, codes):
    ''' 
    Abstract method. Unregisters KEY_CMD with device .
    
    @param codes: list of lists of KEY_CMD* codes
    @type codes: list
    @raise NotImplementedError: When this method is not overridden by a subclass
    '''
    raise NotImplementedError
    
