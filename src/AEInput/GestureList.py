'''
Defines a class representing a collection of L{Gesture}ss performed in sequence
on an L{AEInput} device.

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
from Gesture import Gesture

class GestureList(object):
  '''
  Manages a list of L{Gesture} objects in the order in which they occured.
  Represents a sequence of L{Gesture}s for the purposes of detecting and 
  filtering that sequence when it occurs on an input device.
  
  @ivar device: Input device on which the L{Gesture} sequence is performed
  @type device: L{AEInput.AEInput}
  @ivar gestures: L{Gesture}s in the sequence
  @type gestures: list of L{Gesture}
  @ivar hash: Cached copy of the hash value of the L{GestureList}
  @type hash: integer
  '''
  def __init__(self, device, action_codes=None, gestures=None,
               copy_gestures=None):
    '''    
    Stores a reference to the input device. If action codes are specified, wraps
    them in L{Gesture} objects and stores those objects. If L{Gesture}s are
    specified, stores those objects. If L{Gesture}s to copy are given, makes a
    copy of those objects and stores the copies, not the original references. If
    none of these is given, creates an empty list to be filled with L{Gesture}s
    later.
    
    @param device: Input device on which this L{Gesture} is performed
    @type device: L{AEInput.AEInput}
    @param action_codes: List of lists of action codes that will be stored in
        L{Gesture} objects in this L{GestureList}
    @type action_codes: list of lists of integer
    @param gestures: L{Gesture}s that this L{GestureList} should store
    @type gestures: list of L{Gesture}  
    @param copy_gestures: L{Gesture}s that this L{GestureList} should duplicate
      and store
    @type copy_gestures: list of L{Gesture}
    '''
    self.device = device
    self.hash = None
    if action_codes is not None:
      # build Gestures from the lists of codes in the action codes list
      self.gestures = [Gesture(self.device, codes) for codes in action_codes]
    elif gestures is not None:
      # store gesture references
      self.gestures = gestures
    elif copy_gestures is not None:
      # store copies of gestures
      self.gestures = [Gesture(self.device, gesture=g) for g in copy_gestures]
    else:
      # initialize to an empty list if no gestures provided
      self.gestures = []
      
  def __eq__(self, other):
    '''
    Compares this L{GestureList} to the one provided to see if they represent
    the same sequence of L{Gesture}s on the same device. The comparison
    performed is order dependent since a different sequence of L{Gesture}s could
    represent a different input intention.
    
    @param other: L{GestureList} to compare to this one
    @type other: L{GestureList}
    @return: Is this L{GestureList} equal to the one provided?
    @rtype: boolean
    '''
    # equality comparison on lists of Gesture objects invokes __eq__ on each
    # object so this comparison is by value, not by instance
    try:
      return (self.device == other.device) and \
             (self.gestures == other.gestures)
    except Exception:
      return False

  def __hash__(self):
    '''
    Builds a hash code for this L{GestureList} based on its L{Gesture} contents
    by XORing their hash values together. The hash code is used by an L{AEInput}
    device to store and retrieve filters from a dictionary.
    
    @return: Hash code for this L{GestureList}
    @rtype: integer
    '''
    if self.hash is None:
      self.hash = 0
      for gesture in self.gestures:
        self.hash = self.hash^hash(gesture)
    return self.hash
    
  def getGestureAt(self, i):
    '''    
    Gets the L{Gesture} stored at the given index. If the index is invalid,
    raises IndexError.
    
    @param i: Index of the L{Gesture} to retrieve
    @type i: integer
    @return: L{Gesture} retrieved from this L{GestureList}
    @rtype: L{Gesture}
    @raise IndexError: When the index is invalid
    '''
    return self.gestures[i]
    
  def addGesture(self, gesture):
    '''
    Adds a L{Gesture} to the end of this L{GestureList}.
    
    @param gesture: L{Gesture} to add
    @type gesture: L{Gesture}
    '''
    self.gestures.append(gesture)
    self.hash = None
  
  def getNumGestures(self):
    '''
    Gets the total number of L{Gesture}s currently in this L{GestureList}.
    
    @return: L{Gesture} count
    @rtype: integer
    '''
    return len(self.gestures)

  def asString(self):
    '''    
    Gets a human readable representation of all the L{Gesture}s in this
    L{GestureList} determined by the L{AEInput} device on which they are
    performed.
    
    @return: Text representation of all the L{Gesture}s in this L{GestureList}
    @rtype: string
    '''
    out = []
    for gesture in self.gestures:
      out.append('(%s)' % gesture.asString())
    return ' '.join(out)
  
  def getDevice(self):
    '''
    Gets the device on which the L{Gesture}s in this L{GestureList} are
    performed.
    
    @return: Device on which the L{Gesture}s in this L{GestureList} are 
      performed
    @rtype: L{AEInput.AEInput}
    '''
    return self.device