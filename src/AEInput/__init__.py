'''
Defines abstract classes representing the interfaces that must be implemented
by an input device for LSR. All input devices derive from the base class in 
L{AEInput}. Devices that serve double duty as sources of input for LSR as well 
as the rest of the operating system derive from the L{SystemInput} subclass.

Concrete classes implementing the methods of L{AEInput} that ship with LSR are 
not located in this package. Rather, they are stored in the Devices folder and
referenced by the L{UIRegistrar} when they need to be loaded.

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
from Base import *
from SystemInput import SystemInput
from GestureList import GestureList
from Gesture import Gesture
from Error import *
