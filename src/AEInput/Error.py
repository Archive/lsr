'''
Defines a hierarchy of L{AEInput} exceptions that are raised when some device
operation fails. A hierarchy of exceptions is used rather than a single 
exception with various error codes to support the catching of a particular 
subtree of the hierarchy, a single kind of exception, or all exceptions.

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''

from i18n import _

class AEInputError(Exception):
  '''
  Base class for all L{AEInput} related errors. All arguments are simply passed
  to the Exception base class, including any error code or message.
  '''
  pass

class InitError(AEInputError):
  '''
  Raised when an input device can not be initialized in some manner.
  '''
  def __init__(self, *args, **kwargs):
    AEOutputError.__init__(self,
      _('could not initialize input device'), *args, **kwargs)