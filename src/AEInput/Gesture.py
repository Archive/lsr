'''
Defines a class representing an input L{Gesture}, one or more device specific
input actions (e.g. keyboard key press, Braille button press, speech input
command, camera tracked gesture, etc.) performed simultaneously.

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''

class Gesture(object):
  '''
  Represents one or more device specific input actions. Maintains a list of
  action codes specific to a L{AEInput} device as well as a reference to the
  device itself. Maintains a separate list of the action codes sorted according
  to the order specified by the L{AEInput} device owning this L{Gesture} in
  order to speed comparisons of two L{Gesture}s. Maintains a hash value computed
  by XORing the sorted action codes in order to support the deterministic
  hashing of L{Gesture}s with the same action codes to the same bin in a
  dictionary. Both the cached sorted list and the hash code are reset on each
  successful call to L{Gesture.addActionCode}.
  
  @ivar device: Input device on which the L{Gesture} is performed
  @type device: L{AEInput.AEInput}
  @ivar sorted_codes: Cached action codes sorted by the device owning this 
    L{Gesture} 
  @type sorted_codes: list of integer
  @ivar hash: Cached copy of the hash value of the L{Gesture}
  @type hash: integer
  @ivar action_codes: Codes for the actions performed on the device forming this
    L{Gesture}
  @type action_codes: list of integer
  '''
  def __init__(self, device, action_codes=None, gesture=None):
    '''    
    Stores a reference to the input device. Stores a copy of the the provided
    action codes or gets a copy of the action codes from the provided
    L{Gesture}. If neither is specified, creates an empty action code list.
    
    @param device: Input device on which this L{Gesture} is pressed
    @type device: L{AEInput.AEInput}
    @param action_codes: Action codes used to initialize the action code list
    @type action_codes: list of integer
    @param gesture: L{Gesture} object that this L{Gesture} should copy
    @type gesture: L{Gesture}
    '''
    self.device = device
    self.sorted_codes = None
    self.hash = None
    
    if action_codes is not None:
      # add action codes one by one using addActionCode method
      self.action_codes = []
      for code in action_codes:
        self.addActionCode(code)
    elif gesture is not None:
      # make a copy of the action codes in the gesture
      self.action_codes = gesture.getActionCodes()
    else:
      # start an empty action codes list
      self.action_codes = []
      
  def __str__(self):
    '''
    Gets a human readable representation of the L{Gesture} as a string from
    L{asString}.
    
    @return: Text describing the gesture
    @rtype: string
    '''
    return self.asString()
      
  def __eq__(self, other):
    '''
    Compares this L{Gesture} to the one provided. The comparison is performed
    independent of the order of the actions forming the gesture by using 
    L{Gesture.getSortedActionCodes}.
    
    @param other: L{Gesture} to compare to this one
    @type other: L{Gesture}
    @return: Is this L{Gesture} equal to the one provided?
    @rtype: boolean
    '''
    # equality comparison on a list is deep so that two lists with contents that
    # equate are considered equal, even if they are not the same instance
    # (the 'is' operate is used to check equality of instances)
    return self.getSortedActionCodes() == other.getSortedActionCodes()
  
  def __iter__(self):
    '''
    Gets an iterator over the action codes in this L{Gesture}.
    
    @return: Iterator over all action codes in this L{Gesture}
    @rtype: iterator
    '''
    return iter(self.action_codes)
  
  def __contains__(self, action_code):
    '''
    Gets if this L{Gesture} contains the given action code.
    
    @return: Is the action code in this L{Gesture}?
    @rtype: boolean
    '''
    return action_code in self.action_codes
    
  def __hash__(self):
    '''     
    Builds a hash code for this L{Gesture} based on its action code contents by
    XORing them together. The hash code is used by a L{GestureList} in building
    its hash code. The action codes are sorted in device dependent order using
    L{Gesture.getSortedActionCodes} before XORing to ensure two L{Gesture}s with
    the same actions hash to the same value.
    
    @return: Hash code for this L{Gesture}
    @rtype: integer
    '''
    if self.hash is None:
      self.hash = 0
      for code in self.getSortedActionCodes():
        self.hash = self.hash^code
    return self.hash
  
  def __len__(self):
    '''
    Gets the number of codes in the current gesture.
    
    @return: Number of codes
    @rtype: integer
    '''
    return len(self.action_codes)

  def getDevice(self):
    '''
    Gets the device on which this L{Gesture} is performed.
    
    @return: Device on which this L{Gesture} is performed
    @rtype: L{AEInput.AEInput}
    '''
    return self.device
  
  def addActionCode(self, code):
    '''
    Adds the given action code to this L{Gesture}. If the L{Gesture} is full
    (i.e. it has the maximum number of action as determined by its device), a
    ValueError is raised. If the L{Gesture} is not full, the new action code is
    added to the end of L{action_codes}. If the action code was already in the
    L{Gesture}, the action code is moved to the end of L{action_codes}.
    
    @param code: Action code to add to this L{Gesture}
    @type code: integer
    @raise ValueError: When the L{Gesture} is full according to 
      L{AEInput.Base.AEInput.getMaxActions}
    '''
    # check if full
    if len(self.action_codes) == self.device.getMaxActions():
      raise ValueError('Maximum action codes reached for this device')
    try:
      # remove the action if it's already there
      self.action_codes.remove(code)
    except ValueError:
      # the action wasn't already there
      pass
    # add the new action to the end
    self.action_codes.append(code)
    # reset cached sorted action codes and hash value
    self.sorted_codes = None
    self.hash = None
  
  def removeActionCode(self, code):
    '''
    Deletes an action code from this L{Gesture}.
    
    @param code: Action code to remove from this L{Gesture}
    @type code: integer
    @raise ValueError: When the action code does not exist in the L{Gesture}
    '''
    self.action_codes.remove(code)
    self.sorted_codes = None
    self.hash = None
    
  def clearActionCodes(self):
    '''
    Deletes all action codes from this L{Gesture}.
    '''
    self.action_codes = []
    self.sorted_codes = None
    self.hash = None
    
  def getActionCodes(self):
    '''
    Gets a copy of all the action codes in this L{Gesture}. Makes a fast copy by taking
    a slice using the start and end slice defaults.
    
    @return: Copy of all the action codes in this L{Gesture}
    @rtype: list of integer
    '''
    return self.action_codes[:]
  
  def peekActionCodes(self):
    '''
    Gets a reference to the list of action codes in this L{Gesture}. The reference to
    the list should be used for read-only access even though protections against
    modification are not used. This method exists solely to offset the time
    cost incurred by making a copy of actions using L{getActionCodes}.
    '''
    return self.action_codes
    
  def getSortedActionCodes(self):
    '''
    Gets a copy of all the action codes in this L{Gesture} in a sort order 
    determined by the L{AEInput} device on which the actions are performed.
    
    @return: Copy of all the action codes in device dependent sort order
    @rtype: list of integer
    '''
    if self.sorted_codes is None:
      self.sorted_codes = self.device.sortGesture(self)
    return self.sorted_codes
  
  def getNumActions(self):
    '''
    Gets the number of action codes stored in this L{Gesture}.
    
    @return: Number of action codes bounded by
    L{AEInput.Base.AEInput.getMaxActions}
    @rtype: integer
    '''
    return len(self.action_codes)
  
  def asString(self):
    '''
    Gets a human readable representation of the action codes in this L{Gesture} 
    determined by the L{AEInput} device on which the actions were performed.
    
    @return: Text representation of the actions in this L{Gesture}
    @rtype: string
    '''
    return self.device.asString(self)
