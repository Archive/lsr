'''
Defines an abstract base class for all input devices that serve double duty as 
sources of input for LSR and for the rest of the operating system.

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
import Base, AEConstants

class SystemInput(Base.AEInput):
  '''
  Abstract base class for input devices that are used by both LSR and the OS.
  Provides an interface for registering modifier actions that can be used to
  indicate the start of commands intended for LSR. Also provides an interface
  for registering filtered L{GestureList}s that indicate which combination of
  L{Gesture}s should not be passed to other applications. The definition of what
  constitues a valid L{Gesture} and L{GestureList} is left to a subclass. The
  decision as to when registered L{Gesture}s are filtered or not is determined
  by a mode managed in this class via L{getFilterMode} and L{setFilterMode}.
  
  This class is abstract as some of the methods inherited from 
  L{Base.AEInput} are not overridden and raise NotImplementedError.
  
  @ivar modifiers: Dictionary of action codes that are considered modifiers
  @type modifiers: dictionary
  @ivar filter_mode: Mode that determines which L{Gesture}s are filtered
  @type filter_mode: integer
  '''
  def __init__(self):
    '''
    Initializes the filters to an empty list, the modifiers to an empty
    dictionary, and the filtering mode to none.
    '''
    Base.AEInput.__init__(self)
    self.modifiers = {}
    self.filter_mode = AEConstants.FILTER_NONE
    
  def getCapabilities(self):
    '''
    @return: 'system input' as the only capability of this device.
    @rtype: list of string
    '''
    return ['system input']

  def getFilterMode(self):
    '''
    Gets the current filter mode.
    
    @return: Current filter mode
    @rtype: integer
    '''
    return self.filter_mode
    
  def setFilterMode(self, mode):
    '''
    Stores the current filter mode.
    
    @param mode: Current filter mode
    @type mode: integer
    '''
    self.filter_mode = mode
    
  def addModifier(self, code):
    ''' 
    Adds an action code to the modifiers dictionary to identify it as a modifier
    for other actions.  
    
    @param code: Action code to register as a modifier
    @type code: integer
    '''
    try:
      self.modifiers[code] += 1
    except KeyError:
      self.modifiers[code] = 1
    
  def removeModifier(self, code):
    '''
    Removes an action code from the modifiers dictionary if it is no longer
    in use.  Otherwise, it decrements the reference count.
    
    @param code: Action code to unregister as a modifier
    @type code: integer
    '''
    try:
      self.modifiers[code] -= 1
      if self.modifiers[code] <= 0:
        del self.modifiers[code]
    except KeyError:
      pass
    
  def clearModifiers(self):
    '''
    Removes all modifiers by destroying the modifiers dictionary and recreating
    it.
    '''
    self.modifiers = {}

  def addFilter(self, gestures):
    '''
    Abstact method. Adds a L{GestureList} as a filter to this device. A filter
    typically indicates input that should be consumed and prevented from 
    reaching other applications. Exactly how the filter is stored and used is 
    determined by a subclass.
    
    @param gestures: L{Gesture}s to possibly filter
    @type gestures: L{GestureList}
    @raise NotImplementedError: When this method is not overridden by a 
      subclass
    '''
    raise NotImplementedError
    
  def removeFilter(self, gestures):
    '''
    Abstract method. Removes a L{GestureList} as a filter from this device. 
    A filter typically indicates input that should be consumed and prevented 
    from reaching other applications. How the filter is removed is determined 
    by a subclass.
    
    @param gestures: L{Gesture}s to no longer filter
    @type gestures: L{GestureList}
    @raise NotImplementedError: When this method is not overridden by a
      subclass
    '''
    raise NotImplementedError
  
  def clearFilters(self):
    '''    
    Abstract method. Removes all L{GestureList} filters from the device. How 
    the filters are removed is determined by a subclass. A filter typically 
    indicates input that should be consumed and prevented from reaching other 
    applications. Exactly How the filter is removed is determined by a 
    subclass.
    
    @raise NotImplementedError: When this method is not overridden by a 
      subclass
    '''
    raise NotImplementedError

  def resetState(self):
    '''
    Abstract method. Resets the state of the input device.

    @raise NotImplementedError: When not overridden in a subclass
    '''
    raise NotImplementedError