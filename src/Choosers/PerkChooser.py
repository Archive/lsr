'''
Defines a gtk dialog L{AEChooser} for loading/unloading L{Perk}s from a single
L{Tier} at runtime.

@author: Brett Clippingdale
@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''

import pygtk
pygtk.require('2.0')
import gtk, gobject
import gtk.glade
from pyLinAcc import Atk as pyatk
import AEChooser
from i18n import _
import gettext
from GTKUIEView import *

__uie__ = dict(kind='chooser')

class PerkChooser(AEChooser.AEChooser): 
  '''
  Dialog for selecting Perks to load and unload on-the-fly.
  
  @ivar dialog: Dialog widget for choosing L{Perk}s
  @type dialog: gtk.Dialog
  @ivar uie_view: View of the status and order of loaded and unloaded L{Perk}s 
  @type uie_view: L{GTKUIEView.UIEView}
  
  @cvar RAISE: raise load priority (lower in stack) of L{Perk} in list of perks
  @type RAISE: integer
  @cvar LOWER: lower load priority (raise in stack) of L{Perk} in list of perks
  @type LOWER: integer
  @cvar TO_LOAD: indicates a L{Perk} is to be loaded in the current L{Tier}, if
    OK or APPLY buttons are then selected.
  @type TO_LOAD: integer
  @cvar TO_UNLOAD: indicates a L{Perk} is to be unloaded from the current 
    L{Tier}, if OK or APPLY buttons are then selected.
  @type TO_UNLOAD: integer
  '''
  # signal constants pulled from GTKUIEView
  RAISE = RAISE
  LOWER = LOWER
  TO_LOAD = TO_LOAD
  TO_UNLOAD = TO_UNLOAD
  APP_SINGLETON = True

  def init(self, app_name, loaded, unloaded, timestamp, **kwargs):
    '''
    Creates and shows the chooser dialog and its components.

    @param app_name: Name of application for current L{Tier}
    @type app_name: string
    @param loaded: Loaded L{Perk} class names, human readable names, and 
      descriptions
    @type loaded: list of 3-tuple of string
    @param unloaded: Unloaded L{Perk} class names, human readable names, and 
      descriptions
    @type unloaded: list of 3-tuple of string
    @param timestamp: Time at which input was given indicating the start of
      this chooser
    @type timestamp: float
    '''
    # initialize the view to None
    self.uie_view = None
    # load the glade file
    gtk.glade.set_custom_handler(self._createCustomWidget, loaded, unloaded)
    source = gtk.glade.XML(self._getResource('perk_chooser.glade'), 
                           'main dialog',
                           gettext.textdomain())
    
    # get the dialog widget
    self.dialog = source.get_widget('main dialog')
    self.dialog.set_title(self.dialog.get_title() % app_name)
    
    # set the data
    self.uie_view.setData(loaded, unloaded)
    
    # connect all signal handlers
    source.signal_autoconnect(self)
    
    self.activate(timestamp, present=False)

  def activate(self, timestamp, present=True, **kwargs):
    '''Try to bring the window to the foreground.'''
    try:
      # try to bring the window to the foreground if this feature is supported
      self.dialog.window.set_user_time(timestamp)
    except AttributeError:
      pass
    if present:
      self.dialog.present()
    
  def _createCustomWidget(self, source, name, func, *args):
    '''
    Creates an ordered UIE chooser widget.
    
    @param source: glade XML parser
    @type source: gtk.glade.XML
    @param name: Name of the widget to create
    @type name: string
    @param func: Name of the function to call to create this widget
    @type func: string
    @return: Custom widget
    @rtype: gtk.Widget
    '''
    self.uie_view = UIEView(self)
    return self.uie_view.getWidget()

  def _onOK(self, widget):
    '''
    Handles 'OK' button 'clicked' events.  Gets the current user-configured 
    loaded/unloaded lists, passes them on to DefaultDialogPerk in a request to 
    update the loaded L{Perk}s for the current L{Tier}, and then closes this 
    L{PerkChooser} dialog.
    
    @param widget: source of GUI event
    @type widget: gtk.Widget
    '''
    # get updated, user-configured loaded/unloaded lists
    loaded, unloaded = self.uie_view.getCurrentUIEs()
    # notify DefaultDialogPerk
    self._signal(self.OK, loaded=loaded, unloaded=unloaded)
    self.close()

  def _onCancel(self, widget):
    '''
    Handles for 'Cancel' button 'clicked' events.  Signals to DefaultDialogPerk
    that this L{AEChooser} will close, then closes this L{PerkChooser} dialog.
    
    @param widget: source of GUI event
    @type widget: gtk.Widget
    '''
    # notify DefaultDialogPerk
    self._signal(self.CANCEL)
    self.close()

  def _onApply(self, widget):
    '''
    Handles 'Apply' button 'clicked' events.  Gets the current user-
    configured loaded/unloaded lists, passes them on to DefaultDialogPerk in
    a request to update the loaded L{Perk}s for the current L{Tier}.
    
    @param widget: source of GUI event
    @type widget: gtk.Widget
    '''
    # get updated, user-configured loaded/unloaded lists
    loaded, unloaded = self.uie_view.getCurrentUIEs()
    # notify DefaultDialogPerk
    self._signal(self.APPLY, loaded = loaded, unloaded=unloaded)

  def close(self):
    '''
    Closes the chooser, preventing further chooser interaction with the user.
    '''
    gtk.glade.set_custom_handler(lambda x: None)
    self.dialog.destroy()
      
  def getName(self):
    '''
    Gets the name of the chooser.
    
    @return: Human readable name of the chooser
    @rtype: string
    '''
    return _('Perk Chooser')
