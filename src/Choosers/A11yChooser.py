'''
Defines a gtk dialog with a yes/no option for enabling desktop accessibility.

@note: Current implementation is AT-SPI, GNOME dependent

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''

import pygtk
pygtk.require('2.0')
import gtk, gobject
import gconf
import AEChooser
from i18n import _

__uie__ = dict(kind='chooser')

class A11yChooser(AEChooser.AEChooser): 
  '''
  Shows a dialog that lets the user choose whether to enable or disable 
  desktop accessibility at startup.
  
  @ivar dialog: Message dialog
  @type dialog: gtk.MessageDialog
  '''
  def init(self, **kwargs):
    '''
    Creates and shows the chooser dialog and its components.
    '''
    # get the dialog widget
    self.dialog = gtk.MessageDialog(type=gtk.MESSAGE_WARNING,
                                    buttons=gtk.BUTTONS_YES_NO)
    self.dialog.set_title(_('Desktop accessibility disabled'))
    self.dialog.set_property('text', 
     _('LSR cannot see the applications on your desktop. You'
       ' must enabled desktop accessibility to fix this '
       ' problem. Do you want to enable it now?'))
    self.dialog.set_property('secondary-text',
                             _('Note: Changes take effect after logout.'))
    self.dialog.set_property('use-markup', False)
    self.dialog.show_all()
    
    self.dialog.connect('response', self._onResponse)

  def _onResponse(self, widget, response):
    '''
    Processes the yes/no response and closes the dialog.
    
    @param widget: Source of GUI event
    @type widget: gtk.Widget
    '''
    if response == gtk.RESPONSE_YES:
      cl = gconf.client_get_default()
      cl.set_bool('/desktop/gnome/interface/accessibility', True)
    self.close()

  def close(self):
    '''
    Closes the chooser, preventing further interaction with the user.
    '''
    self.dialog.destroy()
      
  def getName(self):
    '''
    Gets the name of the chooser.
    
    @return: Human readable name of the chooser
    @rtype: string
    '''
    return _('Accessibility message dialog')
