'''
Defines a gtk dialog L{AEChooser} for configuring LSR scripts, devices, 
profiles, and system settings. Uses a glade XML file to define the shell of the
dialog. Dynamically generates the contents of the script, device, and system
panes internal panes based on the available L{AEState} settings. Generates the
contents of the profile views using information from the L{UIRegistrar}.

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
import pygtk
pygtk.require('2.0')
import gtk, gobject, atk
import gtk.glade
from pyLinAcc import Atk
import AEChooser, AEState
from i18n import _, DOMAIN
from GTKUIEView import *
import weakref, logging

__uie__ = dict(kind='chooser')

log = logging.getLogger('SettingsChooser')

class WidgetFactory(object):
  '''
  Widget factory for creating gtk views for L{AEState.Setting} objects. Has
  one public method for creating new widgets. All other methods are protected
  for internally handling widget events and updating their correspoding 
  settings.
  
  @ivar last_setting: Last setting object to change. Used to prevent cyclic
    notification between widgets and setting objects.
  @type last_setting: L{AEState.Setting}
  ''' 
  def __init__(self):
    self.last_setting = None
    
  def _isCycle(self, setting):
    '''
    Detects duplicate notifications of setting changes. This can be caused by
    a pygtk widget changing, updating a setting, and then getting renotified
    by the setting about the change. It can also occur when the setting 
    changes, updates the widget, and the widget tries to update the setting
    again.
    
    @param setting: Setting which is about to change or has changed value
    @type setting: L{AEState.Setting}
    '''
    if self.last_setting is setting:
      # don't notify ourselves again
      self.last_setting = None
      return True
    self.last_setting = setting
    return False
  
  def _boolWidgetChange(self, widget, setting):
    if self._isCycle(setting): return
    setting.value = widget.get_active()
    
  def _boolSettingChange(self, state, setting, widget):
    if self._isCycle(setting): return
    widget.set_active(setting.value)
    
  def _numericWidgetChange(self, adj, setting):
    if self._isCycle(setting): return
    setting.getView().value = adj.get_value()
    
  def _numericSettingChange(self, state, setting, widget):
    if self._isCycle(setting): return
    widget.set_value(setting.getView().value)
    
  def _filenameWidgetChange(self, widget, setting):
    if self._isCycle(setting): return
    fn = widget.get_filename()
    if fn is not None:
      setting.value = fn
    
  def _filenameSettingChange(self, state, setting, widget):
    if self._isCycle(setting): return
    widget.set_filename(value)

  def _stringWidgetChange(self, widget, event, setting):
    if self._isCycle(setting): return
    setting.value = widget.get_text()
    
  def _stringSettingChange(self, state, setting, widget):
    if self._isCycle(setting): return
    widget.set_text(value)

  def _choiceWidgetChange(self, widget, setting):
    if self._isCycle(setting): return
    index = widget.get_active()
    setting.value = setting.values[index]
    
  def _choiceSettingChange(self, state, setting, widget):
    if self._isCycle(setting): return
    widget.set_active(setting.values.index(setting.value))
    
  def _colorWidgetChange(self, widget, setting):
    if self._isCycle(setting): return
    color = widget.get_color()
    if widget.get_use_alpha():
      alpha = widget.get_alpha()
      setting.value = (color.red, color.green, color.blue, alpha)
    else:
      setting.value = (color.red, color.green, color.blue)
      
  def _colorSettingChange(self, state, setting, widget):
    if self._isCycle(setting): return
    widget.set_color(gtk.gdk.Color(red=value[0], green=value[1],
                                   blue=value[2]))
    if len(value) > 3:
      widget.set_alpha(value[3])
  
  def create(self, setting):
    widget = None
    if isinstance(setting, AEState.Group):
      # another group
      widget = gtk.Frame()
      widget.set_shadow_type(gtk.SHADOW_NONE)
      label = gtk.Label()
      label.set_markup('<b>%s</b>' % setting.name)
      widget.set_label_widget(label)
      return widget, None
    elif isinstance(setting, AEState.BoolSetting):
      # boolean
      widget = gtk.CheckButton(setting.label)
      widget.set_active(setting.value)
      id = widget.connect('toggled', 
                          AEState.Proxy(self._boolWidgetChange), setting)
      # register for setting value changes
      setting.addObserver(self._boolSettingChange, widget)
      return widget, None
    elif isinstance(setting, AEState.FilenameSetting):
      # filename
      widget = gtk.FileChooserButton(setting.label)
      widget.set_filename(setting.value)
      widget.connect('selection-changed', 
                     AEState.Proxy(self._filenameWidgetChange), setting)
      # register for setting value changes
      setting.addObserver(self._filenameSettingChange, widget)
    elif isinstance(setting, AEState.StringSetting):
      # string
      widget = gtk.Entry()
      widget.set_text(setting.value)
      widget.connect('focus-out-event', 
                     AEState.Proxy(self._stringWidgetChange), setting)
      # register for setting value changes
      setting.addObserver(self._stringSettingChange, widget)
    elif isinstance(setting, AEState.NumericSetting):
      # use a view object for a range
      view = setting.getView()
      # numeric or range
      if view.precision == 0:
        step = 1
      else:
        step = 0.1**view.precision
      page = int((view.max-view.min)/10.0)
      adj = gtk.Adjustment(view.value, view.min, view.max, step, page)
      if isinstance(setting, (AEState.RangeSetting, AEState.RelRangeSetting)):
        widget = gtk.HScale(adj)
      else:
        widget = gtk.SpinButton(adj)
      widget.set_digits(view.precision)
      widget.set_value(view.value)
      adj.connect('value-changed', AEState.Proxy(self._numericWidgetChange)
                  , setting)
      # register for setting value changes
      setting.addObserver(self._numericSettingChange, widget)
    elif isinstance(setting, AEState.EnumSetting):
      # enum
      widget = gtk.combo_box_new_text()
      map(widget.append_text, setting.labels)
      widget.set_active(setting.values.index(setting.value))
      widget.connect('changed', 
                     AEState.Proxy(self._choiceWidgetChange), setting)
      # register for setting value changes
      setting.addObserver(self._choiceSettingChange, widget)
    elif isinstance(setting, AEState.ChoiceSetting):
      # choice
      widget = gtk.combo_box_new_text()
      map(widget.append_text, setting.values)
      widget.set_active(setting.values.index(setting.value))
      widget.connect('changed', 
                     AEState.Proxy(self._choiceWidgetChange), setting)
      # register for setting value changes
      setting.addObserver(self._choiceSettingChange, widget)
    elif isinstance(setting, AEState.ColorSetting):
      # color
      widget = gtk.ColorButton(gtk.gdk.Color(red=setting.value[0],
                                             green=setting.value[1],
                                             blue=setting.value[2]))
      if len(setting.value) > 3:
        widget.set_use_alpha(True)
        widget.set_alpha(setting.value[3])
      else:
        widget.set_use_alpha(False)
      widget.connect('color-set', 
                     AEState.Proxy(self._colorWidgetChange), setting)
      # register for setting value changes
      setting.addObserver(self._colorSettingChange, widget)
    else:
      raise NotImplementedError
    # create a label
    label = gtk.Label(setting.label)
    label.set_alignment(0, 0.5)
    
    return widget, label

class SettingsChooser(AEChooser.AEChooser): 
  '''
  LSR settings dialog for configuring Perks, devices, profiles, and other 
  system settings.
  
  @ivar factory: Factory used to create gtk widget for settings
  @type factory: L{WidgetFactory}
  @ivar dialog: Dialog widget for configuring LSR settings
  @type dialog: gtk.Dialog
  @ivar system: LSR system wide settings
  @type system: L{AEState}
  @ivar perks: Per L{Perk} settings keyed by human readable Perk name
  @type perks: dictionary of string : L{AEState}
  @ivar devices: Per L{AEOutput} device settings keyed by human readable device
    name
  @type devices: dictionary of string : L{AEState}
  @ivar installed: All UIE class names, human readable names, and 
    descriptions installed keyed by their type
  @type installed: dictionary of string : 3-tuple of string
  @ivar associated: All UIE class names associated with this profile
  @type associated: list of string
  @ivar section_nb: Main tabbed panel
  @type section_nb: gtk.Notebook
  @ivar perk_tv: List of configurable L{Perk}s
  @type perk_tv: gtk.TreeView
  @ivar device_tv: List of configurable L{AEInput} and L{AEOutput} devices
  @type device_tv: gtk.TreeView
  @ivar system_vp: Panel for system settings
  @type system_vp: gtk.Viewport
  @ivar profile_perks: Aids selection of L{Perk}s to associate with the profile
  @type profile_perks: L{GTKUIEView.UIEView}
  @ivar profile_monitors: Aids selection of L{AEMonitor}s to associate with the
    profile
  @type profile_monitors: L{GTKUIEView.UIEView}
  @ivar profile_choosers: View of L{AEChooser}s installed
  @type profile_choosers: L{GTKUIEView.UIEView}
  @ivar profile_devices: Aids selection of preferred L{AEOutput} and L{AEInput}
    devices associated with this profile
  @type profile_devices: L{GTKUIEView.UIEView}
  @cvar FRAME_MARKUP: Pango markup for frame panels to make text larger
  @type FRAME_MARKUP: string
  @cvar PERK: Tab number for configuring Perks
  @type PERK: integer
  @cvar DEVICE: Tab number for configuring devices
  @type DEVICE: integer
  @cvar SYSTEM: Tab number for configuring system settings
  @type SYSTEM: integer
  @cvar PROFILE: Tab number for configuring the profile
  @type PROFILE: integer
  @cvar RAISE: Raise priority of a L{UIElement} for handling events or getting 
    loaded
  @type RAISE: integer
  @cvar LOWER: Lower priority of a L{UIElement} for handling events or getting 
    loaded
  @type LOWER: integer
  @cvar TO_LOAD: Indicates a L{UIElement} is now set to be loaded
  @type TO_LOAD: integer
  @cvar TO_UNLOAD: Indicates a L{UIElement} is now set to be unloaded
  @type TO_UNLOAD: integer
  ''' 
  GLOBAL_SINGLETON = True
  PERK = 0
  DEVICE = 1
  SYSTEM = 2
  PROFILE = 3
  FRAME_MARKUP = '<span size="xx-large" weight="bold">%s</span>'
  
  # signal constants pulled from GTKUIEView
  RAISE = RAISE
  LOWER = LOWER
  TO_LOAD = TO_LOAD
  TO_UNLOAD = TO_UNLOAD
  
  def init(self, system, perks, installed, associated, profile, devices, 
           timestamp, **kwargs):
    '''
    Creates and shows the settings dialog and its components

    @param system: LSR system wide settings
    @type system: L{AEState}
    @param perks: Per L{Perk} settings keyed by Perk name
    @type perks: dictionary of string : L{AEState}
    @param devices: Per L{AEOutput}/L{AEInput} settings keyed by device name
    @type devices: dictionary of string : L{AEState}
    @param installed: All UIE class names, human readable names, and 
      descriptions installed keyed by their type
    @type installed: dictionary of string : list of 3-tuple of string
    @param associated: All UIE class names, human readable names, and 
      descriptions associated with this profile keyed by their type
    @type associated: dictionary of string : list of 3-tuple of string
    @param timestamp: Time at which input was given indicating the start of
      this chooser
    @type timestamp: float
    @param profile: Name of the profile
    @type profile: string
    '''
    # create a widget factory
    self.factory = WidgetFactory()

    # store group objects
    self.system = system
    self.perks = perks
    self.devices = devices
    
    # load the glade file
    gtk.glade.set_custom_handler(self._createCustomWidget)
    source = gtk.glade.XML(self._getResource('lsr_settings.glade'), 
                           'main dialog', DOMAIN)

    # get the dialog widget
    self.dialog = source.get_widget('main dialog')
    
    
    # get widget references
    self.tips = gtk.Tooltips()
    self.section_nb = source.get_widget('section notebook')
    self.perk_tv = source.get_widget('perk treeview')
    perk_label = source.get_widget('perk label')
    perk_vp = source.get_widget('perk viewport')
    self.device_tv = source.get_widget('device treeview')
    device_label = source.get_widget('device label')
    device_vp = source.get_widget('device viewport')
    self.system_vp = source.get_widget('system viewport')
    # put the name of the profile on the profile tab
    pl = source.get_widget('profile label')
    pl.set_text(pl.get_text() % profile.title())
   
    # create widget models
    self.perk_tv.set_model(gtk.ListStore(gobject.TYPE_STRING))
    self.perk_tv.set_data('subsection label', perk_label)
    self.perk_tv.set_data('subsection viewport', perk_vp)
    
    self.device_tv.set_model(gtk.ListStore(gobject.TYPE_STRING))
    self.device_tv.set_data('subsection label', device_label)
    self.device_tv.set_data('subsection viewport', device_vp)
    
    # connect all signal handlers
    source.signal_autoconnect(self)
    
    # populate all sections
    self._createPerkSettingsView()
    self._createDeviceSettingsView()
    self._createProfileView(associated, installed)
    self._createSystemSettingsView()
    
    self.activate(timestamp, present=False)
    
  def activate(self, timestamp, present=True, **kwargs):
    '''Try to bring the window to the foreground.'''
    try:
      # try to bring the window to the foreground if this feature is supported
      self.dialog.window.set_user_time(timestamp)
    except AttributeError:
      pass
    if present:
      try:
        self.dialog.present()
      except gtk.Warning:
        # ignore strange gtk warnings that prevent further building of the UI
        pass
    
  def _createCustomWidget(self, glade, function_name, widget_name, *args):
    '''
    Creates a custom widget. Invoked during processing by glade.
    
    @param glade: glade XML parser
    @type glade: gtk.glade.XML
    @param widget_name: Name of the widget to create
    @type widget_name: string
    @param function_name: Name of the function to call to create this widget
    @type function_name: string
    @return: Custom widget
    @rtype: gtk.Widget
    '''
    return getattr(self, function_name)()
  
  def _createProfilePerkWidget(self):
    '''
    Creates a L{GTKUIEView.UIEView} instance to manage selection of 
    L{Perk}s.
    
    @return: Root widget of the UIE view
    @rtype: gtk.Widget
    '''
    self.profile_perks = UIEView(self, ordered=False)
    return self.profile_perks.getWidget()
    
  def _createProfileMonitorWidget(self):
    '''
    Creates a L{GTKUIEView.UIEView} instance to manage selection of 
    L{AEMonitor}s.
    
    @return: Root widget of the UIE view
    @rtype: gtk.Widget
    '''
    self.profile_monitors = UIEView(self, ordered=False)
    return self.profile_monitors.getWidget()
    
  def _createProfileChooserWidget(self):
    '''
    Creates a L{GTKUIEView.UIEView} instance to manage selection of 
    L{AEChooser}s.
    
    @return: Root widget of the UIE view
    @rtype: gtk.Widget
    '''
    self.profile_choosers = UIEView(self, ordered=False, 
                                           activatable=False)
    return self.profile_choosers.getWidget()
  
  def _createProfileDevicesWidget(self):
    '''
    Creates a L{GTKUIEView.UIEView} instance to manage selection of 
    L{AEInput} and L{AEOutput} devices.
    
    @return: Root widget of the UIE view
    @rtype: gtk.Widget
    '''
    self.profile_devices = UIEView(self, ordered=True)
    return self.profile_devices.getWidget()

  def _createSystemSettingsView(self):
    '''
    Populates the system settings profile panel.
    '''
    layout = self._populateSection(self.system.getGroups(), self.system_vp)
    self.system_vp.add(layout)
    self.system_vp.show_all()
    
  def _createProfileView(self, associated, installed):
    '''
    Populates all of the profile view tab panels.
    '''
    order = (('perks', self.profile_perks), 
             ('devices', self.profile_devices),
             ('monitors', self.profile_monitors),
             ('choosers', self.profile_choosers))
    for name, view in order:
      loaded = associated[name]
      unloaded = set(installed[name])-set(loaded)
      view.setData(loaded, unloaded)
    
  def _createPerkSettingsView(self):
    '''
    Populates the list of configurable L{Perk}s and selects the first to 
    generate its settings panel.
    '''
    # reset the view completely
    model = self.perk_tv.get_model()
    model.clear()
    cols = self.perk_tv.get_columns()
    map(self.perk_tv.remove_column, cols)
    
    # configure the perk list view
    crt = gtk.CellRendererText()
    tvc = gtk.TreeViewColumn('', crt, text=0)
    self.perk_tv.append_column(tvc)
    
    # populate the list of perks
    if len(self.perks) > 0:
      keys = self.perks.keys()
      keys.sort()
      for name in keys:
        try:
          self.perks[name].getGroups()
        except NotImplementedError:
          # ignore states that have no configurable settings
          continue
        model.append([name])
      # set the first one as selected to cause an update of the panel
      self.perk_tv.set_cursor((0,))
      
  def _createDeviceSettingsView(self):
    '''
    Populates the list of configurable L{AEInput} and L{AEOutput} devices and 
    selects the first to generate its settings panel.
    '''
    # reset the view completely
    model = self.device_tv.get_model()
    model.clear()
    cols = self.device_tv.get_columns()
    map(self.device_tv.remove_column, cols)
    
    # configure the perk list view
    crt = gtk.CellRendererText()
    tvc = gtk.TreeViewColumn('', crt, text=0)
    self.device_tv.append_column(tvc)
    
    # populate the list of perks with settings
    if len(self.devices) > 0:
      keys = self.devices.keys()
      keys.sort()
      for name in keys:
        try:
          self.devices[name].getGroups()
        except NotImplementedError:
          # ignore states that have no configurable settings
          continue
        model.append([name])
      # set the first one as selected to cause an update of the panel
      self.device_tv.set_cursor((0,))

  def _onScrollToFocus(self, widget, direction, viewport):
    '''
    Scrolls a focused widget in a settings panel into view.
    
    @param widget: Widget that has the focus
    @type widget: gtk.Widget
    @param direction: Direction constant, ignored
    @type direction: integer
    @param viewport: Viewport to scroll to bring the widget into view
    @type viewport: gtk.Viewport
    '''
    x, y = widget.translate_coordinates(viewport, 0, 0)
    w, h = widget.size_request()
    vw, vh = viewport.window.get_geometry()[2:4]
         
    adj = viewport.get_vadjustment()
    if y+h > vh:
      adj.value += (y+h) - vh - 3
    elif y < 0:
      adj.value = max(adj.value + y, adj.lower)
    
  def _onSubSectionChange(self, tv):
    '''
    Prepares a settings panel for viewing when the name of a L{Perk} or 
    L{AEInput}/L{AEOutput} device is selected from the list of configurable
    elements.
    
    @param tv: Treeview in which a configurable item is selected
    @type tv: gtk.TreeView
    '''
    # get model data
    model = tv.get_model()
    label = tv.get_data('subsection label')
    viewport = tv.get_data('subsection viewport')
    # get selected category info
    path, col = tv.get_cursor()
    name = model[path][0]
    
    # throw away the previous contents
    child = viewport.get_child()
    if child:
      viewport.remove(child)
      
    # set the label on the section frame
    label.set_markup(self.FRAME_MARKUP % name)
    # populate the subsection
    if self.perk_tv == tv:
      group = self.perks[name].getGroups()
      try:
        layout = self._populateSection(group, viewport)
        viewport.add(layout)
      except AssertionError:
        log.warn('empty perk settings group')
    elif self.device_tv == tv:
      group = self.devices[name].getGroups()
      try:
        layout = self._populateSection(group, viewport)
        viewport.add(layout)
      except AssertionError:
        log.warn('empty device settings group')
    viewport.show_all()

  def _populateSection(self, group, viewport):
    '''
    Populate a section pane with widgets representing the L{AEState.Setting}s 
    in the group. The container is the top level widget in which other widgets
    should be made. If recursive groups exist, their settings will be properly
    represented by widgets in a widget sub-container.

    @param group: Group organizing a subset of settings in the state
    @type group: L{AEState.Setting.Group}
    @param viewport: Reference to the viewport to populate with widgets
    @type viewport: gtk.Viewport
    @raise AssertionError: When there are no settings in the group
    '''
    n = len(group)
    if n < 1:
      # no settings in the group
      raise AssertionError
    # use a n by 2 table to order widgets nicely
    table = gtk.Table(n, 2)
    table.set_row_spacings(5)
    table.set_col_spacings(5)
    # nit: add in reverse order else controls show up backwards in acc. tree
    for row, setting in group.iterSettings(reverse=True):
      widget, label = self.factory.create(setting)
      if isinstance(widget, gtk.Frame):
        # if the widget is another group, recurse
        layout = self._populateSection(setting, viewport)
        layout.set_border_width(5)
        # add the sublayout to the group widget
        widget.add(layout)
        # attach the group widget to the table across both columns
        # nit: add widget on the right first then label on the left, else gail
        # screws up the order in the acc. hierarchy
        table.attach(widget, 0, 2, n-row-1, n-row, yoptions=gtk.FILL, 
                     xpadding=5)
      else:
        if isinstance(widget, gtk.CheckButton):
          # check buttons have a label already, so fill the row
          table.attach(widget, 0, 2, n-row-1, n-row, yoptions=gtk.FILL)
        else:
          # relate the label to the widget
          Atk.setRelation(widget, atk.RELATION_LABELLED_BY, label)
          Atk.setRelation(label, atk.RELATION_LABEL_FOR, widget)
          # attach to the table
          # note: add widget on the right first then label on the left,
          # else gail screws up the order in the acc. hierarchy
          table.attach(widget, 1, 2, n-row-1, n-row, yoptions=gtk.FILL)
          table.attach(label, 0, 1, n-row-1, n-row, gtk.FILL, gtk.FILL, 5, 0)
          # watch for focus on the widget so we can scroll
        if setting.description:
          # set description as a tooltip on the widget itself; does not work
          # with all widgets (e.g. combobox)
          self.tips.set_tip(widget, setting.description)
          # also set description as accessible description on the widget
          Atk.setNameDesc(widget, description=setting.description)
        # watch for focus so we can scroll the widget into view
        widget.connect('focus', self._onScrollToFocus, viewport)
    return table
  
  def _getAssociated(self):
    '''
    Gets lists of L{UIElement} class names currently set to be associated with
    the active profile.
    
    @return: Dictionary of associated UIEs keyed by their kind
    @rtype: dictionary of string : list of string
    '''
    d = {}
    if self.profile_perks.isDirty():
      d['perks'] = self.profile_perks.getCurrentUIEs()[0]
    if self.profile_monitors.isDirty():
      d['monitors'] = self.profile_monitors.getCurrentUIEs()[0]
    if self.profile_devices.isDirty():
      d['devices'] = self.profile_devices.getCurrentUIEs()[0]
    return d

  def _onOK(self, widget):
    '''
    Closes the dialog and sends a signal indicating the OK action.
    
    @param widget: Source of GUI event
    @type widget: gtk.Widget
    '''
    self._signal(self.OK, system=self.system, perks=self.perks, 
                 devices=self.devices, associated=self._getAssociated())
    self.close()

  def _onCancel(self, widget):
    '''
    Closes the dialog and sends a signal indicating the Cancel action.
    
    @param widget: Source of GUI event
    @type widget: gtk.Widget
    '''
    self._signal(self.CANCEL, system=self.system, perks=self.perks,
                 devices=self.devices)
    self.close()

  def _onApply(self, widget):
    '''
    Sends a signal indicating the Apply action.
    
    @param widget: Source of GUI event
    @type widget: gtk.Widget
    '''
    # get all associated
    self._signal(self.APPLY, system=self.system, perks=self.perks,
                 devices=self.devices, associated=self._getAssociated())

  def close(self):
    '''
    Closes the chooser, preventing further chooser interaction with the user.
    '''
    self.tips = None
    self.factory = None
    gtk.glade.set_custom_handler(lambda x: None)
    self.dialog.destroy()

  def getName(self):
    '''
    Gets the name of the chooser.
    
    @return: Human readable name of the chooser
    @rtype: string
    '''
    return _('LSR Settings')

  def updateDevices(self, devices):
    '''
    Updates the list of devices according to those that are now loaded.
    
    @param devices: Per L{AEOutput}/L{AEInput} settings keyed by device name
    @type devices: dictionary of string : L{AEState}
    '''
    self.devices = devices
    self._createDeviceSettingsView()
