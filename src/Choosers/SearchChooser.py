'''
Defines a gtk dialog L{AEChooser} for searching for a given accessible in the
current view.

@author: Scott Haeger
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''

import pygtk
pygtk.require('2.0')
import gtk, gobject
import gtk.glade
from pyLinAcc import Atk as pyatk
import AEChooser
from i18n import _
import gettext
from GTKUIEView import *

__uie__ = dict(kind='chooser')

class SearchChooser(AEChooser.AEChooser): 
  '''
  Dialog for searching current view for given accessible.
  
  @ivar dialog: Dialog widget for choosing L{Perk}s
  @type dialog: gtk.Dialog
  @ivar query: Search query
  @type query: string
  @ivar matchcase: Make search case sensitive
  @type matchcase: boolean
  @ivar wrap: Make search wrap around 
  @type wrap: boolean
  '''
  START = 10
  ALLOW_MULTI_APPS = True
  
  def init(self, app_name, timestamp, **kwargs):
    '''
    Creates and shows the chooser dialog and its components.

    @param app_name: Name of application for current L{Tier}
    @type app_name: string
    @param timestamp: Time at which input was given indicating the start of
      this chooser
    @type timestamp: float
    '''
    self.query = ''
    self.matchcase = False
    self.wrap = False
        
    # load the glade file
    self.source = gtk.glade.XML(self._getResource('search_chooser.glade'), 
                           'main dialog',
                           gettext.textdomain())
    # get the dialog widget
    self.dialog = self.source.get_widget('main dialog')
    self.dialog.set_title(self.dialog.get_title() % app_name)
    
    # connect all signal handlers
    self.source.signal_autoconnect(self)
    
    # show the dialog
    try:
      # try to bring the window to the foreground if this feature is supported
      self.dialog.window.set_user_time(timestamp)
    except AttributeError:
      pass
    self._signal(self.START)
      
  def _onSearchChange(self, widget):
    '''
    Callback for search text entry.  Updates query.  
    
    @param widget: source of GUI event
    @type widget: gtk.Widget
    '''
    self.query = widget.get_text()
    
  def _onMatchClicked(self, widget):
    '''
    Callback for 'match' checkbox.  Sets instance variable according to 
    checkbox state.
    
    @param widget: source of GUI event
    @type widget: gtk.Widget
    '''
    self.matchcase = widget.get_active()  
    
  def _onWrapClicked(self, widget):
    '''
    Callback for 'wrap' checkbox.  Sets instance variable according to 
    checkbox state.
    
    @param widget: source of GUI event
    @type widget: gtk.Widget
    '''
    self.wrap = widget.get_active()
    
  def _onClose(self, widget):
    '''
    Notify the L{Perk} that the dialog is closing and destroy it.
    
    @param widget: Source of GUI event
    @type widget: gtk.Widget
    '''
    self._signal(self.CANCEL)
    self.close()
    
  def close(self):
    '''
    Closes the chooser, preventing further chooser interaction with the user.
    '''
    self.dialog.destroy()
      
  def getName(self):
    '''
    Gets the name of the chooser.
    
    @return: Human readable name of the chooser
    @rtype: string
    '''
    return _('Search Chooser')
  
  def getQuery(self):
    '''
    Gets the search query.
    
    @return: Search query
    @rtype: string
    '''
    return self.query
        
  def isWrapSet(self):
    '''
    Should the search wrap to the top (or bottom) of the accessible tree ?
    
    @return: Is search wrap set?
    @rtype: boolean
    '''
    return self.wrap
  
  def isMatchCaseSet(self):
    '''
    Should the search be case sensitive ?
    
    @return: Is match case set?
    @rtype: boolean
    '''
    return self.matchcase
  
  
  

