'''
Defines reusable gtk widgets for ordering L{UIElement}s and setting them to
load or not load at runtime.

@var RAISE: Raise priority of a L{UIElement} for handling events or getting 
  loaded
@type RAISE: integer
@var LOWER: Lower priority of a L{UIElement} for handling events or getting 
  loaded
@type LOWER: integer
@var TO_LOAD: Indicates a L{UIElement} is now set to be loaded
@type TO_LOAD: integer
@var TO_UNLOAD: Indicates a L{UIElement} is now set to be unloaded
@type TO_UNLOAD: integer

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
import pygtk
pygtk.require('2.0')
import gtk, gobject
import gtk.glade
import weakref
from i18n import _, DOMAIN

# signal constants
RAISE = 0
LOWER = 1
TO_LOAD = 2
TO_UNLOAD = 3

class UIEView(object):
  '''
  Reusable viewer for L{UIElement}s. Shows the name and description of all
  elements. If constructed with the ordered flag set, allows reordering of 
  elements using raise and lower buttons. If constructed with the activatable
  flag set, allows for selecting UIEs to load or not load.
  
  @ivar chooser: Chooser that is showing this view
  @type chooser: weakref.proxy for L{AEChooser}
  @ivar loaded: Class names, names, and descriptions of all loaded 
    L{UIElement}s
  @type loaded: list of 3-tuple of string
  @ivar unloaded: Class names, names, and descriptions of all L{UIElement}s
  @type unloaded: list of 3-tuple of string
  @ivar uie_tv: List view of all L{UIElement}s
  @type uie_tv: gtk.TreeView
  @ivar lower_button: button to lower priority of L{Perk}s
  @type lower_button: gtk.Button
  @ivar raise_button: button to raise priority of L{Perk}s
  @type raise_button: gtk.Button
  @ivar changed: Has the view been maniuplated in any way?
  @type changed: boolean
  '''
  def __init__(self, chooser, ordered=True, activatable=True, wrap=450):
    '''
    Initializes the view by constructing the list view and populating it with
    data.
    
    @param chooser: Chooser that is showing this view
    @type chooser: L{AEChooser}
    @param ordered: Can loaded items be reordered?
    @type ordered: boolean
    @param activatable: Can items be set to load/unload?
    @type activatable: boolean
    @param wrap: Width in pixels at which to wrap description text
    @type wrap: integer
    '''
    self.changed = False
    # store a reference to the chooser for signaling
    self.chooser = weakref.proxy(chooser)
    
    # determine which root widget to use
    if ordered:
      name = 'ordered vbox'
    else:
      name = 'unordered scrollwindow'
    
    # load the glade file for this widget
    source = gtk.glade.XML(chooser._getResource('uie_widget.glade'), 
                           name, DOMAIN)
    
    # get top level widgets
    self.uie_tv = source.get_widget('uie treeview')
    self.root = source.get_widget(name)
    
    # configure the list view
    model = gtk.ListStore(str, str, str, bool)
    self.uie_tv.set_model(model)
    
    # name column
    col = gtk.TreeViewColumn(_('Name'))
    rend = gtk.CellRendererText()
    if activatable:
      rend2 = gtk.CellRendererToggle()
      rend2.set_property('activatable', True)
      col.pack_end(rend2, False)
      col.set_attributes(rend2, active=3)
      # watch for toggle events
      rend2.connect('toggled', self._onToggleLoad, model, 3)
    col.pack_start(rend, True)
    col.set_attributes(rend, text=1)
    self.uie_tv.append_column(col)
    # description column
    rend = gtk.CellRendererText()
    rend.set_property('wrap-width', wrap)
    col = gtk.TreeViewColumn(_('Description'), rend, text=2)
    self.uie_tv.append_column(col)
    
    # get buttons for raising/lowering UIEs
    self.raise_button = source.get_widget('raise button')
    self.lower_button = source.get_widget('lower button')
    
    # connect signal handlers
    source.signal_autoconnect(self)
    
  def isDirty(self):
    '''
    Gets if the order or load/unload status has been changed by the user.
    
    @return: Has the view been manipulated in any way?
    @rtype: boolean
    '''
    return self.changed

  def setData(self, loaded, unloaded):
    '''
    Sets the L{UIElement} data to be shown by the list view. Puts the cursor
    on the first item if it exists.
    
    @param loaded: Class names, names, and descriptions of all loaded 
      L{UIElement}s
    @type loaded: list of 3-tuple of string
    @param unloaded: Class names, names, and descriptions of all L{UIElement}s
    @type unloaded: list of 3-tuple of string
    '''
    model = self.uie_tv.get_model()
    # store loaded and unloaded perks with an extra boolean indicating state
    self.loaded = [metadata+(True,) for metadata in loaded]
    self.unloaded = [metadata+(False,) for metadata in unloaded]
    
    # populate the data model
    map(model.append, self.loaded)
    map(model.append, self.unloaded)
    
    # select the first item in the loaded list if it is not empty
    first = model.get_iter_first()
    if first is not None:
      self.uie_tv.set_cursor_on_cell(model.get_path(first))

  def _getLoadedCount(self):
    '''
    @return: Count of the number of L{Perk}s set to load
    @rtype: integer
    '''
    perks = self.uie_tv.get_model()
    return len([row[0] for row in perks if row[3]])
  
  def getWidget(self):
    '''
    Gets the root widget for the view, either the box when the view supports
    UIE ordering or the tree view when it does not.
    
    @return: Root widget of the view for integration in another UI
    @rtype: gtk.Widget
    '''
    return self.root
  
  def getAllUIEs(self):
    '''
    Returns the list of all L{UIElement}s.
    
    @return: All L{UIElement} class names
    @rtype: list of string
    '''
    return [row[0] for row in model]

  def getCurrentUIEs(self):
    '''
    Returns the new loaded/unloaded L{UIElement} configuration.
    
    @return: 2-tuple of loaded, unloaded L{UIElement} class names
    @rtype: 2-tuple of list, list
    '''
    model = self.uie_tv.get_model()
    loaded = [row[0] for row in model if row[3]]
    unloaded = [row[0] for row in model if not row[3]]
    return loaded, unloaded
  
  def _onToggleLoad(self, cell, path, model, column):
    '''
    Sets a L{UIElement} to load or unload. Moves the UIE to the top of the load
    order if it is now set to load or to the bottom of the entire list if it
    is set to unload.
    
    @param cell: Cell toggled
    @type cell: gtk.TreeViewCell
    @param path: Path to the row toggled
    @type path: tuple
    @param model: Model for the gtk.TreeView
    @type model: gtk.ListStore
    @param column: Column in the model that the toggle should affect
    @type column: integer
    '''
    self.changed = True
    val = not model[path][column]
    # toggle model data
    model[path][column] = val
    if self.raise_button is None:
      # don't do auto reordering if ordering is not supported
      return
    if val == True:
      # move to start of list
      iter = model.get_iter(path)
      model.move_after(iter, None)
      self.uie_tv.scroll_to_cell((0,))
      self.chooser._signal(TO_LOAD, name=model[0][1])
      self.raise_button.set_sensitive(False)
      self.lower_button.set_sensitive(True)
    else:
      # move to end of list
      iter = model.get_iter(path)
      last = reduce(lambda x, s: s + int(x), [r[3] for r in model], 0)
      new_path = (last,)
      new_iter = model.get_iter(new_path)
      model.move_after(iter, new_iter)
      self.uie_tv.scroll_to_cell(new_path)
      self.chooser._signal(TO_UNLOAD, name=model[last][1])
      self.raise_button.set_sensitive(False)
      self.lower_button.set_sensitive(False)

  def _onRaise(self, widget):
    '''
    Handles 'raise' button 'clicked' events.  Determines the current selected
    item in 'loaded' list, raises its index (loaded priority) so it displays at
    a higher position.
    
    @param widget: Source of GUI event
    @type widget: gtk.Widget
    '''
    model = self.uie_tv.get_model()
    path, col = self.uie_tv.get_cursor()
    if path is not None:
      self.changed = True
      row = path[0]
      new_path = (row-1,)
      # swap selected row with row immediately preceding it
      iter1 = model.get_iter(path)
      iter2 = model.get_iter(new_path)
      model.swap(iter1, iter2)
      # notify the perk so it can say something intelligent
      if row < self._getLoadedCount():
        below = model[row][1]
      else:
        below = None
      if row-2 >= 0:
        above = model[row-2][1]
      else:
        above = None
      self.chooser._signal(RAISE, above=above, below=below, 
                      name=model[row-1][1])
      self.uie_tv.scroll_to_cell(new_path)
      self._onCursorChanged(self.uie_tv)

  def _onLower(self, widget):
    '''
    Handles 'lower' button 'clicked' events. Determines the current selected 
    item in 'loaded' list, lower its index (loaded priority) so it displays at
    a lower position.
    
    @param widget: Source of GUI event
    @type widget: gtk.Widget
    '''
    model = self.uie_tv.get_model()
    path, col = self.uie_tv.get_cursor()
    if path is not None:
      self.changed = True
      row = path[0]
      new_path = (row+1,)
      # swap selected row with row immediately preceding it
      iter1 = model.get_iter(path)
      iter2 = model.get_iter(new_path)
      model.swap(iter1, iter2)
      # notify the perk so it can say something intelligent
      if row+2 < self._getLoadedCount():
        below = model[row+2][1]
      else:
        below = None
      if row >= 0:
        above = model[row][1]
      else:
        above = None
      self.chooser._signal(LOWER, above=above, below=below, 
                      name=model[row+1][1])
      # scroll the cell into view
      self.uie_tv.scroll_to_cell(new_path)
      # announce the new selection
      self._onCursorChanged(self.uie_tv)

  def _onCursorChanged(self, tv):  
    '''
    Determines whether or not raise/lower button should be enabled (in Gtk
    terms, whether or not the 'sensitive' flag is set to True).  If current
    selection is at top, disable 'raise' button; if selection is at bottom,
    disable 'lower' button.  This only affects usability, not functionality;
    that is, gtk doesn't dim the buttons automatically, the onRaise/onLower
    methods discard requests to move a table item beyond its proper scope.
    
    Does nothing if the widget is not ordered.

    @param tv: View of the loaded or unloaded L{UIElement}s
    @type tv: gtk.TreeView
    '''
    if self.lower_button is None:
      # not ordered, so bail
      return
    model = tv.get_model()
    path, col = tv.get_cursor()
    can_up = can_down = True
    n = self._getLoadedCount()
    if path[0] == 0:
      # top of the list
      can_up = False
      can_down = True
    elif path[0] == n-1:
      # last loaded item
      can_up = True
      can_down = False
    elif path is None or n == 0 or path[0] >= n:
      # unloaded item or empty list
      can_up = can_down = False
    # enable/disable raise/lower appropriately
    self.lower_button.set_sensitive(can_down)
    self.raise_button.set_sensitive(can_up)
