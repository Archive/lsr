'''
Input related constants.

@var CMD_GESTURE: Constant that represents an input command gesture.
@type CMD_GESTURE: integer
@var FILTER_NONE: Input filter mode used in filtering keyboard gestures, no 
filtering in this case.
@type FILTER_NONE: integer
@var FILTER_HANDLED: Input filter mode used in filtering keyboard gestures, 
filtered gesture is acted upon in this case.
@type FILTER_HANDLED: integer
@var FILTER_ALL: Input filter mode used in filtering keyboard gestures, 
all gestures are acted upon in this case.
@type FILTER_ALL: integer
@var INPUT_COMMAND_NAMES: A reverse mapping from command constants to string 
command names
@type INPUT_COMMAND_NAMES: dictionary

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
# input commands are negative numbers to prevent conflict with output commands
CMD_GESTURE = -1

# constants representing the input filter modes
FILTER_NONE, FILTER_HANDLED, FILTER_ALL = 0, 1, 2

# provide a reverse mapping from command constants to string command names
INPUT_COMMAND_NAMES = dict([(value, name.lower()[4:]) for name, value in 
                      locals().items() if name.startswith('CMD_')])
