'''
Help message constants. All HELP_* variables are strings shown at the command
line.

@var MSG_CONFIRM_REMOVE: Message displayed to confirm a removal at the command
  line
@type MSG_CONFIRM_REMOVE: string
@var HELP_GENERATE: Help string 'Generate a template for a new UIE'
@type HELP_GENERATE: string
@var HELP_INSTALL: Help string 'Install a UIE'
@type HELP_INSTALL: string
@var HELP_UNINSTALL: Help string 'Uninstall a UIE'
@type HELP_UNINSTALL: string
@var HELP_ASSOCIATE: Help string 'Associate a UIE with a profile'
@type HELP_ASSOCIATE: string
@var HELP_DISASSOCIATE: Help string 'Disassociate a UIE from a profile'
@type HELP_DISASSOCIATE: string
@var HELP_CREATE_PROFILE: Help string 'Create a new user profile'
@type HELP_CREATE_PROFILE: string
@var HELP_REMOVE_PROFILE: Help string 'Remove a user profile'
@type HELP_REMOVE_PROFILE: string
@var HELP_DUPLICATE_PROFILE: Help string 'Duplicate a user profile'
@type HELP_DUPLICATE_PROFILE: string
@var HELP_SHOW: Help string 'Show installed UIEs'
@type HELP_SHOW: string
@var HELP_PROFILE: Help string 'Profile name'
@type HELP_PROFILE: string
@var HELP_SAY: Help string 'Output a string using the default device'
@type HELP_SAY: string
@var HELP_INDEX: Help string 'Load order index for the UIE'
@type HELP_INDEX: string
@var HELP_TIER: Help string 'Process name with which a Perk should be 
associated'
@type HELP_TIER: string
@var HELP_ALL_TIERS: Help string 'Flag indicating a Perk should be associated 
with all apps'
@type HELP_ALL_TIERS: string
@var HELP_GLOBAL: Help string 'Flag indicating the command should be performed 
globally'
@type HELP_GLOBAL: string
@var HELP_LOG_LEVEL: Help string 'level of log messages'
@type HELP_LOG_LEVEL: string
@var HELP_LOG_CHANNEL: Help string 'channel of log messages, any of: %s'
@type HELP_LOG_CHANNEL: string
@var HELP_LOG_FILENAME: Help string 'filename for simple log output'
@type HELP_LOG_FILENAME: string
@var HELP_LOG_CONFIG: Help string 'filename for Python logging module 
configuration'
@type HELP_LOG_CONFIG: string
@var HELP_NO_INTRO: Help string 'suppress the welcome announcement'
@type HELP_NO_INTRO: string
@var HELP_INIT_PROFILES: Help string 'initialize built-in profiles'
@type HELP_INIT_PROFILES: string
@var HELP_INIT_GLOBAL: Help string 'initialize installed UIEs with packaged 
UIEs'
@type HELP_INIT_GLOBAL: string
@var HELP_A11Y: Help string 'Desktop accessibility disabled. Enabling 
Please logout and run LSR again.'
@type HELP_A11Y: string
@var HELP_NO_A11Y: Help string 'skips check for desktop accessibility'
@type HELP_NO_A11Y: string
@var HELP_NO_BRIDGE: Help string 'prevents UIE windows from becoming 
accessible'
@type HELP_NO_BRIDGE: string
@var HELP_EXPORT_PATH: Help string 'prints the path to the LSR package'
@type HELP_EXPORT_PATH: string

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
from i18n import _

# command line help strings
HELP_GENERATE = _('generate a template for a new UIE')
HELP_INSTALL = _('install a UIE')
HELP_UNINSTALL = _('uninstall a UIE')
HELP_ASSOCIATE = _('associate a UIE with a profile')
HELP_DISASSOCIATE = _('disassociate a UIE from a profile')
HELP_CREATE_PROFILE = _('create a new user profile')
HELP_REMOVE_PROFILE = _('remove a user profile')
HELP_DUPLICATE_PROFILE = _('duplicate a user profile')
HELP_SHOW = _('show installed UIEs')
HELP_PROFILE = _('profile name')
HELP_SAY = _('output a string using the default device')
HELP_INDEX = _('load order index for the UIE')
HELP_TIER = _('process name with which a Perk should be associated')
HELP_ALL_TIERS = _('flag indicating a Perk should be associated with all apps')
HELP_GLOBAL = _('flag indicating the command should be performed globally')
HELP_LOG_LEVEL = _('level of log messages: %s')
HELP_LOG_CHANNEL = _('channel of log messages, any of: %s')
HELP_LOG_FILENAME = _('filename for simple log output')
HELP_LOG_CONFIG = _('filename for Python logging module configuration')
HELP_NO_INTRO = _('suppress the welcome announcement')
HELP_INIT_PROFILES = _('initialize built-in profiles')
HELP_INIT_GLOBAL = _('initialize installed UIEs with packaged UIEs')
HELP_A11Y = _('Desktop accessibility disabled. Enabling...' \
              '\nPlease logout and run LSR again.')
HELP_NO_A11Y = _('skips check for desktop accessibility')
HELP_NO_BRIDGE = _('prevents UIE windows from becoming accessible')
HELP_EXPORT_PATH = _('prints the path to the LSR package')

# command line messages
MSG_CONFIRM_REMOVE = _('Are you sure you want to remove %s? ') + '(y/n) '