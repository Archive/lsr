'''
L{Task.Tools} related constants.

@var CHAIN_BEFORE: Links one L{Task} before the execution of another.
@type CHAIN_BEFORE: integer
@var CHAIN_AFTER: Links one L{Task} after the execution of another.
@type CHAIN_AFTER: integer
@var CHAIN_AROUND: Links on L{Task} around the execution of another.
@type CHAIN_AROUND: integer
@var UIE_LOADED: Constant indicating L{UIElement}s loaded
@type UIE_LOADED: integer
@var UIE_INSTALLED: Constant indicating L{UIElement}s installed
@type UIE_INSTALLED: integer
@var UIE_TIER_ASSOCIATED: Constant indicating L{Perk}s associated with the 
current L{Tier} in the active profile
@type UIE_TIER_ASSOCIATED: integer
@var UIE_ALL_ASSOCIATED: Constant indicating L{UIElement}s associated with the 
active profile
@type UIE_ALL_ASSOCIATED: integer

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
CHAIN_BEFORE = 0
CHAIN_AFTER = 1
CHAIN_AROUND = 2

UIE_LOADED = 0
UIE_INSTALLED = 1
UIE_TIER_ASSOCIATED = 2
UIE_ALL_ASSOCIATED = 3
