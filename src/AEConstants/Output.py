'''
Output related constants.

@var CMD_NOOP: No-op command for all devices.
@type CMD_NOOP: integer
@var CMD_STOP: Instruct the output device to stop.
@type CMD_STOP: integer
@var CMD_TALK: Instruct the output device to send buffered data.
@type CMD_TALK: integer
@var CMD_STRING: Instuction indicating that a string is being sent to 
an output device.
@type CMD_STRING: integer
@var CMD_FILENAME: Instuction indicating that a file name is being sent to 
an output device.
@type CMD_FILENAME: integer
@var CMD_GET_CLOSEST_LANG: Instruction indicating that an output device should
output the closest language possible.
@type CMD_GET_CLOSEST_LANG: integer
@var CMD_CARET: Braille command used to retrieve current caret position.
@type CMD_CARET: integer
@var CMD_TRUNCATE: Braille command used to retrieve right and left text 
trucation indicators.
@type CMD_TRUNCATE: integer
@var CMD_GET_ELLIPSIS_SIZE: Braille command to retrieve both left and right
ellipsis sizes as number of cells.
@type CMD_GET_ELLIPSIS_SIZE: integer
@var CMD_GET_MISSINGCELL_COUNT: Braille command to retrieve the number of 
missing or broken cells.
@type CMD_GET_MISSINGCELL_COUNT: integer
@var CMD_INDEX: Instruction indicating that an output device is receiving an 
index marker.
@type CMD_INDEX: integer
@var CMD_STRING_SYNC: Instruction indicating that an output device should 
buffer the given string.
@type CMD_STRING_SYNC: integer
@var CMD_GOTO: Magnifier command instructing magnifier to goto a specified 
screen position.
@type CMD_GOTO: integer
@var CMD_GET_ROI: Magnifier command used to retrieve the region of interest
from the magnifier device.
@type CMD_GET_ROI: integer
@var CARET_NONE: Braille command indicating no caret will be used.
@type CARET_NONE: integer
@var CARET_TWO_BOTTOM: Braille command indicating the bottom two pins will be
used to represent the caret.
@type CARET_TWO_BOTTOM: integer
@var CARET_BOTTOM_RIGHT: Braille command indicating the bottom right pin will be
used to represent the caret.
@type CARET_BOTTOM_RIGHT: integer
@var CARET_LAST: Constant representing last caret type.  Used for iterating 
through caret types.
@type CARET_LAST: integer
@var ELLIPSIS_CONTINUATION: Braille command indicating that the continuation
symbol will be used for the ellipsis.
@type ELLIPSIS_CONTINUATION: integer
@var ELLIPSIS_ELLIPSIS: Braille command indicating that the ellipsis
symbol will be used for the ellipsis.
@type ELLIPSIS_ELLIPSIS: integer
@var ELLIPSIS_LAST: Constant representing last ellipsis type.  Used for iterating 
through ellipsis types.
@type ELLIPSIS_LAST: integer
@var OUTPUT_COMMAND_NAMES: Dictionary containing all output commands.
@type OUTPUT_COMMAND_NAMES: dictionary

@author: Peter Parente
@author: Scott Haeger
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
# no-op command for all devices
CMD_NOOP = 0

# common commands for serial output devices (e.g. audio, Braille)
CMD_STOP = 1
CMD_TALK = 2
CMD_STRING = 3
CMD_FILENAME = 5
CMD_GET_CLOSEST_LANG = 8

# commands for Braille devices
BRAILLE_CMD = 0x0010
CMD_CARET = (BRAILLE_CMD | 0x0001)
CMD_TRUNCATE = (BRAILLE_CMD | 0x0002)
CMD_GET_ELLIPSIS_SIZE = (BRAILLE_CMD | 0x0003)
CMD_GET_MISSINGCELL_COUNT = (BRAILLE_CMD | 0x0004)

# commands for audio devices
CMD_INDEX = 7
CMD_STRING_SYNC = 4

# commands for magnifiers
CMD_GOTO = 9
CMD_GET_ROI = 10

# braille constants
# Caret display styles
CARET_NONE = 0
CARET_TWO_BOTTOM = 1
CARET_BOTTOM_RIGHT = 2
CARET_ALL = 3
CARET_LAST = CARET_ALL

# ellipsis display styles
ELLIPSIS_CONTINUATION = 0
ELLIPSIS_ELLIPSIS = 1
ELLIPSIS_LAST = ELLIPSIS_ELLIPSIS

# provide a reverse mapping from command constants to string command names
OUTPUT_COMMAND_NAMES = dict([(value, name.lower()[4:].replace('_', ' '))
                             for name, value in 
                             locals().items() if name.startswith('CMD_')])

# settings common to all devices

# settings common to audio device styles
