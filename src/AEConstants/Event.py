'''
L{AEEvent} related constants.

@var EVENT_TEXT_BOUNDS: Bounds on a body of text changed
@type EVENT_TEXT_BOUNDS: integer
@var EVENT_VISIBLE_DATA: Visible data on the screen changed in some manner
@type EVENT_VISIBLE_DATA: integer
@var EVENT_OBJECT_BOUNDS: Bounds on an object changed
@type EVENT_OBJECT_BOUNDS: integer
@var EVENT_MOUSE_MOVE: Mouse pointer moved
@type EVENT_MOUSE_MOVE: integer
@var EVENT_MOUSE_PRESS: Mouse button clicked
@type EVENT_MOUSE_PRESS: integer
@var EVENT_MOUSE_RELEASE: Mouse button released
@type EVENT_MOUSE_RELEASE: integer
@var EVENT_ACTIVE_ITEM_SELECT: Active item changed inside a container-like 
  object
@type EVENT_ACTIVE_ITEM_SELECT: integer
@var EVENT_ADD_ITEM_SELECT: A new item was added to a selection
@type EVENT_ADD_ITEM_SELECT: integer
@var EVENT_REMOVE_ITEM_SELECT: An item was removed from a selection
@type EVENT_REMOVE_ITEM_SELECT: integer
@var EVENT_CHANGE_TEXT_SELECT: A string of text was added or removed from a 
  selection
@type EVENT_CHANGE_TEXT_SELECT: integer
@var EVENT_VIEW_GAINED: View was gained by activating it
@type EVENT_VIEW_GAINED: integer
@var EVENT_VIEW_LOST: View was lost by deactivating it
@type EVENT_VIEW_LOST: integer
@var EVENT_VIEW_STARTUP: View was identified at LSR startup
@type EVENT_VIEW_STARTUP: integer
@var EVENT_VIEW_FIRST_GAINED: View was gained by activating it during LSR 
  startup
@type EVENT_VIEW_FIRST_GAINED: integer
@var EVENT_SYNTHMOUSE_B1P: Synthesized mouse button 1 press.
@type EVENT_SYNTHMOUSE_B1P: string
@var EVENT_SYNTHMOUSE_B1R: Synthesized mouse button 1 release.
@type EVENT_SYNTHMOUSE_B1R: string
@var EVENT_SYNTHMOUSE_B1C: Synthesized mouse button 1 click.
@type EVENT_SYNTHMOUSE_B1C: string
@var EVENT_SYNTHMOUSE_B1D: Synthesized mouse button 1 double-click.
@type EVENT_SYNTHMOUSE_B1D: string
@var EVENT_SYNTHMOUSE_B2P: Synthesized mouse button 2 press.
@type EVENT_SYNTHMOUSE_B2P: string
@var EVENT_SYNTHMOUSE_B2R: Synthesized mouse button 2 release.
@type EVENT_SYNTHMOUSE_B2R: string
@var EVENT_SYNTHMOUSE_B2C: Synthesized mouse button 2 click.
@type EVENT_SYNTHMOUSE_B2C: string
@var EVENT_SYNTHMOUSE_B2D: Synthesized mouse button 2 double-click.
@type EVENT_SYNTHMOUSE_B2D: string
@var EVENT_SYNTHMOUSE_B3P: Synthesized mouse button 3 press.
@type EVENT_SYNTHMOUSE_B3P: string
@var EVENT_SYNTHMOUSE_B3R: Synthesized mouse button 3 release.
@type EVENT_SYNTHMOUSE_B3R: string
@var EVENT_SYNTHMOUSE_B3C: Synthesized mouse button 3 click.
@type EVENT_SYNTHMOUSE_B3C: string
@var EVENT_SYNTHMOUSE_B3D: Synthesized mouse button 3 double-click.
@type EVENT_SYNTHMOUSE_B3D: string
@var EVENT_SYNTHMOUSE_ABS: Move mouse pointer to absolute screen position.
@type EVENT_SYNTHMOUSE_ABS: string
@var EVENT_SYNTHMOUSE_REL: Move mouse pointer to relative screen position.
@type EVENT_SYNTHMOUSE_REL: string

@author: Peter Parente
@author: Scott Haeger
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
# layer constants, with guaranteed order from most specific to least specific
LAYER_FOCUS = 0
LAYER_TIER = 1
LAYER_BACKGROUND = 2
LAYERS_ALL = [LAYER_FOCUS, LAYER_TIER, LAYER_BACKGROUND]
LAYER_NAMES = ['focus', 'tier', 'background']

# priority constants
EXEC_NORMAL = 0
EXEC_HIGH = 1
EXEC_IMMEDIATE = 2

# screen change events
EVENT_OBJECT_BOUNDS = 0
EVENT_VISIBLE_DATA = 1 
EVENT_TEXT_BOUNDS = 2

# mouse events
EVENT_MOUSE_MOVE = 0
EVENT_MOUSE_PRESS = 1
EVENT_MOUSE_RELEASE = 2

# selection events
EVENT_ACTIVE_ITEM_SELECT = 0
EVENT_ADD_ITEM_SELECT = 1 
EVENT_REMOVE_ITEM_SELECT = 2
EVENT_CHANGE_TEXT_SELECT = 3

# view events
EVENT_VIEW_LOST = 0
EVENT_VIEW_GAINED = 1 
EVENT_VIEW_STARTUP = 2
EVENT_VIEW_FIRST_GAINED = 3

# synthesized mouse events
# maps directly to AT-SPI output strings, other adapters will need to
# provide their own mapping
EVENT_SYNTHMOUSE_B1P = 'b1p'
EVENT_SYNTHMOUSE_B1R = 'b1r'
EVENT_SYNTHMOUSE_B1C = 'b1c'
EVENT_SYNTHMOUSE_B1D = 'b1d'
EVENT_SYNTHMOUSE_B2P = 'b2p'
EVENT_SYNTHMOUSE_B2R = 'b2r'
EVENT_SYNTHMOUSE_B2C = 'b2c'
EVENT_SYNTHMOUSE_B2D = 'b2d'
EVENT_SYNTHMOUSE_B3P = 'b3p'
EVENT_SYNTHMOUSE_B3R = 'b3r'
EVENT_SYNTHMOUSE_B3C = 'b3c'
EVENT_SYNTHMOUSE_B3D = 'b3d'
EVENT_SYNTHMOUSE_ABS = 'abs'
EVENT_SYNTHMOUSE_REL = 'rel'
