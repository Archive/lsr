'''
Color related constants.

@var COLOR_MAP: Mapping from a six bit palette of colors to names describing
  them
@type COLOR_MAP: dictionary

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
from i18n import _
COLOR_MAP = {
  0:_('black'),
  1:_('dark purple'),
  2:_('dark blue'),
  3:_('blue'), 
  4:_('dark green'),
  5:_('dark cyan'),
  6:_('aqua'), 
  7:_('medium blue'),
  8:_('dark green'),
  9:_('lime green'), 
  10:_('dark cyan'),
  11:_('aqua'),
  12:_('green'), 
  13:_('bright green'),
  14:_('blue green'),
  15:_('cyan'),
  16:_('dark red'),
  17:_('magenta'),
  18:_('purple'),
  19:_('purple'), 
  20:_('olive green'),
  21:_('medium grey'),
  22:_('steel blue'),
  23:_('blue grey'),
  24:_('green'),
  25:_('pale green'), 
  26:_('blue grey'),
  27:_('light blue'),
  28:_('light green'), 
  29:_('light green'),
  30:_('cyan'),
  31:_('light blue'), 
  32:_('dark red'),
  33:_('magenta'),
  34:_('dark pink'),
  35:_('purple'),
  36:_('light brown'),
  37:_('flesh'),
  38:_('pink'), 
  39:_('light purple'),
  40:_('olive green'),
  41:_('light grey'),
  42:_('dark grey'),
  43:_('light blue'),
  44:_('light green'), 
  45:_('pale green'),
  46:_('pale green'),
  47:_('light blue'), 
  48:_('red'), 
  49:_('pink'), 
  50:_('pink'), 
  51:_('pink'), 
  52:_('orange'), 
  53:_('puce'), 
  54:_('pink'), 
  55:_('pink'), 
  56:_('dark yellow'), 
  57:_('tan'), 
  58:_('light pink'), 
  59:_('light pink'),
  60:_('yellow'),
  61:_('pale yellow'), 
  62:_('off white'),
  63:_('white') }
