'''
Profile constants.

@var DEFAULT_PROFILE: Name of the default profile to use when none is specified
@type DEFAULT_PROFILE: string
@var BUILTIN_PROFILES: Names of built-in, protected profiles
@type BUILTIN_PROFILES: list of string
@var DEFAULT_ASSOCIATIONS: Mapping from built-in profile name to L{UIElement}s
  that ship with LSR that should be part of the profile by default
@type DEFAULT_ASSOCIATIONS: dictionary
@var GENERATE_PROFILES: Names of profiles to which generated L{Perk}s should be
  added automatically
@type GENERATE_PROFILES: list of string

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
# associations for default profiles that ship with LSR
DEFAULT_ASSOCIATIONS = {
  'user': ['Keyboard', 
           'CliqueAudio', 
           'IBMSpeech', 
           'DECtalkSpeech', 
           'EloquenceSpeech', 
           'LoquendoSpeech', 
           'SwiftSpeech', 
           'ThetaSpeech',
           'EspeakSpeech', 
           'FreeTTSSpeech', 
           'SpeechDispatcher', 
           'FestivalSpeech', 
           'BrlAPIDevice', 
           'ReviewPerk',
           'BasicSpeechPerk', 
           'BasicBraillePerk', 
           'BookmarkPerk',
           'DefaultDialogPerk', 
           'AltShiftPerk', 
           'MetacityPerk', 
           'GaimPerk', 
           'GTerminalPerk', 
           'FirefoxPerk',
           'SearchPerk',
           'GKeybindingPerk',
           'GCalcPerk',
           'GEditPerk'],
  'developer' :
          ['Keyboard', 
           'CliqueAudio', 
           'IBMSpeech', 
           'DECtalkSpeech', 
           'EloquenceSpeech', 
           'LoquendoSpeech', 
           'SwiftSpeech', 
           'ThetaSpeech',
           'EspeakSpeech', 
           'FreeTTSSpeech', 
           'SpeechDispatcher', 
           'FestivalSpeech', 
           'BrlAPIDevice', 
           'ReviewPerk',
           'BasicSpeechPerk', 
           'BasicBraillePerk', 
           'BookmarkPerk',
           'DefaultDialogPerk', 
           'AltShiftPerk',
           'DeveloperPerk',
           'MetacityPerk', 
           'GaimPerk', 
           'GTerminalPerk', 
           'FirefoxPerk',
           'RawEventMonitor',
           'TaskMonitor', 
           'IOMonitor',
           'SearchPerk',
           'GKeybindingPerk',
           'GCalcPerk',
           'GEditPerk'],
  'monitors' :
          ['RawEventMonitor',
           'TaskMonitor', 
           'IOMonitor', 
           'Keyboard',
           'ReviewPerk', 
           'BasicSpeechPerk', 
           'BasicBraillePerk', 
           'DefaultDialogPerk', 
           'AltShiftPerk', 
           'DeveloperPerk', 
           'MetacityPerk', 
           'GaimPerk', 
           'GTerminalPerk', 
           'FirefoxPerk',
           'GKeybindingPerk',
           'GCalcPerk',
           'GEditPerk'],
  'autotest' :
          ['TestLogMonitor', 
           'Keyboard', 
           'ReviewPerk', 
           'BasicSpeechPerk', 
           'BookmarkPerk', 
           'DefaultDialogPerk', 
           'AltShiftPerk', 
           'MetacityPerk',
           'GaimPerk', 
           'GTerminalPerk'
           'FirefoxPerk',
           'GKeybindingPerk',
           'GCalcPerk',
           'GEditPerk'],
  'login' :
          ['Keyboard', 
           'CliqueAudio', 
           'IBMSpeech', 
           'DECtalkSpeech', 
           'EloquenceSpeech', 
           'LoquendoSpeech', 
           'SwiftSpeech', 
           'ThetaSpeech',
           'EspeakSpeech', 
           'FreeTTSSpeech', 
           'SpeechDispatcher', 
           'FestivalSpeech', 
           'BrlAPIDevice', 
           'ReviewPerk', 
           'BasicSpeechPerk', 
           'BasicBraillePerk', 
           'AltShiftPerk', 
           'GdmPerk',
           'SearchPerk'],
  'mag' :
          ['Keyboard', 
           'GnomeMagDevice', 
           'BasicMagPerk', 
           'DefaultDialogPerk',
           'AltShiftPerk']
        }
# names of built-in and default LSR profiles
BUILTIN_PROFILES = DEFAULT_ASSOCIATIONS.keys()
DEFAULT_PROFILE = 'user'
GENERATE_PROFILES = ['user', 'developer']
