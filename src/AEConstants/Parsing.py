'''
Word parsing related constants.

@var FORMAT_TEXT: Words will be output without spelling
@type FORMAT_TEXT: integer
@var FORMAT_PRONOUNCE: Punctuation will be pronounced
@type FORMAT_PRONOUNCE: integer
@var FORMAT_SPELL: All characters in a word will be spelled
@type FORMAT_SPELL: integer
@var FORMAT_PHONETIC: All characters in a word will be spelled using a phonetic
  spelling table
@type FORMAT_PHONETIC: integer
@var WORD_NON_BLANK: Definition indicating all non-blank characters should be 
  considered the main part of a word
@type WORD_NON_BLANK: integer
@var WORD_ALPHABETIC: Definition indicating all letters should be considered 
  the main part of a word
@type WORD_ALPHABETIC: integer
@var WORD_ALPHA_NUMERIC: Definition indicating all letters and numbers should 
  be considered the main part of a word
@type WORD_ALPHA_NUMERIC: integer
@var WORD_ALPHA_PUNCT: Definition indicating all letters and punctuation should
  be considered the main part of a word
@type WORD_ALPHA_PUNCT: integer
@var WORD_ALPHA_NUMERIC_PUNCT: Definition indicating all letters, numbers, and
  punctuation should be considered the main part of a word
@type WORD_ALPHA_NUMERIC_PUNCT: integer

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''

# constants for Word definitions
WORD_NON_BLANK = 0
WORD_ALPHABETIC = 1
WORD_ALPHA_NUMERIC = 2
WORD_ALPHA_PUNCT = 3
WORD_ALPHA_NUMERIC_PUNCT = 4
WORD_LAST = 4

# constants for spelling format
FORMAT_TEXT = 0
FORMAT_PRONOUNCE = 1
FORMAT_SPELL = 2
FORMAT_PHONETIC = 3
FORMAT_LAST = 3
