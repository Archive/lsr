'''
Semantic tag constants.

@var STYLE_GROUP_ALL: List of all style group constants
@type STYLE_GROUP_ALL: list
@var SEMANTIC_STYLES: Mapping from semantic to its default group
@type SEMANTIC_STYLES: dictionary
@var SEMANTIC_NAMES: Mapping from semantic constant to its human readable name,
  currently the constant variable name
@type SEMANTIC_NAMES: dictionary

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
# default style groupings
STYLE_GROUP_CONTENT = 0
STYLE_GROUP_PROPERTIES = 1
STYLE_GROUP_ECHO = 2
STYLE_GROUP_CONTEXT = 3
STYLE_GROUP_ALL = [STYLE_GROUP_CONTENT, STYLE_GROUP_PROPERTIES,
                   STYLE_GROUP_ECHO, STYLE_GROUP_CONTEXT]

# enumeration of all default semantics
# CONTENT 10-29
SEM_CHAR = 1
SEM_WORD = 2
SEM_ITEM = 3
SEM_NAME = 5
SEM_LABEL = 6
SEM_DESC = 7
SEM_VALUE = 8
SEM_LINK = 9
# PROPERTIES 30-59
SEM_ROLE = 30
SEM_FONT = 31
SEM_TEXT_ATTR = 32
SEM_COLOR = 33
SEM_STATE = 34
SEM_COUNT = 35
SEM_INDEX = 36
SEM_LEVEL = 37
SEM_STATE = 38
SEM_STATUS = 39
SEM_HOTKEY = 40
SEM_EXTENTS = 41
# ECHO 60-79
SEM_INFO = 60
SEM_ERROR = 61
SEM_CONFIRM = 62
# CONTEXT 80-99
SEM_APP = 80
SEM_WINDOW = 81
SEM_SECTION = 82

# mapping of semantics to default style groups
SEMANTIC_STYLES = {
  # CONTENT
  SEM_NAME :      STYLE_GROUP_CONTENT,
  SEM_CHAR :      STYLE_GROUP_CONTENT,
  SEM_WORD :      STYLE_GROUP_CONTENT,
  SEM_ITEM :      STYLE_GROUP_CONTENT,
  SEM_LABEL :     STYLE_GROUP_CONTENT,
  SEM_DESC :      STYLE_GROUP_CONTENT,
  SEM_VALUE:      STYLE_GROUP_CONTENT,
  # PROPERTIES
  SEM_ROLE :      STYLE_GROUP_PROPERTIES,
  SEM_FONT :      STYLE_GROUP_PROPERTIES,
  SEM_TEXT_ATTR : STYLE_GROUP_PROPERTIES,
  SEM_COLOR :     STYLE_GROUP_PROPERTIES,
  SEM_STATE :     STYLE_GROUP_PROPERTIES,
  SEM_COUNT :     STYLE_GROUP_PROPERTIES,
  SEM_INDEX :     STYLE_GROUP_PROPERTIES,
  SEM_LEVEL :     STYLE_GROUP_PROPERTIES,
  SEM_STATE :     STYLE_GROUP_PROPERTIES,
  SEM_STATUS :    STYLE_GROUP_PROPERTIES,
  SEM_HOTKEY :    STYLE_GROUP_PROPERTIES,
  SEM_EXTENTS :   STYLE_GROUP_PROPERTIES,
  # CONTEXT
  SEM_APP :       STYLE_GROUP_CONTEXT,
  SEM_WINDOW :    STYLE_GROUP_CONTEXT,
  SEM_SECTION :   STYLE_GROUP_CONTEXT,
  # ECHO
  SEM_INFO :      STYLE_GROUP_ECHO,
  SEM_ERROR :     STYLE_GROUP_ECHO,
  SEM_CONFIRM :   STYLE_GROUP_ECHO
}

# provide a reverse mapping from semantic value to semantic variable name
SEMANTIC_NAMES = dict([(value, name.lower()[4:]) for name, value in 
                       locals().items() if name.startswith('SEM_')])