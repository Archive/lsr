'''
Wraps all global constants in a convenient package.

@author: Pete Brunet
@author: Peter Parente
@author: Brett Clippingdale
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
from Color import *
from Event import *
from Help import *
from Input import *
from Output import *
from Parsing import *
from Platform import *
from Profile import *
from Semantic import *
from Spelling import *
from Tools import *

def register(kwargs):
  '''
  Registers new constant names and values. As soon as a repeat constants 
  between those that are already registered and those that are going to be 
  registered is found, registration ceases. This is an optimization for
  L{Perk}s that appear in all L{Tier}s but don't want to wait to have all 
  their constants registered numerous times.
  
  @param kwargs: Constant name/value pairs
  @type kwargs: dictionary
  '''
  g = globals()
  for name, val in kwargs.items():
    if name in g:
      continue
    g[name] = val
