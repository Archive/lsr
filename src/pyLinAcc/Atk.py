'''
Defines convenience methods for using Gnome ATK/GAIL accessibility platform,
using the atk.py bindings.  Allows Python developers to easily write accessible
applications without knowledge of arcane ATK programming patterns and syntax.

@author: Brett Clippingdale
@author: Pete Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
import atk

def setNameDesc(widget, name=None, description=None, row=None, col=0):
  '''
  Sets the name, and description (optional), of a GTK+ widget.  Example code::
  
    import pyatk
    pyatk.setNameDesc(apply_button, 'Apply')
    pyatk.setNameDesc(up_button, 'Up', 'Move the selected item up")
    pyatk.setNameDesc(my_table, 'Y5Q3', 'Year Five, Quarter Three", 4, 2)
    
  @param widget: Some GTK+ widget
  @type widget: gtk.Object
  @param name: Accessible name to set
  @type name: string
  @param description: Accessible description to set
  @type description: string
  @param row: row, if in a table/tree
  @type row: integer
  @param col: column, if in a table/tree
  @type col: integer
  '''
  acc = widget.get_accessible()
  if row is not None: # it's a table/tree
    model = widget.get_model()
    numCols = model.get_n_columns() # each column has a header row
    # skip the table headers
    acc = acc.ref_accessible_child( numCols + (row * numCols) + col) 
  if name is not None:
    acc.set_name(name) # widget.set_name() is not sufficient
  if description is not None:
    acc.set_description(description)

def setRelation(source, atk_relation, *targets):
  '''
  Sets the relation between a GTK+ source widget and one or more GTK+ target
  widgets. Example code::
  
    import atk, pyatk
    pyatk.setRelation(item, atk.RELATION_LABELLED_BY, item_label)
    pyatk.setRelation(group_label, atk.RELATION_LABEL_FOR, member1, member2)
    pyatk.setRelation(spinner_button, atk.RELATION_CONTROLLER_FOR, spinner)

  For a list and definition of ATK relationships, see: 
  U{http://developer.gnome.org/doc/API/2.0/atk/AtkRelation.html}
    
  @param source: Source accessible on which the relation will be set
  @type source: atk.Object
  @param atk_relation: Relation to set
  @type atk_relation: atk.Relation
  @param targets: Target accessibles of the relation
  @type targets: list of atk.Object
  '''
  acc_targets = [] # list of accessible widgets to apply relation attribute
  acc_src = source.get_accessible()
  relation_set = acc_src.ref_relation_set()
  # get a reference to each widget and add to the accessible target list
  for target in targets: 
    acc_targ = target.get_accessible()
    acc_targets.append(acc_targ)
  relation = atk.Relation(acc_targets, 1) # create the relation
  relation.set_property('relation-type', atk_relation)
  relation_set.add(relation) # apply the relation to the target widget(s)
