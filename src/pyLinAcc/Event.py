'''
Defines classes for monitoring and wrapping AT-SPI events. Hides most of the
details of registering and unregistering for events behind a simple interface.

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at 
U{http://www.opensource.org/licenses/bsd-license.php}
'''

# these modules are generated from IDL by the __init__ module
# see http://developer.gnome.org/doc/guides/corba/html/c561.html about POA
import Accessibility, Accessibility__POA
import pyLinAcc, Constants, Interfaces
import weakref, traceback

class Event(object):
  '''
  Wraps an AT-SPI event with a more Pythonic interface. Both AT-SPI device 
  events (e.g. keyboard) and "normal" AT-SPI events (everything else) are 
  handled by this class.
  
  @note: All unmarked attributes of this class should be considered public
    readable and writable as the class is acting as a record object.
  @ivar consume: Should this event be consumed and not allowed to pass on to
      observers further down the dispatch chain?
  @type consume: boolean
  @ivar type: The type of the AT-SPI event
  @type type: L{EventType}
  @ivar detail1: First AT-SPI event parameter
  @type detail1: integer
  @ivar detail2: Second AT-SPI event parameter
  @type detail2: integer
  @ivar any_data: Extra AT-SPI data payload
  @type any_data: object
  @ivar source: Source of the event
  @type source: Accessibility.Accessible
  '''
  def __init__(self, event):
    '''
    Extracts information from the provided event. If the event is a "normal" 
    event, pulls the detail1, detail2, any_data, and source values out of the
    given object and stores it in this object. If the event is a device event,
    key ID is stored in detail1, scan code is stored in detail2, key name, 
    key modifiers (e.g. ALT, CTRL, etc.), is text flag, and timestamp are 
    stored as a 4-tuple in any_data, and source is None (since key events are
    global).

    @param event: Event from an AT-SPI callback
    @type event: Accessibility.Event or Accessibility.DeviceEvent
    '''
    # always start out assuming no consume
    self.consume = False
    try:
      # wrapping will fail if the event is actually a device event
      self.type = EventType(event.type)
      self.detail1 = event.detail1
      self.detail2 = event.detail2
      # store the event source and increase the reference count since event 
      # sources are borrowed references; the AccessibleMixin automatically
      # decrements it later
      self.source = event.source
      self.source.ref()
      # process any_data in a at-spi version independent manner
      self.any_data = event.any_data.value()
      try:
        self.any_data = self.any_data.any_data.value()
      except Exception:
        pass
      try:
        # increase the any_data reference count since the objects are borrowed 
        # references; the AccessibleMixin automatically decrements it later
        self.any_data.ref()
      except AttributeError:
        pass
    except AttributeError:
      # a device event, make up the type
      self.type = EventType(Constants.device_type_to_name[event.type])
      self.detail1 = event.id
      self.detail2 = event.hw_code
      # pack all other data into a tuple in any_data
      self.any_data = (event.event_string, event.modifiers, event.is_text, 
                       event.timestamp)
      # key events have no source
      self.source = None

  def __str__(self):
    '''
    Builds a human readable representation of the event including event type,
    parameters, and, optionally, source info. This method is used by an
    L{AEMonitor} to buffer string representations of events.

    @return: Event description
    @rtype: string
    '''
    try:
      s = '%s(%s, %s, %s)\n\tsource: %s\n\tapplication: %s' % \
        (self.type.asString(), self.detail1, self.detail2, self.any_data,
         self.source, self.source.getApplication())
    except Exception:
      if self.any_data is None:
        s = '%s(%s, %s, None)' % \
        (self.type.asString(), self.detail1, self.detail2)
      else:
        s = '%s(%s, %s, %s, %s, %s, %s)' % \
          ((self.type.asString(), self.detail1, self.detail2)+self.any_data)
    return s

class EventType(object):
  '''
  Wraps the AT-SPI event type string so its components can be accessed 
  individually as klass (can't use the keyword class), major, minor, and detail 
  (klass:major:minor:detail).
  
  @note: All attributes of an instance of this class should be considered public
    readable as it is acting a a struct.
  @ivar klass: Most general event type identifier (object, window, mouse, etc.)
  @type klass: string
  @ivar major: Second level event type description
  @type major: string
  @ivar minor: Third level event type description
  @type minor: string
  @ivar detail: Lowest level event type description
  @type detail: string
  @ivar name: Full, unparsed event name as received from AT-SPI
  @type name: string
  @cvar format: Names of the event string components
  @type format: 4-tuple of string
  '''
  format = ('klass', 'major', 'minor', 'detail')
  def __init__(self, name):
    '''
    Parses the full AT-SPI event name into its components 
    (klass:major:minor:detail). If the provided event name is an integer instead
    of a string, then the event is really a device event. An exception is raised
    in this case so L{Event} knows to convert the device event integer constant
    to a keyboard event name string.
    
    @param name: Full AT-SPI event name
    @type name: string
    @raise AttributeError: When the given event name is not a valid string 
    '''
    # get rid of any leading and trailing ':' separators
    self.name = name.strip(':')
    self.klass = None
    self.major = None
    self.minor = None
    self.detail = None
    
    # split type according to delimiters
    split = self.name.split(':')
    # loop over all the components
    for i in xrange(len(split)):
      # store values of attributes in this object
      setattr(self, self.format[i], split[i])
      
  def __str__(self):
    '''
    @return: Full event name as human readable representation of this event type
    @rtype: string
    '''
    return self.asString()
    
  def asString(self):
    '''
    Builds a human readable representation of the event type.
    
    @return: Event type description
    @rtype: string
    '''
    return self.name
    
def _createObserver(manager, et):
  '''
  Factory function that builds the right kind of observer based on whether
  events to monitor are at the device level or not.
    
  @param manager: Manager that will own this observer
  @type manager: L{Manager}
  @param et: Type of event the created observer will monitor
  @type et: L{EventType}
  @return: Created observer object
  @rtype: L{Observer}
  '''
  if et.klass == 'keyboard':
    # keyboard event types need to be monitored by a device observer
    ob = DeviceObserver(manager)
  else:
    # all others use the regular observer
    ob = EventObserver(manager)
  return ob

class Observer(object):
  '''
  Parent class for all event observers. Dispatches all received events to the 
  L{Manager} that created this L{Observer}. Provides basic reference counting
  functionality needed by L{Manager} to determine when an L{Observer} can be
  released for garbage collection. 
  
  The reference counting provided by this class is independent of the reference
  counting used by CORBA. Keeping the counts separate makes it easier for the
  L{Manager} to detect when an L{Observer} can be freed in the 
  L{Manager._unregisterObserver} method.
  
  @ivar manager: Reference to the L{Manager} that created this L{Observer}
  @type manager: L{Manager}
  @ivar ref_count: Reference count on this L{Observer}
  @type ref_count: integer
  '''
  def __init__(self, manager):
    '''
    Stores a reference to the creating L{Manager}. Intializes the reference
    count on this object to zero.
    
    @param manager: The L{Manager} that created this observer
    @type manager: L{Manager}
    '''
    self.manager = manager
    self.ref_count = 0

  def clientRef(self):
    '''
    Increments the L{pyLinAcc} reference count on this L{Observer} by one. This
    method is called when a new client is registered in L{Manager} to receive
    notification of an event type monitored by this L{Observer}.
    '''
    self.ref_count += 1
    
  def clientUnref(self):
    '''
    Decrements the L{pyLinAcc} reference count on this L{Observer} by one. This
    method is called when a client is unregistered in L{Manager} to stop 
    receiving notifications of an event type monitored by this L{Observer}.
    '''
    self.ref_count -= 1
    
  def getClientRefCount(self):
    '''
    @return: Current L{pyLinAcc} reference count on this L{Observer}
    @rtype: integer
    '''
    return self.ref_count
  
  def ref(self): 
    '''Required by CORBA. Does nothing.'''
    pass
    
  def unref(self): 
    '''Required by CORBA. Does nothing.'''
    pass

class DeviceObserver(Observer, Accessibility__POA.DeviceEventListener):
  '''
  Observes keyboard press and release events. The settings for the listener
  mode are hard coded so that key presses and releases can be consumed by any
  client registered with the L{Manager} to observe these events.
  
  @ivar mode: Keyboard event mode (currently hardwired as non-preemptive and 
      asynchronous)
  @type mode: Accessibility.EventListenerMode
  '''
  def __init__(self, manager):
    '''
    Creates a mode object that defines when key events will be received from 
    the system.
    
    @param manager: The L{Manager} that created this observer
    @type manager: L{Manager}
    '''
    Observer.__init__(self, manager)
    self.mode = Accessibility.EventListenerMode()
    self.mode.preemptive = True
    self.mode.synchronous = True
    self.mode._global = False
    
  def register(self, registry, name):
    '''
    Starts keyboard event monitoring on all keys and key combinations. The 
    event type to be monitored (key press or key release) is given by the event 
    name. See L{Manager.addClient} for valid event names.
    
    @param registry: Registry where the device event controller is defined
    @type registry: Accessibility.Registry
    @param name: L{pyLinAcc.Constants} keyboard event name
    @type name: string
    '''
    mask = 0
    # map the pretty keyboard event name string to the real AT-SPI constant
    type = [Constants.name_to_device_type[name]]
    # create an AT-SPI device event controller
    dc = registry.getDeviceEventController()
    # register for all possible key combos (plain key, key+shift, key+alt, ...)
    while mask <= (1 << Accessibility.MODIFIER_NUMLOCK):
      dc.registerKeystrokeListener(self._this(), [], mask, type, self.mode)
      mask += 1

  def unregister(self, registry, name):
    '''
    Stops keyboard event monitoring on all keys and key combinations. The event
    type to stop monitoring (key press or key release) is given by the event 
    name. See L{Manager.addClient} for possible event names.
        
    @param registry: Registry where the device event controller is defined
    @type registry: Accessibility.Registry
    @param name: Pretty AT-SPI event name
    @type name: string
    '''
    mask = 0
    type = [Constants.name_to_device_type[name]]
    dc = registry.getDeviceEventController()
    # unregister all possible key combos
    while mask <= (1 << Accessibility.MODIFIER_NUMLOCK):
      dc.deregisterKeystrokeListener(self._this(), [], mask, type)
      mask += 1
      
  def queryInterface(self, repo_id):
    '''
    Reports that this class only implements the AT-SPI DeviceEventListener 
    interface. Required by AT-SPI.
    
    @param repo_id: Request for an interface 
    @type repo_id: string
    @return: The underlying CORBA object for the device event listener
    @rtype: Accessibility.EventListener
    '''
    if repo_id == Constants.DEVICEEVENT_LISTENER_IDL:
      return self._this()
    else:
      return None

  def notifyEvent(self, event):
    '''
    Notifies the L{Manager} that an event has occurred. Wraps the raw event 
    object in our L{Event} class to support automatic ref and unref calls. An
    observer can set the L{Event} consume flag to True to indicate this event
    should not be allowed to pass to other AT-SPI observers or the underlying
    application.
    
    @param event: Low-level AT-SPI event (keyboard, mouse)
    @type event: Accessibility.DeviceEvent
    @return: Should the event be consumed (True) or allowed to pass on to other
        AT-SPI observers (False)?
    @rtype: boolean
    '''
    wrap = Event(event)
    try:
      self.manager.handleEvent(wrap)
    except Exception:
      pass
    return wrap.consume

class EventObserver(Observer, Accessibility__POA.EventListener):
  '''
  Observes all non-keyboard AT-SPI events.
  '''
  def register(self, registry, name):
    '''
    Starts monitoring for the given event. See L{Manager.addClient} for
    possible event names.
    
    @param registry: Registry on which the listener will be registered
    @type registry: Accessibility.Registry
    @param name: AT-SPI event name
    @type name: string
    '''
    registry.registerGlobalEventListener(self._this(), name)
    
  def unregister(self, registry, name):
    '''
    Stops monitoring for the given event. See L{Manager.addClient} for possible
    event names.
    
    @param registry: Registry on which the listener will be registered
    @type registry: Accessibility.Registry
    @param name: AT-SPI event name
    @type name: string
    '''
    registry.deregisterGlobalEventListener(self._this(), name)

  def queryInterface(self, repo_id):
    '''
    Reports that this class only implements the AT-SPI DeviceEventListener 
    interface. Required by AT-SPI.

    @param repo_id: Request for an interface 
    @type repo_id: string
    @return: The underlying CORBA object for the device event listener
    @rtype: Accessibility.EventListener
    '''
    if repo_id == EVENT_LISTENER_IDL:
      return self._this()
    else:
      return None

  def notifyEvent(self, event):
    '''
    Notifies the L{Manager} that an event has occurred. Wraps the raw event 
    object in our L{Event} class to support automatic ref and unref calls.
    Aborts on any exception indicating the event could not be wrapped.
    
    @param event: High-level AT-SPI event (anything but keyboard, mouse)
    @type event: Accessibility.Event
    '''
    try:
      ev = Event(event)
    except Exception:
      return
    try:
      self.manager.handleEvent(ev)
    except Exception:
      pass
    
class Manager(object):
  '''
  Manages all event L{Observer}s. Allows clients to register and unregister
  callbacks for observed events. Acts as a point of serialization for events so
  events of the same klass (see L{EventType}) can be gauranteed to be delivered 
  to the registered clients in the order they were registered. No such guarantee
  is made across event klasses.

  @ivar registry: Reference to the Gnome Accessibility.Registry
  @type registry: Accessibility.Registry
  @ivar clients: Registered Clients to be notified about events
  @type clients: dictionary {event name : client callable}
  @ivar observers: L{Observer} objects registered to monitor events
  @type observers: dictionary {event klass : L{Observer}}
  '''
  def __init__(self):
    '''
    Stores references to the Accessibility.Registery and creates the empty 
    client and observer dictionaries.
    '''
    self.registry = pyLinAcc.Registry
    self.dev = self.registry.getDeviceEventController()
    self.clients = {}
    self.observers = {}
    
  def _registerClients(self, client, name):
    '''
    Internal method that recursively associates a client with AT-SPI event 
    names. Allows a clientto incompletely specify an event name in order to 
    register for subevents without specifying their full names manually. See
    L{Manager.addClient} for valid event names.
    
    @param client: Client callback to receive event notifications
    @type client: callable
    @param name: Partial or full event name
    @type name: string
    '''
    try:
      # look for an event name in our event tree dictionary
      events = Constants.event_tree[name]
    except KeyError:
      # if the event name doesn't exist, it's a leaf event meaning there are
      # no subtypes for that event
      # add this client to the list of clients already in the dictionary 
      # using the event name as the key; if there are no clients yet for this 
      # event, insert an empty list into the dictionary before appending 
      # the client
      et = EventType(name)
      clients = self.clients.setdefault(et.name, [])
      try:
        clients.index(client)
      except ValueError:
        # only allow a particular client to register once
        clients.append(client)
        self._registerObserver(name)
      return
    # if the event name does exist in the tree, there are subevents for this 
    # event; loop through them calling this method again to get to the leaf
    # events
    for e in events:
      self._registerClients(client, e)
      
  def _unregisterClients(self, client, name):
    '''
    Internal method that recursively unassociates a client with AT-SPI event 
    names. Allows a client to incompletely specify an event name in order to 
    unregister for subevents without specifying their full names manually.
    
    @param client: Client callback to receive event notifications
    @type client: callable
    @param name: Partial or full event name
    @type name: string
    '''
    missed = False
    try:
      # look for an event name in our event tree dictionary
      events = Constants.event_tree[name]
    except KeyError:
      try:
        # if the event name doesn't exist, it's a leaf event meaning there are
        # no subtypes for that event
        # get the list of registered clients and try to remove the one provided
        et = EventType(name)
        clients = self.clients[et.name]
        clients.remove(client)
        self._unregisterObserver(name)
      except (ValueError, KeyError):
        # ignore any exceptions indicating the client is not registered
        missed = True
      return missed
    # if the event name does exist in the tree, there are subevents for this 
    # event; loop through them calling this method again to get to the leaf
    # events
    for e in events:
      missed |= self._unregisterClients(client, e)
    return missed
      
  def _registerObserver(self, name):
    '''
    Creates a new L{Observer} to watch for events of the given type or returns
    the existing observer if one is already registered. One L{Observer}
    is created for each event type which has no subevents.
   
    @param name: Name of the event to observe
    @type name: string
    @return: L{Observer} object that is monitoring the event
    @rtype: L{Observer}
    '''
    et = EventType(name)
    try:
      # see if an observer already exists for this event
      ob = self.observers[et.name]
    except KeyError:
      # build a new observer if one does not exist
      ob = _createObserver(self, et)
      # we have to register for the raw name because it may be different from
      # the parsed name determined by EventType (e.g. trailing ':' might be 
      # missing)
      ob.register(self.registry, name)
      self.observers[et.name] = ob
    ob.clientRef()
    return ob
    
  def _unregisterObserver(self, name):
    '''
    Destroys an existing L{Observer} for the given event type only if no clients 
    are registered for the events it is monitoring.
    
    @param name: Name of the event to observe
    @type name: string
    @raise KeyError: When an observer for the given event is not regist
    '''
    et = EventType(name)
    # see if an observer already exists for this event
    ob = self.observers[et.name]
    ob.clientUnref()
    if ob.getClientRefCount() == 0:
      ob.unregister(self.registry, name)
      del self.observers[et.name]
      
  def close(self):
    '''
    Shuts down the L{Manager} by destroying all observers.
    '''
    for name, ob in self.observers.items():
      ob.unregister(self.registry, name)
    
  def addClient(self, client, *names):
    '''
    Registers a new client callback for the given event names. Supports 
    registration for all subevents if only partial event name is specified.
    Do not include a trailing colon. The valid event names are the following:
    
    terminal
    terminal:line-changed
    terminal:columncount-changed
    terminal:linecount-changed
    terminal:application-changed',
    terminal:charwidth-changed
    document:load-complete
    document:reload
    document:load-stopped
    document:content-changed
    document:attributes-changed
    object
    object:bounds-changed
    object:link-selected
    object:property-change
    object:state-changed
    object:children-changed
    object:visible-data-changed
    object:selection-changed
    object:model-changed
    object:active-descendant-changed
    object:row-inserted
    object:row-reordered
    object:row-deleted
    object:column-inserted
    object:column-reordered
    object:column-deleted
    object:attributes-changed
    object:text-attributes-changed
    object:text-selection-changed
    object:text-caret-moved
    object:text-changed
    object:text-changed:insert
    object:text-changed:delete
    object:property-change
    object:property-change:accessible-parent
    object:property-change:accessible-name
    object:property-change:accessible-description
    object:property-change:accessible-value
    object:property-change:accessible-role
    object:property-change:accessible-table-caption
    object:property-change:accessible-table-column-description
    object:property-change:accessible-table-column-header
    object:property-change:accessible-table-row-description
    object:property-change:accessible-table-row-header
    object:property-change:accessible-table-summary
    object:children-changed
    object:children-changed:add
    object:children-changed:remove
    object:state-changed
    object:state-changed:active
    object:state-changed:armed
    object:state-changed:busy
    object:state-changed:checked
    object:state-changed:collapsed
    object:state-changed:defunct
    object:state-changed:editable
    object:state-changed:enabled
    object:state-changed:expandable
    object:state-changed:expanded
    object:state-changed:focusable
    object:state-changed:focused
    object:state-changed:has-tooltip
    object:state-changed:horizontal
    object:state-changed:iconified
    object:state-changed:indeterminate
    object:state-changed:invalid
    object:state-changed:last-defined
    object:state-changed:manages-descendants
    object:state-changed:modal
    object:state-changed:multi-line
    object:state-changed:multiselectable
    object:state-changed:opaque
    object:state-changed:pressed
    object:state-changed:resizable
    object:state-changed:selectable
    object:state-changed:selected
    object:state-changed:sensitive
    object:state-changed:showing
    object:state-changed:single-line
    object:state-changed:stale
    object:state-changed:transient
    object:state-changed:vertical
    object:state-changed:visible
    ...
    mouse
    mouse:abs
    mouse:rel
    mouse:button
    mouse:button
    mouse:button:1p
    mouse:button:1r
    mouse:button:2p
    mouse:button:2r
    mouse:button:3p
    mouse:button:3r
    window
    window:minimize
    window:maximize
    window:restore
    window:close
    window:create
    window:reparent
    window:desktop-create
    window:desktop-destroy
    window:activate
    window:deactivate
    window:raise
    window:lower
    window:move
    window:resize
    window:shade
    window:unshade
    window:restyle
    focus
    keyboard
    keyboard:press
    keyboard:release

    For example, 'object' will register for all object events, 
    'object:property-change' will register for all property change events,
    and 'object:property-change:accessible-parent' will register only for the
    parent property change event.
    
    Registered clients will not be automatically removed when the client dies.
    To ensure the client is properly garbage collected, call 
    L{Manager.removeClient}.

    @param client: Callable to be invoked when the event occurs
    @type client: callable
    @param names: List of full or partial event names
    @type names: list of string
    '''
    for name in names:
      # store the callback for each specific event name
      self._registerClients(client, name)

  def removeClient(self, client, *names):
    '''
    Unregisters an existing client callback for the given event names. Supports 
    unregistration for all subevents if only partial event name is specified.
    Do not include a trailing colon.
    
    This method must be called to ensure a client registered by 
    L{Manager.addClient} is properly garbage collected.

    @param client: Client callback to remove
    @type client: callable
    @param names: List of full or partial event names
    @type names: list of string
    @return: Were event names specified for which the given client was not
        registered?
    @rtype: boolean
    '''
    missed = False
    for name in names:
      # remove the callback for each specific event name
      missed |= self._unregisterClients(client, name)
    return missed
  
  def generateKeyEvent(self, key_code, press=True):
    '''
    Synthesizes a key press or key release to be sent to the foreground window
    with the input focus.
    
    @param key_code: Key code representing the physical key on the keyboard to
        press
    @type key_code: integer
    @param press: True to send a key press, False to send a key release
    @type press: boolean
    '''
    if press: 
      kind = Constants.KEY_PRESS
    else:
      kind = Constants.KEY_RELEASE
    self.dev.generateKeyboardEvent(key_code, '', kind)
    
  def handleEvent(self, event):
    '''
    Handles an AT-SPI event. The default implementation immediately calls 
    L{_dispatchEvent} to send the event on to the registered clients. Override
    this method to implement some other default behavior (e.g. queueing events
    for later processing.
    
    As of 9/12/05, the faster this method returns, the better, as AT-SPI event
    handling appears to be done synchronously across the Gnome desktop. If this
    method stalls, the entire desktop will freeze.
    
    @param event: Wrapped AT-SPI event
    @type event: L{Event}
    '''
    self._dispatchEvent(event)
    
  def _dispatchEvent(self, event):
    '''
    Dispatches L{Event}s to registered clients. Clients are called in the order
    they were registered for the given AT-SPI event. If any client sets the 
    L{Event} consume flag to True, callbacks cease immediately for that event.
    For keyboard events, this also implies that no other observer registered 
    with AT-SPI across the system will receive the event nor will the 
    application with the input focus.
        
    @param event: Wrapped AT-SPI event
    @type event: L{Event}
    '''
    try:
      # try to get the client registered for this event type
      clients = self.clients[event.type.name]
    except KeyError:
      # if there's an exception, we might have a keyboard event constant
      # which needs to be mapped to our keyboard event string names
      try:
        clients = self.clients[Constants.device_type_to_name[event.type.name]]
      except KeyError:
        return
    # make the call to each client
    for client in clients:
      try:
        client(event)
      except Exception:
        traceback.print_exc()
      if event.consume:
        # don't allow further processing if the consume flag is set
        break
