'''
Defines constants used throughout pyLinAcc.

@author: Peter Parente
@author: Pete Brunet
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
import Accessibility, ORBit

# interface name strings for ease of reference
DEVICEEVENT_LISTENER_IDL = 'IDL:Accessibility/DeviceEventListener:1.0'
EVENT_LISTENER_IDL = 'IDL:Accessibility/EventListener:1.0'
ACTION_IDL = 'IDL:Accessibility/Action:1.0'
ACCESSIBLE_IDL = 'IDL:Accessibility/Accessible:1.0'
APPLICATION_IDL = 'IDL:Accessibility/Application:1.0'
COLLECTION_IDL = 'IDL:Accessibility/Collection:1.0'
COMPONENT_IDL = 'IDL:Accessibility/Component:1.0'
DESKTOP_IDL = 'IDL:Accessibility/Desktop:1.0'
DOCUMENT_IDL = 'IDL:Accessibility/Document:1.0'
EDITABLE_TEXT_IDL = 'IDL:Accessibility/EditableText:1.0'
HYPERLINK_IDL = 'IDL:Accessibility/Hyperlink:1.0'
HYPERTEXT_IDL = 'IDL:Accessibility/Hypertext:1.0'
IMAGE_IDL = 'IDL:Accessibility/Image:1.0'
LOGIN_HELPER_IDL = 'IDL:Accessibility/LoginHelper:1.0'
STREAMABLE_CONTENT_IDL = 'IDL:Accessibility/StreamableContent:1.0'
TABLE_IDL = 'IDL:Accessibility/Table:1.0'
TEXT_IDL = 'IDL:Accessibility/Text:1.0'
VALUE_IDL = 'IDL:Accessibility/Value:1.0'
SELECTION_IDL = 'IDL:Accessibility/Selection:1.0'

# constants used in the Component interface to get screen coordinates
DESKTOP_COORDS = 0
WINDOW_COORDS = 1

# provides pretty names for keyboard device events
name_to_device_type = {'keyboard:release' : Accessibility.KEY_RELEASED_EVENT,
                       'keyboard:press' : Accessibility.KEY_PRESSED_EVENT}
device_type_to_name = {Accessibility.KEY_RELEASED : 'keyboard:release',
                       Accessibility.KEY_PRESSED : 'keyboard:press'}
                       
# pull important exceptions into the local namespace so other modules don't have
# to reference CORBA directly
CORBAException = ORBit.CORBA.Exception
SystemException = ORBit.CORBA.SystemException
UserException = ORBit.CORBA.UserException
NotImplemented = ORBit.CORBA.NO_IMPLEMENT
CommFailure = ORBit.CORBA.COMM_FAILURE
NotExist = ORBit.CORBA.OBJECT_NOT_EXIST

# dictionary used to correct the bug of not being able to register for all the
# subevents given only an AT-SPI event class (i.e. first part of the event name)
# keys are event names having subevents and values are the subevents under the
# key event
event_tree = {
  'terminal':
    ['terminal:line-changed',
     'terminal:columncount-changed',
     'terminal:linecount-changed',
     'terminal:application-changed',
     'terminal:charwidth-changed'
     ],
  'document':
    ['document:load-complete',
     'document:reload',
     'document:load-stopped',
     'document:content-changed',
     'document:attributes-changed'
     ],
  'object': 
    ['object:property-change',
     'object:bounds-changed',
     'object:link-selected',
     'object:state-changed',
     'object:children-changed',
     'object:visible-data-changed',
     'object:selection-changed',
     'object:model-changed',
     'object:active-descendant-changed',
     'object:row-inserted',
     'object:row-reordered',
     'object:row-deleted',
     'object:column-inserted',
     'object:column-reordered',
     'object:column-deleted',
     'object:text-bounds-changed',
     'object:text-selection-changed',
     'object:text-changed',
     'object:text-attributes-changed',
     'object:text-caret-moved',  
     'object:attributes-changed'],
  'object:text-changed' :
    ['object:text-changed:insert',
    'object:text-changed:delete'],
  'object:property-change' :
    ['object:property-change:accessible-parent', 
    'object:property-change:accessible-name',
    'object:property-change:accessible-description',
    'object:property-change:accessible-value',
    'object:property-change:accessible-role',
    'object:property-change:accessible-table-caption',
    'object:property-change:accessible-table-column-description',
    'object:property-change:accessible-table-column-header',
    'object:property-change:accessible-table-row-description',
    'object:property-change:accessible-table-row-header',
    'object:property-change:accessible-table-summary'],
  'object:children-changed' :
    ['object:children-changed:add',
    'object:children-changed:remove'],
  # PP: do we really want to list these? then we have to keep them in sync with
  # the states actually defined by AT-SPI
  'object:state-changed' :
    ['object:state-changed:active',
    'object:state-changed:armed',
    'object:state-changed:busy',
    'object:state-changed:checked',
    'object:state-changed:collapsed',
    'object:state-changed:defunct',
    'object:state-changed:editable',
    'object:state-changed:enabled',
    'object:state-changed:expandable',
    'object:state-changed:expanded',
    'object:state-changed:focusable',
    'object:state-changed:focused',
    'object:state-changed:has-tooltip',
    'object:state-changed:horizontal',
    'object:state-changed:iconified',
    'object:state-changed:indeterminate',
    'object:state-changed:invalid',
    'object:state-changed:last-defined',
    'object:state-changed:manages-descendants',
    'object:state-changed:modal',
    'object:state-changed:multi-line',
    'object:state-changed:multiselectable',
    'object:state-changed:opaque',
    'object:state-changed:pressed',
    'object:state-changed:resizable',
    'object:state-changed:selectable',
    'object:state-changed:selected',
    'object:state-changed:sensitive',
    'object:state-changed:showing',
    'object:state-changed:single-line',
    'object:state-changed:stale',
    'object:state-changed:transient',
    'object:state-changed:vertical',
    'object:state-changed:visible'],
  'mouse' :
    ['mouse:abs',
    'mouse:rel',
    'mouse:button'],
  'mouse:button' :
    ['mouse:button:1p',
    'mouse:button:1r',
    'mouse:button:2p',
    'mouse:button:2r',
    'mouse:button:3p',
    'mouse:button:3r'],
  'window' :
    ['window:minimize',
    'window:maximize',
    'window:restore',
    'window:close',
    'window:create',
    'window:reparent',
    'window:desktop-create',
    'window:desktop-destroy',
    'window:activate',
    'window:deactivate',
    'window:raise',
    'window:lower',
    'window:move',
    'window:resize',
    'window:shade',
    'window:unshade',
    'window:restyle'],
  'keyboard' :
    ['keyboard:press',
    'keyboard:release'],
  'focus' :
    ['focus:']
}

# recommendation for which tasks should be displayed by default by EventMonitor
default_types = ['window', 'focus', 'object:active-descendant-changed']
# all registered Task types; used to build display menu in EventMonitor
all_types = ['window', 'focus', 'mouse', 'keyboard']
all_types.extend(event_tree['object'])

# pull ROLE_*, STATE_*, TEXT_*, MODIFIER_*, LOCALE_*, and RELATION_*, etc. 
# constants into the local namespace for convenient access
# grab all the variable names and their values from the Accessibility module
acc_dict = vars(Accessibility)
# get the dictionary for the local namespace
loc_dict = locals()
# these are the prefixes for the variable names we want to pull out of the 
# Accessibility module
prefixes = ['ROLE_', 'STATE_', 'TEXT_', 'MODIFIER_', 'LOCALE_', 'RELATION_',
            'KEY_', 'MATCH_', 'SORT_', 'LAYER_']

# build a dictionary mapping state values to names based on the prefix of the
# constant name imported from Accessibility
state_val_to_name = dict(((value, name[6:].lower().replace('_', ' ')) 
                          for name, value 
                          in acc_dict.items()
                          if name.startswith('STATE_')))

# build a dictionary mapping relation values to names based on the prefix of 
# the constant name imported from Accessibility
rel_val_to_name = dict(((value, name[9:].lower().replace('_', ' ')) 
                          for name, value 
                          in acc_dict.items()
                          if name.startswith('RELATION_')))

# build a dictionary mapping modifier values to names based on the prefix of 
# the constant name imported from Accessibility
mod_val_to_name = dict(((value, name[9:].lower().replace('_', ' ')) 
                          for name, value 
                          in acc_dict.items()
                          if name.startswith('MODIFIER_')))

# for each variable name in the Accessibility namespace, check if it starts with
# at least one of the prefixes above; if it does, add a 2-tuple of variable name
# and value to the values list
values = ((name, value) for name, value in acc_dict.items()
          if len([p for p in prefixes if name.startswith(p)]))
# create a new dictionary from the list of tuples and then update the local
# namespace dictionary with that dictionary
loc_dict.update(dict(values))
# throw away any temporary variables so they don't hang around in this module
del acc_dict, loc_dict, prefixes, values