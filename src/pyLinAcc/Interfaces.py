'''
Defines functions that make queryInterface more Pythonic.

@author: Peter Parente
@author: Pete Brunet
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''

import Constants

def queryInterface(obj, idl):
  '''
  Queries an object for another interface.
  
  @param obj: Object to query for another interface
  @type obj: object
  @param idl: Desired interface identifier
  @type idl: string
  @return: An object with the desired interface
  @rtype: object
  @raise NotImplementedError: When the desired interface is not supported
  '''
  try:
    i = obj.queryInterface(idl)
  except Exception:
    raise NotImplementedError
  if i is None:
    raise NotImplementedError
  i = i._narrow(i.__class__)
  return i

def IAccessible(obj):
  '''
  @return: Object with the Accessible interface or None if not supported
  @rtype: Accessibility.Accessible
  '''
  return queryInterface(obj, Constants.ACCESSIBLE_IDL)

def IAction(obj):
  '''
  @return: Object with the Action interface
  @rtype: Accessibility.Action
  '''  
  return queryInterface(obj, Constants.ACTION_IDL)
  
def IApplication(obj):
  '''
  @return: Object with the Application interface or None if not supported
  @rtype: Accessibility.Application
  '''
  return queryInterface(obj, Constants.APPLICATION_IDL)

def ICollection(obj):
  '''
  @return: Object with the Collection interface or None if not supported
  @rtype: Accessibility.Collection
  '''
  return queryInterface(obj, Constants.COLLECTION_IDL)

def IComponent(obj):
  '''
  @return: Object with the Component interface or None if not supported
  @rtype: Accessibility.Component
  '''
  return queryInterface(obj, Constants.COMPONENT_IDL)
  
def IDesktop(obj):
  '''
  @return: Object with the Desktop interface or None if not supported
  @rtype: Accessibility.Desktop
  '''
  return queryInterface(obj, Constants.DESKTOP_IDL)

def IDocument(obj):
  '''
  @return: Object with the Document interface or None if not supported
  @rtype: Accessibility.Document
  '''
  return queryInterface(obj, Constants.DOCUMENT_IDL)

def IEditableText(obj):
  '''
  @return: Object with the EditableText interface or None if not supported
  @rtype: Accessibility.EditableText
  '''
  return queryInterface(obj, Constants.EDITABLE_TEXT_IDL)

def IHyperlink(obj):
  '''
  @return: Object with the Hyperlink interface or None if not supported
  @rtype: Accessibility.Hyperlink
  '''
  return queryInterface(obj, Constants.HYPERLINK_IDL)

def IHypertext(obj):
  '''
  @return: Object with the Hypertext interface or None if not supported
  @rtype: Accessibility.Hypertext
  '''
  return queryInterface(obj, Constants.HYPERTEXT_IDL)

def IImage(obj):
  '''
  @return: Object with the Image interface or None if not supported
  @rtype: Accessibility.Image
  '''
  return queryInterface(obj, Constants.IMAGE_IDL)

def ILoginHelper(obj):
  '''
  @return: Object with the LoginHelper interface or None if not supported
  @rtype: Accessibility.LoginHelper
  '''
  return queryInterface(obj, Constants.LOGIN_HELPER_IDL)

def IStreamableContent(obj):
  '''
  @return: Object with the StreamableContent interface or None if not supported
  @rtype: Accessibility.StreamableContent
  '''
  return queryInterface(obj, Constants.STREAMABLE_CONTENT_IDL)

def ITable(obj):
  '''
  @return: Object with the Table interface or None if not supported
  @rtype: Accessibility.Table
  '''
  return queryInterface(obj, Constants.TABLE_IDL)
  
def IText(obj):
  '''
  @return: Object with the Text interface or None if not supported
  @rtype: Accessibility.Text
  '''
  return queryInterface(obj, Constants.TEXT_IDL)
  
def IValue(obj):
  '''
  @return: Object with the Value interface or None if not supported
  @rtype: Accessibility.Value
  '''
  return queryInterface(obj, Constants.VALUE_IDL)

def ISelection(obj):
  '''
  @return: Object with the Selection interface or None if not supported
  @rtype: Accessibility.Selection
  '''
  return queryInterface(obj, Constants.SELECTION_IDL)
