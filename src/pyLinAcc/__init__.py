'''
Wraps the Gnome Assistive Technology Service Provider Interface for use in
Python. Imports the bonobo and ORBit modules. Initializes the ORBit ORB.
Activates the bonobo Accessibility Registry. Loads the Accessibility typelib and
imports the classes implementing the AT-SPI interfaces.

@var Registry: Reference to the AT-SPI registry daemon intialized on successful
  import
@type Registry: Accessibility.Registry

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''

REGISTRY_IID = "OAFIID:Accessibility_Registry:1.0"
TYPELIB_NAME = "Accessibility"

# import ORBit and bonobo first (required)
import ORBit, bonobo
# initialize the ORB
orb = ORBit.CORBA.ORB_init()
# get a reference to the gnome Accessibility registry
data = bonobo.activation.activate_from_id(REGISTRY_IID)
if data is None:
  raise RuntimeError('Could not activate:', REGISTRY_IID)
else:
  try:
    Registry = data[0]
  except IndexError:
    raise ValueError('Activation received unexpected object: '+str(data))
# generate Python code for the Accessibility module from the IDL
ORBit.load_typelib(TYPELIB_NAME)

# import the generated Accessibility module
import Accessibility
# import our modules	
import Accessible, Event, Constants, Interfaces, Atk

def getDefaults():
  '''
  Suggests the default AT-SPI events to be monitored.
  
  @return: Names of defaults to monitor
  @rtype: list of string
  '''
  return Constants.default_types
  
def getNames():
  '''
  Gets the names of all the top-level (klass) AT-SPI events.
  
  @return: List of all names
  @rtype: list of string
  '''
  names = Constants.all_types
  names.sort()
  return names

def stringToConst(prefix, suffix):
  '''
  Maps a string name to an LSR constant. The rules for the mapping are as 
  follows:
    - The prefix is captalized and has an _ appended to it.
    - All spaces in the suffix are mapped to the _ character. 
    - All alpha characters in the suffix are mapped to their uppercase.
    
  The resulting name is used with getattr to look up a constant with that name
  in the L{pyLinAcc.Constants} module. If such a constant does not exist, the
  string suffix is returned instead. 

  This method allows strings to be used to refer to roles, relations, etc. 
  without direct access to the constants. It also supports the future expansion
  of roles, relations, etc. by allowing arbitrary strings which may or may not
  map to the current standard set of roles, relations, etc., but may still match
  some non-standard role, relation, etc. being reported by an application.
  
  @param prefix: Prefix of the constant name such as role, relation, state, 
    text, modifier, key
  @type prefix: string
  @param suffix: Name of the role, relation, etc. to use to lookup the constant
  @type suffix: string
  @return: The matching constant value
  @rtype: object
  '''
  name = prefix.upper()+'_'+suffix.upper().replace(' ', '_')
  return getattr(Constants, name, suffix)

def stateToString(value):
  '''
  Converts a state value to a string based on the name of the state constant in 
  the L{Constants} module that has the given value.
  
  @param value: An AT-SPI state
  @type value: Accessibility.StateType
  @return: Human readable, untranslated name of the state
  @rtype: string
  '''
  return Constants.state_val_to_name.get(value)

def relationToString(value):
  '''
  Converts a relation value to a string based on the name of the state constant
  in the L{Constants} module that has the given value.
  
  @param value: An AT-SPI relation
  @type value: Accessibility.RelationType
  @return: Human readable, untranslated name of the relation
  @rtype: string
  '''
  return Constants.rel_val_to_name.get(value)

def errorToLookupError(func):
  '''
  Decorates the given function with a try/except that catches all pyLinAcc 
  CORBA exceptions and re-raises them as standard Python lookup errors. This
  decorator is useful for reducing the number of imports of the pyLinAcc
  package across LSR.
  
  @param func: Function to decorate
  @type func: function
  '''
  def _inner(self, *args, **kwargs):
    # function that will be called in place of the decorated func
    try:
      # try calling the original func
      return func(self, *args, **kwargs)
    except Constants.CORBAException:
      # raise LookupError on any pyLinAcc exception
      raise LookupError
    # make sure the inner function adopts the name and doc of the original
  _inner.__doc__ = func.__doc__
  _inner.__name__ = func.__name__
  _inner.__dict__.update(func.__dict__)
  return _inner
