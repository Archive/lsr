'''
Defines a mixin class that extend the functionality of the existing AT-SPI 
objects with features such as searching on any criteria and comparison of
two AT-SPI Accessibles.

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''

import new, types
import Constants, Interfaces

def stirInto(cls, new_cls, include=None, exclude=None):
  '''
  Adds the methods in new_cls named in include or those not in exclude, or all 
  methods if neither is specified, to cls. After stirring, all instances of
  cls will have the new methods. If there is a method name clash, the method
  already in cls will be prefixed with an underscore before the new method of
  the same name is mixed in.
  
  Based on code from http://wwwx.cs.unc.edu/~parente/cgi-bin/RuntimeClassMixins.
  
  @param cls: Existing class to mix features into
  @type cls: class
  @param new_cls: Class containing features to add
  @type new_cls: class
  @param include: Method names to add
  @type include: list
  @param exclude: Method names to avoid adding
  @type exclude: list
  '''       
  # add new methods to the class
  for name in new_cls.__dict__.keys():
    # add methods
    if isinstance(new_cls.__dict__[name], types.FunctionType) and \
      (include is None or name in include) and \
      (exclude is None or name not in exclude):
      # get the function from the new_cls
      func = new_cls.__dict__[name]
      # build a new function that is a clone of the one from new_cls
      method = new.function(func.func_code, func.func_globals, name, 
                            func.func_defaults, func.func_closure)
      try:
        # check if a method of the same name already exists in the target
        old_method = getattr(cls, name)
      except AttributeError:
        pass
      else:
        # rename the old method so we can still call it if need be
        setattr(cls, '_'+name, old_method)
      # add the clone to cls
      setattr(cls, name, method)

class AccessibleMixin(object):
  '''
  Defines additional methods to be added to the AT-SPI Accessible object. The
  features defined here will be added to the Accessible class at run time so
  that all instances of Accessible have them (i.e. there is no need to
  explicitly wrap an Accessible in this class or derive a new class from it.)
  '''
  def __del__(self):
    '''
    Decrements the reference count on the accessible object when there are no
    Python references to this object. This provides automatic reference 
    counting for AT-SPI objects.
    '''
    try:
      self.unref()
    except Exception:
      pass
    
  def __iter__(self):
    '''
    Iterator that yields one accessible child per iteration. If an exception is
    encountered, None is yielded instead.
    
    @return: A child accessible
    @rtype: Accessibility.Accessible
    '''
    for i in xrange(self.childCount):
      try:
        yield self.getChildAtIndex(i)
      except Constants.CORBAException:
        yield None
    
  def __str__(self):
    '''
    Gets a human readable representation of the accessible.
    
    @return: Role and name information for the accessible
    @rtype: string
    '''
    try:
      return '[%s | %s]' % (self.getLocalizedRoleName(), self.name)
                      #           self.getApplication())
    except Exception:
      return '[DEAD]'
    
  def __nonzero__(self):
    '''
    @return: True, always
    @rtype: boolean
    '''
    return True
    
  def __getitem__(self, index):
    '''
    Thin wrapper around getChildAtIndex.
    
    @param index: Index of desired child
    @type index: integer
    @return: Accessible child
    @rtype: Accessibility.Accessible
    '''
    return self.getChildAtIndex(index)
  
  def __len__(self):
    '''
    Thin wrapper around childCount.
    
    @return: Number of child accessibles
    @rtype: integer
    '''
    return self.childCount
    
  def getIndexInParent(self):
    '''
    Gets the index of this accessible in its parent. Uses the implementation of
    this method provided by the Accessibility.Accessible object, but checks the
    bound of the value to ensure it is not outside the range of childCount 
    reported by this accessible's parent.
    
    @return: Index of this accessible in its parent
    @rtype: integer
    '''
    i = self._getIndexInParent()
    try:
      # correct for out-of-bounds index reporting
      return min(self.parent.childCount-1, i)
    except AttributeError:
      # return sentinel if there is no parent
      return -1
  
  def findDescendant(self, pred, breadth_first=False):
    '''
    Searches for a descendant node satisfying the given predicate starting at 
    this node. The search is performed in depth-first order by default or
    in breadth first order if breadth_first is True. For example,
    
    my_win = node.findDescendant(lambda x: x.name == 'My Window')
    
    will search all descendants of node until one is located with the name 'My
    Window' or all nodes are exausted. Calls L{_findDescendantDepth} or
    L{_findDescendantBreadth} to start the recursive search.
    
    @param pred: Search predicate returning True if accessible matches the 
        search criteria or False otherwise
    @type pred: callable
    @param breadth_first: Search breadth first (True) or depth first (False)?
    @type breadth_first: boolean
    @return: Accessible matching the criteria or None if not found
    @rtype: Accessibility.Accessible or None
    '''
    if breadth_first:
      return self._findDescendantBreadth(pred)
    for i in xrange(self.childCount):
      try:
        c = self.getChildAtIndex(i)
        ret = c._findDescendantDepth(pred)
      except Exception:
        ret = None
      if ret is not None: return ret
      
  def _findDescendantBreadth(self, pred):
    '''
    Internal function for locating one descendant. Called by 
    L{AccessibleMixin.findDescendant} to start the search.
    
    @param pred: Search predicate returning True if accessible matches the 
        search criteria or False otherwise
    @type pred: callable
    @return: Matching node or None to keep searching
    @rtype: Accessibility.Accessible or None
    '''
    for i in xrange(self.childCount):
      try:
        c = self.getChildAtIndex(i)
        if pred(c): return c
      except Exception:
        pass
    for i in xrange(self.childCount):
      try:
        c = self.getChildAtIndex(i)
        ret = c._findDescendantBreadth(pred)
      except Exception:
        ret = None
      if ret is not None: return ret

  def _findDescendantDepth(self, pred):
    '''
    Internal function for locating one descendant. Called by 
    L{AccessibleMixin.findDescendant} to start the search.
    
    @param pred: Search predicate returning True if accessible matches the 
        search criteria or False otherwise
    @type pred: callable
    @return: Matching node or None to keep searching
    @rtype: Accessibility.Accessible or None
    '''
    try:
      if pred(self): return self
    except Exception:
      pass
    for i in xrange(self.childCount):
      try:
        c = self.getChildAtIndex(i)
        ret = c._findDescendantDepth(pred)
      except Exception:
        ret = None
      if ret is not None: return ret
      
  def findAllDescendants(self, pred):
    '''
    Searches for all descendant nodes satisfying the given predicate starting at 
    this node. For example,
    
    pred = lambda x: x.getRole() == Accessible.ROLE_PUSH_BUTTON
    buttons = node.findAllDescendants(pred)
    
    will locate all push button descendants of the current node. Calls 
    L{findIterDescendants} to start the exhaustive recursive search.
    
    @param pred: Search predicate returning True if accessible matches the 
        search criteria or False otherwise
    @type pred: callable
    @return: All nodes matching the search criteria
    @rtype: list
    '''
    return [result for result in self.findIterDescendants(pred)]
      
  def findIterDescendants(self, pred):
    '''
    Iterator that yields Accessibility.Accessible that passed search criteria
    defined by pred.  The search is performed in depth-first order and the 
    results are returned in that order.
    
    @param pred: Search predicate returning True if accessible matches the 
        search criteria or False otherwise
    @type pred: callable
    @return: A node matching the search criteria
    @rtype: Accessibility.Accessible or None
    '''
    for i in xrange(self.childCount):
      try:
        c = self.getChildAtIndex(i)
        if pred(self): yield c
      except AttributeError:
        continue
    
  def findAncestor(self, pred):
    '''
    Searches for an ancestor satisfying the given predicate. Note that the
    AT-SPI DOM is not perfectly doubly linked. Node A may consider node B its
    child, but B is not guaranteed to have node A as its parent (i.e. its parent
    may be set to None). This means some searches may never make it all the
    way up the DOM to the desktop level.
    
    @param pred: Search predicate returning True if accessible matches the 
        search criteria or False otherwise
    @type pred: callable
    @return: Node matching the criteria or None if not found
    @rtype: Accessibility.Accessible or None
    '''
    if self.parent is None:
      return None
    else:
      try:
        if pred(self.parent): return self.parent
      except Exception:
        pass
      return self.parent.findAncestor(pred)
    
  def getApplication(self):
    '''
    Gets the most-parent accessible (the application) of this accessible. Tries 
    using the getApplication method introduced in AT-SPI 1.7.0 first before 
    resorting to traversing parent links.
    
    @warning: Cycles involving more than the previously traversed accessible 
      are not detected by this code.
    @return: Application object
    @rtype: Accessibility.Application
    '''
    try:
      return self._getApplication()
    except AttributeError:
      pass
    curr = self
    try:
      while curr.parent is not None and (not curr.parent == curr):
        curr = curr.parent
      return Interfaces.IApplication(curr)
    except Exception:
      pass
    # return None if the application isn't reachable for any reason
    return None
  
  def isAlive(self):
    '''
    Verifies that the underlying CORBA object still exists.

    @return: True if the CORBA object is still valid. False if not.
    @rtype: boolean
    '''
    try:
      self.id
    except Exception:
      return False
    else:
      return True
    
# mixes into the Accessibility.Accessible object; don't need to mix into 
# Accessibility.Application or Accessibility.Desktop because they derive from
# Accessibility.Accessible
import Accessibility
stirInto(Accessibility.Accessible, AccessibleMixin)
