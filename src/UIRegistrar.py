'''
Defines the registrar functions responsible for managing an on-disk repository 
of User Interface Elements (UIEs) that define the L{Perk}s, Devices, Choosers,
and Monitors available to the user and the system. Defines a helper class 
L{UIESet} that is used internally by the registrar to represent collections 
of UIEs that are to be loaded when LSR starts, when any L{Tier} starts, or when 
a particular L{Tier} starts for a particular user.

Functions to install, uninstall, associate, disassociate, and list UIEs are
primarily of interest to third-party UIE developers and expert users. These
methods allow extensions to be added to LSR and associated with profiles so
they are loaded automatically at startup or when L{Tier}s are created. The
L{AEMain} script defines a command line interface for accessing these
methods. See the documentation in that module or the LSR man page for details.
  
Functions to load UIEs are of interest to LSR developers. Given a UIE name, the
L{loadOne} method will import the Python module containing the named UIE and
return an instance of the class in that module of the same name (if the UIE
module is properly installed). Given a UIE kind and name along with the name
of a , the L{loadAssociated} method will import all Python modules containing
the UIEs to be loaded and return a list of UIE objects sorted in the desired
load order. Objects, not classes, are returned by this method implying any
initialization requiring data from an external source must be done outside the
constructor.


@var LOCAL_PATH: Location of the set of installed UIEs for this user
@type LOCAL_PATH: string
@var GLOBAL_PATH: Location of the set of installed UIEs for all users of the
  system
@type GLOBAL_PATH: string
@var PERK: Kind of UIE responsible for registering L{Task}s that handle 
  L{AEEvent}s and L{AEInput.Gesture}s
@type PERK: string
@var DEVICE: Kind of UIE responsible for managing a method of input 
    (e.g keyboard), managing a method of output (e.g speech), or both (e.g. 
    Braille device)
@type DEVICE: string
@var CHOOSER: Kind of UIE responsible for interacting with the user via more 
    than the usual LSR key combos (e.g. configuration, help, search)
@type CHOOSER: string
@var MONITOR: Kind of UIE responsible for creating a human-readable 
    representation (e.g. GUI dialog, log file) of some AccessEngine data (e.g. 
    input, output, events) for the purposes of development and debugging
@type MONITOR: string
@var ALL_KINDS: List of all known kinds of UIEs
@type ALL_KINDS: list
@var STARTUP_KINDS: Subset of L{ALL_KINDS} that should be loaded at startup
@type STARTUP_KINDS: list

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
import os, glob, imp, cPickle, sys, logging, shutil, traceback
from SettingsManager import SettingsManager
from i18n import _
from AEConstants import HOME_USER, HOME_DIR, PROG_VERSION, PROG_DATE, \
     BUILTIN_PROFILES

# create a logger for UIRegistrar
log = logging.getLogger('UIRegistrar')

# paths to the pickled listings of installed UIEs, global to the system and 
# local to this user
LOCAL_PATH = os.path.join(HOME_USER,os.path.basename(sys.argv[0])+'.installed')
GLOBAL_PATH = os.path.join(HOME_DIR, 'global.installed')

# constants representing kinds of UIEs and the folders where they are stored 
# under the INSTALL_PATH
PERK = 'perk'
DEVICE = 'device'
CHOOSER = 'chooser'
MONITOR = 'monitor'

# list of all supports types of UIEs
ALL_KINDS = [DEVICE, CHOOSER, MONITOR, PERK]
STARTUP_KINDS = [DEVICE, MONITOR]

class Metadata(object):
  '''
  Bag class for specifying UIE metadata.
  
  @ivar name: Name of the UIE which this metadata describes
  @type name: string
  @ivar path: Absolute path to the UIE on disk
  @type path: string
  @ivar kind: Kind of UIE
  @type kind: string
  @ivar tier: Name of the L{Tier} to which a L{Perk} should be applied
  @type tier: string
  @ivar all_tiers: Apply a L{Perk} to all L{Tier}s?
  @type all_tiers: boolean
  @ivar profiles: Suggested names of profiles that should be updated to include
    the UIE
  @type profiles: list of string
  '''
  def __init__(self, name, path, data):
    '''
    Checks and stores values from the data dictionary retrieved from a __uie__
    module variable in a UIE.
    
    @param name: Name of the UIE
    @type name: string
    @param path: Absolute path to the UIE on disk
    @type path: string
    @param data: Dictionary containing the __uie__ metadata
    @type data: dictionary
    '''
    self.name = name
    self.path = os.path.realpath(path)
    try:
      self.kind = data['kind']
    except KeyError:
      raise AttributeError(_('Missing kind metadata for %s') % name)
    self.tier = data.get('tier')
    self.all_tiers = bool(data.get('all_tiers', False))
    if self.kind == PERK and (not data.has_key('tier') and 
                              not data.has_key('all_tiers')):
      raise AttributeError(_('Missing Tier metadata for %s') % name)
    self.profiles = data.get('profiles', BUILTIN_PROFILES)

class UIESet(object):
  '''  
  Associates the names of installed UIEs with times when they should be
  automatically loaded by LSR under a particular user profile. Supported times
  for automatically loading UIEs include when LSR starts, when any new L{Tier}
  is created, or when a L{Tier} with a particular process name is created.
  
  @ivar name: Name of this UIESet
  @type name: string
  @ivar on_startup: Lists of UIE names keyed by UIE kind
  @type on_startup: dictionary
  @ivar on_tier: Lists of UIE names keyed by UIE kind in dictionaries keyed by
      L{Tier} name
  @type on_tier: dictionary
  @ivar on_any_tier: Lists of UIE names keyed by UIE kind
  @type on_any_tier: dictionary
  '''
  def __init__(self, name):
    '''
    Stores the name of the UIESet. Initializes the instance dictionaries.
    
    @param name: Name of the UIESet
    @type name: string
    '''
    self.name = name
    self.on_startup = {}
    self.on_tier = {}
    self.on_any_tier = {}
    
  def addToStartup(self, kind, name, index):
    '''    
    Adds the name of a UIE of the given kind to the L{on_startup} dictionary so
    that it is loaded automatically when LSR starts. The index determines when
    in the list of all L{on_startup} UIEs of the given kind the named UIE is
    loaded.
    
    @param kind: Kind of UIE, one of L{ALL_KINDS}
    @type kind: string
    @param name: Name of the UIE, unique across all UIEs of the same kind
    @type name: string
    @param index: Load order of the UIE when multiple UIEs are to be loaded at
        the given time. A value of None meaning after all other UIEs of the same
        kind.
    @type index: integer or None
    '''
    uies = self.on_startup.setdefault(kind, [])
    if name in uies:
      uies.remove(name)
    if index is None: 
      index = len(uies)
    uies.insert(index, name)
  
  def removeFromStartup(self, kind, name):
    '''    
    Removes the name of a UIE of the given kind from the L{on_startup}
    dictionary so that it is no longer loaded automatically on LSR startup.
    
    @param kind: Kind of UIE, one of L{ALL_KINDS}
    @type kind: string
    @param name: Name of the UIE, unique across all UIEs of the same kind
    @type name: string
    @raise ValueError: When the UIE of the given name and kind is not associated
        with this profile to be loaded on startup
    '''
    uies = self.on_startup.get(kind, [])
    try:
      uies.remove(name)
    except ValueError:
      raise ValueError(_('%s not associated with %s') % (name, self.name))
  
  def addToTier(self, kind, name, tier_name, index):
    '''
    Adds the name of a UIE of the given kind to the L{on_tier} dictionary so
    that it is loaded automatically when a L{Tier} with the given name is
    created. The index determines when in the list of all L{on_tier} UIEs of the
    given kind the named UIE is loaded.
    
    @param kind: Kind of UIE, one of L{ALL_KINDS}
    @type kind: string
    @param name: Name of the UIE, unique across all UIEs of the same kind
    @type name: string
    @param tier_name: Name of the L{Tier}
    @type tier_name: string
    @param index: Load order of the UIE when multiple UIEs are to be loaded at
        the given time. A value of None meaning after all other UIEs of the same
        kind.
    @type index: integer or None
    '''
    kinds = self.on_tier.setdefault(tier_name, {})
    uies = kinds.setdefault(kind, [])
    if name in uies:
      uies.remove(name)
    if index is None: 
      index = len(uies)
    uies.insert(index, name)
  
  def removeFromTier(self, kind, name, tier_name):
    '''    
    Removes the name of a UIE of the given kind from the L{on_tier}
    dictionary so that it is no longer loaded automatically on when a L{Tier}
    with the given name is created.
    
    @param kind: Kind of UIE, one of L{ALL_KINDS}
    @type kind: string
    @param name: Name of the UIE, unique across all UIEs
    @type name: string
    @param tier_name: Name of the L{Tier}
    @type tier_name: string
    @raise ValueError: When the UIE of the given name and kind is not associated
      with this profile to be loaded when the given L{Tier} starts
    '''
    kinds = self.on_tier.get(tier_name, {})
    uies = kinds.get(kind, [])
    try:
      uies.remove(name)
    except ValueError:
      raise ValueError(_('%s not associated with %s' % (name, self.name)))
    if not len(uies):
      del kinds[kind]
    if not len(kinds):
      del self.on_tier[tier_name]
  
  def addToAnyTier(self, kind, name, index):
    '''     
    Adds the name of a UIE of the given kind to the L{on_any_tier} dictionary
    so that it is loaded automatically when any L{Tier} starts. The index
    determines when in the list of all L{on_any_tier} UIEs of the given kind
    the named UIE is loaded.
    
    @param kind: Kind of UIE, one of L{ALL_KINDS}
    @type kind: string
    @param name: Name of the UIE, unique across all UIEs of the same kind
    @type name: string
    @param index: Load order of the UIE when multiple UIEs are to be loaded at
        the given time. A value of None meaning after all other UIEs of the same
        kind.
    @type index: integer or None
    '''
    uies = self.on_any_tier.setdefault(kind, [])
    if name in uies:
      uies.remove(name)
    if index is None: 
      index = len(uies)
    uies.insert(index, name)
  
  def removeFromAnyTier(self, kind, name):
    '''  
    Removes the name of a UIE of the given kind from the L{on_any_tier}
    dictionary so that it is no longer loaded automatically when any L{Tier} is
    created.
    
    @param kind: Kind of UIE, one of L{ALL_KINDS}
    @type kind: string
    @param name: Name of the UIE, unique across all UIEs of the same kind
    @type name: string
    @raise ValueError: When the UIE of the given name and kind is not associated
        with this profile to be loaded when any L{Tier} is created
    '''
    uies = self.on_any_tier.get(kind, [])
    try:
      uies.remove(name)
    except ValueError:
      raise ValueError(_('%s not associated with %s' % (name, self.name)))
    
  def removeFromAll(self, name, kind=None):
    '''
    Removes all references to the UIE with the given name and kind from the
    L{on_startup}, L{on_tier}, and L{on_any_tier} dictionaries. Ignores all
    exceptions.
    
    @param name: Name of the UIE, unique across all UIEs of the same kind
    @type name: string
    @param kind: Kind of UIE, one of L{ALL_KINDS}, or None to indicate the kind
      is unknown and all kinds should be searched
    @type kind: string
    '''
    for kind in ALL_KINDS:
      try:
        self.removeFromStartup(kind, name)
      except ValueError, e:
        pass
      try:
        self.removeFromAnyTier(kind, name)
      except ValueError, e:
        pass
      for tier_name in self.on_tier.keys():
        try:
          self.removeFromTier(kind, name, tier_name)
        except ValueError, e:
          pass
      
  def listStartup(self, kind):
    '''
    Gets a list of all UIE names of the given kind that are to be loaded at LSR
    startup.
    
    @param kind: Kind of UIE, one of L{ALL_KINDS}
    @type kind: string
    @return: Names of all UIEs to be loaded at LSR startup
    @rtype: list of string
    '''
    return self.on_startup.get(kind, [])
  
  def listTierMap(self, kind):
    '''
    Gets a list of all UIE names of the given kind paired with the names of the
    L{Tier}s on which they will load.
    
    @param kind: Kind of UIE, one of L{ALL_KINDS}
    @type kind: string
    @return: Lists of L{Perk} names associated with L{Tier} names
    @rtype: dictionary of string : list
    '''
    d = {}
    for name, kinds in self.on_tier.items():
      try:
        d[name] = kinds[kind]
      except KeyError:
        pass
    return d
  
  def listTier(self, kind, tier=None):
    '''
    Gets a list of all UIE names of the given kind that are to be loaded when 
    a L{Tier} with the given name is created. If the given L{Tier} name is 
    None, then list all UIEs to be loaded for particular L{Tier}s.
    
    @param kind: Kind of UIE, one of L{ALL_KINDS}
    @type kind: string
    @param tier: Name of the L{Tier}
    @type tier: string
    @return: Names of all UIEs to be loaded at L{Tier} creation time
    @rtype: list of string
    '''
    # get all UIEs for all named tiers
    if tier is None:
      l = []
      for name, kinds in self.on_tier.items():
        try:
          l.extend(kinds[kind])
        except KeyError:
          pass
      return l
    else:
      # get UIEs for the named tier only
      kinds = self.on_tier.get(tier, {})
      return kinds.get(kind, [])
  
  def listAnyTier(self, kind):
    '''
    Gets a list of all UIE names of the given kind that are to be loaded when 
    any L{Tier} is created.
    
    @param kind: Kind of UIE, one of L{ALL_KINDS}
    @type kind: string
    @return: Names of all UIEs to be loaded at L{Tier} creation time
    @rtype: list of string
    '''
    return self.on_any_tier.get(kind, [])

def _ensureUIEValid(real_path):
  '''    
  Ensures the UIE module at the given path contains a class matching the
  stated name. Ensure L{Metadata} is specified in the UIE to aid in
  installation and association. Returns the UIE metadata.

  @note: No longer checks if a class fits the definition of the given kind of
  UIE. This validation is difficult when the module imports dependencies and
  actually provides little useful information about whether or not the UIE is
  implemented correctly. It can only tell us if it provides the proper
  interface. We have gracefully degredation at runtime, so why bother "almost
  ensuring" it's valid at install time?
  
  @param real_path: Real path to the UIE module located on disk
  @type real_path: string
  @return: Metadata from the UIE
  @rtype: L{Metadata}
  @raise AttributeError: When __uie__ dictionary is missing from the UIE
  @raise IOError: When the UIE module cannot be read
  '''
  # compute the name of the UIE
  name, ext = os.path.basename(real_path).split(os.extsep)
  # read the UIE file as text
  text = file(real_path).read()
  # make sure it contains a class of the same name
  if text.find('class %s' % name) < 0:
    raise AttributeError(
      _('Module %s must have class with name %s') % (name, name))
  # check for and get the metadata
  return _parseUIEMetadata(name, real_path, text)

def _parseUIEMetadata(name, path, text):
  '''
  Gets the metadata from a UIE module without importing it.
  
  @param name: Name of the installed UIE to fetch metadata from
  @type name: string
  @param text: Pre-fetched contents of the UIE
  @type text: string
  @return: Metadata from the UIE
  @rtype: L{Metadata}
  @raise AttributeError: When __uie__ dictionary is missing from the UIE
  @raise KeyError: When the named UIE is not installed
  @raise IOError: When the UIE module cannot be read
  '''
  # find the __uie__ line
  s = text.find('__uie__')
  if s < 0 or (s != 0 and text[s-1] != '\n'):
    raise AttributeError(
      _('Module %s does not have __uie__ metadata') % name)
  e = text.find('\n', s)
  # turn the __uie__ var into a dictionary
  exec(text[s:e])
  # and wrap it in a convenience class
  return Metadata(name, path, __uie__)

def _getUIEInstance(name):
  '''
  Gets an instance of the UIE class having the same name as the module 
  indicated by the given name.
    
  @param name: Name of the UIE, unique across all UIEs of the same kind
  @type name: string
  @return: UIE instance
  @rtype: object
  @raise KeyError: When the UIE is not installed globally or locally
  @raise AttributeError: When the module is missing a class of the given name
  @raise ImportError: When the UIE module cannot be imported
  @raise IOError: When the UIE module cannot be read
  '''
  mod = _getUIEModule(name)
  # now get the class and instantiate it
  cls = getattr(mod, name)
  return cls()

def _getUIEModule(name):
  '''
  Loads the UIE module of the given name. Returns a reference to the module 
  for use by L{_getUIEInstance} by querying the L{InstallCache}.
  
  @param name: Name of the UIE, unique across all UIEs
  @type name: string
  @return: Reference to the UIE module
  @rtype: module
  @raise KeyError: When the UIE is not installed globally or locally
  @raise IOError: When the UIE module cannot be read
  @raise ImportError: When the UIE module cannot be imported
  '''
  # get the path without the filename of the UIE module
  metadata = InstallCache.get(name)
  path = os.path.dirname(metadata.path)
  # get the metadata for the module to import
  f, pathname, description = imp.find_module(name, [path])
  # add the foldering containing the UIE to the Python search path temporarily
  sys.path.append(os.path.dirname(pathname))
  try:
    # load the module from disk
    m = imp.load_module(name, f, pathname, description)
  finally:
    # make sure we close the file
    f.close()
    # remove the last added search path
    sys.path.pop()
  # get the module
  return m

def install(real_path, local=True, overwrite=False):
  '''  
  Installs a new UIE module in either the local or global repository.
  Installing in the global repository requires write permissions on the LSR
  home directory at its install location. Checks if the UIE to be installed is
  valid as determined by L{_ensureUIEValid} and retrieves its metadata.
  
  @param real_path: Relative or absolute location of the UIE module on disk
  @type real_path: string
  @param local: Install this UIE for the current user only (True) or for all 
    users (False)?
  @type local: boolean
  @param overwrite: Ignore errors if a UIE is already installed and overwrite?
  @type overwrite: boolean
  @raise AttributeError: When __uie__ dictionary is missing from the UIE
  @raise KeyError: When the named UIE is already installed
  @raise IOError: When the UIE module cannot be read
  '''
  # make sure the UIE is valid
  metadata = _ensureUIEValid(real_path)
  if local:
    InstallCache.addLocal(metadata, overwrite)
  else:
    InstallCache.addGlobal(metadata, overwrite)

def uninstall(name, local=True):
  '''
  Removes a UIE module from the global or local repository. Removes all 
  references to a UIE uninstalled from the local repository from existing 
  profiles.
  
  @param name: Name of the UIE, unique across all UIEs
  @type name: string
  @param local: Uninstall this UIE for the current user only (True) or for all 
    users (False)?
  @type local: boolean
  @raise AttributeError: When __uie__ dictionary is missing from the UIE
  @raise KeyError: When the UIE is not already installed
  @raise IOError: When the local on-disk cache cannot be updated
  '''
  # remove the install cache entry
  if local:
    metadata = InstallCache.removeLocal(name)
  else:
    metadata = InstallCache.removeGlobal(name)
  kind = metadata.kind
  # remove all references in the available profiles
  for pn in SettingsManager.listProfiles():
    sm = SettingsManager(profile=pn)
    uieset = sm.load(__name__)
    uieset.removeFromAll(name, kind)
    sm.save(__name__, uieset)

def associate(name, profiles=None, tier=None, all_tiers=False, 
              index=None):
  '''
  Associates an installed UIE of the given name with a profile so that it is 
  loaded automatically by LSR.
  
  @param name: Name of the UIE, unique across all UIEs
  @type name: string
  @param profiles: Names of the profiles in which UIE of the given name will 
    be loaded automatically. Defaults to None meaning L{BUILTIN_PROFILES}
    will be used.
  @type profiles: list of string
  @param tier: Name of the L{Tier} which will cause the loading of the
    given UIE. Defaults to None.
  @type tier: string
  @param all_tiers: Load this UIE on all L{Tier}s?
  @type all_tiers: boolean
  @param index: Load order of the UIE when multiple UIEs are to be loaded at
    the given time. Defaults to None meaning after all other associated UIEs
    of the same kind.
  @type index: integer
  @return: Names of profiles with which the UIE was associated
  @rtype: list of string
  @raise KeyError: When the UIE is not already installed
  '''
  # get the UIE metadata
  metadata = InstallCache.get(name)
  profiles = profiles or BUILTIN_PROFILES
  all_tiers = all_tiers or metadata.all_tiers
  tier = tier or metadata.tier
  associated = []
  for profile in profiles:
    # load the UIESet
    sm = SettingsManager(profile)
    try:
      uieset = sm.load(__name__)
    except KeyError:
      # create a new UIESet if it does not exist for this profile
      uieset = UIESet(profile)
    # decide when to load the give UIE
    if metadata.kind in STARTUP_KINDS:
      uieset.addToStartup(metadata.kind, name, index)
    elif all_tiers:
      uieset.addToAnyTier(metadata.kind, name, index)
    elif tier is not None:
      uieset.addToTier(metadata.kind, name, tier, index)  
    else:
      # not an automatically loaded UIE
      continue    
    associated.append(profile)
    # write the profile back to disk
    sm.save(__name__, uieset)
  return associated

def disassociate(name, profiles=None, tier=None, all_tiers=False):
  '''
  Disassociates an installed UIE of the given name from a profile so 
  that it is no longer loaded automatically by LSR.

  @param name: Name of the UIE, unique across all UIEs
  @type name: string
  @param profiles: Names of the profiles in which UIE of the given name will 
    no longer be loaded automatically. Defaults to None meaning 
    L{BUILTIN_PROFILES} will be used.
  @type profiles: list of string
  @param tier: Name of the L{Tier} which will no longer cause the loading
    of the given UIE. Defaults to None.
  @type tier: string
  @param all_tiers: Disassociate this UIE from loading in all L{Tier}s?
  @type all_tiers: boolean
  @return: Names of profiles from which the UIE was disassociated
  @rtype: list of string
  '''
  try:
    # get the UIE metadata
    metadata = InstallCache.get(name)
    all_tiers = all_tiers or metadata.all_tiers
    tier = tier or metadata.tier
  except KeyError:
    # the UIE might be missing already
    metadata = None
  profiles = profiles or BUILTIN_PROFILES
  disassociated = []
  for profile in profiles:
    # load the UIESet
    sm = SettingsManager(profile)
    try:
      uieset = sm.load(__name__)
    except KeyError:
      # create a new UIESet if it does not exist for this profile
      uieset = UIESet(profile)
    disassociated.append(profile)
    try:
      # remove from all if the metadata is no longer available
      if metadata is None:
        uieset.removeFromAll(name)
        # decide when not to load the given UIE
      elif metadata.kind in STARTUP_KINDS:
        uieset.removeFromStartup(metadata.kind, name)
      elif all_tiers:
        uieset.removeFromAnyTier(metadata.kind, name)
      elif tier is not None:
        uieset.removeFromTier(metadata.kind, name, tier)     
    except ValueError:
      pass
    # write the profile back to disk
    sm.save(__name__, uieset)
  return disassociated

def listInstalled(kind=None):
  '''
  Gets a list of all installed UIEs of the given kind, both local and global.
  
  @param kind: Kind of UIE, one of L{ALL_KINDS} or None to indicate all kinds
  @type kind: string
  @return: Names of installed UIEs
  @rtype: list of string
  '''
  # gets lists of local and global installed UIEs
  uies = InstallCache.list()
  if kind is None:
    # return all if no kind specified
    return uies
  # only get the kind indicated
  return [metadata.name for metadata in 
          (InstallCache.get(name) for name in uies)
          if (metadata.kind == kind and os.path.exists(metadata.path))]

def hasTierAssociated(kind, profile, tier):
  '''
  Gets if at least one UIE of the given kind is associated with the given 
  L{Tier} in the profile.
  
  @param kind: Kind of UIE, one of L{ALL_KINDS}
  @type kind: string
  @param profile: Name of the profile
  @type profile: string
  @param tier: Name of the L{Tier} potentially associated with UIEs
  @type tier: string
  @return: At least one L{Tier} specific UIE?
  @rtype: boolean
  @raise ValueError: When the named profile does not exist
  '''
  # load the profile
  sm = SettingsManager(profile)
  sm.ensureProfile()
  try:
    uieset = sm.load(__name__)
  except KeyError:
    # no information stored, so create a new UIESet
    uieset = UIESet(profile)
    sm.save(__name__, uieset)
  return len(uieset.listTier(kind, tier)) > 0

def listAssociatedMap(kind, profile):
  '''
  Gets a map associating L{Tier} names with UIEs of the given kind in this
  profile.
  
  @param kind: Kind of UIE, one of L{ALL_KINDS}
  @type kind: string
  @param profile: Name of the profile
  @type profile: string
  @return: Dictionary mapping L{Tier} names to lists of UIE names
  @rtype: dictionary of string : list
  @raise ValueError: When the named profile does not exist
  '''
  # load the profile
  sm = SettingsManager(profile)
  sm.ensureProfile()
  try:
    uieset = sm.load(__name__)
  except KeyError:
    # no information stored, so create a new UIESet
    uieset = UIESet(profile)
    sm.save(__name__, uieset)
  return uieset.listTierMap(kind)

def listAssociatedAny(kind, profile):
  '''
  Gets a list of UIEs of the given kind associated with any L{Tier} in this
  profile.
  
  @param kind: Kind of UIE, one of L{ALL_KINDS}
  @type kind: string
  @param profile: Name of the profile
  @type profile: string
  @return: List of UIEs that load on any L{Tier}
  @rtype: list of string
  @raise ValueError: When the named profile does not exist
  '''
  # load the profile
  sm = SettingsManager(profile)
  sm.ensureProfile()
  try:
    uieset = sm.load(__name__)
  except KeyError:
    # no information stored, so create a new UIESet
    uieset = UIESet(profile)
    sm.save(__name__, uieset)
  return uieset.listAnyTier(kind)
  
def listAssociated(kind, profile, tier=None):
  '''
  Gets a list of the names of all UIEs of the given kind associated with the
  given profile. The tier may be specified for L{PERK}s. If it is, only UIEs
  associated with that L{Tier} are returned.
    
  @param kind: Kind of UIE, one of L{ALL_KINDS}
  @type kind: string
  @param profile: Name of the profile
  @type profile: string
  @param tier: Name of the L{Tier} associated with the UIEs. If None, fetches
    UIE names for all L{Tier}s.
  @type tier: string
  @return: Names of the UIEs
  @rtype: list of string or 2-tuple
  @raise ValueError: When the named profile does not exist
  '''
  # load the profile
  sm = SettingsManager(profile)
  sm.ensureProfile()
  try:
    uieset = sm.load(__name__)
  except KeyError:
    # no information stored, so create a new UIESet
    uieset = UIESet(profile)
    sm.save(__name__, uieset)
  # decide what list to check
  if kind in STARTUP_KINDS:
    # kind of UIE that loads on startup
    return uieset.listStartup(kind)
  else:
    # Perks that load on a particular Tier
    return uieset.listAnyTier(kind) + uieset.listTier(kind, tier)
  
def getPath(name):
  '''
  Gets the absolute path where an installed UIE is located on disk. Does not
  include the filename of the UIE.
  
  @param name: Name of the UIE, unique across all UIEs
  @type name: string 
  @return: Absolute parent directory of the UIE module on disk
  @rtype: string
  @raise KeyError: When a UIE with the given name is not installed
  '''
  path = InstallCache.get(name).path
  return os.path.dirname(path)

def loadOne(name):
  '''
  Gets the class representing the installed UIE of the given kind and name.
  Checks if the UIE is installed. Fails to load the UIE if any of the
  dependencies are missing.
  
  @param name: Name of the UIE, unique across all UIEs of the same kind
  @type name: string
  @return: Instantiated UIE object or None if the object could not be loaded
  @rtype: object
  '''
  try:
    return _getUIEInstance(name)
  except SyntaxError, e:
    # on any exception, pass on loading this UIE, but log the error
    log.error('syntax error in %s: %s', name, str(e))
    return None    
  except Exception, e:
    # on any exception, pass on loading this UIE, but log the error
    info = traceback.extract_tb(sys.exc_info()[2])
    log.debug('cannot load %s (%d): %s', name, info[-1][1], str(e))
    return None

def loadAssociated(kind, profile, tier=None):
  '''
  Gets all UIE classes in the given profile to be loaded at the given time.
  
  @param kind: Kind of UIE, one of L{ALL_KINDS}
  @type kind: string
  @param profile: Name of the profile
  @type profile: string
  @param tier: Name of the L{Tier} which causes the loading of the listed UIEs
  @type tier: string
  @return: All UIE instances to be loaded sorted in proper load order
  @rtype: list of object
  '''
  names = listAssociated(kind, profile, tier)
  return [uie for uie in (loadOne(name) for name in names) if uie is not None]

class InstallCache(object):
  '''
  Manages an in-memory cache of installed UIEs with persistent storage of the 
  cache on disk. The in-memory cache is brought up-to-date each time data is 
  read on the cache. The on-disk cache is updated each time new data is written
  to the cache.
  
  @ivar l_timestamp: Timestamp of the local cache file on disk
  @type l_timestamp: integer
  @ivar g_timestamp: Timestamp of the global cache file on disk
  @type g_timestamp: integer
  @ivar local: In-memory cache of UIEs installed for the local user pairing UIE
    names with their absolute install paths
  @type local: dictionary
  @ivar local: In-memory cache of UIEs installed globally to be shared across 
    all users of the system user pairing UIE names with their absolute install 
    paths
  @type local: dictionary
  '''
  def __init__(self):
    '''
    Initialize timestamps and try to load UIEs from disk.
    '''
    # initialize the time stamps
    self.l_timestamp = 0
    self.g_timestamp = 0
    # initialize the local and global install dictionaries in memory
    self.local = {}
    self.globl = {}
    
  def init(self):
    '''
    Try to read the local and global contents from disk.
    '''
    self._refreshLocal()
    self._refreshGlobal()

  def _write(self, path, data):
    '''
    Persist the in-memory cache on disk. Include the current version and 
    revision information.
    
    @param path: Path to the on-disk cache
    @type path: string
    @param data: Dictionary of name/value pairs
    @type data: dictionary
    @raise IOError: When the cache file cannot be written
    '''
    try:
      f = file(path, 'wb')
      cPickle.dump((data, PROG_VERSION+PROG_DATE), f, protocol=-1)
    except cPickle.PickleError:
      f.close()
      raise IOError

  def _read(self, path):
    '''
    Read the on-disk cache into memory. Loads the version and revision 
    information persisted when the cache was written also.
    
    @param path: Path to the on-disk cache
    @type path: string
    @return: Dictionary of name/value pairs and version/revision informatoin
    @rtype: tuple of dictionary and string
    @raise IOError: When the cache file cannot be read
    '''
    try:
      f = file(path, 'rb')
      return cPickle.load(f)
    except cPickle.PickleError:
      f.close()
      raise IOError
      
  def _mtime(self, path):
    '''
    @return: Modified time on the file at path
    @rtype: integer
    '''
    try:
      return os.stat(path).st_mtime
    except OSError:
      return 0
  
  def _refreshLocal(self):
    '''
    Refreshes the in-memory local cache if the on-disk cache is newer.
    
    @raise IOError: When the cache file cannot be read
    '''
    mtime = self._mtime(LOCAL_PATH)
    if mtime > self.l_timestamp:
      self.local, version = self._read(LOCAL_PATH)
      self.l_timestamp = mtime
      
  def _refreshGlobal(self):
    '''
    Refreshes the in-memory global cache if the on-disk cache is newer.
    
    @raise IOError: When the cache file cannot be read
    '''
    mtime = self._mtime(GLOBAL_PATH)
    if mtime > self.g_timestamp:
      self.globl, version = self._read(GLOBAL_PATH)
      self.g_timestamp = mtime
      #self._updateDefaultProfiles()
      
  #def _updateDefaultProfiles(self):
    #'''
    #Updates the L{BUILTIN_PROFILES} by associating newly installed global UIEs 
    #and removing uninstalled global UIEs.
    
    #@note: Not currently used. May be supported in future versions.
    #'''
    #for profile in BUILTIN_PROFILES:
      #sm = SettingsManager(profile)
      ## load the UIESet for the profile
      #try:
        #uieset = sm.load(__name__)
        ## get the UIEs already installed in the profile
        #wl = uieset.getWhitelist()
        ## and those that were once installed but now removed
        #bl = uieset.getBlacklist()
      #except KeyError:
        #wl = bl = set()
      ## finally, get the set of globally installed UIEs
      #il = set(self.globl)
      ## UIEs to associate: (il - wl) - bl
      #for name in (il - wl) - bl:
        #associate(name, [profile])
      ## UIEs to disassociate: (wl - il)
      #for name in wl - il:
        #disassociate(name, [profile])

  def list(self):
    '''
    Lists all installed UIEs, regardless of local or global status.
    
    @return: List of UIE names
    @rtype: list
    '''
    self._refreshLocal()
    self._refreshGlobal()
    return self.local.keys() + self.globl.keys()
  
  def addLocal(self, metadata, overwrite):
    '''
    Adds a new UIE to the list of UIEs installed locally.
    
    @param metadata: Metadata describing the UIE
    @type metadata: L{Metadata}
    @param overwrite: Ignore errors if a UIE is already installed and 
      overwrite?
    @type overwrite: boolean
    @raise KeyError: When a UIE of the given name is already installed locally
      or globally
    @raise IOError: When the local on-disk cache cannot be updated
    '''
    name = metadata.name
    if not overwrite:
      if self.local.has_key(name):
        raise KeyError(_('%s is already installed locally') % name)
      if self.globl.has_key(name):
        raise KeyError(_('%s is already installed globally') % name)
    self.local[name] = metadata
    self._write(LOCAL_PATH, self.local)
  
  def addGlobal(self, metadata, overwrite):
    '''
    Adds a new UIE to the list of UIEs installed globally.
    
    @param metadata: Metadata describing the UIE
    @type metadata: L{Metadata}
    @param overwrite: Ignore errors if a UIE is already installed and 
      overwrite?
    @type overwrite: boolean
    @raise KeyError: When a UIE of the given name is already installed globally
    @raise IOError: When the global on-disk cache cannot be updated
    '''
    name = metadata.name
    if not overwrite:
      if self.globl.has_key(name):
        raise KeyError(_('%s is already installed globally') % name)
    self.globl[name] = metadata
    self._write(GLOBAL_PATH, self.globl)
  
  def removeLocal(self, name):
    '''
    Uninstalls an existing UIE from the local cache.
    
    @param name: Name of the UIE
    @type name: string
    @return: Metadata for the removed UIE
    @rtype: L{Metadata}
    @raise KeyError: When the UIE is not already installed
    @raise IOError: When the local on-disk cache cannot be updated
    '''
    try:
      metadata = self.local[name]
    except KeyError:
      raise KeyError(_('%s is not installed locally') % name)
    del self.local[name]
    self._write(LOCAL_PATH, self.local)
    return metadata
    
  def removeGlobal(self, name):
    '''
    Uninstalls an existing UIE from the global cache.
    
    @param name: Name of the UIE
    @type name: string
    @return: Metadata for the removed UIE
    @rtype: L{Metadata}
    @raise KeyError: When the UIE is not already installed
    @raise IOError: When the global on-disk cache cannot be updated
    '''
    try:
      metadata = self.globl[name]
    except KeyError:
      raise KeyError(_('%s is not installed globally') % name)
    del self.globl[name]
    self._write(GLOBAL_PATH, self.globl)
    return metadata

  def get(self, name):
    '''
    Gets the absolute path to an installed UIE whether it is local or global.
    
    @param name: Name of the UIE
    @type name: string
    @param metadata: Metadata describing the UIE
    @type metadata: L{Metadata}
    @raise KeyError: When the UIE isn't installed locally or globally
    '''
    try:
      self._refreshLocal()
      return self.local[name]
    except KeyError:
      pass
    self._refreshGlobal()
    try:
      return self.globl[name]
    except KeyError:
      raise KeyError(_('%s not installed') % name)

# make the install cache a singleton
InstallCache = InstallCache()
InstallCache.init()
