'''
Defines an L{AEEvent} indicating the view has been updated by the
L{ViewManager}.

@author: Peter Parente
@author: Pete Brunet
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''

import Base
import AEConstants
from AEInterfaces import *

class ViewChange(Base.AccessEngineEvent):
  '''
  Event that fires when the L{ViewManager} is creating or updating its view.
  
  This class registers its name and whether it should be monitored by default
  in an L{AEMonitor} using the L{Base.registerEventType} function when
  this module is first imported. An L{AEMonitor} can use this information to
  build its menus.

  @ivar gained: Type of view change
  @type gained: boolean
  @ivar title: Name of the root element of the new view (e.g. title of the
    foreground window)
  @type title: string
  '''
  Base.registerEventType('ViewChange', True)
  def __init__(self, por, gained, **kwargs):
    '''
    Stores the type of view change and intializes the title attribute to
    an empty string.

    @param por: Point-of-regard to the accessible at the root of the new view
    @type por: L{POR}    
    @param gained: Type of view change
    @type gained: integer
    '''
    focused = gained in (AEConstants.EVENT_VIEW_GAINED, 
                         AEConstants.EVENT_VIEW_FIRST_GAINED)
    Base.AccessEngineEvent.__init__(self, focused=focused, **kwargs)
    self.por = por
    self.gained = gained
    self.title = ''
    
  def __str__(self):
    '''
    Returns a human readable representation of this event including its name,
    its L{POR}, its type, and the title of the new view.
    
    @return: Information about this event
    @rtype: string
    '''
    name = Base.AccessEngineEvent.__str__(self)
    if self.gained == AEConstants.EVENT_VIEW_GAINED:
      action = 'gained'
    elif self.gained == AEConstants.EVENT_VIEW_LOST:
      action = 'lost'
    elif self.gained == AEConstants.EVENT_VIEW_FIRST_GAINED:
      action = 'first gained'
    elif self.gained == AEConstants.EVENT_VIEW_STARTUP:
      action = 'startup'
    return '%s:\n\tPOR: %s\n\ttitle: %s\n\taction: %s' % (name, self.por,
                                                          self.title, action)

  def execute(self, tier_manager, view_manager, **kwargs):
    '''
    Stores the name of the root element of the new view in the title
    attribute. Calls L{TierManager.TierManager.switchTier} to switch to the 
    appropriate L{Tier} for this view. Calls 
    L{TierManager.TierManager.manageEvent} to allow it to dispatch this
    L{AEEvent} to the active L{Tier} and its registered L{Perk}.

    @param tier_manager: TierManager that will handle the event
    @type tier_manager: L{TierManager}
    @param view_manager: ViewManager defining the current View
    @type view_manager: L{ViewManager}
    @param kwargs: Packed references to other managers not of interest here
    @type kwargs: dictionary
    @return: True to indicate the event executed properly
    @rtype: boolean
    '''
    # get the accessible name of the root element
    ai = IAccessibleInfo(self.por)
    try:
      self.title = ai.getAccName()
    except LookupError:
      # view is dead
      return True
    if self.gained in (AEConstants.EVENT_VIEW_GAINED, 
                       AEConstants.EVENT_VIEW_FIRST_GAINED):
      # get information about the accessible's application
      try:
        aid = ai.getAccAppID()
        aname = ai.getAccAppName()
      except LookupError:
        # view is dead
        return True
      # inform the view manager of the AE view change
      view_manager.setAEView(self.por)
      # ask the tier manager to switch tiers first
      tier_manager.switchTier(aname, aid, self.por)
    elif self.gained == AEConstants.EVENT_VIEW_LOST:
      if self.por == view_manager.getAEView():
        # only unset if the view being deactivated is still the active view
        view_manager.setAEView(None)
    # then process the view change event
    tier_manager.manageEvent(self)
    return True
  
  def getDataForTask(self):
    '''
    Fetches data out of this L{ViewChange} for use by a L{Task.ViewTask}.
    
    @return: Dictionary of parameters to be passed to a L{Task.ViewTask} as
      follows:
        - por: Point of regard to the top of the new view
        - title: String containing the window title of the view which changed
        - gained: The type of view change, indicated by one of the integer
          class variables
    @rtype: dictionary
    '''
    return {'title':self.title, 'gained':self.gained, 'por':self.getPOR()}
