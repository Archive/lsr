'''
Defines the base class for all L{AccessEngine} events (L{AEEvent}s).

@author: Peter Parente
@author: Pete Brunet
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
import AEConstants
from AEInterfaces import *

default_types = []
all_types = []

def registerEventType(name, default):
  '''
  Called once by every L{AEEvent} class to register itself for buffering and 
  filtering by an L{AEMonitor}.
  
  @param name: Name of the L{AEEvent}
  @type name: string
  @param default: Should this L{AEEvent} be buffered by default?
  @type default: boolean
  '''
  global default_types
  global all_types
  all_types.append(name)
  if default:
    default_types.append(name)
    
def getDefaults():
  '''
  Suggests the default L{AEEvent}s to be monitored.
  
  @return: Names of defaults to monitor
  @rtype: list of string
  '''
  return default_types  
  
def getNames():
  '''
  Gets the names of all the L{AEEvent} types.
  
  @return: List of all L{AEEvent} names
  @rtype: list of string
  '''
  return all_types

class AccessEngineEvent(object):
  '''
  Most base class for all L{AccessEngine} events. Supports the concept of a 
  priority which may be used to prioritize event execution.

  @ivar priority: Priority of this event
  @type priority: integer
  @ivar focused: Was the source of the event focused at the time of event
    creation?
  @type focused: boolean
  @ivar por: Point of regard associated with this event
  @type por: L{POR}
  '''
  def __init__(self, priority=AEConstants.EXEC_NORMAL, focused=False,**kwargs):
    '''
    Stores the event priority and whether the event source was focused or not.

    @param priority: Priority of this event, defaults to normal
    @type priority: integer    
    @param focused: Was the source of the event focused at the time of event 
      creation?
    @type focused: boolean
    '''
    self.priority = priority
    self.por = None
    if focused:
      self.layer = AEConstants.LAYER_FOCUS
    else:
      self.layer = AEConstants.LAYER_BACKGROUND
    
  def __str__(self):
    '''
    Returns the name of this event.
    
    @return: Class name of this AEEvent
    @rtype: string
    '''
    level = AEConstants.LAYER_NAMES[self.layer]
    return '%s (%s)' % (self.__class__.__name__, level)
    
  def execute(self, **kwargs):
    '''
    Executes the logic that will handle this event. The L{EventManager} can 
    provide whatever arguments are necessary for an L{AEEvent} subclass to 
    execute as keyword arguments. It is up to the subclass to unpack and use 
    the arguments it needs by name.
    
    See L{EventManager.EventManager._executeEvent} for the arguments provided.
    
    @param kwargs: Named references to various managers
    @type kwargs: dictionary
    @return: Was this class able to execute successfully? Always True here.
    @rtype: boolean
    '''
    return True
    
  def getPriority(self):
    '''
    @return: Current priority value
    @rtype: integer
    '''
    return self.priority
    
  def setPriority(self, priority):
    '''
    @param priority: New priority value
    @type priority: integer
    '''
    self.priority = priority
    
  def setLayer(self, layer):
    '''
    @param layer: Layer of this event (focus, tier, background)
    @type layer: integer
    '''
    self.layer = layer
    
  def getLayer(self):
    '''
    Gets the layer for this event. If the L{POR} is marked as imcomplete, builds
    a complete L{POR} and then determines if the proper L{POR} is in a different
    layer than the one originally computed.
    
    @return: Gets the layer for this event
    @rtype: integer
    '''
    if self.por is not None and self.por.incomplete:
      if IAccessibleInfo(self.getPOR()).hasAccState('focused'):
        self.layer = AEConstants.LAYER_FOCUS
    return self.layer

  def getDataForTask(self):
    '''
    Fetches data out of an L{AccessEngineEvent} for use by a L{Task}. This 
    method must be implemented by any L{AEEvent} that will be handled by 
    L{TierManager.TierManager.manageEvent} or
    L{TierManager.TierManager.manageGesture}.
    
    @return: Empty dictionary
    @rtype: dictionary
    '''
    return {}
  
  def getTimestamp(self):
    '''
    Gets the timestamp for when the event occurred.
    
    @return: Always zero when not overridden
    @rtype: float
    '''
    return 0
  
  def getPOR(self):
    '''
    @return: Point of regard associated with this event
    @rtype: L{POR}
    '''
    if self.por is not None and self.por.incomplete:
      self.por = IPORFactory(self.por).create()
    return self.por
  
  def getAppID(self):
    '''
    @return: Unique application ID identifying the top most container for the
      source of this event (i.e. the application)
    @rtype: opaque object
    '''
    if self.por is None:
      return None
    else:
      try:
        return IAccessibleInfo(self.por).getAccAppID()
      except LookupError:
        # app is dead
        return None
