'''
Defines an L{AEEvent} indicating that a child has been added/removed to an 
accessible.

@author: Brett Clippingdale
@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''

import Base
from AEInterfaces import *

class ChildrenChange(Base.AccessEngineEvent):
  '''
  Event that fires on children changes.
  
  This class registers its name and whether it should be monitored by default in
  an L{AEMonitor} using the L{Base.registerEventType} function
  when this module is first imported. The L{AEMonitor} can use this
  information to build its menus.

  @ivar por: The L{POR} of the parent
  @type por: L{POR}
  @ivar added: True when a child is added, False when removed
  @type added: boolean
  @ivar child_por: The L{POR} of added/removed child
  @type child_por: L{POR}
  '''
  Base.registerEventType('ChildrenChange', False)
  def __init__(self, por, added, child_por, **kwargs):
    '''
    Stores the L{POR}, event name, first and last children (if any) associated
    with the event.   
    '''
    Base.AccessEngineEvent.__init__(self, **kwargs)
    self.por = por
    self.added = added
    self.child_por = child_por
    
  def __str__(self):
    '''
    Returns a human readable representation of this event including its name,
    its L{POR}, its event name, and its associated values.
    
    @return: Information about this event
    @rtype: string
    '''
    name = Base.AccessEngineEvent.__str__(self)
    return '%s:\n\tPOR: %s\n\tadded: %s\n\tchild_por: %s\n' \
           %(name, self.por, self.added, self.child_por)
  
  def execute(self, tier_manager, **kwargs):
    '''
    Contacts the L{TierManager} so it can manage the children change event.
    
    @param tier_manager: TierManager that will handle the event
    @type tier_manager: L{TierManager}
    @param kwargs: Packed references to other managers not of interest here
    @type kwargs: dictionary
    @return: Always True to indicate the event executed properly
    @rtype: boolean
    '''
    tier_manager.manageEvent(self)
    return True

  def getDataForTask(self):
    '''
    Fetches data out of this L{AEEvent.ChildrenChange} for use by a
    L{Task.ChildrenTask}.
    
    @return: Dictionary of parameters to be passed to a
      L{Task.ChildrenTask} as follows:
        - por:  The L{POR} of the accessible parent
        - added: True when a child is added, False when removed
        - child_por: The L{POR} of added/removed child
    '''
    if self.child_por.incomplete:
      self.child_por = IPORFactory(self.child_por).create()
    return {'por':self.getPOR(), 'added':self.added, 'child_por':self.child_por}