'''
Defines an L{AEEvent} indicating that a table has had a row or column added or
deleted, or has been reordered.

@author: Brett Clippingdale
@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''

import Base

class TableChange(Base.AccessEngineEvent):
  '''
  Event that fires when the rows and columns in a table change.
  
  This class registers its name and whether it should be monitored by default in
  an L{AEMonitor} using the L{Base.registerEventType} function
  when this module is first imported. The L{AEMonitor} can use this
  information to build its menus.

  @ivar por: Point of regard for the table
  @type por: L{POR}
  @ivar is_row: Is it a row (True) or column (False) that changed?
  @type is_row: boolean
  @ivar added: Was the row/col added (True), deleted (False), or moved (None)?
  @type added: boolean
  @ivar first_child_por: Point of regard of first added/removed/moved child
  @type first_child_por: L{POR}
  @ivar last_child_por: Point of regard of last added/removed/moved child
  @type last_child_por: L{POR}
  '''
  Base.registerEventType('TableChange', False)
  def __init__(self, por, is_row, added, first_child_por, last_child_por, 
               **kwargs):
    '''
    Stores the L{POR}, information about the change, and first and last children
    associated with the event.

    @param por: Point of regard for the table
    @type por: L{POR}
    @param is_row: Is it a row (True) or column (False) that changed?
    @type is_row: boolean
    @param added: Was the row/col added (True), deleted (False), or moved (None)?
    @type added: boolean
    @param first_child_por: Point of regard of first added/removed/moved child
    @type first_child_por: L{POR}
    @param last_child_por: Point of regard of last added/removed/moved child
    @type last_child_por: L{POR}
    '''
    Base.AccessEngineEvent.__init__(self, **kwargs)
    self.por = por
    self.is_row = is_row
    self.added = added
    self.first_child_por = first_child_por
    self.last_child_por = last_child_por
    
  def __str__(self):
    '''
    Returns a human readable representation of this event including its name,
    its L{POR}, whether it concerns a row or col, whether it describes an
    add, remove, or move; the first affected child L{POR} and the last affected
    child L{POR}.
    
    @return: Information about this event
    @rtype: string
    '''
    name = Base.AccessEngineEvent.__str__(self)
    return '%s:\n\tPOR: %s\n\tis_row: %s\n\tadded: %s \
            \n\tfirst_child_por: %s\n\t\last_child_por: %s\n\t\layer: %s' \
            % (name, self.por, self.is_row, self.added, self.first_child_por, 
             self.last_child_por, self.layer)
  
  def execute(self, tier_manager, **kwargs):
    '''
    Contacts the L{TierManager} so it can manage the table change event.
    
    @param tier_manager: TierManager that will handle the event
    @type tier_manager: L{TierManager}
    @param kwargs: Packed references to other managers not of interest here
    @type kwargs: dictionary
    @return: Always True to indicate the event executed properly
    @rtype: boolean
    '''
    tier_manager.manageEvent(self)
    return True

  def getDataForTask(self):
    '''
    Fetches data out of this L{TableChange} for use by a L{Task.TableTask}.
    
    @return: Dictionary of parameters to be passed to a L{Task.TableTask} as
    follows:
        - por:  The L{POR} of the accessible whose property changed
        - is_row: Row (True) or column (False) changes
        - added: Added (True), removed (False), or moved (None)
        - first_child_por: The L{POR}of first added/removed/moved child
        - last_child_por: The L{POR} of last added/removed/moved child
    '''
    return {'por':self.getPOR(), 'is_row':self.is_row, 'added':self.added, 
            'first_child_por':self.first_child_por,
            'last_child_por':self.last_child_por}