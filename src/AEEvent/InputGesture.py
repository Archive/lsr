'''
Defines an L{AEEvent} indicating a gesture has been performed on an L{AEInput} 
device.

@author: Peter Parente
@author: Scott Haeger
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
import AEInput
import Base

class InputGesture(Base.AccessEngineEvent):
  '''  
  Event that fires when some L{AEInput.Gesture} is detected on an L{AEInput}
  device. Always defaults to the focused layer.
  
  This class registers its name and whether it should be monitored by default in
  an L{AEMonitor} using the L{Base.registerEventType} function
  when this module is first imported. The L{AEMonitor} can use this
  information to build its menus.
  
  @ivar gesture: Gesture detected
  @type gesture: L{AEInput.Gesture}
  @ivar timestamp: Time at which the gesture was received on the device
  @type timestamp: float
  @ivar kwargs: Additional arguments to be passed to a L{Task.InputTask}
  @type kwargs: dictionary
  '''
  Base.registerEventType('InputGesture', True)
  def __init__(self, gesture, timestamp, **kwargs):
    '''
    Calls the base class (which sets the event priority) and then stores the 
    L{AEInput.GestureList} that will be used to trigger a L{Task}.
    
    @param gesture: Gestures detected on an L{AEInput} device
    @type gesture: L{AEInput.Gesture}
    @param timestamp: Time at which the gesture was received on the device
    @type timestamp: float
    @param kwargs: Additional arguments to be passed to a L{Task.InputTask}
    @type kwargs: dictionary
    '''
    Base.AccessEngineEvent.__init__(self, focused=True, **kwargs)
    self.gesture = gesture
    self.timestamp = timestamp
    self.kwargs = kwargs
    
  def __str__(self):
    '''
    Returns a human readable representation of this event including its name,
    its gesture codes, and its device.
    
    @return: Information about this event
    @rtype: string
    '''
    name = Base.AccessEngineEvent.__str__(self)
    return '%s:\n\tgesture: %s\n\tdevice: %s' % \
           (name, self.gesture, self.gesture.getDevice().getName())

  def execute(self, tier_manager, **kwargs):
    '''
    Contacts the L{TierManager} and asks it to manage this event as a gesture.
    
    @param tier_manager: TierManager that will handle the event
    @type tier_manager: L{TierManager}
    @param kwargs: Packed references to other managers not of interest here
    @type kwargs: dictionary
    @return: True to indicate the event executed properly
    @rtype: boolean
    '''
    tier_manager.manageGesture(self)
    return True
  
  def getTaskKey(self):
    '''
    Gets the L{AEInput.GestureList} that triggered this event. This 
    information is used to locate the L{Task} that should handle this event.
    
    @return: Gesture seen on an L{AEInput} device
    @rtype: L{AEInput.GestureList}
    '''
    g = self.gesture
    return AEInput.GestureList(g.getDevice(), gestures=[g])
  
  def getTimestamp(self):
    '''
    Gets the timestamp for when the event occurred. This timestamp is useful
    for connecting input events to changes on the desktop. For instance, the
    timestamp for keyboard input is needed when key presses open a new dialog
    on some platforms such that the window manager can activate the dialog 
    once it appears.
    
    @return: Gesture timestamp
    @rtype: float
    '''
    return self.timestamp
  
  def getDataForTask(self):
    '''
    Fetches data out of this L{InputGesture} for use by a
    L{Task.InputTask}.
    
    @return: Dictionary of parameters to be passed to a L{Task.InputTask}
      as follows:
        - timestamp: The time when the input gesture occurred. The scale and
          absolute zero is device dependent.
        - any additional data in the L{kwargs} instance variable        
    @rtype: dictionary
    '''
    d = {}
    d.update(self.kwargs)
    d['timestamp'] = self.timestamp
    d['gesture'] = self.gesture
    return d
