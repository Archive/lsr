'''
Defines an L{AEEvent} indicating that the selector has moved.

@author: Pete Brunet
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''

import Base, AEConstants
from AEConstants import LAYER_FOCUS

class SelectorChange(Base.AccessEngineEvent):
  '''
  Event that fires when the selector moves.
  
  This class registers its name and whether it should be monitored by default in
  an L{AEMonitor} using the L{Base.registerEventType} function
  when this module is first imported. The L{AEMonitor} can use this
  information to build its menus.
  
  @ivar text: Accessible text associated with the event, typically the item 
    text or the selected text
  @type text: string
  '''
  Base.registerEventType('SelectorChange', True)
  def __init__(self, por, text, kind=AEConstants.EVENT_ACTIVE_ITEM_SELECT, 
               **kwargs):
    '''    
    Calls the base class (which set the event priority) and then stores the
    item to be passed along to the Tier as part of the event.
    
    @param por: Point-of-regard
    @type por: L{POR}
    @param text: Accessible text associated with the event, typically the 
      item text or the selected text
    @type text: string
    @param kind: Kind of selection event
    @type kind: integer
    '''
    Base.AccessEngineEvent.__init__(self, **kwargs)
    self.text = text
    self.por = por
    self.kind = kind
    
  def __str__(self):
    '''
    Returns a human readable representation of this event including its name,
    its L{POR}, and its item text.
    
    @return: Information about this event
    @rtype: string
    '''
    name = Base.AccessEngineEvent.__str__(self)
    if self.kind == AEConstants.EVENT_ACTIVE_ITEM_SELECT:
      action = 'active'
    elif self.kind == AEConstants.EVENT_ADD_ITEM_SELECT:
      action = 'add'
    elif self.kind == AEConstants.EVENT_REMOVE_ITEM_SELECT:
      action = 'remove'
    else:
      action = 'text'
    return '%s:\n\tPOR: %s\n\titem text: %s\n\taction: %s' % \
      (name, self.por, self.text, action)
  
  def execute(self, tier_manager, view_manager, **kwargs):
    '''
    Contacts the L{TierManager} so it can manage the selector change event.
    
    @param tier_manager: TierManager that will handle the event
    @type tier_manager: L{TierManager}
    @param kwargs: Packed references to other managers not of interest here
    @type kwargs: dictionary
    @return: True if there is an active view, False if not to delay execution 
      of this event until there is
    @rtype: boolean
    '''
    if view_manager.getAEView() is None and self.layer == LAYER_FOCUS:
      return False
    else:
      tier_manager.manageEvent(self)
      return True

  def getFocusPOR(self):
    '''
    Returns the L{POR} for active item events. Used by L{Tier} to maintain a 
    focus L{POR}.
    
    @return: Point-of-regard for this focus event
    @rtype: L{POR}
    @raise AttributeError: When the event does not represent a change in the
      focus
    '''
    if (self.layer != LAYER_FOCUS or
        self.kind != AEConstants.EVENT_ACTIVE_ITEM_SELECT):
      raise AttributeError
    return self.por

  def getDataForTask(self):
    '''
    Fetches data out of this L{SelectorChange} for use by a
    L{Task.SelectorTask}.
    
    @return: Dictionary of parameters to be passed to a
      L{Task.SelectorTask} as follows:
        - por: The L{POR} pointing at the item at which selection changed or
               the current caret location for a text selection change
        - text: The text now selected or unselected
        - kind: The kind of selection change
    @rtype: dictionary
    '''
    return {'por':self.getPOR(), 'text':self.text, 'kind':self.kind}
