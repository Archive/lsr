'''
Defines an L{AEEvent} indicating that the focus has changed.

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''

import Base
from AEConstants import LAYER_FOCUS

class FocusChange(Base.AccessEngineEvent):
  '''
  Event that fires when the focused accessible changes.
  
  This class registers its name and whether it should be monitored by default in
  an L{AEMonitor} using the L{Base.registerEventType} function
  when this module is first imported. The L{AEMonitor} can use this
  information to build its menus.
  
  @ivar gained: Type of focus change: True if gained, False if lost
  @type gained: boolean
  '''
  Base.registerEventType('FocusChange', True)
  
  def __init__(self, por, gained, **kwargs):
    '''
    Stores the L{POR} for the focused object and the type of focus event.
    
    @param por: Point-of-regard from the accessible that just got focus
    @type por: L{POR}
    @param gained: Was focused gained or lost?
    @type gained: boolean
    '''
    Base.AccessEngineEvent.__init__(self, **kwargs)
    self.por = por
    self.gained = gained
    
  def __str__(self):
    '''
    Returns a human readable representation of this event including its name,
    its L{POR}, and its type.
    
    @return: Information about this event
    @rtype: string
    '''
    name = Base.AccessEngineEvent.__str__(self)
    if self.gained:
      action = 'gained'
    else:
      action = 'lost'
    return '%s:\n\tPOR: %s\n\taction: %s' % (name, self.por, action)
    
  def getFocusPOR(self):
    '''
    Returns the L{POR} for this focus event. Used by L{Tier} to maintain a 
    focus L{POR}.
    
    @return: Point-of-regard for this focus event
    @rtype: L{POR}
    '''
    return self.por
  
  def getDataForTask(self):
    '''
    Fetches data out of this L{FocusChange} for use by a
    L{Task.FocusTask}.
    
    @return: Dictionary of parameters to be passed to a L{Task.FocusTask}
      as follows:
        - por:  The L{POR} of the accessible in which the focus change occured
        - type: Whether focus was gained (True) or lost (False)
    @rtype: dictionary
    '''
    return {'por' : self.getPOR(), 'gained' : self.gained}
    
  def execute(self, tier_manager, view_manager, **kwargs):
    '''
    Contacts the L{TierManager} so it can manage the focus change event.
    
    @param tier_manager: TierManager that will handle the event
    @type tier_manager: L{TierManager}
    @param kwargs: Packed references to other managers not of interest here
    @type kwargs: dictionary
    @return: True if there is an active view, False if not to delay execution 
      of this event until there is
    @rtype: boolean
    '''
    if view_manager.getAEView() is None and self.layer == LAYER_FOCUS:
      return False
    else:
      tier_manager.manageEvent(self)
      return True
