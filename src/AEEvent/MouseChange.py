'''
Defines an L{AEEvent} indicating that a mouse event occurred.

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
import Base
import AEConstants

class MouseChange(Base.AccessEngineEvent):
  '''
  Event that fires when the mouse cursor moves or a button is pressed.
  
  This class registers its name and whether it should be monitored by default in
  an L{AEMonitor} using the L{Base.registerEventType} function
  when this module is first imported. The L{AEMonitor} can use this
  information to build its menus.

  @ivar kind: Kind of mouse event
  @type kind: integer
  @ivar pos: Absolute position of the mouse pointer
  @type pos: 2-tuple of integer
  @ivar button: Number of the button pressed
  @type button: integer
  ''' 
  Base.registerEventType('MouseChange', False)
  
  def __init__(self, kind, pos=None, button=None, **kwargs):
    '''
    Calls the base class and stores the kind and position or button number.
   
    @param kind: Kind of mouse event
    @type kind: integer
    @param pos: Absolute position of the mouse pointer
    @type pos: 2-tuple of integer
    @param button: Number of the button pressed or released
    @type button: integer
    '''
    Base.AccessEngineEvent.__init__(self, focused=True, **kwargs)
    self.kind = kind
    self.pos = pos
    self.button = button
    
  def __str__(self):
    '''
    Returns a human readable representation of this event including its name,
    its kind, and its absolute position.
    
    @return: Information about this event
    @rtype: string
    '''
    name = Base.AccessEngineEvent.__str__(self)
    if self.kind == AEConstants.MOVE:
      action = 'move'
      return '%s:\n\taction: %s\n\tposition: %s' % (name, action, self.pos)
    else:
      if self.kind == AEConstants.PRESS:
        action = 'press'
        return '%s:\n\taction: %s\n\tbutton: %s' % (name, action, self.button)
      else:
        action = 'release'
        return '%s:\n\taction: %s\n\tbutton: %s' % (name, action, self.button)

  def execute(self, tier_manager, **kwargs):
    '''
    Contacts the L{TierManager} so it can manage the mouse event.
    
    @param tier_manager: TierManager that will handle the event
    @type tier_manager: L{TierManager}
    @param kwargs: Packed references to other managers not of interest here
    @type kwargs: dictionary
    @return: Always True to indicate the event executed properly
    @rtype: boolean
    '''
    tier_manager.manageEvent(self)
    return True

  def getDataForTask(self):
    '''    
    Fetches data out of this L{MouseChange} for use by a L{Task.MouseTask}.
    
    @return: Dictionary of parameters to be passed to a L{Task.MouseTask} as 
      follows:
        - kind: The kind of mouse event
        - pos: The new absolute position of the cursor
        - button: The number of the button pressed or released
    @rtype: dictionary
    '''
    return {'kind':self.kind, 'pos':self.pos, 'button':self.button}