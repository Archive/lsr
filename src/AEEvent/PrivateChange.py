'''
Defines an L{AEEvent} encapsulating arbitrary private data intended to be
consumed by the L{TierManager} or L{Tier}, but not passed on to L{Perk}s or
L{Task}s.

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''

import Base

class PrivateChange(Base.AccessEngineEvent):
  '''
  Event that fires when some change occurs.

  @ivar kind: Kind of chage, one of class variables in this class
  @type kind: integer
  @ivar kwargs: Aribitrary data to be provided with the event
  @type kwargs: dictionary
  '''
  KEY_PRESS = 0
  
  def __init__(self, kind, **kwargs):
    '''
    Calls the base class and stores the kind and data.
   
    @param kind: Kind of chage, one of class variables in this class
    @type kind: integer
    @param kwargs: Aribitrary data to be provided with the event
    @type kwargs: dictionary
    '''
    Base.AccessEngineEvent.__init__(self, focused=True, **kwargs)
    self.kind = kind
    self.kwargs = kwargs

  def execute(self, tier_manager, **kwargs):
    '''
    Contacts the L{TierManager} so it can manage the private event.
    
    @param tier_manager: TierManager that will handle the event
    @type tier_manager: L{TierManager}
    @param kwargs: Packed references to other managers not of interest here
    @type kwargs: dictionary
    @return: Always True to indicate the event executed properly
    @rtype: boolean
    '''
    tier_manager.managePrivate(self)
    return True

  def getDataForTask(self):
    '''    
    Fetches data out of this L{PrivateChange} for use by the L{TierManager}
    or a L{Tier}.
    
    @return: Dictionary of parameters to be passed to a L{Task.MouseTask} as 
      follows:
        - kind: The kind of event
        - any additional data in the L{kwargs} instance variable
    @rtype: dictionary
    '''
    d = {}
    d.update(self.kwargs)
    d['kind'] = self.kind
    return d
