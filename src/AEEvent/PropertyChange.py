'''
Defines an L{AEEvent} indicating that a simple property (text, numeric) of an 
accessible changed.

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''

import Base

class PropertyChange(Base.AccessEngineEvent):
  '''
  Event that fires when a text or numeric property changes.
  
  This class registers its name and whether it should be monitored by default in
  an L{AEMonitor} using the L{Base.registerEventType} function
  when this module is first imported. The L{AEMonitor} can use this
  information to build its menus.

  @ivar name: Name of the property that changed
  @type name: string
  @ivar value: The new value associated with the property
  @type value: string or integer
  '''
  Base.registerEventType('PropertyChange', False)
  def __init__(self, por, name, value, **kwargs):
    '''
    Stores the L{POR}, property name, and its new value.
    
    @param por: Point of regard to the accessible whose property changed
    @type por: L{POR}
    @param name: Name of the property that changed
    @type name: string
    @param value: The new value associated with the property
    @type value: string or integer
    '''
    Base.AccessEngineEvent.__init__(self, **kwargs)
    self.name = name
    self.value = value
    self.por = por
    
  def __str__(self):
    '''
    Returns a human readable representation of this event including its name,
    its L{POR}, its property name, and its new value.
    
    @return: Information about this event
    @rtype: string
    '''
    name = Base.AccessEngineEvent.__str__(self)
    return '%s:\n\tPOR: %s\n\tproperty: %s\n\tvalue: %s' % \
           (name, self.por, self.name, self.value)
  
  def execute(self, tier_manager, **kwargs):
    '''
    Contacts the L{TierManager} so it can manage the property change event.
    
    @param tier_manager: TierManager that will handle the event
    @type tier_manager: L{TierManager}
    @param kwargs: Packed references to other managers not of interest here
    @type kwargs: dictionary
    @return: Always True to indicate the event executed properly
    @rtype: boolean
    '''
    tier_manager.manageEvent(self)
    return True

  def getDataForTask(self):
    '''
    Fetches data out of this L{PropertyChange} for use by a
    L{Task.PropertyTask}.
    
    @return: Dictionary of parameters to be passed to a
      L{Task.PropertyTask} as follows:
        - por:  The L{POR} of the accessible whose property changed
        - name: The text name of the property that changed
        - value: The new text value of the property
    @rtype: dictionary
    '''
    return {'por':self.getPOR(), 'name':self.name, 'value':self.value}