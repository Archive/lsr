'''
Defines an L{AEEvent} indicating that the selector has moved.

@author: Pete Brunet
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''

import Base

class ScreenChange(Base.AccessEngineEvent):
  '''
  Event that fires when some aspect of screen bounds or visible data changes.
  
  This class registers its name and whether it should be monitored by default in
  an L{AEMonitor} using the L{Base.registerEventType} function
  when this module is first imported. The L{AEMonitor} can use this
  information to build its menus.
  
  @ivar kind: One of the integer class variables in this class
  @type kind: integer
  '''
  Base.registerEventType('ScreenChange', False)
  def __init__(self, por, kind, **kwargs):
    '''
    Calls the base class and stores L{POR} and type of event.
    
    @param por: Point of regard to an object that changed on screen
    @type por: L{POR}
    @param kind: Kind of screen change event
    @type kind: integer
    '''
    Base.AccessEngineEvent.__init__(self, **kwargs)
    self.por = por
    self.kind = kind
    
  def __str__(self):
    '''
    Returns a human readable representation of this event including its name,
    its L{POR}, and its kind.
    
    @return: Information about this event
    @rtype: string
    '''
    name = Base.AccessEngineEvent.__str__(self)
    if self.kind == AEConstants.EVENT_OBJECT_BOUNDS:
      action = 'object bounds'
    elif self.kind == AEConstants.EVENT_TEXT_BOUNDS:
      action = 'text bounds'
    else:
      action = 'visible data'
    return '%s:\n\tPOR: %s\n\taction: %s' % \
      (name, self.por, action)
  
  def execute(self, tier_manager, **kwargs):
    '''
    Contacts the L{TierManager} so it can manage the screen change event.
    
    @param tier_manager: TierManager that will handle the event
    @type tier_manager: L{TierManager}
    @param kwargs: Packed references to other managers not of interest here
    @type kwargs: dictionary
    @return: Always True to indicate the event executed properly
    @rtype: boolean
    '''
    tier_manager.manageEvent(self)
    return True

  def getDataForTask(self):
    '''
    Fetches data out of this L{ScreenChange} for use by a L{Task.ScreenTask}.
    
    @return: Dictionary of parameters to be passed to a 
      L{Task.ScreenTask} as follows:
        - por: The L{POR} pointing at the item at which selection changed or
               the current caret location for a text selection change
        - kind: The kind of screen change
    @rtype: dictionary
    '''
    return {'por' : self.getPOR(), 'kind' : self.kind}