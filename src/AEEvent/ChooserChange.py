'''
Defines an L{AEEvent} indicating an event of interest occurred in an 
L{AEChooser}.

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''

import Base

class ChooserChange(Base.AccessEngineEvent):
  '''  
  Event that fires when a L{AEChooser} indicates important input from a user.
  
  This class registers its name and whether it should be monitored by default
  in an L{AEMonitor} using the L{Base.registerEventType} function when
  this module is first imported. The L{AEMonitor} can use this information to
  build its menus.
  
  @ivar aid: Unique identifier for the application L{Tier} with which the 
    L{AEChooser} that fired this event is associated
  @type aid: opaque
  @ivar chooser: L{AEChooser} that fired this event
  @type chooser: L{AEChooser}
  @ivar kind: Kind of event, one of OK, CANCEL, or APPLY from L{AEChooser}
  @type kind: integer
  @ivar kwargs: Aribitrary data to be passed to the handler of this event
  @type kwargs: dictionary
  '''
  Base.registerEventType('ChooserChange', False)
  def __init__(self, aid, chooser, kind, **kwargs):
    '''
    Stores important references.
    
    @param aid: Unique identifier for the application L{Tier} with which the 
      L{AEChooser} that fired this event is associated
    @type aid: opaque
    @param chooser: L{AEChooser} that fired this event
    @type chooser: L{AEChooser}
    @param kind: Kind of event, one of OK, CANCEL, or APPLY from L{AEChooser}
    @type kind: integer
    '''
    Base.AccessEngineEvent.__init__(self, focused=True, **kwargs)
    self.aid = aid
    self.kwargs = kwargs
    self.kind = kind
    self.chooser = chooser
    
  def __str__(self):
    '''
    Returns a human readable representation of this event including its name,
    action, and chooser.
    
    @return: Information about this event
    @rtype: string
    '''
    name = Base.AccessEngineEvent.__str__(self)
    if self.kind == AEConstants.CHOOSER_OK:
      action = 'ok'
    elif self.kind == AEConstants.CHOOSER_CANCEL:
      action = 'cancel'
    elif self.kind == AEConstants.CHOOSER_APPLY:
      action = 'apply'
    else:
      action = self.kind
    return '%s:\n\tchooser: %s\n\taction: %s' % \
           (name, self.chooser.getName(), action)

  def execute(self, tier_manager, **kwargs):
    '''
    Contacts the L{TierManager} and asks it to manage this chooser event.
    
    @param tier_manager: TierManager that will handle the event
    @type tier_manager: L{TierManager}
    @param kwargs: Packed references to other managers not of interest here
    @type kwargs: dictionary
    @return: True to indicate the event executed properly
    @rtype: boolean
    '''
    tier_manager.manageChooser(self)
    return True
  
  def getTaskKey(self):
    '''    
    Gets the L{AEChooser} ID that triggered this event. This information is 
    used to locate the L{Task} that should handle this event.
    
    @return: ID of the chooser that fired the event
    @rtype: integer
    '''
    return id(self.chooser)
  
  def getAppID(self):
    '''
    @return: Unique application ID identifying the top most container for the
      source of this event (i.e. the application)
    @rtype: opaque object
    '''
    return self.aid
  
  def getDataForTask(self):
    '''
    Fetches data out of this L{ChooserChange} for use by a L{Task.ChooserTask}.
    
    @return: Dictionary of parameters to be passed to a L{Task.ChooserTask} as 
      follows:
        - chooser: The chooser that fired the event
        - kind: The kind of event
        - any addition data in the L{kwargs} instance variable
    @rtype: dictionary
    '''
    self.kwargs['chooser'] = self.chooser
    self.kwargs['kind'] = self.kind
    return self.kwargs