'''
Defines an L{AEEvent} indicating that the caret has moved.

@author: Pete Brunet
@author: Larry Weiss
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''

import Base
from AEConstants import LAYER_FOCUS

class CaretChange(Base.AccessEngineEvent):
  '''
  Event that fires when the caret moves in an accessible.
  
  This class registers its name and whether it should be monitored by default
  in an L{AEMonitor} using the L{Base.registerEventType} function when this
  module is first imported. The L{AEMonitor} can use this information to build
  its menus.
  
  @ivar text: The text inserted, deleted or the line of the caret
  @type text: string
  @ivar text_offset: The offset of the inserted/deleted text or the line 
    offset when movement only
  @type text_offset: integer
  @ivar added: True when text added, False when text deleted, and None 
    (the default) when event is for caret movement only
  @type added: boolean
  '''
  Base.registerEventType('CaretChange', False)
  
  def __init__(self, por, text, text_offset, added=None, **kwargs):
    '''
    Calls the base class (which set the event priority) and then stores the 
    text and offset to be passed along to the Tier as part of the event.
    
    @param por: Point of regard
    @type por: L{POR}
    @param text: The text inserted, deleted or the line of the caret
    @type text: string
    @param text_offset: The offset of the inserted/deleted text or the line 
      offset when movement only
    @type text_offset: integer
    @param added: True when text added, False when text deleted, and None 
      (the default) when event is for caret movement only
    @type added: boolean
    '''
    Base.AccessEngineEvent.__init__(self, **kwargs)
    self.por = por
    self.text = text
    self.text_offset = text_offset
    self.added = added

  def __str__(self):
    '''
    Returns a human readable representation of this event including its name,
    its text, its text offset, and whether the text was added, removed, or 
    moved.
    
    @return: Information about this event
    @rtype: string
    '''
    name = Base.AccessEngineEvent.__str__(self)
    if self.added is None:
      action = 'moved'
    elif self.added:
      action = 'inserted'
    else:
      action = 'deleted'
    return '%s:\n\tPOR: %s\n\ttext: %s\n\toffset: %d\n\taction: %s' % \
           (name, self.por, self.text, self.text_offset, action)
  
  def execute(self, tier_manager, view_manager, **kwargs):
    '''
    Contacts the L{TierManager} so it can manage the caret change event.
    
    @param tier_manager: TierManager that will handle the event
    @type tier_manager: L{TierManager}
    @param kwargs: Packed references to other managers not of interest here
    @type kwargs: dictionary
    @return: True if there is an active view, False if not to delay execution 
      of this event until there is
    @rtype: boolean
    '''
    if view_manager.getAEView() is None and self.layer == LAYER_FOCUS:
      return False
    else:
      tier_manager.manageEvent(self)
      return True

  def getFocusPOR(self):
    '''
    Returns the L{POR} for caret movement events. Used by L{Tier} to maintain a
    focus L{POR}.
    
    @return: Point-of-regard for this focus event
    @rtype: L{POR}
    @raise AttributeError: When the event does not represent a change in the
      focus
    '''
    if (self.layer != LAYER_FOCUS or
        self.added is not None):
      raise AttributeError
    return self.por

  def getDataForTask(self):
    '''
    Fetches data out of this L{CaretChange} for use by a
    L{Task.CaretTask}.
    
    @return: Dictionary of parameters to be passed to a L{Task.CaretTask} as 
      follows:
        - por:  The L{POR} of the accessible in which the caret moved
        - text: The text inserted, deleted or the line of the caret
        - text_offset: The offset of the inserted/deleted text or the line 
            offset when movement only
        - added: Boolean that is True when the text was added
    @rtype: dictionary
    '''    
    return {'por':self.getPOR(), 'text':self.text,
            'text_offset':self.text_offset, 'added':self.added}
