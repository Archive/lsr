'''
Defines an L{AEEvent} indicating a timer event occurred.

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
import Base

class TimerAlert(Base.AccessEngineEvent):
  '''  
  Event that fires when a registered timer triggers a notification.
  
  This class registers its name and whether it should be monitored by default
  in an L{AEMonitor} using the L{Base.registerEventType} function when
  this module is first imported. The L{AEMonitor} can use this information to
  build its menus.

  @ivar aid: Unique identifier for the application L{Tier} with which the 
    timer that fired this event is associated
  @type aid: opaque
  @ivar tid: ID of the L{Task} that will handle the timer event
  @type tid: integer
  @ivar interval: Interval on which the timer is triggered
  @type interval: integer
  '''
  Base.registerEventType('TimerAlert', False)
  def __init__(self, aid, tid, interval, **kwargs):
    '''
    Stores important references.
    
    @param aid: Unique identifier for the application L{Tier} with which the 
    timer that fired this event is associated
    @type aid: opaque
    @param tid: ID of the L{Task} that will handle the timer event
    @type tid: integer
    @param interval: Interval on which the timer is triggered
    @type interval: integer
    '''
    Base.AccessEngineEvent.__init__(self, focused=False, **kwargs)
    self.aid = aid
    self.tid = tid
    self.interval = interval
    
  def __str__(self):
    '''
    Returns a human readable representation of this event including its name 
    and interval.
    
    @return: Information about this event
    @rtype: string
    '''
    name = Base.AccessEngineEvent.__str__(self)
    return '%s:\n\tinterval: %d' % (name, self.interval)

  def execute(self, tier_manager, **kwargs):
    '''
    Contacts the L{TierManager} and asks it to manage this chooser event.
    
    @param tier_manager: TierManager that will handle the event
    @type tier_manager: L{TierManager}
    @param kwargs: Packed references to other managers not of interest here
    @type kwargs: dictionary
    @return: True to indicate the event executed properly
    @rtype: boolean
    '''
    tier_manager.manageTimer(self)
    return True
  
  def getAppID(self):
    '''
    @return: Unique application ID identifying the top most container for the
      source of this event (i.e. the application)
    @rtype: opaque object
    '''
    return self.aid
  
  def getTaskKey(self):
    '''
    Gets the ID of the L{Task} that should handle the timer event. This 
    information is used to locate the L{Task} that should handle this event.
    
    @return: ID of the L{Task} that will handle the event
    @rtype: integer
    '''
    return self.tid
  
  def getDataForTask(self):
    '''
    Fetches data out of this L{TimerAlert} for use by a L{Task.TimerTask}.
    
    @return: Dictionary of parameters to be passed to a L{Task.TimerTask} as 
      follows:
        - interval: The interval on which the timer fires
    @rtype: dictionary
    '''
    return {'interval' : self.interval}