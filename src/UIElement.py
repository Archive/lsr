'''
Defines the base class for all user interface elements (UIEs).

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
import UIRegistrar

class UIE(object):
  '''
  Most base class for all user interface elements. Subclasses of this class
  include L{AEOutput}, L{AEInput}, L{AEMonitor}, L{AEChooser}, L{Perk}, and
  L{Task}. Provides methods for getting metadata about a loaded instance of a
  UIE including its class name, human readable name, description, and path on
  disk (when applicable).
  '''
  def getClassName(self):
    '''
    @return: Name of this object's class
    @rtype: string
    '''
    return str(self.__class__.__name__)  
  
  def getName(self):
    '''
    @return: Human readable name of this object. Defaults to its class name.
    @rtype: string
    '''
    return self.getClassName()
  
  def getPath(self):
    '''
    @return: Absolute path location of the module containing the implementor
      of this interface
    @rtype: string
    '''
    return UIRegistrar.getPath(self.getClassName())
  
  def getDescription(self):
    '''
    @return: Description of this component. Defaults to the first paragraph 
      (all lines up to the first blank line) in its class docstring.
    @rtype: string
    '''
    desc = []
    doc = self.__class__.__doc__
    if doc is None:
      return ''
    for i, line in enumerate(doc.split('\n')):
      line = line.strip()
      if i != 0 and not line:
        break
      elif i != 0:
        desc.append(line)
    return ' '.join(desc)