'''
Provides basic magnification services.

@author: Eitan Isaacson
@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2006 IBM Corp
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
# import useful modules for Perks
import Perk, Task, AEConstants
from i18n import bind, _
from AEConstants import CMD_GOTO, CMD_GET_ROI
from math import cos, sin, atan, pi, sqrt

# metadata describing this Perk
__uie__ = dict(kind='perk', tier=None, all_tiers=True)

class ZoomMove(object):
  '''
  Class that deals with seamless panning and repositioning of the magnifier.

  @cvar pan_rate: Duration in milliseconds between "frames"
  @type pan_rate: int
  @cvar end_coord: Coordinate that we are trying to get to
  @type end_coord: tuple
  @cvar last_track_type: The last type of tracking we did
  @type last_track_type: int
  @ivar accel: Acceleration in px/sec**2
  @type accel: int
  @ivar velocity: Velocity in px/sec
  @type velocity: int
  @ivar smooth_pan: Allow smooth panning
  @type smooth_pan: boolean
  '''
  pan_rate = 40
  smooth_pan = None
  accel = None
  velocity = None
  _curr_velocity = None
  _curr_coord = None
  _vect_length = None
  _end_coord = None
  
  def goTo(self, end_coord, start_coord=None, smooth=False):
    '''
    Main public method for putting the magnifier on a coordinate.

    @param end_coord: Coordinate we want to reach.
    @type end_coord: tuple
    @param start_coord: Coordinate we are starting at, if none is given, 
      there will be no panning.
    @type start_coord: tuple
    @param smooth: Use smooth panning?
    @type smooth: boolean
    '''
    if start_coord and (self.smooth_pan or smooth):
      self._panTo(end_coord, start_coord)
    else:
      self._hopTo(end_coord)

  def _hopTo(self, end_coord):
    '''
    Stop panning and hop to given coordinate.

    @param end_coord: Coordinate we want to reach.
    @type end_coord: tuple
    '''
    self.stopPan()
    self.goToPos(end_coord)

  def _panTo(self, end_coord, start_coord):
    '''
    Pan to given coordinate.

    @param end_coord: Coordinate we want to reach.
    @type end_coord: tuple
    @param start_coord: Coordinate we are starting at.
    @type start_coord: tuple
    '''
    if end_coord == self._end_coord:
      return
        
    self.stopPan()
        
    self._curr_coord = float(start_coord[0]), float(start_coord[1])
    
    self._end_coord = end_coord
    
    x = self._end_coord[0] - self._curr_coord[0]
    y = self._end_coord[1] - self._curr_coord[1]

    self._vect_length = sqrt(x**2 + y**2)

    if (x, y) == (0, 0):
      return
    elif y == 0 and x > 0:
      angle = pi/2
    elif y == 0 and x < 0:
      angle = -pi/2
    elif y < 0:
      angle = atan (float(x)/float(y)) + pi
    else:
      angle = atan (float(x)/float(y))

    self._curr_velocity = 0
      
    self._x_step = sin(angle)
    self._y_step = cos(angle)
            
    self.startPan()

  def _panStep (self):
    '''
    Increment X and Y coordinates in a certain vector.
    Accelerate, deccelerate accordingly.
    '''
    # This is neccesarry to avoid jerks when tracking is enabled both with
    # mouse and anything else.
    if not self.smooth_pan:
      self.goToPos (self._end_coord)
      return False

    # If accel is set to zero it is disabled.
    if self.accel == 0:
      self._curr_velocity = self.velocity
      # currentvelocity**2/(2*acceleration) = The distance needed to get
      # to a complete stop at the current velocity and deceleration.
    elif (self._curr_velocity**2/(2*self.accel)) >= self._vect_length:
      self._curr_velocity -= self.accel
      # If the current velocity is smaller than the final velocity 
      # then accelerate.
    elif self._curr_velocity < self.velocity:
      self._curr_velocity += self.accel

    x = self._curr_velocity*self._x_step
    y = self._curr_velocity*self._y_step
    
    self._curr_coord = (x + self._curr_coord[0], 
                        y + self._curr_coord[1])
      
    progress = sqrt(x**2 + y**2)
    self._vect_length -= progress

    if self._vect_length < 0:
      self.goToPos (self._end_coord)
      return False
    else:
      self.goToPos (tuple(map(int,map(round,self._curr_coord))))
      return True

  def stopPan(self):
    '''
    Stop current pan motion.
    '''
    raise NotImplementedError

  def startPan(self):
    '''
    Start a timeout and execute _panstep
    '''
    raise NotImplementedError
    

  def goToPos(self, pos):
    '''
    Actual method that translates the zoomer, to be implemented by perk.
    '''
    raise NotImplementedError

class BasicMagPerkState(Perk.PerkState):
  '''  
  Defines a single set of state variables for all instances of the
  L{BasicMagPerk} and their L{Task}s. The variables are configuration settings
  that should be respected in all instances of L{BasicMagPerk}.

  MouseTrack (bool): Track cursor
  FocusTrack (bool): Track focus
  CaretTrack (bool): Track caret
  SmoothPan (bool): Smooth pan
  CaretPan (bool): Smooth caret panning
  PanAccel (int): Pan acceleration
  PanVelocity (int): Pan velocity
  '''
  def init(self):
    self.newBool('MouseTrack', True, _('Track cursor'),
                 _('Magnifier is to track mouse movemenself.'))
    self.newBool('FocusTrack', True, _('Track focus'),
                 _('Magnifier is to track focused area'))
    self.newBool('CaretTrack', True, _('Track caret'),
                 _('Magnifier is to track text caret'))
    self.newBool('SmoothPan', False, _('Smooth panning'),
                 _('Magnifier will pan to next selection'))
    self.newBool('CaretPan', False, _('Smooth caret panning'),
                 _('Magnifier will smoothly pan on caret movements, by '
                   'default it only pans on new lines.'))
    self.newRange('PanAccel', 40, _('Pan Acceleration'), 1, 100, 0,
       _('Acceleration rate of magnifier pan motion (pixels per second '
         'squared).'))
    self.newRange('PanVelocity', 600, _('Pan Velocity'), 1, 1000, 0,
       _('Velocity of magnifier pan motion (pixels per second).'))
    self.newBool('MagReview', False, _('Track review keys?'), 
           _('When set, reviewing is tracked by the magnifier.  '
           'Otherwise, magnifier is not updated when reviewing.'))
    
  def getGroups(self):
    g = self.newGroup()
    t = g.newGroup(_('Tracking'))
    t.extend(['MouseTrack', 
              'FocusTrack', 
              'CaretTrack'])
    p = g.newGroup(_('Kinematics'))
    p.extend(['SmoothPan',
              'CaretPan',
              'PanAccel', 
              'PanVelocity'])
    r = g.newGroup(_('Reviewing'))
    r.append('MagReview')
    return g

class BasicMagPerk(Perk.Perk, ZoomMove):
  '''
  A perk to provide magnification for a user.
  '''
  STATE=BasicMagPerkState
  FUDGE=5

  def init(self):
    self.mousepos = (0, 0)
    self.parent_por_focus = None
    self.parent_por_select = None
    
    self.setPerkIdealOutput('magnifier')

    self.registerTask(HandleMouseChange('mag mouse'))
    self.registerTask(HandleSelectorChange('mag selector'))
    self.registerTask(HandleFocusChange('mag focus'))
    self.registerTask(HandleCaretChange('mag caret'))
    self.registerTask(HandleReviewChange('mag review'))
    self.registerTask(ChangeZoomLevel('mag increase zoom'))
    self.registerTask(ChangeZoomLevel('mag decrease zoom'))

    self.accel = self.perk_state.PanAccel*(self.pan_rate/1000.0)
    self.velocity = self.perk_state.PanVelocity*(self.pan_rate/1000.0)
    self.smooth_pan = self.perk_state.SmoothPan

    for setting in self.state:
      setting.addObserver(self._updateSetting)

    self.focused_por = self.task_por
    self.last_caret_offset = 0
    
    # link to review mode tasks
    self.chainTask('mag review', AEConstants.CHAIN_AFTER, 
                   'review previous item')
    self.chainTask('mag review', AEConstants.CHAIN_AFTER, 
                   'review current item')
    self.chainTask('mag review', AEConstants.CHAIN_AFTER, 
                   'review next item')
    self.chainTask('mag review', AEConstants.CHAIN_AFTER, 
                   'review previous word')
    self.chainTask('mag review', AEConstants.CHAIN_AFTER, 
                   'review current word')
    self.chainTask('mag review', AEConstants.CHAIN_AFTER, 
                   'review next word')
    self.chainTask('mag review', AEConstants.CHAIN_AFTER, 
                   'review previous char')
    self.chainTask('mag review', AEConstants.CHAIN_AFTER, 
                   'review current char')
    self.chainTask('mag review', AEConstants.CHAIN_AFTER, 
                   'review next char')
    self.chainTask('mag review', AEConstants.CHAIN_AFTER,
                   'pointer to por')
    
  def _updateSetting(self, state, setting):
    name = setting.name
    value = setting.value
    if name == 'PanAccel':
      self.accel = value*(self.pan_rate/1000.0)
    elif name == 'PanVelocity':
      self.velocity = value*(self.pan_rate/1000.0)
    elif name == 'SmoothPan':
      self.smooth_pan = value
      
  def getName(self):
    return _('Basic magnifier')
    
  def getDescription(self):
    return _('Manages basic screen magnification services. Requires a '
             'loaded magnifier device.')
  
  def startPan(self):
    '''
    Register a TimerTask for the pan animation
    '''
    self.registerTask(PanTimer('mag pan step', self.pan_rate))

  def stopPan(self):
    '''
    Unregister pan animation timer task
    '''
    task = self.getNamedTask('mag pan step')
    if task is not None:
      self.unregisterTask(task)
    
  def goToPos(self, pos):
    '''
    Send command to magnifier device to center zoomer on given coordinate.
    
    @param pos: x,y coordinate
    @type pos: 2-tuple of integer
    '''
    roi = self.getMagROI()
    if roi is None:
      return
    x1, y1, x2, y2 = roi
    w, h = x2 - x1, y2 - y1
    x1 = pos[0] - w/2
    y1 = pos[1] - h/2
    x2, y2 = x1 + w, y1 + h
    self.setMagGoto((x1, y1, x2, y2))
  
  def getPos(self):
    '''
    Get ROI from magnifier and return center coordinate
    
    @return: magnifier's focal point.
    @rtype: tuple
    '''
    roi = self.getMagROI()
    if roi is None:
      return (0, 0, 0, 0)
    else:
      return roi
  
  def getDesiredFocalPoint(self, por, track_mouse=True):
    '''
    Calculates the desired focal point for a specific POR.
    And retuns current focal point, and desired focal point.

    @param por: POR that's focal point needs to be determined.
    @type por: L{POR.POR}
    @param track_mouse: Take the current position of the mouse pointer into
      account so that the magnifier doesn't jerk?
    @type track_mouse: boolean
    '''
    # get the extents of the POR
    try:
      por_width, por_height = self.getAccVisualExtents(por)
    except TypeError:
      # In case the extents of the POR can't be determined, the mouse pointer
      # position is not taken into account when calculating the desired focal
      # point, since we can't evaluate if it is within the POR.
      track_mouse = False
    # get the position of the POR
    pos_x, pos_y = self.getAccPosition() 
    # get the last known mouse position
    m_x, m_y = self.perk.mousepos
    # get the recommended focal point for the current POR
    end_x, end_y = self.getAccVisualPoint(por)

    # calculate start_pos
    # get the left, top, right, and bottom coords of the magnifier zoom reg.
    roi_x1, roi_y1, roi_x2, roi_y2 = self.perk.getPos()
    # start position is the center point
    start_pos = ((roi_x1 + roi_x2)/2, (roi_y1 + roi_y2)/2)
    
    if (track_mouse and 
        (m_x >= pos_x-self.FUDGE) and (m_x <= (pos_x+por_width+self.FUDGE)) and
        (m_y >= pos_y-self.FUDGE) and (m_y <= (pos_y+por_height+self.FUDGE))):
      # if the mouse pointer is within the control and we want to consider that
      # make the end position of the jump the mouse pointer position so that
      # the magnifier doesn't jump to some other location in the control and
      # then jump back again; the user is likely a mouse user anyways
      end_x = m_x
      end_y = m_y

    return start_pos, (end_x, end_y)

class HandleFocusChange(Task.FocusTask):
  '''
  Moves the magnifier to coordinate of this focused element.
  '''
  def executeGained(self, por, layer, **kwargs):
    if not self.perk.perk_state.FocusTrack:
      return True
    
    if por != self.perk.parent_por_focus:
      start_pos, end_pos = self.perk.getDesiredFocalPoint(self.task_por)
      self.perk.goTo(end_pos, start_pos)
   
    self.perk.focused_por = self.task_por
    self.perk.parent_por_focus = self.getParentAcc()
        
    
class HandleCaretChange(Task.CaretTask):
  '''
  Moves the magnifier to coordinate of this caret
  '''
  def execute(self, por, **kwargs):
    if not self.perk_state.CaretTrack:
      return True
    
    # sanity check
    text_len = self.getAccTextLength()
    if (por.char_offset < 0 or 
        por.item_offset < 0 or
        text_len < (por.char_offset + por.item_offset)):
      return 

    start_pos, end_pos = self.perk.getDesiredFocalPoint(por)

    if (self.perk.focused_por == por or 
        por.item_offset != self.perk.last_caret_offset or 
        self.perk_state.CaretPan):
      self.perk.goTo(end_pos, start_pos)
    else:
      self.perk.goTo(end_pos)

    self.perk.last_caret_offset =  por.item_offset

class HandleSelectorChange(Task.SelectorTask):
  '''
  Moves the magnifier to coordinate of this selected element
  '''
  def execute(self, por, **kwargs):
    if not self.perk.perk_state.FocusTrack:
      return True

    if por != self.perk.parent_por_select:
      start_pos, end_pos = self.perk.getDesiredFocalPoint(self.task_por)
      self.perk.goTo(end_pos, start_pos)
    
    self.perk.parent_por_select = self.getParentAcc()
      
class HandleMouseChange(Task.MouseTask):
  '''
  Moves the magnifier to coordinate of mouse
  '''
  def executeMoved(self, pos, **kwargs):
    '''
    Execute on each mouse event, for now we only look at cursor movements.
    '''
    if not self.perk.perk_state.MouseTrack:
      return True
    self.perk.mousepos = pos
    self.perk.goTo(pos)
   
class PanTimer(Task.TimerTask):
  '''
  Execute on timed intervals. Used to animate pan magnifier movement.
  '''
  def execute(self, **kwargs):
    rv = self.perk._panStep()
    if not rv:
      self.unregisterTask(self)

class HandleReviewChange(Task.InputTask):
  '''  
  Synchronizes the magnified area to the text at the pointer when reviewing.
  '''
  def execute(self, **kwargs):
    if self.getPerkSettingVal('MagReview'):
      result = self.getTempVal('review')
      if result in (AEConstants.REVIEW_OK, AEConstants.REVIEW_WRAP):
        start_pos, end_pos = self.perk.getDesiredFocalPoint(self.task_por ,\
                                                            track_mouse=False)
        self.perk.goTo(end_pos, start_pos)

class ChangeZoomLevel(Task.InputTask):
  '''
  Increases or decreases zoom level.
  '''
  def execute(self, **kwargs):
    print self.getMagROI()
    s = self.getStyleSetting('Zoom')
    id = self.getIdentity()
    if id.find('increase') > -1:
      s.value += 10**(-s.precision)
    else:
      s.value -= 10**(-s.precision)
    print self.getMagROI()
