'''
Provides basic speech services and speech reports common to a screen reader.

@author: Peter Parente
@author: Pete Brunet
@author: Larry Weiss
@author: Brett Clippingdale
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
import Perk, Task, AEConstants, AEEvent
from POR import POR
import unicodedata
from i18n import _

# verbosity settings
SPEAK_NEVER = 0
SPEAK_ALWAYS = 1 
SPEAK_DIFF = 2
SPEAK_LINE = 3
SPEAK_ALL = 4

__uie__ = dict(kind='perk', all_tiers=True)

class BasicSpeechPerkState(Perk.PerkState):
  '''
  User configurable settings for this L{Perk}.

  WordEcho (bool): Echo words when typing?
  
  CharEcho (bool): Echo characters when typing?
  
  AutoLang (bool): Switch languages automatically?

  CharLimit (numeric): Set the threshold for text insertion synthesizing.
    If text exceeds this threshold, it will be summarized.
    
  RoleVerbosity (bool): Say role information always, only when it changes,
    or never?
    
  IndexVerbosity (enum): Say the index and size of a collection always, 
    only when it changes, or never?
  
  HeaderVerbosity (enum): Say headers always, when they change in a table, or
    never?
  
  TextVerbosity (enum): Say the line containing the caret or read all text
    starting with the line containing the caret?
    
  @ivar activewincnt: Used to track the number of active windows.  
  @type last_count: integer
  '''
  def init(self):
    self.newBool('WordEcho', False, _('Echo words?'), 
                 _('When set, entire words are spoken when editing text.'))
    self.newBool('CharEcho', True, _('Echo characters?'), 
                 _('When set, individual characters are spoken when editing '
                   'text.'))
    self.newBool('AutoLang', True, _('Switch languages automatically?'),
                 _('When set, the language of the speech synthesizer tries to '
                   'switch to the appropriate language and dialect '
                   'automatically based on the current locale.'))
    self.newNumeric('CharLimit', 5000, _('Summarize insertions longer than'), 
                    0, 10000, 0, _('Set the threshold (in characters) for text'
                    ' insertions. Any text that exceeds this limit will be '
                    ' summarized by its length.'))
    self.newEnum('RoleVerbosity', SPEAK_DIFF, _('Say role'),
                 {_('Always') : SPEAK_ALWAYS,
                  _('When different') : SPEAK_DIFF,
                  _('Never') : SPEAK_NEVER},
                 _('Always say widget roles, never say widget roles, or '
                   'only say widget roles when they differ from the last '
                   'report.'))
    self.newEnum('IndexVerbosity', SPEAK_ALWAYS, _('Say index'),
                 {_('Always') : SPEAK_ALWAYS,
                  _('When different') : SPEAK_DIFF,
                  _('Never') : SPEAK_NEVER},
                 _('Always say the index of an item and size of a collection, '
                   'never say either, or only say index and size when they '
                   'differ from the last report.'))
    self.newEnum('HeaderVerbosity', SPEAK_DIFF, _('Say table headers'),
                 {_('Always') : SPEAK_ALWAYS,
                  _('When different') : SPEAK_DIFF,
                  _('Never') : SPEAK_NEVER},
                 _('Always say table headers, never say table headers, '
                   'or only say the headers when they differ from the last '
                   'report.'))
    self.newEnum('TextVerbosity', SPEAK_LINE, _('Say text area'),
                 {_('All') : SPEAK_ALL,
                  _('Line') : SPEAK_LINE,
                  _('Nothing') : SPEAK_NEVER},
                 _('Read an entire multiline text area when it receives the '
                   'focus, read only the line containing the caret, or read '
                   'nothing.'))
    
    # instance variables that are part of state but not a user settings
    self.activewincnt = 0

  def getGroups(self):
    '''
    Gets configurable settings for this L{Perk}.
    
    @return: Group of all configurable settings
    @rtype: L{AEState.Setting.Group}
    '''
    g = self.newGroup()
    s = g.newGroup(_('Echo'))
    s.extend(['CharEcho', 'WordEcho'])
    s = g.newGroup(_('Verbosity'))
    s.extend(['RoleVerbosity', 'IndexVerbosity',  
              'HeaderVerbosity', 'TextVerbosity', 'CharLimit'])
    s = g.newGroup(_('Language'))
    s.extend(['AutoLang'])
    return g
  
class BasicSpeechPerk(Perk.Perk):
  '''
  Defines a default user interface that makes LSR act like a typical screen 
  reader.
  
  @ivar last_caret: Stores the previous caret L{POR}
  @type last_caret: L{POR}
  @ivar last_role: Stores the previous role to avoid duplicate announcements
  @type last_role: string
  @ivar last_level: Stores the previous level to avoid duplicate announcements
  @type last_level: integer
  @ivar last_container: Stores the previously announced container name
  @type last_container: string
  @ivar last_sel_len: Length of the selected text when the last selection event
    was received
  @type last_sel_len: integer
  @ivar last_row: Index of the last row activated
  @type last_row: integer
  @ivar last_col: Index of the last column activated
  @type last_col: integer
  @ivar last_count: Was count of items in a collection announced during last
    announcement?
  @type last_count: integer 
  '''
  # defines the class to use for state information in this Perk
  STATE = BasicSpeechPerkState
  
  def init(self):
    '''
    Registers L{Task}s to handle focus, view, caret, selector, state, and 
    property change events. Registers named L{Task}s that can be mapped to 
    input gestures.
    '''
    # set an audio device as the default output
    self.setPerkIdealOutput('audio')
    # register event handlers
    self.registerTask(HandleFocusChange('read focus'))
    self.registerTask(HandleViewChange('read view',all=True))
    self.registerTask(HandleCaretChange('read caret'))
    self.registerTask(HandleSelectorChange('read selector'))
    self.registerTask(HandleStateChange('read state'))
    self.registerTask(HandlePropertyChange('read property'))
    # register named tasks
    # these will be mapped to input gestures in other Perks
    self.registerTask(Stop('stop now'))
    self.registerTask(IncreaseRate('increase speech rate'))
    self.registerTask(DecreaseRate('decrease speech rate'))
    self.registerTask(ReadReviewItem('read review item'))
    self.registerTask(ReadReviewSkip('read review skip'))
    self.registerTask(ReadReviewWord('read review word'))
    self.registerTask(ReadReviewChar('read review char'))
    self.registerTask(ReadPointerToFocus('read pointer to por'))
    self.registerTask(WhereAmINow('where am i now'))
    self.registerTask(WhereAmIAncestors('where am i ancestors'))
    self.registerTask(ReadTop('read top'))
    self.registerTask(ReadTextColor('read text color'))
    self.registerTask(ReadTextAttributes('read text attributes'))
    self.registerTask(ReadDescription('read description'))
    self.registerTask(ReadMessage('read message'))
    self.registerTask(ReadNewLabel('read new label'))
    self.registerTask(ReadNewRole('read new role'))
    self.registerTask(ReadNewLevel('read new level'))
    self.registerTask(ReadNewHeaders('read new headers'))
    self.registerTask(ReadNewContainer('read new container'))
    self.registerTask(ReadItemIndex('read item index'))
    self.registerTask(ReadItemDetails('read item details'))
    self.registerTask(ReadAll('read all'))
    self.registerTask(SpellReviewWord('spell review word'))
    self.registerTask(PronounceReviewWord('pronounce review word'))
    self.registerTask(PronounceReviewChar('pronounce review char'))
    
    # cyclic tasks
    self.registerTask(Task.CyclicInputTask('cycle review char',
                                           'read review char',
                                           'pronounce review char'))
    self.registerTask(Task.CyclicInputTask('cycle review word',
                                           'read review word',
                                           'spell review word',
                                           'pronounce review word'))
    self.registerTask(Task.CyclicInputTask('cycle text attributes',
                                           'read text color',
                                           'read text attributes'))
    self.registerTask(Task.CyclicInputTask('cycle where am i',
                                           'where am i now',
                                           'where am i ancestors'))

    
    # link to review mode tasks
    self.chainTask('read review item', AEConstants.CHAIN_AFTER, 
                   'review previous item')
    self.chainTask('read review item', AEConstants.CHAIN_AFTER, 
                   'review current item')
    self.chainTask('read review item', AEConstants.CHAIN_AFTER, 
                   'review next item')
    
    self.chainTask('read review word', AEConstants.CHAIN_AFTER, 
                   'review previous word')
    self.chainTask('cycle review word', AEConstants.CHAIN_AFTER, 
                   'review current word')
    self.chainTask('read review word', AEConstants.CHAIN_AFTER, 
                   'review next word')
    
    self.chainTask('read review char', AEConstants.CHAIN_AFTER, 
                   'review previous char')
    self.chainTask('cycle review char', AEConstants.CHAIN_AFTER, 
                   'review current char')
    self.chainTask('read review char', AEConstants.CHAIN_AFTER, 
                   'review next char')
    
    self.chainTask('read pointer to por', AEConstants.CHAIN_AFTER,
                   'pointer to por')
    
    self.chainTask('read review skip', AEConstants.CHAIN_AFTER,
                   'review skip report')
    
    # stateful variables; OK to access from other Task classes in this module
    self.resetLasts()
  
  def resetLasts(self, container=''):
    '''
    Resets variables tracking the last container, caret position, tree level, 
    selection length, row/col offsets, and row/col count on view change.
    
    @param container: Last container name announced
    @type container: string
    '''
    self.last_container = container
    self.last_caret = POR(item_offset=0)
    self.last_role = None
    self.last_level = None
    self.last_sel_len = 0
    self.last_row = None
    self.last_col = None
    self.last_count = False
    
  def resetLastsOnFocus(self):
    '''
    Resets variables tracking some of the stateful information on a focus
    change. Does not reset the last role or last container announced.
    '''
    self.last_caret = POR(item_offset=0)
    self.last_count = False
    self.last_sel_len = 0
    self.last_row = None
    self.last_col = None
    self.last_level = None
    
  def resetLastsForNew(self):
    '''
    Resets variables tracking whether an item is new or not for read new item,
    read new role, read new level, read new label, etc.
    '''
    self.last_role = None
    self.last_count = False
    self.last_sel_len = 0
    self.last_row = None
    self.last_col = None
    self.last_level = None
    
  def getName(self):
    '''
    @return: Human readable name of this L{Perk}.
    @rtype: string
    '''
    return _('Basic speech')
  
  def getDescription(self):
    '''
    @return: Human readable description of this L{Perk}.
    @rtype: string
    '''
    return _('Manages basic screen reader speech output. Requires a loaded '
             'speech device.')
  
  def switchLanguages(self):
    '''
    Attempts to switch the active speech engine to the POSIX message language
    in use in this L{Tier}.
    '''
    if not self.getPerkSettingVal('AutoLang'):
      # do not switch language automatically if the user has disabled the
      # feature
      return
    # get the local string from the application
    l = self.getAccAppLocale()
    if l is None:
      # no locale information provided
      return
    # convert the string to IANA format for the speech devices
    tag = self.convertPOSIXToIANA(l)
    # ask the active device to find a language close to the one specified
    tag = self.getClosestLang(tag)
    if tag is not None:
      # set the tag as the active language for all voices on the device
      self.setStyleVal('Language', tag)

class Stop(Task.InputTask):
  '''
  Task to stop speech immediately, ignoring the value of the Stopping setting.
  '''
  def execute(self, **kwargs):
    self.stopAll()
    task = self.getNamedTask('continuous read')
    if task is not None:
      self.unregisterTask(task)

class IncreaseRate(Task.InputTask):
  '''
  Increase the speech rate. The maximum rate is announced when reached.

  @see: L{DecreaseRate}
  '''
  def execute(self, **kwargs):
    self.stopNow()
    rate = self.getStyleSetting('Rate')
    # make up a step size
    step  = (abs(rate.max)+abs(rate.min))/30
    if rate.value+step < rate.max:
      rate.value += step
      self.sayInfo(text=rate.value, template=('rate %d'))
    else:
      self.sayInfo(text=_('maximum rate'))

class DecreaseRate(Task.InputTask):
  '''
  Decrease the speech rate. The minimum rate is announced when reached.

  @see: L{IncreaseRate}
  '''
  def execute(self, **kwargs):
    self.stopNow()
    rate = self.getStyleSetting('Rate')
    # make up a step size
    step  = (abs(rate.max)+abs(rate.min))/30
    if rate.value-step > rate.min:
      rate.value -= step
      self.sayInfo(text=rate.value, template=_('rate %d'))
    else:
      self.sayInfo(text=_('minimum rate'))
      
class ReadReviewItem(Task.InputTask):
  '''  
  Reads the details of the item at the pointer or speaks an informational
  message about why the item cannot be read.
  '''
  def execute(self, **kwargs):
    result = self.getTempVal('review')
    if not self.getTempVal('has skipped'):
      # only stop if we haven't already skipped another item, otherwise we 
      # might still be speaking
      self.mayStop()
    if result == AEConstants.REVIEW_OK:
      self.doTask('read item details')
    elif result == AEConstants.REVIEW_NO_NEXT_ITEM:
      self.sayInfo(text=_('last item'))
    elif result == AEConstants.REVIEW_NO_PREV_ITEM:
      self.sayInfo(text=_('first item'))
      
class ReadReviewSkip(Task.InputTask):
  '''  
  Reads a summary of an item skipped during review.
  '''
  def execute(self, **kwargs):
    if not self.getTempVal('has skipped'):
      # be sure to stop speech if we haven't skipped other items yet
      self.stopNow(reset=False)
    self.doTask('read new role')
    
class ReadReviewWord(Task.InputTask):
  '''  
  Reads the word at the pointer or speaks an informational message about why
  the word cannot be read.
  '''
  def execute(self, text=None, **kwargs):
    '''
    @param text: Text to say, or None to use the pointer L{POR} by default
    @type text: string
    '''
    result = self.getTempVal('review')
    self.stopNow()
    # allow None case to pass as normal so this task can be reused to read 
    # words in cases other than reviewing
    if (result is None or result == AEConstants.REVIEW_OK or 
        result == AEConstants.REVIEW_WRAP):
      self.sayWord(text=text)
    elif result == AEConstants.REVIEW_NO_NEXT_ITEM:
      self.sayInfo(text=_('last item'))
    elif result == AEConstants.REVIEW_NO_PREV_ITEM:
      self.sayInfo(text=_('first item'))
    elif result == AEConstants.REVIEW_NO_NEXT_WORD:
      self.sayInfo(text=_('last word'))
    elif result == AEConstants.REVIEW_NO_PREV_WORD:
      self.sayInfo(text=_('first word'))

class ReadReviewChar(Task.InputTask):
  '''  
  Reads the character at the pointer or speaks an informational message about
  why the item cannot be read.  
  '''
  def execute(self, **kwargs):
    result = self.getTempVal('review')
    self.stopNow()
    if result == AEConstants.REVIEW_OK or result == AEConstants.REVIEW_WRAP:
      self.sayChar()
    elif result == AEConstants.REVIEW_NO_NEXT_ITEM:
      self.sayInfo(text=_('last item'))
    elif result == AEConstants.REVIEW_NO_PREV_ITEM:
      self.sayInfo(text=_('first item'))
    elif result == AEConstants.REVIEW_NO_NEXT_CHAR:
      self.sayInfo(text=_('last char'))
    elif result == AEConstants.REVIEW_NO_PREV_CHAR:
      self.sayInfo(text=_('first char'))
    
class SpellReviewWord(Task.InputTask):
  ''' 
  Spells the word at the current POR. Says translated names for known symbols
  and punctuation. Says Unicode values for unknown characters.
  '''
  def execute(self, text=None, **kwargs):
    '''
    @param text: Text to say, or None to use the pointer L{POR} by default
    @type text: string
    '''
    sem = AEConstants.SEM_WORD
    # get the spelling format setting
    sf = self.getStyleSetting('SpellFormat', sem=sem)
    # store the old value and set the new one
    old = sf.value
    sf.value = AEConstants.FORMAT_SPELL
    # say the current word with the style changed
    self.stopNow()
    self.sayWord(text=text)
    # restore the old value
    sf.value = old
    
class PronounceReviewWord(Task.InputTask):
  '''  
  Spells the word at the current POR using the NATO phonetic alphabet and names
  for known symbols and punctuation. Says Unicode values and classifications
  for unknown characters. 
  '''
  def execute(self, text=None, **kwargs):
    '''
    @param text: Text to say, or None to use the pointer L{POR} by default
    @type text: string
    '''
    sem = AEConstants.SEM_WORD
    # get the spelling format setting
    sf = self.getStyleSetting('SpellFormat', sem=sem)
    # store the old value and set the new one
    old = sf.value
    sf.value = AEConstants.FORMAT_PHONETIC
    # say the current word with the style changed
    self.stopNow()
    self.sayWord(text=text)
    # restore the old value
    sf.value = old

class PronounceReviewChar(Task.InputTask):
  '''
  Says the current character using the NATO phonetic alphabet and names for 
  known symbols and punctuation. Says Unicode values and classifications for 
  unknown characters.
  '''
  def execute(self, **kwargs):
    sem = AEConstants.SEM_CHAR
    # get the spelling format setting
    sf = self.getStyleSetting('SpellFormat', sem=sem)
    # store the old value and set the new one
    old = sf.value
    sf.value = AEConstants.FORMAT_PHONETIC
    # say the current word with the style changed
    self.stopNow()
    self.sayChar()
    # restore the old value
    sf.value = old
    
class ReadPointerToFocus(Task.InputTask):
  '''
  Reports the new location of the pointer when it snaps back to the application
  focus, selection, and caret position.
  '''
  def execute(self, **kwargs):
    self.doTask('read focus', por=self.task_por, gained=True)
    self.doTask('read item details')    
      
class WhereAmIAncestors(Task.InputTask):
  '''
  Reports all control and container names in child-to-parent order from the 
  current L{task_por} to the root accessible.
  '''
  def execute(self, **kwargs):
    # reset all variables tracking whether information is "new"
    self.perk.resetLastsForNew()
    for curr in self.iterAncestorAccs():
      # announce role and text
      self.sayRole(curr)
      self.sayItem(curr)
      
class WhereAmINow(Task.InputTask):
  '''
  Reports the details of the current L{task_por}.
  '''
  def execute(self, **kwargs):
    # reset all variables tracking whether information is "new"
    self.perk.resetLastsForNew()
    # make sure verbosity is all the way up
    r, i, h = (self.perk_state.RoleVerbosity, self.perk_state.IndexVerbosity,
               self.perk_state.HeaderVerbosity)
    self.perk_state.RoleVerbosity = SPEAK_ALWAYS
    self.perk_state.IndexVerbosity = SPEAK_ALWAYS
    self.perk_state.HeaderVerbosity = SPEAK_ALWAYS
    # and then announce all details
    self.doTask('read item details')
    self.perk_state.RoleVerbosity = r
    self.perk_state.IndexVerbosity = i
    self.perk_state.HeaderVerbosity = h
    
class ReadItemIndex(Task.Task):
  '''
  Reads the row and/or column indices of the current item followed by the 
  total number of items. The total number are only announced if a new control
  has received focus.
  '''
  def execute(self, por=None, **kwargs):
    '''    
    Speaks the row and column indices of the given L{POR} or the L{task_por} if
    no L{POR} is given. Also speaks the total number of items if it has not
    been announced previously.
    
    @param por: Point of regard to an object possibly having an index of 
      interest
    @type por: L{POR}
    '''
    iv = self.getPerkSettingVal('IndexVerbosity')
    if iv == SPEAK_NEVER:
      # say nothing as the user demands
      return
    
    # get the number of rows and cols
    exts = self.getAccTableExtents(por)
    if exts is not None:
      # get the row index
      row = self.getAccRow(por)
      if row is None:
        # no index to report, happens on a container
        return
      # add one to the row offset to make it 1-based for the user
      row += 1
      rows, cols = exts
    else:
      # get attributes for position and size
      row = self.getAccPosInSet(por)
      rows = self.getAccSetSize(por)
      if row is None and rows is None:
        # no index or size, not a table or list, abort
        return
      cols = 0
      
    if cols <= 1:
      # 1D lists
      if rows is not None and (iv == SPEAK_ALWAYS or not self.perk.last_count):
        text = (row, rows)
        template = _('item %d of %d')
      else:
        text = row+1
        template = _('item %d')
    else:
      # 2D tables
      col = self.getAccColumn(por)
      if iv == SPEAK_ALWAYS or not self.perk.last_count:
        try:
          text = (row, col+1, rows, cols)
        except TypeError:
          # not in a cell
          return
        template = _('item %d, %d, of %d, %d')
      else:
        try:
          text = (row, col+1)
        except TypeError:
          # not in a cell
          return
        template = _('item %d, %d')
    self.sayIndex(text=text, template=template)
    self.perk.last_count = True

class ReadNewHeaders(Task.Task):
  '''
  Reads the row and/or column headers if they differ from the last ones
  announced.
  '''
  def execute(self, por=None, **kwargs):
    '''    
    Speaks the row and column headers of the given L{POR} or the L{task_por} if
    no L{POR} is given. Only makes an announcement if the last row/col
    encountered by this L{Task} or L{ReadNewHeaders} is different from the
    current.
    
    @param por: Point of regard to a table cell possibly having a header
    @type por: L{POR}
    '''
    hv = self.getPerkSettingVal('HeaderVerbosity')
    if hv == SPEAK_NEVER:
      # never speaking headers according to user settings
      return
    # get the current row
    row = self.getAccRow(por)
    if row is not None and (hv == SPEAK_ALWAYS or row != self.perk.last_row):
      # speak if setting is always speak, or if the row changed if diff speak
      header = self.getAccRowHeader(por)
      if header:
        self.saySection(text=header, template=_('row %s'))
      self.perk.last_row = row
    # get the current column
    col = self.getAccColumn(por)
    if col is not None and (hv == SPEAK_ALWAYS or col != self.perk.last_col):
      # speak if setting is always speak, or if the col changed if diff speak
      header = self.getAccColumnHeader(por)
      if header:
        self.saySection(text=header, template=_('column %s'))
      self.perk.last_col = col

class ReadNewRole(Task.Task):
  '''
  Reads the role of a widget if it is different from the last role read.
  '''
  def execute(self, por=None, role=None, say_role=True, **kwargs):
    '''
    Speaks the role of the given L{POR} or the L{task_por} if no L{POR} is 
    given. Depends on the RoleVerbosity setting. Does an explicit check for
    the identity of the L{Task} invoking this one to account for double role
    announcements from both focus and selector events.
    
    @param por: Point of regard to the accessible whose role should be said, 
      defaults to L{task_por} if None
    @type por: L{POR}
    @param role: Translated role name to speak. Prefers using this if available
      over fetching the string from the L{POR}
    @type role: string
    @param say_role: Recommendation based on data other than the L{last_role}
      to take into account when the role verbosity is set to always so that
      we don't double announce roles.
    @type say_role: boolean
    '''
    # get the verbosity setting value
    val = self.getPerkSettingVal('RoleVerbosity')
    if val == SPEAK_NEVER:
      # never say roles
      return
    if role is None:
      # fetch the role name
      role = self.getAccRoleName(por)
    
    if (val == SPEAK_ALWAYS and say_role) or self.perk.last_role != role:
       # take the say_role flag into account to make sure we don't repeat role
       # announcements immediately on events
      self.sayRole(text=role)
      self.perk.last_role = role

class ReadNewLevel(Task.Task):
  '''
  Reads the tree level of a widget if it is different from the last level read.
  '''
  def execute(self, por=None, **kwargs):
    '''
    Speaks the level of the given L{POR} or the L{task_por} if no L{POR} is 
    given only if it is different than the last level spoken by this method.
    
    @param por: Point of regard to the accessible whose role should be said, 
      defaults to L{task_por} if None
    @type por: L{POR}
    '''
    level = self.getAccLevel(por)
    if level is not None and level != self.perk.last_level:
      # root is 1
      self.sayLevel(text=level+1, template=_('level %d'))
      self.perk.last_level = level

class ReadNewLabel(Task.Task):
  '''
  Read the label on a widget if it is different from the item text of the
  widget and the last container label read.
  '''
  def execute(self, por=None, **kwargs):
    '''
    Speaks the text of a new label at the given L{POR} or at the L{task_por} 
    if no L{POR} is given only if it is different than the item text and the
    last container announced by L{ReadNewContainer}.
    
    @param por: Point of regard to an accessible that should be announced if it
      is a new label
    @type por: L{POR}
    '''
    label = self.getAccLabel(por)
    if (label and label != self.perk.last_container):
      self.sayLabel(text=label)

class ReadNewContainer(Task.Task):
  '''
  Read the label of the container of the current item as long as it is not the
  same as the label of the container last announced.
  '''
  def execute(self, por=None, **kwargs):
    '''
    Speaks the first interesting ancestor of the given L{POR} if it is 
    different from the last interesting ancestor announced.

    @param por: Point of regard to an accessible that should be announced if it
      is a new menu.
    @type por: L{POR}
    '''
    # get the root accessible
    root = self.getRootAcc()
    for anc in self.iterAncestorAccs(por, allow_trivial=True):
      # don't count the application itself
      if self.hasAccRole('application', anc):
        break
      to_check = [anc]
      # try the previous peer too if it's a label
      prev = self.getPrevPeerAcc(anc)
      if (prev and self.hasAccRole('label', prev) and 
          not self.getAccRelations('label for', prev)):
        to_check.append(prev)
      last = self.perk.last_container
      for acc in to_check:
        name_text = self.getAccName(acc)
        label_text = self.getAccLabel(acc)
        if (((name_text and last.startswith(name_text)) or
            (label_text and last.startswith(label_text)) or
            self.hasAccRole('menu bar', acc))):
          return
        if name_text and name_text.strip():
          # try to announce the name text first
          self.saySection(text=name_text)
          self.perk.last_container = name_text
          return
        if label_text and label_text.strip():
          # then try the label
          self.saySection(text=label_text)
          self.perk.last_container = label_text
          return
          
class ReadMessage(Task.Task):
  '''
  Reads a programmatic LSR announcement.
  '''
  def execute(self, text, sem=AEConstants.SEM_INFO, **kwargs):
    '''
    Makes an arbitrary announcement using the given semantic for styling.
    
    @param text: Text to announce
    @type text: string
    @param sem: Semantic of the announcement. Defaults to info.
    @type sem: integer
    '''
    self.say(text, sem=sem)

class ReadAll(Task.InputTask):
  '''
  Read the remaining contents of the active view starting at the current
  point of regard.
  '''
  def execute(self, interval=None, **kwargs):
    self.mayStop()
    self.registerTask(_ContinueReadAll('read all continued', 1000))
    # read the current item again first
    self.doTask('review current item')
    self.doTask('read all continued')

class _ContinueReadAll(Task.TimerTask):
  '''
  Continue queuing up contents in the view on a timed interval.
  
  @note: if index markers are supported, update the start_por accordingly
  so a stop leaves the user near the last reviewed location, or maybe this is
  an option?
  
  @ivar start_por: Starting point of regard for the read operation
  @type start_por: L{POR}
  @ivar continue_por: Point of regard where reading should continue
  @type continue_por: L{POR}
  '''
  def init(self):
    self.start_por = None
    self.continue_por = None
  
  def execute(self, **kwargs):
    # initialize the start point
    self.start_por = self.start_por or self.task_por
    # always read as if focused
    self.layer = AEConstants.LAYER_FOCUS
    if self.task_por == self.start_por:
      self.task_por = self.continue_por or self.task_por
      # continue where we left off
      for i in xrange(5):
        # always indicate items have been skipped so we don't stop the speech
        # announcing them
        self.setTempVal('has skipped', True)
        # only invoke the review task, not all other chained to it
        self.doTask('review next item', chain=False)
        # now invoke the speech task for reviewing items
        self.doTask('read review item', chain=False, stop=False)
        if self.getTempVal('review') == AEConstants.REVIEW_NO_NEXT_ITEM:
          # if the review perk indicates no next item, stop reading
          self.unregisterTask(self)
          break
    else:
      # task por changed since last continuation, abort
      self.unregisterTask(self)
    # store the last review position
    self.continue_por = self.task_por
    # restore the starting por
    self.task_por = self.start_por

class ReadTop(Task.InputTask):
  '''
  Read top of the active view, typically the title of the foreground window.
  '''
  def execute(self, **kwargs):
    self.stopNow()
    self.sayWindow()
    
class _BaseAttributes(Task.InputTask):
  '''
  Base class for other Tasks that will read text attributes.
  '''
  def _getAttrs(self):
    '''
    Reusable method for getting default and current text attributes and 
    merging them into one dictionary.
    
    @return: Dictionary of name/value attribute pairs
    @rtype: dictionary
    '''
    attrs = self.getAccAllTextAttrs()
    dattrs = self.getAccDefTextAttrs()
    
    if not attrs and not dattrs:
      # no atttribute information
      self.sayInfo(text=_('text attributes unavailable'))
      return None
    elif not attrs:
      # only default attribute information
      attrs = dattrs
    elif not dattrs:
      # only current attribute information
      pass
    else:
      # overwrite default with current
      dattrs.update(attrs)
      attrs = dattrs
    return attrs
      
class ReadTextColor(_BaseAttributes):
  '''
  Read the text foreground and background color names followed by their values.
  '''
  def execute(self, **kwargs):
    self.stopNow()
    # get merged attributes
    attrs = self._getAttrs()
    
    if attrs is None: return

    # get foreground value
    fgval = attrs.get('fg-color')
    fgname = self.getColorString(fgval) or _('default foreground') 
    
    # get background value
    bgval = attrs.get('bg-color')
    bgname = self.getColorString(bgval) or _('default background')

    # i18n: foreground on background color (e.g. "black on white")
    template = _('%s on %s.') 
    names = template % (fgname, bgname)
    if bgval is None and fgval is None:
      self.sayColor(text=names)
    elif bgval is not None and fgval is not None:
      self.sayColor(text=(fgval, bgval), template=names + ' ' + template)
    elif fgval is not None:
      self.sayColor(text=fgval, template=names + ' %s')
    elif bgval is not None:
      self.sayColor(text=bgval, template=names + ' %s')
      
class ReadTextAttributes(_BaseAttributes):
  '''
  Read all remaining text attribute name/value pairs sans color information.
  
  '''
  def execute(self, **kwargs):
    self.stopNow()
    attrs = self._getAttrs()
    for name, value in attrs.items():
      if not name.endswith('color'):
        self.sayTextAttrs(text=(name, value), template='%s: %s.')

class ReadDescription(Task.InputTask):
  '''
  Read the description of the accessible at the current point of regard.
  '''
  def execute(self, **kwargs):
    self.stopNow()
    self.sayDesc()
  
class ReadItemDetails(Task.Task):
  '''
  Reads all details of the current item.
  '''
  def execute(self, text=None, say_role=True, **kwargs):
    '''
    @param text: Item text to use in the announcement
    @type text: string
    @param say_role: Recommendation about whether the role should be announced
      or not purely based on the type of event received
    @type say_role: boolean
    '''
    if text is None:
      text = self.getItemText()
    # announce the item role only if the caller asks for it, otherwise we
    # could be making a repeat announcement
    self.doTask('read new role', say_role=say_role)
    # announce the item text only if it is different than the label
    label = self.getAccLabel()
    if (label != text):
      self.sayItem(text=text)
    # try to announce the headers on the row and column
    self.doTask('read new headers')
    # see if there are any states worth reporting
    self.sayState()
    # try to announce the level
    self.doTask('read new level')
    # try to announce the item index
    self.doTask('read item index')
    # try to announce the total number of items
    self.doTask('read item count')
    # get the hotkey for this item
    self.sayHotkey()
    if not text:
      # only announce value if it isn't encoded in the item text
      self.sayValue()
    # i18n hint: a numeric range by a step (e.g. 0.0 to 100.0 by 2.0)
    self.sayValueExtents(template=_('%.1f to %.1f by %.1f'))
    
class InViewTimer(Task.TimerTask):
  '''
  Executes some time after a View lost event to check for an active window.  
  Makes an announcement if no window is active.
  '''
  def execute(self, **kwargs):
    # make sure there are still no active windows
    if self.perk_state.activewincnt == 0:
      self.stopNow()
      self.sayInfo(text=_('no active window'))
    self.unregisterTask(self)

class HandleViewChange(Task.ViewTask):
  '''
  Task that handles a L{AEEvent.ViewChange}.
  
  @todo: disabled announcement for the time being
  '''
  def executeLost(self, por, title, **kwargs): 
    # decrement active window count 
    self.perk_state.activewincnt -= 1
    # start timer if there are no active windows
    # if self.perk_state.activewincnt == 0:
      #self.registerTask(InViewTimer('in view timer', 1500))
    return True
  
  def executeFirstGained(self, por, title, **kwargs):
    '''    
    Announces the title of the first view to be activated, but without stopping
    previous speech which is likely to be the LSR welcome message. Inhibits the
    next stop to avoid never announcing the title because of another immediate
    event following this one (i.e. focus).
    
    @param por: Point of regard to the root of the new view
    @type por: L{POR}
    @param title: Title of the newly activated view
    @type title: string
    '''
    # say the title of the new view
    if not self.sayWindow(text=title):
      self.sayWindow(text=_('no title'))
    # reset stateful vars
    self.perk.resetLasts(title)
    # increment active window count
    self.perk_state.activewincnt += 1
    return True
  
  def executeGained(self, por, **kwargs):
    '''
    Stops output then calls L{executeFirstGained} to reuse its code for 
    announcing the new view.
    '''
    # stop output
    self.stopNow()
    # switch to a language matching the application locale
    self.perk.switchLanguages()
    rv = self.executeFirstGained(por, **kwargs)
    # prevent the next event (focus?) from stopping this announcement
    self.inhibitMayStop()
    # see if we want to handle this new view in a special way
    role = self.getAccRole()
    if role == 'alert':
      self.registerTask(ReadAlert('read alert'))
    elif role == 'dialog':
      self.registerTask(ReadDialog('read dialog'))
    return rv
    
class HandleFocusChange(Task.FocusTask):
  '''  
  Task that handles a L{AEEvent.FocusChange}.
  '''  
  def executeGained(self, por, **kwargs):
    '''
    Annouces the label and role of the newly focused widget. The role is output
    only if it is different from the last one announced.
    
    @param por: Point of regard for the new focus
    @type por: L{POR}
    '''
    self.mayStop()
    # announce containers when they change
    self.doTask('read new container', por=por)
    # announce the label when present if it is not the same as the name or the
    # container
    self.doTask('read new label', por=por)
    # only announce role when it has changed
    self.doTask('read new role', por=por)
    # prevent the next selector or caret from stopping this announcement
    self.inhibitMayStop()
    # reset stateful variables
    self.perk.resetLastsOnFocus()
    return True

class HandleSelectorChange(Task.SelectorTask):
  ''' 
  Task that handles a L{AEEvent.SelectorChange}.
  '''
  def executeActive(self, por, text, **kwargs):
    '''
    Announces the text of the active item.
    
    @param por: Point of regard where the selection event occurred
    @type por: L{POR}
    @param text: The text of the active item
    @type text: string
    '''
    self.mayStop()
    say_role = not isinstance(self.event_man.last_event, AEEvent.FocusChange)
    self.doTask('read item details', por=por, text=text, say_role=say_role)
    
  def executeText(self, por, text, **kwargs):
    '''
    Announces the localized word selected or unselected followed by the total 
    number of characters in the selection.
    
    Single line text boxes fire selection events followed by a caret event.  
    Multiline text boxes fire events in the opposite order.  The
    inhibitMayStop() allows the selection event to complete for single line
    text boxes before the caret event stops it with a call to mayStop().
    
    @param por: Point of regard where the selection event occurred
    @type por: L{POR}
    @param text: The currently selected text
    @type text: string
    '''
    if self.getAccCaret() != self.getPointerPOR():
      # if the caret is out of sync with our pointer, we haven't received a
      # movement event yet corresponding to this selection; inhibit the next
      # may stop as the next event will be the movement
      self.inhibitMayStop()
    n = len(text)
    if n >= self.perk.last_sel_len:
      self.sayTextAttrs(text=n, template=_('selected %d'))
    elif self.perk.last_sel_len != 0:
      # don't say unselection when the last selection length was zero also
      self.sayTextAttrs(text=(self.perk.last_sel_len-n, n),\
                   template=_('unselected %d, %d remaining'))
    self.perk.last_sel_len = n

class HandleCaretChange(Task.CaretTask):
  '''  
  Task that handles a L{AEEvent.CaretChange}. The L{execute} method performs
  actions that should be done regardless of whether text was inserted or
  deleted or the caret moved. The other three methods handle these more
  specific cases.
  
  @ivar changed: Tracks whether the last caret move event was a caused by a
    change (insert/delete) or just navigation
  @type changed: boolean
  '''
  def init(self):
    self.changed = False
  
  def execute(self, por, text, text_offset, added, **kwargs):
    '''
    Stores the point of regard to the caret event location. Tries to stop
    current output.
    
    @param por: Point of regard where the caret event occurred
    @type por: L{POR}
    @param text: The text inserted, deleted or the line of the caret
    @type text: string
    @param text_offset: The offset of the inserted/deleted text or the line 
      offset when movement only
    @type text_offset: integer
    @param added: True when text added, False when text deleted, and None 
      (the default) when event is for caret movement only
    @type added: boolean
    '''
    # let the base class call the appropriate method for inset, move, delete
    rv = Task.CaretTask.execute(self, por, text, text_offset, added, **kwargs)
    # store for next comparison
    self.perk.last_caret = por
    return rv
    
  def executeInserted(self, por, text, text_offset, **kwargs):
    '''
    Announces the inserted text.
    
    @param por: Point of regard where the caret event occurred
    @type por: L{POR}
    @param text: The text inserted
    @type text: string
    @param text_offset: The offset of the inserted text
    @type text_offset: integer
    '''
    self.changed = True
    if len(text) == 1:
      w = (self.perk_state.WordEcho and 
           unicodedata.category(text)[0] in ('C', 'Z'))
      c = self.perk_state.CharEcho
      if w and c:
        # word and character echo
        #s = self.getStyle(layer=AEConstants.LAYER_TIER, 
                          #sem=AEConstants.SEM_WORD)
        self.mayStop()
        self.sayChar(text=text)
        # Don't repeat 'blank' announcement
        word_text= self.getWordText(por)
        if word_text.strip():
          self.say(word_text, por, sem=AEConstants.SEM_WORD,
                   layer=AEConstants.LAYER_TIER)
      elif w:
        # word echo
        self.mayStop()
        self.sayWord(por)
      elif c:
        # character echo
        self.mayStop()
        self.sayChar(text=text)
    else:
      # chunk of text, if length is longer than CharLimit summarize
      # it (speak text length), else say the whole thing
      if len(text) > self.perk_state.CharLimit:
        template=_('%d characters inserted')
        self.mayStop()
        self.sayInfo(text=len(text), template=template)
      else:
        self.mayStop()
        self.sayItem(text=text)
    
  def executeMoved(self, por, text, text_offset, **kwargs):
    '''
    Announces the text passed in the caret movement.
    
    @param por: Point of regard where the caret event occurred
    @type por: L{POR}
    @param text: The text passed during the move
    @type text: string
    @param text_offset: The offset of the new caret position
    @type text_offset: integer
    '''
    # don't echo again if a change just happened
    if self.changed:
      self.changed = False
      return
    
    if not por.isSameAcc(self.perk.last_caret):
      # say the current line or read the entire text depending on the user
      # setting for text verbosity
      tv = self.getPerkSettingVal('TextVerbosity')
      if tv == SPEAK_NEVER:
        return
      elif tv == SPEAK_LINE:
        self.mayStop()
        self.sayItem(text=text)
        if (self.hasAccState('multi line') and 
            self.getAccTextSelectionCount() > 0):
          # announce the selection if this is a multiline box as we often do not
          # receive an event; klude that probably breaks for non-gtk apps
          self.sayTextAttrs(text=len(self.getAccTextSelection()[0]),
                            template=_('selected %s'))
      else:
        # read everything starting from the caret position
        self.doTask('read all', **kwargs)
    elif not por.isSameItem(self.perk.last_caret):
      # say the new line when item_offset changes
      self.mayStop()
      self.sayItem(text=text)
    else:
      # respond to caret movement on same line
      if por.isCharBefore(self.perk.last_caret):
        # moved left, say text between positions
        diff = text[por.char_offset : self.perk.last_caret.char_offset]
        self.mayStop()
        if len(diff) > 1:
          self.sayItem(text=diff)
        else:
          self.sayChar(text=diff)
      elif por.isCharAfter(self.perk.last_caret):
        # moved right, say text between positions
        diff = text[self.perk.last_caret.char_offset : por.char_offset]
        self.mayStop()
        if len(diff) > 1:
          self.sayItem(text=diff)
        else:
          self.sayChar(text=diff)
    self.may_stop = 0
   
  def executeDeleted(self, por, text, text_offset, **kwargs):
    '''
    Announces the deleted text. Prepends delete and backspace text to indicate
    which way the caret moved.
    
    @param por: Point of regard where the caret event occurred
    @type por: L{POR}
    @param text: The text delete
    @type text: string
    @param text_offset: The offset of the delete text
    @type text_offset: integer
    '''
    self.mayStop()
    if (self.perk.last_caret.item_offset + 
        self.perk.last_caret.char_offset) > text_offset:
      # say backspace if we're backing up
      self.sayItem(text=text, template=_('backspace %s'))
      self.changed = True
    else:
      # say delete if we're deleting the current
      self.sayItem(text=text, template=_('delete %s'))
      self.changed = False

  def update(self, por, text, text_offset, added, **kwargs):
    '''
    Updates the last caret cache with the current L{por}, but only if the 
    temp value of 'no update' has not been set to True.
    
    @param por: Point of regard where the caret event occurred
    @type por: L{POR}
    @param text: The text inserted, deleted or the line of the caret
    @type text: string
    @param text_offset: The offset of the inserted/deleted text or the line 
      offset when movement only
    @type text_offset: integer
    @param added: True when text added, False when text deleted, and None 
      when event is for caret movement only
    @type added: boolean
    '''
    if self.getTempVal('no update'):
      return
    # store for next comparison
    self.perk.last_caret = por
    self.changed = False
  
class HandleStateChange(Task.StateTask):
  '''  
  Task that handles a L{AEEvent.StateChange}.
  '''
  def execute(self, por, name, value, **kwargs):
    '''
    Announces state changes according to the ones of interest defined by
    L{getStateText}.
    
    @param por: Point of regard where the state change occurred
    @type por: L{POR}
    @param name: Name of the state that changed
    @type name: string
    @param value: Is the state set (True) or unset (False)?
    @type value: boolean
    '''
    text = self.getStateText(por, name, value)
    if text:
      # try a may stop
      self.mayStop()
      self.sayState(text=text)
    # don't update the pointer on a state change
    self.inhibitAutoPointer()

class HandlePropertyChange(Task.PropertyTask):
  '''
  Task that handles a L{AEEvent.PropertyChange}.
  '''
  def execute(self, por, name, value, **kwargs):
    '''
    Announces value changes.
    
    @param por: Point of regard where the property change occurred
    @type por: L{POR}
    @param name: Name of the property that changed
    @type name: string
    @param value: New value of the property
    @type value: object
    '''
    if name == 'value':
      if not self.getItemText(por):
        # only announce value if we're not going to get a text change event
        # immediately after saying the same thing
        self.mayStop()
        self.sayItem(text=value)
    # don't update the pointer on a property change
    self.inhibitAutoPointer()
    
class ReadAlert(Task.FocusTask):
  '''
  Task registered temporarily when an alert dialog is shown. Collects all 
  labels of interest in the dialog and says them in order.
  '''
  def executeGained(self, **kwargs):
    por = self.getViewRootAcc()
    # start iteration at the root
    for por in self.iterNextAccs(por):
      # announce all labels
      if self.hasAccRole('label', por):
        self.sayItem(por)
    # unregister this task
    self.unregisterTask(self)
    return True
      
class ReadDialog(Task.FocusTask):
  '''
  Task registered temporarily when a dialog (not alert) is shown.  Announces 
  first two labels in the dialog in the order they were traversed.  Traversal 
  of some containers is bypassed.
  '''
  def executeGained(self, **kwargs):
    count = 0
    por = self.getViewRootAcc()
    # start iteration at the root
    while por is not None:
      role = self.getAccRole(por)
      if self.hasAccRole('label', por) and count < 2:
        text = self.getItemText(por)
        if text.strip():
          self.sayItem(por)
          count += 1
      elif count >= 2:
        break
      # don't traverse some containers
      if self.hasOneAccRole(por, 'page tab','table','push button','combo box'):
        por = self.getNextPeerAcc(por)
      # get next por walker traverses
      else:
        por = self.getNextAcc(por)
    # unregister this task  
    self.unregisterTask(self)
    return True
