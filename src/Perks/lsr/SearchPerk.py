'''
Defines L{Task}s for searching through accessible tree.

@author: Scott Haeger
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
# import useful modules for Perks
import Perk, Task, AEConstants
from POR import POR
from i18n import _

__uie__ = dict(kind='perk', all_tiers=True)

class SearchPerk(Perk.Perk):
  '''
  Manages the search dialog and performs search through accessible tree using
  query input by the user.
  '''
  def init(self):
    self.chooser = None   
    # Register for search specific tasks
    self.registerTask(SearchChooserTask('search show chooser'))
    self.registerTask(SearchFindNext('search find next'))
    self.registerTask(SearchFindPrev('search find prev'))
    
  def getName(self):
    return _('Search dialog')
  
  def getDescription(self):
    return _('Searches the foreground window for user specified text')
  
class SearchFindNext(Task.InputTask):
  '''
  Search through the accessible tree in the forward (down the tree) direction.
  '''
  def execute(self, **kwargs):
    if self.perk.chooser is None:
      return False
    query = self.perk.chooser.getQuery()
    if query is None or query == '':
      return False
    # define predicates
    if self.perk.chooser.isMatchCaseSet():
      def pred(acc):
        return self.getAccName(por=acc) == query
    else:
      querylower = query.lower()
      def pred(acc):
        return self.getAccName(por=acc).lower() == querylower
    # do search
    for acc in self.iterNextItems(wrap=True):
      if pred(acc):
        self.doTask('pointer to por', por=acc)
        return
    if self.perk.chooser.isWrapSet():
      for acc in self.iterNextItems(por=self.getRootAcc(), wrap=True):
        if pred(acc):
          self.doTask('pointer to por', por=acc)
          return    
    self.doTask('read message', text=_('%s not found') %query) 
    
    
class SearchFindPrev(Task.InputTask):
  '''
  Search through the accessible tree in the backward (up the tree) direction.
  '''
  def execute(self, **kwargs):
    if self.perk.chooser is None:
      return False
    query = self.perk.chooser.getQuery()
    if query is None or query == '':
      return False
    # define predicates
    if self.perk.chooser.isMatchCaseSet():
      def pred(acc):
        return self.getAccName(por=acc) == query
    else:
      querylower = query.lower()
      def pred(acc):
        return self.getAccName(por=acc).lower() == querylower

    for acc in self.iterPrevItems(wrap=True):
      if pred(acc):
        self.doTask('pointer to por', por=acc)
        return
    if self.perk.chooser.isWrapSet():
      for acc in self.iterPrevItems(por=self.getEndAcc(), wrap=True):
        if pred(acc):
          self.doTask('pointer to por', por=acc)
          return    
    self.doTask('read message', text=_('%s not found') %query) 

class SearchChooserTask(Task.ChooserTask):
  '''  
  Shows the Search chooser which allows a user to search the current view for
  a given accessible.
  '''
  def executeStart(self, timestamp, **kwargs):
    '''
    Runs when an input gesture is given to start the chooser. 
    
    @param timestamp: Time to provide to the chooser dialog so it can raise 
      itself to the foreground properly
    @type timestamp: float
    '''
    # load the chooser if it is not loaded
    if self.perk.chooser is None:
      self.perk.chooser = self.loadChooser('SearchChooser', 
                                           app_name=self.getAppName(), 
                                           timestamp=timestamp)
    else:
      # close the search dialog on second gesture
      self.perk.chooser.close()
      self.executeEnd(self.perk.chooser)
      # announce that the dialog closed in the background
      self.doTask('read message', 
                  text=_('%s search dialog closed') % self.getAppName()) 
    
  def executeEnd(self, chooser, **kwargs):
    '''
    Runs when the chooser is closed by a repeat search key press, or the dialog
    is closed.

    @param chooser: Chooser that fired the event
    @type chooser: L{AEChooser}
    '''
    self.perk.chooser = None
    self.unloadChooser(chooser)
