'''
Allows a user to switch between up to three languages of their choosing as 
supported by the current speech engine.

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
# import useful modules for Perks
import Perk, Task, AEConstants
from POR import POR
from i18n import bind, _

# metadata describing this Perk
__uie__ = dict(kind='perk', tier=None, all_tiers=False)

class LanguagePerkState(Perk.PerkState):
  '''
  Defines three enumeration fields which allow the user to select three 
  different languages among which to choose.
  
  Alt1Lang (enum): First language, defaults to current device setting
  
  Alt2Lang (enum): Second language, defaults to current device setting
  
  Alt3Lang (enum): Third language, defaults to current device setting
  '''
  def init(self):
    self.newEnum('Alt1Lang', None, _('First language'), {_('None') : None},
                 _('First alternative language'), False)
    self.newEnum('Alt2Lang', None, _('Second language'), {_('None') : None},
                 _('Second alternative language'), False)
    self.newEnum('Alt3Lang', None, _('Third language'), {_('None') : None},
                 _('Third alternative language'), False)
    
  def getGroups(self):
    g = self.newGroup()
    g.extend(['Alt1Lang', 'Alt2Lang', 'Alt3Lang'])
    return g

class LanguagePerk(Perk.Perk):
  '''
  Enables switching the active speech engine among up to three languages using
  a hotkey press in the current application.
  '''
  STATE = LanguagePerkState
  
  def init(self):
    kbd = self.getInputDevice(None, 'keyboard')
    self.registerTask(SwitchSpeech('language switch speech'))
    self.registerCommand(kbd, 'language switch speech', False,
                         [kbd.AEK_CAPS_LOCK, kbd.AEK_L])
    
    # get the languages supported by the current device
    try:
      lang = self.getStyleSetting('Language')
    except Task.InvalidStyleError:
      # language not supported, do nothing
      return
    
    # populate the lists with the current available languages and set the 
    # current language as the default
    for s in self.perk_state:
      s.update(lang)
      s.value = lang.value

  def getName(self):
    return _('Language switcher')
                         
  def getDescription(self):
    return _('Enables switching the active speech engine among up to three '
             'languages using a hotkey press in the current application.')

class SwitchSpeech(Task.InputTask):
  '''
  Cycles through the alternative languages
  '''
  def execute(self, **kwargs):
    self.stopNow()
    # get the current language setting
    lang = self.getStyleVal('Language')
    # get all setting names, ordered
    names = [s.name for s in self.perk_state]
    names.sort()
    # get all setting values
    values = [s.value for s in (self.getPerkSetting(n) for n in names)]

    # find the current value and start searching for a new value from there
    try:
      start = values.index(lang)
    except ValueError:
      start = 0
    
    for i in xrange(start+1, start+len(names)):
      n = i % len(names)
      val = values[n]
      if val != lang:
        # switch to the first one that the active lang doesn't match
        self.setStyleVal('Language', val)
        text = _('speaking in %s')%self.getPerkSetting(names[n]).getLabel(val)
        self.doTask('read message', text=text)
        break
