'''
Provides review commands for traversing items, words, and characters.

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
# import useful modules for Perks
import Perk, Task, AEConstants
from POR import POR
from i18n import bind, _

# metadata describing this Perk
__uie__ = dict(kind='perk', tier=None, all_tiers=True)

# constants for Skipping enumeration
SKIP_ALL = 0
SKIP_REPORT = 1
SKIP_NONE = 2

# result constants for review Tasks
REVIEW_OK = 0
REVIEW_NO_NEXT_ITEM = 1
REVIEW_NO_NEXT_WORD = 2
REVIEW_NO_NEXT_CHAR = 3
REVIEW_NO_PREV_ITEM = 4
REVIEW_NO_PREV_WORD = 5
REVIEW_NO_PREV_CHAR = 6
REVIEW_WRAP = 7

class ReviewPerkState(Perk.PerkState):
  '''
  Defines options having to do with review mode.
  
  Skipping (enumeration): Determines how the review keys handle empty nodes 
    that meet the requires for the walker but may be of no interest to the 
    user. If SKIP_ALL, any item with no item text is skipped and the
    walk continues. If SKIP_REPORT_IMPORTANT, a signal is sent to a special 
    task named 'review skip' before skipping empty items that are interactive 
    and continuing the walk. If SKIP_REPORT_ALL, a signal is sent to the same 
    task before skipping any empty item and continuing the walk. If SKIP_NONE,
    the walk stops on all items.
  
  OnlyVisible (bool): Should review keys only walk visible items or hidden 
    items also?
    
  Wrap (bool): Should movement of the pointer wrap at item boundaries?
  '''
  def init(self):
    self.newEnum('Skipping', SKIP_REPORT, _('Skip empty items'), 
                 {_('Always') : SKIP_ALL, _('Never') : SKIP_NONE, 
                  _('Report') : SKIP_REPORT},
                 _('Determines how the review commands react to empty items. '
                   'Always skips all empty, non-focusable items without '
                   'notification. Report skips all empty, non-focusable items '
                   'with a report they were skipped. Never disables '
                   'skipping.'))
    self.newBool('OnlyVisible', True, _('Review visible items only?'), 
                 _('When set, reviewing only includes visible items. '
                   'Otherwise, invisible items are also included.'))
    self.newBool('Wrap', True, _('Wrap pointer across items?'),
                 _('When set, next and previous, word and character navigation'
                   ' can cross item boundaries. Otherwise, first and last,'
                   ' word and character are announced instead.'))
    
  def getGroups(self):
    '''
    Creates configuration groups from pre-defined settings.
    @return: root setting group
    @rtype: L{AEState.Setting.Group}
    '''
    g = self.newGroup()
    g.append('Skipping')
    g.append('OnlyVisible')
    g.append('Wrap')
    return g

class ReviewPerk(Perk.Perk):
  '''
  Registers L{Task}s for reviewing items, words, and characters. Also registers
  L{Task}s for moving pointer to por and focus to por.
  '''
  STATE = ReviewPerkState
  
  def init(self):
    # register all tasks twice, once as preview and once as not
    # the base class for all review classes will take care of adding the word
    # 'peek' to those tasks with the peek flag set
    for i in (True, False):
      self.registerTask(CurrentItem(i, 'review current item'))
      self.registerTask(NextItem(i, 'review next item'))
      self.registerTask(PreviousItem(i, 'review previous item'))
      
      self.registerTask(CurrentWord(i, 'review current word'))
      self.registerTask(NextWord(i, 'review next word'))
      self.registerTask(PreviousWord(i, 'review previous word'))
      
      self.registerTask(CurrentChar(i, 'review current char'))
      self.registerTask(NextChar(i, 'review next char'))
      self.registerTask(PreviousChar(i, 'review previous char'))
      
    self.registerTask(FocusToPOR('focus to por'))
    self.registerTask(PointerToPOR('pointer to por'))
    self.registerTask(MouseToPOR('mouse to por'))
    
    self.registerTask(SkipItem('review skip report'))
    self.registerTask(ShouldSkip('review should skip'))
    
    # make result constants accessible from AEConstants
    self.registerConstants(globals(), 'REVIEW_NO_NEXT_CHAR', 
                           'REVIEW_NO_NEXT_ITEM',
                           'REVIEW_NO_NEXT_WORD', 'REVIEW_NO_PREV_CHAR',
                           'REVIEW_NO_PREV_WORD', 'REVIEW_NO_PREV_ITEM',
                           'REVIEW_OK', 'REVIEW_WRAP', 'SKIP_ALL',
                           'SKIP_REPORT', 'SKIP_NONE')
                           

  def getName(self):
    return _('Review mode')
                         
  def getDescription(self):
    return _('Defines commands for reviewing applications by items, words, '
             'and characters as well as moving the pointer and focus.')

class ReviewBase(Task.InputTask):
  '''
  Base class for all review actions.
  
  @ivar peek: Should this review L{Task} instance actually affect the pointer
    L{POR} and allow announcements of its changed state or simply give a 
    preview of where it will wind up for programmatic purposes?
  @type peek: boolean
  '''
  def __init__(self, peek, *args, **kwargs):
    '''
    Appends the word 'peek' to the name of the class if the L{peek} flag is
    True.
    '''
    super(ReviewBase, self).__init__(*args, **kwargs)
    self.peek = peek
    if self.peek:
      # append 'peek' to the name of the class 
      self.ident += ' peek'
          
  def moveToPOR(self, por):
    '''
    Overrides the L{Task.Tools} method to account for peeking. If L{peek} is
    True, stores the new L{POR} as a temporary task value under the name
    'peek' instead of updating the actual pointer L{POR}.
    '''
    if self.peek:
      self.setTempVal('peek', por)
    else:
      Task.InputTask.moveToPOR(self, por)

class CurrentItem(ReviewBase):
  '''
  Moves the POR to the beginning of the current item.
  
  Chain values
    - review (integer): L{REVIEW_OK}
    - has skipped (boolean): False
    - will skip (boolean): False
    - peek: L{POR} of current item (if L{peek} is True)
  '''
  def execute(self, **kwargs):
    por = self.getCurrItem()
    self.moveToPOR(por)
    self.setTempVal('review', REVIEW_OK)
    self.setTempVal('has skipped', False)

class NextItem(ReviewBase):
  '''
  Moves the POR to the beginning of the next item.  Keyword argument 
  'onlyvisible' overrides OnlyVisible user setting. 
  
  Chain values
    - review (integer): L{REVIEW_NO_NEXT_ITEM} or L{REVIEW_OK}
    - has skipped (boolean): True if at least one item has been skipped
    - will skip (boolean): When peek is True, True if at least one item will be
    skipped
    - peek: L{POR} of next item if L{peek} is True
  '''
  def execute(self, onlyvisible=None, **kwargs):
    # keyword argument overrides user setting
    if onlyvisible is None:
      onlyvisible = self.perk_state.OnlyVisible
    # respect if the skipping flag has already been set
    skipped = self.getTempVal('has skipped')
    self.setTempVal('has skipped', skipped or False)
    # try to get the next item
    for por in self.iterNextItems(wrap=True, 
                                  only_visible=onlyvisible):
      # check if we can stop on this item
      self.doTask('review should skip', por=por, peek=self.peek)
      if not self.getTempVal('should skip'):
        # indicate the item is OK to review
        self.setTempVal('review', REVIEW_OK)
        # store the new POR
        self.moveToPOR(por)
        return
    # indicate there is no next item
    self.setTempVal('review', REVIEW_NO_NEXT_ITEM)
      
class PreviousItem(ReviewBase):
  '''
  Moves the POR to the beginning of the previous item.  Keyword argument 
  'onlyvisible' overrides OnlyVisible user setting. 
  
  Chain values
    - review (integer): L{REVIEW_NO_PREV_ITEM} or L{REVIEW_OK}
    - has skipped (boolean): True if at least one item has been skipped
    - will skip (boolean): When peek is True, True if at least one item will be
    skipped
    - peek: L{POR} of previous item if L{peek} is True
  '''
  def execute(self, onlyvisible=None, **kwargs):
    # keyword argument overrides user setting
    if onlyvisible is None:
      onlyvisible = self.perk_state.OnlyVisible
    # respect if the skipping flag has already been set
    skipped = self.getTempVal('has skipped')
    self.setTempVal('has skipped', skipped or False)
    
    # try to get the previous item
    for por in self.iterPrevItems(wrap=True,
                                  only_visible=onlyvisible):
      # check if we can stop on this item
      self.doTask('review should skip', por=por, peek=self.peek)
      if not self.getTempVal('should skip'):
        # indicate this item is OK to review
        self.setTempVal('review', REVIEW_OK)
        # store the new POR
        self.moveToPOR(por)
        return
    # indicate there is no previous item
    self.setTempVal('review', REVIEW_NO_PREV_ITEM)

class CurrentWord(ReviewBase):
  '''
  Moves the POR to the beginning of the current word.
  
  Chain values
    - review (integer): L{REVIEW_OK}
    - peek: L{POR} of current word if L{peek} is True
  '''
  def execute(self, **kwargs):
    por = self.getCurrWord()
    self.moveToPOR(por)
    self.setTempVal('review', REVIEW_OK)

class NextWord(ReviewBase):
  '''
  Moves the POR to the beginning of the next word.
  
  Chain values
    - review (integer): L{REVIEW_NO_NEXT_WORD}, L{REVIEW_NO_NEXT_ITEM}, 
      L{REVIEW_OK}, L{REVIEW_WRAP}
    - peek: L{POR} of next word if L{peek} is True
  '''
  def execute(self, **kwargs):
    # try to get the next word
    por = self.getNextWord()
    if por is not None:
      # indicate we moved to the next word in the current item
      self.moveToPOR(por)
      self.setTempVal('review', REVIEW_OK)
      return
    if not self.perk_state.Wrap:
      # indicate a next word exists, but we can't wrap to it because of the
      # current settings
      self.setTempVal('review', REVIEW_NO_NEXT_WORD)
      return
    # get the first word of the next item
    por = self.getNextItem(wrap=True, only_visible=self.perk_state.OnlyVisible)
    if por is None:
      # indicate this is the last item
      self.setTempVal('review', REVIEW_NO_NEXT_ITEM)
      return
    # move to the first word of the next item
    self.moveToPOR(por)
    # indicate we moved to the first word in the next item
    self.setTempVal('review', REVIEW_WRAP)

class PreviousWord(ReviewBase):
  '''
  Moves the POR to the beginning of the previous word.
  
  Chain values
    - review (integer): L{REVIEW_NO_PREV_WORD}, L{REVIEW_NO_PREV_ITEM}, 
      L{REVIEW_OK}, L{REVIEW_WRAP}
    - peek: L{POR} of previous word if L{peek} is True
  '''
  def execute(self, **kwargs):
    # try to get the previous word
    por = self.getPrevWord()
    if por is not None:
      # indicate we moved to the previous word in the current item
      self.moveToPOR(por)
      self.setTempVal('review', REVIEW_OK)
      return
    if not self.perk_state.Wrap:
      # indicate a prev word exists, but we can't wrap to it because of the
      # current settings
      self.setTempVal('review', REVIEW_NO_PREV_WORD)
      return
    # get the last word of the previous item
    por = self.getPrevItem(wrap=True, only_visible=self.perk_state.OnlyVisible)
    if por is None:
      # indicate this is the first item
      self.setTempVal('review', REVIEW_NO_PREV_ITEM)
      return
    por = self.getLastWord(por)
    # if it's still None, there's a problem
    if por is None:
      raise Task.PORError
    # move to the last word in the previous item
    self.moveToPOR(por)
    # indicate we moved to the last word in the previous item
    self.setTempVal('review', REVIEW_WRAP)
    
class CurrentChar(ReviewBase):
  '''
  Does nothing. The POR is already referring to the current character. This 
  task only exists so others can link to it.
  
  Chain values
    - review (integer): L{REVIEW_OK}
    - peek: L{POR} of current character if L{peek} is True
  '''
  def execute(self, **kwargs):
    # for the sake of peeking, move to where we are already
    self.moveToPOR(self.task_por)
    self.setTempVal('review', REVIEW_OK)

class PreviousChar(ReviewBase):
  '''
  Moves the POR to the next character.
  
  Chain values
    - review (integer): L{REVIEW_NO_PREV_CHAR}, L{REVIEW_NO_PREV_ITEM}, 
      L{REVIEW_OK}, L{REVIEW_WRAP}
    - peek: L{POR} of previous character if L{peek} is True
  '''
  def execute(self, **kwargs):
    self.stopNow()
    # try to get the previous character
    por = self.getPrevChar()
    if por is not None:
      # indicate we moved to the previous character in the current item
      self.moveToPOR(por)
      self.setTempVal('review', REVIEW_OK)
      return
    if not self.perk_state.Wrap:
      # indicate a prev char exists, but we can't wrap to it because of the
      # current settings
      self.setTempVal('review', REVIEW_NO_PREV_CHAR)
      return
    # get the last char of the previous item
    por = self.getPrevItem(wrap=True, only_visible=self.perk_state.OnlyVisible)
    if por is None:
      # indicate this is the first item
      self.setTempVal('review', REVIEW_NO_PREV_ITEM)
      return
    # get the last character
    por = self.getLastChar(por)
    # if it's still None, there's a problem
    if por is None:
      raise Task.PORError
    # move to the last character of the previous item
    self.moveToPOR(por)
    # indicate we moved to the last character in the previous item
    self.setTempVal('review', REVIEW_WRAP)

class NextChar(ReviewBase):
  '''
  Moves the POR to the next character.
  
  Chain values
    - review (integer): L{REVIEW_NO_NEXT_CHAR}, L{REVIEW_NO_NEXT_ITEM}, 
      L{REVIEW_OK}, L{REVIEW_WRAP}
    - peek: L{POR} of next character if L{peek} is True
  '''
  def execute(self, **kwargs):
    self.stopNow()
    # try to get the next character
    por = self.getNextChar()
    if por is not None:
      # indicate we moved to the next character in the current item
      self.moveToPOR(por)
      self.setTempVal('review', REVIEW_OK)
      return
    if not self.perk_state.Wrap:
      # indicate a next char exists, but we can't wrap to it because of the
      # current settings
      self.setTempVal('review', REVIEW_NO_NEXT_CHAR)
      return
    # get the first char of the next item
    por = self.getNextItem(wrap=True, only_visible=self.perk_state.OnlyVisible)
    if por is None:
      # indicate this is the last item
      self.setTempVal('review', REVIEW_NO_NEXT_ITEM)
      return
    # move to the first character of the next item
    self.moveToPOR(por)
    # indicate we moved to the first character in the next item
    self.setTempVal('review', REVIEW_WRAP)

class FocusToPOR(Task.InputTask):
  '''
  Attempts to focus, select, and move the caret to the pointer POR by default
  or the given L{POR} if not None.
  '''
  def execute(self, por=None, **kwargs):
    self.stopNow()
    self.setAccPOR(por=por)

class PointerToPOR(Task.InputTask):
  '''
  Moves the pointer to the location of the last focus, selector, or caret
  event by default or the given L{POR} if not None.
  '''
  def execute(self, por=None, **kwargs):
    if por is None:
      self.moveToPOR(self.getAccFocus())
    else:
      self.moveToPOR(por)
    self.setTempVal('review', REVIEW_OK)

class MouseToPOR(Task.InputTask):
  '''
  Move the mouse pointer to the location of the pointer POR by default or
  the given L{POR} if not None.
  '''
  def execute(self, por=None, **kwargs):
    # do an absolute movement to the location
    self.mouseEventPOR(AEConstants.EVENT_SYNTHMOUSE_ABS, por=por)
    
class SkipItem(Task.Task):
  '''
  Does nothing. Exists so that other Perks can register Tasks to respond to 
  skipped items.
  '''
  pass

class ShouldSkip(Task.InputTask):
  '''
  Determines if the current L{POR} should be skipped or not based on the
  user setting for Skipping. Executes a L{Task} named 'review skip report' when
  Skipping is set to L{SKIP_REPORT_IMPORTANT} or L{SKIP_REPORT_ALL}.
  
  @param peek: When set, do not 'review skip report' since we are looking to
    preview whether or not an item will be skipped. We do not want chained 
    L{Task}s to execute and announce any information in this case.
  @type peek: boolean
  @param por: Point of regard to test
  @type por: L{POR}
  '''
  def execute(self, peek, por=None, **kwargs):
    text = self.getItemText(por)
    if text.strip() or self.hasOneAccState(por, 'focusable', 'editable'):
      # has content or can have content from the user, don't skip
      self.setTempVal('should skip', False)
      return 
    if self.perk_state.Skipping == SKIP_ALL:
      self.setTempVal('should skip', True)
    elif self.perk_state.Skipping == SKIP_NONE:
      self.setTempVal('should skip', False)
    elif self.perk_state.Skipping == SKIP_REPORT:
      if not peek:
        # update the Perk object's task_por
        self.moveToPOR(por)
        # then invoke the review report task
        self.doTask('review skip report')
        # indicate something has been skipped and reported
        self.setTempVal('has skipped', True)        
      else:
        # indicate something will be skipped and reported
        self.setTempVal('will skip', True)
      self.setTempVal('should skip', True)

