'''
Defines tools for assisting L{Perk} developers.

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
# import useful modules for Perks
import Perk, Task
from POR import POR
from i18n import _

__uie__ = dict(kind='perk', all_tiers=True)

class DeveloperPerk(Perk.Perk):
  '''  
  Defines keyboard commands for Perk developers that report all Perks in the
  current Tier, refresh all Perks in the current Tier, show/hide all
  monitors registered in the current profile, and mute/unmute all output.
  '''
  def init(self):
    '''
    Registers keys for all L{Task}s in the module.
    '''
    # set an audio device as the default output
    self.setPerkIdealOutput('audio')
    
    # register keyboard bindings
    kbd = self.getInputDevice(None, 'keyboard')
    # set Alt+CapsLock as the modifier
    self.addInputModifiers(kbd, kbd.AEK_ALT_L, kbd.AEK_ALT_R, 
                           kbd.AEK_CAPS_LOCK, kbd.AEK_CONTROL_R, 
                           kbd.AEK_CONTROL_L)
  
    # register named tasks
    self.registerTask(FocusDebug('developer focus debug',
                                 focus=True, tier=True, background=True))
    self.registerTask(ViewDebug('developer view debug'))
    self.registerTask(SayPerks('developer say perks'))
    self.registerTask(ReloadPerks('developer reload perks'))
    self.registerTask(ShowHideMonitors('developer toggle monitors'))
    self.registerTask(Mute('developer toggle mute'))
    
    # map named tasks to commands
    self.registerCommand(kbd, 'developer toggle mute', False,
                         [kbd.AEK_CONTROL_R, kbd.AEK_CONTROL_L])    
    
    pairs = [[kbd.AEK_ALT_L, kbd.AEK_CAPS_LOCK],
             [kbd.AEK_ALT_R, kbd.AEK_CAPS_LOCK]]
    for pair in pairs:
      self.registerCommand(kbd, 'developer say perks', False, pair+[kbd.AEK_J])
      self.registerCommand(kbd, 'developer reload perks', False, 
                           pair+[kbd.AEK_K])
      self.registerCommand(kbd, 'developer toggle monitors', False, 
                           pair+[kbd.AEK_L])
      
  def getName(self):
    return _('Developer tools')

class Mute(Task.InputTask):
  '''
  Task to mute output indefinitely or unmute it if it has already been muted.
  '''
  def execute(self, **kwargs):
    self.stopAll()
    mute = self.getStyleVal('Mute') 
    if mute:
      self.setStyleVal('Mute', False)
      self.sayInfo(text=_('unmuted'))
    else:
      self.sayInfo(text=_('muted'))
      self.setStyleVal('Mute', True)

class SayPerks(Task.InputTask):
  '''
  Says the number of L{Perk}s loaded on the active L{Tier} followed by their
  names.
  '''
  def execute(self, **kwargs):
    '''
    Stops speech then makes the announcement.
    '''
    self.stopNow()
    names = self.getPerkNames()
    n = len(names)
    # i18n: %d is number of perks, %s is application name
    self.sayInfo(text=(n, self.getAppName()), template=_('%d perks in %s.'))
    self.sayInfo(text=', '.join(names))

class ReloadPerks(Task.InputTask):
  '''
  Reloads all L{Perk}s in the current L{Tier}. Useful during L{Perk} 
  development when changes have been made to a L{Perk} and those changes should
  be tested without restarting LSR.
  '''
  def execute(self, **kwargs):
    '''
    Reloads L{Perk}s then makes an announcement.
    '''
    self.stopNow()
    # i18n: %s is application name
    self.sayInfo(text=self.getAppName(), template=_('reloaded perks in %s'))
    self.reloadPerks()

class ShowHideMonitors(Task.InputTask):
  '''
  Shows all monitors associated with the current profile if all are hidden.
  Hides all monitors associated with the current profile if any one is shown.
  '''
  def execute(self, **kwargs):
    '''
    Reloads L{Perk}s then makes an announcement.
    '''
    self.stopNow()
    self.inhibitMayStop()
    if not self.loadAllMonitors():
      self.unloadAllMonitors()
      self.sayInfo(text=_('hiding monitors'))
    else:
      self.sayInfo(text=_('showing monitors'))
      
class FocusDebug(Task.FocusTask):
  '''
  Prints the focus L{POR}.
  '''
  def executeGained(self, por, **kwargs):
    print 'focus:', por
    
  def executeLost(self, por, **kwargs):
    print 'unfocus:', por
    
class ViewDebug(Task.ViewTask):
  '''
  Prints the view L{POR}.
  '''
  def execute(self, por, gained, **kwargs):
    print 'view:', por, gained