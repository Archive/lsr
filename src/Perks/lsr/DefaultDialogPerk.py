'''
Defines L{Task}s for managing the chooser dialogs packaged with the LSR core.

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
# import useful modules for Perks
import Perk, Task, AEConstants
from POR import POR
from i18n import _

__uie__ = dict(kind='perk', all_tiers=True)

class DefaultDialogPerk(Perk.Perk):
  '''
  Manages the basic LSR dialogs for configuring settings and loading Perks.
  
  Shows and hides default LSR chooser dialogs for configuring LSR settings,
  loading and unloading extensions at run time, and so on. Tasks for starting 
  choosers are defined, but not mapped to any particular input device. Some
  other Perk must define the input mappings.
  '''
  def init(self):
    '''
    Registers named tasks for the following choosers:
    
      - show perk chooser
      - show settings chooser
    '''
    self.registerTask(PerkChooserTask('show perk chooser'))
    self.registerTask(SettingsChooserTask('show settings chooser'))
    
  def getName(self):
    return _('Basic dialogs')
    
class SettingsChooserTask(Task.ChooserTask):
  '''  
  Shows the settings chooser which allows a user to configure LSR devices,
  Perks, profiles, and general system settings.
  
  @ivar installed: All UIE class names, human readable names, and 
    descriptions installed keyed by their type
  @type installed: dictionary of string : list of 3-tuple of string
  @ivar associated: All UIE class names, human readable names, and 
    descriptions associated with this profile keyed by their type
  @type associated: dictionary of string : list of 3-tuple of string
  '''
  def init(self):
    '''
    Initialize installed, associated, and chooser variables.
    '''
    self.installed = None
    self.associated = None
    
  def executeStart(self, timestamp, **kwargs):
    '''
    Load the L{SettingsChooser} and give it the required settings model for
    the system, L{Perk}s, devices, and profile.
    
    @param timestamp: Time to provide to the chooser dialog so it can raise 
      itself to the foreground properly
    @type timestamp: float

    '''
    # get profile information
    self._getProfileInfo()
    profile = self.getProfileName()
    # get the LSR system settings and save state to support undo
    system = self.lsr_state
    system.save()
    
    # get metadata of all perks in this profile and currently loaded
    # can't just get names because ordering is lost when using sets to filter
    # duplicates
    md = set(self.getPerkMetadata(AEConstants.UIE_ALL_ASSOCIATED))
    md.update(self.getPerkMetadata(AEConstants.UIE_LOADED))
    # load state for each named perk
    states = [(name, self.getPerkState(cls_name)) 
              for (cls_name, name, desc) in md]
    perks = self._readyStates(states)
    
    # get names of all devices currently loaded
    names = self.getDeviceClassNames(AEConstants.UIE_LOADED)
    # load state for each named device
    states = [self.getDeviceState(name) for name in names]
    states = zip(self.getDeviceNames(AEConstants.UIE_LOADED), states)
    devices = self._readyStates(states)

    # load and show the chooser
    chooser = self.loadChooser('SettingsChooser', system=system, 
                               perks=perks, associated=self.associated, 
                               installed=self.installed, 
                               profile=profile, devices=devices, 
                               timestamp=timestamp)

  def _getProfileInfo(self):
    '''
    Populates the installed and associated profile metadata dictionary.
    '''
    self.installed = {
      'perks' : self.getPerkMetadata(AEConstants.UIE_INSTALLED),
      'devices' : self.getDeviceMetadata(AEConstants.UIE_INSTALLED),
      'monitors' : self.getMonitorMetadata(AEConstants.UIE_INSTALLED),
      'choosers' : self.getChooserMetadata(AEConstants.UIE_INSTALLED)}
    
    self.associated = {
      'perks' : self.getPerkMetadata(AEConstants.UIE_ALL_ASSOCIATED),
      'devices' : self.getDeviceMetadata(AEConstants.UIE_ALL_ASSOCIATED),
      'monitors' : self.getMonitorMetadata(AEConstants.UIE_ALL_ASSOCIATED),
      'choosers' : self.getChooserMetadata(AEConstants.UIE_ALL_ASSOCIATED)}
    
  def _readyStates(self, states):
    '''
    Prepare settings objects by storing a copy of the current settings so 
    that they may be restored at a later time.
    
    @param states: States paired with the names of the UIEs having those
      states
    @type states: list of 2-tuple of string, L{AEState}
    @return: Dictionary pairing UIE name with its state object
    @rtype: dictionary
    '''
    uies = {}
    for name, state in states:
      try:
        state.save()
      except AttributeError:
        # ignore missing states
        continue
      state.save()
      uies[name] = state
    return uies

  def executeSignal(self, chooser, kind, layer, **kwargs):
    '''    
    Runs when the chooser sends a signal to this L{Perk} indicating some
    change in the chooser that the L{Perk} should be aware of. Signals of
    interest defined by this particular chooser include:
    
      - OK: Complete the dialog accepting current values
      - APPLY: Accept current values without completing
      - RAISE: Move a L{UIElement} up in the load order
      - LOWER: Move a L{UIElement} down in the load order
      - TO_LOAD: Add a L{UIElement} to the list to load
      - TO_UNLOAD: Remove a L{UIElement} from the list to load
      
    @param chooser: Reference to the chooser object that sent the signal
    @type chooser: L{AEChooser}
    @param kind: Signal name
    @type kind: string
    @param layer: Ignored. Pulled out of kwargs so they can be passed to
      L{doTask} without modification
    @type layer: integer
    @param kwargs: Keyword arguments specific to each signal type
    @type kwargs: dictionary
    '''
    # make changes first, then do announcements in case a new device is loaded
    if kind == chooser.APPLY:
      self.doApply(chooser, **kwargs)
    elif kind == chooser.OK:
      self.doApply(None, **kwargs)
    elif kind == chooser.CANCEL:
      # undo any setting changes
      self.doCancel(**kwargs)

    # stop speech and make a model-based announcement
    self.stopNow()
    # inhibit the next event from stopping us (possibly a selection)
    self.inhibitMayStop()      
    if kind == chooser.APPLY:
      self.doTask('read message', text=_('applied changes to settings'))
    elif kind == chooser.RAISE:
      text = _('raised %(name)s above %(below)s, below %(above)s') % kwargs
      self.doTask('read message', text=text)
    elif kind == chooser.LOWER:
      text = _('lowered %(name)s below %(above)s, above %(below)s') % kwargs
      self.doTask('read message', text=text)
    elif kind == chooser.TO_LOAD:
      text = _('added %(name)s as first') % kwargs
      self.doTask('read message', text=text)
    elif kind == chooser.TO_UNLOAD:
      text = _('removed %(name)s') % kwargs
      self.doTask('read message', text=text)

  def doApply(self, chooser, system, perks, devices, associated, 
              installed=None, **kwargs):
    '''
    Apply all system, L{Perk}, and device settings. Make any necessary changes
    to the current profile and install/uninstall any L{UIElement}s.
        
    @param chooser: Reference to the chooser object that sent the signal
    @type chooser: L{AEChooser}
    @param system: System state
    @type system: L{AEState}
    @param perks: L{Perk} state to apply
    @type perks: list of L{AEState}
    @param devices: Device state to apply
    @type devices: list of L{AEState}
    @param installed: All UIE class names selected for installation keyed by 
      their type
    @type installed: dictionary of string : list of string
    @param associated: All UIE class names selected for association with this
      profile keyed by their type
    @type associated: dictionary of string : list of string
    '''
    # save all runtime settings
    system.save()
    [state.save() for state in perks.values()]
    [state.save() for state in devices.values()]
    # task tools to use to do associations and disassociations
    tools = {'perks' : (self.associatePerk, self.disassociatePerk),
             'monitors' : (self.associateMonitor, self.disassociateMonitor),
             'devices' : (self.associateDevice, self.disassociateDevice)}
    # compare current and saved associations
    for kind in associated:
      new = associated[kind]
      old = [metadata[0] for metadata in self.associated[kind]]
      to_remove = set(old) - set(new)
      to_add = set(new) - set(old)
      # do removals first
      map(tools[kind][1], to_remove)
      if kind == 'devices':
        # reassociate all devices to get the order right
        map(tools[kind][0], new)
        # reload devices according to new preferences
        self.refreshDevices()
        if chooser is not None:
          # update the chooser with the list of new devices
          # get names of all devices currently loaded
          names = self.getDeviceClassNames(AEConstants.UIE_LOADED)
          # load state for each named device
          states = [self.getDeviceState(name) for name in names]
          states = zip(self.getDeviceNames(AEConstants.UIE_LOADED), states)
          devices = self._readyStates(states)
          chooser.updateDevices(devices)
      else:
        # do new associations without regard to order for non-devices
        map(tools[kind][0], to_add)
    # rebuild lists of installed and associated
    self._getProfileInfo()
      
  def doCancel(self, system, perks, devices, **kwargs):
    '''
    Cancel all system, L{Perk}, and device setting changes. Ignore changes
    to the current profile.
    
    @param system: System state
    @type system: L{AEState}
    @param perks: L{Perk} state to apply
    @type perks: list of L{AEState}
    @param devices: Device state to apply
    @type devices: list of L{AEState}
    '''
    system.restore()
    for state in perks.values():
      state.restore()
    for state in devices.values():
      state.restore()
        
class PerkChooserTask(Task.ChooserTask):
  '''  
  Shows the Perk chooser which allows a user to pick Perks to load and unload
  from the current application at runtime.
  '''
  def executeStart(self, timestamp, **kwargs):
    '''
    Runs when an input gesture is given to start the chooser. Buids lists of
    currently loaded and available L{Perk}s, shows the chooser, and provides it
    with the lists.
    
    @param timestamp: Time to provide to the chooser dialog so it can raise 
      itself to the foreground properly
    @type timestamp: float
    '''
    # get programmatic and human readable names and description
    loaded = self.getPerkMetadata()
    # put the Perks in execution order, not load order
    #loaded.reverse()
    installed = self.getPerkMetadata(AEConstants.UIE_INSTALLED)
    # compute the difference of the two lists using sets
    unloaded = list(set(installed) - set(loaded))
    # sort the unloaded in alphabetical order
    unloaded.sort()
    # load the chooser and give it the two sets
    self.loadChooser('PerkChooser', loaded=loaded, unloaded=unloaded, 
                     app_name=self.getAppName(), timestamp=timestamp)                     
    
  def executeSignal(self, chooser, kind, layer, **kwargs):
    '''    
    Runs when the chooser sends a signal to this L{Perk} indicating some
    change in the dialog that the L{Perk} should be aware of. Signals of
    interest defined by this particular chooser include:
    
      - OK: Complete the dialog accepting current load/unload configuration
      - APPLY: Accept current load/unload configuration without completing
      - RAISE: Move a L{Perk} up in the load order
      - LOWER: Move a L{Perk} down in the load order
      - TO_LOAD: Add a L{Perk} to the list of L{Perk}s to load
      - TO_UNLOAD: Remove a L{Perk} from the list of L{Perk}s to load
      
    @param chooser: Reference to the chooser object that sent the signal
    @type chooser: L{AEChooser}
    @param kind: Signal name
    @type kind: string
    @param layer: Ignored. Pulled out of kwargs so they can be passed to
      L{doTask} without modification
    @type layer: integer
    @param kwargs: Keyword arguments specific to each signal type
    @type kwargs: dictionary
    '''
    # stop speech and make a model-based announcement
    self.stopNow()
    # inhibit the next event from stopping us (possibly a selection)
    self.inhibitMayStop()
    if kind == chooser.APPLY:
      self.doTask('read message', text=_('applied changes to %s') % 
                  self.getAppName())
    elif kind == chooser.RAISE:
      text = _('raised %(name)s above %(below)s, below %(above)s') % kwargs
      self.doTask('read message', text=text)
    elif kind == chooser.LOWER:
      text = _('lowered %(name)s below %(above)s, above %(below)s') % kwargs
      self.doTask('read message', text=text)
    elif kind == chooser.TO_LOAD:
      kwargs['app'] = self.getAppName()
      text = _('added %(name)s to %(app)s as first') % kwargs
      self.doTask('read message', text=text)
    elif kind == chooser.TO_UNLOAD:
      kwargs['app'] = self.getAppName()
      text = _('removed %(name)s from %(app)s') % kwargs
      self.doTask('read message', text=text)

    if kind in (chooser.OK, chooser.APPLY):
      # load and unload Perks
      self.doPerkLoad(**kwargs)

    
  def doPerkLoad(self, loaded, unloaded, **kwargs):
    '''
    Loads and unloads L{Perk}s according to the current state of the chooser
    dialog.
    
    @param loaded: Names of L{Perk}s to load on this L{Tier}
    @type loaded: list
    @param unloaded: Names of L{Perk}s not to load on this L{Tier}
    @type unloaded: list
    '''
    # get the names of all Perks loaded on this Tier
    curr_loaded = self.getPerkClassNames()
    # compute the difference between those that should be loaded according 
    # to the chooser and those that are currently loaded in the Tier
    to_load = set(loaded) - set(curr_loaded)
    # compute the intersection between those that are loaded in the Tier and 
    # those that should be unloaded according to the chooser
    to_unload = set(curr_loaded).intersection(unloaded)
    # get rid of perks to unload first
    indices = [curr_loaded.index(perk) for perk in to_unload]
    self.popPerk(*indices)
    # now load new perks in the proper order
    for perk in to_load:
      # get the proper index for this perk
      i = loaded.index(perk)
      # insert it into the perk stack
      self.insertPerk(i, perk)
