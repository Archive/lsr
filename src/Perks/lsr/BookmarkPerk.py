'''
Defines a L{Perk} that keeps track of user assigned bookmarks to L{POR}s in 
an application.

Steps for tying an input gesture to a bookmark location and another gesture 
to jump to the bookmark stored at the new location:
  
1)  Do the following to bind a gesture that adds a bookmark :
    self.doTask('bookmark reg add gesture',dev=dev, gesture=gesture, 
    name=aname); where 'dev' is an L{AEInput} object, 'gesture' is a list of 
    key commands and 'name' is the name of the bookmark.  Please note that
    'name' will be announced during the bookmark review procedure.
2)  Do the following to bind a gesture that goes to a bookmark 
    self.doTask('bookmark reg goto gesture',dev=dev, gesture=gesture, 
    name=aname); where 'dev' is an L{AEInput} object, 'gesture' is a list of 
    key commands and 'name' is the name of the bookmark used in step 1.  

@author: Scott Haeger
@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
# import useful modules for Perks
import Perk, Task
from POR import POR
from i18n import _
from AEConstants import *

__uie__ = dict(kind='perk', tier=None, all_tiers=True)

class BookmarkPerk(Perk.Perk):
  '''
  Defines a user interface into a collection of bookmarked L{POR}s.
  
  @ivar bookmarks: Collection of bookmarks where bookmark name is the key and
  the value is a dictionary containing associated 'add' gestures ('gestures') 
  and bookmarked POR ('por').
  @type bookmarks: dictionary
  @ivar goto_gestures: collection of 'goto' gestures where bookmark name is key
  and value is a list of 'goto' gestures.
  @type goto_gestures: dictionary
  
  '''
  def init(self):
    self.bookmarks = {}
    self.goto_gestures = {}
    self.whereami_gestures = {}
    self.bm_compare_gestures = {}
        
    self.registerTask(HandleAddBookmark('bookmark add bookmark'))
    self.registerTask(HandleGotoBookmark('bookmark goto bookmark'))
    self.registerTask(HandleReviewBookmark('bookmark review bookmark'))
    self.registerTask(HandleBookmarkWhereAmI('bookmark where am i'))
    self.registerTask(HandleCompareBookmark('bookmark compare bookmark'))
    
    self.registerTask(RegBookmarkAddGesture('bookmark reg add gesture'))
    self.registerTask(RegBookmarkGotoGesture('bookmark reg goto gesture'))
    self.registerTask(RegBookmarkWhereAmIGesture('bookmark reg where am i gesture'))
    self.registerTask(RegBookmarkCompareGesture('bookmark reg bm compare gesture'))
    
 
    # register keyboard bindings
    kbd = self.getInputDevice(None, 'keyboard')
    # set Alt+CapsLock as a modifier
    self.addInputModifiers(kbd, kbd.AEK_ALT_L, kbd.AEK_ALT_R, 
                           kbd.AEK_CAPS_LOCK)
    # set Alt+Shift as a modifier
    self.addInputModifiers(kbd, kbd.AEK_ALT_L, kbd.AEK_SHIFT_L, kbd.AEK_ALT_R,
                           kbd.AEK_SHIFT_R)
                                                  
    self.keys = [kbd.AEK_2, kbd.AEK_3, kbd.AEK_4, kbd.AEK_5, kbd.AEK_6,
                 kbd.AEK_7, kbd.AEK_8, kbd.AEK_9, kbd.AEK_0]
    modifiers = [[kbd.AEK_ALT_L, kbd.AEK_CAPS_LOCK],
                 [kbd.AEK_ALT_R, kbd.AEK_CAPS_LOCK]]
    keynames = ['keyboard 2', 'keyboard 3', 'keyboard 4', 'keyboard 5',
                'keyboard 6', 'keyboard 7', 'keyboard 8', 'keyboard 9',
                'keyboard 0']
    
    # Register for keyboard input command Alt-CapsLock-(2-0).
    # Keyboard keys 2 - 0 (9 keys in kb layout order)
    # Adds bookmark          
    for modifier in modifiers:
      for i in range(len(self.keys)):
        self.doTask('bookmark reg add gesture', 
              dev=kbd, gesture=modifier+[self.keys[i]], name=keynames[i]) 

    # Register for keyboard input command Alt-Shift-tilde.
    # Report all bookmarks
    for modifier in modifiers:
      self.registerCommand(kbd, 'bookmark review bookmark', False, 
           modifier+[kbd.AEK_TILDE])
      
    # Register for keyboard input command CapsLock-(2-0).
    # Keyboard keys 2 - 0 (9 keys in kb layout order)
    # Move pointer to bookmark
    for i in range(len(self.keys)):
      self.doTask('bookmark reg goto gesture',
                  dev=kbd, gesture=[kbd.AEK_CAPS_LOCK]+[self.keys[i]],
                  name=keynames[i])

    # Register for keyboard input command Alt-Shift-(2-0).
    # Keyboard keys 2 - 0 (9 keys in kb layout order)
    # Report where am I relative to root
    modifiers = [[kbd.AEK_ALT_L, kbd.AEK_SHIFT_L], 
                [kbd.AEK_ALT_R, kbd.AEK_SHIFT_R]]
    for modifier in modifiers:
      for i in range(len(self.keys)):
        self.doTask('bookmark reg where am i gesture', 
                    dev=kbd, gesture=modifier+[self.keys[i]],
                    name=keynames[i])

    # Register for keyboard input command Alt-CapsLock-Shift-(2-0).
    # Keyboard keys 2 - 0 (9 keys in kb layout order)
    # Report where am I relative to bookmark
    modifiers = [[kbd.AEK_ALT_L, kbd.AEK_SHIFT_L, kbd.AEK_CAPS_LOCK], 
                [kbd.AEK_ALT_R, kbd.AEK_SHIFT_R, kbd.AEK_CAPS_LOCK]]
    for modifier in modifiers:
      for i in range(len(self.keys)):
        self.doTask('bookmark reg bm compare gesture', 
              dev=kbd, gesture=modifier+[self.keys[i]], name=keynames[i])
        
      
  def sayBookmark(self, por):
    '''
    Speaks information about the given L{POR} using an audio output device.
    
    @param por: Point of regard for a bookmark
    @type por: L{POR}
    '''
    # move POR to the given bookmark
    self.moveToPOR(por)
    # speak the details about the given bookmark
    self.doTask('read item details', text=self.getItemText(por=por))
   
  def getName(self):
    return _('Bookmarks')
  
  def getDescription(self):
    return _('Allows the user to bookmark locations in an application and '
             'return to them using hotkeys at a later time.')
  
class HandleAddBookmark(Task.CyclicInputTask):
  '''
  Adds a new bookmark. If a current, different bookmark will be overwritten, 
  warns the user first and requires they activate this task again.
  '''
  def execute(self, cycle_count, gesture=None, **kwargs):
    if gesture is None: return
    self.stopNow()
    # get sorted list of the keys associated with this gesture
    sorted_gest = gesture.getSortedActionCodes()
    # get the bookmark name that is used as index
    bm_index = self._reverseLookup(sorted_gest)
    # safety
    if bm_index is None:
      return
    
    if cycle_count == 0:
      # already a bookmark.  give user a warning
      if self.perk.bookmarks[bm_index]['por']:
        self.sayInfo(text=_('activate again to overwrite bookmark %s') %
                     (bm_index))
      # no bookmark registered so add it and tell user
      else:
        self.sayInfo(text=_('adding bookmark %s') %(bm_index))
        self.perk.bookmarks[bm_index]['por'] = self.task_por
    # same por is bookmarked.  no need to add again
    elif self.perk.bookmarks[bm_index]['por'] == self.task_por:
      self.sayInfo(text=_('already bookmarked'))
    else:
      # user wants you to overwrite
      self.sayInfo(text=_('overwriting bookmark %s.') %(bm_index))
      self.perk.bookmarks[bm_index]['por'] = self.task_por
      
  def _reverseLookup(self, gesture):
    '''
    Does a reverse lookup on bookmarks using given gesture
    '''
    for name, value in self.perk.bookmarks.iteritems():
      for g in value['gestures']:
        if g == gesture:
          return name
    return None    
    
class HandleGotoBookmark(Task.InputTask):
  '''
  Triggers a 'pointer to por' event using the bookmarked L{POR}.
  '''
  def execute(self, gesture=None, **kwargs):
    if gesture is None: return
    # get sorted list of the keys associated with this gesture
    sorted_gest = gesture.getSortedActionCodes()
    self.stopNow()
    # get the bookmark name that is used as index
    bm_index = self._reverseLookup(sorted_gest)
    # make error announcement if no registered bookmark for this gesture
    if bm_index is None or self.perk.bookmarks[bm_index]['por'] is None:
      self.sayError(text=_('no bookmark registered'))
    else:
      # change pointer to bookmarked por and announce new pointer location
      self.doTask('pointer to por', por=self.perk.bookmarks[bm_index]['por'])
      
  def _reverseLookup(self, gesture):
    '''
    Does a reverse lookup on goto_gestures using given gesture.
    '''
    for name, value in self.perk.goto_gestures.iteritems():
      for g in value:
        if g == gesture:
          return name
    return None    
      
      
class HandleReviewBookmark(Task.CyclicInputTask):
  '''
  Reviews all bookmarks saved in the current application.
  '''
  def execute(self, cycle_count, gesture=None, **kwargs):
    self.stopNow()
    
    # announce registered bookmarks
    if cycle_count == 0:
      # See if there are any bookmarks registered.  
      havebookmark = False
      for value in self.perk.bookmarks.values():
        if value['por'] is not None:
          havebookmark = True
          break
      # make announcement if there are no bookmarks registered
      if not havebookmark:
        self.sayInfo(text=_("no bookmarks registered"))
      # announce any registered bookmarks
      else:
        bm_keys = self.perk.bookmarks.keys()
        bm_keys.sort()
        for key in bm_keys:
          if self.perk.bookmarks[key]['por'] is not None:
            self.sayInfo(text=_(key), talk=False)
            self.perk.sayBookmark(self.perk.bookmarks[key]['por'])
    # announce available bookmarks       
    else:
      haveavailable = False
      for value in self.perk.bookmarks.values():
        if value['por'] is None:
          haveavailable = True
          break
      # make announcement if there are no open bookmarks
      if not haveavailable:
        self.sayInfo(text=_("no open bookmarks"))
      else:
        bm_keys = self.perk.bookmarks.keys()
        bm_keys.sort()
        outtext = []
        for key in bm_keys:
          if self.perk.bookmarks[key]['por'] is None:
            outtext.append(_(key))
        self.sayInfo(text=_("Bookmarks %s are available.") % ', '.join(outtext))
            
class RegBookmarkAddGesture(Task.Task):
  '''
  Register a named 'add' bookmark gesture.
  
  @param dev: An input device
  @type dev: L{AEInput}
  @param gesture: List of keys that represent a gesture.
  @type gesture: list
  @param name: Point of regard for a bookmark
  @type name: string  
  '''
  def execute(self, dev=None, gesture=None, name=None, **kwargs):
    if dev is None or gesture is None or name is None:
      return
    # register the gesture to trigger the proper task
    self.registerCommand(dev, 'bookmark add bookmark', False, gesture)
    # sort gesture for comparison operation performed later 
    gesture.sort()
    # add to bookmarks data structure
    if not self.perk.bookmarks.has_key(name):
      self.perk.bookmarks[name] = {'gestures':[gesture], 'por':None}
    else:
      self.perk.bookmarks[name]['gestures'].append(gesture)
          
    
class RegBookmarkGotoGesture(Task.Task):
  '''
  Register a named 'goto' bookmark gesture.
  
  @param dev: An input device
  @type dev: L{AEInput}
  @param gesture: List of keys that represent a gesture.
  @type gesture: list
  @param name: Point of regard for a bookmark
  @type name: string  
  '''
  def execute(self, dev=None, gesture=None, name=None, **kwargs):
    if dev is None or gesture is None or name is None:
      return
    # register the gesture to trigger the proper task
    self.registerCommand(dev, 'bookmark goto bookmark', False, gesture)
    # sort gesture for comparison operation performed later 
    gesture.sort()
    # add to goto_gestures data structure
    if not self.perk.goto_gestures.has_key(name):
      self.perk.goto_gestures[name] = [gesture]
    else:
      self.perk.goto_gestures[name].append(gesture)
      
class RegBookmarkWhereAmIGesture(Task.Task):
  '''
  Register a named 'where am i' bookmark gesture.
  
  @param dev: An input device
  @type dev: L{AEInput}
  @param gesture: List of keys that represent a gesture.
  @type gesture: list
  @param name: Point of regard for a bookmark
  @type name: string  
  '''
  def execute(self, dev=None, gesture=None, name=None, **kwargs):
    if dev is None or gesture is None or name is None:
      return
    # register the gesture to trigger the proper task
    self.registerCommand(dev, 'bookmark where am i', False, gesture)
    # sort gesture for comparison operation performed later 
    gesture.sort()
    # add to goto_gestures data structure
    if not self.perk.whereami_gestures.has_key(name):
      self.perk.whereami_gestures[name] = [gesture]
    else:
      self.perk.whereami_gestures[name].append(gesture)
    
   
class HandleBookmarkWhereAmI(Task.InputTask):
  '''
  Reports the location of the selected bookmark by announcing all control and
  container names in child-to-parent order.
  '''
  def execute(self, gesture=None, **kwargs):
    if gesture is None: return
    sorted_gest = gesture.getSortedActionCodes()
    self.stopNow()
    # get the bookmark name that is used as index
    bm_index = self._reverseLookup(sorted_gest)
    # make error announcement if no registered bookmark for this gesture
    if bm_index is None or self.perk.bookmarks[bm_index]['por'] is None:
      self.sayError(text=_('no bookmark registered'))
    else:
      currentpor = self.task_por
      self.moveToPOR(self.perk.bookmarks[bm_index]['por'])
      self.doTask('where am i')
      self.moveToPOR(currentpor)
    
  def _reverseLookup(self, gesture):
    '''
    Does a reverse lookup on whereami_gestures using given gesture.
    '''
    for name, value in self.perk.whereami_gestures.iteritems():
      for g in value:
        if g == gesture:
          return name
    return None 
    
    
class RegBookmarkCompareGesture(Task.Task):
  '''
  Register a named 'compare bookmark' bookmark gesture.
  
  @param dev: An input device
  @type dev: L{AEInput}
  @param gesture: List of keys that represent a gesture.
  @type gesture: list
  @param name: Point of regard for a bookmark
  @type name: string  
  '''
  def execute(self, dev=None, gesture=None, name=None, **kwargs):
    if dev is None or gesture is None or name is None:
      return
    # register the gesture to trigger the proper task
    self.registerCommand(dev, 'bookmark compare bookmark', False, gesture)
    # sort gesture for comparison operation performed later 
    gesture.sort()
    # add to goto_gestures data structure
    if not self.perk.bm_compare_gestures.has_key(name):
      self.perk.bm_compare_gestures[name] = [gesture]
    else:
      self.perk.bm_compare_gestures[name].append(gesture)    
    
    
class HandleCompareBookmark(Task.InputTask):
  '''
  Compares the position of a given bookmark to that of the current task_por and
  announces comparison information.  ie. 'same POR', 'same container' 
  '''
  def execute(self, gesture=None, **kwargs):
    if gesture is None: return
    sorted_gest = gesture.getSortedActionCodes()
    self.stopNow()
    # get the bookmark name that is used as index
    bm_index = self._reverseLookup(sorted_gest)
    # make error announcement if no registered bookmark for this gesture
    if bm_index is None or self.perk.bookmarks[bm_index]['por'] is None:
      self.sayError(text=_('no bookmark registered'))
    else:
      bm_por = self.perk.bookmarks[bm_index]['por']
      
      # Begin a series of comparison tests from most specific to most general
      # Are they the same POR ?
      if bm_por == self.task_por:
        self.sayInfo(text=_('bookmark equals pointer'))
        return
      # Do they share the same parent ?
      parent = self.getParentAcc()
      if parent == self.getParentAcc(por=bm_por):
        self.sayInfo(text=_('bookmark and pointer have same parent'))
        self.perk.sayBookmark(parent)
        return
      # Is bookmark in the same container as task_por ?
      taskpor_ancestors = {}
      for p in self.iterAncestorAccs():
        taskpor_ancestors[p] = None
      #next do same for bookmark and compare to task_por's ancestors
      for p in self.iterAncestorAccs(por=bm_por):
        if taskpor_ancestors.has_key(p):
          self.sayInfo(text=_('bookmark in same container'))
          self.perk.sayBookmark(p)
          return
        
      # Last resort.  Are they within the same application ?  
      if self.getAppName() == self.getAppName(por=bm_por):
        self.sayInfo(text=_('bookmark and pointer are in the same application'))
        return
      # gotta say something
      self.sayInfo(text=_('comparison unknown'))
    
  def _reverseLookup(self, gesture):
    '''
    Does a reverse lookup on bm_compare_gestures using given gesture.
    '''
    for name, value in self.perk.bm_compare_gestures.iteritems():
      for g in value:
        if g == gesture:
          return name
    return None    
