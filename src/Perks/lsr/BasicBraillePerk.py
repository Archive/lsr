'''
Provides basic Braille services.

@author: Scott Haeger
@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''

# import useful modules for Perks
import Perk, Task.Tools
from Task.Tools.Error import *
from POR import POR
from i18n import bind, _
from AEConstants import *


# metadata describing this Perk
__uie__ = dict(kind='perk', tier=None, all_tiers=True)

class BasicBraillePerkState(Perk.PerkState):
  '''
  Settings for BasicBraillePerk
  
  Overlap (integer): Number of orig. chars remaining on page forward/backward
  
  LeftPadding (integer): remaining cells on left side during scrolling
  
  RightPadding (integer): remaining cells on right side during scrolling
  
  SnapOnUpdate (bool): snap braille display to previous location upon update?
  
  SnapToWord (bool): snap braille display to nearest word upon paging?
  @ivar BrailleAvailable: Is Braille device available?
  @type BrailleAvailable: boolean
  '''
  def init(self):
    '''
    Initializes setting objects.
    '''
    self.newBool('SnapOnUpdate', False, _('Snap on update?'), 
                _('When set, display snaps to caret location on any event '
                  'after scrolling.'))
    self.newNumeric('Overlap', 0, _('Overlapping cells'), 0, 10, 0)
    self.newBool('SnapToWord', False, _('Snap to word?'), 
                _('When set, display snaps to nearest word during display '
                  'paging.'))
    self.newNumeric('LeftPadding', 0, _('Left padding'), 0, 10, 0,
                  _('Number of cells to pad for caret movement on the left '
                    'side of the display.'))
    self.newNumeric('RightPadding', 0, _('Right padding'), 0, 10, 0,
                  _('Number of cells to pad for caret movement on the left '
                    'side of the display.'))                    
    self.newBool('MoveCaret', False, _('Move caret on Home, EOL, SOL?'), 
                _('When set, Home, End of Line, and Start of Line commands '
                  'move caret to respective position. Otherwise, display '
                  'scrolls but caret remains in the same location.'))
    self.newBool('BrailleReview', False, _('Track review keys?'), 
                _('When set, reviewing is tracked by the Braille display.  '
                  'Otherwise, display is not updated when reviewing.'))
    self.BrailleAvailable = True
    
   
  def getGroups(self):
    '''
    Creates configuration groups from pre-defined settings.
    @return: root setting group
    @rtype: L{AEState.Setting.Group}
    '''
    root = self.newGroup()
    
    g = root.newGroup(_('General Navigation'))
    g.append('SnapOnUpdate')
    g.append('MoveCaret')
    
    g = root.newGroup(_('Page Forwarding'))
    g.append('SnapToWord')
    g.append('Overlap')
    
    g = root.newGroup(_('Padding'))
    g.append('LeftPadding')
    g.append('RightPadding')
    
    g = root.newGroup(_('Reviewing'))
    g.append('BrailleReview')
    
    return root
    
  
class BasicBraillePerk(Perk.Perk):
  '''
  Defines a default braille user interface.
  
  @ivar left_slice: Left slice index for current_text including scroll offset
  @type left_slice: integer
  @ivar pre_left_slice: Left slice index into current_text before scroll_offset
  @type pre_left_slice: integer
  @ivar scroll_offset: Scroll offset relative to pre_left_slice
  @type scroll_offset: integer
  @ivar abscaretpos: Index of the caret position within current_text
  @type abscaretpos: integer
  @ivar current_text: String containing current item accessible on the Braille
    display. Managed to support scrolling.
  @type current_text: string
  '''
  # defines the class to use for state information in this Perk
  STATE = BasicBraillePerkState
  
  def init(self):
    '''
    Registers L{Task}s to handle focus, caret, selector, state, and 
    property change events. 
    '''
    self.left_slice = 0
    self.pre_left_slice = 0
    self.scroll_offset = 0
    self.abscaretpos = -1
    self.current_text = ""
    
    # set the output device      
    self.setPerkIdealOutput('braille')
    
    # register braille events handlers
    self.registerTask(HandleSelectorChange('braille selector'))
    self.registerTask(HandleCaretChange('braille caret'))
    self.registerTask(HandleFocusChange('braille focus'))
    
    # register named tasks
    self.registerTask(OutputText('braille current text'))
    
    # register braille named tasks for input events
    self.registerTask(HandleScrollForward('braille scroll forward'))
    self.registerTask(HandleScrollBackward('braille scroll backward'))
    self.registerTask(HandlePageForward('braille page forward'))
    self.registerTask(HandlePageBackward('braille page backward'))
    self.registerTask(HandleTouchCursor('braille touch cursor'))
    self.registerTask(HandleLineBegin('braille line begin'))
    self.registerTask(HandleLineEnd('braille line end'))
    self.registerTask(HandleHome('braille home'))
    self.registerTask(HandleReview('braille review'))
    self.registerTask(HandlePointerToFocus('braille pointer to por'))
    self.registerTask(BrailleText('braille text'))
                       
    #register braille device commands
    try:
      brlindev = self.getInputDevice(None, 'braille')
    except:
      if self.state.BrailleAvailable:
        self.stopNow()
        self.doTask('read message', text=_('braille device unavailable'), 
                     sem=SEM_ERROR)
        self.state.BrailleAvailable = False
      return
      
    
    self.registerCommand(brlindev, 'braille scroll backward', True, 
                         [brlindev.KEY_CMD_FWINLT])
    self.registerCommand(brlindev, 'braille scroll forward', True, 
                         [brlindev.KEY_CMD_FWINRT])
    self.registerCommand(brlindev, 'braille page backward', True, 
                         [brlindev.KEY_CMD_FWINLTSKIP])
    self.registerCommand(brlindev, 'braille page forward', True, 
                         [brlindev.KEY_CMD_FWINRTSKIP])            
    self.registerCommand(brlindev, 'braille touch cursor', True, 
                         [brlindev.KEY_CMD_ROUTE])
    self.registerCommand(brlindev, 'braille home', True, 
                         [brlindev.KEY_CMD_HOME])
    self.registerCommand(brlindev, 'braille line begin', True, 
                         [brlindev.KEY_CMD_LNBEG])
    self.registerCommand(brlindev, 'braille line end', True, 
                         [brlindev.KEY_CMD_LNEND])
    
    # link to review mode tasks
    self.chainTask('braille review', CHAIN_AFTER, 'review previous item')
    self.chainTask('braille review', CHAIN_AFTER, 'review current item')
    self.chainTask('braille review', CHAIN_AFTER, 'review next item')
    self.chainTask('braille review', CHAIN_AFTER, 'review previous word')
    self.chainTask('braille review', CHAIN_AFTER, 'review current word')
    self.chainTask('braille review', CHAIN_AFTER, 'review next word')
    self.chainTask('braille review', CHAIN_AFTER, 'review previous char')
    self.chainTask('braille review', CHAIN_AFTER, 'review current char')
    self.chainTask('braille review', CHAIN_AFTER, 'review next char')
    self.chainTask('braille pointer to por', CHAIN_AFTER, 'pointer to por')
    
  def close(self):
    try:
      brlindev = self.getInputDevice(None, 'braille')
      self.unregisterCommand(brlindev, [brlindev.KEY_CMD_FWINLT])
      self.unregisterCommand(brlindev, [brlindev.KEY_CMD_FWINRT])
      self.unregisterCommand(brlindev, [brlindev.KEY_CMD_FWINRTSKIP])
      self.unregisterCommand(brlindev, [brlindev.KEY_CMD_FWINLTSKIP])
      self.unregisterCommand(brlindev, [brlindev.KEY_CMD_ROUTE])
      self.unregisterCommand(brlindev, [brlindev.KEY_CMD_HOME])
      self.unregisterCommand(brlindev, [brlindev.KEY_CMD_LNBEG])
      self.unregisterCommand(brlindev, [brlindev.KEY_CMD_LNEND])
    except InvalidDeviceError:
      pass
                                                     
  def getDescription(self):
    return _('Manages basic screen reader Braille input and output. Requires '
             'a loaded Braille device.')
  
  def getName(self):
    return _('Basic Braille')
  
  def setAbsCaretPos(self, pos):
    '''
    Set absolute caret position
    '''
    if pos >= -1 and pos < len(self.current_text):
      self.abscaretpos = pos
    else:
      self.abscaretpos = -1 
            
  def getRelCaretPos(self, totalcells):
    '''
    Get relative (to device display) caret position
    
    @return: caret position
    @rtype: Integer
    '''
    # -1 (0 on device) is special case where we don't show cursor
    if self.abscaretpos == -1:
      return -1
    else:
      return max(-1, self.abscaretpos - self.getLeftSlice(totalcells)) 
     
  def getLeftSlice(self, totalcells):
    '''
    Get the leftmost slice indice for the current braille output
    
    @return: Left slice index
    @rtype: int
    '''  
    state = self.getState()
    misscellcnt = self.send(CMD_GET_MISSINGCELL_COUNT, None)
    
    if self.task_por.char_offset-self.pre_left_slice < state.LeftPadding:
      retval = max(0,self.task_por.char_offset-state.LeftPadding)
    elif self.task_por.char_offset-self.pre_left_slice >= \
                totalcells-state.RightPadding - misscellcnt:
      retval = self.task_por.char_offset-totalcells+state.RightPadding+1
      retval += misscellcnt
    else:
      retval = self.pre_left_slice
    
    # set before applying scroll_offset
    self.pre_left_slice = retval
    
    # apply scroll offset
    if self.scroll_offset > 0:
      retval += self.scroll_offset
    elif self.scroll_offset < 0:
      retval = max(0, retval + self.scroll_offset)
      
    return retval
    
class HandleLineEnd(Task.InputTask):
  '''
  Moves caret to last cell in current line of text, redraws braille display.
  
  @note: getEndOfHardLine skews offset one cell on last line, probably an error
  '''
  def execute(self, **kwargs):
    # move caret if desired. 
    if self.perk.getState().MoveCaret:
      # HandleCaretChange.update() will update current_text
      eol_por = self.getEndOfHardLine()
      self.leftClickPOR(eol_por)
    else:
      # otherwise just set current_text for new por location
      self.task_por.char_offset = len(self.getItemText())
      self.perk.current_text = self.getItemText()
    
    self.perk.scroll_offset = 0
    self.doTask("braille current text")
    
class HandleHome(Task.InputTask):
  '''
  Moves caret to first cell of first line, redraws braille display
  '''
  def execute(self, **kwargs):
    caret_pos = self.task_por.char_offset + self.task_por.item_offset
    self.task_por.char_offset = 0
    self.task_por.item_offset = 0
    
    # move caret if desired. 
    if self.perk.getState().MoveCaret:
      # HandleCaretChange.update() will update current_text
      self.leftClickPOR(self.task_por)
    else:
      # otherwise just set current_text for new por location
      self.perk.current_text = self.getItemText()
      self.perk.setAbsCaretPos(caret_pos)
    
    self.perk.scroll_offset = 0
    self.doTask("braille current text")

class HandleLineBegin(Task.InputTask):
  '''
  Moves caret to first cell in current line of text, redraws braille display
  '''
  def execute(self, **kwargs):
    self.task_por.char_offset = 0
    
    # move caret if desired. 
    if self.perk.getState().MoveCaret:
      # HandleCaretChange.update() will update current_text
      self.leftClickPOR(self.task_por)
    else:
      # otherwise just set current_text for new por location
      self.perk.current_text = self.getItemText()
    
    self.perk.scroll_offset = 0
    self.doTask("braille current text")
    
class HandleTouchCursor(Task.InputTask):
  '''
  Sets caret at given location in text accessibles or left clicks the center 
  of the given accessible.
  '''
  def execute(self, argument=None, **kwargs):
    if argument is None:
      return
    if argument < 0 or argument > self.getStyle().TotalCells:
      return
  
    newoffset = self.perk.pre_left_slice + self.perk.scroll_offset + argument
    self.perk.pre_left_slice += self.perk.scroll_offset  
    if not self.perk.getState().SnapOnUpdate:
      self.perk.scroll_offset = 0
         
    if newoffset < 0:
      newoffset = 0
    elif newoffset > len(self.perk.current_text)-1:
      newoffset = len(self.perk.current_text) - 1
      
    # adjust current por and send touch cursor 
    self.task_por.char_offset = newoffset
    self.leftClickPOR(self.task_por)
    
class HandleScrollForward(Task.InputTask):
  '''
  Scrolls Braille viewport to right
  '''
  def execute(self, **kwargs):
    style=self.getStyle()
    if style is None:
      return
    state=self.perk.getState()
   
    if self.perk.pre_left_slice + self.perk.scroll_offset < \
          len(self.perk.current_text)-style.TotalCells+state.RightPadding:
      self.perk.scroll_offset += 1
    self.doTask("braille current text")
     
class HandleScrollBackward(Task.InputTask):
  '''
  Scrolls Braille viewport to left
  '''
  def execute(self, **kwargs):
    if self.perk.pre_left_slice + self.perk.scroll_offset > 0:
      self.perk.scroll_offset -= 1
    self.doTask("braille current text")
    
class HandlePageForward(Task.InputTask):
  '''
  Scrolls Braille viewport to right one display length minus overlap
  '''
  def execute(self, **kwargs):
    style=self.getStyle()
    if style is None:
      return
    state=self.perk.getState()
    
    ctlen = len(self.perk.current_text)
    l_el_len, r_el_len = self.send(CMD_GET_ELLIPSIS_SIZE, None)
    
    if  2*style.TotalCells+self.perk.pre_left_slice+ \
           self.perk.scroll_offset-state.Overlap < ctlen + state.RightPadding:
      self.perk.scroll_offset += \
                     style.TotalCells-state.Overlap-l_el_len-r_el_len
    else:
      self.perk.scroll_offset = ctlen + state.RightPadding\
             - style.TotalCells - self.perk.pre_left_slice
      
    if state.SnapToWord:
      found = False
      wordadj = 0
      for i in xrange(self.perk.scroll_offset+ \
                     self.perk.pre_left_slice+l_el_len, -1, -1):
        if self.perk.current_text[i] == ' ' or i == 0:
          if i != 0:
            wordadj += 1
            found = True
          break
        else:
          wordadj -= 1 
      if found:
        self.perk.scroll_offset += wordadj
   
    self.doTask("braille current text")
  
class HandlePageBackward(Task.InputTask):
  '''
  Scrolls Braille viewport to left one display length minus overlap
  '''
  def execute(self, **kwargs):
    style=self.getStyle()
    if style is None:
      return
    state=self.perk.getState()
    
    l_el_len, r_el_len = self.send(CMD_GET_ELLIPSIS_SIZE, None)
    
    if self.perk.pre_left_slice+self.perk.scroll_offset- \
                     (style.TotalCells-state.Overlap) > 0:
      self.perk.scroll_offset -= \
                     style.TotalCells-state.Overlap-l_el_len-r_el_len
    else:
      self.perk.scroll_offset = (self.perk.pre_left_slice * -1)
    
    if state.SnapToWord:
      found = False
      wordadj = 0
      for i in xrange(self.perk.scroll_offset+self.perk.pre_left_slice, \
                      -1, -1):
        if self.perk.current_text[i] == ' ' or i == 0:
          if i != 0:
            wordadj += 1
            found = True
          break
        else:
          wordadj -= 1 
      if found:
        self.perk.scroll_offset += wordadj
    self.doTask("braille current text")
    
    
class HandleFocusChange(Task.FocusTask):
  '''  
  Task that handles a L{AEEvent.FocusChange}.
    
  '''  
  def executeGained(self, por, **kwargs):
    '''
    @param por: Point of regard for the new focus
    @type por: L{POR}
    '''
    self.perk.scroll_offset = 0
    self.perk.current_text = ""
    self.doTask("braille current text")
    return True

class HandleSelectorChange(Task.SelectorTask):
  ''' 
  Task that handles a L{AEEvent.SelectorChange}.
  '''
  def executeActive(self, por, text, **kwargs):
    '''
    Announces the text of the active item.
    
    @param por: Point of regard where the selection event occurred
    @type por: L{POR}
    @param text: The text of the active item
    @type text: string
    '''  
    # update current text
    self.perk.current_text = self.getItemText()
    self.perk.scroll_offset = 0
    # Don't show cursor
    self.perk.setAbsCaretPos(-1)
    # send out line of text
    self.doTask("braille current text")
    
class HandleCaretChange(Task.CaretTask):
  '''  
  Task that handles a L{AEEvent.CaretChange}. The L{execute} method performs
  actions that should be done regardless of whether text was inserted or
  deleted or the caret moved. The other three methods handle these more
    
  specific cases.
  
  @ivar changed: Tracks whether the last caret move event was a caused by a
    change (insert/delete) or just navigation
  @type changed: boolean
  '''
  def executeInserted(self, por, text, text_offset, **kwargs):
    '''
    Outputs the entire text to braille display in a managed fashion (overlaps,
    caret position, padding etc.).  Snaps to original position (before any 
    scrolling) if requested.
    
    @param por: Point of regard where the caret event occurred
    @type por: L{POR}
    @param text: The text inserted
    @type text: string
    @param text_offset: The offset of the inserted text
    @type text_offset: integer
    '''
    # update current text
    style = self.getStyle()
    if style is None:
      return
    self.perk.current_text = self.getItemText()
    if self.perk.getState().SnapOnUpdate:
      self.perk.scroll_offset = 0
    self.perk.setAbsCaretPos(por.char_offset)
    # send out line of text
    self.doTask("braille current text") 
    
  def executeMoved(self, por, text, text_offset, **kwargs):
    '''
    Outputs the entire text to braille display in a managed fashion (overlaps,
    caret position, padding etc.).  Snaps to original position (before any 
    scrolling) if requested.
    
    @param por: Point of regard where the caret event occurred
    @type por: L{POR}
    @param text: The text passed during the move
    @type text: string
    @param text_offset: The offset of the new caret position
    @type text_offset: integer
    '''
    # update current text
    style = self.getStyle()
    if style is None:
      return
    self.perk.current_text = self.getItemText()
    if self.perk.getState().SnapOnUpdate:
      self.perk.scroll_offset = 0
    self.perk.setAbsCaretPos(por.char_offset)
    # send out line of text
    self.doTask("braille current text")
    
  def executeDeleted(self, por, text, text_offset, **kwargs):
    '''
    Outputs the entire text to braille display in a managed fashion (overlaps,
    caret position, padding etc.).  Snaps to original position (before any 
    scrolling) if requested.
    
    @param por: Point of regard where the caret event occurred
    @type por: L{POR}
    @param text: The text delete
    @type text: string
    @param text_offset: The offset of the delete text
    @type text_offset: integer
    '''
    # update current text
    style = self.getStyle()
    if style is None:
      return
    self.perk.current_text = self.getItemText()
    if self.perk.getState().SnapOnUpdate:
      self.perk.scroll_offset = 0
    self.perk.setAbsCaretPos(por.char_offset)
    # send out line of text
    self.doTask("braille current text")

  def update(self, por, text, text_offset, added, **kwargs):
    '''
    Updates state variables and relies on other methods to handle any output.
    
    @param por: Point of regard where the caret event occurred
    @type por: L{POR}
    @param text: The text inserted, deleted or the line of the caret
    @type text: string
    @param text_offset: The offset of the inserted/deleted text or the line 
      offset when movement only
    @type text_offset: integer
    @param added: True when text added, False when text deleted, and None 
      when event is for caret movement only
    @type added: boolean
    '''
    # update all state variables 
    style = self.getStyle()
    if style is None:
      return
    self.perk.current_text = self.getItemText()
    if self.perk.getState().SnapOnUpdate:
      self.perk.scroll_offset = 0
    self.perk.setAbsCaretPos(por.char_offset)

class OutputText(Task.Task):
  '''
  Outputs the current text to a Braille device. Relays caret position and 
  truncation information to device.
  '''
  def execute(self, **kwargs):
    style = self.getStyle()
    if style is None:
      return
    state = self.perk.getState()
   
    # set caret position. 
    caretpos = self.perk.getRelCaretPos(style.TotalCells)
    if caretpos < 0 or caretpos >= style.TotalCells:
      self.send(CMD_CARET, -1)
    else:
      self.send(CMD_CARET, caretpos)
  
    leftslice = self.perk.getLeftSlice(style.TotalCells)
    #Note: rightslice >= len(current_text) does not throw exception
    rightslice = leftslice+style.TotalCells
    
    # truncation indicators for ellipsis
    misscellcnt = self.send(CMD_GET_MISSINGCELL_COUNT, None)
    self.send(CMD_TRUNCATE, (leftslice>0, 
        rightslice<len(self.perk.current_text)+misscellcnt)) 
        
    # send text to device
    self.sayItem(text=self.perk.current_text[leftslice:rightslice])
    
class HandleReview(Task.InputTask):
  '''  
  Synchronizes the Braille display with the text at the pointer when reviewing.
  
  '''
  def execute(self, **kwargs):
    if self.perk.getState().BrailleReview:
      result = self.getTempVal('review')
      if result in (REVIEW_OK, REVIEW_WRAP):
        # update current text
        self.perk.current_text = self.getItemText()
        # get length of ellipsis
        l_el_len, r_el_len = self.send(CMD_GET_ELLIPSIS_SIZE, None)
        
        if self.perk_state.SnapOnUpdate:
          self.perk.scroll_offset = 0
        # get the por at the focus
        focus_por = self.getAccFocus()
        # set caret position
        if focus_por is not None and (focus_por.item_offset == self.task_por.item_offset):
          self.perk.setAbsCaretPos(focus_por.char_offset)
        else:
          self.perk.setAbsCaretPos(-1)
          
        if self.task_por.char_offset == 0:
          self.perk.pre_left_slice = self.task_por.char_offset
        elif self.task_por.char_offset - l_el_len >= 0:
          self.perk.pre_left_slice = self.task_por.char_offset - l_el_len
        else: # self.task_por.char_offset - l_el_len < 0
          # Don't move viewport when less than ellipsis size
          pass
             
        self.doTask('braille current text')
      
        
class HandlePointerToFocus(Task.InputTask):
  '''
  Synchronizes the Braille display with the text at the focus when the pointer
  is warped back to the caret.
  '''
  def execute(self, **kwargs):
    # update current text
    self.perk.current_text = self.getItemText()
    if self.perk_state.SnapOnUpdate:
      self.perk.scroll_offset = 0
    # now Braille it
    self.doTask('braille current text')
    
class BrailleText(Task.Task):
  '''
  Outputs item text to Braille display.
  '''
  def execute(self, text=None, **kwargs):
    # update current text
    if text is None:
      self.perk.current_text = self.getItemText()
    else:
      self.perk.current_text = text
    if self.perk_state.SnapOnUpdate:
      self.perk.scroll_offset = 0
    # now Braille it
    self.doTask('braille current text')    
