'''
Maps the commands registered in the screen reader Perks to keys with 
Alt-Shift as the standard modifier. Also defines Ctrl as the stop key for
speech.

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
import Perk
from i18n import _

__uie__ = dict(kind='perk', all_tiers=True)

class AltShiftPerk(Perk.Perk):
  '''
  Defines keyboard mappings for features in ReviewPerk, BasicSpeechPerk, and 
  DefaultDialogPerk using Alt-Shift as the modifier.
  '''
  def init(self):
    '''    
    Gets a reference to the L{Keyboard} device. Sets the Alt and Shift keys as
    modifier keys. Registers named L{Task}s as commands.
    '''
    # get the Keyboard device and register modifiers and commands
    kbd = self.getInputDevice(None, 'keyboard')
    self.addInputModifiers(kbd, kbd.AEK_ALT_L, kbd.AEK_SHIFT_L, kbd.AEK_ALT_R,
                           kbd.AEK_SHIFT_R, kbd.AEK_CAPS_LOCK)

    # speech perk
    self.registerCommand(kbd, 'stop now', True, [kbd.AEK_CONTROL_R])
    self.registerCommand(kbd, 'stop now', True, [kbd.AEK_CONTROL_L])
    
    # register commands for either left Alt-Shift or right Alt-Shift
    pairs = [[kbd.AEK_ALT_L, kbd.AEK_SHIFT_L], 
             [kbd.AEK_ALT_R, kbd.AEK_SHIFT_R]]

    for pair in pairs:
      # review perk
      self.registerCommand(kbd, 'review previous item', False, 
                           pair+[kbd.AEK_U])
      self.registerCommand(kbd, 'review current item', False, pair+[kbd.AEK_I])
      self.registerCommand(kbd, 'review next item', False, pair+[kbd.AEK_O])
      self.registerCommand(kbd, 'review previous word', False, 
                           pair+[kbd.AEK_J])
      self.registerCommand(kbd, 'review current word', False, pair+[kbd.AEK_K])
      self.registerCommand(kbd, 'review next word', False, pair+[kbd.AEK_L])
      self.registerCommand(kbd, 'review previous char', False, 
                           pair+[kbd.AEK_M])
      self.registerCommand(kbd, 'review current char', False, 
                           pair+[kbd.AEK_COMMA])
      self.registerCommand(kbd, 'review next char', False, 
                           pair+[kbd.AEK_PERIOD])
      self.registerCommand(kbd, 'focus to por', False, 
                           pair+[kbd.AEK_R])
      self.registerCommand(kbd, 'pointer to por', False, pair+[kbd.AEK_F])
      self.registerCommand(kbd, 'mouse to por', False, pair+[kbd.AEK_X])
      
      # speech perk
      self.registerCommand(kbd, 'increase speech rate', False,pair+[kbd.AEK_T])
      self.registerCommand(kbd, 'decrease speech rate',False,pair+[kbd.AEK_G])
      self.registerCommand(kbd, 'cycle where am i', False, pair+[kbd.AEK_1])
      self.registerCommand(kbd, 'read top', False, pair+[kbd.AEK_Y])
      self.registerCommand(kbd, 'cycle text attributes',False,pair+[kbd.AEK_B])
      self.registerCommand(kbd, 'read description', False, pair+[kbd.AEK_D])
      self.registerCommand(kbd, 'read all', False, pair+[kbd.AEK_H])
      
      # default dialog perk
      self.registerCommand(kbd, 'show perk chooser', False, pair+[kbd.AEK_A])
      self.registerCommand(kbd, 'show settings chooser', False, 
                           pair+[kbd.AEK_Q])
                           
      # search perk
      self.registerCommand(kbd, 'search show chooser', False, pair+[kbd.AEK_S])
      self.registerCommand(kbd, 'search find prev', False, pair+[kbd.AEK_W])
      self.registerCommand(kbd, 'search find next', False, pair+[kbd.AEK_E])

    # register commands for either left Alt-CapsLock or right Alt-CapsLock
    pairs = [[kbd.AEK_ALT_L, kbd.AEK_CAPS_LOCK], 
             [kbd.AEK_ALT_R, kbd.AEK_CAPS_LOCK]]
    
    for pair in pairs:
      # magnifier perk
      self.registerCommand(kbd, 'mag increase zoom', False, pair+[kbd.AEK_T])
      self.registerCommand(kbd, 'mag decrease zoom', False, pair+[kbd.AEK_G])

  def getDescription(self):
    return _('Defines hotkeys for functions in basic speech, magnifier, '
             'Braille, review, and dialog Perks')

  def getName(self):
    return _('Alt-Shift keymap')
