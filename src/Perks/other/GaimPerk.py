'''
Defines a user interface for gaim.

Keys registered

CapsLock-W: Reports status of all open conversations
CapsLock-S: Cycle focus between message history and compose area

Event announcements

(X) means optional announcement X based on type of widget
[X] means X always announced
<X> indicates continue announcements in X

on activate: ["chat with" username]
on focus: ["compose" or "history"] <active descendant change>
on message received: [text of message]
  
@var CONTAINER_PATH: path from frame to container with accessible elements
@type CONTAINER_PATH: integer
@var NUM_CHILDREN_IN_CHAT_WINDOW: to identify a chat window: assume that only
  this acc will have 2 kids, and further assume that accessibles with similar
  hierarchical placement will not.
@type NUM_CHILDREN_IN_CHAT_WINDOW: integer
@var MENU_BAR_PATH: path: from container to menu bar
@type MENU_BAR_PATH: integer
@var TAB_PATH: path from container to page tab list (contains conversations)
@type TAB_PATH: integer
@var HISTORY_PATH: path from a conversation tab to its history text panel
@type HISTORY_PATH: integer
@var COMPOSE_PATH: path from a conversation tab to its compose text panel
@type COMPOSE_PATH: integer
@var UNREAD_STATUS: Constant representing a conversation has an unread 
  message
@type UNREAD_STATUS: integer
@var TYPING_STATUS: Constant representing a chat partner is currently typing
  in a conversation
@type TYPING_STATUS: integer
@var PAUSED_STATUS: Constant representing a chat partner is stoped typing
  in a conversation
@type PAUSED_STATUS: integer
@var IDLE_STATUS: Constant representing a chat partner is idle in a 
  conversation
@type IDLE_STATUS: integer
@var OFFLINE_STATUS: Constant representing a chat partner is offline in a 
  conversation
@type OFFLINE_STATUS: integer
@var RED: Gtk color description of color red, as string
@type RED: string
@var GREEN: Gtk color description of color green, as string
@type GREEN: string
@var YELLOW: Gtk color description of color yellow, as string
@type YELLOW: string
@var LOGOUT_GREY: Gtk color description of grey, as string, used in logoff
@type LOGOUT_GREY: string
@var STATUS_MSG: List of strings describing the unread, typing, etc. status
  of conversations sorted by their likely importance to the user
@type STATUS_MSG: list of string

@author: Brett Clippingdale
@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
import Perk, Task, AEState, AEConstants
from POR import POR
from i18n import _

__uie__ = dict(kind='perk', tier='gaim')

# actual paths, derived using at-poke, assumed to be static.  YMMV.
# path: from frame to container with accessible elements   
CONTAINER_PATH = 0
# to identify a chat window: assume that only this acc will have 2 kids, and
# further assume that accessibles with similar hierarchical placement will
# not.  Again, YMMV, so analyze carefully with AT-Poke and then verify.
NUM_CHILDREN_IN_CHAT_WINDOW = 2
# path: from container to page tab list (contains all conversations)
TAB_PATH = 1  

# actual paths in Gaim 1.5, derived using at-poke, assumed to be static
# if Gaim 2, these will get different values in setGaimEnviron()
HISTORY_PATH = (0,0,0,0,0) # path: tab (a conversation) to history text panel
COMPOSE_PATH = (0,0,1,1,0,0,0) # path: tab to chat compose text panel

# gaim colors, (eg. convo tab text color depends on typing/login status)
RED = "57311,16962,7710" # unread msg
GREEN = "18247,41120,17990" # active typing
YELLOW = "53713,38036,3084" # paused typing
GREY = "34438,33410,29298" # off-line
LOGOUT_GREY = "32639, 32639, 32639" # used in buddy list on logoff
# constants for status of conversations
UNREAD_STATUS = 0
TYPING_STATUS = 1
PAUSED_STATUS = 2
IDLE_STATUS = 3
OFFLINE_STATUS = 4
LOGIN_STATUS = 5
LOGOUT_STATUS = 6
STATUS_MSG = [_('unseen'), _('typing'), _('paused'), _('idle'), _('offline'),
              _('logged in'), _('logged out')] 

class GaimPerkState(Perk.PerkState):
  '''
  Settings for Gaim.
  
  ReadBackground (bool): Read incoming messages when gaim is in the background?
  '''
  def init(self):
    self.newBool('ReadBackground', False, _('Read background messages?'), 
                 _('When checked, incoming messages will be read even when '
                   'Gaim is in the background.'))
    self.newBool('BrailleBackground', False, _('Braille background messages?'), 
                 _('When checked, incoming messages will be output to Braille '
                 'device even when Gaim is in the background.'))
  
  def getGroups(self):
    g = self.newGroup()
    g.append('ReadBackground')
    g.append('BrailleBackground')
    return g

class GaimPerk(Perk.Perk):
  '''
  Provides hotkeys for switching between the message history and message 
  compose areas. Announces incoming messages and allows for review by entire
  message.
  
  Has initial support for announcing when buddies sign in and out and when the
  status of a conversation changes. Written to work with both gaim 1.5 and 2.0.
    
  @ivar chat_compose_por: Reference to chat msg compose panel
  @type chat_compose_por: L{POR}
  @ivar chat_history_por: Reference to chat history panel
  @type chat_history_por: L{POR}
  @ivar last_convo: Reference to the last selected conversation tab
  @type last_convo: L{POR}
  @ivar initialized: have gaim environment variables been initialized?
  @type initialized: boolean
  @ivar is_gaim2: is user client Gaim2 ?
  @type is_gaim2: boolean
  @ivar is_history_focused: does chat history panel currently have focus?
  @type is_history_focused: boolean
  @ivar last_history_caret: track caret in history panel, read when on new line
  @type last_history_caret: L{POR}
  '''
  STATE = GaimPerkState
  
  def init(self):
    '''
    Registers keyboard bindings that give status of conversation tabs (unread
    message, active typing, paused typing) and drive focus back and forth
    between chat composition text area and history text area.
    '''
    # set an audio device as the default output
    self.setPerkIdealOutput('audio')    
    
    # register event handlers
    # events here are app-level, other Perks will handle gnome-level events,
    # so only register events of interest here. 
    self.registerTask(ActivateGaimWindow('gaim read window'))
    self.registerTask(HandleFocusChange('gaim read focus'))
    self.registerTask(HandleSelectorChange('gaim read selector'))
    self.registerTask(HandleCaretChange('game read caret'))
    self.registerTask(ReadIncoming('gaim read incoming', all=True))
    # not working for background events ???
    #self.registerEventTask(HandlePropertyChange(focus=True, tier=True,
    #                       background=True))
    # @note: not handling TableChange until certain Gaim bugs fixed
    # if re-enabled, may want to uncomment debug code: last_por_inserted (global
    # variable) and code in handleCaretChange
    #self.registerEventTask(BuddySignOnOff(focus=True, tier=True,
                           #background=True))
    
    # register named tasks.  Specify the task class name (see below) and a 
    # description.  These descriptions are dictionary keys and therefore
    # must be unique.  Here, Gaim is a prefix to help ensure uniqueness.
    self.registerTask(ToggleChatPanel('gaim chat panel toggle'))
    self.registerTask(ConversationStatus('gaim conversation status'))

    # get the Keyboard device and register modifiers.
    kbd = self.getInputDevice(None, 'keyboard')
    self.addInputModifiers(kbd, kbd.AEK_CAPS_LOCK)
    
    # register the keys and their associated commands, binding them using the
    # unique dictionary key we registered above.
    # Pass 3 element tuple.  Need to capture keys as pressed though we only
    # handle them if all both modifiers pressed before the non-modifier.
    self.registerCommand(kbd, 'gaim chat panel toggle', False,
                         [kbd.AEK_CAPS_LOCK, kbd.AEK_S])
    self.registerCommand(kbd, 'gaim conversation status', False,
                         [kbd.AEK_CAPS_LOCK, kbd.AEK_W])
    
    # POR reference to chat composition text area
    self.chat_compose_por = POR() 
    # POR reference to chat history text area
    self.chat_history_por = POR() 
    self.last_convo = POR()
  
    # have gaim environment variables been initialized?  This must happen first
    # time that gaim chat window is activated
    self.initialized = False 
    # is user client Gaim 2.0 instead of 1.5?
    self.is_gaim2 = False 
  
    # does chat history panel currently have focus
    self.is_history_focused = False 
    self.last_history_caret = POR()
  
    # is there selection in the composition area? used to avoid announcing
    # deleted text when sending
    self.selection = False
  
    # may be used in case where, on sign-off, multiple inserts fire
    #debug, see handleRowInserted, handleCaretChange
    #last_row_inserted_por = None 

  def getActivePageTabListPOR(self):
    '''
    Gets a Point of Regard to the page tab list in an active chat window.
    
    @return: Point of Regard to the page tab list in an active chat window, 
    or None otherwise
    @rtype: L{POR}
    '''
    frame_por = self.getRootAcc()
    container_por = self.getAccFromPath(frame_por, CONTAINER_PATH)
    # is chat window active?
    if (self.hasAccState('active', frame_por) and 
        self.getAccCount(container_por) == NUM_CHILDREN_IN_CHAT_WINDOW):
      # get 'page tab list'
      tab_list_por = self.getAccFromPath(container_por, TAB_PATH)
      if self.hasAccRole("page tab list", tab_list_por):
        return tab_list_por
    # in all other cases, return None
    return None
    
  def getTypingStatus(self, por):
    '''
    Gets current typing status on a given conversation L{POR}.
    
    @return: One of L{UNREAD_STATUS}, L{TYPING_STATUS}, L{PAUSED_STATUS}, 
      L{IDLE_STATUS} that can be mapped to a message in L{STATUS_MSG}
    @rtype: integer
    '''
    value = self.getAccTextAttr("fg-color", por) 
    # red: message typed, unread
    if value == RED:
      return UNREAD_STATUS
    # green: typing
    elif value == GREEN:
      return TYPING_STATUS
    # yellow: paused
    elif value == YELLOW:
      return PAUSED_STATUS
    # grey: offline
    elif value == GREY:
      return OFFLINE_STATUS
    # black: (no typing), and no fg-color value
    else:
      return IDLE_STATUS
  
  def setGaimVersion(self):
    '''
    Applies a heuristic to determine which Gaim version is running locally.
    This is important because the the representation of the UI varies by local
    client, and in the case of a local Gaim 2 it also varies by remote client.
    '''
    tab_list_por = self.getActivePageTabListPOR()
    # get selected conversation tab
    selected_tab_por = self.getFirstSelected(tab_list_por)  
    self.chat_history_por = \
        self.getAccFromPath(selected_tab_por, *HISTORY_PATH)
    # if this is a "filler" rather than a text area, must be Gaim2
    if self.getAccRoleName(self.chat_history_por) == "filler":
      self.is_gaim2 = True
      print "chat client is Gaim beta 2 or higher"
    else:
      print "chat client is Gaim 1.5 or lower"
      
  def getName(self):
    '''
    @return: Human readable name of this L{Perk}.
    @rtype: string
    '''
    return _('Gaim')

#
# Tasks executed by keys
#
            
class ToggleChatPanel(Task.InputTask):
  '''
  Toggles focus between chat composition and chat history panels.
  '''
  # each task class must implement an execute method which is called when
  # the associated key bindings (registered in init method, above) are pressed
  def execute(self, **kwargs):
    # determine which chat panel currently has focus.  Specify which 
    # ATSPI-defined property of interest (here, 'focused') and the POR to check
    # for that property
    tab_list_por = self.perk.getActivePageTabListPOR()
    if tab_list_por is not None:
      if not self.perk.is_history_focused:
        #change focus to chat history panel
        self.setAccFocus(self.perk.chat_history_por)  
      else:
        #change focus to chat compose panel
        self.setAccFocus(self.perk.chat_compose_por) 
    return True # allow triggering keystrokes to propagate (default)

class ConversationStatus(Task.InputTask):
  '''
  Announces the status of the current conversations. Status for conversations
  is grouped by status type except for the foreground conversation which is
  always reported first.
  '''
  def execute(self, **kwargs):
    brailleout=[]
    tab_list_por = self.perk.getActivePageTabListPOR()
    # is chat window active? otherwise, don't switch
    if tab_list_por is None:
      return True
    
    # stop prior speech
    self.stopNow()

    # get selected conversation tab
    selected_tab_por = self.getFirstSelected(tab_list_por)
    
    por = self.getFirstPeerAcc(selected_tab_por)
    l = []
    while por is not None:
      # get the tab name and status value
      name = self.getAccName(por)
      # if acc not an artifact, process
      if name != '':
        value = self.perk.getTypingStatus(por)
        print selected_tab_por
        if selected_tab_por == por:
          self.sayState(text=(name, STATUS_MSG[value]), template='%s %s,')
          brailleout.append('%s: %s, ' % (name, STATUS_MSG[value]))
        else:
          l.append((value, name))
      por = self.getNextPeerAcc(por)
        
    # sort the list of status values and names by priority, highest first)
    l.sort()

    last = -1
    for value, name in l:
      if last != value:
        last = value
        self.sayState(text=STATUS_MSG[value], template='%s,')
        brailleout.append('%s: ' % (STATUS_MSG[value]))
      self.sayState(text=name, template='%s,')
      brailleout.append('%s, ' % (name))
    # output to Braille device 
    self.doTask('braille text', text=''.join(brailleout))

#
# Tasks executed by events
#

class ActivateGaimWindow(Task.ViewTask):
  '''
  Task that handles a L{AEEvent.ViewChange}.
  '''
  def executeGained(self, por, title, **kwargs):
    '''
    Prevents the window title from being announced if it is the chat window, 
    since better information is given for the chat window on the subsequent
    focus change event. Accomplished by stopping propagation of the event.
    
    @param por: Point of regard to the root of the new view
    @type por: L{POR}
    @param title: Title of the newly activated window
    @type title: string
    '''
    self.perk.last_convo = POR()
    tab_list_por = self.perk.getActivePageTabListPOR()
    print 'GAIM:', tab_list_por
    # test to see if not a chat window
    if tab_list_por is None:
      return True # allow other Perks to handle the view change
    # must be a chat window
    else:
      if not self.perk.initialized:
        self.perk.setGaimVersion()
        self.perk.initialized = True
      # don't allow the view change event to propagate
      return False

class HandleFocusChange(Task.FocusTask):
  '''  
  Task that handles a L{AEEvent.FocusChange}.
  '''  
  GAIM2_HISTORY_PATH = (0,0,0,0,0,0,0) #history text panel
  GAIM2_COMPOSE_PATH = (0,0,1,0,0,0,0,2,0) # compose text panel - gaim2 remote
  GAIM2_ALT_COMPOSE_PATH = (0,0,1,0,1,0,0,2,0) # compose text panel - gaim1.5
  
  def executeGained(self, por, **kwargs):
    # get 'page tab list'
    tab_list_por = self.perk.getActivePageTabListPOR()
    if tab_list_por is None:
      # let the event propagate
      return True
    
    # possibly stop prior speech
    self.mayStop()
    # and prevent this speech from being interrupted
    self.inhibitMayStop()

    # get selected conversation tab
    selected_tab_por = self.getFirstSelected(tab_list_por)
    
    # check if we should announce the state of the current convo or not
    if selected_tab_por != self.perk.last_convo:
      name = self.getAccName(selected_tab_por)
      self.saySection(text=name, template=_('chat with %s'))
      value = self.perk.getTypingStatus(selected_tab_por)
      self.sayState(text=STATUS_MSG[value])
      self.perk.last_convo = selected_tab_por

    # locate message history/compose text box PORs   
    # gaim 2 local client
    if self.perk.is_gaim2: 
      self.perk.chat_history_por = \
        self.getAccFromPath(selected_tab_por, *self.GAIM2_HISTORY_PATH)
      self.perk.chat_compose_por = \
        self.getAccFromPath(selected_tab_por, *self.GAIM2_COMPOSE_PATH)
      if (self.perk.chat_compose_por is None or
          not self.hasAccRole('text', self.perk.chat_compose_por)):
        self.perk.chat_compose_por = \
          self.getAccFromPath(selected_tab_por, *self.GAIM2_ALT_COMPOSE_PATH)
        print 'alt', self.perk.chat_compose_por
    # gaim 1.5 local client
    else:
      self.perk.chat_history_por = \
          self.getAccFromPath(selected_tab_por, *HISTORY_PATH)
      self.perk.chat_compose_por = \
          self.getAccFromPath(selected_tab_por, *COMPOSE_PATH)   

    # tell user which text area is focused
    self.perk.is_history_focused = \
        self.hasAccState('focused', self.perk.chat_history_por)
    if self.perk.is_history_focused:
    #if self.hasAccState('focused', self.perk.chat_history_por): 
      self.saySection(text=_('history,')) # this odd syntax supports i18n
      self.perk.last_history_caret = POR() # reset
      return False
    elif self.hasAccState('focused', self.perk.chat_compose_por):
    #elif self.hasAccState('focused', self.perk.chat_compose_por):
      self.saySection(text=_('compose,')) # this odd syntax supports i18n
      return False
    #else:
    #  return True # propagate handling of the event (default)

class HandleSelectorChange(Task.SelectorTask):
  '''
  Task that tracks text selection changes. Used to updated the selection flag
  so the L{HandleCaretChange.executeDeleted} method can decide whether or not
  to allow reporting of deleted text.
  '''
  def executeText(self, por, text, **kwargs):
    '''
    Sets the selection flag to True if some text is selected else sets it to
    False.
    
    @param por: Point of regard to the text caret
    @type por: L{POR}
    @param text: Currently selected text
    @type text: string
    '''
    if text:
      self.perk.selection = True
    else:
      self.perk.selection = False
    return True
    
class ReadIncoming(Task.CaretTask):
  '''  
  Reads incoming messages.
  '''
  def executeInserted(self, por, text, text_offset, layer, **kwargs):
    '''
    Speaks text that starts with a new line character. This indicates a new
    line of text has been inserted into the message history.
    
    @param por: Point of regard where the caret event occurred
    @type por: L{POR}
    @param text: The text passed during the move
    @type text: string
    @param text_offset: The offset of the new caret position
    @type text_offset: integer
    @param layer: Layer of the event, focus, tier, or background
    @type layer: integer
    '''
    # convoluted logic is necessary in order to minimize expensive  
    # compareAncestorRoles calls.
    if (not self.perk_state.ReadBackground and 
        not self.perk_state.BrailleBackground and
        layer == AEConstants.LAYER_BACKGROUND):
      # don't output background messages if the user has it disabled.
      return
    
    rv = self.compareAncestorRoles(por, 'scroll pane', 'filler', 'panel',
                                   'filler', 'split pane', 'filler', 
                                   'page tab')
                                   
    if rv and (layer != AEConstants.LAYER_BACKGROUND or 
         (layer == AEConstants.LAYER_BACKGROUND and 
          self.perk_state.ReadBackground)):
      # queue up text to speak
      self.sayItem(text=text)
      
    if rv and (layer !=  AEConstants.LAYER_BACKGROUND or 
         (layer == AEConstants.LAYER_BACKGROUND and 
          self.perk_state.BrailleBackground)):
      # output to braille device
      self.doTask('braille text', text=text)
      
      
class HandleCaretChange(Task.CaretTask):
  '''  
  Task that handles a L{AEEvent.CaretChange}. Used to read an entire message in
  chat history, rather than simply the current line.  The L{executeMoved} method
  finds the beginning and end of the current message, truncates the timestamp
  and user name when redundant, then reads the message.
  ''' 
  def executeMoved(self, por, text, text_offset, **kwargs):
    '''
    Announces an entire message from a remote user by looking for hard line 
    breaks (\\n) when the caret moves to a position just after or just before
    the break character. If the caret is anywhere else, lets other L{Perk}s do
    their default caret processing.
    
    @param por: Point of regard where the caret event occurred
    @type por: L{POR}
    @param text: The text passed during the move
    @type text: string
    @param text_offset: The offset of the new caret position
    @type text_offset: integer
    '''
    # debugging code for handleTableChange, since we may or may not get
    # multiple events when a remote buddy logs off, announce when caret changes
    # just as a test, not for final code.
    #if self.perk.last_row_inserted_por is not None:
    #  print "last_row_inserted_por" , self.perk.last_row_inserted_por
    #  self.perk.last_row_inserted_por = None
    # could eliminate by setting is_history_focused = False when appropriate
    tab_list_por = self.perk.getActivePageTabListPOR()
    # if chat window and chat history have focus
    if tab_list_por is not None and self.perk.is_history_focused:
      # say line when item_offset changes
      if (not por.isSameItem(self.perk.last_history_caret)):
        # get the start of the line (character after a \n)
        start = self.getStartOfHardLine(por)
        # get the end of the line (next encountered \n)
        end = self.getEndOfHardLine(por)

        # store the current POR
        self.perk.last_history_caret = por
        # if we're not at the start or end of a true message line, let 
        # other Perks handle the caret event unless at start/end of msg
        if por != end and por != start:
          return True
        
        # get all the text between the start and end PORs
        msg = self.getAccTextBetween(start, end)
        
        self.mayStop()
        self.sayItem(text=msg)
        #self.inhibitMayStop()
        return False
      
      # respond to caret movement on same line
      # just let other Perks handle this case
      else: 
        self.perk.last_history_caret = por
        return True
       
    # not in chat history
    else:
      return True
    
  def executeInserted(self, **kwargs):
    '''Resets the selection flag.'''
    self.perk.selection = False
    return True
    
  def executeDeleted(self, text, **kwargs):
    '''
    Consumes the deletion event if the selection flag is False and more than
    one character of text was deleted (i.e. a message was sent). Avoids
    announcing the sent text as "Backspace <text>" as well as the text that
    appears in the message history. If the selection flag is True, resets it.
    
    @param text: The text deleted
    @type text: string
    '''
    if not self.perk.selection and len(text) > 1:
      return False
    self.perk.selection = False
    return True
  
class HandlePropertyChange(Task.PropertyTask):
  '''  
  Task that handles a L{AEEvent.PropertyChange}.  
  '''  
  def execute(self, por, name, value, **kwargs):
    '''
    If in a chat window, check to see if the text color properties of a
    conversation tab have changed.  Announce change if to anything other than 
    'idle' (ie. active typing, paused typing, away, away idle, logged out).
    '''
    is_chat_window = self.perk.getActivePageTabListPOR()
    # test to see if a chat window and if a conversation page tab
    if is_chat_window is not None and self.hasAccRoleName('page tab', por):
      user = self.getAccName(por)
      status = self.perk.getTypingStatus(por)
      #print "%s: %s: %s" %(name, STATUS_MSG[status], por)
      # don't report idle status, we already report paused and new message
      if status != IDLE_STATUS:
        #print "GaimPerk::Property change:", por, name, value
        #print "%s: %s: %s" %(user, STATUS_MSG[status], por)
        self.sayState(text=(user, STATUS_MSG[status]), template='%s %s,')
    return True
  
 
class BuddySignOnOff(Task.TableTask):
  '''  
  Task that handles a L{AEEvent.TableChange}.
  
  These events will be used to know when a remote buddy logs in/out, however
  current information makes the "in/out" determination unreliable.  Bugs are
  being filed against Gaim.  Therefore, this perk does not currently register
  to handle TableChange events.
  '''  
  
  def executeTableRowInserted(self, por, first_child_por, last_child_por, 
                              layer, **kwargs):
    '''
    Executes this L{Task} in response to a row-inserted table change event. 
    Called by L{Task.TableTask.execute}.
        
    @param por: The L{POR} for the related accessible
    @type por: L{POR}
    @param first_child_por: The L{POR} of first inserted row
    @type first_child_por: L{POR}
    @param last_child_por: The L{POR} of last inserted row
    @type last_child_por: L{POR}
    @param layer: Layer on which the event occurred
    @type layer: integer
    '''
    #print "GaimPerk::executeTableRowInserted()", por, first_child_por, \
    #      last_child_por, layer
    frame_name = self.getWindowTitle(por)
    # only handle table change events from buddy list (don't announce when
    # conversation tab order changes, assume only a sighted user would do that).
    if frame_name == "Buddy List":
      #print "1" , self.getItemText(first_child_por) 
      #print "2" , self.getItemText(last_child_por) 
      # may be used in case where, on sign-off, multiple inserts fire
      # self.perk.last_row_inserted_por = first_child_por
      # name = self.getItemText(self.perk.last_row_inserted_por)
      #print "%s logged in" %name
      status = STATUS_MSG[LOGIN_STATUS]
      self.sayState(text=(name, status), template='%s: %s,')
    return True
  
  def executeTableRowDeleted(self, por, first_child_por, last_child_por, 
                             layer, **kwargs):
    '''
    Executes this L{Task} in response to a row-deleted table change event. 
    Called by L{execute}.
        
    @param por: The L{POR} for the related accessible
    @type por: L{POR}
    @param first_child_por: The L{POR} of first deleted row
    @type first_child_por: L{POR}
    @param last_child_por: The L{POR} of last deleted row
    @type last_child_por: L{POR}
    @param layer: Layer on which the event occurred
    @type layer: integer
    '''
    #print "GaimPerk::executeTableRowDeleted()" , por, first_child_por, \
    #      last_child_por, layer
    frame_name = self.getWindowTitle(por)
    if frame_name == "Buddy List":
      #print "1" , self.getItemText(first_child_por) 
      #print "2" , self.getItemText(last_child_por) 
      name = self.getItemText(first_child_por)
      #print "%s logged out" %name
      status = (name, STATUS_MSG[LOGOUT_STATUS])
      self.sayState(text=status, template='%s: %s,')
    return True
