'''
Defines a L{Perk} for the Firefox web browser.

@var DOCUMENT_ROLE: Role name of the top of a document view.
@type DOCUMENT_ROLE: string
@var DOCUMENT_MODE: Document structure browsing mode
@type DOCUMENT_MODE: integer
@var TABLE_MODE: Table structure browsing mode
@type TABLE_MODE: integer
@var WIDGET_MODE: Interactive widget browsing mode
@type WIDGET_MODE: integer
@var TOTAL_MODES: Total number of defined modes
@type TOTAL_MODES: integer

@author: Peter Parente
@author: Scott Haeger
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
# import useful modules for Perks
import Perk, Task
from POR import POR
from i18n import _
from AEConstants import *
import AEConstants 
import time, urlparse, urllib2

__uie__ = dict(kind='perk', tier='Minefield', all_tiers=False)

DOCUMENT_ROLE = 'document frame'

# Navigation modes
DOCUMENT_MODE = 0
TABLE_MODE = 1
WIDGET_MODE = 2
TOTAL_MODES = 3

class FirefoxPerkState(Perk.PerkState):
  '''
  Defines settings specific to Firefox.
  
  BoundReview (boolean): Keep review functions within the Firefox document when
  reviewing begins inside the document, or let them walk across document
  boundaries?
  
  AutoMode(boolean): When True, modes are automatically updated on a L{POR} 
  change.
  
  SearchDepth (integer): Number of nodes visited before iterative searches are 
  cancelled.  Affects all find, goto, and where am I? functions.
  '''
  def init(self):
    self.newBool('BoundReview', True, _('Bound review to document?'),
                 _('When set, the review keys will not navigate outside '
                   'the current web page when reviewing starts in the page. '
                   'Otherwise, review keys can walk out of the document.'))           
    self.newBool('AutoMode', True, 
              _('Automatically change navigation mode?'),
              _('When set, the navigation mode is automatically set to the '
                'most specific reading mode when the point of regard changes. '
                'Otherwise, reading modes are not updated.'))
    self.newNumeric('SearchDepth', 500, _('Search Depth'), 0, 5000, 0,
                  _('Number of nodes visited before iterative searches '
                    'are cancelled.  Affects all find, goto, and where '
                    'am I? functions.')) 

  def getGroups(self):
    '''
    Gets configurable settings for this L{Perk}.
    
    @return: Group of all configurable settings
    @rtype: L{AEState.Setting.Group}
    '''
    root = self.newGroup()
    root.append('BoundReview')
    root.append('AutoMode')
    root.append('SearchDepth')
    return root
    
class FirefoxPerk(Perk.Perk):
  '''
  Defines hotkeys to improve document browsing and chrome access for the 
  Firefox web browser.
  
  Flowcharts:
  Review task chaining: keyboard event -> BoundReview(All Review) -> 
  HandleAutofocus -> ConvReviewReg.
  Convenience review chaining: keyboard event -> ConvReview -> 
  BoundReview(All Review) -> HandleAutofocus -> ConvReviewReg.
  Review skip: ShouldSkip -> FFShouldSkip.
  Read role: ReadReviewRole(ReadNewRole)
  
  @cvar STATE: L{AEState} object that holds user setting values.
  @type STATE: L{AEState}
  @cvar SKIPPED_ITEMS: Additional items to be skipped when reviewing.
  @type SKIPPED_ITEMS: tuple
  @cvar SKIPPED_ROLES: Additional roles to be skipped when reviewing.
  @type SKIPPED_ROLES: tuple
  @cvar WIDGET_ROLES: A dictionary where the keys are roles which will
  prevent the convenience review keys from being registered. When a value is
  not None, it is a state that must be contained in this accessible's state set
  before convenience review keys are registered.
  @type WIDGET_ROLES: dictionary
  
  @ivar convkey_reg: Are convenience review keys registered?
  @type convkey_reg: boolean
  @ivar nav_element_reg: Are element navigation keys registered?
  @type nav_element_reg: boolean
  @ivar currentmode: The current navigation mode.
  @type currentmode: integer
  @ivar doc_pors: Dictionary containing saved pors.  Key is doc frame of web
  page to be restored.
  @type doc_pors: dictionary
  @ivar cached_pointer: L{POR} saved during caret event.  Used to restore POR 
  during certain restoration scenerios involving chrome.
  @type cached_pointer: L{POR}
  @ivar lastporinfocus: L{POR} saved during restoration routine to help
  differentiate tab browsing from Tab/Tab-Shift events.
  @type last_focus_por: L{POR}
  @ivar lastdocinfocus: L{POR} saved during restoration routine to help
  differentiate tab browsing from Tab/Tab-Shift events.
  @type lastdocinfocus: L{POR}
  @ivar viewlost: Flag that indicates a view lost event has just occurred.  
  Used during the saved L{POR} restoration routine.
  @type viewlost: boolean
  
  '''
  CONTAINERS_BY_ROLE =('image', 'form', 'list', 'calendar', 'menu', 'menu bar',
                'page tab', 'tool bar', 'application', 'tree', 'tree table', 'entry')
  TRIVIAL_WIDGETS = ('push button', '', 'document frame', 'check box', 'image')
  STATE = FirefoxPerkState
  SKIPPED_ITEMS = ()
  SKIPPED_ROLES = ()
  
  def init(self):
    self.doc_pors = {}
    self.cached_pointer = None
    self.lastporinfocus = None
    self.lastdocinfocus = None
    self.viewlost = False
    
    # Navigation mode related
    self.nav_element_reg = False
    self.convkey_reg = False
    self.currentmode = WIDGET_MODE
    self.savedmode = None
    
    # set an audio device as the default output
    self.setPerkIdealOutput('audio')
    
    # Register for events
    self.registerTask(ExtraCarets(None))
    self.registerTask(PointerCache(None))
    
    # Register general review related tasks
    self.registerTask(FirefoxReview('firefox review'))
    self.registerTask(ReadReviewRole('firefox review role'))
    self.registerTask(ReviewShouldSkip('firefox review should skip'))
    self.registerTask(FFReadNewContainer('firefox read new container'))
    
    # POR restoration related (Firefox tabbing, app switching, menu selection)
    self.registerTask(HandleDocumentPORView(None, all=True))
    self.registerTask(HandleDocumentPORFocus(None, all=True))
    
    # Register convenience review key related.  Registration order matters
    # ModeChangerOnView and ModeChangerOnFocus must come after
    # HandleDocumentPORView and HandleDocumentPORFocus, respectively.
    self.registerTask(ConvReview('firefox convenience review'))
    self.registerTask(ModeChangerOnFocus('firefox mode changer focus', 
                                         all=True))  
    self.registerTask(ModeChangerOnView('firefox mode changer view', 
                                        all=True))
    self.registerTask(ModeChangerOnUser('firefox mode changer user'))
    
    # Register user triggered move/announcement events
    self.registerTask(GoToActiveDoc('firefox go to activedoc'))
    self.registerTask(FFWhereAmINow('firefox where am i now'))
    self.registerTask(GoToDocStart('firefox go to doc start'))
    self.registerTask(GoToDocEnd('firefox go to doc end'))
    self.registerTask(GoToItemStart('firefox go to item start'))
    self.registerTask(GoToItemEnd('firefox go to item end'))
    self.registerTask(GoToContainer('firefox go to container'))
    self.registerTask(SkipContainer('firefox skip container'))
    # navigation by element
    self.registerTask(GoToElement('firefox go to next anchor'))
    self.registerTask(GoToElement('firefox go to next embed'))
    self.registerTask(GoToElement('firefox go to next button'))
    self.registerTask(GoToElement('firefox go to next check box'))
    self.registerTask(GoToElement('firefox go to next combo box'))
    self.registerTask(GoToElement('firefox go to next entry'))
    self.registerTask(GoToElement('firefox go to next form'))
    self.registerTask(GoToElement('firefox go to next form control'))
    self.registerTask(GoToElement('firefox go to next image'))
    self.registerTask(GoToElement('firefox go to next heading'))
    self.registerTask(GoToElement('firefox go to next list'))
    self.registerTask(GoToElement('firefox go to next list item'))
    self.registerTask(GoToElement('firefox go to next section'))
    self.registerTask(GoToElement('firefox go to next radio button'))
    self.registerTask(GoToElement('firefox go to next table'))
    self.registerTask(GoToElement('firefox go to next unvisited link'))
    self.registerTask(GoToElement('firefox go to next visited link'))
    self.registerTask(GoToElement('firefox go to prev anchor'))
    self.registerTask(GoToElement('firefox go to prev embed'))
    self.registerTask(GoToElement('firefox go to prev button'))
    self.registerTask(GoToElement('firefox go to prev check box'))
    self.registerTask(GoToElement('firefox go to prev combo box'))
    self.registerTask(GoToElement('firefox go to prev entry'))
    self.registerTask(GoToElement('firefox go to prev form'))
    self.registerTask(GoToElement('firefox go to prev form control'))
    self.registerTask(GoToElement('firefox go to prev image'))
    self.registerTask(GoToElement('firefox go to prev heading'))
    self.registerTask(GoToElement('firefox go to prev list'))
    self.registerTask(GoToElement('firefox go to prev list item'))
    self.registerTask(GoToElement('firefox go to prev section'))
    self.registerTask(GoToElement('firefox go to prev radio button'))
    self.registerTask(GoToElement('firefox go to prev table'))
    self.registerTask(GoToElement('firefox go to prev unvisited link'))
    self.registerTask(GoToElement('firefox go to prev visited link'))
    # navigation by landmark
    self.registerTask(GoToElement('firefox go to next banner'))
    self.registerTask(GoToElement('firefox go to next content info'))
    self.registerTask(GoToElement('firefox go to next definition'))
    self.registerTask(GoToElement('firefox go to next main'))
    self.registerTask(GoToElement('firefox go to next navigation'))
    self.registerTask(GoToElement('firefox go to next note'))
    self.registerTask(GoToElement('firefox go to next search'))
    self.registerTask(GoToElement('firefox go to next secondary'))
    self.registerTask(GoToElement('firefox go to next see also'))
    self.registerTask(GoToElement('firefox go to prev banner'))
    self.registerTask(GoToElement('firefox go to prev content info'))
    self.registerTask(GoToElement('firefox go to prev definition'))
    self.registerTask(GoToElement('firefox go to prev main'))
    self.registerTask(GoToElement('firefox go to prev navigation'))
    self.registerTask(GoToElement('firefox go to prev note'))
    self.registerTask(GoToElement('firefox go to prev search'))
    self.registerTask(GoToElement('firefox go to prev secondary'))
    self.registerTask(GoToElement('firefox go to prev see also'))
    # navigation by relation
    self.registerTask(CyclicGoToElement('firefox go to next similar'))
    self.registerTask(CyclicGoToElement('firefox go to prev similar'))
    self.registerTask(CyclicGoToElement('firefox go to next different'))
    self.registerTask(CyclicGoToElement('firefox go to prev different'))
    
    # read document URL
    self.registerTask(ReadDocAddressAndTitle('firefox read document address'))
    # read link preview
    self.registerTask(ReadLinkPreview('firefox read link preview'))
    # Register state watching tasks
    self.registerTask(HandleAutocomplete('firefox say autocomplete', 
                                         tier=True))
    # Register saved POR flushing task.  Runs once every minute.
    self.registerTask(SavedPORFlusher('firefox saved por flusher', 60000))
    
    # Add keyboard modifiers
    kbd = self.getInputDevice(None, 'keyboard')
    self.addInputModifiers(kbd, kbd.AEK_CAPS_LOCK)
    self.addInputModifiers(kbd, kbd.AEK_CONTROL_R)
    self.addInputModifiers(kbd, kbd.AEK_CONTROL_L)
    self.addInputModifiers(kbd, kbd.AEK_SHIFT_R)
    self.addInputModifiers(kbd, kbd.AEK_SHIFT_L)
    
    # Register keyboard commands for reading document address and move to 
    # active doc frame.
    self.registerCommand(kbd, 'firefox read document address', False, 
                         [kbd.AEK_CAPS_LOCK, kbd.AEK_Y])
    self.registerCommand(kbd, 'firefox go to activedoc', False, 
                         [kbd.AEK_CAPS_LOCK, kbd.AEK_HOME])
    self.registerCommand(kbd, 'firefox go to container', False, 
                         [kbd.AEK_CAPS_LOCK,kbd.AEK_UP])
    self.registerCommand(kbd, 'firefox skip container', False, 
                         [kbd.AEK_CAPS_LOCK,kbd.AEK_RIGHT])
                         
    self.registerCommand(kbd, 'firefox mode changer user', False, 
                         [kbd.AEK_CAPS_LOCK, kbd.AEK_SPACE])
    
    # chain to other tasks.  See Perk docstring for task flowcharts.
    self.chainTask('firefox review role', CHAIN_AROUND, 'read new role')
    self.chainTask('firefox review should skip', CHAIN_AROUND, 
                   'review should skip')
    self.chainTask('firefox where am i now', CHAIN_AROUND, 
                   'where am i now')
    self.chainTask('firefox read new container', CHAIN_BEFORE, 
                   'read new container')

    # register the firefox review task around all the basic review tasks that
    # move the pointer so we can account for specific cases and features
    names = self.getPerkTaskNames('ReviewPerk')
    for name in names:
      if (((name.startswith('review previous') or # reviewing, not peeking
            name.startswith('review next')) and
            not name.endswith('peek'))):
        self.chainTask('firefox review', CHAIN_AROUND, name)
        
  def getName(self):
    return _('Firefox')
  
  def getDescription(self):
    return _('Defines keyboard commands and settings to customize Firefox '
             'access.')
             
  def getDocFrame(self, por):
    '''
    Determines if a L{POR} is within or equal to the doc frame.
    
    @param por: L{POR} in question
    @type por: L{POR}
    @return: Is por within doc frame?
    @rtype: tuple
    '''
    if por == None:
      return None
    if self.hasAccRole(DOCUMENT_ROLE, por):
      return por
    return self.findAccByRole(DOCUMENT_ROLE, por=por, ancestor=True)

  def needsWidgetMode(self, por):
    '''
    Determines if the given L{POR} is a candidate to switch modes to widget 
    mode.
    
    @param por: Some point of regard
    @type por: L{POR}
    @return: Will the control need the arrow keys?
    @rtype: boolean
    '''
    role = self.getAccRole(por=por)
    return (self.hasAccState('focusable', por) and \
            role not in self.TRIVIAL_WIDGETS)
      
  def needsFocus(self, por):
    '''
    Determines if the given L{POR} should receive focus according to the 
    user settings and the accessible's role, states, and actions.
      
    @return: Does the given L{POR} need to be given focus according to the 
      user settings and the states of the accessible?
    @rtype: boolean
    '''
    role = self.getAccRole(por=por)
    if self.perk_state.AutoMode:
      return (self.hasAccState('focusable', por) and \
              not self.hasAccState('focused', por))
    else:
      return (self.hasAccState('focusable', por) and \
              not self.hasAccState('focused', por) and \
              role in self.TRIVIAL_WIDGETS)

  def getActiveDocFrame(self):
    '''
    Returns the active document frame as a L{POR}.
    
    @return: Point of regard to the document frame accessible
    @rtype: L{POR}
    '''
    frame = self.getViewRootAcc()
    relations = self.getAccRelations('embeds', por=frame)
    # Good enough for now. Might need to change this in future if more than one
    # relation is in the relation set.
    if relations:
      return relations[0]
    else:
      return None
    
  def getDocURI(self):
    return self.getAccAttrs(por=self.getActiveDocFrame())['DocURL']
  
  def getLastSearchAcc(self):
    '''
    Returns the last accessible to be searched.  Returns either the panel 
    containing the next doc frame or the accessible returned by getEndAcc.
    
    @return: Point of regard to the last accessible to be searched
    @rtype: L{POR}
    '''
    grandparent = self.getParentAcc(por= \
                  self.getParentAcc(por=self.perk.getActiveDocFrame()))     
    peer =  self.getNextPeerAcc(por=grandparent)
    if peer is None:
      return self.getEndAcc()
    else:
      return self.getChildAcc(0, por=peer)              
    
  def isLayoutTable(self, por, role):
    '''
    Determines if a table or cell is for layout only using Firefox's 
    suggestion in the layout-guess attribute.

    @param por: Point of regard to a potential layout table or cell
    @type por: L{POR}
    @param role: Pre-determined role of the L{POR}
    @type role: string    
    '''
    if role == 'table cell':
      table = self.getParentAcc(por=por)
      if table is None:
        return False
      attrs = self.getAccAttrs(por=table) 
      try:
        return (attrs is not None and attrs['layout-guess'] == 'true')
      except (AttributeError, KeyError):
        return False
    elif role == 'table':
      attrs = self.getAccAttrs(por=por) 
      try:
        return (attrs is not None and attrs['layout-guess'] == 'true')
      except (AttributeError, KeyError):
        return False
    else:
      return False        
              
  # ***** Navigation mode related *****************
  def setModeByPOR(self, por):
    '''
    Sets the mode based on a given L{POR}

    @param por: Point of regard to a potential layout table or cell
    @type por: L{POR}  
    @return: mode type
    @rtype: integer
    '''
    # Don't update modes if user does not want you to.
    if not self.perk_state.AutoMode:
      self.setMode(self.currentmode)
      return self.currentmode
    
    # unregister all modes if within chrome
    if self.getAccRole(por=por) != DOCUMENT_ROLE and \
                                         self.getDocFrame(por) is None:
      self.setMode(WIDGET_MODE)
      return WIDGET_MODE
    # we are in doc frame.  Select mode based on POR attributes, states etc.
    else:
      if not self.needsWidgetMode(por):
        self.setMode(DOCUMENT_MODE)  
        return DOCUMENT_MODE
      else:
        self.setMode(WIDGET_MODE)
        return WIDGET_MODE
           
  def setMode(self, modetype):
    '''
    Set the navigation mode based on given mode type.  Calls methods that 
    register key commands relevant to enable given mode.
    
    @param modetype: The type of mode to set
    @type por: integer
    @raise NameError: when modetype is not a valid mode
    '''
    # report any mode changes
    if modetype != self.currentmode:
      self.currentmode = modetype
      self.reportMode(modetype)
    # reg/unreg keys associated with each mode 
    if modetype == DOCUMENT_MODE:
      self.registerNavByElement()
      self.registerConvReview()
    elif modetype == TABLE_MODE:
      self.registerTableReview()
    elif modetype == WIDGET_MODE:
      self.unsetAllModes()
    else:
      raise NameError
    
  def unsetMode(self, modetype):
    '''
    Unsets a navigation mode based on given mode type.  Calls methods that 
    unregister key commands relevant to disable a given mode.
    
    @param modetype: The type of mode to unset
    @type por: integer
    '''
    if modetype is None:
      return
    elif modetype == DOCUMENT_MODE:
      self.unregisterNavByElement()
      self.unregisterConvReview()
    elif modetype == TABLE_MODE:
      self.unregisterTableReview()
    # WIDGET_MODE implies all navigation keys unregistered
    self.currentmode = WIDGET_MODE
      
  def unsetCurrentMode(self):
    '''
    Unsets the current mode
    '''
    if self.currentmode is not None:
      self.unsetMode(self.currentmode)
  
  def unsetAllModes(self):
    '''
    Unset all modes.
    '''
    for i in range(TOTAL_MODES-1):
      self.unsetMode(i)  
            
  def reportMode(self, mode):
    '''
    Reports (announces) given mode type.
    '''
    if mode is None:
      return
    # only announce mode changes that occur within doc frame
    if self.getDocFrame(self.task_por):
      if mode == DOCUMENT_MODE:
        self.sayInfo(text=_("entering document mode"))
      elif mode == TABLE_MODE:
        self.sayInfo(text=_("entering table mode"))
      elif mode == WIDGET_MODE:
        self.sayInfo(text=_("entering widget mode"))
      self.inhibitMayStop()
    
  def registerNavByElement(self):
    '''
    Registers keys related to navigation by element, navigation by landmark, 
    and navigation by relation.
    '''
    if not self.nav_element_reg:
      kbd = self.getInputDevice(None, 'keyboard')
      self.registerCommand(kbd, 'firefox go to next anchor', False, [kbd.AEK_A])
      self.registerCommand(kbd, 'firefox go to next embed', False, [kbd.AEK_D])
      self.registerCommand(kbd, 'firefox go to next button', False, [kbd.AEK_B])
      self.registerCommand(kbd, 'firefox go to next check box', False, [kbd.AEK_X])
      self.registerCommand(kbd, 'firefox go to next combo box', False, [kbd.AEK_C])
      self.registerCommand(kbd, 'firefox go to next entry', False, [kbd.AEK_E])
      self.registerCommand(kbd, 'firefox go to next form', False, [kbd.AEK_F])
      self.registerCommand(kbd, 'firefox go to next form control', False, [kbd.AEK_G])
      self.registerCommand(kbd, 'firefox go to next image', False, [kbd.AEK_I])
      self.registerCommand(kbd, 'firefox go to next heading', False, [kbd.AEK_H])
      self.registerCommand(kbd, 'firefox go to next list', False, [kbd.AEK_L])
      self.registerCommand(kbd, 'firefox go to next list item', False, [kbd.AEK_SEMICOLON])
      self.registerCommand(kbd, 'firefox go to next section', False, [kbd.AEK_S])
      self.registerCommand(kbd, 'firefox go to next radio button', False, [kbd.AEK_R])
      self.registerCommand(kbd, 'firefox go to next table', False, [kbd.AEK_T])
      self.registerCommand(kbd, 'firefox go to next unvisited link', False, [kbd.AEK_U])
      self.registerCommand(kbd, 'firefox go to next visited link', False, [kbd.AEK_V])
      # navigation by landmark
      self.registerCommand(kbd, 'firefox go to next banner', False, [kbd.AEK_OPEN_BRACKET])
      self.registerCommand(kbd, 'firefox go to next content info', False, [kbd.AEK_QUOTE])
      self.registerCommand(kbd, 'firefox go to next definition', False, [kbd.AEK_PERIOD])
      self.registerCommand(kbd, 'firefox go to next main', False, [kbd.AEK_M])
      self.registerCommand(kbd, 'firefox go to next navigation', False, [kbd.AEK_N])
      self.registerCommand(kbd, 'firefox go to next note', False, [kbd.AEK_O])
      self.registerCommand(kbd, 'firefox go to next search', False, [kbd.AEK_COMMA])
      self.registerCommand(kbd, 'firefox go to next secondary', False, [kbd.AEK_Y])
      self.registerCommand(kbd, 'firefox go to next see also', False, [kbd.AEK_P])
      # navigation by relation
      self.registerCommand(kbd, 'firefox go to next different', False, [kbd.AEK_J])
      self.registerCommand(kbd, 'firefox go to next similar', False, [kbd.AEK_K])
      # link preview
      modifiers = [kbd.AEK_CAPS_LOCK]
      for m in modifiers:
        self.registerCommand(kbd, 'firefox read link preview', False, [m,kbd.AEK_K])
        
      # now do get previous commands Shift-*
      modifiers = [kbd.AEK_SHIFT_R, kbd.AEK_SHIFT_L]
      for m in modifiers:
        self.registerCommand(kbd, 'firefox go to prev anchor', False, [m,kbd.AEK_A])
        self.registerCommand(kbd, 'firefox go to prev embed', False, [m,kbd.AEK_D])
        self.registerCommand(kbd, 'firefox go to prev button', False, [m,kbd.AEK_B])
        self.registerCommand(kbd, 'firefox go to prev check box', False, [m,kbd.AEK_X])
        self.registerCommand(kbd, 'firefox go to prev combo box', False, [m,kbd.AEK_C])
        self.registerCommand(kbd, 'firefox go to prev entry', False, [m,kbd.AEK_E])
        self.registerCommand(kbd, 'firefox go to prev form', False, [m,kbd.AEK_F])
        self.registerCommand(kbd, 'firefox go to prev form control', False, [m,kbd.AEK_G])
        self.registerCommand(kbd, 'firefox go to prev image', False, [m,kbd.AEK_I])
        self.registerCommand(kbd, 'firefox go to prev heading', False, [m,kbd.AEK_H])
        self.registerCommand(kbd, 'firefox go to prev list', False, [m,kbd.AEK_L])
        self.registerCommand(kbd, 'firefox go to prev list item', False, [m,kbd.AEK_SEMICOLON])
        self.registerCommand(kbd, 'firefox go to prev section', False, [m,kbd.AEK_S])
        self.registerCommand(kbd, 'firefox go to prev radio button', False, [m,kbd.AEK_R])
        self.registerCommand(kbd, 'firefox go to prev table', False, [m,kbd.AEK_T])
        self.registerCommand(kbd, 'firefox go to prev unvisited link', False, [m,kbd.AEK_U])
        self.registerCommand(kbd, 'firefox go to prev visited link', False, [m,kbd.AEK_V])
        # navigation by landmark
        self.registerCommand(kbd, 'firefox go to prev banner', False, [m,kbd.AEK_OPEN_BRACKET])
        self.registerCommand(kbd, 'firefox go to prev content info', False, [m,kbd.AEK_QUOTE])
        self.registerCommand(kbd, 'firefox go to prev definition', False, [m,kbd.AEK_PERIOD])
        self.registerCommand(kbd, 'firefox go to prev main', False, [m,kbd.AEK_M])
        self.registerCommand(kbd, 'firefox go to prev navigation', False, [m,kbd.AEK_N])
        self.registerCommand(kbd, 'firefox go to prev note', False, [m,kbd.AEK_O])
        self.registerCommand(kbd, 'firefox go to prev search', False, [m,kbd.AEK_COMMA])
        self.registerCommand(kbd, 'firefox go to prev secondary', False, [m,kbd.AEK_Y])
        self.registerCommand(kbd, 'firefox go to prev see also', False, [m,kbd.AEK_P])
        # navigation by relation
        self.registerCommand(kbd, 'firefox go to prev different', False, [m,kbd.AEK_J])
        self.registerCommand(kbd, 'firefox go to prev similar', False, [m,kbd.AEK_K])
      # some additional document mode reading commands  
      self.registerCommand(kbd, 'firefox go to item start', False, [kbd.AEK_HOME])
      self.registerCommand(kbd, 'firefox go to item end', False, [kbd.AEK_END])
      modifiers = [kbd.AEK_CONTROL_R, kbd.AEK_CONTROL_L]
      for m in modifiers:
        self.registerCommand(kbd, 'firefox go to doc start', False, [m, kbd.AEK_HOME])
        self.registerCommand(kbd, 'firefox go to doc end', False, [m, kbd.AEK_END])
      # set our flag
      self.nav_element_reg = True
  
  def unregisterNavByElement(self):
    '''
    Unregisters keys related to navigation by element, navigation by landmark, 
    and navigation by relation.
    '''
    if self.nav_element_reg:
      kbd = self.getInputDevice(None, 'keyboard')
      self.unregisterCommand(kbd, [kbd.AEK_A])
      self.unregisterCommand(kbd, [kbd.AEK_D])
      self.unregisterCommand(kbd, [kbd.AEK_B])
      self.unregisterCommand(kbd, [kbd.AEK_X])
      self.unregisterCommand(kbd, [kbd.AEK_C])
      self.unregisterCommand(kbd, [kbd.AEK_E])
      self.unregisterCommand(kbd, [kbd.AEK_F])
      self.unregisterCommand(kbd, [kbd.AEK_G])
      self.unregisterCommand(kbd, [kbd.AEK_I])
      self.unregisterCommand(kbd, [kbd.AEK_H])
      self.unregisterCommand(kbd, [kbd.AEK_L])
      self.unregisterCommand(kbd, [kbd.AEK_SEMICOLON])
      self.unregisterCommand(kbd, [kbd.AEK_S])
      self.unregisterCommand(kbd, [kbd.AEK_R])
      self.unregisterCommand(kbd, [kbd.AEK_T])
      self.unregisterCommand(kbd, [kbd.AEK_U])
      self.unregisterCommand(kbd, [kbd.AEK_V])
      # navigation by landmark
      self.unregisterCommand(kbd, [kbd.AEK_OPEN_BRACKET])
      self.unregisterCommand(kbd, [kbd.AEK_QUOTE])
      self.unregisterCommand(kbd, [kbd.AEK_PERIOD])
      self.unregisterCommand(kbd, [kbd.AEK_M])
      self.unregisterCommand(kbd, [kbd.AEK_N])
      self.unregisterCommand(kbd, [kbd.AEK_O])
      self.unregisterCommand(kbd, [kbd.AEK_COMMA])
      self.unregisterCommand(kbd, [kbd.AEK_Y])
      self.unregisterCommand(kbd, [kbd.AEK_P])
      # navigation by relation
      self.unregisterCommand(kbd, [kbd.AEK_J])
      self.unregisterCommand(kbd, [kbd.AEK_K])
      # link preview
      modifiers = [kbd.AEK_CAPS_LOCK]
      for m in modifiers:
        self.unregisterCommand(kbd, [m,kbd.AEK_K])
      
      # now do get previous commands Shift-*
      modifiers = [kbd.AEK_SHIFT_R, kbd.AEK_SHIFT_L]
      for m in modifiers:
        self.unregisterCommand(kbd, [m,kbd.AEK_A])
        self.unregisterCommand(kbd, [m,kbd.AEK_D])
        self.unregisterCommand(kbd, [m,kbd.AEK_B])
        self.unregisterCommand(kbd, [m,kbd.AEK_X])
        self.unregisterCommand(kbd, [m,kbd.AEK_C])
        self.unregisterCommand(kbd, [m,kbd.AEK_E])
        self.unregisterCommand(kbd, [m,kbd.AEK_F])
        self.unregisterCommand(kbd, [m,kbd.AEK_G])
        self.unregisterCommand(kbd, [m,kbd.AEK_I])
        self.unregisterCommand(kbd, [m,kbd.AEK_H])
        self.unregisterCommand(kbd, [m,kbd.AEK_L])
        self.unregisterCommand(kbd, [m,kbd.AEK_SEMICOLON])
        self.unregisterCommand(kbd, [m,kbd.AEK_S])
        self.unregisterCommand(kbd, [m,kbd.AEK_R])
        self.unregisterCommand(kbd, [m,kbd.AEK_T])
        self.unregisterCommand(kbd, [m,kbd.AEK_U])
        self.unregisterCommand(kbd, [m,kbd.AEK_V])
        # navigation by landmark
        self.unregisterCommand(kbd, [m,kbd.AEK_OPEN_BRACKET])
        self.unregisterCommand(kbd, [m,kbd.AEK_QUOTE])
        self.unregisterCommand(kbd, [m,kbd.AEK_PERIOD])
        self.unregisterCommand(kbd, [m,kbd.AEK_M])
        self.unregisterCommand(kbd, [m,kbd.AEK_N])
        self.unregisterCommand(kbd, [m,kbd.AEK_O])
        self.unregisterCommand(kbd, [m,kbd.AEK_COMMA])
        self.unregisterCommand(kbd, [m,kbd.AEK_Y])
        self.unregisterCommand(kbd, [m,kbd.AEK_P])
        # navigation by relation
        self.unregisterCommand(kbd, [m,kbd.AEK_J])
        self.unregisterCommand(kbd, [m,kbd.AEK_K])
      # some additional document mode reading commands  
      self.unregisterCommand(kbd, [kbd.AEK_HOME])
      self.unregisterCommand(kbd, [kbd.AEK_END])
      modifiers = [kbd.AEK_CONTROL_R, kbd.AEK_CONTROL_L]
      for m in modifiers:
        self.unregisterCommand(kbd, [m, kbd.AEK_HOME])
        self.unregisterCommand(kbd, [m, kbd.AEK_END]) 
      # set our flag 
      self.nav_element_reg = False
             
  def registerConvReview(self):
    '''
    Registers convenience review keys.  Does not attempt to register if
    already registered
    '''
    if not self.convkey_reg:
      kbd = self.getInputDevice(None, 'keyboard')
      self.registerCommand(kbd, 'firefox convenience review', False, 
                            [kbd.AEK_LEFT])
      self.registerCommand(kbd, 'firefox convenience review', False, 
                            [kbd.AEK_RIGHT])
      self.registerCommand(kbd, 'firefox convenience review', False, 
                            [kbd.AEK_UP])
      self.registerCommand(kbd, 'firefox convenience review', False, 
                            [kbd.AEK_DOWN])
                            
      modifiers = [[kbd.AEK_CONTROL_R], [kbd.AEK_CONTROL_L]]
      keys = [kbd.AEK_LEFT, kbd.AEK_RIGHT]        
      for modifier in modifiers:
        for key in keys:
          self.registerCommand(kbd, 'firefox convenience review', False,
                                modifier+[key])
      self.convkey_reg = True
  
  def unregisterConvReview(self):
    '''
    Unregisters convenience review keys.  Does not attempt to unregister if
    already unregistered
    '''
    if self.convkey_reg:
      kbd = self.getInputDevice(None, 'keyboard')
      self.unregisterCommand(kbd, [kbd.AEK_LEFT])
      self.unregisterCommand(kbd, [kbd.AEK_RIGHT])
      self.unregisterCommand(kbd, [kbd.AEK_UP])
      self.unregisterCommand(kbd, [kbd.AEK_DOWN])
                          
      modifiers = [[kbd.AEK_CONTROL_R], [kbd.AEK_CONTROL_L]]
      keys = [kbd.AEK_LEFT, kbd.AEK_RIGHT]        
      for modifier in modifiers:
        for key in keys:
          self.unregisterCommand(kbd, modifier+[key])
      self.convkey_reg = False 
      
  def registerTableReview(self):
    '''
    Registers keys related to table navigation.
    '''
    pass
   # print 'FirefoxPerk: registerTableReview not implemented'
    
  def unregisterTableReview(self):
    '''
    Unregisters keys related to table navigation.
    '''
    pass
   # print 'FirefoxPerk: unregisterTableReview not implemented' 
   
  # ****** Search and go to predicates ******
  def rolePredicate(self, por, role=None, **kwargs):
    ''' Generic predicate used for a single role comparison ''' 
    return (self.hasAccRole(role, por=por))
  
  def attrPredicate(self, por, key=None, value=None, **kwargs):
    ''' Generic predicate used for a single key/value attribute comparison '''
    testattrs = self.getAccAttrs(por=por)
    if testattrs is None:
      return False
    try:
      if testattrs[key] == value:
        return True
    except (TypeError, KeyError):
      return False
    
  def entryPredicate(self, por):
    ''' Predicate used for finding entry/password text accessibles '''
    role = self.getAccRole(por=por)
    return (role == 'entry' or role == 'password text')
         
  def formcontrolPredicate(self, por):
    ''' Predicate used for finding next form control '''
    try:
      attrs = self.getAccAttrs(por=por)
      return attrs['tag'] == 'INPUT' or attrs['tag'] == 'SELECT'
    except (TypeError, KeyError):
      return False
  
  def unvisitedlinkPredicate(self, por):
    ''' Predicate used for finding an unvisited link '''
    try:
      return (self.getAccAttrs(por=por)['tag'] == 'A'  \
              and not self.hasAccState('visited', por=por)) 
    except (TypeError, KeyError):
      return False
  
  def visitedlinkPredicate(self, por):
    ''' Predicate used for finding a visited link '''
    try:
      return (self.getAccAttrs(por=por)['tag'] == 'A' \
              and self.hasAccState('visited', por=por)) 
    except (TypeError, KeyError):
      return False
  
  def tablePredicate(self, por, **kwargs):
    return (self.getAccRole(por=por) == 'table' and \
            not self.isLayoutTable(por, 'table'))
               
  
class HandleAutocomplete(Task.StateTask):
  '''
  Announces autocomplete menu opening
  '''
  def execute(self, por, name, value, **kwargs):
    if name == 'visible' and value == 1:
      role = self.getAccRole(por)
      if role == 'tree' or role == 'tree table':
        self.sayState(text=_('autocomplete menu opened'))
  
class ModeChangerOnView(Task.ViewTask):
  '''
  Sets the proper reading mode on a view event.  The mode will always be set
  to WIDGET_MODE on a view lost event, while the view gained event will
  pass and let the focus gained event do the work.
  '''
  def executeLost(self, por, **kwargs):
    if self.perk.savedmode is None:
      self.perk.savedmode = self.perk.currentmode
    self.perk.setMode(WIDGET_MODE)

class ModeChangerOnFocus(Task.FocusTask):
  '''
  Sets proper reading mode on a focus event.  The mode is based on the saved 
  mode (obtained when leaving doc frame), if available, or the current L{POR}. 
  '''   
  def executeLost(self, por, **kwargs):
    if not self.perk.getDocFrame(por):
      if self.perk.savedmode is None:
        self.perk.savedmode = self.perk.currentmode
      self.perk.setMode(WIDGET_MODE)
      
  def executeGained(self, por, **kwargs):
    # if in doc frame restore saved mode if possible else set mode according to
    # current por
    if self.perk.getDocFrame(por):
      if self.perk.savedmode is not None:
        self.perk.setMode(self.perk.savedmode)
        self.perk.savedmode = None
      else:
        self.perk.setModeByPOR(por)
    # in chrome.  store mode and set to WIDGET_MODE
    else:
      if self.perk.savedmode is None:
        self.perk.savedmode = self.perk.currentmode
      self.perk.setMode(WIDGET_MODE)
    
class ModeChangerOnUser(Task.InputTask):
  '''
  Allows the user to enter or leave the suggested navigation mode.
    
  @todo: adhere to new mode method
  @param por: Point of regard to test
  @type por: L{POR}
  '''
  def execute(self, **kwargs):
    if self.perk_state.AutoMode:
      if self.perk.currentmode == DOCUMENT_MODE:
        currentmode = self.perk.setModeByPOR(self.task_por)
        if currentmode == DOCUMENT_MODE:
          self.sayInfo(text=_('still in document mode'))
      elif self.perk.currentmode == TABLE_MODE:
        pass
      elif self.perk.currentmode == WIDGET_MODE:
        self.perk.setMode(DOCUMENT_MODE)
        # set focus if need be
        if self.perk.needsFocus(self.task_por):
          self.setAccPOR()
    else:
      if self.perk.currentmode == DOCUMENT_MODE:
        self.perk.setMode(WIDGET_MODE)
      else:
        self.perk.setMode(DOCUMENT_MODE)
        # set focus if need be
        if self.perk.needsFocus(self.task_por):
          self.setAccPOR()
       
class ConvReview(Task.InputTask):
  '''
  Provides convenience key functionality by calling the appropriate 
  review task. 
  
  Review keys are registered/unregistered with the focus and view event 
  handlers, L{ModeChangerOnFocus} and L{ModeChangerOnView} respectively.  They
  force the review keys to be active only within a document frame. 
  '''
  def execute(self, gesture=None, **kwargs):    
    kbd = self.getInputDevice(None, 'keyboard')
    # handle item first, most common
    if kbd.AEK_UP in gesture:
      self.doTask('review previous item')
    elif kbd.AEK_DOWN in gesture:
      self.doTask('review next item')
    # check for word before character, since it's the more specific case
    elif kbd.AEK_LEFT in gesture and \
          (kbd.AEK_CONTROL_R in gesture or kbd.AEK_CONTROL_L in gesture):
      self.doTask('review previous word')
    elif kbd.AEK_RIGHT in gesture and \
          (kbd.AEK_CONTROL_R in gesture or kbd.AEK_CONTROL_L in gesture):
      self.doTask('review next word')
    # now check character
    elif kbd.AEK_LEFT in gesture:
      self.doTask('review previous char')
    elif kbd.AEK_RIGHT in gesture:
      self.doTask('review next char')
      
class HandleDocumentPORView(Task.ViewTask):
  '''
  Returns to the last user point of regard in a document after the Firefox 
  window is reactivated.
  '''
  def executeLost(self, por, **kwargs):
    '''
    Store the pointer using it's doc frame as key.
    '''
    self.perk.viewlost = True
    doc = self.perk.getDocFrame(self.getPointerPOR())
    if doc is not None:
      self.perk.doc_pors[doc] = self.getPointerPOR()
    # clear cached pointer that is set during caret event
    self.perk.cached_pointer = None
           
class HandleDocumentPORFocus(Task.FocusTask):
  '''
  Tracks the last position reviewed in a document so that it may become the
  user's L{POR} again after focus is restored to the document.
  '''  
  def executeGained(self, por, **kwargs):
    '''
    Try to restore the pointer.  Use the current doc frame as index into
    perk.doc_pors.
    '''
    # clear cached pointer that is set during caret event and only used in 
    # executeLost.
    self.perk.cached_pointer = None
    
    # get a local copy of viewlost.  viewlost is set in view executeLost and 
    # is used for restoration after an application switch
    viewlost = self.perk.viewlost
    self.perk.viewlost = False
    
    # get a local copy of the last por that was in focus.  This is used to
    # help differentiate menu selections from tab/tab-shift events.
    lastporinfocus = self.perk.lastporinfocus
    self.perk.lastporinfocus = por
    
    # is the user tabbing or tab browsing
    currentdocinfocus = self.perk.getActiveDocFrame()
    if self.perk.lastdocinfocus is None or \
       self.perk.lastdocinfocus != currentdocinfocus:
      tabbrowsing = True
    else:
      tabbrowsing = False
    self.perk.lastdocinfocus = currentdocinfocus
    
    # try to restore the saved POR for all tab browsing events
    if tabbrowsing:
      # try to restore pointer for this doc frame
      try:
        # only restore if we are within doc frame
        if self.perk.getDocFrame(self.task_por):
          # announce window title if restoration will occur
          if self.perk.doc_pors.has_key(currentdocinfocus):
            self.inhibitMayStop()
            self.sayWindow()
          # try to do restoration  
          self.doTask('pointer to por',
                              por=self.perk.doc_pors[currentdocinfocus])
          # consume the next selector to block selector announcements
          self.blockNTasks(1, Task.SelectorTask)
          # register/unregister convenience keys after pointer move
          self.perk.setModeByPOR(self.task_por)
          # don't propagate task chain
          return False
        else:
          return True
      except KeyError:
        # the selector event will make announcement
        return False
    # user is hitting tab/tab-shift.    
    else:
      lastrole = self.getAccRole(lastporinfocus)    
   
      # Try to restore only for certain cases that are misinterpreted
      # as a tab/tab-shift event.  This includes when a view lost event
      # occured
      if viewlost or ((lastrole == 'menu' or lastrole == 'menu item') \
                       and self.perk.getDocFrame(por)): 
        try:
          # only restore if we are within doc frame
          if self.perk.getDocFrame(self.task_por):
            self.doTask('pointer to por',
                                por=self.perk.doc_pors[currentdocinfocus])
            # consume the next selector to block selector announcements
            self.blockNTasks(1, Task.SelectorTask)
            # register/unregister convenience keys after pointer move
            self.perk.setModeByPOR(self.task_por)
            # don't propagate task chain
            return False
          else:
            return True
        except KeyError:
          # let propagated events make announcements
          return True
    
  def executeLost(self, por, **kwargs):
    '''
    Store the pointer using it's container doc frame as index if both task_por and
    pointer are with doc frame.
    '''
    # get the cached pointer from caret event or the current pointer
    pointer = self.perk.cached_pointer or self.getPointerPOR()
    # If the current por is the doc frame, make sure pointer is also within
    # the doc frame.  Store it if it is.
    if self.hasAccRole(DOCUMENT_ROLE, por):
      if self.perk.getDocFrame(pointer) is not None:
        self.perk.doc_pors[por] = pointer
    else:
      # find doc frame of current pointer.  Store pointer if found.
      doc = self.perk.getDocFrame(pointer)
      if doc is not None:
        self.perk.doc_pors[doc] = pointer 
    # clear cached pointer that is set during caret event
    self.perk.cached_pointer = None
   
    
class PointerCache(Task.CaretTask):
  '''  
  Saves pointer for document, fixes case where tabbing from page with focus in
  document frame to page with focus in URL bar. Without this fix a caret event
  switches the pointer before the focus event. Must be the
  first caret event handler.
  '''
  def execute(self, por, **kwargs):
    if self.perk.getDocFrame(self.getPointerPOR()) and \
                         not self.perk.getDocFrame(por):
      self.perk.cached_pointer = self.getPointerPOR()
      
    
class ExtraCarets(Task.CaretTask):
  '''
  Ignores extra caret events sent after selection of in-page content that
  this is not editable.
  '''
  def execute(self, por, **kwargs):
    if not self.hasAccState('editable'):
      return False

class ReadDocAddressAndTitle(Task.InputTask):
  '''
  Reads the address of the current document.
  '''
  def execute(self, cycle_count, **kwargs):
    '''
    Cyclical task.  1) Announces the window title.  2) Does a search of the 
    accessible hierarchy for the urlbar then finds the entry field in 
    the urlbar to announce its text.
    '''
    self.stopNow()
    if cycle_count % 3 == 0:
      self.sayWindow()
    elif cycle_count % 3 == 1:
      attrs = self.getAccAttrs(por=self.perk.getActiveDocFrame())
      self.sayItem(text=_(attrs['DocURL']))
    else:
      self._readPageSummary()
      
  def _readPageSummary(self):
    '''
    Reads the quantity of headings, forms, tables, visited and unvisited links, 
    plus the locale.
    
    @todo: optimize with L{Collections}.
    '''
    headings = 0 
    forms = 0
    tables = 0
    vlinks = 0
    uvlinks = 0
    nodetotal = 0
    
    # set start of iteration
    startpor = self.perk.getActiveDocFrame()
    # set end of iteration, either next tab panel or last acc
    endpor = self.perk.getLastSearchAcc()
    # begin traversal of document
    for i in self.iterNextItems(por=startpor, wrap=True):
      # track the number of nodes seen
      nodetotal += 1
      # safety net in case of cyclical navigation/ very long document
      if nodetotal > self.perk_state.SearchDepth:
        sayInfo(_('search depth reached'))
        return
      # some politeness
      if nodetotal == 200:
        self.sayInfo(text=_('one moment please'))
        time.sleep(.1)
      if not nodetotal % 1000:
        self.sayInfo(text=_('%d nodes searched' %nodetotal))
        time.sleep(.1)
      # search is over when endpor is seen
      if i == endpor:
        break
      role = self.getAccRole(por=i)
      if role == 'heading':
        headings += 1
      elif role == 'form':
        forms += 1
      elif role == 'table' and self.perk.isLayoutTable(i, role):
        tables += 1
      elif self.perk.visitedlinkPredicate(i):
        vlinks += 1
      elif self.perk.unvisitedlinkPredicate(i):
        uvlinks += 1
    # get our locale    
    locale = self.getAccAppLocale()
    # form output and send it
    text = (headings, forms, tables, vlinks, uvlinks, locale)
    template = _('%d headings. %d forms. %d tables. %d visited links. \
                  %d unvisited links. locale is %s')
    self.sayInfo(text=text, template=template)
    
class FirefoxReview(Task.InputTask):
  '''
  This L{Task} is chained around all previous/next review tasks so that it can
  handle cases specific to Firefox. It provides the following features.
  
  1. Registration and unregistration of convenience review keys when reviewing
     over unfocusable and focusable elements in the document frame.
  2. Giving focus to focusable elements encountered during review while 
     respecting user settings.
  3. Bounding the review to the active document while respecting user settings.
  '''
  def _isInDoc(self, por=None):
    '''
    @return: Is the given L{POR} inside the document frame or is it the 
      document frame itself?
    @rtype: boolean
    '''
    return (self.findAccByRole(DOCUMENT_ROLE, por, ancestor=True) is not None 
            or self.hasAccRole(DOCUMENT_ROLE, por))
  
  def _needsBounding(self):
    '''
    @return: Should the review be bounded to the current document frame 
      according to the user settings?
    @rtype: boolean
    '''
    return self.getPerkSettingVal('BoundReview')
  
  def _setScroll(self, por):
    '''
    Scrolls the screen to bring the por visible trying to account for bullets
    and other oddities.
    
    @param por: Point of regard to scroll in view
    @type por: L{POR}
    '''
    role = self.getAccRole(por=por)
    if role == 'list item' or role == 'menu item':
      if self.getAnchorTaskId().find(' next ') > 0:
        por = self.getNextItem(por=por, wrap=True)
      else:
        por = self.getPrevItem(por=por, wrap=True)  
    self.setAccCaret(por=por)
  
  def execute(self, **kwargs):
    # start off assuming we're going to call the arounded review task
    original = True
    # start off assuming we're going to let other tasks execute in the chain
    propagate = True
    # check if we're inside or outside the document to start
    in_doc = self._isInDoc()
    # check whether the next/previous chunk we're reviewing is inside or 
    # outside the document
    self.doTask('%s peek' % self.getAnchorTaskId(), 
                            onlyvisible=False, chain=False)
    next_por = self.getTempVal('peek')
    next_in_doc = self._isInDoc(next_por)
    # now determine whether we're 1) in doc to in doc, 2) out of doc to in doc,
    # 3) in doc to out of doc, or 4) out of doc to out of doc
    # navigating within document or into the document (1 and 2)
    if next_in_doc:
      # moving caret programmatically scrolls screen and brings por into view.
      # need to set the next item to account for lists.  Must be called before 
      # any other set focus type calls
      self._setScroll(next_por) 
      # check if the item should get the focus
      # edge case where we just entered doc frame from chrome
      if not in_doc:
        # set the focus to the document frame as we pass by it
        self.setAccFocus(self.perk.getActiveDocFrame())
      elif self.perk.needsFocus(next_por):
        # set focus to a manipulable
        self.setAccPOR(next_por)
        # inhibit the focus announcement to follow
        original = False
        propagate = False
      # register/unregister convenience review keys 
      self.perk.setModeByPOR(next_por)
    elif in_doc and not next_in_doc:
      # navigating out of the document (3)
      # check if the user wants the review to hit a wall here
      if self._needsBounding():
        # we're at the edge of a document, set the review return value properly
        anchor = self.getAnchorTaskId()
        if anchor.startswith('review next'):
          # at the end
          self.setTempVal('review', AEConstants.REVIEW_NO_NEXT_ITEM)
        elif anchor.startswith('review previous'):
          # at the beginning
          self.setTempVal('review', AEConstants.REVIEW_NO_PREV_ITEM)
        # do not call the arounded task as we're setting the bound flags 
        # ourselves
        original = False
      else:
        # unregister review keys
        self.perk.setModeByPOR(next_por)
    # navigating in the chrome (4)
    # do nothing, let the review procede as normal
    if original:
      # execute the anchor, but do not let its immediate chains run too since
      # that would end up re-executing this task ad infinitum
      self.doTask(self.getAnchorTaskId(), chain=False)
    return propagate

class ReadReviewRole(Task.Task):
  '''
  Task is chained around (overrides) 'read new role'. Speaks the role 
  of the given L{POR} or substitutes an output based on the role type.
  Speaks the role of the given L{POR} or the L{task_por} if no L{POR} is 
  given only if it is different than the last role spoken by this method or
  if the RepeatRole setting is True.
    
  @param por: Point of regard to the accessible whose role should be said, 
    defaults to L{task_por} if None
  @type por: L{POR}
  '''
  def execute(self, por=None, role=None, **kwargs):
    rolename = self.getAccRoleName(por) or _('link') 
    # get untranslated role name
    role = self.getAccRole(por) 
    if role == 'heading':
      attrs = self.getAccAttrs()
      if attrs is not None and attrs.has_key('level'):
        # i18n: 1st string is translated word 'heading', second is value 1 
        # through 6
        outtext = _("%s level %s") % (rolename, attrs['level'])
      else:
        outtext = rolename
    else: # default case
      outtext = rolename
        
    if not ((role in self.perk.SKIPPED_ROLES) or
            (role == 'table cell' and self.perk.isLayoutTable(por, role))):
      # invoke the anchor with our pre-computed text
      self.doTask(self.getAnchorTaskId(), role=outtext, por=por, chain=False,
                  **kwargs)

class ReviewShouldSkip(Task.InputTask):
  '''
  Task is chained after 'review should skip' and provides additional rules to
  determines if the current L{POR} should be skipped or not based on the
  role of the accessible.
    
  @param por: Point of regard to test
  @type por: L{POR}
  '''
  def execute(self, peek, por=None, **kwargs):
    text = self.getItemText(por)
    role = self.getAccRole(por=por)
    layouttable = self.perk.isLayoutTable(por, role)
    if (text.strip() or self.hasOneAccState(por, 'focusable', 'editable')):
      # has content or can have content from the user, don't skip
      self.setTempVal('should skip', False)
      return 
    
    skipping = self.getPerkSettingVal('Skipping', perk_name='ReviewPerk')
    if skipping == AEConstants.SKIP_ALL:
      self.setTempVal('should skip', True)
    elif skipping == AEConstants.SKIP_NONE:
      self.setTempVal('should skip', False)
    elif skipping == AEConstants.SKIP_REPORT:
      if not peek:
        if (role == 'table' or role == 'table cell') and layouttable:
          # if we're on a layout table
          # update the task POR so it points to it
          self.moveToPOR(por)
          # DO NOT announce anything or set any skipping flags
          # we didn't report anything so we don't want to inhibitMayStop
          # by accident
        else:
          # update the task POR to the next location
          self.moveToPOR(por)
          # if we're not on a layout table
          # then invoke the review report task
          self.doTask('review skip report')
          # indicate something has been skipped and reported
          self.setTempVal('has skipped', True)
      else:
        # indicate something will be skipped and reported
        self.setTempVal('will skip', True)
      # always give the peek hint that something should be skipped
      self.setTempVal('should skip', True)

class GoToActiveDoc(Task.InputTask):
  '''
  Moves pointer to the restore por location if available and user is 
  navigating chrome, otherwise pointer is set to the active document frame.
    
  @param por: Point of regard to test
  @type por: L{POR}
  '''
  def execute(self, por=None, **kwargs):
    self.stopNow()
    docframe = self.perk.getActiveDocFrame()
    try:
      # try to use it's restore por location if in chrome
      if not self.perk.getDocFrame(self.task_por):
        self.doTask('pointer to por', por=self.perk.doc_pors[docframe])
      else:
        self.doTask('pointer to por', por=docframe)
    except:
      # just set it to the current docframe
      self.doTask('pointer to por', por=docframe)
    # set reading mode
    self.perk.setModeByPOR(self.task_por)
    
class GoToDocStart(Task.InputTask):
  '''
  Moves pointer to the active document frame.
    
  @param por: Point of regard to test
  @type por: L{POR}
  '''
  def execute(self, por=None, **kwargs):
    self.stopNow()
    self.doTask('pointer to por', por=self.perk.getActiveDocFrame())
    # set reading mode
    self.perk.setModeByPOR(self.task_por)
    
class GoToDocEnd(Task.InputTask):
  '''
  Moves pointer to the end of the active document frame.
    
  @param por: Point of regard to test
  @type por: L{POR}
  '''
  def execute(self, por=None, **kwargs):
    self.stopNow()
    endacc = self.getLastAccUnder(por=self.perk.getActiveDocFrame())
    self.doTask('pointer to por', por=endacc)
    # set reading mode
    self.perk.setModeByPOR(self.task_por)
     
class GoToItemStart(Task.InputTask):
  '''
  Moves pointer to the start of the current item
    
  @param por: Point of regard to test
  @type por: L{POR}
  '''
  def execute(self, por=None, **kwargs):
    self.stopNow()
    itemstart = self.getCurrItem()
    self.doTask('pointer to por', por=itemstart)
    # set reading mode todo: is this necessary?  would modes change?
    self.perk.setModeByPOR(self.task_por)
    
class GoToItemEnd(Task.InputTask):
  '''
  Moves pointer to the end of the current item
    
  @param por: Point of regard to test
  @type por: L{POR}
  '''
  def execute(self, por=None, **kwargs):
    self.stopNow()
    itemend = self.getLastChar()
    self.doTask('pointer to por', por=itemend)
    # set reading mode todo: is this necessary?  would modes change?
    self.perk.setModeByPOR(self.task_por)

class GoToElement(Task.InputTask):
  '''
  Task used for searching for an element.  The search criteria and
  direction of search are derived from the task identity associated to this
  task.  The pointer is updated and the L{POR} is announced upon finding the 
  accessible, otherwise the pointer is NOT moved and an announcement declaring
  that the accessible has not been found is made.
  
  Currently used by navigation by element and navigation by landmark.
  '''
  def execute(self, **kwargs):
    pred = None
    # keyword arguments passed to predicate
    kwargs = {}
    taskid = self.getIdentity()
    # default the output name to it's role. Change the outputname to something
    # else, if the need arises, within the big if statement.
    outputname = taskid.split(' ')[-1]
    # set predicate (and it's kwargs) and or outputname according to last word 
    # in task identity
    if taskid.endswith('anchor'):
      pred = self.perk.attrPredicate
      kwargs = {'key':'tag','value':'A'}
      outputname = 'link'
    elif taskid.endswith('embed'):
      pred = self.perk.attrPredicate
      kwargs = {'key':'tag','value':'EMBED'}
      outputname = 'embed'
    elif taskid.endswith('radio button'):
      pred = self.perk.rolePredicate
      kwargs = {'role':'radio button'}
      outputname = 'radio button'
    elif taskid.endswith('button'):
      pred = self.perk.rolePredicate
      kwargs = {'role':'push button'}
      outputname = 'push button'
    elif taskid.endswith('check box'):
      pred = self.perk.rolePredicate
      kwargs = {'role':'check box'}
      outputname = 'check box'
    elif taskid.endswith('combo box'):
      pred = self.perk.rolePredicate
      kwargs = {'role':'combo box'}
      outputname = 'combo box'
    elif taskid.endswith('entry'):
      pred = self.perk.entryPredicate
    elif taskid.endswith('form'):
      pred = self.perk.rolePredicate
      kwargs = {'role':'form'}
    elif taskid.endswith('form control'):
      pred = self.perk.formcontrolPredicate
      outputname = 'form control'
    elif taskid.endswith('image'):
      pred = self.perk.rolePredicate
      kwargs = {'role':'image'}
    elif taskid.endswith('heading'):
      pred = self.perk.rolePredicate
      kwargs = {'role':'heading'}
    elif taskid.endswith('list'):
      pred = self.perk.rolePredicate
      kwargs = {'role':'list'}
    elif taskid.endswith('list item'):
      pred = self.perk.attrPredicate
      kwargs = {'key':'tag','value':'LI'}
      outputname = 'list item'
      # todo: set outputname to list item or menu item based on results
    elif taskid.endswith('section'):
      pred = self.perk.rolePredicate
      kwargs = {'role':'section'}
    elif taskid.endswith('table'):      
      pred = self.perk.tablePredicate
      outputname = 'table'
    elif taskid.endswith('unvisited link'):
      pred = self.perk.unvisitedlinkPredicate
      outputname = 'unvisited link'
    elif taskid.endswith('visited link'):
      pred = self.perk.visitedlinkPredicate 
      outputname = 'visited link' 
    # navigation by landmark
    elif taskid.endswith('banner'):
      pred = self.perk.attrPredicate
      kwargs = {'key':'xml-roles','value':'banner'}
      outputname = 'banner'
    elif taskid.endswith('content info'):
      pred = self.perk.attrPredicate
      kwargs = {'key':'xml-roles','value':'contentinfo'}
      outputname = 'content info'
    elif taskid.endswith('definition'):
      pred = self.perk.attrPredicate
      kwargs = {'key':'xml-roles','value':'definition'}
      outputname = 'definition'
    elif taskid.endswith('main'):
      pred = self.perk.attrPredicate
      kwargs = {'key':'xml-roles','value':'main'}
      outputname = 'main'
    elif taskid.endswith('navigation'):
      pred = self.perk.attrPredicate
      kwargs = {'key':'xml-roles','value':'navigation'}
      outputname = 'navigation'
    elif taskid.endswith('note'):
      pred = self.perk.attrPredicate
      kwargs = {'key':'xml-roles','value':'note'} 
      outputname = 'note'
    elif taskid.endswith('search'):
      pred = self.perk.attrPredicate
      kwargs = {'key':'xml-roles','value':'search'} 
      outputname = 'search'
    elif taskid.endswith('secondary'):
      pred = self.perk.attrPredicate
      kwargs = {'key':'xml-roles','value':'secondary'}
      outputname = 'secondary'
    elif taskid.endswith('see also'):
      pred = self.perk.attrPredicate
      kwargs = {'key':'xml-roles','value':'seealso'}
      outputname = 'see also'      
    else:
      # output task identity in this error state
      outputname = taskid
    
    if pred is None:
      self.sayError(text=_('unrecognized task name %s' %outputname))
    else:
      nodetotal = 0
      if taskid.find('next') > 0:
        # get last acc in active doc frame.  It will be our stopping point.  
        endacc = self.perk.getLastSearchAcc()    
        # iterate through tree and apply predicate 
        for nextpor in self.iterNextItems(wrap=True):
          # bound to active doc frame
          if nextpor == endacc:
            break
          nodetotal += 1
          # safety net in case of cyclical navigation
          if nodetotal > self.perk_state.SearchDepth:
            break
          if pred(nextpor, **kwargs):
            self.stopNow()
            # get the previous item before search query and review to the next
            # item.  This will skip any undesirable accs
            self.moveToPOR(por=self.getPrevItem(por=nextpor, wrap=True))
            self.doTask('review next item')
            return False
            
      elif taskid.find('prev'):
        docframe = self.perk.getActiveDocFrame()
        # no need to search if we are at doc frame, we are already at the end.  
        # Continue on so error announcement is made.
        if self.task_por != docframe:
          for nextpor in self.iterPrevItems(wrap=True):
            nodetotal += 1
            # safety net in case of cyclical navigation
            if nodetotal > self.perk_state.SearchDepth:
              break
            if pred(nextpor, **kwargs):
              self.stopNow()
              # get the next item after search query and review to the prev
              # item.  This will skip any undesirable accs
              self.moveToPOR(por=self.getNextItem(por=nextpor, wrap=True))
              self.doTask('review previous item')
              return False
            if nextpor == docframe:
              break
      else:
        # todo: is this correct?
        raise NameError
      # didn't find it
      self.sayError(text=_('could not find %s' %outputname))
      
  
class CyclicGoToElement(Task.InputTask):
  '''
  Cyclic Task used for searching for an element.  The search criteria and
  direction of search are derived from the task identity associated to this
  task.  The pointer is updated and the L{POR} is announced upon finding the 
  accessible, otherwise the pointer is NOT moved and an announcement declaring
  that the accessible is not found is made.  This task does not stop on
  'undesirable' accessibles as defined by L{ReviewPerk} settings.
  
  Cyclic Behavior
  First cycle - use tight search predicate
  Second cycle - use loose search predicate
  
  Currently used by navigation by relation.
  '''
  task_por_attrs = None
  task_por_role = None
  cycle_count = 0
  
  def execute(self, **kwargs):
    self.task_por_role = self.getAccRole()
    self.task_por_attrs = self.getAccAttrs()
    
    taskid = self.getIdentity()
    if taskid.endswith('similar'):
      if self.cycle_count == 0:
        pred = self._tightSimilarPred
      else:
        pred = self._looseSimilarPred
    elif taskid.endswith('different'):
      if self.cycle_count == 0:
        pred = self._tightDifferentPred
      else:
        pred = self._looseDifferentPred
    else:
      return
    nodetotal = 0
    if taskid.find('next') > 0:
      # get last acc in active doc frame.  It will be our stopping point.
      endacc = self.perk.getLastSearchAcc()  
      # iterate through tree and apply predicate  
      for nextpor in self.iterNextItems(wrap=True):
        # search is over.  did not find so break to announce error
        if nextpor == endacc:
          break
        nodetotal += 1
        # safety net in case of cyclical navigation. break to announce error
        if nodetotal > self.perk_state.SearchDepth:
          break
        if pred(nextpor):
          self.stopNow()
          # get the previous item before search query and review to the next
          # item.  This will skip any undesirable accs
          self.moveToPOR(por=self.getPrevItem(por=nextpor, wrap=True))
          self.doTask('review next item')
          self.cycle_count = 0
          return False
    elif taskid.find('prev'):
      docframe = self.perk.getActiveDocFrame()
      # no need to search if we are at doc frame, we are already at the end.  
      # Continue on so error announcement is made.
      if self.task_por != docframe:
        # iterate through tree and apply predicate
        for nextpor in self.iterPrevItems(wrap=True):
          nodetotal += 1
          # safety net in case of cyclical navigation. break to announce error
          if nodetotal > self.perk_state.SearchDepth:
            break
          if pred(nextpor):
            self.stopNow()
            # get the next item after search query and review to the prev
            # item.  This will skip any undesirable accs
            self.moveToPOR(por=self.getNextItem(por=nextpor, wrap=True))
            self.doTask('review previous item')
            self.cycle_count = 0
            return False
          # search is over.  did not find so break to announce error
          if nextpor == docframe:
            break
    # didn't find it, so announce an error
    if taskid.find('similar') > 0:
      self.sayError(text=_('could not find %s' %self.task_por_role))
    else:
      self.sayError(text=_('different than %s could not be found' %self.task_por_role))
    self.cycle_count += 1
      
  def _looseSimilarPred(self, por):
    ''' 
    Predicate used for finding next 'similar' accessible node using role only. 
    '''
    return(self.task_por_role == self.getAccRole(por=por))
  
  def _tightSimilarPred(self, por):
    ''' 
    Predicate used for finding next 'similar' accessible node using role and 
    attributes. 
    '''
    return(self.task_por_role == self.getAccRole(por=por) and \
           self.task_por_attrs == self.getAccAttrs(por=por))
           
  def _looseDifferentPred(self, por):
    ''' 
    Predicate used for finding next 'different' accessible node using role only
    '''
    return(self.task_por_role != self.getAccRole(por=por))
  
  def _tightDifferentPred(self, por):
    ''' 
    Predicate used for finding next 'different' accessible node using role and 
    attributes. 
    '''
    return(self.task_por_role != self.getAccRole(por=por) and \
           self.task_por_attrs != self.getAccAttrs(por=por))
    
class FFWhereAmINow(Task.InputTask): 
  '''
  Provides addition output on top of L{BasicSpeechPerk} WhereAmI?.  Additional
  output includes index and total information and percent read information.  This
  task arounds WhereAmINow.
  '''
  def execute(self, por=None, **kwargs): 
    por = por or self.task_por
    kwargs = {}
    
    # set the current por match predicate
    # kwargs is passed to predicate during iteration
    role = self.getAccRole(por=por)
    attrs = self.getAccAttrs(por=por)
    # default the output name to it's role. Change the outputname to something
    # else, if the need arises, within the following if statement.
    outputname = self.getAccRole()
    # tables are special because of layout tables
    if role == 'table':
      pred = self.perk.tablePredicate
    # links are special because of no role name
    elif role == '':
      pred = self.perk.rolePredicate
      kwargs['role'] = role
      outputname = 'link'
    # some special elements based on attribute tag values
    elif attrs is not None and attrs.has_key('tag'):
      if attrs['tag'] == 'INPUT':
        pred = self.perk.formcontrolPredicate
        outputname = 'form control'
      else:
        pred = self.perk.rolePredicate
        kwargs['role'] = role
    # default is based on role
    else:
      pred = self.perk.rolePredicate
      kwargs['role'] = role
    # stop any output
    self.stopNow()
    # make general 'where am i now' announcement
    self.doTask(self.getAnchorTaskId(), chain=False, **kwargs)
    # make additional announcements if information can be obtained
    time.sleep(1)
    if pred is not None:
      # make our one time call to the document iteration routine (expensive)
      typeindex,typetotal,nodeindex,nodetotal = \
               self._extractIndexInfo(pred, **kwargs)
      # if we got valid information. make additional announcement.  Otherwise,
      # announcement from above will suffice
      # output current por index/total information
      if typeindex > 0:
        text = (typeindex, typetotal, outputname)
        template = _('%d of %d %ss in document.')
        self.sayInfo(text=text, template=template)
      # output container index/total information 
      if nodeindex > 0:
        text = (int(nodeindex*100/nodetotal))
        template = _('%d percent of document read')
        self.sayInfo(text=text, template=template)
        
  def _extractIndexInfo(self, pred,
                        por=None, startpor=None, endpor=None, **kwargs):
    '''
    Extracts all indexing information for a given L{POR} Returned tuple 
    includes the following:
      
      nodeindex: node index of the subject por as with respect to all 
      accessibles.
      nodetotal: overall nodes in document
      typeindex: = index of subject por with respect to similar accessibles.
      typetotal: = total number of similar accessibles in document
    
    @param pred: A predicate used for
    @type pred: L{POR}
    @return: Is por within doc frame?
    @rtype: tuple
    '''
    # set current por
    por = por or self.task_por
    # set start of iteration
    startpor = startpor or self.perk.getActiveDocFrame()
    # get last acc in active doc frame.  It will be our stopping point.
    endpor = self.perk.getLastSearchAcc()
    # track overall nodes and index within for the current por, used for 
    # percent through doc
    nodeindex = -1
    nodetotal = 0
    # track the number of similar accessibles, either within the document if 
    # there is no container predicate, otherwise within the container.
    typeindex = -1
    typetotal = 0
    # begin traversal of document
    for i in self.iterNextItems(por=startpor, wrap=True):
      # traversal is over when we see the endpor      
      if i == endpor:
        break
      nodetotal += 1
      # safety net in case of cyclical navigation
      if nodetotal > self.perk_state.SearchDepth:
        return (-1, 0, -1, 0)
      # apply current por precicate and increment related indices/totals
      if pred is not None:
        if pred(i, **kwargs):
          typetotal += 1
          if i == por:
            nodeindex = nodetotal
            typeindex = typetotal
    return (typeindex, typetotal, nodeindex, nodetotal)
        
class GoToContainer(Task.InputTask): 
  '''
  Navigate to the container root containing the pointer.  Container are defined
  as accessibles with roles: image, form, list, calendar, menu, menu bar, entry,
  page tab, tool bar, application, tree, or tree table.  Container are also
  document frames that are not the active doc frame and tables that are not
  layout tables.
  '''
  def execute(self, **kwargs): 
    # stop any speech output
    self.stopNow()
    # test the current POR, it might be a container eg. entry
    role = self.getAccRole()
    if role in self.perk.CONTAINERS_BY_ROLE:
      # task_por is a container and we are on an item.  set to beginning of acc
      self.task_por.item_offset=None
      self.task_por.char_offset=0
      self.doTask('pointer to por')
      return
    # go up tree until you find a container or break to announce error when doc 
    # frame is seen.
    activedoc = self.perk.getActiveDocFrame()
    for p in self.iterAncestorAccs(only_visible=False,allow_trivial=False):
      # our search is done.  breakout to announce container not found
      if p == activedoc:
        break
      role = self.getAccRole(por=p)
      if role in self.perk.CONTAINERS_BY_ROLE or \
           role == 'table' and not self.perk.isLayoutTable(p, role) or \
           role == DOCUMENT_ROLE and p != activedoc:
        self.doTask('pointer to por', por=p)
        # set reading mode. no need to set focus on the container
        self.perk.setModeByPOR(p)
        return 
    self.sayError(text=_('top container'))
                                
class SkipContainer(Task.InputTask): 
  '''
  Skips containers if pointer is currently on a container, otherwise an error
  message is announced.  See L{GoToContainer} for list of defined containers.
  '''
  def execute(self, **kwargs): 
    self.stopNow()
    lastunder = self.getLastAccUnder()
    if lastunder == self.task_por:
      self.sayError(text=_('no container to skip'))
    else:
      self.moveToPOR(lastunder)
      # review task will take care of mode and focus settings
      self.doTask('review next item')
      self.perk.setModeByPOR(self.task_por)
      
class SavedPORFlusher(Task.TimerTask):
  '''
  Removes L{POR}s saved during restoration routine that are no longer valid.
  '''
  def execute(self, **kwargs):
    flushed = []
    for key in self.perk.doc_pors.iterkeys():
      attrs = self.getAccAttrs(por=key)
      # invalid if the URL is not given
      try:
        if attrs['DocURL'] == '':
          flushed.append(key)
      except (TypeError, KeyError):
        pass
    for i in flushed:   
      del self.perk.doc_pors[i]
      

class ReadLinkPreview(Task.InputTask):
  '''
  Reads information about the current link.  Information includes protocol,
  domain differences, and download size if available.
  '''
  def execute(self, **kwargs):
    role = self.getAccRole()
    if role != '':
      self.sayError(text=_('pointer not on a link'))
      return
    # get our two URIs of importance (current page and the link)
    link_uri = self.getAccURI(0)
    doc_uri  = self.perk.getDocURI()
    # sanity check
    if link_uri is None or doc_uri is None:
      self.sayError(text=_('error in determining URI'))
      return
    # returned tuple contains six components: 
    # scheme://netloc/path;parameters?query#fragment.
    link_uri_info = urlparse.urlparse(link_uri)
    doc_uri_info = urlparse.urlparse(doc_uri)
    # setup our three possible output defaults
    linkoutput = '%s link.' %link_uri_info[0]
    domainoutput = ''
    sizeoutput = ''
    
    # determine location differences
    if link_uri_info[1] == doc_uri_info[1]:
      if link_uri_info[2] == doc_uri_info[2]:
        domainoutput = 'same page'
      else:
        domainoutput = 'same site'
    else:
      # check for different machine name on same site
      linkdomain = link_uri_info[1].split('.')
      docdomain = doc_uri_info[1].split('.')
      if len(linkdomain) > 1 and docdomain > 1 and \
           linkdomain[-1] == docdomain[-1] and linkdomain[-2] == docdomain[-2]:
        domainoutput = 'same site' 
      else:
        domainoutput = 'different site'
    
    # get size and other protocol specific information
    if link_uri_info[0] == 'ftp' or link_uri_info[0] == 'ftps' or \
              link_uri_info[0] == 'file':
      # change out link output message to include filename
      filename = link_uri_info[2].split('/')
      linkoutput = '%s link to %s' %(link_uri_info[0], filename[-1])
      sizestr = self._extractSize(link_uri)
      sizeoutput = self._formatSizeOutput(sizestr)
    # add addition protocol specific changes
    elif link_uri_info[0] == 'javascript':
      pass
    
    # format and send output message
    text = (linkoutput, domainoutput, sizeoutput)
    template = _('%s. %s. %s')
    self.stopNow()
    self.sayInfo(text=text, template=template)  
    
  def _extractSize(self, uri):
    '''
    Get the http header for a given uri and try to extract the size (Content-length).
    
    @param uri: A Uniform Resource Identifier
    @type uri: string  
    @return: size of file pointed to by uri as indicated by http header if 
    available
    @rtype: string
    '''
    try:
      x=urllib2.urlopen(uri)
      try:
        return x.info()['Content-length']
      except KeyError:
        return None
    except (ValueError, urllib2.URLError, OSError):
      return None
  
  def _formatSizeOutput(self, sizestr):
    '''
    Format the size output announcement.  Changes wording based on size.
    
    @param sizestr: size in bytes as a string
    @type sizestr: string  
    @return: The size announcement
    @rtype: string
    '''
    # sanity check
    if sizestr is None or sizestr == '':
      return ''
    size = int(sizestr)
    if size < 10000:
      return '%s bytes' %sizestr
    elif size < 1000000:
      out = '%.2f kilobytes' %(float(size) * .001)
      return out
    elif size >= 1000000:
      return '%.2f megabytes' %(float(size) * .000001)
    
class FFReadNewContainer(Task.Task):
  '''
  Containers should not be read in webpages.  This task 'befores' 
  ReadNewContainer and stops task propagation to prevent it from executing.
  '''
  def execute(self, por=None, **kwargs):  
    return False
  