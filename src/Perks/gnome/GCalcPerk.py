'''
Reports buttons and their hotkeys as they are pressed. Reads the equation bar
intelligently.

@author: Joel Feiner
@author: Peter Parente
@organization: UNC Chapel Hill
@copyright: Copyright (c) 2007, Joel Feiner
@license: BSD
@note: Contributed to the LSR project via GNOME bug #426659.

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which is available at
http://www.opensource.org/licenses/bsd-license.php

See http://www.unc.edu/campus/policies/copyright.html Section 5.D.2.A for UNC
copyright rules.
'''
# import useful modules for Perks
import Perk, Task, AEConstants
from POR import POR
from i18n import bind, _

# metadata describing this Perk
__uie__ = dict(kind='perk', tier='gcalctool', all_tiers=False)

class GCalcPerk(Perk.Perk):
  '''
  CalcPerk, perk for handling gcalctool. The getDescription method
  should be changed to return a translated description of this Perk. The 
  description should include a statement of which Tiers this Perk applies to
  by default.
  '''
  def init(self):
    print 'loading gcalc perk'
    # basic registration stuff
    self.registerTask(ReadButtonName('gcalc read button'))
    self.registerTask(ReadEquation('gcalc read equation'))
    self.registerTask(HandleEquationChange('gcalc read caret', tier=True))
    #self.registerTask(HandleStateBarChange('gcalc status bar', tier=True))

    # this allows us to get keyboard input, only used for the CAPS+I hotkey for
    # reading the equation
    kbd = self.getInputDevice('Keyboard')
    self.addInputModifiers(kbd, kbd.AEK_CAPS_LOCK)
    self.registerCommand(kbd, 'gcalc read equation', False, 
                         [kbd.AEK_CAPS_LOCK, kbd.AEK_I])

    # do some work around normal announcements
    self.chainTask('gcalc read button', AEConstants.CHAIN_AROUND,
                   'read selector')

  def getDescription(self):
    return _('Applies to gcalctool by default.')

class ReadEquation(Task.InputTask):
  '''
  Is invoked whenever user hits CapsLock+A.  It reads the entire equation,
  character by character
  '''
  def execute(self, **kwargs):
    # POR path: 0,0,1,0
    frame = self.getRootAcc()
    textBox = self.getAccFromPath(frame, 0, 1, 0, 0)
    text = self.getItemText(textBox)	
    self.stopNow()
    sf = self.getStyleVal('SpellFormat')
    self.setStyleVal('SpellFormat', AEConstants.FORMAT_SPELL)
    self.sayInfo(text=text)
    self.setStyleVal('SpellFormat', sf)

class ReadButtonName(Task.Task):
  '''
  Invoked when focus changes.  We use this to announce the proper text of
  buttons as the user arrows between them, or uses Tab.
  '''   
  def execute(self, por, **kwargs):
    hotkey = None
    # only do this if focus is for a button
    if self.hasAccRole('push button', por):
      self.mayStop()
      # get the button name
      name = self.getAccName(por)
      if name:
        # look for a hotkey
        e = name.rfind(']')
        if e > -1:
          s = name.rfind('[')
          if s > -1:
            # get hotkey
            hotkey = name[s+1:e]
            # then shorten string
            name = name[:s]
        # parse the hotkey out of the name
        kwargs['text'] = name
    self.doTask(self.getAnchorTaskId(), por=por, chain=False, **kwargs)
  
    # say the shortcut key too, after the normal announcement
    if hotkey is not None:
      self.sayHotkey(text=hotkey)
    
class HandleStatusBarChange(Task.PropertyTask):
  '''
  Handles text being displayed in the statusbar

  @note: this does not work because gcalctool only sometimes sends a property
  change event.  The code is retained here in case it works in the future
  '''
  def init(self):
    # need this ugly little hack because we get this change
    # event four times with no apparent difference between each message
    callCount = 0

  def execute(self, por, name, value, layer, **kwargs):
    self.callCount = self.callCount + 1
    if self.callCount == 4:
      statusText = self.getItemText(por)
      self.mayStop()
      self.inhibitMayStop()
      self.sayInfo(text=statusText)
      self.callCount = 0

class HandleEquationChange(Task.CaretTask):
  '''
  Reads text that is added to the equation, either by keyboard or by clicking
  buttons.

  @ivar old_string: Copy of what was in the equation box
  @type old_string: string
  @ivar cleared: Set to True if the equation box was cleared
  @type cleared: integer
  @ivar startup: Set to True until the first caret event happens
  @type startup: boolean
  '''
  def init(self):
    self.old_string = ""
    self.cleared = True
    self.startup = True
    
  def executeDeleted(self, por, text, text_offset, **kwargs):
    '''
    When a button is pressed, delete, move, insert events are fired in that
    order. Uses the delete event to capture what the string was before the
    new text is added by the insert.
    '''
    self.old_string = text

  def executeInserted(self, por, text, text_offset, **kwargs):
    '''
    Announces the current text in the display area.
    '''
    # extract the change in the text
    startString = ''
    endString = ''
    startIndex = text.find(self.old_string)

    # if we're just starting up, make sure to do the right thing with
    # whatever's in the equation box
    if self.startup == True:
      self.startup = False
      startString = ''

      # question is whether to read everything here...there's no way to read
      # the last thing entered. Hopefully, the user would start LSR
      # before starting gcalctool to avoid this problem
      endString = text
      startIndex = 0
      self.cleared = False
    elif startIndex == -1:
      # so we did not add any text at the beginning or end
      if self.cleared:
        # well, we were previously cleared, so now we're adding new text
        # the 0 will be replaced by whatever was just added
        startString = ""
        endString = text
        self.cleared = False
      else:
        if self.old_string == "0":
          # just replaced a cleared calculator
          startString = ""
          endString = text
        elif text != "0":
          # not a clearing, but an equals, so we should read whole result
          # ...but it could be a backspace, so let's check for that condition
          # first (if new string is prefix of old string, we had a backspace)
          if self.old_string.find(text) == 0:
            startString = ""
            endString = ""
          else:
            startString = ""
            endString = text
        else:
          # we just got cleared, so mark it as such
          self.cleared = True
    else:
      # this is the normal case: something got added, so let's say it
      startString = text[0:startIndex]
      endString = text[(startIndex + len(self.old_string)):]
          
    if endString != "":
      self.mayStop()
      self.inhibitMayStop()
            
    if self.cleared == True:
      self.sayInfo(text=_('cleared'))
    elif startString == "1/(":
      self.sayInfo(text=_('invert'))
    elif startString == "-(":
      self.sayInfo(text=_('negate'))
    elif endString == "Sqrt(":
      self.sayInfo(text=_('square root'))
    elif endString == '-':
      self.sayInfo(text=_('minus'))
    elif endString == '*':
      self.sayInfo(text=_('times'))
    elif endString == '/':
      self.sayInfo(text=_('divides'))
    elif endString == '%':
      self.sayInfo(text=_('modulo'))
    elif endString == "Int(":
      self.sayInfo(text=_('integer'))
    elif endString == "Abs(":
      self.sayInfo(text=_('absolute value'))
    elif endString == "Frac(":
      self.sayInfo(text=_('fraction'))
    elif endString != "":
      self.sayInfo(text=endString)
