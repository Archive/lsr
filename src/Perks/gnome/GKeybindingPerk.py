'''
Silences the pronunciation of "<" and ">" in key combinations such as 
<Ctrl><Alt>K. Informs the user of a new shortcut when pressing Enter or Space,
and when cancelling with Escape or Backspace.

@author: Andy Shi
@author: Peter Parente
@organization: UNC Chapel Hill
@copyright: Copyright (c) 2007, Andy Shi
@license: BSD
@note: Contributed to the LSR project via GNOME bug #426661.

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD which is available at
http://www.opensource.org/licenses/bsd-license.php

See http://www.unc.edu/campus/policies/copyright.html Section 5.D.2.A for UNC
copyright rules.
'''
# import useful modules for Perks
import Perk, Task, AEConstants
from POR import POR
from i18n import bind, _

# metadata describing this Perk
__uie__ = dict(kind='perk', tier='gnome-keybinding-properties',all_tiers=False)

class GKeybindingPerk(Perk.Perk):
  '''
  Registers tasks to correct speech reporting of keyboard bindings.
  '''
  def init(self):
    self.registerTask(ReadKeybinding('gkp read keybinding'))
    self.registerTask(EnterNewShortcut('gkp new shortcut'))
    self.registerTask(LeaveShortcut('gkp leave shortcut'))

    kbd = self.getInputDevice(None, 'keyboard')
    # keys that trigger a new shortcut box change
    keys = [kbd.AEK_ENTER, kbd.AEK_SPACE]
    for key in keys:
      self.registerCommand(kbd, 'gkp new shortcut', True,
                           [key])
    # keys that cancel editing of a shortcut
    keys = [kbd.AEK_BACK_SPACE, kbd.AEK_ESCAPE]
    for key in keys:
      self.registerCommand(kbd, 'gkp leave shortcut', True,
                           [key])

    # correct reading of selector and carets in the second column
    self.chainTask('gkp read keybinding', AEConstants.CHAIN_AROUND,
                   'read caret')
    self.chainTask('gkp read keybinding', AEConstants.CHAIN_AROUND,
                   'read selector')

  def getDescription(self):
    return _('Improves usability of gnome-keybindings-properties dialog.')

class ReadKeybinding(Task.Task):
  '''
  Remove the angle brackets and replace with a space between Control, Alt,
  Shift, and other keys.
  '''
  def execute(self, por, text, **kwargs):
    col = self.getAccColumn(por)
    if col == 1:
      # replace text
      text = text.replace('>', ' ')
      text = text.replace('<', ' ')
    # invoke the original no matter what, without chaining else we'll recurse
    self.doTask(self.getAnchorTaskId(), por=por, text=text, chain=False, 
                **kwargs)
    
class EnterNewShortcut(Task.InputTask):
  '''
  Announces the phrase 'new accelerator'.
  '''
  def execute(self, **kwargs):
    self.stopNow()
    self.sayItem(text=_('new accelerator'))

class LeaveShortcut(Task.InputTask):
  '''
  Announces the existing accelerator.
  '''
  def execute(self, **kwargs):
    col = self.getAccColumn()
    if col == 1:
      self.stopNow()
      text = self.getItemText()
      text = text.replace('>', ' ')
      text = text.replace('<', ' ')
      self.sayItem(text=text)
