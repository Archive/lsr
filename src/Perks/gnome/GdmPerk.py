'''
Defines a L{Perk} for the gdm login screen.

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
# import useful modules for Perks
import Perk, Task
from POR import POR
from i18n import _

# metadata describing this Perk
# apply it to all Tiers since the login greeter name can change and run it in
# a special login profile
__uie__ = dict(kind='perk', tier=None, all_tiers=True)

class GdmPerk(Perk.Perk):
  '''
  Registers L{Task}s to announce the focused control on startup, typically the
  username field.  
  '''
  def init(self):
    # set an audio device as the default output
    self.setPerkIdealOutput('audio')    
    
    # register tasks
    self.registerTask(AnnounceStartup('gdm startup'))
    self.registerTask(AnnounceLabelChange('gdm label change'))
    
  def getName(self):
    return _('GNOME login')
    
  def getDescription(self):
    return _('Corrects username/password accessibility problems at login. '
             'Applies to gdm by default. Works best with the plain greeter.')
  
class AnnounceStartup(Task.ViewTask):
  '''
  Announces the control that has the focus on startup.
  '''
  def executeFirstGained(self, por, title, **kwargs):
    self.sayWindow(text=_('GDM Login'))
    # find the widget that has the focus; search for it since we weren't 
    # started in time for the focus event
    por = self.findAccByPredicate(lambda por: self.hasAccState('focused', por))
    # set the focus to that location
    self.setAccPOR(por)
    self.doTask('pointer to por', por=por)
    return False
  
class AnnounceLabelChange(Task.CaretTask):
  '''
  Announces the changing label of the text box as it serves dual duty as the 
  username and password field.
  '''
  def init(self):
    self.first = True
  
  def execute(self, por, text, text_offset, added, **kwargs):
    if not text and text_offset == 0 and added is not None:
      if added:
        if not self.first:
          self.doTask('pointer to por', por=por)
        self.first = False
      return False
