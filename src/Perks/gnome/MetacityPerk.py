'''
Defines corrections for the metacity desktop and app switcher.

Event announcements

on cycle windows: [name of window]
on cycle desktops: [name of desktop]

@author: Brett Clippingdale
@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
import Perk, Task
import AEConstants
from i18n import _

__uie__ = dict(kind='perk', tier='metacity')

class MetacityPerk(Perk.Perk):
  '''
  Announces information in the metacity desktop and application switcher 
  windows.
  '''
  def init(self):
    '''   
    Registers to receive property change events from the little status bar
    containing the name of the currently selected window in the application or 
    workspace switcher.
    '''
    # set an audio device as the default output
    self.setPerkIdealOutput('audio')
    
    # register event handlers
    self.registerEventTask(HandlePropertyChange(self), tier=True)
    
  def getName(self):
    return _('Metacity')

class HandlePropertyChange(Task.PropertyTask):
  '''  
  Announces Alt-Tab application switching by reading the accessible name of
  the status text when it changes.
  '''  
  def execute(self, por, name, value, **kwargs):
    # get the text in status bar
    text = self.getItemText(por)
    # pretend we're on the focus layer even though text is changing elsewhere
    self.stopNow(layer=AEConstants.LAYER_FOCUS)
    self.say(text=text, sem=AEConstants.SEM_WINDOW, 
             layer=AEConstants.LAYER_FOCUS)
    return True

