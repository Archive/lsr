'''
Handle several problems with gedit, including mainly the spellcheck dialogue
and the document statistics dialogue.

@author: Andy Shi
@author: Cristobal Palmer
@author: Peter Parente
@organization: UNC Chapel Hill
@copyright: Copyright (c) 2007, Andy Shi, Cristobal Palmer
@license: BSD
@note: Contributed to the LSR project via GNOME bug #426660.

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD License which is available at
U{http://www.opensource.org/licenses/bsd-license.php}

See http://www.unc.edu/campus/policies/copyright.html Section 5.D.2.A for UNC
copyright rules.
'''
# import useful modules for Perks
import Perk, Task, AEConstants
from POR import POR
from i18n import bind, _

# metadata describing this Perk
__uie__ = dict(kind='perk', tier='gedit', all_tiers=False)

class GEditPerk(Perk.Perk):
  '''  
  This class exists to register tasks that handle the check spelling dialog and
  document statistics dialog. It also defines a hotkey, CapsLock-K, that reads
  the current misspelled word in the context of the spell check dialog.
  
  @ivar spelling: Is the spell check dialog active?
  @type spelling: boolean
  @ivar stats: Is the document statistics dialog active?
  @type stats: boolean
  '''
  def init(self):
    self.context = None
    self.spelling = False
    self.stats = False
    
    self.registerTask(ReadStatsDialog('gedit read stats'))
    self.registerTask(ReadSpellingDialog('gedit read spelling'))
    self.registerTask(ReadMisspelledWord('gedit read misspelled word'))
    self.registerTask(ReadMisspelledContext('gedit read misspelled context'))
    self.registerTask(TrackTierCarets(None, tier=True))
    self.registerTask(TrackDocumentFocus(None, tier=True, background=True))
    #self.registerTask(CheckCurrentWord('gedit check word', tier=True))

    # run the view change handler after the basic speech handler
    self.chainTask('gedit read stats', AEConstants.CHAIN_AFTER, 'read dialog')
    self.chainTask('gedit read spelling', AEConstants.CHAIN_AFTER,
                   'read view')

    kbd = self.getInputDevice('Keyboard')
    self.addInputModifiers(kbd, kbd.AEK_CAPS_LOCK)
    self.registerCommand(kbd, 'gedit read misspelled word', False,
                         [kbd.AEK_CAPS_LOCK, kbd.AEK_K])
    self.registerCommand(kbd, 'gedit read misspelled context', False,
                         [kbd.AEK_CAPS_LOCK, kbd.AEK_I])

  def getDescription(self):
    return _('Improves the usability of gedit spell check and document '
             'statistics dialogs.')  
    
  def isDocument(self, por):
    return self.hasAccRole('text', por) and self.hasAccState('multi line', por)

class ReadStatsDialog(Task.Task):
  '''
  Reads the documents statistics dialog in a sensible order.
  
  @todo: we should say the labels as row and column headers, at least for
    the first row
  '''  
  def _isStats(self, por):
    # check if stats dialog is open by querying some well known paths
    lblOne = self.getAccFromPath(por, 0,0,0)
    lblTwo = self.getAccFromPath(por, 0,0,1,1,2)
    lblThree = self.getAccFromPath(por, 0,0,1,1,3)
    if not (self.hasAccRole('label', lblOne) and 
            self.hasAccRole('label', lblTwo) and
            self.hasAccRole('label', lblThree)):
      # not the dialog we seek
      self.perk.stats = False
      return False
    self.perk.stats = True
    return True

  def _sayStats(self, por):
    item_list = []
    #line
    item_list.append(self.getAccFromPath(por, 0,0,1,1,2))
    item_list.append(self.getAccFromPath(por, 0,0,1,1,1))
    item_list.append(self.getAccFromPath(por, 0,0,1,2,1))
    #word
    item_list.append(self.getAccFromPath(por, 0,0,1,1,3))
    item_list.append(self.getAccFromPath(por, 0,0,1,1,4))
    item_list.append(self.getAccFromPath(por, 0,0,1,2,2))
    #character with spaces
    item_list.append(self.getAccFromPath(por, 0,0,1,1,6))
    item_list.append(self.getAccFromPath(por, 0,0,1,1,5))
    item_list.append(self.getAccFromPath(por, 0,0,1,2,3))
    #character without spaces
    item_list.append(self.getAccFromPath(por, 0,0,1,1,8))
    item_list.append(self.getAccFromPath(por, 0,0,1,1,7))
    item_list.append(self.getAccFromPath(por, 0,0,1,2,4))
    #bytes
    item_list.append(self.getAccFromPath(por, 0,0,1,1,10))
    item_list.append(self.getAccFromPath(por, 0,0,1,1,9))
    item_list.append(self.getAccFromPath(por, 0,0,1,2,5))
    for i in xrange(0, len(item_list), 3):
      self.sayLabel(text=self.getAccName(item_list[i]), talk=False)
      self.sayItem(item_list[i+1], talk=False)
      self.sayItem(item_list[i+2], talk=False)

  def execute(self, **kwargs):
    por = self.getRootAcc()
    if self._isStats(por):
      self._sayStats(por)
  
class ReadSpellingDialog(Task.Task):
  '''
  Reads the misspelled word in the spelling dialog when it first appears.
  '''
  def _isSpelling(self, por):
    # for check spelling dialog using some well known paths
    checkLabel = self.getAccFromPath(por, 0,0,1)
    checkButton = self.getAccFromPath(por, 0,0,0,1)
    checkTextField = self.getAccFromPath(por, 0,0,0,0)
    if not (self.hasAccRole('label', checkLabel) and 
            self.hasAccRole('push button', checkButton) and
            self.hasAccRole('text', checkTextField)):
      # not the spell check dialog
      self.perk.spelling = False
      return False
    self.perk.spelling = True
    return True
  
  def _saySpelling(self, por):
    # get the misspelled word label
    label_por = self.getAccFromPath(por, 0,0,3)
    label_text = self.getAccName(label_por)
    # get the misspelled word, there is not relation to its label
    word_por = self.getAccFromPath(por, 0,0,2)
    word_text = self.getAccName(word_por)
    # say the label and the word
    self.inhibitMayStop()
    self.say(text=label_text, sem=AEConstants.SEM_LABEL, 
             layer=AEConstants.LAYER_FOCUS)
    self.say(text=word_text, sem=AEConstants.SEM_WORD,
             layer=AEConstants.LAYER_FOCUS)
  
  def execute(self, por, gained, **kwargs):
    if gained:
      if self._isSpelling(por):
        self._saySpelling(por)

class TrackDocumentFocus(Task.FocusTask):
  '''
  Keeps track of the L{POR} to the misspelled word.
  '''
  def executeLost(self, por, **kwargs):
    if not self.perk.isDocument(por):
      # not the main text area
      return
    # store the context for later if the user requests it
    self.perk.context = self.getAccCaret(por)

class TrackTierCarets(Task.CaretTask):
  '''
  Keeps track of the L{POR} to the misspelled word. Announces it when the 
  spelling dialog is not shown.
  '''
  def executeMoved(self, por, text, text_offset, **kwargs):
    if not self.perk.isDocument(por):
      # not the main text area
      return
    # store the context for later if the user requests it
    self.perk.context = por
    if self.perk.spelling:
      # say the misspelled word if the spelling dialog is already open
      self.inhibitMayStop()
      self.inhibitMayStop()
      self.say(text=_('misspelled word'), sem=AEConstants.SEM_LABEL, 
               layer=AEConstants.LAYER_FOCUS)
      self.say(text=self.getWordText(), sem=AEConstants.SEM_WORD,
               layer=AEConstants.LAYER_FOCUS)
      
class ReadMisspelledContext(Task.InputTask):
  '''
  Reads the line containing the misspelled word. Raises pitch on the misspelled
  word if that feature is supported.
  '''
  def execute(self, **kwargs):
    if not self.perk.spelling:
      # do nothing if the spell check dialog is not active
      return
    por = self.perk.context
    if por is None:
      # if we have no context info, abort
      return
    # get the item text
    text = self.getItemText(por)
    # stop all output
    self.stopNow()
    try:
      # see if we can modify pitch
      sett = self.getStyleSetting('Pitch', sem=AEConstants.SEM_WORD)
    except Task.InvalidStyleError:
      self.sayItem(text=text)
    else:
      # change pitch on the misspelled word
      diff = (sett.max - sett.min)/10
      sett.value += diff
      # say prefix
      prefix = text[:por.char_offset]
      if prefix.strip():
        self.sayItem(text=prefix, talk=False)
      # say word
      word = self.getWordText(por)
      self.sayWord(text=word, talk=False)
      # say suffix
      suffix = text[por.char_offset+len(word):]
      if suffix:
        self.sayItem(text=suffix)
      # restore pitch
      sett.value -= diff

class ReadMisspelledWord(Task.InputTask):
  ''' 
  Reads the misspelled word whenever the user presses a hotkey. Because the
  first misspelled word is not highlighted by gedit, getAccSelection cannot be
  used to retrieve the misspelled word. The only way to get to the misspelled
  word is by accessing the misspelled word label to explicitly extract its
  contents.
  '''
  def execute(self, **kwargs):
    if not self.perk.spelling:
      # do nothing if the spell check dialog is not active
      return
    # get the root por
    root = self.getRootAcc()
    # por to the misspelled word
    misspelled_word_por = self.getAccFromPath(root, 0,0,2)
    # word is in the name field, the text does not always update
    misspelled_word_text = self.getAccName(misspelled_word_por)
    # cycle through reading, spelling, and pronouncing the misspelled word
    self.doTask('cycle review word', text=misspelled_word_text, **kwargs)

class CheckCurrentWord(Task.StateTask):
  '''
  Monitors state changes on the table to determine if a word is misspelled or
  correct.
  
  @todo: needs work, what about the case where the table doesn't change state?
  '''
  def execute(self, por, name, value, **kwargs):
    # check for spelling dialog
    if not self.perk.spelling:
      return
    # get the suggestions table
    root = self.getRootAcc()
    table = self.getAccFromPath(root, 0,1,3,0)
    if (table == por and name == 'sensitive'):
      if value:
        self.sayInfo(text=_('misspelled'))
      else:
        self.sayInfo(text=_('spelled correctly'))
