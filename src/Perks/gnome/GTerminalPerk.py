'''
Defines a L{Perk} for the GNOME Terminal.

@author: Peter Parente
@author: Brett Clippingdale
@author: Eitan Isaacson
@author: Scott Haeger
@author: Luiz Rocha
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made 
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
import Perk, Task
import AEConstants
from POR import POR
from i18n import bind, _

__uie__ = dict(kind='perk', tier='gnome-terminal', all_tiers=False)

class GTerminalPerk(Perk.Perk):
  '''
  Corrects caret problems in in gnome-terminal.

  @ivar actions: Maps all possible 'added' values to strings
  @type actions: dictionary
  @ivar move_keys: 
  @type move_keys: tuple
  @ivar delete_keys: 
  @type delete_keys: tuple
  @ivar ret_key:
  @type ret_key: string
  @ivar last_caret: Stores the previous caret L{POR}
  @type last_caret: L{POR}
  @ivar last_key: Stores the last key pressed for future comparision
  @type last_key: tuple
  '''
  def init(self):
    '''
    Register L{Task}s and chain it around the default L{Task} for caret change
    events. Initialize perk variables.
    '''
    # set an audio device as the default output
    self.setPerkIdealOutput('audio')

    # registering tasks
    self.registerTask(HandleTerminalCaret('gterm caret'))

    # chain to other tasks
    self.chainTask('gterm caret', AEConstants.CHAIN_AROUND, 'read caret')

    # initialize perk variables
    self.actions = {None: 'move', False: 'delete', True: 'insert'}
    self.move_keys = ('Left', 'Right', 'Home', 'End')
    self.delete_keys = ('Delete', 'BackSpace')
    self.space_key = 'space'
    self.ret_key = 'Return'
    self.last_caret = POR(item_offset=0)
    self.last_key = None

  def getName(self):
    return _('GNOME Terminal')

  def getDescription(self):
    return _('Improve general accessibility of the GNOME Terminal.')

class HandleTerminalCaret(Task.Task):
  '''
  L{Task} that handles messy L{AEEvent.CaretChange} sent from gnome-terminal.
  This one is chained around the basic event handling L{Task}. The {execute}
  method identifies the event, fix, propagate or ignore it althogether.
  '''
  def execute(self, por, text, text_offset, added, **kwargs):
    '''
    TODO: write a nice description for execute.

    @param por: Point of regard where the caret event occurred
    @type por: L{POR}
    @param text: The text inserted, deleted or the line of the caret
    @type text: string
    @param text_offset: The offset of the inserted/deleted text or the line 
      offset when movement only
    @type text_offset: integer
    @param added: True when text added, False when text deleted, and None 
      (the default) when event is for caret movement only
    @type added: boolean
    '''
    # TODO: write a nice description for execute.
    # TODO: would it be a good idea to refactor this and move the conditional
    #       blocks for each 'added' to separate methods inside this task?
    # TODO: check long cmdlines (call a random prog with a long line of args)
    # TODO: check reverse/incremental search (Ctrl+R) output
    # TODO: say the whole word (instead of just the ending) on tab-completion?
    # TODO: say the character (ie. 'vertical bar' for '|') or common name ('pipe')?
    # TODO: selection is bogus, fix
    # TODO: gnome-terminal breaks long commnand lines (ex. du --apparent-size 
    #       --human-readable --summarize 
    #       ~/downloads/mozilla/firefox/granparadiso), find a way around.

    # handling only terminal events
    if not self.hasAccRole('terminal') or None in self.getLastKey():
      return False

    # last key pressed in gterm is our current key
    current_key = self.getLastKey()

    # TODO: remove the 'else' from the action conditional loop and use this as
    #       default condition setting?
    # propagate to chained tasks by default
    # propagate = True

    # --- Insert Events ----------------------------------------------------- #
    if self.perk.actions[added] == 'insert':
      # insert events are fired by gnome-terminal when an insertion or a 
      # deletion happens. Cases to implement:
      #
      # 1. Terminal responding to user input should not be interrupted
      # 2. Ignore insert event fired by delete keys
      # 3. Route all other events to basic speech perk

      # 1. if the current key is a Return, the terminal is probably responding
      # to some user input. the avalanche of events gterm sends might break the
      # speech output. inhibit the next stop to prevent this.
      if current_key[1] == self.perk.ret_key:
        self.inhibitMayStop()
        propagate = True
      # 2. insert event bundled in a valid deletes, ignore those
      elif current_key[1] in self.perk.delete_keys:
        propagate = False
      # 3. let basic speech handle anything else
      else:
        propagate = True
    # --- Move Events ------------------------------------------------------- #
    elif self.perk.actions[added] == 'move':
      # everything you do on gnome-terminal fires a move event, but we want to
      # react only to a few cases:
      #
      # 1. Events fired by actual move keys (Up/Down/Home/End),
      # 2. Events fired by space key, since gterm considers space to be just
      #    a move key, thus produce no announce,
      # 3. Move events that happens before an insert, to set the actual caret
      #    position,
      # 4. Ignore anything else.

      # 1. react if this move event was fired by an actual move key
      if current_key[1] in self.perk.move_keys:
        # TODO: because of the basic speech self.changed var, first move never
        #       gets announced. get around that.
        self.perk.last_caret = por
        propagate = True
      # 2. gnome-terminal only move the caret when space is pressed. do a fake
      #    space announcement then.
      elif current_key[1] in self.perk.space_key:
        self.sayChar(text=' ')
        self.perk.last_caret = por
        propagate = False
      # 3. if this event is bundled with an insert event, store the caret
      #    position for future use
      elif not current_key[1] in self.perk.delete_keys:
        self.perk.last_caret = por
        propagate = False
      # 4. Ignore anything else
      else:
        propagate = False
    # --- Delete Events ----------------------------------------------------- #
    elif self.perk.actions[added] == 'delete':
      # delete events are fired in both inserts and deletes, but reaction to it
      # should happen only when delete keys are pressed. Cases:
      #
      # 1. Handle event fired by delete key press,
      # 2. Ignore event bundled on insert events.

      # 1. delete event fired by a delete key, let's handle it
      if current_key[1] in self.perk.delete_keys:
        # the text param on delete events contains the whole text after the
        # deleted character. trim the text to announce only the deleted char
        #
        # TODO: selection delete is correctly announced?
        text = text[:1]
        # pass to BasicSpeechPerk last_caret the last valid caret position
        self.setPerkVar('BasicSpeechPerk', 'last_caret', self.perk.last_caret)
        # update last_caret position
        self.perk.last_caret = por
        # let basic speech do the rest
        propagate = True
      # not a valid delete event, ignore it
      else:
        propagate = False
    else:
      # propagate and let BasicSpeechPerk handle anything we missed here
      propagate = True

    # if propagate is True, call BasicSpeechPerk named task 'read caret'.
    if propagate:
      self.doTask('read caret', chain=False, por=por, text=text,
          text_offset=text_offset, added=added)

    # store keyboard input and last caret position for future comparisons
    self.perk.last_key = current_key

    return propagate
