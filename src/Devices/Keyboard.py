'''
Defines a class representing a standard L{Keyboard} input device and action 
code constants used to register filters on that device.

@var MAX_KEY_CHORD: Maximum number of actions that can be in a keyboard gesture
@type MAX_KEY_CHORD: integer
@var def_keymap: Default keymap for translation between key codes, keysyms, and
  key names
@type def_keymap: gtk.gdk.Keymap

@author: Peter Parente
@author: Scott Haeger
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
import AEInput, pyLinAcc, AEConstants
# need pygtk to map keycodes to keynames
import pygtk
pygtk.require('2.0')
import gtk.gdk as gdk
from i18n import _

DEBUG = False

def_keymap = gdk.keymap_get_default()
MAX_KEY_CHORD = 4

__uie__ = dict(kind='device')

def _keyCodeToKeyName(code):
  '''
  Gets the name of the given key code by using the default key map to lookup
  its keysym value and then its localized name. If the code is not known, an
  empty string is returned.
  
  @param code: Hardware keycode
  @type code: integer
  @return: Name of the key
  @rtype: string
  '''
  entries = def_keymap.get_entries_for_keycode(code)
  if entries and len(entries):
    return gdk.keyval_name(entries[0][0])
  return ''

class Keyboard(AEInput.SystemInput):
  '''
  Basic keyboard input device.
  
  Has a reference to the L{EventManager} to register for system wide keyboard
  key press and key release events. Matches key presses against a collection of
  stored L{AEInput.Gesture}s representing key combinations that should be
  filtered before they can reach the window with the input focus. Observers of
  this device are notified about L{AEInput.Gesture}s when one or more press
  events are received followed by a release.

  Instances of L{Keyboard} impose some constraints on the filters that can be
  registered. A filter can contain at most one L{AEInput.Gesture} and that
  L{AEInput.Gesture} may have at most L{MAX_KEY_CHORD} key codes and at least 
  one. The filtered gesture may start with zero, one, or two modifiers 
  registered via L{addModifier} and must end with exactly one non-modifier.

  The L{Keyboard} device is robust against typomatic repeats and more than
  L{MAX_KEY_CHORD} sustained presses. Typomatic repeats are ignored so
  that holding two modifier keys, holding a typomatic key, releasing one of the
  modifiers, and then releasing the typomatic key does not result in two
  notifications. Key presses beyond the maximum number are also ignored without
  affecting the current state.

  @ivar filters: Registered L{AEInput.GestureList} filters keyed by the one and
     only one L{AEInput.Gesture} allowed in each filter
  @type filters: dictionary keyed by L{AEInput.Gesture}s
  @ivar event_manager: Reference to the L{pyLinAcc.Event.Manager} which will
    provide us with raw keyboard events
  @type event_manager: L{EventManager}
  @ivar curr_gesture: Actions seen so far that might form a filtered
    L{AEInput.Gesture}
  @type curr_gesture: L{AEInput.Gesture}
  @ivar synth_press: Ordered list of synthesized key presses that have been
      sent for processing and should be ignored when detected
  @type synth_press: list
  @ivar synth_release: Ordered list of synthesized key releases that have been
      sent for processing and should be ignored when detected
  @type synth_release: list
  @ivar state: Current state method for the key event handling state machine
  @type state: callable
  @ivar last_press: Was the last event a press (True) or release (False)?
  @type last_press: boolean
  @ivar LOCK_KEYS: Key codes for keys that can be locked in a particular state,
    namely L{AEK_CAPS_LOCK}, L{AEK_SCROLL_LOCK}, and L{AEK_NUM_LOCK}. These
    are constant.
  @type LOCK_KEYS: list of integer
  @ivar ACC_KEYS: Key codes for keys that should never be consumed because they
    activate accessibility features such as slow keys or sticky keys
  @type ACC_KEYS: list of integer
  @ivar EATEN_GESTURES: List of gestures commonly eaten by a party higher than
    this device in the AT-SPI event chain (e.g. the window manager). This 
    device should not synthesize these gestures when detected.
  @type EATEN_GESTURES: list of L{AEInput.Gesture}
  '''
  def init(self):
    '''
    Initializes the empty filters dictionary and sets the event_manager
    reference to None. Creates an empty L{AEInput.Gesture} that will be used to
    buffer incoming keystrokes until the L{AEInput.Gesture} definitely matches
    or does not match one of the held filters. Registers for key press and 
    release events via L{pyLinAcc}. Sets the filtering mode to handled by
    default. Also registers for window activate events to account for missing 
    keys caused by system wide hotkey presses.
    '''
    self.filters = {}
    self.event_manager = pyLinAcc.Event.Manager()
    self.curr_gesture = AEInput.Gesture(self)
    self.state = self._stateInvalid
    self.synth_press = []
    self.synth_release = []
    self.last_press = False
    self.LOCK_KEYS = [self.AEK_CAPS_LOCK, self.AEK_SCROLL_LOCK, 
                      self.AEK_NUM_LOCK]
    self.ACC_KEYS = [self.AEK_SHIFT_L, self.AEK_SHIFT_R]
    self.EATEN_GESTURES = [
      AEInput.Gesture(self,[self.AEK_ALT_L, self.AEK_F4]),
      AEInput.Gesture(self,[self.AEK_ALT_R, self.AEK_F4])
    ]
    
    self.event_manager.addClient(self._onKeyPressed, 'keyboard:press')
    self.event_manager.addClient(self._onKeyReleased, 'keyboard:release')
    self.event_manager.addClient(self._onForegroundChange, 'window:activate')
    self.setFilterMode(AEConstants.FILTER_HANDLED)

  def close(self):
    '''
    Informs the L{EventManager} that notifications about keyboard events are no
    longer of interest to this L{Keyboard} device. Sets the filtering mode to
    none for good measure. Removes the request for focus events also.
    '''
    AEInput.SystemInput.close(self)
    self.event_manager.removeClient(self._onKeyPressed, 'keyboard:press')
    self.event_manager.removeClient(self._onKeyReleased, 'keyboard:release')
    self.event_manager.removeClient(self._onForegroundChange,'window:activate')
    self.setFilterMode(AEConstants.FILTER_NONE)
    
  def getCapabilities(self):
    '''
    Gets the list of capabilities provided by this device, namely 'keyboard.'
    
    @return: 'keyboard' as the only capability of this device.
    @rtype: list of string
    '''
    return ['keyboard']

  def addFilter(self, key_filter):
    '''
    Adds a L{AEInput.GestureList} to the collection of filters. The fire
    L{AEInput.Gesture} in the L{AEInput.GestureList} will be used when the mode
    is set to filter handled to prevent keys consumed by LSR from reaching the 
    foreground application.

    The L{Keyboard} input device only supports one L{AEInput.Gesture} per
    filter, not a sequence of L{AEInput.Gesture}s, and only one non-modifier
    key code per L{AEInput.Gesture} chord. Any L{AEInput.GestureList} breaking
    these rules will cause this method to raise a ValueError.

    @param key_filter: Filter to add to the filter list
    @type key_filter: L{AEInput.GestureList}
    @raise ValueError: When the given L{AEInput.GestureList} does not conform 
      to the rules set forth above
    '''
    # make sure there is only one Gesture in the list
    if key_filter.getNumGestures() != 1:
      raise ValueError('only one Gesture allowed in a Keyboard filter')
    gesture = key_filter.getGestureAt(0)
    # make sure there is at least one keystroke in the Gesture
    if gesture.getNumActions() < 1:
      raise ValueError('at least one key code required')
    # make sure there is only one non-modifier key code
    count = 0
    for code in gesture:
      try:
        self.modifiers[code]
      except KeyError:
        count += 1
    if count > 1:
      raise ValueError('at most one non-modifier key allowed')
    # add the filter to the filters dictionary indexed by the first Gesture
    self.filters[gesture] = None

  def removeFilter(self, key_filter):
    '''
    Removes a L{AEInput.GestureList} from the collection of filters.

    @param key_filter: Filter to remove from the filter list
    @type key_filter: L{AEInput.GestureList}
    @raise KeyError: When the L{AEInput.GestureList} to remove is not found
    @raise IndexError: When the L{AEInput.GestureList} is empty
    '''
    del self.filters[key_filter.getGestureAt(0)]

  def clearFilters(self):
    '''
    Removes all filters by destroying the dictionary and recreating it.
    '''
    self.filters = {}

  def getMaxActions(self):
    '''
    Gets the maximum L{AEInput.Gesture} chord length as defined by
    L{MAX_KEY_CHORD}.

    @return: Maximum L{AEInput.Gesture} chord length
    @rtype: integer
    '''
    return MAX_KEY_CHORD

  def getName(self):
    '''
    Gets the name of this input device.

    @return: Localized device name
    @rtype: string
    '''
    return _('Keyboard')

  def resetState(self):
    '''
    Resets the state of the L{Keyboard} by clearing out the L{curr_gesture} and
    returning to L{_stateInvalid}.
    '''
    self.curr_gesture.clearActionCodes()
    self.state = self._stateInvalid
    
  def _debug(self, header, code, event, press):
    if DEBUG:
      if press:
        print 'press:', code
      else:
        print 'release:', code
      print header
      print 'next state:', self.state
      print 'consumed:', event.consume
      print 'curr key:', str(self.curr_gesture)
      print 'synth pressed:', str(self.synth_press)
      print 'synth released:', str(self.synth_release)
      print '----------------------------'
    
  def _hasOneModifier(self, gesture):
    '''
    Peeks in the given gestures to see if it contains at least one modifier.
    
    @param gesture: Gesture to check for a modifier
    @type gesture: L{AEInput.Gesture}
    @return: Does the gesture contain at least one modifier?
    @rtype: boolean
    '''
    for code in gesture.peekActionCodes():
      if self.modifiers.has_key(code):
        return True
    return False

  def _hasOnlyModifiers(self, gesture):
    '''
    Peeks in the given gesture to see if it contains only modifiers.

    @param gesture: Gesture to check for a non-modifier
    @type gesture: L{AEInput.Gesture}
    @return: Does the gesture contain at least one non-modifier?
    @rtype: boolean
    '''
    for code in gesture.peekActionCodes():
      if not self.modifiers.has_key(code):
        return False
    return True #gesture.getNumActions() != 0

  def _sendGesture(self, gesture, press):
    '''    
    Synthesizes key strokes for the key codes in the given L{AEInput.Gesture}.
    The presses are noted in L{synth_press} and L{synth_release} so that they
    can be ignored when they are seen in L{_onKeyPressed} or L{_onKeyReleased}
    to avoid duplicate processing. 

    @param press: Synthesize key presses (True) or releases (False)?
    @type press: boolean
    @param gesture: Gesture containing key codes to be pressed
    @type gesture: L{AEInput.Gesture}
    '''
    # see if the gesture is one of our eaten key combos
    if gesture in self.EATEN_GESTURES:
      # do nothing
      return

    # generate all events
    for code in gesture.peekActionCodes():
      if code in self.LOCK_KEYS or code in self.ACC_KEYS:
        continue
      self._sendKey(code, press)

  def _sendKey(self, code, press):
    '''
    Synthesizes a single key stroke. The presses are noted in L{synth_press} 
    and L{synth_release} so that they can be ignored when they are seen in 
    L{_onKeyPressed} or L{_onKeyReleased} to avoid duplicate processing.
    
    @param code: Keycode of the key to generate
    @type code: integer
    @param press: Synthesize key press (True) or releas (False)?
    @type press: boolean
    '''
    self.event_manager.generateKeyEvent(code, press)
    if press:
      self.synth_press.append(code)
    else:
      self.synth_release.append(code)
      
  def _consumeKey(self, event, press):
    '''
    Consumes the given event as long as it is not a special key used by other
    system services for accessibility purposes such as shift for slow keys
    and sticky keys.
    
    @param event: Keyboard event
    @type event: L{pyLinAcc.Event.Event}
    @param press: Press or release event?
    @type press: boolean
    '''
    #if not press or (event.detail2 not in self.ACC_KEYS):
    event.consume = True
    if press and (event.detail2 in self.ACC_KEYS):
      # allow accessibility keys to pass, but as synthesized events so later
      # release events can be paired properly by X and not generate a second 
      # press
      self._sendKey(event.detail2, True)
      
  def _handleStateful(self, event, press):
    '''
    Handles the use of stateful keys such as caps lock, num lock, and scroll
    lock as modifiers. When a lock key is pressed, this method immediately 
    sets it back to its former state. When a lock key is released, this method
    allows the change to pass but only if all of the conditions are met:
    
      - The lock key was the only key in the L{curr_gesture} since it was last
        empty.
      - The lock key is not a gesture itself.
      - The lock key is currently the only key in the L{curr_gesture}.
       
    @param event: Keyboard event
    @type event: L{pyLinAcc.Event.Event}
    @param press: Was the event a press (True) or release (False)?
    @type press: boolean
    '''
    code = event.detail2
    if code in self.LOCK_KEYS:
      if press:
        # deactive lock immediately after it has been pressed
        self._sendKey(code, True)
      elif (self.last_press == True and
            not self.filters.has_key(self.curr_gesture) and 
            len(self.curr_gesture) == 1):
        # reactivate lock if it is the only key pressed, wasn't part of a combo
        # where another key was pressed and released, and is not a filter key
        # itself
        self._sendKey(code, True)
        self._sendKey(code, False)
      
  def _statePossible(self, event, press):
    '''
    Represents a state where the current key codes in L{curr_gesture} and the
    next key press may lead to a match on one of the registered L{filters}.
    The actions in this state are as follows.

    If the event is a press or release of one of the registered L{modifiers},
    the key is consumed and we remain in this state.

    If the event is a press of a non-modifier and the L{curr_gesture} matches
    one of the registered L{filters}, we consume the key and transition to
    L{_stateMatch}.

    If the event is anything else, we synthesize all keys in the
    L{curr_gesture}, consume the current event, and transition to
    L{_stateInvalid}.

    @param event: Keyboard event
    @type event: L{pyLinAcc.Event.Event}
    @param press: Was the event a press (True) or release (False)?
    @type press: boolean
    @return: Method representing the next state
    @rtype: callable
    '''
    if self.modifiers.has_key(event.detail2):
      # stay in this state and consume the key
      self._consumeKey(event, press)
      return self._statePossible
    elif self.filters.has_key(self.curr_gesture):
      # move to the match state and consume the key
      self._consumeKey(event, press)
      return self._stateMatch
    else:
      # update the os key state
      self._sendGesture(self.curr_gesture, True)
      # move to the invalid state and consume the current key
      self._consumeKey(event, press)
      return self._stateInvalid

  def _stateInvalid(self, event, press):
    '''
    Represents a state where the current key codes in L{curr_gesture} and the
    next key event is not likely to match one of the registered L{filters}.
    The actions in this state are as follows.

    If the event is a press resulting in the L{curr_gesture} having only
    L{modifiers} in it, we consume the press and transition to
    L{_statePossible}.
    
    If the event is a press resulting in the L{curr_gesture} exactly matching
    one of the registered L{filters}, we consume the press transition to 
    L{_stateMatch}.

    If the event is anything else, we allow the event to pass and stay in this
    state.

    @param event: Keyboard event
    @type event: L{pyLinAcc.Event.Event}
    @param press: Was the event a press (True) or release (False)?
    @type press: boolean
    @return: Method representing the next state
    @rtype: callable
    '''
    if press:
      if self._hasOnlyModifiers(self.curr_gesture):
        # just a modifier, possibly a filter to match
        self._consumeKey(event, press)
        return self._statePossible
      elif self.filters.has_key(self.curr_gesture):
        # already a matched filter
        self._consumeKey(event, press)
        return self._stateMatch
      elif len(self.synth_press):
        # still waiting for presses to be synthesized, eat and synth
        self._consumeKey(event, press)
        self._sendKey(event.detail2, True)

  def _stateMatch(self, event, press):
    '''
    Represents a state where the current key codes in L{curr_gesture} matches
    one of the L{filters} and the next event may determine if the key codes in
    L{curr_gesture} are consumed forever or synthesized to be passed on to the
    OS. The actions in this state are as follows.

    If the event is a press and L{curr_gesture} matches one of the L{filters},
    we stay in this state. (This case is needed to handle repeating, typomatic
    presses of non-modifier keys.)

    If the event is a press and L{curr_gesture} no longer matches one of the
    L{filters}, we synthesize key presses for all keys in L{curr_gesture},
    consume the current press, and transition to L{_stateInvalid}.

    If the event is a release of one of the L{modifiers}, we consume the
    release and move to L{_stateAfterMatch}.

    If the event is anything else (i.e. a non-modifier release), we consume the
    key and transition to L{_statePossible}.

    @param event: Keyboard event
    @type event: L{pyLinAcc.Event.Event}
    @param press: Was the event a press (True) or release (False)?
    @type press: boolean
    @return: Method representing the next state
    @rtype: callable
    '''
    if press:
      if self.filters.has_key(self.curr_gesture):
        # stay in this state and eat the key, typomatic
        self._consumeKey(event, press)
        return self._stateMatch
      else:
        # send all key presses
        self._sendGesture(self.curr_gesture, True)
        # move to the invalid state and eat the key
        self._consumeKey(event, press)
        return self._stateInvalid
    elif self.modifiers.has_key(event.detail2):
      # move to the after match state and eat the key
      self._consumeKey(event, press)
      return self._stateAfterMatch
    else:
      # move to the possible state and eat the key
      self._consumeKey(event, press)
      return self._statePossible
    
  def _stateAfterMatch(self, event, press):
    '''    
    Represents a state where the the keys in L{curr_gesture} just matched one
    of the L{filters} and the next event was a release of one of the modifier
    keys instead of the non-modifier. This state is needed to avoid typomatic
    repeat presses of the still held non-modifier key. The actions in this
    state are as follows.
    
    If the event is a release of a non-modifier and the L{curr_gesture} is now
    empty, we consume the release and move to L{_stateInvalid}.
    
    If the event is a release of one of the non-modifiers and the
    L{curr_gesture} still contains key strokes (e.g. L{modifiers}), we consume
    the release and move to L{_statePossible}.
    
    @param event: Keyboard event
    @type event: L{pyLinAcc.Event.Event}
    @param press: Was the event a press (True) or release (False)?
    @type press: boolean
    @return: Method representing the next state
    @rtype: callable
    '''
    if not press and not self.modifiers.has_key(event.detail2):
      # eat the key
      self._consumeKey(event, press)
      if self.curr_gesture.getNumActions() == 0:
        # move to state invalid
        return self._stateInvalid
      else:
        # move to state possible
        return self._statePossible
    else:
      # stay in this state and eat the key
      self._consumeKey(event, press)

  def _onKeyPressed(self, event):
    '''
    Handles a key press event by acting on the hardware key code in
    event.detail2. If the key code is at the top of the L{synth_press} list,
    then the press is ignored since it was synthesized and that code is removed
    from the list. Otherwise, an attempt is made to add the key code to the
    L{curr_gesture}. If the current gesture is full, the press is ignored and
    the method returns immediately. If not, the event is dispatched to the
    current state method (one of L{_statePossible}, L{_stateInvalid},
    L{_stateMatch}). The return value of the state method is set as the next
    state and the L{last_press} flag is set to True.

    @param event: Keyboard key press event
    @type event: L{pyLinAcc.Event.Event}
    '''
    code = event.detail2
    if len(self.synth_press) and event.detail2 == self.synth_press[0]:
      # ignore keys we know we synthesized
      self.synth_press.pop(0)
      self._debug('ignored our press', code, event, True)
      return
    elif code in self.synth_release:
      # ignore presses for keys we're waiting to release
      # do not account for ACC_KEYS, ignore everything
      event.consume = True
      self._debug('ignored our press before release', code, event, True)
      return

    if code not in self.curr_gesture:
      # only count a non-typomatic press as a last press
      self.last_press = True
      # don't need to do the following if it was a typomatic press, repeats are
      # ignored when trying to add to a Gesture
      try:
        # try to store the action code in the current gesture
        self.curr_gesture.addActionCode(code)
      except ValueError:
        # ignore the press if the gesture is full
        self._debug('error adding', code, event, True)
        return
      
    self._handleStateful(event, True)
      
    self.state = self.state(event, True) or self.state
    self._debug('state machine handled', code, event, True)

  def _onKeyReleased(self, event):
    '''
    Handles a key release event by acting on the hardware key code in
    event.detail2. If the key code is at the top of the L{synth_release} list,
    then the release is ignored since it was synthesized and that code is
    removed from the list. Otherwise, the L{last_press} flag is checked to see
    if it is True indicating the last event was a key press. If so, the
    L{curr_gesture} is copied into a new L{AEInput.Gesture} and sent to any
    listeners via the L{_notifyInputListeners} method. Next an attempt is made
    to remove the key code from the L{curr_gesture}. If the key code does not
    exist, the release is ignored and the method returns immediately. If not,
    the event is dispatched to the current state method (one of
    L{_statePossible}, L{_stateInvalid}, L{_stateMatch}). The return value of
    the state method is set as the next state and the L{last_press} flag is set
    to False.

    @param event: Keyboard key release event
    @type event: L{pyLinAcc.Event.Event}
    '''
    code = event.detail2
    if len(self.synth_release) and code == self.synth_release[0]:
      # ignore keys we know we synthesized
      self.synth_release.pop(0)
      try:
        # take the released key out of the gesture
        self.curr_gesture.removeActionCode(code)
      except ValueError:
        pass
      self._debug('ignored our release', code, event, False)
      return
    elif len(self.synth_press):
      # ignore releases when we're waiting to synth presses
      self._consumeKey(event, False)
      # but make sure to synth our own release
      self._sendKey(code, False)
      self._debug('ignored release before press', code, event, False)
      return
    elif self.last_press:
      # make a copy of the current gesture before we modify it
      gesture = AEInput.Gesture(self, gesture=self.curr_gesture)
      # notify listeners of the gesture and its timestamp
      self._notifyInputListeners(gesture, event.any_data[3])

    # do special handling for stateful keys
    self._handleStateful(event, False)     
      
    try:
      # take the released key out of the gesture
      self.curr_gesture.removeActionCode(code)
    except ValueError:
      self._debug('error removing', code, event, False)
      return
    
    self.state = self.state(event, False) or self.state
    self._debug('state machine handled', code, event, False)
    self.last_press = False
    
  def _onForegroundChange(self, event):
    '''    
    Resets the internal key state on a window:activate event. This is needed to
    resolve the problem of system wide hotkey releases going missing.
    
    @see: U{http://bugzilla.gnome.org/show_bug.cgi?id=104058}
    @param event: Keyboard key release event
    @type event: L{pyLinAcc.Event.Event}
    '''
    self.resetState()

  def asString(self, gesture):
    '''
    Gets a string representation of all of the key codes in the given
    L{AEInput.Gesture} using the L{_keyCodeToKeyName} function

    @param gesture: L{AEInput.Gesture} to render as a string
    @type gesture: L{AEInput.Gesture}
    @return: String representation of the provided L{AEInput.Gesture}
    @rtype: string
    '''
    return ','.join([_keyCodeToKeyName(i) for i in gesture.getActionCodes()])
  
  # action code constants specific to the Keyboard device (key codes)
  AEK_ESCAPE = 9
  AEK_F1 = 67
  AEK_F2 = 68
  AEK_F3 = 69
  AEK_F4 = 70
  AEK_F5 = 71
  AEK_F6 = 72
  AEK_F7 = 73
  AEK_F8 = 74
  AEK_F9 = 75
  AEK_F10 = 76
  AEK_F11 = 95
  AEK_F12 = 96
  AEK_PRINT_SCREEN = 111 # PP: unconfirmed
  AEK_SCROLL_LOCK = 78
  AEK_PAUSE = 110
  AEK_TILDE = 49
  AEK_1 = 10
  AEK_2 = 11
  AEK_3 = 12
  AEK_4 = 13
  AEK_5 = 14
  AEK_6 = 15
  AEK_7 = 16
  AEK_8 = 17
  AEK_9 = 18
  AEK_0 = 19
  AEK_MINUS = 20
  AEK_EQUALS = 21
  AEK_BACK_SPACE = 22
  AEK_TAB = 23
  AEK_Q = 24
  AEK_W = 25
  AEK_E = 26
  AEK_R = 27
  AEK_T = 28
  AEK_Y = 29
  AEK_U = 30
  AEK_I = 31
  AEK_O = 32
  AEK_P = 33
  AEK_OPEN_BRACKET = 34
  AEK_CLOSE_BRACKET = 35
  AEK_BACK_SLASH = 51
  AEK_CAPS_LOCK = 66
  AEK_A = 38
  AEK_S = 39
  AEK_D = 40
  AEK_F = 41
  AEK_G = 42
  AEK_H = 43
  AEK_J = 44
  AEK_K = 45
  AEK_L = 46
  AEK_SEMICOLON = 47
  AEK_QUOTE = 48
  AEK_ENTER = 36
  AEK_SHIFT_L = 50
  AEK_Z = 52
  AEK_X = 53
  AEK_C = 54
  AEK_V = 55
  AEK_B = 56
  AEK_N = 57
  AEK_M = 58
  AEK_COMMA = 59
  AEK_PERIOD = 60
  AEK_SLASH = 61
  AEK_SHIFT_R = 62
  AEK_CONTROL_L = 37
  AEK_ALT_L = 64
  AEK_SPACE = 65
  AEK_ALT_R = 113
  AEK_MENU = 117
  AEK_CONTROL_R = 109
  AEK_LEFT = 100
  AEK_RIGHT = 102
  AEK_UP = 98
  AEK_DOWN = 104
  AEK_PAGE_DOWN = 105
  AEK_PAGE_UP = 99
  AEK_DELETE = 107
  AEK_INSERT = 106
  AEK_HOME = 97
  AEK_END = 103
  AEK_NUM_LOCK = 77
  AEK_DIVIDE = 112
  AEK_MULTIPLY = 63
  AEK_ADD = 86
  AEK_SUBTRACT = 82
  AEK_DECIMAL = 91
  AEK_NUMPAD0 = 90
  AEK_NUMPAD1 = 87
  AEK_NUMPAD2 = 88
  AEK_NUMPAD3 = 89
  AEK_NUMPAD4 = 83
  AEK_NUMPAD5 = 84
  AEK_NUMPAD6 = 85
  AEK_NUMPAD7 = 79
  AEK_NUMPAD8 = 80
  AEK_NUMPAD9 = 81
  AEK_NUMPAD_ENTER = 108
  AEK_THINKPAD_FN = 227
  AEK_THINKPAD_BACK = 234
  AEK_THINKPAD_FORWARD = 233
