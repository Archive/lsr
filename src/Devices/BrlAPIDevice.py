'''
This device provides the braille interface to brlapi (brltty)

@var MAX_KEY_CHORD: brlapi only handles a key press event, thus making chords 
  impossible.  All key events coming back from braille devices are abstacted 
  by brlapi to a single key_cmd.
@type MAX_KEY_CHORD: integer

@author: Scott Haeger
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
import brlapi, gobject, time, logging, array
import AEOutput, AEInput
from i18n import _
from AEConstants import *

__uie__ = dict(kind='device')

MAX_KEY_CHORD = 1
log = logging.getLogger('BrlAPIDevice')

def _keyCodeToKeyName(command):
  '''
  Gets the name of the given key code by using the brlapi's module dictionary
  to lookup its command value and return its command name. If the code is not
  known, an empty string is returned.
  
  @note: brlapi delivers commands in the tuple (type, command, argument, flags)
  
  @param command: Hardware keycode command
  @type command: integer
  @return: Name of the key
  @rtype: string
  '''
  for name, value in brlapi.__dict__.items():
      if command == value:
        return name
  return ''


class BrlAPIStyle(AEOutput.BrailleStyle):
  '''
  Overrides the base L{AEOutput.BrailleStyle} class.  All properties
  currently reside in base class. 
  '''
  def getGroups(self):
    '''
    Gets configurable absolute settings affecting all output from this device.
    
    @return: Root group of all configurable settings
    @rtype: L{AEState.Setting.Group}
    '''
    root = self.newGroup()
    if self.isDefault():
      # generate a group for standard braille settings
      self._newBrailleGroup(root)
    return root
 
class BrlAPIDevice(AEOutput.Braille, AEInput.AEInput):
  '''
  Braille output via Brlapi interface.
  '''
  USE_THREAD = False
  STYLE = BrlAPIStyle
  EllipsisStyles = [ (chr(brlapi.DOT4 + \
                    brlapi.DOT5 + brlapi.DOT6)+ \
                    chr(brlapi.DOT1 + brlapi.DOT2 + brlapi.DOT3+\
                    brlapi.DOT4 + brlapi.DOT6)+chr(0),chr(0)+ \
                    chr(brlapi.DOT4+brlapi.DOT5 + brlapi.DOT6)+\
                    chr(brlapi.DOT1 + brlapi.DOT2 + brlapi.DOT3+\
                    brlapi.DOT4 + brlapi.DOT6)), \
                    (''.center(3,chr(brlapi.DOT3))+chr(0),\
                    chr(0)+''.center(3,chr(brlapi.DOT3)))]
  
  def init(self):
    '''
    Initializes the braille interface.
    
    @ivar brlconn:  connection object to brlapi
    @type brlconn: object    
    @ivar writestruct:  write structure for brlapi.write()
    @type writestruct: object 
    @ivar inputmngr: Input listener thread reference 
    @type inputmngr: object 
    @ivar last_style:  The last style object seen
    @type last_style: integer 
    @ivar commands:  List of output commands
    @type commands: list 
    @ivar caretpos: The one-based cell position of the caret.
    @type caretpos: integer 
    
    @raise AEOutput.InitError: When the device can not be initialized
    '''
    AEInput.AEInput.__init__(self)
    
    self.brlconn = None
    self.writestruct = None
    self.inputmngr = None
    self.last_style = None
    self.commands = []
    self.caretpos = 0
    self.registered_keys = {}
    
    try:
      self.brlconn = brlapi.Connection()  
      self.brlconn.enterTtyMode()
      self.writestruct = brlapi.WriteStruct()
    except brlapi.ConnectionError:
      raise AEOutput.InitError
    
    # load key command bindings and put in __class__.__dict__
    self._loadKeyCmdConstants()
    
    # tell brlapi to not send me any key_cmds.  Each task will register for key
    # TODO fix when brltty API settles down
    # self.brlconn.ignoreKeyRange([0, brlapi.KEY_MAX])
    self.brlconn.ignoreAllKeys()
    
    # start input event manager
    self.watcher = gobject.io_add_watch(self.brlconn.fileDescriptor,
                                        gobject.IO_IN,
                                        self._readInput)
    
    # populate display size properties
    self.default_style.DisplayColumns = self.brlconn.displaySize[0]
    self.default_style.DisplayRows = self.brlconn.displaySize[1]
    self.default_style.TotalCells = self.brlconn.displaySize[0] * self.brlconn.displaySize[1]
    
  def postInit(self):
    '''
    Called after the L{init} method and after either L{loadStyles} or 
    L{createDistinctStyles}. missingcellcnt is the number of missing (broken)
    cells defined by the user and stored in the L{Setting} string "CellMask".
    
    @raise Error.InitError: When a communication or state problem exists for 
      the specific device
    '''
    self.default_style.missingcellcnt = self.default_style.CellMask.count('0')    
       
  def getBrlConnection(self):
    '''
    Returns the connection object to brltty 
    
    @return: A brlapi connection object
    @rtype: brlapi.Connection
    '''
    return self.brlconn
     
  # -------------    begin input related methods  -----------------------------
  def _loadKeyCmdConstants(self):
    '''
    Reads brlapi constants and puts them in this class 
    '''
    for name, value in brlapi.__dict__.items():
      if name.startswith('KEY_CMD'):
        setattr(self.__class__, name, value)
        
  def _readInput(self, arg1, arg2):
    key = self.brlconn.readKey(wait=False)
    if key is not None:
      # Callback requires True/False return value.  _onKeyClick returns True/False
      # based on it's success
      return self._onKeyClick(key)

  def _onKeyClick(self, key):
    '''
    Handles a key click event by acting on the hardware key code in
    key. 
    
    @param key: a brlapi structure representing a key click event
    @type brlapi.key: key
    @return: successfully notified listeners
    @rtype: boolean
    '''
    try:
      k = self.brlconn.expandKeyCode(key)  
    except brlapi.OperationError:
      log.warn('braille input failed')
      return False
    print "Key %ld (%x %x %x %x) !" % (key, k["type"], k["command"], k["argument"], k["flags"])
    print "keycode=", _keyCodeToKeyName(k["command"])
    
    gesture = AEInput.Gesture(self)
    gesture.addActionCode(k['command'])
    
    # use default arguments or create key_cmd specific ones
    if k['command'] == brlapi.KEY_CMD_ROUTE:
      if len(self.default_style.CellMask) == self.default_style.TotalCells:
        # pressed a dead cell
        if self.default_style.CellMask[k['argument']] == '0':
          return True
        # adjust cell number by number of missing cells between selected and left end
        for i in range(k['argument']):
          if self.default_style.CellMask[i] == '0':
            k["argument"] -= 1
    
    # notify listeners of the gesture with given args at the timestamp
    self._notifyInputListeners(gesture, time.time(), \
                argument=k["argument"], flags=k["flags"])
    return True

  def getMaxActions(self):
    '''
    Gets the maximum number of actions that can be in a L{Gesture} on this 
    input device.
    
    @return: Maximum number of actions per L{Gesture} supported by this device
    @rtype: integer
    '''
    return MAX_KEY_CHORD
    
  def asString(self, gesture): 
    ''' 
    Gets a human readable representation of the given L{Gesture}.
    
    @param gesture: L{Gesture} object to render as text
    @type gesture: L{Gesture}
    @return: Text representation of the L{Gesture} 
    @rtype: string
    '''
    return ','.join([_keyCodeToKeyName(i) for i in gesture.getActionCodes()])
  
  def addKeyCmd(self, codes):
    ''' 
    Registers each KEY_CMD within codes.
    
    @param codes: list of lists of KEY_CMD* codes
    @type codes: list
    @raise NotImplementedError: When this method is not overridden by a subclass
    '''
    for codelist in codes:
      for code in codelist:
        if self.registered_keys.has_key(code):
          self.registered_keys[code] += 1
        else:
          self.registered_keys[code] = 1
          self.brlconn.acceptKeys(brlapi.rangeType_command,[brlapi.KEY_TYPE_CMD|code])
            
  def removeKeyCmd(self, codes):
    ''' 
    Unregisters each KEY_CMD within codes.
    
    @param codes: list of lists of KEY_CMD* codes
    @type codes: list
    @raise NotImplementedError: When this method is not overridden by a subclass
    '''
    for codelist in codes:
      for code in codelist:
        try:
          self.registered_keys[code] -= 1
          if self.registered_keys[code] == 0:
            del self.registered_keys[code]
            self.brlconn.ignoreKeys(brlapi.rangeType_command,[brlapi.KEY_TYPE_CMD|code])
        except KeyError:
          pass
  
  # -------------    begin output related methods  ----------------------------
  def close(self):
    '''
    Closes the braille device.
    '''
    gobject.source_remove(self.watcher)
    del self.brlconn
    # this call has blocked in past if brltty is offline.
    self.brlconn.leaveTtyMode()
    
    self.commands = []
    self.brlconn = None
    
  def sendString(self, text, style):
    '''
    Adds the given text and associated style to command list.  Text will be 
    output to the braille device in sendTalk.

    @param text: String to be output
    @type text: string
    @param style: Style with which this string should be output; None means no
      style change should be applied
    @type style: integer
    '''
    self.commands.append((CMD_STRING, text, style))
       
  def sendTalk(self, style=None):
    '''
    Iterates through list of commands and builds output string including user
    selected ellipsis and caret.
    
    @param style: Ignored
    @type style: L{AEOutput.Style}
    '''
    stream = []
    truncateleft = False
    truncateright = False
    # iterate through commands and build pre-output string and set local vars
    for command, value, style in self.commands:
      if style is not None and (style.isDirty() or self.last_style != style):
        self.last_style = style
      if command is CMD_STRING:
        if value is not None:
          stream.append(value)
      elif command is CMD_TRUNCATE:
        truncateleft = value[0]
        truncateright = value[1]
       
    # get preliminary output string as an array of unicode
    outarray = array.array('u', ''.join(stream))
    
    if len(self.default_style.CellMask) == self.default_style.TotalCells:
      # insert space for missing cell.  min for protection
      for i in xrange(min(len(outarray),self.default_style.TotalCells)): 
        if self.default_style.CellMask[i] == '0':
          outarray.insert(i, u' ')
          if i < self.caretpos:
            self.caretpos += 1
    elif len(self.default_style.CellMask) != 0:
      log.info("Missing cell mask not equal to display length")
    
    # make sure length equals display length before sending to device
    if len(outarray) < self.default_style.TotalCells:
      for i in xrange(self.default_style.TotalCells - len(outarray)):
        outarray.append(u' ')
    elif len(outarray) > self.default_style.TotalCells:
      outarray = outarray[0:self.default_style.TotalCells]
      
    # initialize and/or masks
    andmask = array.array('c', ''.center(self.default_style.TotalCells, \
                    chr(brlapi.DOT1 + brlapi.DOT2 +\
                        brlapi.DOT3 + brlapi.DOT4 +\
                        brlapi.DOT5 + brlapi.DOT6 +\
                        brlapi.DOT7 + brlapi.DOT8)))
    ormask = array.array('c', ''.center(40, chr(0)))
    
    # add ellipsis properties to masks
    if truncateleft and self.default_style.EllipsisLeft:
      for i in range(len(self.EllipsisStyles[self.default_style.EllipsisStyle][0])):
        andmask[i] = chr(0)
        ormask[i] = self.EllipsisStyles[self.default_style.EllipsisStyle][0][i]
        outarray[i] = u' '  # corrects brltty/xwindows bug
    if truncateright and self.default_style.EllipsisRight: 
      count = 0
      for i in range(self.default_style.TotalCells-len(self.EllipsisStyles[self.default_style.EllipsisStyle][1]), self.default_style.TotalCells):
        andmask[i] = chr(0)
        ormask[i] = self.EllipsisStyles[self.default_style.EllipsisStyle][1][count]
        outarray[i] = u' '  # corrects brltty/xwindows bug
        count += 1
        
    # add cursor properties to mask and set cursor position
    if self.caretpos <= 0 or self.caretpos > self.default_style.TotalCells:
      self.writestruct.cursor = 0    
    elif self.default_style.CaretStyle is CARET_NONE:
      self.writestruct.cursor = 0  
    elif self.default_style.CaretStyle is CARET_TWO_BOTTOM:
      ormask[self.caretpos-1] = chr(brlapi.DOT7 + brlapi.DOT8)
      self.writestruct.cursor = self.caretpos
    # TODO: check pins for CaretBottomRight
    elif self.default_style.CaretStyle is CARET_BOTTOM_RIGHT:
      ormask[self.caretpos-1] = chr(brlapi.DOT6 + brlapi.DOT8)
      self.writestruct.cursor = self.caretpos
    else: # style.CaretStyle is style.CaretAll:
      ormask[self.caretpos-1] = chr(brlapi.DOT1 + brlapi.DOT2 +\
                        brlapi.DOT3 + brlapi.DOT4 +\
                        brlapi.DOT5 + brlapi.DOT6 +\
                        brlapi.DOT7 + brlapi.DOT8)
      self.writestruct.cursor = self.caretpos
                        
    # set write structure fields
    self.writestruct.attrAnd = andmask.tostring()
    self.writestruct.attrOr = ormask.tostring()
    self.writestruct.text = outarray.tounicode() 
    self.writestruct.regionBegin = 1
    self.writestruct.regionSize = self.default_style.TotalCells
    
    # write to device  
    try:
       self.brlconn.write(self.writestruct) 
    except brlapi.OperationError:
       log.warn('braille output failed')
    
    self.commands = []
      
  def sendTruncate(self, left, right, style):
    '''
    Sends indicators of whether text was truncated on either side of the 
    current line or not. The style object is used by the device in deciding how
    the truncation should be presented.

    @param left: Was text truncated to the left?
    @type left: boolean
    @param right: Was text truncated to the right?
    @type right: boolean
    @param style: Style with which the truncation should be indicated
    @type style: L{AEOutput.Style}
    @raise NotImplementedError: When not overridden in a subclass
    '''
    self.commands.append((CMD_TRUNCATE, (left,right), style))
    
  def sendCaret(self, pos, style):
    '''
    Sends the current caret position relative to the first cell (zero-offset) 
    on the device. The style object is used by the device in deciding how the
    caret should be presented.
    @note: braille displays are one-based.  We will offset here.
    @param pos: Zero-offset cell position of the caret, up to the device to 
      change to one-offset if need be
    @type pos: string
    @param style: Style with which the caret should be indicated
    @type style: L{AEOutput.Style}
    @raise NotImplementedError: When not overridden in a subclass
    '''
    self.caretpos = pos + 1
    
  def sendGetEllipsisSizes(self, style):
    '''
    @param style: Style with which the ellipsis are defined
    @type style: L{AEOutput.Style}
    @return: tuple containing length of left and right ellipsis
    @rtype: tuple
    @raise NotImplementedError: When not overridden in a subclass
    '''
    left = 0
    right = 0
    
    if self.default_style.EllipsisLeft:
      left = len(self.EllipsisStyles[self.default_style.EllipsisStyle][0])
    if self.default_style.EllipsisRight:
      right = len(self.EllipsisStyles[self.default_style.EllipsisStyle][1])
   
    return (left, right)
    
  def sendGetMissingCellCount(self):
    '''
    @return: integer containing missing cell count
    @rtype: integer
    @raise NotImplementedError: When not overridden in a subclass
    '''
    return self.default_style.missingcellcnt
  
  def isActive(self):
    '''
    Indicates whether the device has text waiting to be output.

    @return: True if the braille device has text to be output, False otherwise
    @rtype: boolean
    '''
    if len(self.commands) > 0:
      return True
    else:
      return False
  
  def sendStop(self, style=None):
    '''
    Stops braille output immediately.
    
    @param style: Ignored
    @type style: L{AEOutput.Style}
    '''
    self.commands = []
    try:
      self.brlconn.writeText("", cursor=0)
    except brlapi.OperationError:
      log.warn('braille output failed')
      pass
    