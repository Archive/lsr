'''
Defines a device that uses the IBM TTS synthesizer via the pyibmtts package.
pyibmtts can be obtained from U{http://ibmtts-sdk.sf.net}.

@author: Larry Weiss
@author: Peter Parente
@author: Brett Clippingdale
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
import AEOutput
import pyibmtts
from i18n import _

__uie__ = dict(kind='device')

class Languages(object):
  '''
  Determines the available languages and stores their IBM TTS constants, 
  IANA tags, and human readable names.
  
  @ivar rfc_to_ibm: Mapping from IANA language tag to IBM language constants
  @type rfc_to_ibm: dictionary of string : int
  @ivar rfc_to_name: Mapping from IANA language tag to human readable 
  (translated) name
  @type rfc_to_name: dictionary of string : string
  @ivar ibm_to_rfc: Reverse of L{rfc_to_ibm}
  @type ibm_to_rfc: dictionary of int : string
  @ivar choices: Mapping from language names to language IANA tags
  @type choices: dictionary of string : string
  @ivar tags: All potentially supported language tags
  @type tags: string
  '''
  def __init__(self):
    '''
    Initializes the instance by mapping IBM TTS constants to standard language 
    and dialect names.
    '''
    self.tags = [
      'en-US', 
      'en-UK', 
      'es-ES', 
      'es-MX', 
      'fr-FR',
      'fr-CA',
      'de-DE',
      'it-IT', 
      'zh-cmn', 
      'zh-cmn-x-pinyin', 
      'zh-cms-TW', 
      'zh-cms-TW-x-zhuyin', 
      'zh-cms-TW-x-pinyin', 
      'pt-BR', 
      'ja-JP', 
      'ja-JP', 
      'fn-FI', 
      'ko-KR', 
      'zh-yue', 
      'zh-yue-HK', 
      'nl-NL', 
      'no-NO', 
      'sv-SE', 
      'da-DK',
      'th-TH']
    
    # make all tags lowercase
    self.tags = [t.lower() for t in self.tags]
    
    # all supported IBM languages
    ibms = [
      pyibmtts.GeneralAmericanEnglish,
      pyibmtts.BritishEnglish,
      pyibmtts.CastilianSpanish,
      pyibmtts.MexicanSpanish,
      pyibmtts.StandardFrench,
      pyibmtts.CanadianFrench,
      pyibmtts.StandardGerman,
      pyibmtts.StandardItalian,
      pyibmtts.MandarinChineseUCS,
      pyibmtts.MandarinChinesePinYin,
      pyibmtts.TaiwaneseMandarinUCS,
      pyibmtts.TaiwaneseMandarinZhuYin,
      pyibmtts.TaiwaneseMandarinPinYin,
      pyibmtts.BrazilianPortuguese,
      pyibmtts.StandardJapanese,
      pyibmtts.StandardJapaneseUCS,
      pyibmtts.StandardFinnish,
      pyibmtts.StandardKoreanUCS,
      pyibmtts.StandardCantoneseUCS,
      pyibmtts.HongKongCantoneseUCS,
      pyibmtts.StandardDutch,
      pyibmtts.StandardNorwegian,
      pyibmtts.StandardSwedish,
      pyibmtts.StandardDanish,
      pyibmtts.StandardThai]
    
    # names for all languages
    names = [
      _('American English'),
      _('British English'),
      _('Castilian Spanish'),
      _('Mexican Spanish'),
      _('French'),
      _('Canadian French'),
      _('German'),
      _('Italian'),
      _('Mandarin Chinese'),
      _('Mandarin Chinese Pinyin'),
      _('Taiwanese Mandarin'),
      _('Taiwanese Mandarin Zhuyin'),
      _('Taiwanese Mandarin Pinyin'),
      _('Brazilian Portuguese'),
      _('Japanese'),
      _('Finnish'),
      _('Korean'),
      _('Chinese Cantonese'),
      _('Hong Kong Cantonese'),
      _('Dutch'),
      _('Norwegian'),
      _('Swedish'),
      _('Danish'),
      _('Thai')]
    
    # mappings for fast lookup
    self.rfc_to_ibm = dict(zip(self.tags, ibms))
    self.ibm_to_rfc = dict(zip(ibms, self.tags))
    self.rfc_to_name = dict(zip(self.tags, names))

    # compute all available languages once when the module is imported
    try:
      consts = pyibmtts.getSupportedLangs()
    except AttributeError:
      # old version of pyibmtts won't support detection of langs, hardcode to
      # english only
      consts = [pyibmtts.GeneralAmericanEnglish]
    # store tags and names for these constants
    ts = [self.ibm_to_rfc[c] for c in consts if c in self.ibm_to_rfc]
    ns = [self.rfc_to_name[t] for t in ts]
    self.choices = dict(zip(ns, ts))
    
  def getTags(self):
    '''
    @return: All tags
    @rtype: list of string
    '''
    return self.choices.values()
       
  def getChoices(self):
    '''
    Gets tags mapped to their names.
    
    @return: Mapping from language names to language tags
    @rtype: dictionary of string : string
    '''
    return self.choices
  
  def ibmToRfc(self, val):
    '''
    Converts an IBM language constant value to an IANA tag.
    
    @param value: Language constant
    @type value: integer
    @return: IANA tag name
    @rtype: string
    '''
    return self.ibm_to_rfc[val]
  
  def rfcToIbm(self, val):
    '''
    Converts an IANA tag to an IBM constant.
    
    @param val: IANA tag name
    @type val: string
    @return: Language constant
    @rtype: integer
    '''
    return self.rfc_to_ibm[val]
  
  def rfcToName(self, val):
    '''
    Converts an IANA tag to descriptive name.
    
    @param val: IANA tag name
    @type val: string
    @return: Language and dialect name
    @rtype: string
    '''
    return self.rfc_to_name[val]

class IBMSpeechStyle(AEOutput.AudioStyle):
  '''
  Overrides the base L{AEOutput.Style} class, filling in fields for supported 
  style properties with their appropriate values.
  '''
  def init(self, device):
    '''
    Create style settings based on known properties of the IBM text to speech
    engine.
    '''
    self.newEnum('Voice', 1, _('Voice'),
                 {_('Adult Male 1') : 1,
                  _('Adult Female 1'): 2,
                  _('Child 1') : 3,
                  _('Adult Male 2') : 4,
                  _('Adult Male 3') : 5,
                  _('Adult Female 2') : 6,
                  _('Elderly Female 1') : 7,
                  _('Elderly Male 1') : 8},
                 _('Name of the active voice'))
    self.newRelRange('Pitch', 220, _('Pitch'), 40, 442, 0,
                     _('Baseline voice pitch in Hertz'))
    self.newRelRange('Rate', 250, _('Rate'), 176, 1297, 0,
                     _('Speech rate in words per minute'))
    self.newRelPercent('Volume', 58982, _('Volume'), 1, 65535, 0,
                       _('Speech volume as a percentage'))
    if self.isDefault():
      self.newEnum('Language', 'en-US'.lower(), _('Language'), 
                   device.langs.getChoices(),
                   _('Name of the language and dialect engine to use when '
                     'synthesizing speech'))

  def getGroups(self):
    '''
    Gets configurable absolute settings affecting all output from this device.
    
    @return: Group of all configurable settings
    @rtype: L{AEState.Setting.Group}
    '''
    g = self.newGroup()
    s = g.newGroup(_('Speech'))
    s.extend(['Pitch', 'Rate', 'Volume', 'Language'])
    if self.isDefault():
      # generate a group for standard word parsing settings on the default
      # object only for now
      self._newWordGroup(g)
    return g
  
class IBMSpeech(AEOutput.Audio):
  '''
  IBM TTS via the pyibmtts wrapper.
 
  @ivar last_style: Last style object to be applied to output
  @type last_style: L{AEOutput.Style}
  @ivar langs: Object that manages the available languages and mappings between
    naming conventions for them  
  @type langs: L{Languages}
  '''
  USE_THREAD = False
  STYLE = IBMSpeechStyle
  
  def _newEngine(self, lang):
    '''
    Creates a new engine with the given language and defaults to using real
    world units. Does not catch any exceptions.
    
    @return: Engine instance
    @rtype: pyibmtts.Engine
    @raise pyibmtts.ECIException: When the engine could not be initialized
    '''
    tts = pyibmtts.Engine(lang)
    tts.realWorldUnits = True
    return tts
    
  def init(self):
    '''
    Initializes the IBM TTS speech driver through pyibmtts.
    
    @raise AEOutput.InitError: When the device can not be initialized
    '''
    self.last_style = None
    # do this before engine start so we don't have a conflict when searching
    # which langs are available
    self.langs = Languages()

    try:
      self.tts = self._newEngine(None)
    except pyibmtts.ECIException:
      raise AEOutput.InitError

  def createDistinctStyles(self, num_groups, num_layers):
    '''
    Creates distinct styles following the guidelines set forth in the base
    class. Only distinguishes groups using voice, not layers.
    
    @param num_groups: Number of sematic groups the requestor would like to
      represent using distinct styles
    @type num_groups: integer
    @param num_layers: Number of content origins (e.g. output originating from
      a background task versus the focus) the requestor would like to represent
      using distinct styles
    @type num_layers: integer   
    @return: Styles
    @rtype: list of L{AEOutput.Style}
    ''' 
    styles = []
    for i in xrange(num_groups*num_layers):
      # create a new style object
      s = IBMSpeechStyle(self.default_style)
      # initialize it
      s.init(self)
      v_num = (i % 8) + 1
      v = self.tts.getVoice(v_num)
      s.Voice = v_num
      # set relative pitch value
      s.Pitch = v.pitchBaseline
      # store the style for later
      styles.append(s)
    return styles

  def close(self):
    '''
    Closes the speech device.
    '''
    del self.tts

  def sendStop(self, style=None):
    '''
    Stops speech immediately.
    
    @param style: Ignored
    @type style: L{AEOutput.Style}
    '''
    try:
      self.tts.stop()
    except pyibmtts.ECIException:
      pass

  def sendTalk(self, style=None):
    '''
    Begins synthesis of the buffered text.
    
    @param style: Ignored
    @type style: L{AEOutput.Style}
    '''
    try:
      self.tts.synthesize()
    except pyibmtts.ECIException:
      pass
    
  def sendString(self, text, style):
    '''
    Adds the given string to the text to be synthesized.

    @param text: String to buffer on the device
    @type text: string
    @param style: Style with which this string should be output; None means no
      style change should be applied
    @type style: integer
    '''
    if style.isDirty() or self.last_style != style:
      self.last_style = style
      self._applyStyle(style)
    try:
      self.tts.addText(text)
    except pyibmtts.ECIException:
      pass

  def isActive(self):
    '''
    Indicates whether the device is active meaning it is busy doing output or
    has text waiting to be output.

    @return: True when the speech device is synthesizing, False otherwise
    @rtype: boolean
    '''
    try:
      return self.tts.speaking()
    except pyibmtts.Exception:
      return False

  def getName(self):
    '''
    Gives the user displayable (localized) name for this output device.
    Relevant version and device status should be included.

    @return: Localized name for the device
    @rtype: string
    '''
    return 'IBM-TTS %s' % pyibmtts.getVersion()
  
  def sendIndex(self, style):
    '''
    Sends an index marker to the device driver. The driver should notify the
    device when the marker has been reached during output.

    @param style: Style indicating channel in which the marker will be appended
    @type style: L{AEOutput.Style}
    '''
    raise NotImplementedError
  
  def sendGetClosestLang(self, target):
    '''
    Maps a language tag to the closest one possible as supported by this 
    device. The result may be an exact match or only a partial match. If 
    absolutely nothing matches the requested tag, not even starting at the
    major language, None should be returned.
    
    @param target: IANA language tag, lower case
    @type target: string
    @return: IANA language tag or None
    @rtype: string
    '''
    # lower case
    target = target.lower()
    
    tags = self.langs.getTags()
    # try exact match first
    if target in tags:
      return target
    
    # split the target
    sect = target.split('-')

    # get just the language
    try:
      lang = sect[0]
    except IndexError:
      # nothing matches
      return None
    # get possible language matches
    lang_matches = [tag for tag in tags if tag.startswith(lang)]
    
    # if no language matches, abort
    if len(lang_matches) == 0:
      return None
    
    # get just the country code
    try:
      terr = sect[1]
    except IndexError:
      # just use the first language match
      return lang_matches[0]
      
    # now try matching the country code inside the language tag
    terr_matches = [tag for tag in lang_matches if tag.find(terr) > -1]
    try:
      # use the first match
      return terr_matches[0]
    except IndexError:
      # just use the first language match
      return lang_matches[0]

  def _applyStyle(self, style):
    '''
    Applies a given style to this output device. All output following this 
    call will be presented in the given style until this method is called again
    with a different style.
    
    @param style: Style to apply to future output
    @type style: L{AEOutput.Style}
    '''
    # check for language changes first
    lang = self.langs.rfcToIbm(style.Language)
    old_lang = self.tts.languageDialect
    if old_lang != lang:
      try:
        # create a new engine
        tts = self._newEngine(lang)
      except pyibmtts.ECIException:
        # ignore errors
        pass
      else:
        # switch to the new engine
        del self.tts
        self.tts = tts
    try:
      v = self.tts.getVoice(style.Voice)
      v = self.tts.setActiveVoice(v)
    except pyibmtts.ECIException:
      # just quit if we can't even get the voice to which style params should 
      # be applied
      return
    # restore voice independent parameters
    try:
      v.speed = style.Rate
    except pyibmtts.ECIException:
      pass
    try:
      v.volume = style.Volume
    except pyibmtts.ECIException:
      pass
    try:
      v.pitchBaseline = style.Pitch
    except pyibmtts.ECIException:
      pass
