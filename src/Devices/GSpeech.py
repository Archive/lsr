'''
Contains the L{GSpeech} abstract base class which provides support for all
gnome-speech devices. Device definitions using gnome-speech should derive from
this class and override the L{GSpeech.USE_THREAD} and L{GSpeech.COMMAND_CHARS}
properties if desired. The subclass should also provide createDistinctStyles
and _applyStyle methods. This module should never be directly registered as
its own speech device with L{UIRegistrar} as the GSpeech class does not fully
implement the L{AEOutput.Base.AEOutput} interface.

@var MAX_BUFFER: Maximum number of characters to buffer before forcing a 
  L{GSpeech.sendTalk} call.
@type MAX_BUFFER: integer
@var SAY_WAIT_RETRY: Time to wait between calls to say if say fails
@type SAY_WAIT_RETRY: float

@author: Larry Weiss
@author: Peter Parente
@author: Brett Clippingdale
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
import time
import AEOutput
from i18n import _

import ORBit, bonobo
# try to get the typelib
ORBit.load_typelib('GNOME_Speech')
import GNOME.Speech, GNOME__POA.Speech

# constants specific to GSpeech devices
SAY_WAIT_RETRY = 0.01
MAX_BUFFER = 80

class GSpeechStyle(AEOutput.AudioStyle):
  '''
  Style object built dynamically as we discover what capabilities the active
  gnome-speech driver supports.
  ''' 
  def init(self, device):
    '''
    Initializes the style object based on the capabilities reported by the 
    device.
    
    @param device: Reference to the gnome speech device class
    @type device: L{GSpeech}
    '''
    voices = device.getVoices()
    # compute all voice names
    names = dict(((v.name, i) for i, v in enumerate(voices)))
    # create the voice setting
    self.newEnum('Voice', 0, _('Voice'), names, _('Name of the active voice'))    
    # create a speaker with the first voice, assumes all voices have roughly
    # the same supported parameters
    sp = device.createSpeaker(voices[0])
    for param in sp.getSupportedParameters():
      # iterate over the supported parameters and stick them on the style obj
      # as setting; this may give us more than our known default set
      # of settings, but it's OK because they'll just be ignored by LSR
      cap_name = param.name.title()
      val = sp.getParameterValue(param.name)
      if param.enumerated:
        pass
      else:
        # new relative range setting
        self.newRelRange(cap_name, param.current, cap_name, param.min,
                         param.max, 0)
  
  def getGroups(self):
    '''
    Gets configurable absolute settings affecting all output from this device.
    
    @return: Group of all configurable settings
    @rtype: L{AEState.Setting.Group}
    '''
    g = self.newGroup()
    s = g.newGroup(_('Speech'))
    s.extend([name for name in ('Pitch', 'Rate', 'Volume')
              if self.hasSetting(name)])
    if self.isDefault():
      # generate a group for standard word parsing settings on the default
      # object only for now
      self._newWordGroup(g)
    return g

class GSpeech(AEOutput.Audio):
  '''
  Defines an abstract base class to send output from LSR to a speech device via
  gnome-speech U{http://cvs.gnome.org/viewcvs/gnome-speech/}. A subclass must
  override the DEVICE_IID class variable to indicate which gnome-speech server
  should be instantiated. If the specific speech server does not have named
  parameters that map automatically to the style properties in 
  L{AEOutput.Style}, L{createDistinctStyles} and L{_applyStyle} should be 
  overridden. If the default style should be something other than an empty shell
  object to be dynamically populated in createDistinctStyles, the subclass 
  should also override L{getDefaultStyle} to return a pre-populated instance of
  L{GSpeechStyle}.

  To be compliant with LSR requirements, this implements the interface defined 
  in the L{AEOutput.Audio} class.

  @ivar last_style: Last style object to be applied to output
  @type last_style: L{AEOutput.Style}
  @ivar driver: Reference to a speech engine server
  @type driver: GNOME.Speech.SynthesisDriver
  @ivar speaker: Speaker that will synthesize speech from buffered text
  @type speaker: GNOME.Speech.Speaker
  @ivar voices: List of all gnome-speech voices
  @type voices: list
  @ivar buffer: Buffer of text and styles to be sent to the device
  @type buffer: list
  @cvar DEVICE_IID: Interface identifier for the gnome-speech device. Defaults 
    to None and should be overridden in a subclass.
  @type DEVICE_IID: string
  ''' 
  DEVICE_IID = None
  STYLE = GSpeechStyle
  
  def _applyStyle(self, style):
    '''
    Applies a given style to this output device. All output following this 
    call will be presented in the given style until this method is called again
    with a different style.

    @param style: Style to apply to future output
    @type style: L{AEOutput.Style}
    '''  
    sp = self.createSpeaker(self.voices[style.Voice])
    #if self.speaker is not None:
      #self.speaker.unref()
    
    for param in sp.getSupportedParameters():
      cap_name = param.name.title()
      try:
        val = style.getSettingVal(cap_name)
        sp.setParameterValue(param.name, val)
      except KeyError:
        pass
    self.speaker = sp
    
  def createSpeaker(self, voice):
    '''
    Creates a new speaker object from the given voice.
    
    @param voice: Voice object
    @type voice: GNOME.Speech.VoiceInfo
    @return: New speaker object
    @rtype: GNOME.Speech.Speaker
    '''
    return self.driver.createSpeaker(voice)
    
  def getVoices(self):
    '''
    Gets the list of available voices
    
    @return: List of all gnome-speech voice objects
    @rtype: list
    '''
    return self.voices
    
  def init(self):
    '''
    Initializes the speech driver through gnome-speech.

    @raise AEOutput.InitError: When the device can not be initialized
    '''
    self.driver = None
    self.speaker = None
    self.voices = []
    self.buffer = []
    self.last_style = None
    
    # initialize CORBA
    ORBit.CORBA.ORB_init()
    # try to activate Festival
    self.driver = bonobo.activation.activate_from_id(self.DEVICE_IID, 0, False)
    try:
      # check if already initialized
      bInit = self.driver.isInitialized()
    except AttributeError:
      # driver is None when the engine is not available
      raise AEOutput.InitError
    # could have been initialized by someone else
    if not bInit:
      try:
        failure = not self.driver.driverInit()
      except Exception:
        failure = True
      if failure:
        # driverInit fails when engine is not in a "good state"
        raise AEOutput.InitError
    # store all voices for later reference
    self.voices = self.driver.getAllVoices()

  def _say(self, text):
    '''
    Loops until L{speaker}.say succeeds by returning a non-negative value. 

    @note: gnome-speech returns a negative value when there is a backlog of 
      events that must be processed by a GNOME.Speech.SpeechCallback before
      say will succeed
    @param text: Text to say
    @type text: string
    '''
    try:
      text = text.encode('latin-1', 'ignore')
    except (TypeError, UnicodeEncodeError):
      return
    while 1:
      sid = self.speaker.say(text)
      if sid >= 0: break
      time.sleep(SAY_WAIT_RETRY)

  def close(self):
    '''
    Stops and closes the speech device.
    '''
    if self.speaker is not None:
      self.speaker.stop()
    del self.speaker
    del self.driver

  def sendStop(self, style=None):
    '''
    Stops speech immediately.
    
    @param style: Ignored
    @type style: L{AEOutput.Style}
    '''
    if self.speaker is not None:
      self.speaker.stop()
    self.buffer = []

  def sendTalk(self, style=None):
    '''
    Begins synthesis of the buffered text.
  
    @param style: Ignored
    @type style: L{AEOutput.Style}
    '''
    stream = []
    for text, style in self.buffer:
      if style.isDirty() or self.last_style != style:
        self._applyStyle(style)
        self._say(' '.join(stream))
        self.last_style = style
        stream = []
      if text is not None:
        stream.append(text)
    if stream:
      self._say(' '.join(stream))
    self.buffer = []

  def sendString(self, text, style):
    '''
    Adds the given string to the text to be synthesized. The string is buffered
    locally in this object since gnome-speech does not support buffering in
    the speech engine driver, even if the engine does support it.

    @param text: String to buffer on the device
    @type text: string
    @param style: Style with which this string should be output; None means no
      style change should be applied
    @type style: integer
    '''
    # just buffer the text since gnome-speech will try speaking it right away
    # actually send the text in sendTalk
    self.buffer.append((text, style.copy()))

  def isActive(self):
    '''
    Indicates whether the device is active meaning it is busy doing output or
    has text waiting to be output.

    @return: True when the speech device is synthesizing, False otherwise
    @rtype: boolean
    '''
    return len(self.buffer) != 0 or self.speaker.isSpeaking()
   
  def sendIndex(self, style):
    '''
    Sends an index marker to the device driver. The driver should notify the
    device when the marker has been reached during output.
    
    @todo: PP: implement when index working

    @param style: Style indicating channel in which the marker will be appended
    @type style: L{AEOutput.Style}
    '''
    raise NotImplementedError

  def createDistinctStyles(self, num_groups, num_layers):
    '''
    Creates distinct styles following the guidelines set forth in the base
    class. Only distinguishes groups in terms of voice, not layers. Assumes
    rate, pitch, and volume are exposed under those weakly specified, but
    typically used names by gnome-speech.
    
    @param num_groups: Number of sematic groups the requestor would like to
      represent using distinct styles
    @type num_groups: integer
    @param num_layers: Number of content origins (e.g. output originating from
      a background task versus the focus) the requestor would like to represent
      using distinct styles
    @type num_layers: integer   
    @return: Styles
    @rtype: list of L{AEOutput.Style}
    '''
    styles = []
    for i in range(0, num_groups*num_layers):
      # compute a valid voice number
      v_num = i % len(self.voices)
      # get that voice from the list of all voices
      v = self.voices[v_num]
      # create a speaker with that voice
      sp = self.createSpeaker(v)
      # create a style object with relative rate, pitch, and volume
      s = GSpeechStyle(self.default_style)
      # initialize the style
      s.init(self)
      # store the current voice number
      s.Voice = v_num
      # iterate over the supported parameters and stick them on the style obj
      # as style attributes; this may give us more than our known default set
      # of styles, but it's OK because they'll just be ignored by LSR
      for param in sp.getSupportedParameters():
        cap_name = param.name.title()
        s.setSettingVal(cap_name, param.current)
      styles.append(s)
    return styles
