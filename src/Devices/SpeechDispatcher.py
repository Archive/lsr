'''
Defines a device that uses the Speech Dispatcher audio server via the speechd
package. Speech Dispatcher can be obtained from 
U{http://www.freebsoft.org/speechd}.

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
import time
import speechd
import AEOutput, AEConstants
from i18n import _

__uie__ = dict(kind='device')
  
class SDSpeechStyle(AEOutput.AudioStyle):
  '''
  Style object built dynamically as we discover what capabilities the active
  gnome-speech driver supports.
  ''' 
  def init(self, device):
    '''
    Initializes the style object based on the voices reported to be supported
    by the current device.
    
    @param device: Reference to the gnome speech device class
    @type device: L{SpeechDispatcher}
    '''
    voices = device.getVoices()
    # compute all voice names
    names = dict(((v, i) for i, v in enumerate(voices)))
    # make voices into an enum with offset in list as value
    self.newEnum('Voice', 0, _('Voice'), names, 
                   _('Name of the active voice'))    
    self.newRelRange('Pitch', 0, _('Pitch'), -100, 100, 0,
                     _('Baseline voice pitch'))
    self.newRelRange('Rate', 60, _('Rate'), -100, 100, 0,
                     _('Speech rate'))
    self.newRelPercent('Volume', 60, _('Volume'), -100, 100, 0,
                       _('Speech volume'))
  
  def getGroups(self):
    '''
    Gets configurable absolute settings affecting all output from this device.
    
    @return: Group of all configurable settings
    @rtype: L{AEState.Setting.Group}
    '''
    g = self.newGroup()
    s = g.newGroup(_('Speech'))
    s.extend([name for name in ('Pitch', 'Rate', 'Volume')
              if self.hasSetting(name)])
    if self.isDefault():
      # generate a group for standard word parsing settings on the default
      # object only for now
      self._newWordGroup(g)
    return g

class SpeechDispatcher(AEOutput.Audio):
  '''
  Speech Dispatcher audio server.
  
  @ivar last_style: Last style object to be applied to output
  @type last_style: L{AEOutput.Style}
  @ivar speaker: Speaker that will synthesize speech from buffered text
  @type speaker: speechd.Client
  @ivar buffer: Buffer of text and styles to be sent to the device
  @type buffer: list
  @ivar voices: Constants for voices supported by the current speechd server
    engine
  @type voices: list
  @cvar ALL_VOICES: Constants representing all voices supported by speechd
  @type ALL_VOICES: list of string
  ''' 
  USE_THREAD = False
  ALL_VOICES = ['MALE1', 'FEMALE1', 'CHILD_MALE', 'MALE2', 'FEMALE2', 'MALE3', 
                'FEMALE3', 'CHILD_FEMALE', 'MALE4', 'FEMALE4']
  STYLE = SDSpeechStyle
  
  def _applyStyle(self, style):
    '''
    Applies a given style to this output device. All output following this 
    call will be presented in the given style until this method is called again
    with a different style.

    @param style: Style to apply to future output
    @type style: L{AEOutput.Style}
    '''
    self.speaker.set_voice(self.voices[style.Voice])
    self.speaker.set_rate(style.Rate)
    self.speaker.set_pitch(style.Pitch)
    self.speaker.set_volume(style.Volume)

  def init(self):
    '''
    Initializes the speech driver through gnome-speech.

    @raise AEOutput.InitError: When the device can not be initialized
    '''
    try:
      self.speaker = speechd.Client(AEConstants.PROG_NAME)
    except:
      raise AEOutput.InitError
    self.last_style = None
    self.buffer = []
    self.voices = []
    
    # compute which voices are supported
    for name in self.ALL_VOICES:
      try:
        self.speaker.set_voice(name)
        self.voices.append(name)
      except AssertionError:
        pass
      
  def getVoices(self):
    '''
    Gets the list of all supported voice names.
    
    @return: All voice names currently supported
    @rtype: list
    '''
    return self.voices

  def _say(self, text):
    '''
    Encodes the text as utf-8 and speaks it. Filters out empty strings.

    @param text: Text to say
    @type text: string
    '''
    if not text:
      return
    try:
      text = text.encode('utf-8', 'replace')
      self.speaker.say(text)
    except (TypeError, UnicodeDecodeError), e:
      pass

  def close(self):
    '''
    Stops and closes the speech device.
    '''
    if self.speaker is not None:
      self.speaker.cancel()
    self.speaker.close()

  def sendStop(self, style=None):
    '''
    Stops speech immediately.
    
    @param style: Ignored
    @type style: L{AEOutput.Style}
    '''
    if self.speaker is not None:
      self.speaker.cancel()
    self.buffer = []

  def sendTalk(self, style=None):
    '''
    Begins synthesis of the buffered text.
  
    @param style: Ignored
    @type style: L{AEOutput.Style}
    '''
    stream = []
    for text, style in self.buffer:
      if style.isDirty() or style != self.last_style:
        self._applyStyle(style)
        self.last_style = style
        self._say(' '.join(stream))
        stream = []
      if text is not None:
        stream.append(text)
    if stream:
      self._say(' '.join(stream))
    self.buffer = []

  def sendString(self, text, style):
    '''
    Adds the given string to the text to be synthesized. The string is buffered
    locally in this object since gnome-speech does not support buffering in
    the speech engine driver, even if the engine does support it.

    @param text: String to buffer on the device
    @type text: string
    @param style: Style with which this string should be output; None means no
      style change should be applied
    @type style: integer
    '''
    self.buffer.append((text, style.copy()))

  def isActive(self):
    '''
    Always returns False since Speech Dispatcher currently does not report its
    activity.

    @return: True when the speech device is synthesizing, False otherwise
    @rtype: boolean
    '''
    return False

  def getName(self):
    '''
    Gives the user displayable (localized) name for this output device.

    @return: The localized name for the device
    @rtype: string
    '''
    return _('Speech Dispatcher')
   
  def sendIndex(self, style):
    '''
    Sends an index marker to the device driver. The driver should notify the
    device when the marker has been reached during output.
    
    @param style: Style indicating channel in which the marker will be appended
    @type style: L{AEOutput.Style}
    @raise NotImplementedError: Until Speech Dispatcher supports index markers
    '''
    raise NotImplementedError

  def createDistinctStyles(self, num_groups, num_layers):
    '''
    Creates distinct styles following the guidelines set forth in the base
    class. Only distinguishes groups in terms of voice, not layers.
    
    @param num_groups: Number of sematic groups the requestor would like to
      represent using distinct styles
    @type num_groups: integer
    @param num_layers: Number of content origins (e.g. output originating from
      a background task versus the focus) the requestor would like to represent
      using distinct styles
    @type num_layers: integer   
    @return: Styles
    @rtype: list of L{AEOutput.Style}
    '''
    styles = []
    for i in range(0, num_groups*num_layers):
      # compute a valid voice number
      v_num = i % len(self.voices)
      # create a style object and init
      s = SDSpeechStyle(self.default_style)
      s.init(self)
      # store the current voice number
      s.Voice = v_num
      styles.append(s)
    return styles
