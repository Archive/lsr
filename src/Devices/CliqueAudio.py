'''
Defines a class representing a multichannel audio output device. Uses pyibmtts
and pySonicEx for speech synthesis and mixing respectively.

@var UTF16_LANGS: All supported languages that accept UTF-16 encoded strings 
  instead of CP1252 strings.
@type UTF16_LANGS: tuple
@var BUFFER_SIZE: Size of the speech buffer for each channel
@type BUFFER_SIZE: integer

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
import AEOutput
import pySonicEx
import pyibmtts
import math, time, threading, Queue
from i18n import _

__uie__ = dict(kind='device')

UTF16_LANGS = (pyibmtts.MandarinChinese, pyibmtts.TaiwaneseMandarin, 
               pyibmtts.StandardJapanese, pyibmtts.StandardKorean, 
               pyibmtts.StandardCantonese)
BUFFER_SIZE = 2**16

class CliqueAudioStyle(AEOutput.AudioStyle):
  '''
  Overrides the base L{AEOutput.Style.AudioStyle} class, filling in 
  fields for supported style properties with their appropriate values.
  '''
  def init(self, device):
    self.newEnum('Voice', 1, _('Voice'),
                 {_('Adult Male 1') : 1,
                  _('Adult Female 1'): 2,
                  _('Child 1') : 3,
                  _('Adult Male 2') : 4,
                  _('Adult Male 3') : 5,
                  _('Adult Female 2') : 6,
                  _('Elderly Female 1') : 7,
                  _('Elderly Male 1') : 8},
                 _('Name of the active voice'))
    self.newRelRange('Pitch', 220, _('Pitch'), 40, 442, 0,
                  _('Baseline voice pitch in Hertz'))
    self.newRelRange('Rate', 250, _('Rate'), 176, 1297, 0,
                  _('Speech rate in words per minute'))
    self.newRelPercent('Volume', 58982, _('Volume'), 1, 65535, 0,
                    _('Speech volume as a percentage'))
    self.newNumeric('Channel', 0, _('Channel'), 0, 10, 0,
                    _('Concurrent channel number'))
    self.newNumeric('PosX', 0, _('Position X'), -10, 10, 1,
                    _('Position of the source left-to-right'))
    self.newNumeric('PosY', 0, _('Position Y'), -10, 10, 1,
                    _('Position of the source up-and-down'))
    self.newNumeric('PosZ', 0, _('Position Z'), -10, 10, 1,
                    _('Position of the source front-to-back'))
    
  def getPosition(self):
    '''
    Convenience method for getting position as a 3-tuple.
    
    @return: x,y,z coordinate
    @rtype: 3-tuple of float
    '''
    return self.PosX, self.PosY, self.PosZ

  def getGroups(self):
    '''
    Gets configurable absolute settings affecting all output from this device.
    
    @return: Group of all configurable settings
    @rtype: L{AEState.Setting.Group}
    '''
    g = self.newGroup()
    s = g.newGroup(_('Speech'))
    s.extend(['Pitch', 'Rate', 'Volume'])
    # generate a group for standard word parsing settings
    self._newWordGroup(g)
    return g

class CliqueAudio(AEOutput.Audio):
  '''
  Multichannel audio output using pySonicEx and IBM TTS.
  
  @ivar mixer: Multichannel sound mixer
  @type mixer: pySonicEx.System
  @ivar channels: Concurrent output channels
  @type channels: dictionary of integer : L{Channel}
  '''
  USE_THREAD = False
  STYLE = CliqueAudioStyle
  
  def _createNewChannel(self, i):
    '''
    Creates a new channel when a style is received with a previously unknown
    channel number.
    
    @param i: Number of the channel to create
    @type i: integer
    @return: Speech and sound channels
    @rtype: 2-tuple of L{Channel}
    '''
    ch = SpeechChannel(self.mixer), SoundChannel(self.mixer)
    self.channels[i] = ch
    return ch
 
  def init(self):
    '''
    Initializes the pySonicEx system.
    '''
    # initialize the pySonicEx mixer
    self.mixer = pySonicEx.System_Create()
    self.mixer.Output = pySonicEx.OUTPUTTYPE_ALSA
    try:
      self.mixer.Init()
    except Exception:
      raise AEOutput.InitError
    self.channels = {}

  def createDistinctStyles(self, num_groups, num_layers):
    '''
    Creates distinct styles following the guidelines set forth in the base
    class. Also distinguishes layers using spatial positioning.
    
    @param num_groups: Number of sematic groups the requestor would like to
      represent using distinct styles
    @type num_groups: integer
    @param num_layers: Number of content origins (e.g. output originating from
      a background task versus the focus) the requestor would like to represent
      using distinct styles
    @type num_layers: integer   
    @return: Styles
    @rtype: list of L{AEOutput.Style}
    '''
    styles = []
    dd = math.pi / num_layers
    # create one channel so we can talk to the engine
    sch, nch = self._createNewChannel(0)
    voice_offset = 0
    pitch_offset = 0
    for l in xrange(0, num_layers):
      # position in a semicircle in front of the user
      a = l * (-1)**l
      # avoid being really precise to prevent shifts in perceived location
      x = round(math.cos(math.pi/2+(dd*a)), 4)
      y = round(math.sin(math.pi/2+(dd*a)), 4)
      for i in xrange(0, num_groups):
        # use CliqueAudioStyle for flyweights, not CliqueAudioStyleDefault
        s = CliqueAudioStyle(self.default_style)
        s.init(self)
        # get a unique voice within this set
        v_num = (i+voice_offset % 8) + 1
        v = sch.tts.getVoice(v_num)
        s.Voice = v_num
        # set values relative to the default
        s.Pitch = v.pitchBaseline + pitch_offset
        # set to the position and channel of this layer
        s.PosX = 0.5*x
        s.PosY = 0.5*y
        s.Channel = l
        styles.append(s)
      voice_offset += 1
      pitch_offset += 20
    return styles

  def close(self):
    '''
    Loops over all L{Channel}s and closes them. When all have been closed,
    deletes the L{mixer}.
    '''
    for sch, nch in self.channels.values():
      sch.close()
      # wait until the thread terminates
      sch.join()
      nch.close()
      # wait until the thread terminates
      nch.join()
    del self.mixer

  def getName(self):
    '''
    @return: Localized name of this device
    @rtype: string
    '''
    return _('Clique Audio')

  def sendString(self, text, style):
    '''
    Adds the given string to the text to be synthesized.

    @param text: String to buffer on the device
    @type text: string
    @param style: Style with which this string should be output; None means no
      style change should be applied
    @type style: integer
    '''
    try:
      ch = self.channels[style.Channel]
    except KeyError:
      ch = self._createNewChannel(style.Channel)
    # have the channel handle the text and style
    ch[0].put(text, style.copy())
    
  def sendFilename(self, fn, style):
    '''
    Adds the given filename to a channel as an indicator of a waveform or 
    sequenced audio file that should be played. All file types supported by
    pySonicEx are acceptable as files indicated here.
    
    @param fn: Absolute filename
    @type fn: string
    @param style: Style with which this text should be output
    @type style: L{AEOutput.Style}
    '''
    try:
      ch = self.channels[style.Channel]
    except KeyError:
      ch = self._createNewChannel(style.Channel)
    s = style.copy()
    ch[0].put(None, s)
    ch[1].put(fn, s)

  def sendStop(self, style=None):
    '''
    Stops speech immediately on the channel indicated by the given style or
    on all channels if it is None.
    
    @param style: Style indicating which channel will be stopped
    @type style: L{AEOutput.Style}
    '''
    if style is None:
      for ch in self.channels.values():
        ch[0].stop()
        ch[1].stop()
    else:
      try:
        ch = self.channels[style.Channel]
      except KeyError:
        ch = self._createNewChannel(style.Channel)
      ch[0].stop()
      ch[1].stop()

  def sendTalk(self, style=None):
    '''
    Starts speech immediately on the channel indicated by the given style or
    on all channels if it is None.
    
    @param style: Style indicating which channel will be started
    @type style: L{AEOutput.Style}
    '''
    if style is None:
      for ch in self.channels.values():
        ch[0].talk()
        ch[1].talk()
    else:
      try:
        ch = self.channels[style.Channel]
      except KeyError:
        ch = self._createNewChannel(style.Channel)
      ch[0].talk()
      ch[1].talk()
  
  def isActive(self):
    '''
    Gets if any L{Channel} is active meaning it has queued text or is busy
    synthesizing.
    
    @return: Is any channel active?
    @rtype: boolean
    '''
    for ch in self.channels.values():
      if ch[0].active():
        return True
      elif ch[1].active():
        return True
    return False
  
class Msg(object):
  '''
  Struct class for storing commands and data to be processed later by a 
  a separate thread.
  
  @cvar TALK: Begin output
  @type TALK: integer
  @cvar STOP: Stop current output
  @type STOP: integer
  @cvar TEXT: Buffer some text
  @type TEXT: integer
  @cvar QUIT: Close the thread
  @type QUIT: integer
  @ivar cmd: One of the commands defined as a class variable in this class
  @type cmd: integer
  @ivar text: Text to buffer or None if no text with this command
  @type text: string
  @ivar style: Style to apply or None if no style with this command
  @type style: L{AEOutput.Style}
  '''
  TALK, STOP, TEXT, FILE, QUIT = 0,1,2,3,4
  def __init__(self, cmd, text=None, style=None):
    '''
    Store instance data. See instance variables for parameter descriptions.
    '''
    self.cmd = cmd
    self.text = text
    self.style = style
    
class Channel(threading.Thread):
  '''
  Base class for all channels.
  
  @ivar last_style: Last style object to be applied to output
  @type last_style: L{AEOutput.Style}
  @ivar queue: Queue of device L{Msg}s to process
  @type queue: L{Queue.Queue}
  @ivar mixer: Multichannel sound mixer
  @type mixer: pySonicEx.System
  @ivar ch: Audio channel on which output will be done
  @type ch: pySonicEx.Channel
  @ivar want_stop: Has a stop command been received?
  @type want_stop: boolean
  @ivar last_pos: Spatial position of the channel
  @type last_pos: 3-tuple of float
  '''
  def __init__(self, mixer):
    '''
    Initializes instance variables and the thread. Does not start the thread.
    
    @param mixer: Reference to the mixer
    @type mixer: pySonicEx.System
    '''
    # initialize the thread
    threading.Thread.__init__(self)
    self.queue = Queue.Queue()
    self.want_stop = False
    self.mixer = mixer
    self.ch = None
    self.last_pos = None
    self.last_style = None
    
  def talk(self):
    '''
    Buffers a talk command indicating synthesis and output should begin on this
    channel.
    '''
    self.queue.put(Msg(Msg.TALK))
  
  def stop(self):
    '''
    Clear all buffered messages yet to be applied. Set the L{want_stop} flag so
    the next processed message will cause a stop to be sent to the channel. 
    Also put a stop message in the old queue so the thread will awake if it is 
    waiting on the old queue.
    '''
    old = self.queue
    self.queue = Queue.Queue()
    self.want_stop = True
    msg = Msg(Msg.STOP)
    old.put_nowait(msg)
    self.queue.put_nowait(msg)
    
  def close(self):
    '''
    Set the stop flag and put a quit message in the queue.
    '''
    old = self.queue
    self.queue = Queue.Queue()
    self.want_stop = True
    msg = Msg(Msg.QUIT)
    old.put_nowait(msg)
    self.queue.put_nowait(msg)

class SoundChannel(Channel):
  '''
  Manages a single sound output stream. Multiple concurrent streams are
  possible using multiple L{Channel} objects.
  
  @ivar buffer: List of sound filenames to play
  @type buffer: list
  '''
  def __init__(self, mixer):
    '''
    Initializes this channel and starts it running as a separate thread. 
    Creates a Queue object to buffer commands until they should be applied.
    
    @param mixer: Reference to the mixer
    @type mixer: pySonicEx.System
    '''
    Channel.__init__(self, mixer)
    self.buffer = []
    self.start()

  def put(self, fn, style):
    '''
    Buffers a filename and style information for later synthesis.
    
    @param fn: Name of the file to output
    @type fn: string
    @param style: Style to apply to the text to output
    @type style: L{AEOutput.Style}
    '''
    self.queue.put(Msg(Msg.FILE, fn, style))

  def active(self):
    '''
    Gets if the channel is active meaning it either is synthesizing or has
    buffered text or commands.
    
    @return: Is the channel active?
    @rtype: boolean
    '''
    rv = self.queue.qsize() != 0 or len(self.buffer) != 0
    if self.ch is not None:
      return rv or self.ch.IsPlaying()
    else:
      return rv
    
  def run(self):
    '''
    Runs the thread loop. The loops sleeps while waiting for a L{Msg} to be
    posted to the L{queue}. When a message is posted, the thread awakes and
    handles it as a text, talk, stop, or quit command.
    '''
    while 1:
      # wait for an event signaling sounds should be output
      msg = self.queue.get()
      if msg.cmd == msg.QUIT:
        # kill this channel thread
        break
      elif msg.cmd == msg.STOP or self.want_stop:
        # clear buffered sounds
        if self.ch and self.ch.IsPlaying():
          self.ch.Stop()
        self.buffer = []
        self.want_stop = False
      elif msg.cmd == msg.FILE:
        self.buffer.append(msg)
        # only keep track of the last requested position
        self.last_pos = msg.style.getPosition()
      elif msg.cmd == msg.TALK:
        for msg in self.buffer:
          # create the sound object for the synthed PCM data
          try:
            snd = self.mixer.CreateSound(msg.text, pySonicEx.ThreeD)
          except:
            # cannot find or load sound, so abort
            continue
          # create a channel to play the sound, but leave it paused
          self.ch = self.mixer.PlaySound(snd, paused=True)#, channel=self.ch)
          # set the channel position
          if self.last_pos:
            self.ch.ThreeDPosition = self.last_pos
          # unpause the channel to start output
          self.ch.Paused = False
          # wait until the sound is done or we want to stop
          while self.ch.IsPlaying() and not self.want_stop:
            self.mixer.Update()
            time.sleep(1e-5)
          if self.want_stop:
            break
        # do an immediate stop to avoid lag
        try: self.ch.Stop()
        except: pass
        self.buffer = []
  
class SpeechChannel(Channel):
  '''
  Manages a single output stream. Multiple concurrent streams are possible 
  using multiple L{Channel} objects.
  
  @ivar tts: IBM TTS engine
  @type tts: pyibmtts.Engine
  @ivar buffer: PCM data buffer from pyibmtts
  @type buffer: pyibmtts.SynthBuffer
  @ivar mode: Mode flags for the type of sound data to be mixed
  @type mode: integer
  '''
  def __init__(self, mixer):
    '''
    Initializes this channel and starts it running as a separate thread. 
    Creates a Queue object to buffer commands until they should be applied.
    Creates an instance of the IBM TTS engine and gives it a memory buffer
    into which it should synthesize speech.
    
    @param mixer: Reference to the mixer
    @type mixer: pySonicEx.System
    '''
    # initialize the thread
    Channel.__init__(self, mixer)
    # initialize the TTS engine
    self.tts = pyibmtts.Engine()
    self.tts.realWorldUnits = True
    self.buffer = pyibmtts.SynthBuffer(self.tts)
    self.tts.setListener(self.buffer)
    self.tts.setOutputBuffer(BUFFER_SIZE)
    # settings used by pySonicEx
    self.mode = pySonicEx.OPENRAW|pySonicEx.OPENMEMORY|pySonicEx.ThreeD
    self.start()
    
  def put(self, text, style):
    '''
    Buffers text and style information for later synthesis. If the style 
    indicates a change in position, an implicit talk command is given since the
    current implementation cannot support position changes mid-stream.
    
    @param text: Text to synthesize
    @type text: string
    @param style: Style to apply to the text to output
    @type style: L{AEOutput.Style}
    '''
    self.queue.put(Msg(Msg.TEXT, text, style))

  def active(self):
    '''
    Gets if the channel is active meaning it either is synthesizing or has
    buffered text or commands.
    
    @return: Is the channel active?
    @rtype: boolean
    '''
    rv = self.queue.qsize() != 0 or self.tts.speaking()
    if self.ch is not None:
      return rv or self.ch.IsPlaying()
    else:
      return rv
    
  def run(self):
    '''
    Runs the thread loop. The loops sleeps while waiting for a L{Msg} to be
    posted to the L{queue}. When a message is posted, the thread awakes and
    handles it as a text, talk, stop, or quit command.
    '''
    while 1:
      # reset the talk and want stop flags
      talk = False
      # wait for the next message
      msg = self.queue.get()
      if msg.cmd == msg.QUIT:
        # kill this channel thread
        break
      elif msg.cmd == msg.STOP or self.want_stop:
        # clear buffered speech text and currently playing speech
        if self.tts.speaking():
          self.tts.stop()
        if self.ch and self.ch.IsPlaying():
          self.ch.Stop()
        self.want_stop = False
      elif msg.cmd == msg.TEXT:
        # apply a new style
        if msg.style and (msg.style.isDirty() or self.last_style != msg.style):
          # talk immediately if a pySonicEx style property changed
          talk = self._applyStyle(msg.style)
          self.last_style = msg.style
        # queue new text
        if msg.text is not None:
          self.tts.addText(msg.text)
      elif (msg.cmd == msg.TALK or talk == True) and self.tts.speaking():
        # synthesize speech synchrnously to the buffer
        self.tts.synthSync()
        if self.buffer.samples < 1:
          continue
        # create a struct describing the sound format
        info = pySonicEx.CreateSoundExInfo(defaultfrequency=11025,
                                           numchannels=1,
                                           format=pySonicEx.SOUND_FORMAT_PCM16,
                                           length=len(self.buffer.pcm))
        # create the sound object for the synthed PCM data
        snd = self.mixer.CreateSound(self.buffer.pcm, self.mode, info)
        # create a channel to play the sound, but leave it paused
        self.ch = self.mixer.PlaySound(snd, paused=True)#, channel=self.ch)
        # set the channel position
        if self.last_pos:
          self.ch.ThreeDPosition = self.last_pos
        # unpause the channel to start output
        self.ch.Paused = False
        # wait until the sound is done or we want to stop
        while self.ch.IsPlaying() and not self.want_stop:
          self.mixer.Update()
          time.sleep(1e-5)
        if self.want_stop and self.ch:
          # do an immediate stop to avoid lag
          try:
            self.ch.Stop()
          except:
            pass
        # reset the buffer for future synthesis
        self.buffer.reset()

  def _applyStyle(self, style):
    '''
    Appplies a new style immediately to the speech engine instance in this
    L{Channel}. Also respects mixer spatial positioning by storing a new 
    position in L{last_pos} so that it may be applied later at the proper point
    in the output.
    
    @param style: Style object to apply
    @type style: L{AEOutput.Style}
    @return: Was a new position stored?
    @rtype: boolean
    '''
    try:
      v = self.tts.getVoice(style.Voice)
    except pyibmtts.ECIException:
      pass
    # try to get the voice to activate
    v = self.tts.setActiveVoice(v)
    # restore voice independent parameters
    try:
      v.speed = style.Rate
    except pyibmtts.ECIException:
      pass
    try:
      v.volume = style.Volume
    except pyibmtts.ECIException:
      pass
    try:
      v.pitchBaseline = style.Pitch
    except pyibmtts.ECIException:
      pass
    
    if self.last_pos is None:
      # if position was never specified, store it now but indicate it has not
      # changed
      self.last_pos = style.getPosition()
      return False
    elif self.last_pos != style.getPosition():
      # if the new position differs from the previous, store it and indicate
      # a talk should be done implicitly to respect the new position
      self.last_pos = style.getPosition()
      return True
    else:
      # otherwise, just return
      return False
