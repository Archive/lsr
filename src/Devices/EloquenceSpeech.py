'''
Defines a class that subclasses L{GSpeech} to support the Eloquence speech 
engine via gnome-speech.

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
import GSpeech

__uie__ = dict(kind='device')

class EloquenceSpeech(GSpeech.GSpeech):
  '''
  Eloquence via gnome-speech.
  '''
  DEVICE_IID = 'OAFIID:GNOME_Speech_SynthesisDriver_Eloquence:proto0.2'
  USE_THREAD = False
