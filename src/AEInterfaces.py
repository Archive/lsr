'''
Defines standard interfaces for inspecting, manipulating, and navigating 
accessibles and handling their events.

@author: Peter Parente
@author: Pete Brunet
@author: Larry Weiss
@author: Brett Clippingdale
@author: Eirikur Hallgrimsson
@author: Scott Haeger
@organization: IBM Corporation
@copyright: Copyright (c) 2005, 2007 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made
available under the terms of the BSD license which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''

from AccAdapt import Interface

def implements(thing, interface):
  '''
  Checks if the given thing implements the given interface. The thing may be
  an instance or class. The thing is checked using issubclass first, followed
  by isinstance next, and then by brute force method comparison last.
  
  @param thing: Class or object to be tested for an interface
  @type thing: class or object
  @param interface: Class representing an interface to be implemented
  @type interface: class
  @return: Does the thing implement the given interface?
  @rtype: boolean
  '''
  try:
    if issubclass(thing, interface): return True
  except:
    pass
  try:
    if isinstance(thing, interface): return True
  except:
    pass
  return False

class IAccessibleNav(Interface):
  '''
  Provides methods for navigating across accessible objects.
  '''
  def getNextAcc():
    '''
    Gets the next accessible relative to the one providing this interface.
    
    @return: Point of regard to the next accessible
    @rtype: L{POR}
    @raise IndexError: When there is no next accessible
    @raise LookupError: When lookup for the next accessible fails even though
      it may exist
    '''
    pass
    
  def getPrevAcc():
    '''
    Gets the previous accessible relative to the one providing this interface.
    
    @return: Point of regard to the previous accessible
    @rtype: L{POR}
    @raise IndexError: When there is no previous accessible
    @raise LookupError: When lookup for the previous accessible fails even 
      though it may exist
    '''
    pass
    
  def getParentAcc():
    '''
    Gets the parent accessible relative to the one providing this interface.
    
    @return: Point of regard to the parent accessible
    @rtype: L{POR}
    @raise LookupError: When lookup for the parent accessible fails because it
      does not exist
    '''
    pass
  
  def getFirstAccChild():
    '''
    Gets the first accessible child relative to the subject accessible.
    
    @return: Point of regard to the first child accessible
    @rtype: L{POR}
    @raise LookupError: When lookup for child fails because it does not exist
    '''
    pass

  def getLastAccChild():
    '''
    Gets the last accessible child relative to the subject accessible.
    
    @return: Point of regard to the last child accessible
    @rtype: L{POR}
    @raise LookupError: When lookup for child fails because it does not exist
    '''
    pass
  
  def getChildAcc(index):
    '''
    Gets the child accessible at the given index relative to the one providing
    this interface.
    
    @param index: Index of the child to retrieve
    @type index: integer
    @return: Point of regard to the child accessible
    @rtype: L{POR}
    @raise IndexError: When there is no child at the given index
    @raise LookupError: When the lookup for the child fails even though it may
      exist
    '''
    pass
  
  def findAncestorAcc(predicate):
    '''
    Searches all ancestors for one matching the given predicate.
    
    @param predicate: Search predicate
    @type predicate: callable
    @return: Point of regard to the target or None if not found
    @rtype: L{POR}
    @raise LookupError: When the lookup fails during the search
    '''
    pass
  
  def findDescendantAcc(predicate, depth_first):
    '''
    Searches all descendants for one matching the given predicate.

    @param predicate: Search predicate
    @type predicate: callable
    @param depth_first: Perform the search in depth-first (True) or breadth 
      first (False) order?
    @type depth_first: boolean
    @return: Point of regard to the target or None if not found
    @rtype: L{POR}
    @raise LookupError: When the lookup fails during the search
    '''
    pass
    
class IItemNav(Interface):
  '''
  Provides methods for navigating items within accessible objects. The 
  definition of an item is left up to the object implementing this interface.
  ''' 
  def getNextItem(only_visible=True): 
    '''    
    Gets the next Item relative to the one indicated by the L{POR}
    providing this interface.
    
    @param only_visible: True when Item in the returned L{POR} must be visible
    @type only_visible: boolean
    @return: Point of regard to the next visible item in the same accessible
    @rtype: L{POR}
    @raise IndexError: When there is no next visible item
    @raise LookupError: When lookup for the next item fails even though it may 
      exist
    '''
    pass
    
  def getPrevItem(only_visible=True):
    '''
    Gets the previous Item relative to the one indicated by the L{POR} providing
    this interface.
    
    @param only_visible: True when Item in the returned L{POR} must be visible
    @type only_visible: boolean
    @return: Point of regard to the previous visible item in the same accessible
    @rtype: L{POR}
    @raise IndexError: When there is no previous visible item
    @raise LookupError: When lookup for the previous item fails even though it
      may exist
    '''
    pass
  
  def getFirstItem(only_visible=True):
    '''
    Gets the first Item relative to the one indicated by the L{POR} 
    providing this interface.
    
    @param only_visible: True when Item in the returned L{POR} must be visible
    @type only_visible: boolean
    @return: Point of regard to the first visible item in the same accessible
    @rtype: L{POR}
    @raise IndexError: When there is no last visible item
    @raise LookupError: When lookup for the last item fails even though it may 
      exist
    '''
    pass
  
  def getLastItem(only_visible=True):
    '''
    Gets the last Item relative to the one indicated by the L{POR} 
    providing this interface.
    
    @param only_visible: True when Item in the returned L{POR} must be visible
    @type only_visible: boolean
    @return: Point of regard to the last visible item in the same accessible
    @rtype: L{POR}
    @raise LookupError: When lookup for the last item fails because it does not
      exist
    '''
    pass
  
  def getAccAsItem(por):
    '''
    Gets a L{POR} to the child as an item of the subject rather than as a child
    of the subject.
    
    @param por: Point of regard to a child of the subject
    @type por: L{POR}
    @return: Point of regard to an item of the subject
    @rtype: L{POR}
    @raise LookupError: When lookup for the offset fails
    '''
    pass

class IAccessibleInfo(Interface):
  '''
  Provides methods for accessing basic information about accessible objects.
  '''
  def getAccSelection():
    '''  
    Gets a list of L{POR}s referring to the selected objects in the subject 
    accessible.
    
    @return: Points of regard to selected accessibles
    @rtype: list of L{POR}s
    @raise LookupError: When the subject accessible is dead
    '''
    pass
  
  def getAccChildCount():
    '''
    Gets the number of child accessibles for the object implementing this 
    interface.
    
    @return: Number of accessible children
    @rtype: integer
    @raise LookupError: When the accessible object is dead
    '''
    pass
  
  def getAccAppName():
    '''
    Gets the name of the application application to which the subject accessible 
    belongs.
    
    @return: Name of the subject's application
    @rtype: string
    @raise LookupError: When the accessible or any parent accessible up to the
      application is dead
    '''
    pass
  
  def getAccAppID():
    '''
    Gets a unique ID identifying the application to which the subject accessible 
    belongs.
    
    @return: Unique identifier for the subject's application
    @rtype: hashable
    @raise LookupError: When the accessible or any parent accessible up to the
      application is dead
    '''
    pass
   
  def getAccRoleName():
    '''
    Gets the localized unicode accessible role name for the object implementing
    this interface.
    
    @return: Unicode role name
    @rtype: string
    @raise LookupError: When the accessible object is dead
    '''
    pass
  
  def getAccName():
    '''    
    Gets the accessible name for the object implementing this interface.
    
    @return: Name of the accessible
    @rtype: string
    @raise LookupError: When the accessible object is dead
    @raise IndexError: When the item offset is outside the bounds of the 
      number of children of the subject accessible
    '''
    pass

  def getAccDescription():
    '''
    Gets the accessible description for the object implementing this interface.
    
    @return: Accessible description of requested object
    @rtype: string
    @raise LookupError: When the subject accessible is dead
    @raise IndexError: When the item offset is outside the bounds of the 
      number of children of the subject accessible
    '''
    pass
  
  def getAccStates():
    '''
    Gets a list of states for the object implementing this interface.
    
    @return: Names of all states
    @rtype: list of string
    @raise LookupError: When the subject accessible is dead
    @raise IndexError: When the item offset is outside the bounds of the 
      number of children of the subject accessible
    '''
    pass
  
  def getAccItemText():
    '''
    Gets the accessible text for the object implementing this interface.
    The item text will be the accessible text if that is available, otherwise 
    it will be the accessible name.
    
    @return: The accessible text or name
    @rtype: string
    @raise LookupError: When the subject accessible is dead
    @raise IndexError: When the item offset is invalid
    '''
    pass
  
  def getAccRelations(kind):
    '''
    Gets a list of accessibles related to the subject accessible in the manner
    given by kind.
    
    @param kind: Name specifying the kind of relations to retrieve
    @type kind: string
    @return: List of L{POR}s related to subject accessible in the manner 
      given by kind
    @rtype: list of L{POR}
    @raise LookupError: When the accessible object is dead
    '''
    pass
  
  def getAccTextAttr(name):
    '''
    Gets the value of a given attribute name.
    
    @param name: Name of the attribute
    @type name: string
    @return: Value of the accessible attribute
    @rtype: string
    @raise LookupError: When the accessible object is dead
    '''
    pass
  
  def getAccAllTextAttrs():
    '''
    Gets all of the name:value attribute pairs of an accessible.
    
    @return: Name:value pairs in the accessible attributes
    @rtype: dictionary
    @raise LookupError: When the accessible object is dead
    '''
    pass
  
  def getAccLevel():
    '''
    Gets the logical depth of an accessible within a tree or hierarchy.
    
    @return: Level of an accessible with zero being the root
    @rtype: integer
    @raise LookupError: When the accessible object is dead
    '''
    pass

  def getAccIndex():
    '''
    Gets the index an accessible within some 1D ordered collection.
    
    @return: Index of an accessible with zero being the first
    @rtype: integer
    @raise LookupError: When the accessible object is dead
    '''
    pass  
  
  def getAccRow():
    '''
    Gets the row index of an accessible within some 2D ordered collection.
    
    @return: Row of an accessible with zero being the first
    @rtype: integer
    @raise LookupError: When the accessible object is dead
    '''
    pass
  
  def getAccColumn():
    '''
    Gets the column index of an accessible within some 2D ordered collection.
  
    @return: Column of an accessible with zero being the first
    @rtype: integer
    @raise LookupError: When the accessible object is dead
    '''
    pass
  
  def getAccRowColIndex(row, col):
    '''
    Gets the 1D index of the cell at the given 2D row and column.
    
    @param row: Row index
    @type row: integer
    @param col: Column index
    @type col: integer
    @return: 1D index into the table
    @rtype: integer
    @raise IndexError: When the row/column offsets are invalid
    @raise LookupError: When the accessible object is dead
    '''
    pass
  
  def getAccColumnHeader():
    ''' 
    Gets the description of the column in which the accessible at the L{POR}
    falls, typically the column header.
    
    @return: Description of the column
    @rtype: string
    @raise LookupError: When the accessible object is dead
    '''
    pass
  
  def getAccRowHeader():
    ''' 
    Gets the description of the row in which the accessible at the L{POR}
    falls, typically the row header.
    
    @return: Description of the row
    @rtype: string
    @raise LookupError: When the accessible object is dead
    '''
    pass

  def getAccAttrs():
    '''
    Gets a list of name:value attribute pairs for the subject.
    
    @return: Name:value pairs of the accessible object attributes
    @rtype: dictionary
    @raise LookupError: When the accessible object is dead
    '''
    pass

  def getAccFloatValueExtents():
    '''
    Gets the minimum, maximum, and step for the floating point value exposed
    by the subject.

    @return: Minimum, maximum, and step values
    @rtype: 3-tuple of float
    @raise LookupError: When the accessible object is dead
    '''
    pass

  def getAccVisualExtents():
    '''
    Gets the visual width and height of the subject.

    @return: Width and height extents
    @rtype: 2-tuple of integer
    @raise LookupError: When the accessible object is dead
    '''
    pass

  def getAccVisualPoint():
    '''
    Gets the focal point within the subject, where the focal point is defined
    as the center of the most interesting point of regard. For a simple
    control, the focal point is the center of the control. For a control with
    items, the focal point is the center of an item.

    @return: x,y coordinates
    @rtype: 2-tuple of integer
    @raise LookupError: When the accessible object is dead
    '''
    pass
  
  def getAccPosition(self):
    '''
    Gets the position of the accessible object, usually upper-left corner.
    
    @return: x,y coordinate
    @rtype: 2-tuple integer
    @raise NotImplementedError: When this accessible does not support the
      Component interface
    '''
    pass
  
  def getAccFloatValue():
    '''
    Gets the current floating point value of the subject
    
    @return: Current value
    @rtype: float
    @raise LookupError: When the accessible object is dead
    '''
    pass
  
  def getAccDefTextAttrs():
    '''
    Gets all of the default text attributes assigned to the subject.
    
    @return: Name:value pairs of the default text attributes
    @rtype: dictionary
    @raise LookupError: When the accessible object is dead
    '''
    pass
  
  def getAccTextSelection(n=None):
    '''
    Gets a list of all text selections in the subject if n is None, or the nth
    selection if it is specified.

    @param n: Index of selection to be returned, or None to indicate all
    @type n: integer
    @return: List of selected text runs
    @rtype: list of string
    @raise LookupError: When the accessible object is dead
    '''
    pass

  def getAccText(self, por):
    '''
    Gets the text starting at the subject and ending at the given L{POR}.

    @param por: Point of regard to the end terminus of the get operation
    @type por: L{POR}
    @raise LookupError: When the accessible object is dead   
    '''
    pass

  def getAccTableExtents():
    '''
    Returns the number of rows and columns in a table.

    @return: Row and column count
    @rtype: 2-tuple of integer
    '''
    pass

  def getAccAppLocale():
    '''
    Gets the POSIX locale of an application as a string.
    
    @return: POSIX locale of the application
    @rtype: string
    '''
    pass
  
  def isAccEmbedded():
    '''
    Gets whether or not the accessible considers itself embedded in an 
    ancestor's content.
    
    @return: Does the accessible consider itself embedded?
    @rtype: boolean
    @raise LookupError: When the accessible object is dead
    '''
    pass
  
  def getAccURI():
    '''
    Obtain a resource locator ('URI') string which can be used to access the 
    content to which this link "points" or is connected.
    
    @return: URI string
    @rtype: string
    @raise LookupError: When the accessible object is dead
    '''
    pass
  
  def getAccAnchorCount():
    ''' 
    Obtain he number of separate anchors associated with this accessible.
    
    @return: Number of anchors
    @rtype: integer
    @raise PORError: When the L{POR} is invalid
    '''
    pass
  
  def isAccTrivial():
    '''
    Gets whether or not the accessible considers itself to be trivial.
    
    @return: Does the accessible consider itself trivial?
    @rtype: boolean
    @raise LookupError: When the accessible object is dead
    '''
    pass

  def isAccVisible():
    '''
    Gets whether or not the accessible considers itself visible.
    
    @return: Does the accessible consider itself visible?
    @rtype: boolean
    @raise LookupError: When the accessible object is dead
    '''
    pass
  
  def getAccRole():
    '''
    Gets the unlocalized string role identifier for the object implementing 
    this interface.
    
    @return: Role name
    @rtype: string
    @raise LookupError: When the accessible object is dead
    '''
    pass

  def hasAccRole(role):
    '''
    Gets if the subject has the given role.
    
    @param role: Name of the role
    @type role: string
    @return: Does the accessible have the given role?
    @rtype: boolean
    @raise LookupError: When the accessible object is dead
    '''
    pass
  
  def hasOneAccRole(*roles):
    '''
    Gets if the subject has any one of the given roles.
    
    @param roles: Names of the roles
    @type roles: list of string
    @return: Does the accessible have any one of the given roles?
    @rtype: boolean
    '''
    pass
  
  def hasAccState(state):
    '''
    Gets if the subject has the given state.
    
    @param state: Name of the state
    @type state: string
    @return: Does the accessible have the given state?
    @rtype: boolean
    @raise LookupError: When the accessible object is dead
    '''
    pass
  
  def hasAccOneState(*states):
    '''
    Gets if the subject has at least one of the given states.
    
    @param states: State names
    @type states: string
    @return: Does the accessible have at least one of the given states?
    @rtype: boolean
    @raise LookupError: When the accessible object is dead
    '''
    pass
  
  def hasAccAllStates(*states):
    '''
    Gets if the subject has all of the given states.
    
    @param states: State names
    @type states: string
    @return: Does the accessible have all of the given states?
    @rtype: boolean
    @raise LookupError: When the accessible object is dead
    '''
    pass
  
  def allowsAccEmbeds():
    '''
    Gets whether or not the subject accessible allows embedded objects in its
    text.
    
    @return: Does the subject allow embedded objects?
    @rtype: boolean
    @raise LookupError: When the accessible object is dead
    '''
    pass
  
  def isAccTopLevelWindow():
    '''
    Gets whether or not the subject accessible is a top-level container in the
    accessible hierarchy.
    
    @return: Is the subject a top-level container or not?
    @rtype: boolean
    @raise LookupError: When the accessible object is dead
    '''
    pass
  
  def getAccActionNames():
    '''
    Gets the list of all action names defined by the subject.
    
    @return: List of all action names
    @rtype: list of string
    @raise LookupError: When the accessible object is dead
    '''
    pass
  
  def getAccActionDescs():
    '''
    Gets the list of all action descriptions defined by the subject.
    
    @return: List of all action descriptions
    @rtype: list of string
    @raise LookupError: When the accessible object is dead
    '''
    pass

  def getAccActionCount():
    '''
    Gets the number of available actions on the subject.
    
    @return: Number of actions available
    @rtype: integer
    @raise LookupError: When the accessible object is dead
    '''
    pass

  def getAccActionKeys():
    '''
    Gets the key bindings associated with all available actions.
    
    @return: List of tuples, each containing one or more string keysym names
      indicating the keys to press in sequence to perform the action
    @rtype: list of tuple of string
    @raise LookupError: When the accessible object is dead
    '''
    pass
  
  def getAccTextSelectionCount(self):
    '''
    Gets the number of discontiguous selected text ranges.
    
    @return: Number of selections
    @rtype: integer
    @raise LookupError: When the accessible object is dead
    '''
    pass
  
  def getAccCaret(self):
    '''
    Gets the current offset of the caret in this text object as a L{POR}.
    
    @return: Point of regard of the text caret in this object
    @rtype: L{POR}
    @raise LookupError: When the accessible object is dead
    '''
    pass
  
  def getAccTextCharCount(self):
    '''
    Gets the number of characters in a text body.
    
    @return: Total number of characters
    @rtype: integer
    @raise LookupError: When the accessible object is dead
    '''
    pass

class IAccessibleAction(Interface):
  '''
  Provides methods for manipulating accessible objects.
  '''
  def setAccFocus():
    '''
    Gives the subject accessible the input focus. 
    
    @return: Did accessible accept (True) or refuse (False) the focus change?
    @rtype: boolean
    @raise LookupError: When the accessible object is dead or does not support
      setting the focus
    '''
    pass
  
  def selectAcc(all=False):
    '''
    Selects the subject accessible or one of its children or items if all is 
    False. Selects all children or items if all is True.
    
    @param all: Select all items?
    @type all: boolean
    @return: Did accessible accept (True) or refuse (False) the selection?
    @rtype: boolean
    @raise LookupError: When the accessible object is dead
    '''
    pass
  
  def deselectAcc(all=False):
    '''
    Deselects the subject accessible if all is False. Deselects all children if
    all is True.
    
    @param all: Deselect all children?
    @type all: boolean
    @return: Did accessible accept (True) or refuse (False) the deselection?
    @rtype: boolean
    @raise LookupError: When the accessible object is dead
    '''
    pass
  
  def doAccAction(index):
    '''
    Executes the action given by the index in the list of all actions.
    
    @param index: Index of the action to execute
    @type index: integer
    @return: Did the action execute (True) or not (False)?
    @rtype: boolean
    @raise LookupError: When the accessible object is dead
    '''
    pass
  
  def setAccCaret():
    '''
    Moves the caret to the location given by the subject.
    
    @return: Indicator of whether the caret was moved successfully
    @rtype: boolean
    @raise LookupError: When the accessible object is dead
    '''
    pass
  
  def setAccText(text):
    '''
    Replace contents of text area with the given text.

    @param text: Text to set as the content of the text area
    @type text: string
    @return: Indicator of whether the text was set successfully or not
    @rtype: boolean
    @raise LookupError: When the accessible object is dead   
    '''
    pass

  def insertAccText(text, attrs):
    '''
    Inserts text at the location given by the subject.

    @param text: Text to insert at the given position
    @type text: string
    @param attrs: Dictionary of string name/value pairs of text attributes to
      set on the inserted text
    @type attrs: dictionary
    @return: Indicator of whether the text was inserted successfully or not
    @rtype: boolean
    @raise LookupError: When the accessible object is dead
    '''
    pass

  def setAccTextAttrs(por, attrs):
    '''
    Sets the accessible text attributes from the subject to the given L{POR}. 

    @param por: Point of regard to the end of the text run
    @type por: L{POR}
    @param attrs: Dictionary of string name/value pairs of text attributes to
      set on the text range
    @type attrs: dictionary
    @return: Indicator of whether the text attributes were set successfully or 
      not
    @rtype: boolean
    @raise LookupError: When the accessible object is dead
    '''
    pass

  def copyAccText(por):
    '''    
    Copy the text starting at the subject and ending at the given L{POR}.

    @param por: Point of regard to the end terminus of the copy operation
    @type por: L{POR}
    @return: Indicator of whether the text was copied successfully or not
    @rtype: boolean
    @raise LookupError: When the accessible object is dead   
    '''
    pass

  def cutAccText(por):
    '''    
    Cut the text starting at the subject and ending at the given L{POR}.

    @param por: Point of regard to the end terminus of the copy operation
    @type por: L{POR}
    @return: Indicator of whether the text was copied successfully or not
    @rtype: boolean
    @raise LookupError: When the accessible object is dead   
    '''
    pass

  def deleteAccText(por):
    ''''    
    Delete the text starting at the subject and ending at the given L{POR}.

    @param por: Point of regard to the end terminus of the copy operation
    @type por: L{POR}
    @return: Indicator of whether the text was copied successfully or not
    @rtype: boolean
    @raise LookupError: When the accessible object is dead   
    '''
    pass

  def pasteAccText():
    '''    
    Paste the text starting at the subject.

    @return: Indicator of whether the text was copied successfully or not
    @rtype: boolean
    @raise LookupError: When the accessible object is dead   
    '''
    pass
  
  def selectAccText(por, n=None):
    '''
    Adds a new selection if n is None or sets the nth selection otherwise
    starting at the subject and ending at the given L{POR}.
    
    @param por: Point of regard to the end terminus of the select operation
    @type por: L{POR}
    @return: Indicator of whether the text was selected successfully or not
    @rtype: boolean
    @raise LookupError: When the accessible object is dead   
    '''
    pass
  
  def deselectAccText(por, n=None):
    '''
    Removes a all text selections if n is None or removes the nth text 
    selection otherwise. When n is None, returns True if at least one selection
    was removed.
    
    @param por: Point of regard to the end terminus of the select operation
    @type por: L{POR}
    @return: Indicator of whether the text was deselected successfully or not
    @rtype: boolean
    @raise LookupError: When the accessible object is dead
    '''
    pass
  
  def doMouseAction(event):
    '''
    Performs a mouse event on the center of a given point of regard.
    
    @param event: Mouse event to perform
    @type event: integer of type EVENT_SYNTHMOUSE*
    @return: Indicator of whether the event was performed (True) or not (False)
    @raise LookupError: When the accessible object is dead
    '''
    pass
  
class IEventHandler(Interface):
  '''
  Provides methods for handling accessible events.
  '''
  def getRawEvents(self, kind):
    '''
    Gets a list of raw event names that map to the given kind of L{AEEvent}.
    
    @param kind: Indicates the type of L{AEEvent}
    @type kind: L{AEEvent} class
    @return: List of raw event names
    @rtype: list of string
    @raise KeyError: When no mapping exists for the given event
    '''
    pass
  
  def getAEViewEvents(self, event, collector, vm):
    '''
    Determines if the active view has changed and what events need to be
    fired in response.
    
    @param event: Raw event
    @type event: L{pyLinAcc.Event.Event}
    @param collector: Callable object that collects L{AEEvent}s to process. 
      Accepts N parameters of type L{AEEvent}.
    @type collector: callable
    @param vm: Reference to the view manager that has information about the 
      current view and needs to store a reference to a new view
    @type vm: L{ViewManager}
    '''
    pass
  
  def getAEEvents(event, collector):
    '''
    Collects the L{AEEvent}s that need to be posted for this event.
    
    @param event: Raw event
    @type event: object
    @param collector: Callable object that collects L{AEEvent}s to process. 
      Accepts N parameters of type L{AEEvent}.
    @type collector: callable
    '''
    pass
  
class IPORFactory(Interface):
  '''
  Provides methods for building L{POR}s.
  '''
  def create():
    '''
    Returns a complete L{POR} built based on the properties of the subject.
    
    @return: Point of regard
    @rtype: L{POR}
    '''
    pass
 
