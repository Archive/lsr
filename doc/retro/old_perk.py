::

  class Foobar(Perk.Perk):
    def init(self):
      self.registerTask(HandleFocusChange('read test focus'))
      self.registerTask(ReadPerkName('read perk name'))
      kbd = self.getInputDevice(None, 'keyboard')
      self.addInputModifiers(kbd, kbd.AEK_CAPS_LOCK)
      self.registerCommand(kbd, 'read perk name', False, 
                         [kbd.AEK_CAPS_LOCK, kbd.AEK_A])
                         
    def getDescription(self):
      return _('Does little of interest.')

  class ReadPerkName(Task.InputTask):
    def execute(self, **kwargs):
      self.stopNow()
      self.sayInfo(text='%s handling an input gesture' % self.perk.getName())

  class HandleFocusChange(Task.FocusTask):
    def executeGained(self, por, **kwargs):
      # stop current output
      self.mayStop()
      # don't stop this output with an immediate next event
      self.inhibitMayStop()
      self.sayInfo(text='%s handling a focus change' % self.perk.getName())
