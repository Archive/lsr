::

  class Foobar(Perk.Perk):
    def init(self):
      # Tasks register under names only
      self.registerTask('read test focus', self.onInput)
      self.registerTask('read perk name', self.onFocusGained)

      # bind a Task to an input gesture 
      kbd = self.getInputDevice(None, 'keyboard')
      self.addInputModifiers(kbd, kbd.AEK_CAPS_LOCK)
      self.bindToGesture('read perk name', kbd, False, [kbd.AEK_CAPS_LOCK,
                                                        kbd.AEK_A])

      # bind a task to an event
      self.bindToEvent('read test focus', AEEvent.FOCUS_GAINED)
                         
    def getDescription(self):
      return _('Does little of interest.')

    def onInput(self, **kwargs):
      self.stopNow()
      self.sayInfo(text='%s handling an input gesture' % self.getName())

    def onFocusGained(self, por, **kwargs):
      # stop current output
      self.mayStop()
      # don't stop this output with an immediate next event
      self.inhibitMayStop()
      self.sayInfo(text='%s handling a focus change' % self.getName())
