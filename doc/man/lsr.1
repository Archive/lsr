./" 
./"  Copyright (c) 2006 IBM Corporation
./" 
./"  All rights reserved. This program and the accompanying materials
./"  are made available under the terms of the BSD License
./"  which accompanies this distribution, and is available at
./"  http://www.opensource.org/licenses/bsd-license.php
./" 
./"  Linux Screen Reader (LSR)
./" 
./"  file: lsr.1.man
./" 
./" 
.TH lsr 1 "2006" Linux "User Manuals"
.SH NAME
lsr \- IBM Linux Screen Reader
.SH SYNOPSIS
.B lsr [--help] | [--profile =
.I PROFILE_NAME
.B ] | [
.I options...
.B ]
.SH AVAILABILITY
LSR is able to run with GNOME 2.14 and Python 2.4.
.SH DESCRIPTION
Linux Screen Reader (LSR) is an extensible assistive technology for people with
disabilities. The design philosophy behind LSR is to provide a core platform 
that enables the development of LSR extensions for improving desktop 
application accessibility and usability and shields extension developers from 
the intricacies of the desktop accessibility architecture.

The primary use of the LSR platform is to give people with visual impairments 
access to the GNOME desktop and its business applications (e.g. Firefox, 
OpenOffice, Eclipse) using speech, Braille, and screen magnification. The 
extensions packaged with the LSR core are intended to meet this end. However, 
LSR's rich support for extensions can be used for a variety of other purposes
such as supporting novel input and output devices, improving accessibility for
users with other disabilities, enabling multi-modal access to the GNOME 
desktop, and so forth.
.SH USAGE
The basic function of the lsr script is to start the Linux Screen Reader.
Starting LSR under the default user profile is as simple as typing:

  lsr
  
The user profile, by default, selects the best available output device and
loads all screen reader scripts. Other built-in profiles include:

.TP
developer
Equivalent to the user profile, but also loads all debugging monitors and the 
developer script.
.TP
login
Equivalent to the user profile, but set to load a special script to assist at 
the login screen.
.TP
monitors
Loads the debugging monitors only.
.TP  
autotest
Loads the output logging monitor for automated testing of LSR.
  
.PP
The lsr script also manages User Interface Elements (UIEs) for LSR, extensions 
for LSR that add new devices and dialogs or change the way applications are 
presented to the user. The four kinds of UIEs are:

.TP
Perk
A script defining a part of the LSR user interface
.TP
Device
A module defining how to communicate with a piece of hardware or software in 
order to use it for input and output in LSR
.TP
Chooser
A dialog for interacting with the user in more complex ways than is possible 
with typical LSR keyboard combinations
.TP
Monitor
A dialog for monitoring events within the LSR core, useful for debugging and 
testing
           
.PP
UIEs can be installed, uninstalled, associated with a profile, and 
disassociated from a profile using the lsr script. When a UIE is installed,
LSR is aware of it and may load it at runtime. When a UIE is uninstalled, LSR
is unaware of it. When a UIE is associated with a profile, LSR will load it
automatically when running under that profile. When a UIE is disassociated from
a profile, LSR will no longer load it automatically when running under that
profile.

Associated devices and monitors load automatically at startup. Associated Perks
either load automatically when any application is started or when a particular
application is started. Choosers and some Perks are not associated with a 
profile, but rather loaded manually by the user or another Perk at runtime.
.SH OPTIONS
.TP
\-h, \--help
Show basic help and exit.
.TP
\-p PROFILE, --profile=PROFILE
Name of the profile to use when running LSR, associating UIEs, or 
disassociating UIEs. Repeat to specify multiple profiles for some operations.
.TP
\-s, --show
Show all installed UIEs. Show UIEs associated with a particular profile when 
\--profile is used.
.TP
\-g PATH KIND, --generate=PATH KIND
Generate a template for a new UIE of the given kind (input, output, monitor,
chooser, perk) in the given path. Install it immediately. Associate it with 
all built-in profiles by default. Specify \--profile, \--tier, and \--all-tiers
as optional arguments.
.TP
\-i PATH, --install PATH
Install a new UIE located at the given path.
.TP
\-u NAME, --uninstall NAME
Uninstall a UIE given its name.
.TP
\-a NAME, --associate NAME
Associate a UIE with all built-in profiles by default. Specify \--profile
to name a particular profile instead. Specify \--tier and 
\--all-tiers to override the defaults coded in a Perk. Specify \--index to
change the load order for the UIE.
.TP
\-d NAME, --disassociate NAME
Disassociate a UIE from all built-in profiles by default. Specify \--profile 
to name a particular profile instead. Specify \--tier and \--all-tiers to 
override defaults coded in a Perk.
.TP
\-c NAME, --create-profile=NAME
Create a new, empty profile.
.TP
\-r NAME, --remove-profile=NAME
Remove an existing profile.
.TP
\--dupe OLD NEW, --duplicate-profile=OLD NEW
Duplicate an existing user profile under a new name. The old and new profiles
are not linked in any way after duplication.
.TP
\-y TEXT, --say=TEXT
Speak a UTF-8 encoded string using the default output device.
.TP
\--index=INDEX
State the load order index for the UIE. Defaults to the last UIE to load for
a given kind. Set to zero to load a UIE before other UIEs of the same kind. 
Note that this definition is slightly muddled for Perks. Perks that apply to
\--all-tiers always load before Perks that apply to a particular \--tier. 
Therefore, an \--all-tiers Perk that loads last still loads before a one 
\--tier Perk that loads first.

all-tier0, all-tier1, ..., all-tierN, tier0, tier1, ..., tierN

.TP
\--tier=NAME
State the name of an accessible application which will cause a Perk to load. 
Optional for \--associate and \--disassociate to override the value coded in 
the Perk itself.
.TP
\--app=NAME
Alias for --tier=NAME.
.TP
\--all-tiers
State the process which will cause a Perk to load. Optional for \--associate 
and \--disassociate to override the value coded in the Perk itself.
\--all-apps
Alias for --all-tiers.
.TP
\--global
State the a UIE should be installed into uninstalled from the system-wide 
repository available to all users. Use of \--install and \--uninstall without
this flag defaults to acting on the repository for the current user. All UIEs 
packaged with LSR are automatically installed into the system-wide repository 
at install time.
.TP
\--init-profiles
Initializes the built-in profiles with the appropriate UIEs. Invoked 
automatically when any one of the built-in profiles is missing. When run 
manually, resets built-in profiles to include their default UIEs, but does not 
disassociate any third-party UIEs.
.TP
\--init-global
Initializes the global repository with all UIEs packaged with LSR. Invoked
automatically when installing or upgrading LSR.
.TP
\-l, --log-level=LOG_LEVEL
Minimum level of messages to log: debug, print, info, warning, error, 
or critical. Less severe levels include messages logged at more severe levels.
.TP
\--log-channel=LOG_CHANNELS
Channel of messages to log: Perk, Tier, Output, Input, ... Specify 
multiple channels to log by reusing this parameter.
.TP
\--log-file=LOG_FILE
Output stream for the log. Creates a file with the given name to hold 
messages if specified. Defaults to standard error (stderr) if not specified.
.TP
\--log-conf=LOG_CONF
Configuration file for the Python standard library logging module. See
http://docs.python.org/lib/logging-config-fileformat.html and
http://antonym.org/node/76 for help in constructing files in this format.
Settings in the configuration file override any specified on the command line.
.TP
\--no-intro
Supress the LSR welcome message sent to the default output device on startup.
Useful at the login screen or when using LSR for purposes other than screen
reading.
.TP
\--no-a11y
Supress the check to ensure desktop accessibility is enabled at startup and
the warning message if it is not. Useful at the login screen where LSR is
running as a different user.
.TP
\--no-bridge
Supress the loading of any bridge that will make LSR dialogs accessible 
themselves. Useful in some situations where events from LSR dialogs polute 
event logs during debugging. Not useful for users who rely on LSR to read its
own windows.
.TP
\--export-path
Prints the install path of the LSR package to stdout and exits.
.SH EXAMPLES
The following examples show how the lsr script can be used to start, 
stop, and configure LSR. The names of the User Interface Elements 
(UIEs) in this script are fictitious. Substitute the names of the UIEs 
you are trying to install, uninstall, etc. Extrapolate the UIE examples 
to include UIEs of kinds other than those noted.

To start with the default "user" profile, type:

  lsr

To say a UTF-8 encoded string using the default output device, type:
  
  lsr -y 'this is a test'

To install a new perk with the name "PythonPerk", type:
  
  lsr -i usr/local/src/PythonPerk.py

To install a new device with the name "WebcamDevice", type:
  
  lsr -i /usr/local/src/devices/WebcamDevice.py

To uninstall a perk with name "MyPerk", type:

  lsr -u MyPerk

To uninstall a device with name "WebcamDevice", type:

  lsr -u WebcamDevice

To create a new profile with name "pete", type:

  lsr -c pete

To remove a profile with name "pete", type:

  lsr -r pete

To run with the "developer" profile, type:

  lsr -p developer

To specify that the installed "FirefoxPerk" should be loaded in the "user" 
profile, type:

  lsr -a FirefoxPerk -p user

To specify that the installed "WebcamDevice" should be loaded on startup in 
all profiles, type:

  lsr -a WebcamDevice
  
To specify that the installed "IBMSpeech" device should be loaded first on
startup in the user profile, type:

  lsr -a IBMSpeech --index=0 -p user

To associate the installed "CalcPerk" should be loaded in the "developer" and
"pete" profiles, type:

  lsr -a CalcPerk -p developer -p pete

To specify that the installed "WebcamDevice" device should no longer be 
loaded on startup in the "user" profile, type:

  lsr -d WebcamDevice -p user 

To specify that the installed "DebugPerk" will be loaded before all other Perks
when using the "developer" profile, type:

  lsr -a DebugPerk -p developer --index=0

To create and install a new, templated Perk called "gEditPerk" in the current 
working directory, associate it with the "developer" profile, and set it to 
load when the only "gedit" application is activated, type:

  lsr -g gEditPerk.py perk --app=gedit -p developer

To create, install, and associate a new, templated Perk called "TestPerk" with 
the profile named "john" so that it is loaded for all applications, type:

  lsr -g TestPerk.py perk -p john --all-apps
  
To create and install a new, templated Perk called "SpellCheckPerk" so that it
is not loaded automatically, but must be manually loaded, type:

  lsr -g SpellCheckPerk.py perk -p user

.SH BUGS
See the lsr module at http://bugzilla.gnome.org.
.SH WEBSITE
Visit the LSR home page at http://live.gnome.org/LSR for user and developer 
documentation, new releases, contact information, etc.
.SH COPYRIGHT
Copyright 2005, 2006 IBM Corporation.
Licensed under the BSD License
<http://www.opensource.org/licenses/bsd-license.php>
