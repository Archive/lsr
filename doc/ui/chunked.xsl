<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:date="http://exslt.org/dates-and-times"
                version="1.0">
  <xsl:import href="/usr/share/sgml/docbook/xsl-stylesheets/xhtml/chunk.xsl"/>
  <xsl:param name="draft.mode">no</xsl:param>
  <xsl:param name="html.extra.head.links">1</xsl:param>
  <xsl:param name="draft.watermark.image">../db/images/draft.png</xsl:param>
  <xsl:param name="generate.toc">
article title,toc,example,table,figure
section toc
  </xsl:param>
  <xsl:param name="toc.max.depth">2</xsl:param>
  <xsl:param name="chunk.section.depth">1</xsl:param>
  <xsl:param name="section.autolabel">1</xsl:param>
  <xsl:param name="use.id.as.filename">1</xsl:param>
  <xsl:param name="section.label.includes.component.label">1</xsl:param>
  <xsl:param name="generate.section.toc.level">1</xsl:param>
  <xsl:param name="toc.section.depth">3</xsl:param>
  <xsl:param name="chunk.first.sections">1</xsl:param>
  <xsl:param name="html.stylesheet">../db/styles/chunked.css</xsl:param>
  <xsl:template match="section[@role = 'noToc']" mode="toc"/>
  <xsl:param name="admon.graphics">1</xsl:param>
  <xsl:param name="admon.graphics.path">../db/images/</xsl:param>
  <xsl:param name="callout.unicode">1</xsl:param>
  <xsl:param name="callout.graphics">0</xsl:param>
  <xsl:param name="header.rule">0</xsl:param>
  <xsl:param name="footer.rule">0</xsl:param>
  <xsl:template name="user.footer.navigation">
    <div class="userfooter">
      <p>Generated 
        <xsl:call-template name="datetime.format">
          <xsl:with-param name="date" select="date:date-time()"/>
          <xsl:with-param name="format">Y/m/d X</xsl:with-param>
        </xsl:call-template>
      </p>
      <p>Copyright &#x00A9; 2006, 2007 IBM</p>
    </div>
  </xsl:template>
</xsl:stylesheet>
