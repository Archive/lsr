#!/bin/bash

# start the logging server
./logserver.py test.log localhost 8000 &

# start lsr under profile autotest with xml-rpc logging
lsr --log-conf=./xmlrpc-log.conf -p autotest &

# get the LSR package path
LSR_PATH=`lsr --export-path`

# start dogtail test case with xml-rpc logging
PYTHONPATH=$PYTHONPATH:$LSR_PATH python example/gedit.py ./xmlrpc-log.conf

# shutdown
sleep 1
pkill lsr
sleep 3
pkill logserver
