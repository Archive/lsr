#!/usr/bin/python
'''
Defines a simple message logging server that listens for XML-RPC calls on a
configurable port and serializes them to a text file.

@author: Peter Parente
@organization: IBM Corporation
@copyright: Copyright (c) 2006 IBM Corporation
@license: The BSD License

All rights reserved. This program and the accompanying materials are made
available under the terms of the BSD License which accompanies
this distribution, and is available at
U{http://www.opensource.org/licenses/bsd-license.php}
'''
from SimpleXMLRPCServer import SimpleXMLRPCServer
import signal, sys

class SerialLogServer(object):
  def __init__(self, fn, host, port):
    self.log = file(fn, 'w')
    self.alive = False
    self.addr = (host, port)

  def write(self, msg):
    msg = msg.encode('utf-8')
    self.log.write(msg+'\n')
    self.log.flush()
    return True

  def close(self, *args):
    self.log.close()
    self.alive = False

  def _run(self):
    self.alive = True
    server = SimpleXMLRPCServer(self.addr, logRequests=False)
    server.register_instance(self)
    while self.alive:
      server.handle_request()

try:
  fn = sys.argv[1]
except IndexError:
  fn = 'test.log'
try:
  host = sys.argv[2]
except IndexError:
  host = 'localhost'
try:
  port = int(sys.argv[3])
except IndexError:
  port = 9000

print 'Python XML-RPC logging server'
print 'usage: logserver [filename=test.log, [host=localhost, [port=9000]]]'
print
server = SerialLogServer(fn, host, port)
signal.signal(signal.SIGINT, server.close)
print 'Log server running on %s:%s ...' % (host, port)
print 'Logging to file', fn, '...'
server._run()
print 'Log server quit'
