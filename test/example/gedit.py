from dogtail.procedural import *
import AELog
import time, sys, logging

AELog.configure(sys.argv[1])
log = logging.getLogger('dogtail')

DELAY = 1.0

log.info('running gedit')
run("gedit")

log.info('test #01 start')
log.info('focusing on gedit')
focus.application('gedit')
log.info('file menu')
keyCombo("<Alt>f")
time.sleep(DELAY)
log.info('activate open menu item')
type("o")
time.sleep(DELAY)
log.info('select last file')
keyCombo("End")
time.sleep(DELAY)
log.info('OK file dialog')
keyCombo("Return")
time.sleep(DELAY)
log.info('file menu')
keyCombo("<Alt>f")
time.sleep(DELAY)
log.info('activate close menu item')
type("c")
time.sleep(DELAY)
log.info('file menu')
keyCombo("<Alt>f")
time.sleep(DELAY)
log.info('activate quit gedit')
type("q")
log.info('test #01 end')
